Before your development, please check wiki and 'WARNING' label issue to get more information.

If you have the basic concepts of our application, you can continue read our `app/theme`, `app/utilities` and `app/components` folder to know our public functions and variables of the project.

## Table of Contents

* [Build Setup](#build-setup)
* [ReBuild App](#rebuild-app)
* [Available Scripts](#available-scripts)
  * [Start](#start)
  * [Reset start](#reset-start)
  * [Test](#test)
  * [IOS](#ios)
  * [Android](#android)
  * [Lint](#lint)
  * [Link](#link)
* [Playground](#playground)
* [Debug](#debug)

## Getting Started
Before your build, please set up the [environment for React Native Project with Native code](https://facebook.github.io/react-native/docs/getting-started.html) first: 

Installation:
```
# Install dependencies
npm install

# Run on iOS simulator, Or
npm run ios

# Android simulator
npm run android
```

## ReBuild App
Delete ios/build or android/build folder.

## Available Scripts
### Start
`npm start`

Runs your app in development mode.

#### Reset start
`npm reset-start`

Sometimes you may need to reset or clear the React Native packager's cache.

#### Test
`npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### IOS
`npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### Android
`npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup).

#### Lint
`npm run lint`

Run the linting utility.

#### Link
`npm run link`  

Link your native dependencies(see [React Native docs](https://facebook.github.io/react-native/docs/linking-libraries-ios.html) for details).

## Playground
If you want to find a place for testing or something else, try using the containers/playground. You can access it by clicking on the version text in the Home page side bar.

## Debug
If you try any other method but cannot solve the error, please try these steps:
1.  re-install node_modules
2.  delete ios/build and android/build
3.  Delete our app on the simulator or device
4.  npm run reset-start
5.  open another terminal, npm run ios


## Change different environment 
Change Different Environment
1.	Open your project via XCode 
2.	At the upper right-hand corner, press the “Set the active scheme” button, you can select different scheme. 
3.	For development for fake login page, “EASE (DEV_FAKE)” scheme is used. 
4.	For development for real login page, “EASE (DEV)” scheme is used. 
5.	Similarly, SIT for “EASE (SIT)” while PROD for “EASE (PROD)” 
6.	Build once you have select your designate schedule

