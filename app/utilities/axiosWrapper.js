import axios from "axios";

export default function({ url, method, data, headers, webServiceUrl }) {
  return axios.request({
    url,
    baseURL: webServiceUrl,
    method,
    headers,
    data
  });
}
