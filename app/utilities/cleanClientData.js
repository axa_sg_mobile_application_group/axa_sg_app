import { ACTION_TYPES } from "eab-web-api";
import { NavigationActions } from "react-navigation";

export default function(dispatch) {
  dispatch({
    type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
  });
  dispatch(
    NavigationActions.navigate({
      routeName: "Profile"
    })
  );
  dispatch(
    NavigationActions.navigate({
      routeName: "ProductsDetail"
    })
  );
  dispatch(
    NavigationActions.navigate({
      routeName: "NeedsDetail"
    })
  );
}
