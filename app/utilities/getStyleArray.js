/**
 * Check isArray and return styleArray
 * @param {*} style object or array
 * @returns {Array} styleArray
 */
export default style => (Array.isArray(style) ? style : [style]);
