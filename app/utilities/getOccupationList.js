import getEABTextByLangType from "./getEABTextByLangType";

/**
 * getOccupationList
 * @description return filtered occupation list base on industry
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @param {industry} industry - industry value
 * @return [{}] - array of objects
 */
export default function getOccupationList({ optionMap, language, industry }) {
  return optionMap.options
    .filter(option => option.condition === industry || industry === "")
    .map(option => ({
      label: getEABTextByLangType({ data: option.title, language }),
      value: option.value,
      key: option.value
    }));
}
