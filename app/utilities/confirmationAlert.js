import { Alert } from "react-native";
import EABi18n from "./EABi18n";

export default ({
  titlePath,
  messagePath,
  language,
  textStore,
  noOnPress = () => {},
  yesOnPress = () => {}
}) => {
  Alert.alert(
    titlePath
      ? EABi18n({
          path: titlePath,
          language,
          textStore
        })
      : null,
    EABi18n({
      path: messagePath,
      language,
      textStore
    }),
    [
      {
        text: EABi18n({ language, textStore, path: "button.no" }),
        onPress: noOnPress,
        style: "cancel"
      },
      {
        text: EABi18n({
          path: "button.yes",
          textStore,
          language
        }),
        onPress: yesOnPress
      }
    ]
  );
};
