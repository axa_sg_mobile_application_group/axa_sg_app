import { ActionSheetIOS, Alert } from "react-native";
import * as _ from "lodash";
import ImagePicker from "react-native-image-picker";
import EABi18n from "./EABi18n";
import { importAttachmentToCB } from "./DocumentManager";

export default function({
  language,
  textStore,
  onStart,
  onCancel,
  onError,
  options,
  onChange
}) {
  const config = {
    quality: 1.0,
    maxWidth: 800,
    maxHeight: 600,
    storageOptions: {
      skipBackup: true
    },
    cameraType: "back",
    mediaType: "photo"
  };

  if (_.isFunction(onStart)) {
    onStart();
  }

  ActionSheetIOS.showActionSheetWithOptions(
    {
      options: [
        "Cancel",
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.takePhoto"
        }),
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.choosePhoto"
        }),
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.chooseDocument"
        })
      ],
      destructiveButtonIndex: -1,
      cancelButtonIndex: 0
    },
    buttonIndex => {
      if (buttonIndex === 0) {
        if (_.isFunction(onCancel)) {
          onCancel();
        }
      } else if (buttonIndex === 1) {
        // fake file size for displaying and calculating otherwise AXA will fffffffffffff...
        // the size will be between 800.0KB to 999.9KB
        const fakeFileSize = () => {
          let size = "";
          let beforePoint = 3;
          while (beforePoint) {
            if (
              size.length === 1 &&
              (parseInt(size, 0) === 0 || parseInt(size, 0) <= 7)
            ) {
              beforePoint += 1;
              size = "";
            }
            size += Math.floor(Math.random() * 10);
            beforePoint -= 1;
          }
          size += `.${Math.floor(Math.random() * 10)}KB`;
          return size;
        };

        ImagePicker.launchCamera(config, response => {
          if ((response.error || !response.data) && !response.didCancel) {
            onError(
              `Error during Supporting Document Take Photo, Reason: ${
                response.error
              }`
            );
          } else if (response.didCancel) {
            onCancel();
          } else {
            const data = {
              base64: `data:image/jpeg;base64,${response.data}`,
              fileSize: fakeFileSize(),
              type: "image/png",
              fileName: 0,
              timestamp: 0,
              width: response.width,
              height: response.height
            };
            onChange(data);
          }
        });
      } else if (buttonIndex === 2) {
        ImagePicker.launchImageLibrary(config, response => {
          if (response.didCancel) {
            if (_.isFunction(onCancel)) {
              onCancel();
            }
            return;
          } else if (response.error) {
            if (_.isFunction(onError)) {
              onError(
                `Error during Supporting Document Choose Photo, Reason: ${
                  response.error
                }`
              );
            }
            return;
          }

          const maxTotalDocumentSize = options.maxUploadSize * 1000 * 1000;
          const isExceededMaximumFileSize =
            options.totalDocumentSize + response.fileSize >
            maxTotalDocumentSize;

          if (isExceededMaximumFileSize) {
            Alert.alert(
              EABi18n({
                language,
                textStore,
                path: "clientForm.actionSheet.exceededMaximumFileSize"
              })
            );
            if (_.isFunction(onCancel)) {
              onCancel();
            }
            return;
          }
          const fileType = response.uri.slice(-3).toLocaleLowerCase();
          if (fileType !== "jpg" && fileType !== "png") {
            Alert.alert(
              EABi18n({
                language,
                textStore,
                path: "clientForm.actionSheet.invalidFileFormat"
              })
            );
          } else if (response.fileSize < 3 * 1000 * 1000) {
            const fileUnit = ["B", "KB", "MB", "GB", "TB"];
            let unitIndex = 0;
            let displaySize = response.fileSize;
            while (displaySize > 1000) {
              displaySize /= 1000;
              unitIndex += 1;
            }
            const displayFileSize = `${Math.round(displaySize * 10) / 10}${
              fileUnit[unitIndex]
            }`;
            const data = {
              base64: `data:image/jpeg;base64,${response.data}`,
              fileSize: displayFileSize,
              type: response.type,
              fileName: response.fileName,
              timestamp: response.timestamp,
              width: response.width,
              height: response.height
            };
            onChange(data);
          } else {
            Alert.alert(
              EABi18n({
                language,
                textStore,
                path: "clientForm.actionSheet.fileSizeTooBig"
              })
            );
          }
        });
      } else if (buttonIndex === 3) {
        options.language = language;
        importAttachmentToCB(options);
      }
    }
  );
}
