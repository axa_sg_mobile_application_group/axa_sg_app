import { bz } from "eab-core-api";
import axiosWrapper from "./axiosWrapper";

export default ({ url, method, data, session, headers, webServiceUrl }) =>
  webServiceUrl
    ? axiosWrapper({
        url,
        data,
        method,
        headers,
        webServiceUrl
      })
    : bz.handleRequest({
        url,
        data,
        session
      });
