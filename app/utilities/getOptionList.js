import getEABTextByLangType from "./getEABTextByLangType";

/**
 * getOptionList
 * @description convert option input to correct display format
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @return {array} array of option objects
 */
export function getOptionList(
  { optionMap, language, testID = "" },
  selectedValue = null
) {
  let filterArr = optionMap.options;

  if (selectedValue) {
    filterArr = filterArr.filter(
      option =>
        option.condition && selectedValue && option.condition === selectedValue
    );
  }

  return filterArr.map((option, index) => ({
    labelOnSelecting: getEABTextByLangType({
      data: option.title,
      language
    }),
    testID,
    value: option.value,
    key: `${index}: ${option.value}`,
    label: option.countryName
      ? `${getEABTextByLangType({
          data: option.title,
          language
        })} ${getEABTextByLangType({ data: option.countryName, language })}`
      : getEABTextByLangType({
          data: option.title,
          language
        })
  }));
}

/**
 * getOptionValue
 * @description convert option input to correct display format
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @param {string} value - the target value of the optionMap.options object
 * @return {string} value string
 */
export function getOptionValue({ optionMap, language, value }) {
  const filterArr = optionMap.options.filter(option => option.value === value);
  if (filterArr[0]) {
    return getEABTextByLangType({ data: filterArr[0].title, language });
  }
  return value;
}

/**
 * getAssetClassName
 * @description special handling for getting the name of asset class
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @param {string} titleKey - the target field name of the optionMap.options object
 * @return {string} title string
 */
export function getAssetClassName({ optionMap, language, titleKey }) {
  let title = "";
  Object.entries(optionMap.options).forEach(([key, value]) => {
    if (key === titleKey) {
      title = getEABTextByLangType({ data: value.title, language });
    }
  });
  return title;
}

/**
 * getSupportingDocumentsName
 * @description special handling for getting the displayName and fileName of the supporting documents
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @param {string} titleKey - the target field name of the optionMap.options object
 * @return {string} title string
 */
export function getSupportingDocumentsName({ optionMap, titleKey }) {
  let data = {};
  Object.entries(optionMap.values).forEach(([key, value]) => {
    if (key === titleKey) {
      data = {
        displayName: value.name,
        fileName: value.filename
      };
    }
  });
  return data;
}

/**
 * isTriggerExist
 * @description convert option input to correct display format
 * @param {string} selectedValue - value from "Country of Residence"
 * @param {object} targetOptionMap - optionMap to be handled
 * @return {array} - array of option objects
 */
export function isTriggerExist(selectedValue, targetOptionMap) {
  if (!selectedValue || !targetOptionMap) {
    return false;
  }

  for (let i = 0; i < targetOptionMap.options.length; i += 1) {
    if (targetOptionMap.options[i].value === selectedValue) {
      return targetOptionMap.options[i].trigger;
    }
  }

  return false;
}

/**
 * getOptionListForCountryCode
 * @description show the selected option's value instead of ites label, a dedicated functoin for country code only
 * @param {object} optionMap - optionMap to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @return {array} - array of option objects
 */
export function getOptionListForCountryCode({ optionMap, language }) {
  return optionMap.options.map(option => ({
    labelOnSelecting: `${getEABTextByLangType({
      data: option.title,
      language
    })} ${getEABTextByLangType({ data: option.countryName, language })}`,
    value: option.value,
    key: `${option.value}${getEABTextByLangType({
      data: option.countryName,
      language
    })}`,
    label: getEABTextByLangType({ data: option.title, language })
  }));
}

/**
 * getOptionListForDynamicSelectorField
 * @description convert options in dynamic json template for component
 * @param {object} options - options to be handled
 * @return {array} - array of option objects
 */
export function getOptionListForDynamicCompnent(options) {
  return options.map(option => ({
    key: option.value,
    value: option.value,
    label: option.title
  }));
}
