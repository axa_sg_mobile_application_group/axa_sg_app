import { AsyncStorage } from "react-native";
import getRemoteTextStore from "../apis/getRemoteTextStore";

/**
 * EABi18n
 * @description get the translated text. please use it with the reducer, do not use it
 *   directly in the page.
 * @param {string} path - the text's path in store
 * @param {string} language - the language that will translate into
 * @param {object} textStore - the text Store
 * */
export default ({ path, language, textStore }) => {
  if (path === "") {
    return "";
  }
  const text = textStore[language][path];

  if (text === undefined) {
    throw new Error(
      `textStore path '${path}' undefined. Have you updated eab-web-api?`
    );
  }
  return text;
};
/**
 * EABi18n
 * @description indicate textStore path is valid or not
 * @param {string} path - the text's path in store
 * @param {string} language - the language that will translate into
 * @param {object} textStore - the text Store
 * */
export const isTextPathUndefined = ({ path, language, textStore }) =>
  textStore[language][path] === undefined;
/**
 * initialTextStore
 * @description fetch text store from the remote space. the work flow is:
 * 1: is the async storage have a copy of text store?
 * |--yes: get it and call the remote.
 * |
 * |--no: get the text store from the app source code.
 *
 * 2: check the text store is it the newest version?
 * |--yes: save it into the reducer
 * |
 * |--no: get the newest version text store from the remote and save it into the reducer, also
 *   save it into async storage
 * @param {object} textStore - the text store from the app source code
 * */
export const initialTextStore = async textStore => {
  let storageTextStore = {};
  let processedText = {};

  await AsyncStorage.getItem("textStore")
    .then(response => {
      /* check the text store is not null */
      storageTextStore = response !== null ? response : textStore;
    })
    .catch(() => {
      /* get text store from async storage fail. return the default text store */
      storageTextStore = textStore;
    });

  await getRemoteTextStore({
    textStoreVersion: storageTextStore.version,
    token: "pass"
  })
    .then(response => {
      const newestTextStoreJson = response.data;

      if (response.data.version === storageTextStore.version) {
        /* save the newest text store */
        AsyncStorage.setItem("textStore", newestTextStoreJson).catch(() => {
          /* TODO save text store to async storage fail. */
        });

        processedText = JSON.parse(newestTextStoreJson);
      } else {
        /* the local version is newest */
        processedText = storageTextStore;
      }
    })
    .catch(() => {
      /* can not get the text store from the remote. return the default text store */
      processedText = storageTextStore;
    });

  return processedText;
};
