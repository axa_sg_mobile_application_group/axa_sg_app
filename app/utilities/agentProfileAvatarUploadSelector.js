import { ActionSheetIOS, Alert } from "react-native";
import * as _ from "lodash";
import ImagePicker from "react-native-image-picker";
import EABi18n from "./EABi18n";

export default function({
  language,
  textStore,
  onStart,
  onCancel,
  onError,
  onChange
}) {
  const config = {
    quality: 1.0,
    maxWidth: 800,
    maxHeight: 600,
    storageOptions: {
      skipBackup: true
    },
    cameraType: "back",
    mediaType: "photo"
  };

  if (_.isFunction(onStart)) {
    onStart();
  }

  ActionSheetIOS.showActionSheetWithOptions(
    {
      options: [
        "Cancel",
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.takePhoto"
        }),
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.choosePhoto"
        })
      ],
      destructiveButtonIndex: -1,
      cancelButtonIndex: 0
    },
    buttonIndex => {
      if (buttonIndex === 0) {
        if (_.isFunction(onCancel)) {
          onCancel();
        }
      } else if (buttonIndex === 1) {
        ImagePicker.launchCamera(config, response => {
          if (response.error || !response.data) {
            onError(
              `Error during Supporting Document Take Photo, Reason: ${
                response.error
              }`
            );
          } else {
            const data = {
              base64: `data:image/jpeg;base64,${response.data}`,
              type: "image/png"
            };
            onChange(data);
          }
        });
      } else if (buttonIndex === 2) {
        ImagePicker.launchImageLibrary(config, response => {
          if (response.error) {
            if (_.isFunction(onError)) {
              onError(
                `Error during Supporting Document Choose Photo, Reason: ${
                  response.error
                }`
              );
            }
            return;
          }
          const fileType = response.uri.slice(-3).toLocaleLowerCase();
          if (fileType !== "jpg" && fileType !== "png") {
            Alert.alert(
              EABi18n({
                language,
                textStore,
                path: "clientForm.actionSheet.invalidFileFormat"
              })
            );
          } else {
            const data = {
              //   base64: `data:image/${fileType};base64,${response.data}`,
              base64: response.data,
              type: `image/${fileType}`
            };
            onChange(data);
          }
        });
      }
    }
  );
}
