import { NativeModules } from "react-native";
import xml2js from "react-native-xml2js";
import headerFooterTemplate from "./template/commonTemplate";

const _ = require("lodash");

/**
 * convert json to xml
 * @param {object} json - json object
 * @param {function} callback - callback after conversion
 * @return {undefined} no return result
 */
module.exports.data2Xml = function data2Xml(json, callback) {
  let xml = "";
  try {
    if (json != null) {
      const builder = new xml2js.Builder();
      xml = builder.buildObject(json);
    }
  } catch (ex) {
    xml = "";
  }
  if (callback) {
    callback(xml);
  }
};

const getByLang = (obj, lang) => {
  if (!obj) {
    return obj;
  }
  if (obj && lang && obj[lang]) {
    return obj[lang];
  }

  // By default get en
  if (_.has(obj, "en")) {
    return obj.en;
  }

  let val = null;
  _.forEach(obj, v => {
    if (!_.isEmpty(v)) {
      val = v;
      return false;
    }
    return true;
  });
  return val;
};

const prepareFnaReportXsl = (reportTemplates, lang) => {
  const { headerFooter } = headerFooterTemplate.commonTemplate;
  const { xsltHeader, xsltFooter } = headerFooter;
  let content = "";

  _.forEach(reportTemplates, reportTemplate => {
    const templates = getByLang(reportTemplate.template, lang);
    const pageHeader = `<div style="margin: 0px 60px; position: relative;min-height: 100%;">`;
    const pageFooter = `</div>`;
    _.forEach(templates, template => {
      content += pageHeader + template + pageFooter;
    });
  });

  return xsltHeader + content + xsltFooter;
};
module.exports.prepareFnaReportXsl = prepareFnaReportXsl;

module.exports.prepareDynamicReportXsl = function prepareDynamicReportXsl(
  reportTemplate,
  lang,
  startingPage,
  totalPage
) {
  const pageHeaderContent = getByLang(reportTemplate.header, lang) || "";
  const pageFooterContent = getByLang(reportTemplate.footer, lang) || "";
  const overlayContent = getByLang(reportTemplate.overlay, lang) || "";

  const { commonTemplateDynamic } = headerFooterTemplate;
  const { pageHeader, pageFooter } = commonTemplateDynamic.headerFooter;

  const templates = getByLang(reportTemplate.template);
  let content = "";
  for (let i = 0; i < templates.length; i += 1) {
    const thisPageFooterContent = pageFooterContent
      .replace("[[@PAGE_NO]]", startingPage + i)
      .replace("[[@TOTAL_PAGE]]", totalPage);

    const pageContent = `<div>${templates[i]}</div>`;

    const thisPageHeader = pageHeader.replace("%s", pageHeaderContent);
    const thispageFooter = pageFooter.replace("%s", thisPageFooterContent);

    content += overlayContent + thisPageHeader + pageContent + thispageFooter;
  }

  let head =
    '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output><xsl:template match="/">';
  const foot = "</xsl:template></xsl:stylesheet>";
  return head + content + foot;
};

module.exports.prepareDynamicReportHtml = function prepareDynamicReportHtml(
  reportTemplate,
  lang
) {
  let content = "";
  const pageHeader = `<div style="width: calc(100% - 120px);margin: 0px 60px; position: relative; min-height: 100%;">`;
  const pageFooter = `</div>`;
  const templates = reportTemplate[lang];
  _.forEach(templates, (template, index) => {
    content +=
      (index ? "" : '<div class="pagebreak"/>') +
      pageHeader +
      template +
      pageFooter;
  });
  return content;
};

module.exports.prepareAppFormXSL = function prepareAppFormXSL(
  reportTemplates,
  lang
) {
  let content = "";

  reportTemplates.forEach(reportTemplate => {
    const template = getByLang(reportTemplate.template, lang);
    content += template;
  });

  let head =
    '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output>';
  const foot = "</xsl:stylesheet>";

  return head + content + foot;
};

// var prepareSupervisorXSL = function(reportTemplates, lang, startingPage, totalPage, callback){
//   var content = reportTemplates;

//   var head = '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
//   head += '<xsl:output method="html"></xsl:output>'
//   var foot = '</xsl:stylesheet>';

//   return head + content + foot;
// };
// module.exports.prepareSupervisorXSL = prepareSupervisorXSL;

const prepareReportXSL = function prepareReportXSL(
  reportTemplate,
  lang,
  startingPage,
  totalPage
) {
  const pageHeaderContent = getByLang(reportTemplate.header, lang) || "";
  const pageFooterContent = getByLang(reportTemplate.footer, lang) || "";
  const overlayContent = getByLang(reportTemplate.overlay, lang) || "";

  const { commonTemplate } = headerFooterTemplate;
  const { pageHeader } = commonTemplate.headerFooter;
  const { pageFooter } = commonTemplate.headerFooter;

  const templates = getByLang(reportTemplate.template);
  let content = "";
  for (let i = 0; i < templates.length; i += 1) {
    const thisPageFooterContent = pageFooterContent
      .replace("[[@PAGE_NO]]", startingPage + i)
      .replace("[[@TOTAL_PAGE]]", totalPage);

    const pageContent = `<div>${templates[i]}</div>`;

    const thisPageHeader = pageHeader.replace("%s", pageHeaderContent);
    const thispageFooter = pageFooter.replace("%s", thisPageFooterContent);

    content += overlayContent + thisPageHeader + pageContent + thispageFooter;
  }

  let head =
    '<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  head += '<xsl:output method="html"></xsl:output><xsl:template match="/">';
  const foot = "</xsl:template></xsl:stylesheet>";
  return head + content + foot;
};
module.exports.prepareReportXSL = prepareReportXSL;

module.exports.xsltTransform = (xml, xsl, callback) => {
  NativeModules.PdfGenerator.convertToHTML(xml, xsl).then(html => {
    if (callback) {
      callback(html);
    }
  });
};

module.exports.prepareDynamicHtml = (body, style) => {
  const { commonTemplateDynamic } = headerFooterTemplate;
  const styles = [style, commonTemplateDynamic.style];

  const styleHtml = `<style>${_.join(styles, "\n")}</style>`;
  return `<html><head>${styleHtml}</head><body>${body}</body></head></html>`;
};

module.exports.prepareHtml = (body, style) => {
  const { commonTemplate } = headerFooterTemplate;
  const styles = [style, commonTemplate.style];

  // issue of phantomJS 2, dpi for different platforms are different, ref: https://github.com/ariya/phantomjs/issues/12685
  // windows: 96 dpi, linux: 72 dpi
  // if (process.platform !== "win32") {
  //   styles.push("html{zoom:75%;}");
  // }

  const styleHtml = `<style>${_.join(styles, "\n")}</style>`;

  return `<html><head>${styleHtml}</head><body>${body}</body></head></html>`;
};

// //for one page content, the library will print a blank page at the end (total 2 pages printed)
// //to fix this, remove css html{height:100%}
// //=> commonTemplate.onePageStyle = commonTemplate.style, except removed html{height:100%}
// module.exports.prepareHtmlOnePage = (body, style) => {
//   var commonTemplate = require('./template.js').commonTemplate;
//   var styles = [style, commonTemplate.onePageStyle];

//   // issue of phantomJS 2, dpi for different platforms are different, ref: https://github.com/ariya/phantomjs/issues/12685
//   // windows: 96 dpi, linux: 72 dpi
//   if (process.platform !== 'win32') {
//     styles.push('html{zoom:75%;}');
//   }

//   var styleHtml = '<style>' + _.join(styles, '\n') + '</style>';
//   return '<html><head>' + styleHtml + '</head><body>' + body + '</body></head></html>';
// };

module.exports.html2Pdf = (html, pdfOptions) =>
  new Promise(resolve => {
    NativeModules.PdfGenerator.convertToPDF(
      html,
      Object.assign({}, pdfOptions, { base64: true })
    ).then(base64PDF => {
      resolve(base64PDF.base64);
    });
  });
