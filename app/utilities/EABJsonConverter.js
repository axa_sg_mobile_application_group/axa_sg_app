import { LANGUAGE_TYPES } from "eab-web-api";
import icCall from "../assets/images/icCall.png";
import icDirections from "../assets/images/icDirections.png";
import icEmail from "../assets/images/icEmail.png";

/**
 * constants
 * */
export const EAB_JSON_TYPES = {
  PROFILE_TAB_LAYOUT: "PROFILE_TAB_LAYOUT"
};

const ITEM_TYPES = {
  PICKER: "picker",
  CONCATENATE: "concatenate",
  TEXT: "text",
  DATE: "date",
  MULTI_LINE: "multiLine",
  CURRENCY: "currency"
};

const REPLACE_STRING = "%s";

const ICON_TYPES = {
  phone: icCall,
  mail: icEmail,
  direction: icDirections
};

const JSON_LANGUAGE_TYPES = {
  [LANGUAGE_TYPES.ENGLISH]: "en",
  [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: "zh-Hant"
};

const TRIGGER_TYPES = {
  SHOW_IF_NOT_EQUAL: "showIfNotEqual",
  NOT_EMPTY: "notEmpty",
  DYNAMIC_CONDITION: "dynamicCondition"
};

/**
 * EABJsonConverter
 * @description convert json file to available data
 * @param {string} type - json file type
 * @param {object} json - object that convert from json
 * @param {object} data - needed data
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * @param {object} optionsMap - picker type item needed data
 * */
export default function({ type, json, data, language, optionsMap = {} }) {
  /**
   * functions
   * */
  const getItemDisplayedString = ({
    item,
    itemLanguage,
    itemData,
    itemOptionsMap
  }) => {
    let returnData = "";
    let dataLog = "";

    switch (item.type) {
      case ITEM_TYPES.PICKER:
        if (typeof item.options === "string") {
          /* get value from optionsMap */
          if (
            Object.prototype.hasOwnProperty.call(itemOptionsMap, item.options)
          ) {
            itemOptionsMap[item.options].options.forEach(option => {
              if (option.value === itemData[item.id]) {
                /* is it selected other? */
                if (option.isOther) {
                  return itemData[`${item.id}Other`];
                }
                /* some options only support one language */
                return typeof option.title === "string"
                  ? option.title
                  : option.title[JSON_LANGUAGE_TYPES[itemLanguage]];
              }
              return "";
            });
          } else {
            const errorMessage = `optionsMap do not have '${
              item.options
            }' key. please check optionsMap json.`;
            throw new Error(errorMessage);
          }
        } else {
          /* get value form json */
          item.options.forEach(option => {
            if (option.value === itemData[item.id]) {
              /* is it selected other? */
              if (option.isOther) {
                return itemData[`${item.id}Other`];
              }
              return option.title[JSON_LANGUAGE_TYPES[itemLanguage]];
            }
            return "";
          });
        }
        break;
      case ITEM_TYPES.CONCATENATE:
        /* initialize */
        returnData = "";

        /* loop concatenate items and combine it */
        item.items.forEach(concatenateItem => {
          dataLog = getItemDisplayedString({
            item: concatenateItem,
            itemLanguage,
            itemData,
            itemOptionsMap
          });

          /* do not add if the data is null */
          if (dataLog && dataLog !== "-") {
            returnData += `${dataLog}`;
          }
        });

        /* check condition before return */
        if (returnData === "") {
          return item.condition === "all" || item.condition === "skip"
            ? null
            : "-";
        }
        return returnData;
      case ITEM_TYPES.MULTI_LINE:
        /* initialize */
        returnData = "";

        /* loop concatenate items and combine to a paragraph */
        item.items.forEach(concatenateItem => {
          dataLog = getItemDisplayedString({
            item: concatenateItem,
            itemLanguage,
            data,
            itemOptionsMap
          });

          /* do not add if the data is null */
          if (dataLog && dataLog !== "-") {
            returnData += `${dataLog}\n`;
          }
        });

        /* remove superfluous space and line */
        returnData.replace(/^\s+\\n(\\n)*( )*(\\n)*\\n+$\s/g, "\n");

        /* check condition before return */
        if (returnData === "") {
          return item.condition === "all" || item.condition === "skip"
            ? null
            : "-";
        }
        return returnData;
      case ITEM_TYPES.DATE:
      case ITEM_TYPES.TEXT:
        if (itemData[item.id]) {
          return item.format
            ? item.format.replace(REPLACE_STRING, data[item.id])
            : data[item.id];
        }
        return item.condition === "all" || item.condition === "skip" ? "" : "-";
      case ITEM_TYPES.CURRENCY:
        return `$${data[item.id]}`;
      default: {
        const errorMessage = `EABJsonConverter do not support item type ${
          item.type
        }, please create a gitlab issue.`;
        throw new Error(errorMessage);
      }
    }
    return "";
  };

  const triggerCheck = ({ trigger, triggerData }) => {
    if (trigger) {
      switch (trigger.type) {
        case TRIGGER_TYPES.SHOW_IF_NOT_EQUAL:
          return triggerData[trigger.id] !== trigger.value;
        case TRIGGER_TYPES.NOT_EMPTY:
          return triggerData[trigger.id];
        case TRIGGER_TYPES.DYNAMIC_CONDITION:
          if (trigger.value && trigger.value.constructor === Array) {
            trigger.value.forEach((value, index) => {
              const reg = new RegExp(`#${index + 1}\\b`, "g");
              return eval(
                triggerData[trigger.id].replace(reg, `_value[${index}]`)
              );
            });
          } else {
            return false;
          }
          break;
        default: {
          const errorMessage = `triggerCheck do not support trigger type ${
            trigger.type
          }, please create a gitlab issue.`;
          throw new Error(errorMessage);
        }
      }
    }
    return true;
  };
  switch (type) {
    case EAB_JSON_TYPES.PROFILE_TAB_LAYOUT: {
      const returnObject = {
        leftSection: [],
        rightSection: []
      };

      json.fields.forEach(field => {
        const section = {};

        /* insert section id */
        section.id = field.id;

        /* insert section title */
        section.title = field.title[JSON_LANGUAGE_TYPES[language]];

        /* insert items to section */
        field.items.forEach(item => {
          if (
            triggerCheck({
              trigger: item.trigger,
              triggerData: data
            })
          ) {
            const itemObject = {};

            /* insert item id */
            itemObject.id = item.id;

            /* insert item title */
            itemObject.title = item.title[JSON_LANGUAGE_TYPES[language]];

            /* insert item icon */
            itemObject.icon = ICON_TYPES[item.icon];

            /* convert data to displayed string */
            itemObject.value = getItemDisplayedString({
              itemData: data,
              itemLanguage: language,
              item,
              itemOptionsMap: optionsMap
            });

            if (section.items && section.items.length !== 0) {
              section.items.push(itemObject);
            } else {
              section.items = [itemObject];
            }
          }
        });

        /* insert section to return object */
        if (field.iOSIsLeftPosition) {
          returnObject.leftSection.push(section);
        } else {
          returnObject.rightSection.push(section);
        }
      });

      return returnObject;
    }
    default:
      throw new Error("The type must come from the constant 'EAB_JSON_TYPES'");
  }
}
