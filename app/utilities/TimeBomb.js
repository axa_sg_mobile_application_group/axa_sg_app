import moment from "moment";
import SInfo from "react-native-sensitive-info";
import {
  SQLITE_ENCRYPT_KEY,
  USER_ID
} from "../constants/ENCRYPTION_FIELD_NAMES";
import { deleteDatabase } from "../utilities/DataManager";
import {
  getTimeBombExpiryDate,
  updateTimeBombExpiryDate,
  deleteSqliteDB
} from "../utilities/SQLiteUtils";

/**
 * getExpiryDate
 * @description get time bomb expiry date from SQLite
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * @param {string} userID - user ID
 * @returns {Promise} Promise object represents the time bomb expiry date stored in SQLite
 * */
export const getExpiryDate = ({ sqliteEncryptionKey, userID }) =>
  getTimeBombExpiryDate(sqliteEncryptionKey, userID);

/**
 * checkTimeBombShouldTrigger
 * @description check if the time bomb should trigger or not
 * @param {string} timeBombExpiryDate - ISO date
 * @returns {boolean} true if the time bomb should trigger
 * */
export const checkTimeBombShouldTrigger = timeBombExpiryDate => {
  const today = moment()
    .hours(0)
    .minutes(0)
    .seconds(0)
    .milliseconds(0);
  const bombTriggerDate = moment(timeBombExpiryDate);
  return today >= bombTriggerDate;
};

/**
 * updateExpiryDate
 * @description update time bomb expiry date to SQLite
 * @param {number} expiryDateISO - ISO date for time bomb expiry date
 * @returns {Promise} Promise object represents the sqlite result on updating time bomb expiry date
 * */
export const updateExpiryDate = expiryDateISO =>
  new Promise(resolve => {
    SInfo.getItem(SQLITE_ENCRYPT_KEY, {})
      .then(sqliteEncryptionKey =>
        SInfo.getItem(USER_ID, {}).then(userID => ({
          sqliteEncryptionKey,
          userID
        }))
      )
      .then(({ sqliteEncryptionKey, userID }) =>
        updateTimeBombExpiryDate(
          sqliteEncryptionKey,
          userID,
          moment(expiryDateISO).valueOf()
        )
      )
      .then(resolve);
  });

/**
 * triggerTimeBomb
 * @description delete CBLite and SQLite when time bomb is trggered
 * @returns {Promise} Promise object represents if the delete action is success
 * */
export const triggerTimeBomb = () =>
  new Promise(resolve => {
    deleteDatabase(resolve);
  })
    .then(() => deleteSqliteDB())
    .then(success => success);
