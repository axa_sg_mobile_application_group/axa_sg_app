import * as _ from "lodash";
import { put, select, call } from "redux-saga/effects";
import { NavigationActions, StackActions } from "react-navigation";
import { ACTION_TYPES, REDUCER_TYPES, FILTER_TYPES } from "eab-web-api";
import { NAVIGATION } from "../../app/constants/REDUCER_TYPES";

/**
 * navigateToApplications
 * @description dispatch Navigation action after handling action type PRE_APPLICATION.UPDATE_APPLICATION_LIST
 * */

function* navigateToNeeds() {
  yield put(
    NavigationActions.navigate({
      routeName: "Needs"
    })
  );
}

/**
 * closeProposal
 * @description dispatch Navigation action after handling action type PROPOSAL.CLOSE_PROPOSAL
 * */
function* closeProposal(action) {
  const { callback } = action;
  const isQuickQuote = yield select(
    state => state[REDUCER_TYPES.QUOTATION].isQuickQuote
  );
  const products = yield select(state => state[NAVIGATION].products);

  if (isQuickQuote) {
    if (products.routes[products.index].routeName !== "QuickQuoteHistory") {
      yield put(StackActions.push({ routeName: "QuickQuoteHistory" }));
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION
      });
    }
  } else {
    yield put({
      type: ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS
    });

    yield put({
      type:
        ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION]
          .UPDATE_UI_APPLICATION_LIST_FILTER,
      selectedAppListFilter: FILTER_TYPES.APPLICATION_LIST.PROPOSED
    });

    yield put(NavigationActions.navigate({ routeName: "Applications" }));
    yield put({ type: ACTION_TYPES[REDUCER_TYPES.QUOTATION].CLEAN_QUOTATION });
  }

  if (_.isFunction) {
    yield call(callback);
  }
}

export default {
  closeProposal,
  navigateToNeeds
};
