/**
 * @jest-environment jsdom
 */

import { put, call, select } from "redux-saga/effects";
import { expectSaga } from 'redux-saga-test-plan';
import { onlineAuthCode } from "./login";
import loginReducer from "../reducers/login";
import {
    fetchAuthToken,
    verifyUserInfo,
    updateMasterKeyToServer
} from "../apis/login";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { LOGIN } from "../constants/REDUCER_TYPES";

describe("config saga", () => {

    it('test', () => {
        expect(1 + 1).toBe(2);
      })

    // const String = 'abc'; 
    // it("should test onlineAuthCode", () => {
    //     const fakeUserID = "peterwong";
    //     const fakeAuthCode = "authCode";
    //     const fakeAuthToken = "auth-token-authCode";
    //     const expectedResult = {
    //         appPassword: null,
    //         authToken: fakeAuthToken,
    //         isLogin: false,
    //         lastLoginTime: null,
    //         loginFlow: null,
    //         showNewAppPasswordScene: false,
    //         timebombExpiryDate: null,
    //         userID: null
    //     };

    //   return expectSaga(onlineAuthCode, {authCode: fakeAuthCode, userID: fakeUserID})
    //     .withReducer(loginReducer)
    //     .provide([
    //         [call(fetchAuthToken, fakeAuthCode), fakeAuthToken]
    //     ])
    //     .put({
    //       type: APP_ACTION_TYPES[LOGIN].ONLINE_AUTH_TOKEN,
    //       authToken: fakeAuthToken,
    //       userID: fakeUserID
    //     })
    //     .run()
    //     .then(result => {
    //       expect(result.storeState).toEqual(expectedResult)
    //     });
    // });
  });
