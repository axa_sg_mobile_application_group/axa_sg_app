import SInfo from "react-native-sensitive-info";
import { put, call, select } from "redux-saga/effects";
import moment from "moment";
import * as _ from "lodash";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { checkVersionAndDevice } from "../apis/login";
import { LOGIN } from "../constants/REDUCER_TYPES";
import {
  TOKEN_EXPIRY_DATE,
  INSTALLATION_ID
} from "../constants/ENCRYPTION_FIELD_NAMES";
import { dataSync } from "../utilities/DataManager";
/**
 * preCheck
 * @description check for auth data.
 * */
export function* preCheck(action) {
  const { callingFunction, alert } = action;
  let isValidForDataSync = true;
  let errorMsg = "";
  let checkVersionAndDeviceResult = {};
  try {
    // Get the appPasswordExpiryDate from keychain
    const tokenExpiryDate = yield call(SInfo.getItem, TOKEN_EXPIRY_DATE, {});
    const tokenExpiryDateToMS = Number(tokenExpiryDate);
    const today = moment(); // Get Today
    const todayToMS = today.valueOf(); // Get Today in terms of ms
    const loginFlowCheck = yield select(state => state.login.loginFlowCheck);
    const isTokenValidPre = yield select(state => state.login.isTokenValid);
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    const environment = yield select(state => state.login.environment);
    const authToken = yield select(state => state.login.authToken);
    if (loginFlowCheck === "local_auth" && !isTokenValidPre) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: false
      });
      isValidForDataSync = false;
      errorMsg = "Auth Token is invalid.";
    } else if (tokenExpiryDateToMS) {
      // compared with appPasswordExpiryDate and todayToMS in terms of ms
      if (todayToMS < tokenExpiryDateToMS) {
        // do
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
          isValid: true
        });

        checkVersionAndDeviceResult = yield call(
          checkVersionAndDevice,
          authToken,
          installationID,
          environment
        );
        if (!checkVersionAndDeviceResult.hasError) {
          if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
            // isValidVersionAndDevice = true --> ok
            // OK la
            isValidForDataSync = true;
          } else {
            // isValidVersionAndDevice = false --> throw errorMsg
            isValidForDataSync = false;
            errorMsg = "Authorization Failed (Error 007)";
          }
        } else {
          isValidForDataSync = false;
          errorMsg = "Authorization Failed (Error 007)";
        }
      } else {
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
          isValid: false
        });
        isValidForDataSync = false;
        errorMsg = "Auth Token is invalid.";
      }
    } else {
      // if cannot get from keychain, bypass the checking on checkAppPasswordExpiryDate;
      // Do Nothing
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: false
      });
      isValidForDataSync = false;
      errorMsg = "Auth Token is invalid.";
    }
    if (isValidForDataSync) {
      if (_.isFunction(callingFunction)) {
        callingFunction();
      } else {
        throw new Error("preCheck do not have callingFunction");
      }
    } else if (_.isFunction(alert)) {
      alert(errorMsg);
    } else {
      throw new Error("preCheck do not have alert");
    }
  } catch (e) {
    if (_.isFunction(alert)) {
      alert("Authorization Checking fail.");
    }
  }
}
/**
 * preCheckOnDataSync
 * @description check and perform dataSync. If checks are all passed, perform
 *   dataSync else, throw error and warning message.
 * */
export function* preCheckOnDataSync(action) {
  const {
    alert,
    closeLoadingFunction = callback => {
      callback();
    }
  } = action;
  const environment = yield select(state => state.login.environment);
  const authToken = yield select(state => state.login.authToken);
  const syncDocumentIds = yield select(state => state.login.syncDocumentIds);
  const agentCode = yield select(state => state.login.agentCode);
  const profileId = yield select(state => state.login.profileId);
  const sessionCookieSyncGatewaySession = yield select(
    state => state.login.sessionCookieSyncGatewaySession
  );
  const sessionCookieExpiryDate = yield select(
    state => state.login.sessionCookieExpiryDate
  );
  const sessionCookiePath = yield select(
    state => state.login.sessionCookiePath
  );

  yield call(preCheck, {
    alert: () => {
      closeLoadingFunction(() => {
        setTimeout(alert, 200);
      });
    },
    callingFunction: () => {
      closeLoadingFunction(() => {
        setTimeout(() => {
          dataSync(
            environment,
            agentCode,
            profileId,
            `Bearer ${authToken}`,
            false,
            false,
            sessionCookieSyncGatewaySession,
            sessionCookieExpiryDate,
            sessionCookiePath,
            syncDocumentIds,
            () => {}
          );
        }, 500);
      });
    }
  });
}

export default {
  preCheck,
  preCheckOnDataSync
};
