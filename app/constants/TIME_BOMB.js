export const LAST_DATA_SYNC_TIME = "lastDataSyncTime";
export const FIRST_WARNING = process.env.NODE_ENV !== "production" ? 99999 : 5;
export const SECOND_WARNING = process.env.NODE_ENV !== "production" ? 99999 : 8;
export const TIME_BOMB = process.env.NODE_ENV !== "production" ? 99999 : 15;
export const TYPE_FIRST_WARNING = "typeFirstWarning";
export const TYPE_SECOND_WARNING = "typeSecondWarning";
export const TYPE_TIME_BOMB = "typeTimeBomb";
export const TYPE_SAFE = "typeSafe";
export const DURATION_FOR_APP_PASSWORD = 60;
