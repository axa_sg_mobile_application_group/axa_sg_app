export const NAVIGATION = "navigation";
export const TEXT_STORE = "textStore";
export const LOGIN = "login";
export const PAYMENT = "payment";
export const SUBMISSION = "submission";
export const POLICYNUMBER = "policynumber";
export const INFLIGHT_JOB = "INFLIGHT_JOB";
