import PropTypes from "prop-types";

export default {
  isRequired: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.objectOf(PropTypes.string),
      PropTypes.string
    ])
  ).isRequired
};
