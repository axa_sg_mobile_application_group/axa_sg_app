export const LOGIN_ERROR = "loginError";
export const POLICY_NUMBER_ERROR = "policyNumberError";
export const PAYMENT_ERROR = "paymentError";
export const SUBMISSION_ERROR = "submissionError";
