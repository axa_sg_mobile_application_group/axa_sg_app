// policy number
export const FAIL_GET_POLICY_NO_DOC = "Cannot get policy number document";
export const FAIL_CREATE_POLICY_NO_DOC = "Cannot create policy number document";
export const POLICY_NO_EXHAUSTED =
  "Policy number exhausted, please connect your device to online mode and login again.";
export const FAIL_TO_GET_PN_FROM_API = "Fail to call api to get policy number";
export const FAIL_TO_UPDATE_POLICY_NO_DOC =
  "[Error] Assign policy number failed, Please retry again.";
export const NO_POLICY_NUMBER_AVAILABLE =
  "No policy number available. Please perform Online Authorization to retrieve Policy Number Document";

// login
export const AUTHORIZATION_FAIL_001 = "Authorization Failed (Error 001)";
export const AUTHORIZATION_FAIL_002 = "Authorization Failed (Error 002)";
export const AUTHORIZATION_FAIL_003 = "Authorization Failed (Error 003)";
export const AUTHORIZATION_FAIL_004 = "Authorization Failed (Error 004)";
export const AUTHORIZATION_FAIL_005 = "Authorization Failed (Error 005)";
export const AUTHORIZATION_FAIL_006 = "Authorization Failed (Error 006)";
export const AUTHORIZATION_FAIL_007 = "Authorization Failed (Error 007)";
export const APP_PASSWORD_STAGE_1 =
  "Login failed. Please enter correct credentials";
export const APP_PASSWORD_STAGE_2 =
  "Login failed. Please enter correct credentials";
export const APP_PASSWORD_STAGE_3 =
  "Your account is locked. Please unlock through Reset Password";
export const MUILTIPLE_DEVICE =
  "By activating the app on this device, any un-synced data on a previously registered device will be lost. Please make sure you have synced the data on your other device before proceeding";
// payment
export const FAIL_TO_CALL_API_INVALIDATE_APPLICATION =
  "Fail to call api to invalidate the application";
export const INFLIGHT_JOB_ALERT =
  "Inflight job is running, please press ok and wait";
