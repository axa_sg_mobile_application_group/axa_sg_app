import { Platform } from "react-native";
import Colors from "./colors";
import Alignments from "./alignments";

const ui = {
  dialogHeader: {
    flex: 0,
    flexBasis: "auto",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: Alignments.alignmentXL,
    paddingBottom: Alignments.alignmentL,
    height: 64,
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.coolGrey
  },
  headerRow: {
    position: "relative",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
    width: "100%"
  },
  headerLeft: {
    position: "absolute",
    left: 0
  },
  headerRight: {
    position: "absolute",
    right: 0
  }
};

const platformUI = Platform.select({
  ios: {},
  android: {}
});

export default { ...platformUI, ...ui };
