import { Platform } from "react-native";

const alignment = {
  alignmentXXXXXXL: 200,
  alignmentXXXXXL: 100,
  alignmentXXXXL: 60,
  alignmentXXXL: 48,
  alignmentXXL: 32,
  alignmentXL: 24,
  alignmentL: 16,
  alignmentM: 12,
  alignmentS: 10,
  alignmentXS: 8,
  alignmentXXS: 4,

  textMaxWidth: 632,
  boxMaxWidth: 976,
  statusBarHeight: 20,

  iconStyle: {
    width: 24,
    height: 24
  }
};

const platformAlignment = Platform.select({
  ios: {},
  android: {}
});

export default { ...platformAlignment, ...alignment };
