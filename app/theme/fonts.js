import { Platform } from "react-native";
import Colors from "../theme/colors";

const fonts = new function fontCreator() {
  this.fontFamilyMain = "SF Pro Display";
  this.fontFamilySecondary = "SF Pro Text";
  this.fontSizeXXXL = 34;
  this.fontSizeXXL = 26;
  this.fontSizeXL = 25;
  this.fontSizeL = 22;
  this.fontSizeXXXM = 20;
  this.fontSizeXXM = 17;
  this.fontSizeXM = 15;
  this.fontSizeM = 14;
  this.fontSizeS = 13;
  this.fontSizeXS = 12;
  this.fontSizeXXS = 10;
  /**
   * display
   * */
  this.display1 = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "400",
    fontSize: this.fontSizeXXXL,
    lineHeight: 35,
    letterSpacing: 0.04,
    color: Colors.darkGrey
  };
  this.display2 = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "700",
    fontSize: this.fontSizeXXL,
    lineHeight: 35,
    letterSpacing: 0.04,
    color: Colors.darkGrey
  };
  this.display3 = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "600",
    fontSize: this.fontSizeL,
    lineHeight: 28,
    letterSpacing: 0.35,
    color: Colors.darkGrey
  };
  this.display3White = {
    ...this.display3,
    color: Colors.white
  };
  /**
   * keyLabel
   * */
  this.keyLabelPrimary = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "400",
    fontSize: this.fontSizeXL,
    color: Colors.black
  };
  this.keyLabelPrimary = {
    ...this.keyLabelPrimary,
    color: Colors.white
  };
  /**
   * tagLine1
   * */
  this.tagLine1Primary = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "500",
    fontSize: this.fontSizeXXXM,
    lineHeight: 25,
    letterSpacing: 0.38,
    color: Colors.darkGrey
  };
  this.tagLine2Primary = {
    fontWeight: "bold",
    fontSize: this.fontSizeXXM,
    lineHeight: 25,
    letterSpacing: 0.38,
    color: Colors.darkGrey
  };
  this.tagLine1Negative = {
    ...this.tagLine1Primary,
    color: Colors.negative
  };
  this.tagLine2Secondary = {
    fontFamily: this.fontFamilyMain,
    fontWeight: "400",
    fontSize: this.fontSizeXXXM,
    lineHeight: 25,
    letterSpacing: 0.38,
    color: Colors.grey
  };
  this.tagLine2Brand = {
    ...this.tagLine2Secondary,
    color: Colors.brand
  };
  this.tagLine2White = {
    ...this.tagLine2Secondary,
    color: Colors.white
  };

  /**
   * headline
   * */
  this.headlineNegative = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.41,
    color: Colors.negative
  };
  this.headlineSecondary = {
    ...this.headlineNegative,
    color: Colors.grey
  };
  this.headlinePrimary = {
    ...this.headlineNegative,
    color: Colors.darkGrey
  };
  this.headlinePositive = {
    ...this.headlineNegative,
    color: Colors.positive
  };
  this.headlineWhite = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXM,
    lineHeight: 22,
    letterSpacing: -0.4,
    color: Colors.white
  };
  this.headlineBrand = {
    ...this.headlineWhite,
    color: Colors.brand,
    fontSize: this.fontSizeXXM
  };

  /**
   * title
   * */
  this.title = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.4,
    color: Colors.darkGrey
  };
  /**
   * textButtonLabelNormalEmphasized
   * */
  this.textButtonLabelNormalEmphasizedDisabled = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.41,
    color: Colors.mediumGrey
  };
  this.textButtonLabelNormalEmphasizedAccent = {
    ...this.textButtonLabelNormalEmphasizedDisabled,
    color: Colors.brand
  };
  /**
   * body
   * */
  this.bodyPrimary = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.41,
    color: Colors.darkGrey
  };
  this.bodyBrand = {
    ...this.bodyPrimary,
    color: Colors.brand
  };
  this.bodyNegative = {
    ...this.bodyPrimary,
    color: Colors.negative
  };
  this.bodyPlaceholder = {
    ...this.bodyPrimary,
    color: Colors.mediumGrey
  };
  this.bodyPositive = {
    ...this.bodyPrimary,
    color: Colors.positive
  };
  this.bodySecondary = {
    ...this.bodyPrimary,
    color: Colors.grey
  };
  this.specialBodySecondary = {
    ...this.bodyPrimary,
    color: Colors.grey,
    bottom: 20
  };
  this.BodyTertiary = {
    ...this.bodyPrimary,
    color: Colors.grey,
    bottom: 40
  };
  this.bodyDisabled = {
    ...this.bodyPrimary,
    color: Colors.mediumGrey
  };
  /**
   * textButtonLabelNormal
   * */
  this.textButtonLabelNormalDisabled = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.42,
    color: Colors.mediumGrey
  };
  this.textButtonLabelNormalAccent = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeXXM,
    lineHeight: 22,
    letterSpacing: -0.41,
    color: Colors.brand
  };
  /**
   * header
   * */
  this.headerBrand = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "500",
    fontSize: this.fontSizeXM,
    lineHeight: 20,
    letterSpacing: -0.24,
    color: Colors.brand
  };
  this.headerNegative = {
    ...this.headerBrand,
    color: Colors.negative
  };
  this.headerPrimary = {
    ...this.headerBrand,
    color: Colors.black
  };
  /**
   * rectangleButtonStyle1Label
   * */
  this.rectangleButtonStyle1Label = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "500",
    fontSize: this.fontSizeXM,
    lineHeight: 20,
    letterSpacing: -0.2,
    color: Colors.white
  };
  /**
   * rectangleButtonStyle2Label
   * */
  this.rectangleButtonStyle2Label = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "500",
    fontSize: this.fontSizeXM,
    lineHeight: 20,
    letterSpacing: -0.24,
    color: Colors.brand
  };
  /**
   * subhead
   * */
  this.subheadPlaceholder = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeXM,
    lineHeight: 20,
    letterSpacing: -0.24,
    color: Colors.mediumGrey
  };
  this.subheadPrimary = {
    ...this.subheadPlaceholder,
    color: Colors.darkGrey
  };
  this.subheadNegative = {
    ...this.subheadPlaceholder,
    color: Colors.negative
  };
  this.subheadWhite = {
    ...this.subheadPlaceholder,
    color: Colors.white
  };
  this.subheadAccent = {
    ...this.subheadPlaceholder,
    color: Colors.accent
  };
  this.subheadSecondary = {
    ...this.subheadPlaceholder,
    color: Colors.grey
  };
  /**
   * textButtonLabelSmall
   * */
  this.textButtonLabelSmallAccent = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeXM,
    lineHeight: 20,
    letterSpacing: -0.24,
    color: Colors.brand
  };
  this.textButtonLabelSmallDisabled = {
    ...this.textButtonLabelSmallAccent,
    color: Colors.mediumGrey
  };
  /**
   * indicator
   * */
  this.indicatorSecondary = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeM,
    lineHeight: 19,
    letterSpacing: -0.15,
    color: Colors.grey
  };
  this.indicatorPrimary = {
    ...this.indicatorSecondary,
    color: Colors.darkGrey
  };
  this.indicatorWhite = {
    ...this.indicatorSecondary,
    color: Colors.white
  };
  this.indicatorAccent = {
    ...this.indicatorSecondary,
    color: Colors.accent
  };
  this.indicatorNegative = {
    ...this.indicatorSecondary,
    color: Colors.negative
  };
  /**
   * toolTipLabel
   * */
  this.toolTipLabel = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeM,
    letterSpacing: -0.15,
    color: Colors.white
  };
  /**
   * segmentedControlTabLabel
   * */
  this.segmentedControlTabLabelUnselected = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeS,
    lineHeight: 18,
    letterSpacing: -0.08,
    color: Colors.brand
  };
  this.segmentedControlTabLabelSelected = {
    ...this.segmentedControlTabLabelUnselected,
    color: Colors.white
  };
  /**
   * tabBarLabel
   * */
  this.tabBarLabelSelected = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeS,
    lineHeight: 18,
    letterSpacing: -0.08,
    color: Colors.brand
  };
  this.tabBarLabelUnselected = {
    ...this.tabBarLabelSelected,
    color: "rgb(142,142,147)"
  };
  /**
   * fieldHintTextOrHelperText
   * */
  this.fieldTextOrHelperTextBrand = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: this.fontSizeS,
    lineHeight: 18,
    letterSpacing: -0.07,
    color: Colors.brand
  };
  this.fieldTextOrHelperTextNegative = {
    ...this.fieldTextOrHelperTextBrand,
    color: Colors.negative
  };
  this.fieldTextOrHelperTextPositive = {
    ...this.fieldTextOrHelperTextBrand,
    color: Colors.positive
  };
  this.fieldTextOrHelperTextDisabled = {
    ...this.fieldTextOrHelperTextBrand,
    color: Colors.mediumGrey
  };
  this.fieldTextOrHelperTextNormal = {
    ...this.fieldTextOrHelperTextBrand,
    color: Colors.grey
  };
  this.fieldTextOrHelperTextSelected = {
    ...this.fieldTextOrHelperTextBrand,
    color: Colors.white
  };
  /**
   * tableColumnLabel
   * */
  this.tableColumnLabel = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXS,
    lineHeight: 16,
    color: Colors.grey
  };
  /**
   * textboxRemainLabel
   * */
  this.textboxRemainLabel = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeS,
    color: Colors.grey,
    marginTop: 16
  };
  /**
   * textboxRemainLabelNoSpacing
   * */
  this.textboxRemainLabelNoSpacing = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeS,
    color: Colors.grey
    // marginTop: 16
  };
  /**
   * stepperLabel
   * */
  this.stepperLabelUnselected = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXS,
    lineHeight: 16,
    color: Colors.grey
  };
  this.stepperLabelSelected = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXS,
    lineHeight: 16,
    color: Colors.brand
  };
  /**
   * chipLabel
   * */
  this.chipLabelBrand = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXS,
    lineHeight: 16.2,
    color: Colors.brand
  };
  this.chipLabelWhite = {
    ...this.chipLabelBrand,
    color: Colors.white
  };
  /**
   * caption
   * */
  this.captionAccent = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXS,
    lineHeight: 16.1,
    color: Colors.accent
  };
  this.captionBrand = {
    ...this.captionAccent,
    color: Colors.brand
  };
  this.captionSecondary = {
    ...this.captionAccent,
    color: Colors.grey
  };
  this.captionPositive = {
    ...this.captionAccent,
    color: Colors.positive
  };
  /**
   * this.tagLabel
   * */
  this.tagLabel = {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "600",
    fontSize: this.fontSizeXXS,
    lineHeight: 13,
    letterSpacing: 0.07,
    color: Colors.white
  };
  this.AboutEASETitle = {
    fontSize: this.fontSizeXXM,
    letterSpacing: -0.4,
    lineHeight: 22,
    fontWeight: "700"
  };
  this.AboutEASEDisclaimer = {
    fontSize: this.fontSizeXXM,
    letterSpacing: -0.4,
    lineHeight: 22,
    color: Colors.grey
  };
  this.AboutEASEDescription = {
    fontSize: this.fontSizeXXM,
    letterSpacing: -0.4,
    lineHeight: 22,
    textAlign: "justify"
  };
}();

const platformFonts = Platform.select({
  ios: {},
  android: {}
});

export default { ...platformFonts, ...fonts };
