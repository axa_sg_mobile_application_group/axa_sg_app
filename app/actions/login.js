import ACTION_TYPES from "../constants/ACTION_TYPES";
import { LOGIN } from "../constants/REDUCER_TYPES";
/**
 * LoginFailByOnlineAuth
 * @description change online login fail reducer state
 * @reducer login.isLogin
 * @param {function} dispatch - redux dispatch function
 * */
export const LoginFailByOnlineAuth = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN_FAIL
  });
};

/**
 * onlineAuthCode
 * @description call AXA api to get authToekn by authCode
 * @param {function} dispatch - redux dispatch function
 * @param {String} authCode - authCode is provided from AXA login page
 * */
export const onlineAuthCode = ({ dispatch, authCode, callback }) => {
  // call AXA API which is handled by saga
  dispatch({
    type: ACTION_TYPES[LOGIN].ONLINE_AUTH_CODE,
    authCode,
    callback
  });
};
export const onlineAuthCode2 = ({ dispatch, callback }) => {
  // call AXA API which is handled by saga
  dispatch({
    type: ACTION_TYPES[LOGIN].ONLINE_AUTH_CODE_2,
    callback
  });
};
/**
 * login
 * @description change login state 'login' to true
 * @param {function} dispatch - redux dispatch function
 * */
export const login = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN
  });
};
/**
 * logout
 * @description change login state 'login' to false
 * @reducer login
 * @param {function} dispatch - redux dispatch function
 * */
export const logout = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGOUT
  });
};

/**
 * saveNewAppPassword
 * @description save app password
 * @reducer login.appPassword
 * @param {function} dispatch - redux dispatch function
 * @param {String} hashedAppPassword - hashed password which inputted by user
 * */
export const saveNewAppPassword = ({
  dispatch,
  hashedAppPassword,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].NEW_APP_PASSWORD,
    appPassword: hashedAppPassword,
    callback
  });
};
export const saveNewAppPasswordDEV = ({
  dispatch,
  hashedAppPassword,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].NEW_APP_PASSWORD_DEV,
    appPassword: hashedAppPassword,
    callback
  });
};
/**
 * resetNewAppPassword
 * @description reset app password
 * @reducer login.appPassword
 * @param {function} dispatch - redux dispatch function
 * @param {String} hashedAppPassword - hashed password which inputted by user
 * */
export const resetAppPasswordManually = ({
  dispatch,
  hashedAppPassword,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY2,
    appPassword: hashedAppPassword,
    callback
  });
};

/**
 * checkLoginStatus
 * @description check if it need to activation or local authentication
 * @param {function} dispatch - redux dispatch function
 * */
export const checkLoginStatus = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_LOGIN_STATUS
  });
};

export const checkTokenExpiryDate = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
    callback
  });
};
/**
 * setEnvironment
 * @description login by local app password
 * @param {function} dispatch - redux dispatch function
 * */
export const setEnvironment = ({ dispatch, environment }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].SET_ENVIRONMENT,
    environment
  });
};
/**
 * checkAppLocked
 * @description checkAppLocked
 * @param {function} dispatch - redux dispatch function
 * */
export const checkAppLocked = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_APP_LOCKED,
    callback
  });
};

export const disableInflightAlert = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].DISABLE_INFLIGHT_ALERT,
    callback
  });
};

/**
 * checkFailAttempt
 * @description checkFailAttempt
 * @param {function} dispatch - redux dispatch function
 * */
export const checkFailAttempt = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_FAIL_ATTEMPT,
    callback
  });
};

/**
 * checkValidDevice
 * @description checkValidDevice
 * @param {function} dispatch - redux dispatch function
 * */
export const checkValidDevice = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_VALID_DEVICE,
    callback
  });
};

/**
 * setLoginFlow
 * @description setLoginFlow
 * @param {function} dispatch - redux dispatch function
 * */
export const setAppLocked = ({ dispatch, isLocked }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].SET_APP_LOCKED,
    isLocked
  });
};
/**
 * setLoginFlow
 * @description setLoginFlow
 * @param {function} dispatch - redux dispatch function
 * */
export const setLoginFlow = ({ dispatch, loginFlow }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].SET_LOGIN_FLOW,
    loginFlow
  });
};

/**
 * loginByAppPassword
 * @description login by local app password
 * @param {function} dispatch - redux dispatch function
 * @param {string} userID - user id
 * @param {string} hashedAppPassword - hashed password which inputted by user
 * @param {srring} callback - callback message to display login ststus
 * */
export const loginByAppPassword = ({
  dispatch,
  userID,
  hashedAppPassword,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN_BY_APP_PASSWORD,
    userID,
    hashedAppPassword,
    callback
  });
};
export const loginByAppPasswordDEV = ({
  dispatch,
  userID,
  hashedAppPassword,
  callback
}) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN_BY_APP_PASSWORD_DEV,
    userID,
    hashedAppPassword,
    callback
  });
};
/**
 * loginByOnlineAuth
 * @description login by online using app password to login
 * @param {function} dispatch - redux dispatch function
 * */
export const loginByOnlineAuth = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN_BY_ONLINE_AUTH,
    callback
  });
};

/**
 * verifyOnlineAuth
 * @description verify 121 for OnlineAuth
 * @param {function} dispatch - redux dispatch function

 * */
export const verify121OnlineAuth = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].LOGIN_BY_ONLINE_AUTH2,
    callback
  });
};

export const checkLastDataSyncTime = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].CHECK_LAST_DATA_SYNC_TIME,
    callback
  });
};

export const setLastDataSyncTime = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[LOGIN].SET_LAST_DATA_SYNC_TIME
  });
};
