import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { SUBMISSION as APP_SUBMISSION } from "../constants/REDUCER_TYPES";

const { APPLICATION } = REDUCER_TYPES;

export const validateSubmission = ({ dispatch, docId, callback }) => {
  dispatch({
    type: ACTION_TYPES[APPLICATION].VALIDATE_SUBMISSION,
    docId,
    callback
  });
};

export const dataSync = ({ dispatch, docId, applicationResult, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_SUBMISSION].DATA_SYNC_SUBMISSION,
    docId,
    applicationResult,
    callback
  });
};

export const submitApplication = ({ dispatch, docId, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_SUBMISSION].SUBMIT_APPLICATION,
    docId,
    callback
  });
};
