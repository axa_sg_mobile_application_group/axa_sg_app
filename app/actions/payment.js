import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { PAYMENT as APP_PAYMENT } from "../constants/REDUCER_TYPES";

const { APPLICATION } = REDUCER_TYPES;

/**
 * setPayment
 * @description setPayment
 * @param {function} dispatch - redux dispatch function
 * @param paymentMethod
 * */
export const setPaymentMethod = ({ dispatch, paymentMethod }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_PAYMENT].SET_PAYMENT_METHOD,
    paymentMethod
  });
};

/**
 * initPayment
 * @description init payment
 * @param {function} dispatch - redux dispatch function
 * */
// export const initPayment = ({ dispatch }) => {
//   dispatch({
//     type: APP_ACTION_TYPES[APP_PAYMENT].INIT_PAYMENT
//   });
// };

/**
 * loginByOnlineAuthForPayment
 * @description login by online using app password to login
 * @param {function} dispatch - redux dispatch function
 * */
export const loginByOnlineAuthForPayment = ({ dispatch }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
  });
};

/**
 * setOnlineAuthCodeForPayment
 * @description call AXA api to get authToekn by authCode
 * @param {function} dispatch - redux dispatch function
 * @param {String} authCode - authCode is provided from AXA login page
 * */
export const setOnlineAuthCodeForPayment = ({
  dispatch,
  authCode,
  callback
}) => {
  // call AXA API which is handled by saga
  dispatch({
    type: APP_ACTION_TYPES[APP_PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
    authCode,
    callback
  });
};

/**
 * verify121OnlineAuthForPayment
 * @description verify 121 for OnlineAuth
 * @param {function} dispatch - redux dispatch function
 * */
export const verify121OnlineAuthForPayment = ({ dispatch }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_PAYMENT].LOGIN_BY_ONLINE_AUTH2_FOR_PAYMENT
  });
};

export const getApplication = ({ dispatch, docId, callback }) => {
  dispatch({
    type: ACTION_TYPES[APPLICATION].GET_APPLICATION_FOR_PAYMENT,
    docId,
    callback
  });
};
export const validatePayment = ({ dispatch, docId, callback }) => {
  dispatch({
    type: ACTION_TYPES[APPLICATION].VALIDATE_PAYMENT,
    docId,
    callback
  });
};

export const getPaymentStatus = ({ dispatch, docId, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_PAYMENT].GET_PAYMENT_STATUS,
    docId,
    callback
  });
};

// TODO: Maybe use later
// export const initPayStore = ({ dispatch, docId }) => {
//   dispatch({
//     type: ACTION_TYPES[APPLICATION].INIT_PAYMENT_STORE,
//     docId
//   });
// };

// export const startPayment = ({ dispatch, docId }) => {
//   dispatch({
//     type: ACTION_TYPES[APPLICATION].START_PAYMENT,
//     docId
//   });
// };
