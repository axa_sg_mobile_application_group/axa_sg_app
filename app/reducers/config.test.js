import reducer from '../reducers/config';
import callServerFunction from '../utilities/callServer';
import { Platform } from "react-native";

describe('config reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
      .toEqual({
        callServer: callServerFunction,
        platform: Platform.OS,
        isLoading: false
      });
  });
});
