import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { POLICYNUMBER } from "../constants/REDUCER_TYPES";

const policyNumber = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[POLICYNUMBER].POLICY_NUMBER_DOC:
      return Object.assign({}, state, action.policyNumberData);
    default:
      return state;
  }
};
const initPolicyNumberDoc = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[POLICYNUMBER].INIT_POLICY_NUMBER_DOC:
      state = true;
      return state;
    default:
      return state;
  }
};

const isFullShield = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[POLICYNUMBER].NO_POLICY_NUMBER_AVAILABLE_SHIELD:
      state = true;
      return state;
    default:
      return state;
  }
};

const isFullNonShield = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[POLICYNUMBER].NO_POLICY_NUMBER_AVAILABLE_NON_SHIELD:
      state = true;
      return state;
    default:
      return state;
  }
};

export default combineReducers({
  policyNumber,
  initPolicyNumberDoc,
  isFullShield,
  isFullNonShield
});
