import { createNavigationReducer } from "react-navigation-redux-helpers";
import { combineReducers } from "redux";
import { ApplicationNavigator } from "../routes/Applications";
import { ClientNavigator } from "../routes/Client";
import { LandingNavigator } from "../routes/Landing";
import { LinkClientDialogNavigator } from "../routes/LinkClientDialog";
import { SupportingDocumentNavigator } from "../routes/SupportingDocument";
import { NeedsNavigator } from "../routes/Needs";
import { ProductsNavigator } from "../routes/Products";
import { ProfileNavigator } from "../routes/Profile";
import { RootNavigator } from "../routes/Root";
import { QuotationNavigator } from "../routes/Quotation";

export default combineReducers({
  root: createNavigationReducer(RootNavigator),
  landing: createNavigationReducer(LandingNavigator),
  client: createNavigationReducer(ClientNavigator),
  profile: createNavigationReducer(ProfileNavigator),
  needs: createNavigationReducer(NeedsNavigator),
  products: createNavigationReducer(ProductsNavigator),
  applications: createNavigationReducer(ApplicationNavigator),
  linkClientDialog: createNavigationReducer(LinkClientDialogNavigator),
  supportingDocument: createNavigationReducer(SupportingDocumentNavigator),
  quotation: createNavigationReducer(QuotationNavigator)
});
