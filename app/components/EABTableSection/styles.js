import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1
  }
});
