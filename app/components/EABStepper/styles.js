import { StyleSheet } from "react-native";
import Theme from "../../theme";
import Colors from "../../theme/colors";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    position: "relative",
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: Theme.radiusXS,
    overflow: "hidden"
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Theme.brand,
    height: 48,
    width: 48
  },
  buttonImage: {
    width: 24,
    height: 24
  },
  value: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#3b3fd8",
    minWidth: 64,
    height: 48
  },
  hintTextWrapper: {
    minHeight: 21
  },
  valueText: {
    textAlign: "center",
    color: Colors.white,
    borderBottomWidth: 0
  }
});
