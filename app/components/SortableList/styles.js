import { StyleSheet } from "react-native";
import theme from "../../theme";

export default StyleSheet.create({
  rowsContainer: {
    flex: 1,
    zIndex: 1
  },

  container: {
    position: "absolute",
    borderRadius: 4,
    shadowColor: "rgba(0,0,0,0.2)",
    shadowOpacity: 1,
    shadowOffset: { height: 2, width: 2 },
    shadowRadius: 2,
    flex: 1,
    alignItems: "center",
    flexDirection: "row"
  },
  horizontalContainer: {
    top: 0,
    bottom: 0
  },
  verticalContainer: {
    left: 0,
    right: 0
  },
  icReorder: {
    paddingRight: theme.alignmentXL,
    paddingLeft: theme.alignmentXL,
    paddingTop: theme.alignmentL,
    height: "100%"
  },
  icPlaceholder: {
    paddingTop: theme.alignmentL,
    height: "100%",
    width: theme.alignmentXL
  }
});
