import * as _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ScrollView,
  View,
  StyleSheet,
  RefreshControl,
  ViewPropTypes
} from "react-native";
import { AUTOSCROLL_INTERVAL, ZINDEX } from "./constants";
import { shallowEqual, swapArrayElements } from "./utils";
import Row from "./Row";
import styles from "./styles";
import theme from "../../theme";

/**
 * <SortableList />
 * @description sortable list field component
 * @param {array|object} data - a list of row data, for function "renderRow"
 * @param {function} renderRow - row layout function
 * @param {object} [rowColor={}] - row background color
 * @param {object} [rowMarginVertial=8] - set row margin, padding
 * @param {array} [order=[]] - array of row key, indicating order of rows
 * @param {object} [style={}] - style of this SortableList component view, the view contains scrollView
 * @param {object} [contentContainerStyle={}] - style of scrollView, including header and footer in scrollView
 * @param {object} [innerContainerStyle={}] - style of the content view without header and footer in scrollView
 * @param {boolean} [sortingEnabled=true] - enable sorting or not
 * @param {boolean} [scrollEnabled=true] - enable scrolling or not
 * @param {boolean} [horizontal=false] - it is landscapge mode or not
 * @param {boolean} [showsVerticalScrollIndicator=true] - A Boolean value that controls whether the vertical scroll indicator is visible.
 * @param {boolean} [showsHorizontalScrollIndicator=true] - A Boolean value that controls whether the horizontal scroll indicator is visible.
 * @param {element} [refreshControl=React.createElement(RefreshControl)] - add pull to refresh functionality. When the ScrollView is at scrollY: 0, swiping down triggers an onRefresh event
 * @param {number} [autoscrollAreaSize=60] - autoscroll area size
 * @param {number} [rowActivationTime=200] - row activation time
 * @param {boolean} [manuallyActivateRows=false] - manually activate rows or not
 * @param {function} [renderHeader=()=>{}] - header layout function
 * @param {function} [renderFooter=()=>{}] - footer layout function
 * @param {function} [onChangeOrder=()=>{}] - callback function after order changed
 * @param {function} [onActivateRow=()=>{}] - callback function after row activated
 * @param {function} [onReleaseRow=()=>{}] - callback function after row released
 * @param {function} [onPressRow=()=>{}] - callback function after row pressed
 * */
export default class SortableList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animated: false,
      order: _.isEmpty(props.order) ? Object.keys(props.data) : props.order,
      rowsLayouts: null,
      containerLayout: null,
      data: props.data,
      activeRowKey: null,
      activeRowIndex: null,
      releasedRowKey: null,
      scrollEnabled: props.scrollEnabled
    };
    this.uniqueRowKeyId = 0;
    /**
     * Stores refs to rows’ components by keys.
     */
    this._rows = {};
    /**
     * Stores promises of rows’ layouts.
     */
    this._rowsLayouts = {};
    this._resolveRowLayout = {};

    this._contentOffset = { x: 0, y: 0 };
    this._onLayoutHeader = this._onLayoutHeader.bind(this);
    this._onLayoutFooter = this._onLayoutFooter.bind(this);
    this._onActivateRow = this._onActivateRow.bind(this);
    this._onMoveRow = this._onMoveRow.bind(this);
    this._onScroll = this._onScroll.bind(this);
    this._onRefContainer = this._onRefContainer.bind(this);
    this._onRefScrollView = this._onRefScrollView.bind(this);
    this._onRefRow = this._onRefRow.bind(this);
    this.getUniqueRowKey = this.getUniqueRowKey.bind(this);
    this._onUpdateLayouts = this._onUpdateLayouts.bind(this);
  }

  componentWillMount() {
    this.state.order.forEach(key => {
      this._rowsLayouts[key] = new Promise(resolve => {
        this._resolveRowLayout[key] = resolve;
      });
    });

    if (this.props.renderHeader && !this.props.horizontal) {
      this._headerLayout = new Promise(resolve => {
        this._resolveHeaderLayout = resolve;
      });
    }
    if (this.props.renderFooter && !this.props.horizontal) {
      this._footerLayout = new Promise(resolve => {
        this._resolveFooterLayout = resolve;
      });
    }
  }

  componentDidMount() {
    this._onUpdateLayouts();
  }

  componentWillReceiveProps(nextProps) {
    const { data, order } = this.state;
    const { data: nextData } = nextProps;
    let { order: nextOrder } = nextProps;

    if (data && nextData && !shallowEqual(data, nextData)) {
      nextOrder = _.isEmpty(nextOrder) ? Object.keys(nextData) : nextOrder;
      this.uniqueRowKeyId += 1;
      this._rowsLayouts = {};
      nextOrder.forEach(key => {
        this._rowsLayouts[key] = new Promise(resolve => {
          this._resolveRowLayout[key] = resolve;
        });
      });
      this.setState({
        animated: false,
        data: nextData,
        containerLayout: null,
        rowsLayouts: null,
        order: nextOrder
      });
    } else if (
      order &&
      !_.isEmpty(nextOrder) &&
      !shallowEqual(order, nextOrder)
    ) {
      this.setState({ order: nextOrder });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { data } = this.state;
    const { data: prevData } = prevState;

    if (data && prevData && !shallowEqual(data, prevData)) {
      this._onUpdateLayouts();
    }
  }

  getUniqueRowKey(key) {
    return `${key}${this.uniqueRowKeyId}`;
  }

  scrollBy({ dx = 0, dy = 0, animated = false }) {
    if (this.props.horizontal) {
      this._contentOffset.x += dx;
    } else {
      this._contentOffset.y += dy;
    }

    this._scroll(animated);
  }

  scrollTo({ x = 0, y = 0, animated = false }) {
    if (this.props.horizontal) {
      this._contentOffset.x = x;
    } else {
      this._contentOffset.y = y;
    }

    this._scroll(animated);
  }

  scrollToRowKey({ key, animated = false }) {
    const { order, containerLayout, rowsLayouts } = this.state;

    let keyX = 0;
    let keyY = 0;

    _.each(order, (row, rowKey) => {
      if (rowKey === key) {
        return false;
      }

      keyX += rowsLayouts[rowKey].width;
      keyY += rowsLayouts[rowKey].height;
      return true;
    });

    // Scroll if the row is not visible.
    if (
      this.props.horizontal
        ? keyX < this._contentOffset.x ||
          keyX > this._contentOffset.x + containerLayout.width
        : keyY < this._contentOffset.y ||
          keyY > this._contentOffset.y + containerLayout.height
    ) {
      if (this.props.horizontal) {
        this._contentOffset.x = keyX;
      } else {
        this._contentOffset.y = keyY;
      }

      this._scroll(animated);
    }
  }

  _renderRows() {
    const {
      horizontal,
      rowActivationTime,
      rowMarginVertial,
      sortingEnabled,
      renderRow,
      fullRow
    } = this.props;
    const {
      animated,
      order,
      data,
      activeRowKey,
      releasedRowKey,
      rowsLayouts
    } = this.state;

    let nextX = 0;
    let nextY = 0;

    return order.map((key, index) => {
      const style = { [ZINDEX]: 0 };
      const location = { x: 0, y: 0 };

      if (rowsLayouts) {
        if (horizontal) {
          location.x = nextX;
          nextX += rowsLayouts[key] ? rowsLayouts[key].width : 0;
        } else {
          location.y = nextY;
          nextY += rowsLayouts[key] ? rowsLayouts[key].height : 0;
        }
      }

      const active = activeRowKey === key;
      const released = releasedRowKey === key;
      const rowCssBoxModel = {
        marginTop: rowMarginVertial,
        marginBottom: rowMarginVertial
      };

      if (active || released) {
        style[ZINDEX] = 100;
      }

      return (
        <Row
          key={this.getUniqueRowKey(key)}
          ref={this._onRefRow.bind(this, key)}
          horizontal={horizontal}
          activationTime={rowActivationTime}
          animated={animated && !active}
          active={active}
          disabled={!sortingEnabled}
          fullRow={fullRow}
          style={style}
          location={location}
          onLayout={!rowsLayouts ? e => this._onLayoutRow(key, e) : null}
          onActivate={(e, gestureState, loc) =>
            this._onActivateRow(key, index, e, gestureState, loc)
          }
          onPress={() => this._onPressRow(key)}
          onRelease={() => this._onReleaseRow(key)}
          onMove={this._onMoveRow}
          manuallyActivateRows={this.props.manuallyActivateRows}
          rowColor={this.props.rowColor}
          rowCssBoxModel={rowCssBoxModel}
        >
          {renderRow({
            key,
            data: data[key],
            disabled: !sortingEnabled,
            active,
            index
          })}
        </Row>
      );
    });
  }

  _renderHeader() {
    if (!this.props.renderHeader || this.props.horizontal) {
      return null;
    }

    const { headerLayout } = this.state;

    return (
      <View onLayout={!headerLayout ? this._onLayoutHeader : null}>
        {this.props.renderHeader()}
      </View>
    );
  }

  _renderFooter() {
    if (!this.props.renderFooter || this.props.horizontal) {
      return null;
    }

    const { footerLayout } = this.state;

    return (
      <View onLayout={!footerLayout ? this._onLayoutFooter : null}>
        {this.props.renderFooter()}
      </View>
    );
  }

  _onUpdateLayouts() {
    const { rowMarginVertial } = this.props;
    Promise.all([
      this._headerLayout,
      this._footerLayout,
      ...Object.values(this._rowsLayouts)
    ]).then(([headerLayout, footerLayout, ...rowsLayouts]) => {
      this._container.measure((x, y, width, height, pageX, pageY) => {
        const rowsLayoutsByKey = {};
        let contentHeight = 0;
        let contentWidth = 0;

        rowsLayouts.forEach(({ rowKey, layout }) => {
          rowsLayoutsByKey[rowKey] = layout;
          contentHeight += layout.height + 2 * rowMarginVertial;
          contentWidth += layout.width;
        });

        this.setState(
          {
            containerLayout: { x, y, width, height, pageX, pageY },
            rowsLayouts: rowsLayoutsByKey,
            headerLayout,
            footerLayout,
            contentHeight,
            contentWidth
          },
          () => {
            this.setState({ animated: true });
          }
        );
      });
    });
  }

  _scroll(animated) {
    this._scrollView.scrollTo({ ...this._contentOffset, animated });
  }

  /**
   * Finds a row under the moving row, if they are neighbours,
   * swaps them, else shifts rows.
   */
  _setOrderOnMove() {
    const { activeRowKey, activeRowIndex, order } = this.state;

    if (activeRowKey === null || this._autoScrollInterval) {
      return;
    }

    const {
      rowKey: rowUnderActiveKey,
      rowIndex: rowUnderActiveIndex
    } = this._findRowUnderActiveRow();

    if (this._movingDirectionChanged) {
      this._prevSwapedRowKey = null;
    }

    // Swap rows if necessary.
    if (
      rowUnderActiveKey !== activeRowKey &&
      rowUnderActiveKey !== this._prevSwapedRowKey
    ) {
      const isNeighbours = Math.abs(rowUnderActiveIndex - activeRowIndex) === 1;
      let nextOrder;

      // If they are neighbours, swap elements, else shift.
      if (isNeighbours) {
        this._prevSwapedRowKey = rowUnderActiveKey;
        nextOrder = swapArrayElements(
          order,
          activeRowIndex,
          rowUnderActiveIndex
        );
      } else {
        nextOrder = order.slice();
        nextOrder.splice(activeRowIndex, 1);
        nextOrder.splice(rowUnderActiveIndex, 0, activeRowKey);
      }

      this.setState(
        {
          order: nextOrder,
          activeRowIndex: rowUnderActiveIndex
        },
        () => {
          if (this.props.onChangeOrder) {
            this.props.onChangeOrder(nextOrder);
          }
        }
      );
    }
  }

  /**
   * Finds a row, which was covered with the moving row’s half.
   */
  _findRowUnderActiveRow() {
    const { horizontal } = this.props;
    const { rowsLayouts, activeRowKey, activeRowIndex, order } = this.state;
    const movingRowLayout = rowsLayouts[activeRowKey];
    const rowLeftX = this._activeRowLocation.x;
    const rowRightX = rowLeftX + movingRowLayout.width;
    const rowTopY = this._activeRowLocation.y;
    const rowBottomY = rowTopY + movingRowLayout.height;

    for (
      let currentRowIndex = 0, x = 0, y = 0, rowsCount = order.length;
      currentRowIndex < rowsCount - 1;
      currentRowIndex += 1
    ) {
      const currentRowKey = order[currentRowIndex];
      const currentRowLayout = rowsLayouts[currentRowKey];
      const nextRowIndex = currentRowIndex + 1;
      const nextRowLayout = rowsLayouts[order[nextRowIndex]];

      x += currentRowLayout.width;
      y += currentRowLayout.height;

      if (
        currentRowKey !== activeRowKey &&
        (horizontal
          ? (x - currentRowLayout.width <= rowLeftX || currentRowIndex === 0) &&
            rowLeftX <= x - currentRowLayout.width / 3
          : (y - currentRowLayout.height <= rowTopY || currentRowIndex === 0) &&
            rowTopY <= y - currentRowLayout.height / 3)
      ) {
        return {
          rowKey: order[currentRowIndex],
          rowIndex: currentRowIndex
        };
      }

      if (
        horizontal
          ? x + nextRowLayout.width / 3 <= rowRightX &&
            (rowRightX <= x + nextRowLayout.width ||
              nextRowIndex === rowsCount - 1)
          : y + nextRowLayout.height / 3 <= rowBottomY &&
            (rowBottomY <= y + nextRowLayout.height ||
              nextRowIndex === rowsCount - 1)
      ) {
        return {
          rowKey: order[nextRowIndex],
          rowIndex: nextRowIndex
        };
      }
    }

    return { rowKey: activeRowKey, rowIndex: activeRowIndex };
  }

  _scrollOnMove(e) {
    const { pageX, pageY } = e.nativeEvent;
    const { horizontal, autoscrollEnable } = this.props;
    const { containerLayout } = this.state;
    let inAutoScrollBeginArea = false;
    let inAutoScrollEndArea = false;

    if (horizontal) {
      inAutoScrollBeginArea =
        pageX < containerLayout.pageX + this.props.autoscrollAreaSize;
      inAutoScrollEndArea =
        pageX >
        containerLayout.pageX +
          this.containerWidth -
          this.props.autoscrollAreaSize;
    } else {
      inAutoScrollBeginArea =
        pageY < containerLayout.pageY + this.props.autoscrollAreaSize;
      inAutoScrollEndArea =
        pageY >
        containerLayout.pageY +
          this.containerHeight -
          this.props.autoscrollAreaSize;
    }

    if (
      !inAutoScrollBeginArea &&
      !inAutoScrollEndArea &&
      this._autoScrollInterval !== null
    ) {
      this._stopAutoScroll();
    }

    // If the auto scoll is processing, return it prevent create non-addressed Interval
    if (this._autoScrollInterval) {
      return;
    }
    if (autoscrollEnable) {
      if (inAutoScrollBeginArea) {
        this._startAutoScroll({
          direction: -1,
          shouldScroll: () => this._contentOffset[horizontal ? "x" : "y"] > 0,
          getScrollStep: stepIndex => {
            const nextStep = stepIndex > 3 ? 60 : 30;
            const contentOffset = this._contentOffset[horizontal ? "x" : "y"];

            return contentOffset - nextStep < 0 ? contentOffset : nextStep;
          }
        });
      } else if (inAutoScrollEndArea) {
        this._startAutoScroll({
          direction: 1,
          shouldScroll: () => {
            const {
              contentHeight,
              contentWidth,
              footerLayout = { height: 0 }
            } = this.state;

            if (horizontal) {
              return (
                this._contentOffset.x < contentWidth - containerLayout.width
              );
            }
            return (
              this._contentOffset.y <
              contentHeight + footerLayout.height - containerLayout.height
            );
          },
          getScrollStep: stepIndex => {
            const nextStep = stepIndex > 3 ? 60 : 30;
            const {
              contentHeight,
              contentWidth,
              footerLayout = { height: 0 }
            } = this.state;

            if (horizontal) {
              return this._contentOffset.x + nextStep >
                contentWidth - containerLayout.width
                ? contentWidth - containerLayout.width - this._contentOffset.x
                : nextStep;
            }
            const scrollHeight =
              contentHeight + footerLayout.height - containerLayout.height;

            return this._contentOffset.y + nextStep > scrollHeight
              ? scrollHeight - this._contentOffset.y
              : nextStep;
          }
        });
      }
    }
  }

  _startAutoScroll({ direction, shouldScroll, getScrollStep }) {
    if (!shouldScroll()) {
      return;
    }

    const { activeRowKey } = this.state;
    const { horizontal } = this.props;
    let counter = 0;
    this._autoScrollInterval = setInterval(() => {
      if (shouldScroll()) {
        const movement = {
          [horizontal ? "dx" : "dy"]: direction * getScrollStep(counter)
        };
        counter += 1;

        this.scrollBy(movement);
        this._rows[activeRowKey].moveBy(movement);
      } else {
        this._stopAutoScroll();
      }
    }, AUTOSCROLL_INTERVAL);
  }

  _stopAutoScroll() {
    clearInterval(this._autoScrollInterval);
    this._autoScrollInterval = undefined; // Prevent scrolling auto when second time
  }

  _onLayoutRow(
    rowKey,
    {
      nativeEvent: { layout }
    }
  ) {
    this._resolveRowLayout[rowKey]({ rowKey, layout });
  }

  _onLayoutHeader({ nativeEvent: { layout } }) {
    this._resolveHeaderLayout(layout);
  }

  _onLayoutFooter({ nativeEvent: { layout } }) {
    this._resolveFooterLayout(layout);
  }

  _onActivateRow(rowKey, index, e, gestureState, location) {
    this._activeRowLocation = location;

    this.setState({
      activeRowKey: rowKey,
      activeRowIndex: index,
      releasedRowKey: null,
      scrollEnabled: false
    });

    if (this.props.onActivateRow) {
      this.props.onActivateRow(rowKey);
    }
  }

  _onPressRow(rowKey) {
    if (this.props.onPressRow) {
      this.props.onPressRow(rowKey);
    }
  }

  _onReleaseRow(rowKey) {
    const { order, activeRowIndex } = this.state;
    let orderArray = order;

    if (this._prevReleaseArray) {
      const startIndex = this._prevReleaseArray.findIndex(
        ele => ele === rowKey
      );
      const endIndex = activeRowIndex;
      // Re-Create a order without the target
      const newOrder = order
        .map((ele, i) => i)
        .filter(ele => ele !== startIndex);
      // Insert the target
      newOrder.splice(endIndex, 0, startIndex);
      orderArray = newOrder;
    }

    this._prevReleaseArray = order.slice();

    this._stopAutoScroll();
    this.setState(({ activeRowKey }) => ({
      activeRowKey: null,
      activeRowIndex: null,
      releasedRowKey: activeRowKey,
      scrollEnabled: this.props.scrollEnabled
    }));

    if (this.props.onReleaseRow) {
      this.props.onReleaseRow(orderArray, rowKey);
    }
  }

  _onMoveRow(e, gestureState, location) {
    const prevMovingRowX = this._activeRowLocation.x;
    const prevMovingRowY = this._activeRowLocation.y;
    const prevMovingDirection = this._movingDirection;

    this._activeRowLocation = location;
    this._movingDirection = this.props.horizontal
      ? prevMovingRowX < this._activeRowLocation.x
      : prevMovingRowY < this._activeRowLocation.y;

    this._movingDirectionChanged =
      prevMovingDirection !== this._movingDirection;
    this._setOrderOnMove();

    if (this.props.scrollEnabled) {
      this._scrollOnMove(e);
    }
  }

  _onScroll({ nativeEvent: { contentOffset } }) {
    this._contentOffset = contentOffset;
  }

  _onRefContainer(component) {
    this._container = component;
  }

  _onRefScrollView(component) {
    this._scrollView = component;
  }

  _onRefRow(rowKey, component) {
    this._rows[rowKey] = component;
  }

  render() {
    const {
      contentContainerStyle,
      horizontal,
      style,
      showsVerticalScrollIndicator,
      showsHorizontalScrollIndicator
    } = this.props;

    let { innerContainerStyle } = this.props;
    const { animated, contentHeight, contentWidth, scrollEnabled } = this.state;
    const containerStyle = StyleSheet.flatten([
      style,
      { opacity: Number(animated) }
    ]);
    innerContainerStyle = [
      styles.rowsContainer,
      horizontal ? { width: contentWidth } : { height: contentHeight },
      innerContainerStyle
    ];
    let { refreshControl } = this.props;

    if (refreshControl && refreshControl.type === RefreshControl) {
      refreshControl = React.cloneElement(this.props.refreshControl, {
        enabled: scrollEnabled
      });
    }

    return (
      <View
        style={containerStyle}
        ref={this._onRefContainer}
        onLayout={event => {
          this.containerHeight = event.nativeEvent.layout.height;
          this.containerWidth = event.nativeEvent.layout.width;
        }}
      >
        <ScrollView
          refreshControl={refreshControl}
          ref={this._onRefScrollView}
          horizontal={horizontal}
          contentContainerStyle={contentContainerStyle}
          scrollEventThrottle={2}
          scrollEnabled={scrollEnabled}
          showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
          showsVerticalScrollIndicator={showsVerticalScrollIndicator}
          onScroll={this._onScroll}
        >
          {this._renderHeader()}
          <View style={innerContainerStyle}>{this._renderRows()}</View>
          {this._renderFooter()}
        </ScrollView>
      </View>
    );
  }
}

SortableList.propTypes = {
  data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  renderRow: PropTypes.func.isRequired,
  rowColor: PropTypes.shape({
    backgroundColor: PropTypes.string
  }),
  rowMarginVertial: PropTypes.number,

  order: PropTypes.arrayOf(PropTypes.string),
  style: ViewPropTypes.style,
  contentContainerStyle: ViewPropTypes.style,
  innerContainerStyle: ViewPropTypes.style,
  sortingEnabled: PropTypes.bool,
  scrollEnabled: PropTypes.bool,
  horizontal: PropTypes.bool,
  autoscrollEnable: PropTypes.bool,
  showsVerticalScrollIndicator: PropTypes.bool,
  showsHorizontalScrollIndicator: PropTypes.bool,
  refreshControl: PropTypes.element,
  autoscrollAreaSize: PropTypes.number,
  rowActivationTime: PropTypes.number,
  manuallyActivateRows: PropTypes.bool,
  fullRow: PropTypes.bool,

  renderHeader: PropTypes.func,
  renderFooter: PropTypes.func,

  onChangeOrder: PropTypes.func,
  onActivateRow: PropTypes.func,
  onReleaseRow: PropTypes.func,
  onPressRow: PropTypes.func
};

SortableList.defaultProps = {
  sortingEnabled: true,
  scrollEnabled: true,
  autoscrollAreaSize: 60,
  autoscrollEnable: true,
  manuallyActivateRows: false,
  showsVerticalScrollIndicator: true,
  showsHorizontalScrollIndicator: true,
  order: [],
  style: {},
  contentContainerStyle: {},
  innerContainerStyle: {},
  horizontal: false,
  refreshControl: null,
  rowActivationTime: 200,
  renderHeader: () => {},
  renderFooter: () => {},
  onChangeOrder: () => {},
  onActivateRow: () => {},
  onReleaseRow: () => {},
  onPressRow: () => {},
  rowColor: {},
  rowMarginVertial: theme.alignmentXS,
  fullRow: false
};
