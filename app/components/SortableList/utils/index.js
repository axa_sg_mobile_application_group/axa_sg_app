export const shallowEqual = (objA, objB) => {
  if (objA === objB) {
    return true;
  }

  const keysA = Object.keys(objA);
  const keysB = Object.keys(objB);

  if (keysA.length !== keysB.length) {
    return false;
  }

  // A's keys different from B.
  const hasOwn = Object.prototype.hasOwnProperty;
  for (let i = 0; i < keysA.length; i += 1) {
    if (!hasOwn.call(objB, keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) {
      return false;
    }
  }

  return true;
};

export const swapArrayElements = (array, indexA, indexB) => {
  if (indexA === indexB) {
    return array;
  }

  if (indexA > indexB) {
    [indexA, indexB] = [indexB, indexA];
  }

  return [
    ...array.slice(0, indexA),
    array[indexB],
    ...array.slice(indexA + 1, indexB),
    array[indexA],
    ...array.slice(indexB + 1)
  ];
};
