import React, { Component } from "react";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import { View } from "react-native";
import PropTypes from "prop-types";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import EABi18n from "../../utilities/EABi18n";
import * as DIALOG_TYPES from "../../components/EABDialog/constants";
import { ContextConsumer } from "../../context";
import EABDialog from "../EABDialog";
import EABButton from "../EABButton";
import EABTextField from "../EABTextField";
import styles from "./style";
import TranslatedText from "../../containers/TranslatedText";

const { CLIENT_FORM, FNA } = REDUCER_TYPES;

/**
 * EASEEmailDialog
 * @description The Dialog on input the Email, used in FNA Report Email.
 * */
export default class EASEEmailDialog extends Component {
  constructor() {
    super();

    this.state = {};
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      email,
      emailOnChange,
      saveClientEmail,
      openEmailDialog,
      textStore,
      updateSkipEmailInReport
    } = this.props;

    return (
      <ContextConsumer>
        {({
          isShowingFNAEmailDialog,
          hideEmailDialog,
          showEmailContentDialog,
          language
        }) => (
          <EABDialog isOpen={isShowingFNAEmailDialog} type={DIALOG_TYPES.FORM}>
            <View style={styles.dialogHeader}>
              <View style={styles.dialogButtonWrapper}>
                <EABButton
                  onPress={() => {
                    hideEmailDialog();
                    openEmailDialog(() => {
                      showEmailContentDialog(FNA);
                    });
                  }}
                >
                  <TranslatedText
                    style={styles.headerButton}
                    path="button.skip"
                  />
                </EABButton>
                <TranslatedText
                  style={styles.headerMiddleText}
                  path="button.email"
                />
                <EABButton
                  onPress={() => {
                    updateSkipEmailInReport(true);
                    saveClientEmail(() => {
                      hideEmailDialog();
                      openEmailDialog(() => {
                        showEmailContentDialog(FNA);
                      });
                    });
                  }}
                >
                  <TranslatedText
                    style={styles.headerButton}
                    path="button.save"
                  />
                </EABButton>
              </View>
            </View>
            <View>
              <EABTextField
                value={email || ""}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "emailDialog.email"
                })}
                onChange={emailOnChange}
                isRequired
                keyboardType="email-address"
              />
            </View>
          </EABDialog>
        )}
      </ContextConsumer>
    );
  }
}

EASEEmailDialog.propTypes = {
  email: REDUCER_TYPE_CHECK[CLIENT_FORM].email.isRequired,
  emailOnChange: PropTypes.func.isRequired,
  saveClientEmail: PropTypes.func.isRequired,
  openEmailDialog: PropTypes.func.isRequired,
  updateSkipEmailInReport: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};
