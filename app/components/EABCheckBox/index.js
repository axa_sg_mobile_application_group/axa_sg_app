import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import * as _ from "lodash";
import Theme from "../../theme";
import { ContextConsumer } from "../../context";
import getStyleArray from "../../utilities/getStyleArray";
import TranslatedText from "../../containers/TranslatedText";
import styles from "./styles";

/**
 * <EABCheckBox />
 * @description check box component.
 * @param {array} options - array of options's object.
 * @param {string} options[].key - check box key
 * @param {string} options[].isSelected - indicates whether the card is
 *   selected or not
 * @param {string|element=} options[].text - checkbox title
 * @param {string|element=} options[].label - checkbox label area, if you just
 *   pass a string, it support to show it.
 * @param {boolean} [isVerticalStyle=true] - indicates whether the check box
 *   layout
 * @param {boolean} [isRequired=false] - indicates whether the text field is
 *   required or not
 * @param {boolean=} isError - indicates whether the text field is error or not
 * @param {function} onPress - the function that will be fire by the check box
 *   onPress event. it will return the option's object to be the function
 *   parameter.
 * @param {boolean} [disable=false] - whether the component is disable or not
 * @param {string} [buttonColor=brand] - check box button color
 * @param {object|number=} style - the checkBox container custom style
 * @param {object|number=} checkBoxStyle - the checkBox custom style
 * @param {object|number=} labelStyle - the checkBox label style
 * @param {object|number=} htmlContentStyle - the checkBox text custom style
 * */
class EABCheckBox extends Component {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get checkBoxStyleArray() {
    const { checkBoxStyle } = this.props;
    return getStyleArray(checkBoxStyle);
  }

  get alignStyle() {
    const { isVerticalStyle } = this.props;
    const { checkBoxContainer, checkBoxContainerVertical } = styles;
    return isVerticalStyle ? checkBoxContainerVertical : checkBoxContainer;
  }

  get optionAlignStyle() {
    const { isVerticalStyle } = this.props;
    const { optionContainer, optionContainerVertical } = styles;
    return isVerticalStyle ? optionContainerVertical : optionContainer;
  }

  get rootStyles() {
    return [this.alignStyle].concat(this.styleArray);
  }

  get optionStyles() {
    return [this.optionAlignStyle].concat(this.checkBoxStyleArray);
  }

  render() {
    /**
     * variables
     * */
    const { options } = this.props;
    /**
     * functions
     * */
    const displayHintText = option => {
      switch (true) {
        case this.props.isRequired && !option.isSelected:
          return (
            <ContextConsumer>
              {({ language }) => (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.302"
                />
              )}
            </ContextConsumer>
          );
        case this.props.isError &&
          !_.includes(option.key, "LIFESTYLE02b_1") &&
          !_.includes(option.key, "LIFESTYLE02c_1"):
          return null;
        default:
          return null;
      }
    };

    const renderMainRow = ({ text, isSelected, disable: optionDisable }) => {
      const {
        buttonColor,
        disable: globalDisable,
        htmlContent,
        labelStyle
      } = this.props;
      const disable = optionDisable || globalDisable;
      const mainRowViewStateStyle = isSelected
        ? {
            backgroundColor: disable ? Theme.coolGrey : buttonColor
          }
        : {
            borderWidth: 2,
            borderColor: disable ? Theme.coolGrey : buttonColor
          };
      let mainRowViewStyles = [mainRowViewStateStyle, styles.checkBoxButton];
      const iconStyle = isSelected
        ? styles.checkBoxButtonTickSelected
        : styles.checkBoxButtonTick;

      let title = null;

      if (htmlContent) {
        title = <View style={styles.htmlContentView}>{htmlContent}</View>;
      } else if (text) {
        title = (
          <Text
            style={[
              disable ? styles.dimText : styles.text,
              getStyleArray(labelStyle)
            ]}
          >
            {text}
          </Text>
        );
      } else {
        /* if only checkbox, add margin to easier to press */
        mainRowViewStyles = [
          ...mainRowViewStyles,
          {
            margin: Theme.alignmentXS
          }
        ];
      }

      return (
        <View style={styles.mainRow}>
          <View style={mainRowViewStyles}>
            <View style={iconStyle} />
          </View>
          {title}
        </View>
      );
    };

    const renderOptionLabel = ({ label }) => {
      switch (true) {
        case typeof label === "string":
          return (
            <View style={styles.label}>
              <Text style={Theme.subheadSecondary}>{label}</Text>
            </View>
          );
        case !label:
          return null;
        case typeof label === "object":
          return <View style={styles.label}>{label}</View>;
        default:
          throw new Error(`unexpected type of checkbox label "${label}"`);
      }
    };

    const renderOption = option => {
      const { onPress, disable: globalDisable, testID } = this.props;
      const { key, disable: optionDisable } = option;
      const disable = optionDisable || globalDisable;
      return (
        <View key={key} style={this.optionStyles}>
          <TouchableOpacity
            testID={`${testID}__cb${option.testID}`}
            activeOpacity={disable ? 1 : 0.5}
            onPress={() => {
              if (!this.props.disable) {
                onPress(option);
              }
            }}
          >
            {renderMainRow(option)}
          </TouchableOpacity>
          {renderOptionLabel(option)}
          {displayHintText(option)}
        </View>
      );
    };

    return <View style={this.rootStyles}>{options.map(renderOption)}</View>;
  }
}

EABCheckBox.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      isSelected: PropTypes.bool.isRequired
    })
  ).isRequired,
  isRequired: PropTypes.bool,
  isVerticalStyle: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  buttonColor: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  checkBoxStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  labelStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  disable: PropTypes.bool,
  htmlContent: PropTypes.string,
  testID: PropTypes.string,
  isError: PropTypes.bool
};

EABCheckBox.defaultProps = {
  isVerticalStyle: true,
  isRequired: false,
  buttonColor: Theme.brand,
  style: {},
  checkBoxStyle: {},
  labelStyle: {},
  disable: PropTypes.false,
  htmlContent: "",
  testID: "",
  isError: false
};

export default EABCheckBox;
