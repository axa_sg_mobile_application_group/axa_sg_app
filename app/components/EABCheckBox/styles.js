import { StyleSheet } from "react-native";
import Theme from "../../theme";

const checkBoxContainer = {
  flexDirection: "row",
  flexWrap: "wrap",
  marginLeft: -88,
  width: "100%"
};

const optionContainer = {
  paddingVertical: 13,
  marginLeft: 88
};

const checkBoxButtonTick = {
  width: 12,
  height: 6,
  transform: [{ translateY: -1 }, { rotate: "-45deg" }]
};

const text = {
  ...Theme.bodyPrimary,
  marginLeft: Theme.alignmentL,
  marginVertical: 1,
  maxWidth: Theme.textMaxWidth
};

const labelText = Theme.bodySecondary;

export default StyleSheet.create({
  checkBoxContainer,
  checkBoxContainerVertical: {
    ...checkBoxContainer,
    flexDirection: "column",
    marginLeft: 0
  },
  optionContainer,
  optionContainerVertical: {
    ...optionContainer,
    marginLeft: 0,
    width: "100%"
  },
  mainRow: {
    flexDirection: "row"
  },
  checkBoxButton: {
    alignItems: "center",
    justifyContent: "center",
    margin: 3,
    borderRadius: Theme.radiusXS,
    width: 18,
    height: 18
  },
  checkBoxButtonTick,
  checkBoxButtonTickSelected: {
    ...checkBoxButtonTick,
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    borderColor: Theme.white
  },
  requiredHint: {
    ...labelText,
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  text,
  dimText: {
    ...text,
    color: Theme.coolGrey
  },
  label: {
    paddingLeft: Theme.alignmentXL + Theme.alignmentL,
    marginTop: 2,
    marginBottom: 1,
    maxWidth: Theme.textMaxWidth
  },
  htmlContentView: {
    flex: 1,
    paddingLeft: Theme.alignmentL
  },
  htmlFontSize: {
    fontSize: Theme.fontSizeXXM
  }
});
