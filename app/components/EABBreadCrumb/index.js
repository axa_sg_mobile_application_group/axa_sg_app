import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import icArrowActive from "../../assets/images/icArrowActive.png";
import icArrowInactive from "../../assets/images/icArrowInactive.png";
import styles from "./styles";

/**
 * <EABBreadCrumb />
 * @description breadcrumb component.
 * @requires icArrowActive - @/assets/images/icArrowActive.png
 * @requires icArrowInactive - @/assets/images/icArrowInactive.png
 * @param {array} steps - array of step's object
 * @param {string} steps[].key - this step key
 * @param {string} steps[].title - this step name
 * @param {function} steps[].onPress - the function that will be fire by the step onPress
 *   event
 * @param {number} currentStepIndex - the index of the current step in steps's array
 * @param {function} currentStepOnPress - the function that will be fire by the current step
 *   onPress event
 * @param {number} [activeOpacity=.5] - the opacity value when the button is pressed
 * */
class EABBreadCrumb extends Component {
  render() {
    /**
     * functions
     * */
    const renderIcon = index => {
      if (index === 0) return null;
      const { currentStepIndex } = this.props;
      const imageSource = currentStepIndex ? icArrowActive : icArrowInactive;
      return <Image style={styles.image} source={imageSource} />;
    };

    const renderButton = ({ title, onPress }, index) => {
      const {
        currentStepIndex,
        currentStepOnPress,
        activeOpacity
      } = this.props;
      const isIndexLessOrEqualToCurrentIndex = index <= currentStepIndex;
      const btnActiveOpacity = isIndexLessOrEqualToCurrentIndex
        ? activeOpacity
        : 1;
      let btnOnPress = null;
      if (isIndexLessOrEqualToCurrentIndex) {
        btnOnPress = index === currentStepIndex ? currentStepOnPress : onPress;
      }
      const btnTextStyle = isIndexLessOrEqualToCurrentIndex
        ? styles.titlePrevious
        : styles.title;
      return (
        <TouchableOpacity onPress={btnOnPress} activeOpacity={btnActiveOpacity}>
          <Text style={btnTextStyle}>{title}</Text>
        </TouchableOpacity>
      );
    };

    const renderSteps = () => {
      const { steps } = this.props;
      const { stepContainerFirst, stepContainer } = styles;
      return steps.map((step, index) => {
        const { key } = step;
        const containerStyle = index === 0 ? stepContainerFirst : stepContainer;
        return (
          <View key={key} style={containerStyle}>
            {renderIcon(index)}
            {renderButton(step, index)}
          </View>
        );
      });
    };
    return <View style={styles.container}>{renderSteps()}</View>;
  }
}

EABBreadCrumb.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      onPress: PropTypes.func.isRequired
    })
  ).isRequired,
  activeOpacity: PropTypes.number,
  currentStepIndex: PropTypes.number.isRequired,
  currentStepOnPress: PropTypes.func.isRequired
};

EABBreadCrumb.defaultProps = {
  activeOpacity: 0.5
};

export default EABBreadCrumb;
