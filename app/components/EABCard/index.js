import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View } from "react-native";
import getStyleArray from "../../utilities/getStyleArray";

import styles from "./styles";

/**
 * <EABCard />
 * @description card component. The selected effects will have different depending on whether
 *   it is plain text or not.
 * @param {boolean} [isPlainText=false] - indicates whether the card is plain text or not
 * @param {boolean} [isSelected=false] - indicates whether the card is selected or not
 * @param {object|number=} style - the card container custom style
 * @param {object|number=} contentStyle - the card content container custom style
 * */
class EABCard extends PureComponent {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get contentStyleArray() {
    const { contentStyle } = this.props;
    return getStyleArray(contentStyle);
  }

  get cardStyle() {
    const { isSelected, isPlainText } = this.props;
    const { container, containerSelected, containerPlainTextSelected } = styles;
    if (isSelected) {
      return isPlainText ? containerPlainTextSelected : containerSelected;
    }
    return container;
  }

  get rootStyles() {
    return [this.cardStyle].concat(this.styleArray);
  }

  get containerStyles() {
    const { cardEdge } = styles;
    return [cardEdge].concat(this.contentStyleArray);
  }

  render() {
    const { children } = this.props;
    return (
      <View style={this.rootStyles}>
        <View style={this.containerStyles}>{children}</View>
      </View>
    );
  }
}

EABCard.propTypes = {
  isSelected: PropTypes.bool,
  isPlainText: PropTypes.bool,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  contentStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  children: PropTypes.node.isRequired
};

EABCard.defaultProps = {
  isPlainText: false,
  isSelected: false,
  style: {},
  contentStyle: {}
};

export default EABCard;
