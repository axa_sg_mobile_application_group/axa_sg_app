import { StyleSheet } from "react-native";
import Theme from "../../theme";

const container = {
  backgroundColor: Theme.white,
  shadowColor: Theme.lightGrey,
  shadowOffset: {
    width: 0,
    height: 0
  },
  shadowRadius: Theme.radiusXS,
  shadowOpacity: 1,
  borderRadius: Theme.radiusXS
};

export default StyleSheet.create({
  container,
  containerSelected: {
    ...container,
    borderWidth: 2,
    borderColor: Theme.brand
  },
  containerPlainTextSelected: {
    ...container,
    borderWidth: 2,
    borderColor: Theme.brand
  },
  cardEdge: {
    overflow: "hidden"
  }
});
