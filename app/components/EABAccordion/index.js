import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image, Text, TouchableWithoutFeedback, View } from "react-native";
import icAddCircleOutline from "../../assets/images/icAddCircleOutline.png";
import icRemoveCircleOutline from "../../assets/images/icRemoveCircleOutline.png";

import styles from "./styles";

/**
 * <EABAccordion />
 * @description accordion component. in auto collapse mode, the accordion do not support
 *   default expand.
 * @requires icAddCircleOutline - @/assets/images/icAddCircleOutline.png
 * @requires icRemoveCircleOutline - @/assets/images/icRemoveCircleOutline.png
 * @param {array} sections - array of accordion object
 * @param {string} sections[].key - question key
 * @param {string} sections[].question - question text
 * @param {string} sections[].answer - answer text
 * @param {boolean} sections[].isExpand - indicates whether the accordion is default expand or
 *   not
 * @param {boolean} [isAutoCollapse=true] - indicates whether the accordion is in auto
 *   collapse mode or not
 * */
class EABAccordion extends PureComponent {
  constructor() {
    super();

    this.state = {
      isExpand: null
    };
  }

  static getDerivedStateFromProps(props) {
    const { sections } = props;
    const isExpand = sections.reduce(
      (prev, section, index) => ({
        ...prev,
        [index]: section.isExpand
      }),
      {}
    );
    return { isExpand };
  }

  render() {
    /**
     * variables
     * */
    const { sections } = this.props;

    /**
     * functions
     * */
    const handlePress = index => {
      const { isAutoCollapse } = this.props;
      const { isExpand } = this.state;
      this.setState({
        isExpand: {
          ...(isAutoCollapse ? {} : isExpand),
          [index]: !isExpand[index]
        }
      });
    };

    const renderImage = index => {
      const { isExpand } = this.state;
      return (
        <Image
          style={styles.button}
          source={isExpand[index] ? icRemoveCircleOutline : icAddCircleOutline}
        />
      );
    };

    const renderAnswerTextView = (index, value) => {
      const { isExpand } = this.state;
      const { answer, answerExpand } = styles;
      const textStyle = isExpand[index] ? [answer, answerExpand] : answer;
      return <Text style={textStyle}>{value}</Text>;
    };

    const renderSection = sectionLength => (
      { key, question, answer },
      index
    ) => {
      const { section, sectionLast } = styles;
      const sectionStyle =
        index === sectionLength - 1 ? [section, sectionLast] : section;
      return (
        <TouchableWithoutFeedback key={key} onPress={() => handlePress(index)}>
          <View style={sectionStyle}>
            <View style={styles.mainRow}>
              {renderImage(index)}
              <Text style={styles.question}>{question}</Text>
            </View>
            {renderAnswerTextView(index, answer)}
          </View>
        </TouchableWithoutFeedback>
      );
    };
    return (
      <View style={styles.container}>
        {sections.map(renderSection(sections.length))}
      </View>
    );
  }
}

EABAccordion.propTypes = {
  sections: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      question: PropTypes.string.isRequired,
      answer: PropTypes.string.isRequired,
      isExpand: PropTypes.bool.isRequired
    })
  ).isRequired,
  isAutoCollapse: PropTypes.bool
};

EABAccordion.defaultProps = {
  isAutoCollapse: true
};

export default EABAccordion;
