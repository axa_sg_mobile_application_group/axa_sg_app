import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    paddingVertical: Theme.alignmentL,
    paddingHorizontal: Theme.alignmentXL,
    width: "100%"
  },
  section: {
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1,
    paddingVertical: Theme.alignmentM,
    width: "100%"
  },
  sectionLast: {
    borderBottomWidth: 0
  },
  mainRow: {
    flexDirection: "row",
    alignItems: "center"
  },
  button: {
    marginRight: Theme.alignmentL,
    width: 24,
    height: 24
  },
  question: {
    ...Theme.bodyPrimary,
    marginVertical: 1,
    maxWidth: Theme.textMaxWidth
  },
  answer: {
    ...Theme.bodySecondary,
    display: "none",
    marginLeft: Theme.alignmentL + Theme.alignmentXL,
    marginTop: Theme.alignmentXS,
    maxWidth: Theme.textMaxWidth
  },
  answerExpand: {
    display: "flex"
  }
});
