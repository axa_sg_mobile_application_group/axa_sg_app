import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Text, TouchableHighlight, View } from "react-native";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABSegmentedControl />
 * @description segmented control component. activated segment do not fire
 *   onPress function
 * @param {array} segments - array of segment object
 * @param {string} segment[].key - segment key
 * @param {string} segment[].title - segment title
 * @param {*} segment[].source - segment badge source, please provide imported
 *   image, not path
 * @param {boolean} segment[].isShowingBadge - indicates whether showing the
 *   badge or not
 * @param {function} segment[].onPress - the function that will be fire by the
 *   segment on press event
 * @param {string} activatedKey - activated segment key
 * @param {object|number=} style - the segmented control container custom style
 * @example see ./code.sample.md
 * */
export default class EABSegmentedControl extends Component {
  render() {
    /**
     * variables
     * */
    const { container } = styles;
    /**
     * functions
     * */
    const renderBadge = segment => {
      const { isShowingBadge, source } = segment;
      if (!isShowingBadge) {
        return null;
      }
      const { badge } = styles;
      return <Image style={badge} source={source} />;
    };

    const renderSegmentContent = (segment, isActive) => {
      const { title } = segment;
      const { titleWrapper, titleSelected, title: titleStyle } = styles;
      const titleTextStyle = isActive ? titleSelected : titleStyle;
      return (
        <View style={titleWrapper}>
          <Text style={titleTextStyle}>{title}</Text>
          {renderBadge(segment)}
        </View>
      );
    };

    const renderInactiveSegment = (segment, index) => {
      const { testID } = this.props;
      const { key, onPress } = segment;
      const { segment: segmentStyle } = styles;
      const borderLeftWidth = index === 0 ? 0 : 1;
      const btnStyle = [segmentStyle, { borderLeftWidth }];
      return (
        <TouchableHighlight
          testID={`${testID}__btn${segment.title}`}
          key={key}
          underlayColor={Theme.brandTransparent}
          onPress={() => onPress(key)}
        >
          <View style={btnStyle}>{renderSegmentContent(segment, false)}</View>
        </TouchableHighlight>
      );
    };

    const renderActiveSegment = (segment, index) => {
      const { key } = segment;
      const borderLeftWidth = index === 0 ? 0 : 1;
      const { segmentSelected } = styles;
      const style = [segmentSelected, { borderLeftWidth }];
      return (
        <View key={key} style={style}>
          {renderSegmentContent(segment, true)}
        </View>
      );
    };

    const renderSegment = (segment, index) => {
      const { activatedKey } = this.props;
      const { key } = segment;
      const isActive = key === activatedKey;
      const renderSegmentItemFunc = isActive
        ? renderActiveSegment
        : renderInactiveSegment;
      return renderSegmentItemFunc(segment, index);
    };

    const renderSegments = () => {
      const { segments } = this.props;
      return segments.map(renderSegment);
    };

    return (
      <View style={[container, getStyleArray(this.props.style)]}>
        {renderSegments()}
      </View>
    );
  }
}

EABSegmentedControl.propTypes = {
  segments: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      source: PropTypes.any,
      isShowingBadge: PropTypes.bool.isRequired,
      onPress: PropTypes.func.isRequired
    })
  ).isRequired,
  activatedKey: PropTypes.string.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

EABSegmentedControl.defaultProps = {
  style: {},
  testID: ""
};
