import { StyleSheet } from "react-native";
import Theme from "../../theme";

const segment = {
  justifyContent: "center",
  alignItems: "center",
  minWidth: 215,
  height: 28,
  borderColor: Theme.brand
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: Theme.brand,
    borderRadius: Theme.radiusS
  },
  segment,
  segmentSelected: {
    ...segment,
    backgroundColor: Theme.brand
  },
  titleWrapper: {
    flexDirection: "row",
    alignItems: "center"
  },
  title: Theme.segmentedControlTabLabelUnselected,
  titleSelected: Theme.segmentedControlTabLabelSelected,
  badge: {
    width: 24,
    height: 24
  }
});
