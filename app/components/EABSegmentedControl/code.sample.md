### SegmentedControl sample code snippets

```Javascript
<SegmentedControl
  segments={[
    {
      key: 'A',
      title: 'Label',
      source: icError, //import icError from 'assets/images/icons/icError.png'
      isShowingBadge: false,
      onPress: () => {}
    },
    {
      key: 'B',
      title: 'Label',
      source: icError, //import icError from 'assets/images/icons/icError.png'
      isShowingBadge: true,
      onPress: () => {}
    }
  ]}
  activatedKey="A"
/>
```