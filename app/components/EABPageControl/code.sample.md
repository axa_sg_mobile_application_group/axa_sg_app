### PageControl sample code snippets

```javascript
<PageControl
  pages={[
    {
      key: 'A',
      component: (
        <View
          style={{
            width: 500,
            height: 400,
            backgroundColor: 'yellow',
          }}
        >
          <Text>
            AAA
          </Text>
        </View>
      ),
    },
    {
      key: 'B',
      component: (
        <View
          style={{
            width: 300,
            height: 500,
            backgroundColor: 'yellow',
          }}
        >
          <Text>
            BBB
          </Text>
        </View>
      ),
    },
    {
      key: 'C',
      component: (
        <View
          style={{
            width: 100,
            height: 100,
            backgroundColor: 'yellow',
          }}
        >
          <Text>
            CCC
          </Text>
        </View>
      ),
    },
  ]}
/>
```