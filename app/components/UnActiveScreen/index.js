import React from "react";
import { Image, View } from "react-native";
import axaLogo from "../../assets/images/axaLogo.png";
import styles from "./styles";

export default function UnActiveScreen() {
  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={axaLogo} />
    </View>
  );
}
