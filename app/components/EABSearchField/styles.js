import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: Theme.alignmentL,
    borderRadius: Theme.radiusXS,
    backgroundColor: Theme.white,
    width: "100%",
    height: 40,
    shadowColor: Theme.lightGrey,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 2,
    shadowOpacity: 1
  },
  title: {
    ...Theme.bodyPrimary,
    flex: 1
  },
  textInputLeft: {
    ...Theme.bodySecondary,
    flex: 1
  },
  textInputRight: {
    ...Theme.bodySecondary,
    flex: 1,
    textAlign: "right"
  }
});
