import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, TextInput, View } from "react-native";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABSearchField />
 * @description text input component. base on
 *   {@link https://facebook.github.io/react-native/docs/textinput.html}
 * @param {string} value - the input field value
 * @param {string} [title=""] - the title text, only show if title is not empty
 * @param {function} onChange - value on change call back. return a onchange
 * @param {string} placeholder - input field label
 * @param {string} [keyboardType='default'] - determines which keyboard to open. check
 *   https://facebook.github.io/react-native/docs/textinput.html#keyboardtype to get more
 *   information
 * @param {boolean} [editable=true] - if false, text is not editable
 * @param {boolean} isAutoFocus - focus the input field when launch page
 * @param {object|number=} style - text field container style.
 * */
class EABSearchField extends Component {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get rootStyles() {
    const { container } = styles;
    return [container].concat(this.styleArray);
  }

  render() {
    const {
      editable,
      value,
      title,
      keyboardType,
      isAutoFocus,
      onChange,
      placeholder
    } = this.props;

    return (
      <View style={this.rootStyles}>
        {title.length > 0 ? <Text style={styles.title}>{title}</Text> : null}
        <TextInput
          value={value}
          style={
            title.length > 0 ? styles.textInputRight : styles.textInputLeft
          }
          editable={editable}
          autoFocus={isAutoFocus}
          onFocus={() => {
            this.props.onFocus(this.props.value);
          }}
          onBlur={() => {
            this.props.onBlur(this.props.value);
          }}
          onChangeText={onChange}
          placeholder={placeholder}
          keyboardType={keyboardType}
          placeholderTextColor={Theme.mediumGrey}
        />
      </View>
    );
  }
}

EABSearchField.propTypes = {
  editable: PropTypes.bool,
  value: PropTypes.string.isRequired,
  title: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  keyboardType: PropTypes.string,
  isAutoFocus: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ])
};

EABSearchField.defaultProps = {
  editable: true,
  keyboardType: "default",
  isAutoFocus: false,
  title: "",
  onFocus: () => {},
  onBlur: () => {},
  style: {}
};

export default EABSearchField;
