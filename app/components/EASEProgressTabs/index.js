import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, TouchableHighlight, View, Image } from "react-native";
import Theme from "../../theme";
import styles from "./style";
import completeImage from "../../assets/images/icCompletedBrand.png";

/**
 * <EASEProgressTabs />
 * @description this component is for EASE only. it had replace the <EABBreadCrumb />.
 * @param {array} steps - array of step's object
 * @param {string} steps[].key - this step key
 * @param {string} steps[].title - this step name
 * @param {boolean} steps[].canPress - indicates whether the tab can press or not
 * @param {function} steps[].onPress - the function that will be fire by the step tab onPress
 *   event
 * @param {function} steps[].isCompleted - the function that will be fire by the step tab isCompleted
 * @param {number} currentStepIndex - the index of the current step in steps's array
 * @param {function} [currentStepOnPress=()=>{}] - the function that will be fire by the current
 *   step tab onPress event. The step.onPress as the fired function parameter.
 * @param {number} [activeOpacity=.5] - the opacity value when the button is pressed
 * */
export default class EASEProgressTabs extends Component {
  render() {
    /**
     * variables
     * */
    const { steps, currentStepIndex, currentStepOnPress, testID } = this.props;

    /**
     * render functions
     * */
    const getColor = ({ step, index }) =>
      step.canPress || index === currentStepIndex ? Theme.brand : Theme.grey;

    const getOnPressColor = ({ step, index }) =>
      index === currentStepIndex || step.canPress
        ? Theme.brandTransparent
        : null;

    const getStepStyle = index => {
      const returnArray = [
        index === 0 ? styles.stepContainerFirst : styles.stepContainer
      ];

      returnArray.push(
        index === currentStepIndex
          ? {
              borderBottomWidth: 2,
              borderBottomColor: Theme.accent
            }
          : {
              paddingBottom: 1
            }
      );

      return returnArray;
    };

    const bindOnPressFunction = ({ step, index }) => {
      const stepOnPress = step.canPress ? step.onPress : () => {};

      return index === currentStepIndex
        ? () => {
            currentStepOnPress(stepOnPress);
          }
        : stepOnPress;
    };

    const renderStepNumber = ({ step, index }) => {
      const showCompleted = step.isCompleted;
      if (showCompleted) {
        return (
          <View style={styles.completeImage}>
            <Image source={completeImage} />
          </View>
        );
      }
      return (
        <View
          style={[
            styles.numberWrapper,
            {
              backgroundColor: getColor({
                step,
                index
              })
            }
          ]}
        >
          <Text
            style={[
              Theme.stepperLabelUnselected,
              {
                color: Theme.white
              }
            ]}
          >
            {index + 1}
          </Text>
        </View>
      );
    };
    return (
      <View style={styles.container}>
        {steps.map((step, index) => (
          <TouchableHighlight
            testID={`${testID}__btn${step.testID}`}
            key={step.key}
            underlayColor={getOnPressColor({
              step,
              index
            })}
            style={styles.stepTouchWrapper}
            onPress={bindOnPressFunction({
              index,
              step
            })}
          >
            <View style={getStepStyle(index)}>
              {renderStepNumber({
                step,
                index
              })}
              <Text
                style={[
                  Theme.tabBarLabelUnselected,
                  {
                    color: getColor({
                      step,
                      index
                    })
                  }
                ]}
              >
                {step.title}
              </Text>
            </View>
          </TouchableHighlight>
        ))}
      </View>
    );
  }
}

EASEProgressTabs.propTypes = {
  testID: PropTypes.string,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      canPress: PropTypes.bool.isRequired,
      onPress: PropTypes.func.isRequired,
      isCompleted: PropTypes.bool
    })
  ).isRequired,
  currentStepIndex: PropTypes.number.isRequired,
  currentStepOnPress: PropTypes.func
};

EASEProgressTabs.defaultProps = {
  testID: "",
  currentStepOnPress() {}
};
