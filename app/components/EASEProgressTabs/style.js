import { StyleSheet } from "react-native";
import Theme from "../../theme";

const stepContainer = {
  flexDirection: "row",
  alignItems: "center",
  paddingHorizontal: Theme.alignmentXL,
  borderColor: Theme.lightGrey,
  borderTopWidth: 1,
  borderBottomWidth: 1,
  borderLeftWidth: 1,
  height: 40
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    width: "100%"
  },
  stepTouchWrapper: {
    flex: 1
  },
  stepContainer,
  stepContainerFirst: {
    ...stepContainer,
    borderLeftWidth: 0
  },
  numberWrapper: {
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
    marginRight: Theme.alignmentXS + 2,
    borderRadius: Theme.radiusL,
    borderWidth: 1,
    width: 20,
    height: 20
  },
  completeImage: {
    margin: 2,
    marginRight: Theme.alignmentXS + 2
  }
});
