import { StyleSheet } from "react-native";
import Colors from "../../theme/colors";
import Theme from "../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginRight: Theme.alignmentXL
  },
  buttonFirst: {
    marginRight: Theme.alignmentXL
  },
  logoWrapper: {
    alignItems: "flex-start",
    marginBottom: Theme.alignmentXL
  },
  dialogHeader: {
    flexDirection: "column",
    borderBottomColor: Colors.coolGrey,
    borderBottomWidth: 1,
    paddingTop: 16,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 16
  },
  dialogContentWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 15
  },
  headerButton: {
    ...Theme.textButtonLabelNormalAccent
  },
  headerMiddleText: {
    fontSize: 17
  },
  segmentWrapper: {
    flexDirection: "row"
  },
  emptySpace: {
    flex: 1
  },
  emailWrapper: {
    flexDirection: "column"
  },
  dialogBottomContentWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  bottomContent: {
    width: 690
  },
  selectText: {
    marginTop: 18,
    fontSize: 17
  },
  toText: {
    padding: 24,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.12)",
    fontSize: 17
  },
  emailContentWrapper: {
    padding: 24,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.12)",
    fontSize: 17
  },
  productName: {
    fontSize: 20,
    paddingTop: 36,
    paddingBottom: 12
  }
});
