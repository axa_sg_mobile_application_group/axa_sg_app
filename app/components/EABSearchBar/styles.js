import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    borderRadius: 10,
    backgroundColor: "rgba(142,142,147,.12)",
    width: "100%",
    height: 36
  },
  icon: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXS,
    width: 14,
    height: 14
  },
  textInput: {
    ...Theme.bodyPrimary,
    flex: 1
  }
});
