import PropTypes from "prop-types";
import React, { Component } from "react";
import moment from "moment";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  findNodeHandle
} from "react-native";
import EABCard from "../EABCard";
import PopoverPicker from "../PopoverPicker";
import PopoverDatePicker from "../../containers/PopoverDatePicker";
import PopoverFlatList from "../PopoverFlatList";
import Theme from "../../theme";
import icDropdown from "../../assets/images/icDropdown.png";
import getStyleArray from "../../utilities/getStyleArray";
import { PICKER, DATE_PICKER, FLAT_LIST } from "./constants";

import styles from "./styles";
import EABCheckBox from "../EABCheckBox";

/**
 *
    <Selector />
 *
 * @description ios popover datepicker component
 * @requires icDropdown - @/assets/images/icDropdown.png
 * @requires selectorType - the type of selector enum('PICKER', 'DATE_PICKER','FLAT_LIST')
 * @param {object} [style] - the style of the component
 * @param {object} [displayTextStyle] - the style of the display text
 * @param {object} [popoverOptions={}] - props of Popover's object
 *
 * @param {boolean} [disable=false] - whether the component is disable or not
 *
 * @param {boolean} [hasCard=true] - whether it's shown Card View or not
 * @param {string} [selectorTitle=''] - selector title when it's without card
 *
 * @param {boolean} [hasCheckbox=false] - whether it's shown checkbox or not
 * @param {boolean} [checkBoxValue=false] - checkbox checked or not
 * @param {function} [checkBoxOnPress] - callback after check the checkbox
 *
 * @param {boolean} [hasLabel=false] - whether it's shown label or not
 * @param {string} [label=''] - label text
 *
 * @param {object} [pickerOptions={items:[], onValueChange: () => {}, selectedValue: ''}]
 * - props of Picker's object
 * @param {object} [flatListOptions={data:[], renderItemOnPress: () => {}, selectedValue: '', extraData: '', keyExtractor: () => {}}]
 * - props of Picker's object
 * @param {object} [datePickerIOSOptions={date: new Date(), onDateChange: () => {}, mode: ''}]
 * - props of DatePickerIOS's object
 * @param {boolean} hasError - only work on hasCard = true
 * @param {string} hintText - only work on hasCard = true
 * */
export default class Selector extends Component {
  constructor(props) {
    super(props);
    this.popoverTarget = React.createRef();

    this.renderWithCard = this.renderWithCard.bind(this);
    this.onFieldPress = this.onFieldPress.bind(this);
    this.onShow = this.onShow.bind(this);
    this.onHide = this.onHide.bind(this);
    this.onFieldPress = this.onFieldPress.bind(this);
    this.showPopover = this.showPopover.bind(this);
    this.showPopoverPicker = this.showPopoverPicker.bind(this);
    this.showPopoverDatePicker = this.showPopoverDatePicker.bind(this);
    this.showPopoverFlatListPicker = this.showPopoverFlatListPicker.bind(this);
    this.renderDisplayText = this.renderDisplayText.bind(this);
    this.renderWithoutCard = this.renderWithoutCard.bind(this);
    this.state = {
      popoverVisible: false
    };
  }

  onShow() {
    if (
      this.props.popoverOptions != null &&
      this.props.popoverOptions.onShow != null
    ) {
      this.props.popoverOptions.onShow();
    }
  }

  onHide() {
    if (
      this.props.popoverOptions != null &&
      this.props.popoverOptions.onHide != null
    ) {
      this.props.popoverOptions.onHide();
    }
    this.setState({
      popoverVisible: false,
      targetNode: 0
    });
  }

  onFieldPress() {
    this.setState({
      popoverVisible: true,
      targetNode: findNodeHandle(this.popoverTarget.current)
    });
  }

  get popoverOptions() {
    return Object.assign({}, this.props.popoverOptions, {
      sourceView: this.state.targetNode,
      onShow: this.onShow.bind(this),
      onHide: this.onHide.bind(this)
    });
  }

  get fieldHeight() {
    let fieldHeight = 40;
    if (this.props.hasCheckbox) {
      fieldHeight = 48;
    } else if (this.props.hasLabel) {
      fieldHeight = 48;
    }
    return fieldHeight;
  }

  get fieldPaddingVertical() {
    let fieldPaddingVertical = 9;
    if (this.props.hasCheckbox) {
      fieldPaddingVertical = 12;
    } else if (this.props.hasLabel) {
      fieldPaddingVertical = 13;
    }
    return fieldPaddingVertical;
  }

  get pickerOptions() {
    const pickerOptions = Object.assign({}, this.props.pickerOptions);

    if (this.props.pickerOptions.selectedValue === "") {
      pickerOptions.items.unshift({ label: "", value: "" });
    }

    pickerOptions.onValueChange = itemValue => {
      if (this.props.pickerOptions && this.props.pickerOptions.onValueChange) {
        this.props.pickerOptions.onValueChange(itemValue);
      }
    };
    return pickerOptions;
  }

  get datePickerIOSOptions() {
    const datePickerIOSOptions = Object.assign(
      {},
      this.props.datePickerIOSOptions
    );

    datePickerIOSOptions.onDateChange = chosenDate => {
      if (
        this.props.datePickerIOSOptions &&
        this.props.datePickerIOSOptions.onDateChange
      ) {
        this.props.datePickerIOSOptions.onDateChange(new Date(chosenDate));
      }
    };
    return datePickerIOSOptions;
  }

  get displayTextStyles() {
    const { displayTextStyle } = this.props;

    return [Theme.subheadSecondary, ...getStyleArray(displayTextStyle)];
  }

  get cardStyle() {
    const { fieldHeight, fieldPaddingVertical } = this;
    const { hasError } = this.props;

    return {
      contentStyle: [
        styles.cardContent,
        {
          width: "100%",
          height: "100%",
          paddingVertical: fieldPaddingVertical,
          paddingLeft: Theme.alignmentL
        }
      ],
      style: {
        height: fieldHeight,
        ...(hasError
          ? {
              borderColor: Theme.negative,
              borderWidth: 1
            }
          : {})
      }
    };
  }

  get hintTextStyle() {
    const { hasError } = this.props;

    if (hasError) {
      return Theme.fieldTextOrHelperTextNegative;
    }
    return Theme.fieldTextOrHelperTextBrand;
  }

  showPopover() {
    switch (this.props.selectorType) {
      case PICKER:
        return (
          this.state.popoverVisible &&
          this.showPopoverPicker(this.popoverOptions)
        );
      case DATE_PICKER:
        return (
          this.state.popoverVisible &&
          this.showPopoverDatePicker(this.popoverOptions)
        );
      case FLAT_LIST:
        return (
          this.state.popoverVisible &&
          this.showPopoverFlatListPicker(this.popoverOptions)
        );
      default:
        throw new Error(
          "The selectorType must come from the constant 'POPOVER_TYPES'"
        );
    }
  }

  showPopoverPicker(popoverOptions) {
    return (
      <PopoverPicker
        popoverOptions={popoverOptions}
        pickerOptions={this.pickerOptions}
      />
    );
  }

  showPopoverDatePicker(popoverOptions) {
    return (
      <PopoverDatePicker
        popoverOptions={popoverOptions}
        datePickerIOSOptions={this.datePickerIOSOptions}
      />
    );
  }

  showPopoverFlatListPicker(popoverOptions) {
    const onRenderItemOnPress = (itemKey, item) => {
      if (this.props.flatListOptions) {
        if (this.props.flatListOptions.renderItemOnPress) {
          this.onHide();

          setTimeout(() => {
            this.props.flatListOptions.renderItemOnPress(item.label, item);
          }, 500); // this (solves) the issue that the alert dialog (if any) to be closed along with onHide()
        }
      }
    };
    if (
      this.props.flatListOptions.data.length >= 2 &&
      this.props.flatListOptions.data.length <= 8
    ) {
      popoverOptions.preferredContentSize = [
        this.props.popoverOptions &&
        this.props.popoverOptions.preferredContentSize
          ? this.props.popoverOptions.preferredContentSize[0]
          : 512,
        50 * this.props.flatListOptions.data.length
      ];
    } else if (this.props.flatListOptions.data.length > 8) {
      popoverOptions.preferredContentSize = [
        this.props.popoverOptions &&
        this.props.popoverOptions.preferredContentSize
          ? this.props.popoverOptions.preferredContentSize[0]
          : 512,
        50 * 8 + Theme.bodyPrimary.lineHeight
      ];
    }

    return (
      <PopoverFlatList
        testID={this.props.testID}
        popoverOptions={popoverOptions}
        flatListOptions={this.props.flatListOptions}
        renderItemOnPress={onRenderItemOnPress}
      />
    );
  }

  renderDisplayText() {
    const {
      selectorType,
      pickerOptions,
      datePickerIOSOptions,
      flatListOptions
    } = this.props;

    switch (selectorType) {
      case PICKER:
        if (pickerOptions && pickerOptions.selectedValue !== "") {
          const selectedItem = pickerOptions.items.find(
            item => pickerOptions.selectedValue === item.value
          );
          if (selectedItem) {
            return selectedItem.label;
          }
        }
        return pickerOptions.selectedValue;
      case DATE_PICKER:
        if (datePickerIOSOptions && datePickerIOSOptions.date) {
          return moment(datePickerIOSOptions.date).format("DD/MM/YYYY");
        }
        return "";
      case FLAT_LIST:
        if (flatListOptions && flatListOptions.selectedValue !== "") {
          const selectedItem = flatListOptions.data.find(
            item =>
              flatListOptions.selectedValue === item.key ||
              flatListOptions.selectedValue === item.value
          );
          if (selectedItem) {
            return selectedItem.label;
          }
        }
        return flatListOptions.selectedValue;
      default:
        throw new Error(
          "The selectorType must come from the constant 'POPOVER_TYPES'"
        );
    }
  }

  renderWithCard() {
    const { cardStyle } = this;
    const { hasCard, testID } = this.props;
    const { contentStyle, style } = cardStyle;
    return (
      <TouchableOpacity
        testID={testID}
        activeOpacity={1}
        onPress={this.onFieldPress}
      >
        <EABCard
          isPlainText
          isSelected={false}
          style={style}
          contentStyle={contentStyle}
        >
          <View style={styles.selectorWrap}>
            {this.props.hasCheckbox && (
              <View
                style={[
                  {
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }
                ]}
              >
                <EABCheckBox
                  style={{ width: "50%" }}
                  checkBoxStyle={[
                    {
                      paddingVertical: 0
                    }
                  ]}
                  options={[
                    {
                      key: this.props.label,
                      text: this.props.label,
                      isSelected: this.props.checkBoxValue
                    }
                  ]}
                  onPress={
                    !this.props.disable ? this.props.checkBoxOnPress : () => {}
                  }
                  disable={this.props.disable}
                />

                {this.props.checkBoxValue && (
                  <View
                    style={{
                      flexDirection: "row",
                      paddingRight: Theme.alignmentM
                    }}
                  >
                    <Text style={this.displayTextStyles}>
                      {this.renderDisplayText()}
                    </Text>
                    {!this.props.disable && (
                      <Image
                        ref={this.popoverTarget}
                        style={styles.dropdownImage}
                        source={icDropdown}
                      />
                    )}
                  </View>
                )}
              </View>
            )}

            {!this.props.hasCheckbox &&
              this.props.hasLabel && (
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    paddingRight: Theme.alignmentM
                  }}
                >
                  <Text style={styles.labelWithCard}>{this.props.label}</Text>
                  <View style={hasCard ? styles.displayTextWithCard : {}}>
                    <Text style={this.displayTextStyles} numberOfLines={1}>
                      {this.renderDisplayText()}
                    </Text>
                  </View>
                  {!this.props.disable && (
                    <Image
                      ref={this.popoverTarget}
                      style={styles.dropdownImage}
                      source={icDropdown}
                    />
                  )}
                </View>
              )}
            {!this.props.hasCheckbox &&
              !this.props.hasLabel && (
                <View
                  style={[
                    {
                      width: "100%",
                      flexDirection: "row",
                      paddingRight: Theme.alignmentM
                    }
                  ]}
                >
                  <Text
                    style={{
                      ...Theme.bodyPrimary,
                      flex: 3
                    }}
                  >
                    {this.renderDisplayText()}
                  </Text>
                  {!this.props.disable && (
                    <Image
                      ref={this.popoverTarget}
                      style={styles.dropdownImage}
                      source={icDropdown}
                    />
                  )}
                </View>
              )}
          </View>
        </EABCard>
      </TouchableOpacity>
    );
  }

  renderWithoutCard() {
    const { testID } = this.props;
    return (
      <TouchableOpacity
        testID={testID}
        activeOpacity={1}
        onPress={this.onFieldPress}
      >
        <View style={styles.selectorWrap}>
          {this.props.selectorTitle !== "" ? (
            <View style={styles.selectorTitleWrapper}>
              <Text style={Theme.subheadPrimary}>{`${
                this.props.selectorTitle
              }:`}</Text>
            </View>
          ) : null}
          <View style={styles.optionWrapper}>
            <Text style={this.displayTextStyles}>
              {this.renderDisplayText()}
            </Text>
            {!this.props.disable && (
              <Image
                ref={this.popoverTarget}
                style={styles.dropdownImage}
                source={icDropdown}
              />
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {
      renderWithCard,
      renderWithoutCard,
      showPopover,
      hintTextStyle
    } = this;
    const { style, hasCard, disable, hintText } = this.props;

    return (
      <View style={[styles.container, ...getStyleArray(style)]}>
        {hasCard ? renderWithCard() : renderWithoutCard()}
        {disable ? null : showPopover()}
        {hintText !== "" ? <Text style={hintTextStyle}>{hintText}</Text> : null}
      </View>
    );
  }
}

Selector.propTypes = {
  popoverOptions: PropTypes.oneOfType([PropTypes.object]),
  hasLabel: PropTypes.bool,
  label: PropTypes.string,
  hasCheckbox: PropTypes.bool,
  hasCard: PropTypes.bool,
  selectorTitle: PropTypes.string,
  selectorType: PropTypes.string,
  pickerOptions: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    )
  ]),
  datePickerIOSOptions: PropTypes.oneOfType([PropTypes.object]),
  flatListOptions: PropTypes.oneOfType([PropTypes.object]),
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  displayTextStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  disable: PropTypes.bool,
  checkBoxValue: PropTypes.bool,
  checkBoxOnPress: PropTypes.func,
  hasError: PropTypes.bool,
  hintText: PropTypes.string,
  testID: PropTypes.string
};
Selector.defaultProps = {
  popoverOptions: {},
  hasLabel: false,
  label: "",
  hasCheckbox: false,
  hasCard: true,
  selectorTitle: "",
  selectorType: DATE_PICKER,
  pickerOptions: {},
  datePickerIOSOptions: {
    date: new Date(),
    onDateChange: () => {}
  },
  flatListOptions: {},
  style: {},
  displayTextStyle: {},
  disable: false,
  checkBoxValue: false,
  checkBoxOnPress: () => {},
  hasError: false,
  hintText: "",
  testID: ""
};
