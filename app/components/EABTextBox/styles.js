import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentL,
    borderRadius: Theme.radiusS,
    borderColor: Theme.lightGrey,
    borderWidth: 1,
    backgroundColor: Theme.white,
    width: "100%"
  },
  textInput: {
    ...Theme.bodyPrimary,
    width: "100%"
  },
  textWrapper: {
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  hitTextWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 2
    // paddingVertical: Theme.alignmentXL
  }
});
