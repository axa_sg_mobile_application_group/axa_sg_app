import PropTypes from "prop-types";
import React, { Component } from "react";
import { Animated, Modal, PanResponder, View } from "react-native";
import styles from "./styles";

/**
 * <EABSideBar />
 * @default side bar component. Have to be aware, this is not a full control component.
 *   you should control it with ref.
 * @param {element|array} content - side bar content.
 * @param {string} content[].key - if content is array, should have a key.
 * @param {element} content[].component - side bar child component
 * @param {function} showCallBack - the function will be fire after show side bar
 * @param {function} hideCallBack - the function will be fire after hide side bar
 * @export showSideBar() - show side bar function
 * @export hideSideBar() - hide side bar function
 * @example see ./code.sample.md
 * */
export default class EABSideBar extends Component {
  constructor() {
    super();

    this.sideBarWidth = 320;
    this.showSideBar = this.showSideBar.bind(this);
    this.showSideBarCallBack = this.showSideBarCallBack.bind(this);
    this.hideSideBar = this.hideSideBar.bind(this);

    /* pan handler */
    this.isHidingSideBar = false;

    this.sideBarResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: ({ nativeEvent }) => {
        if (nativeEvent.pageX > this.sideBarWidth) {
          this.hideSideBar();

          /* Mark the hidden action to not repeat it */
          this.isHidingSideBar = true;
        }
      },
      onPanResponderMove: ({ nativeEvent }, gesture) => {
        if (
          this.state.isOpen &&
          nativeEvent.pageX !== 0 &&
          gesture.vx < -0.15 &&
          !this.isHidingSideBar
        ) {
          this.hideSideBar();
        }
      },
      onPanResponderRelease: () => {
        this.isHidingSideBar = false;
      }
    });

    this.state = {
      isOpen: false,
      animateValue: new Animated.Value(0)
    };
  }

  /**
   * functions
   * */
  showSideBar() {
    this.setState({ isOpen: true });
  }

  showSideBarCallBack() {
    /* show side bar and animate at the same time */
    Animated.timing(this.state.animateValue, {
      toValue: 1,
      duration: 250
    }).start();
    if (this.props.showCallBack) {
      this.props.showCallBack();
    }
  }

  hideSideBar() {
    /* hide side bar after animate */
    Animated.timing(this.state.animateValue, {
      toValue: 0,
      duration: 200
    }).start(({ finished }) => {
      if (finished) {
        this.setState({ isOpen: false });
      }
    });
  }

  render() {
    /**
     * render functions
     * */
    const arrayContent = () =>
      /* children is more than one component? */
      this.props.content.map
        ? this.props.content.map(element =>
            React.cloneElement(element.component, {
              ...element.props,
              key: `${element.key}${Math.random()}`
            })
          )
        : React.cloneElement(this.props.content.component, {
            /* change text color to buttonColor */
            ...this.props.content.props,
            key: this.props.content.key
          });

    return (
      <View style={styles.page}>
        {this.props.children}
        <Modal
          visible={this.state.isOpen}
          transparent
          onShow={this.showSideBarCallBack}
          onDismiss={this.props.hideCallBack}
        >
          <Animated.View
            {...this.sideBarResponder.panHandlers}
            style={[
              styles.container,
              {
                opacity: this.state.animateValue
              }
            ]}
          >
            <Animated.View
              style={[
                styles.sideBar,
                {
                  width: this.sideBarWidth,
                  left: this.state.animateValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-this.sideBarWidth, 0]
                  })
                }
              ]}
            >
              {Array.isArray(this.props.content)
                ? arrayContent()
                : this.props.content}
            </Animated.View>
          </Animated.View>
        </Modal>
      </View>
    );
  }
}

EABSideBar.propTypes = {
  content: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        component: PropTypes.element.isRequired
      })
    )
  ]).isRequired,
  children: PropTypes.node.isRequired,
  showCallBack: PropTypes.func,
  hideCallBack: PropTypes.func
};
EABSideBar.defaultProps = {
  showCallBack: () => {},
  hideCallBack: () => {}
};
