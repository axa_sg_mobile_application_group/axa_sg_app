### SideBar sample code snippets

```Javascript
<SideBar
  ref={ref => {this.sideBar = ref}}
  isOpen={this.state.isOpen}
  content={(
    <View style={{
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%'
    }}>
      <Button
        buttonType={RECTANGLE_BUTTON_1}
        onPress={() => {this.sideBar.hideSideBar()}}
      >
        <Text>hide side bar</Text>
      </Button>
    </View>
  )}
>
  <View style={{
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%'
  }}>
    <Button
      buttonType={RECTANGLE_BUTTON_1}
      onPress={() => {this.sideBar.showSideBar()}}
    >
      <Text>show side bar</Text>
    </Button>
  </View>
</SideBar>
```
