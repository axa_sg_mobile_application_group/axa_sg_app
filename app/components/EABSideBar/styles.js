import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  page: {
    width: "100%",
    height: "100%"
  },
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: Theme.coolGrey
  },
  sideBar: {
    position: "relative",
    height: "100%",
    backgroundColor: Theme.white
  }
});
