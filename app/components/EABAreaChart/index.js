import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View } from "react-native";
import { AreaChart } from "react-native-svg-charts";
import * as shape from "d3-shape";
import getStyleArray from "../../utilities/getStyleArray";

/**
 * <EABAreaChart />
 * @description EABAreaChart component.
 *   {@link https://github.com/JesperLekland/react-native-svg-charts}
 * @param {number|object|array} [style={}] - container custom style
 * @param {array} chart - chart config for each line
 * @param {array} data - data for the chart, array of object / array
 * */
export default class EABAreaChart extends PureComponent {
  render() {
    /**
     * variables
     * */
    const { chart, style, data } = this.props;

    return (
      <View style={[{ ...getStyleArray(style), height: "100%" }]}>
        {chart.map(x => (
          <AreaChart
            style={x.style}
            gridMax={x.gridMax}
            gridMin={x.gridMin}
            data={data}
            yAccessor={x.yAccessor}
            xAccessor={x.xAccessor}
            contentInset={{ top: 30, bottom: 0 }}
            curve={shape.curveNatural}
            svg={x.svg}
          />
        ))}
      </View>
    );
  }
}

EABAreaChart.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  // TODO update object
  chart: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

EABAreaChart.defaultProps = {
  style: {}
};
