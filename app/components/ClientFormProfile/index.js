import {
  REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  NUMBER_FORMAT,
  utilities,
  DEPENDANT
} from "eab-web-api";
import * as _ from "lodash";
import moment from "moment";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import {
  Alert,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import avatar from "../../assets/images/avatar64X64Dp.png";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import EABi18n from "../../utilities/EABi18n";
import { getOptionList } from "../../utilities/getOptionList";
import getOccupationList from "../../utilities/getOccupationList";
import photoPicker from "../../utilities/photoPicker";
import EABButton from "../EABButton";
import EABCheckBox from "../EABCheckBox";
import EABTextField from "../EABTextField";
import SelectorField from "../SelectorField";
import { DATE_PICKER, FLAT_LIST, SEARCH_BOX } from "../SelectorField/constants";
import options from "./options";
import styles from "./styles";

const { CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;
const { MAX_CURRENCY_VALUE } = NUMBER_FORMAT;
const { trigger, numberFormatter } = utilities;
const { optionCondition } = trigger;
const { numberToCurrency, currencyToNumber } = numberFormatter;
/**
 * <ClientFormProfile />
 * @description client form profile part. all props should from reducer
 * @param {function} getProfileData - trigger reload of profile data (and dependant list)
 * @param {string} mainCid - the parent cid when editing a dependant profile
 * */
export default class ClientFormProfile extends PureComponent {
  constructor() {
    super();

    this.state = {};
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    const { isPDA, relationship, relationshipOnChange } = this.props;
    if (isPDA && relationship !== DEPENDANT.SPO) {
      relationshipOnChange(DEPENDANT.SPO);
    }
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate show delete button or not
   * @return {boolean}
   * */
  get shouldShowDeleteButton() {
    const {
      isFamilyMember,
      isCreate,
      isFromProfile,
      haveSignDoc,
      isProposerMissing
    } = this.props;

    return (
      !isCreate &&
      !haveSignDoc &&
      (!isFamilyMember || isFromProfile || !isProposerMissing)
    );
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { shouldShowDeleteButton } = this;
    const {
      isCreate,
      isFamilyMember,
      isProposerMissing,
      isPDA,
      relationship,
      isRelationshipError,
      relationshipOther,
      isRelationshipOtherError,
      givenName,
      isGivenNameError,
      surname,
      otherName,
      hanYuPinYinName,
      name,
      isNameError,
      nameOrder,
      title,
      isTitleError,
      gender,
      isGenderError,
      birthday,
      isBirthdayError,
      nationality,
      isNationalityError,
      singaporePRStatus,
      isSingaporePRStatusError,
      countryOfResidence,
      cityOfResidence,
      isCityOfResidenceError,
      otherCityOfResidence,
      isOtherCityOfResidenceError,
      IDDocumentType,
      isIDDocumentTypeError,
      IDDocumentTypeOther,
      isIDDocumentTypeOtherError,
      ID,
      isIDError,
      smokingStatus,
      isSmokingError,
      maritalStatus,
      isLanguageError,
      languageOther,
      isLanguageOtherError,
      education,
      isEducationError,
      employStatus,
      isEmployStatusError,
      employStatusOther,
      isEmployStatusOtherError,
      industry,
      isIndustryError,
      occupation,
      isOccupationError,
      occupationOther,
      nameOfEmployBusinessSchool,
      countryOfEmployBusinessSchool,
      typeOfPass,
      typeOfPassOther,
      isTypeOfPassError,
      isTypeOfPassOtherError,
      passExpiryDate,
      income,
      optionsMap,
      textStore,
      relationshipOnChange,
      relationshipOtherOnChange,
      givenNameOnChange,
      surnameOnChange,
      otherNameOnChange,
      hanYuPinYinNameOnChange,
      nameOnChange,
      nameOrderOnChange,
      titleOnChange,
      genderOnChange,
      birthdayOnChange,
      nationalityOnChange,
      singaporePRStatusOnChange,
      countryOfResidenceOnChange,
      cityOfResidenceOnChange,
      otherCityOfResidenceOnChange,
      IDDocumentTypeOnChange,
      IDDocumentTypeOtherOnChange,
      IDOnChange,
      smokingStatusOnChange,
      maritalStatusOnChange,
      languageOnChange,
      languageOtherOnChange,
      educationOnChange,
      employStatusOnChange,
      employStatusOtherOnChange,
      industryOnChange,
      occupationOnChange,
      occupationOtherOnChange,
      nameOfEmployBusinessSchoolOnChange,
      countryOfEmployBusinessSchoolOnChange,
      typeOfPassOnChange,
      typeOfPassOtherOnChange,
      passExpiryDateOnChange,
      incomeOnChange,
      photoOnChange,
      photo,
      deleteClient,
      getProfileData,
      mainCid,
      isMaritalStatusError,
      isIncomeError
    } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => (
          <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={40}>
            <ScrollView
              testID="scrollViewClient"
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              <View style={styles.formRow}>
                <EABButton
                  testID="btnAvatar"
                  containerStyle={styles.avatarButtonWrapper}
                  onPress={() => {}}
                >
                  <View style={styles.avatarWrapper}>
                    <TouchableOpacity
                      onPress={() => {
                        photoPicker({ language, textStore, photoOnChange });
                      }}
                    >
                      {!_.isEmpty(photo) ? (
                        <Image style={styles.avatar} source={photo} />
                      ) : (
                        <Image style={styles.avatar} source={avatar} />
                      )}
                    </TouchableOpacity>
                  </View>
                </EABButton>
                {isFamilyMember ? (
                  <View style={styles.wrapper}>
                    <SelectorField
                      style={styles.field}
                      value={relationship}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path: "clientForm.relationshipField"
                      })}
                      editable={!isPDA}
                      isError={isRelationshipError.hasError}
                      hintText={isRelationshipError.message[language]}
                      onChange={relationshipOnChange}
                      selectorType={FLAT_LIST}
                      flatListOptions={{
                        renderItemOnPress: (itemKey, item) => {
                          if (item) {
                            relationshipOnChange(item.value);
                          }
                        },
                        extraData: this.state,
                        keyExtractor: item => item.key,
                        selectedValue: relationship,
                        data: getOptionList({
                          optionMap: optionsMap.relationship,
                          language
                        })
                      }}
                      isRequired
                    />
                    {relationship === "OTH" ? (
                      <EABTextField
                        style={styles.fieldLast}
                        value={relationshipOther}
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path: "clientForm.pleaseSpecifyField"
                        })}
                        isError={isRelationshipOtherError.hasError}
                        hintText={isRelationshipOtherError.message[language]}
                        onChange={relationshipOtherOnChange}
                        isRequired
                      />
                    ) : null}
                  </View>
                ) : null}
              </View>
              <Text style={styles.personalInformationTitle}>
                {EABi18n({
                  language,
                  textStore,
                  path: "clientForm.personalInformationTitle"
                }).toUpperCase()}
              </Text>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtGivenName"
                  style={styles.fieldFirst}
                  value={givenName}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.givenNameField"
                  })}
                  isError={isGivenNameError.hasError}
                  hintText={isGivenNameError.message[language]}
                  onChange={givenNameOnChange}
                  isRequired
                  maxLength={30}
                />
                <EABTextField
                  testID="txtSurname"
                  style={styles.field}
                  value={surname}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.surnameField"
                  })}
                  onChange={surnameOnChange}
                  maxLength={30}
                />
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtOtherName"
                  style={styles.fieldFirst}
                  value={otherName}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.otherNameField"
                  })}
                  onChange={otherNameOnChange}
                  maxLength={30}
                />
                <EABTextField
                  testID="txtHanYuPinYinName"
                  style={styles.field}
                  value={hanYuPinYinName}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.hanYuPinYinNameField"
                  })}
                  onChange={hanYuPinYinNameOnChange}
                  maxLength={30}
                />
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtName"
                  style={styles.fieldFirst}
                  value={name}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.nameField"
                  })}
                  onChange={nameOnChange}
                  isError={isNameError.hasError}
                  hintText={isNameError.message[language]}
                  isRequired
                />
                <SelectorField
                  testID="selectNameOrder"
                  style={styles.field}
                  value={nameOrder}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.nameOrder"
                  })}
                  onChange={nameOrderOnChange}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        nameOrderOnChange(item.value);
                      }
                    },
                    extraData: this.state,
                    keyExtractor: item => item.key,
                    selectedValue: nameOrder,
                    data: getOptionList({
                      optionMap: optionsMap.nameOrder,
                      testID: "selectNameOrder",
                      language
                    })
                  }}
                />
              </View>
              <View style={styles.formRow}>
                <SelectorField
                  testID="selectTitle"
                  style={styles.fieldFirst}
                  value={title}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.titleField"
                  })}
                  isError={isTitleError.hasError}
                  hintText={isTitleError.message[language]}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        titleOnChange(item.value);
                      }
                    },
                    extraData: this.state,
                    keyExtractor: item => item.key,
                    selectedValue: title,
                    data: getOptionList({
                      optionMap: optionsMap.title,
                      testID: "selectTitle",
                      language
                    })
                  }}
                />
                <SelectorField
                  testID="selectGender"
                  style={styles.field}
                  value={gender}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.genderField"
                  })}
                  isError={isGenderError.hasError}
                  hintText={isGenderError.message[language]}
                  isRequired={isCreate || isFamilyMember}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        genderOnChange(item.value);
                      }
                    },
                    extraData: this.state,
                    keyExtractor: item => item.key,
                    selectedValue: gender,
                    data: getOptionList({
                      optionMap: optionsMap.gender,
                      testID: "selectGender",
                      language
                    })
                  }}
                />
              </View>
              <View style={styles.formRow}>
                <SelectorField
                  testID="selectBirthday"
                  style={styles.fieldFirst}
                  value={birthday}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.birthField"
                  })}
                  onChange={birthdayOnChange}
                  selectorType={DATE_PICKER}
                  datePickerIOSOptions={{
                    mode: "date",
                    date:
                      birthday === ""
                        ? new Date(
                            moment()
                              .add(-30, "years")
                              .format("YYYY-MM-DD")
                          )
                        : new Date(birthday),
                    onDateChange: value => {
                      const newValue = value.toISOString();
                      birthdayOnChange(newValue.slice(0, 10));
                    },
                    onDateChangeBirth: birthdayOnChange,
                    maximumDate: new Date(),
                    minimumDate: new Date(
                      moment()
                        .add(-100, "years")
                        .format("YYYY-MM-DD")
                    )
                  }}
                  isRequired={isProposerMissing || isFamilyMember}
                  isError={isBirthdayError.hasError}
                  hintText={isBirthdayError.message[language]}
                />
                <View style={styles.field}>{/* use for alignment */}</View>
              </View>
              <View style={styles.formRowLast}>
                <SelectorField
                  testID="searchBox-Nationality"
                  style={styles.fieldFirst}
                  selectorType={SEARCH_BOX}
                  value={nationality}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.nationalityField"
                  })}
                  options={getOptionList({
                    optionMap: optionsMap.nationality,
                    testID: "nationalityField",
                    language
                  })}
                  onChange={option => {
                    nationalityOnChange(option.value);
                  }}
                  isError={isNationalityError.hasError}
                  hintText={isNationalityError.message[language]}
                />
                {!!nationality &&
                nationality !== "N1" &&
                nationality !== "N2" ? (
                  <SelectorField
                    style={styles.field}
                    value={singaporePRStatus}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.singaporePRStatusField"
                    })}
                    isError={isSingaporePRStatusError.hasError}
                    hintText={isSingaporePRStatusError.message[language]}
                    onChange={singaporePRStatusOnChange}
                    selectorType={FLAT_LIST}
                    flatListOptions={{
                      renderItemOnPress: (itemKey, item) => {
                        if (item) {
                          singaporePRStatusOnChange(item.value);
                        }
                      },
                      extraData: this.state,
                      keyExtractor: item => item.key,
                      selectedValue: singaporePRStatus,
                      data: getOptionList({
                        optionMap: optionsMap.singaporePRStatus,
                        language
                      })
                    }}
                    isRequired
                  />
                ) : (
                  <View style={styles.field}>{/* use for alignment */}</View>
                )}
              </View>
              <View style={styles.formRow}>
                <SelectorField
                  testID="selectCountryOfResidence"
                  style={styles.fieldFirst}
                  selectorType={SEARCH_BOX}
                  value={countryOfResidence}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.countryOfResidenceField"
                  })}
                  options={getOptionList({
                    optionMap: optionsMap.residency,
                    language
                  })}
                  onChange={option => {
                    countryOfResidenceOnChange(option.value);
                  }}
                />
                <View style={styles.field}>{/* use for alignment */}</View>
              </View>
              {optionCondition({
                value: countryOfResidence,
                options: "city",
                optionsMap,
                showIfNoValue: false
              }) ? (
                <View style={styles.formRowLast}>
                  <SelectorField
                    testID="txtCity"
                    style={styles.fieldFirst}
                    value={cityOfResidence}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.cityField"
                    })}
                    isError={isCityOfResidenceError.hasError}
                    hintText={isCityOfResidenceError.message[language]}
                    selectorType={FLAT_LIST}
                    flatListOptions={{
                      renderItemOnPress: (itemKey, item) => {
                        if (item) {
                          cityOfResidenceOnChange(item.value);
                        }
                      },
                      extraData: this.state,
                      keyExtractor: item => item.key,
                      selectedValue: cityOfResidence,
                      data: getOptionList(
                        {
                          optionMap: optionsMap.city,
                          language
                        },
                        countryOfResidence
                      )
                    }}
                  />
                  {/* <EABTextField
                    style={styles.fieldFirst}
                    value={cityOfResidence}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.cityField"
                    })}
                    isError={isCityOfResidenceError.hasError}
                    hintText={isCityOfResidenceError.message[language]}
                    onChange={cityOfResidenceOnChange}
                  /> */}
                  {_.find(
                    _.get(optionsMap, "city.options"),
                    opt =>
                      opt.value === cityOfResidence &&
                      (opt.value === "T110" ||
                        opt.value === "T120" ||
                        opt.value === "T205" ||
                        opt.value === "T207" ||
                        opt.value === "T208" ||
                        opt.value === "T209" ||
                        opt.value === "T224")
                  ) ? (
                    <EABTextField
                      style={styles.field}
                      value={otherCityOfResidence}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path: "clientForm.otherCityOfResidenceField"
                      })}
                      isError={isOtherCityOfResidenceError.hasError}
                      hintText={isOtherCityOfResidenceError.message[language]}
                      onChange={otherCityOfResidenceOnChange}
                      isRequired
                      maxLength={50}
                    />
                  ) : (
                    <View style={styles.field}>{/* use for alignment */}</View>
                  )}
                </View>
              ) : null}
              <View style={styles.formRow}>
                <SelectorField
                  testID="selectDocumentType"
                  style={styles.fieldFirst}
                  value={IDDocumentType}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.IDTypeField"
                  })}
                  onChange={newIDDocType => {
                    IDDocumentTypeOnChange({
                      newIDDocType,
                      nationalityData: nationality,
                      singaporePRStatusData: singaporePRStatus
                    });
                  }}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        IDDocumentTypeOnChange({
                          newIDDocType: item.value,
                          nationalityData: nationality,
                          singaporePRStatusData: singaporePRStatus
                        });
                      }
                    },
                    data: getOptionList({
                      optionMap: optionsMap.IDType,
                      language
                    }),
                    selectedValue: IDDocumentType
                  }}
                  editable={
                    !(singaporePRStatus === "Y" || nationality === "N1")
                  }
                  isError={isIDDocumentTypeError.hasError}
                  hintText={isIDDocumentTypeError.message[language]}
                />
                {IDDocumentType === "other" ? (
                  <EABTextField
                    testID="txtDocumentOtherType"
                    style={styles.field}
                    value={IDDocumentTypeOther}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.IDTypeOtherField"
                    })}
                    isError={isIDDocumentTypeOtherError.hasError}
                    hintText={isIDDocumentTypeOtherError.message[language]}
                    onChange={IDDocumentTypeOtherOnChange}
                    isRequired
                    maxLength={25}
                  />
                ) : (
                  <View style={styles.field}>{/* use for alignment */}</View>
                )}
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtIdNumber"
                  style={styles.fieldFirst}
                  value={ID}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.IDField"
                  })}
                  isError={isIDError.hasError}
                  hintText={isIDError.message[language]}
                  onChange={IDOnChange}
                  maxLength={20}
                />
                <View style={styles.field}>{/* use for alignment */}</View>
              </View>
              <View style={styles.formRow}>
                <SelectorField
                  testID="selectSmokingStatus"
                  style={styles.fieldFirst}
                  value={smokingStatus}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.smokeField"
                  })}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        smokingStatusOnChange(item.value);
                      }
                    },
                    data: getOptionList({
                      optionMap: optionsMap.smokingStatus,
                      language
                    }),
                    selectedValue: smokingStatus
                  }}
                  isError={isSmokingError.hasError}
                  hintText={isSmokingError.message[language]}
                />
                <SelectorField
                  testID="selectMaritalStatus"
                  style={styles.field}
                  value={maritalStatus}
                  editable={relationship !== "SPO"}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.maritalField"
                  })}
                  onChange={newMaritalStatus => {
                    maritalStatusOnChange({
                      newMaritalStatus,
                      relationshipData: relationship
                    });
                  }}
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: (itemKey, item) => {
                      if (item) {
                        maritalStatusOnChange({
                          newMaritalStatus: item.value,
                          relationshipData: relationship
                        });
                      }
                    },
                    data: getOptionList({
                      optionMap: optionsMap.marital,
                      language
                    }),
                    selectedValue: maritalStatus
                  }}
                  isError={isMaritalStatusError.hasError}
                  hintText={isMaritalStatusError.message[language]}
                />
              </View>
              <Text style={styles.personalInformationTitle}>
                {EABi18n({
                  language,
                  textStore,
                  path: "clientForm.languageField"
                }).toUpperCase()}
              </Text>

              {isLanguageError.hasError ? (
                <Text style={styles.fieldTextOrHelperTextNegative}>
                  {isLanguageError.message[language]}
                </Text>
              ) : null}

              <View style={styles.formRow}>
                <EABCheckBox
                  testID="boxLanguage"
                  options={options.language(this.props.language)}
                  onPress={thisOption => {
                    const languageArray =
                      this.props.language !== ""
                        ? this.props.language.split(",")
                        : [];
                    const findIndex = languageArray.findIndex(
                      item => item === thisOption.key
                    );

                    if (findIndex !== -1) {
                      languageArray.splice(findIndex, 1);
                    } else {
                      languageArray.push(thisOption.key);
                    }

                    languageOnChange(languageArray.join());
                  }}
                />
              </View>

              <View style={styles.formRow}>
                {this.props.language.match(/other/) !== null ? (
                  <EABTextField
                    testID="txtLanguageOther"
                    style={styles.field}
                    value={languageOther}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.otherField"
                    })}
                    isError={isLanguageOtherError.hasError}
                    hintText={isLanguageOtherError.message[language]}
                    onChange={languageOtherOnChange}
                    maxLength={30}
                  />
                ) : (
                  <View style={styles.field}>{/* use for alignment */}</View>
                )}
              </View>
              <Text style={styles.personalInformationTitle}>
                {EABi18n({
                  language,
                  textStore,
                  path: "clientForm.educationWorkInformationTitle"
                }).toUpperCase()}
              </Text>
              <View style={styles.formRow}>
                <View style={styles.fieldFirst}>
                  <SelectorField
                    testID="selectEducation"
                    style={styles.field}
                    value={education}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.educationField"
                    })}
                    selectorType={FLAT_LIST}
                    flatListOptions={{
                      renderItemOnPress: (itemKey, item) => {
                        if (item) {
                          educationOnChange(item.value);
                        }
                      },
                      extraData: this.state,
                      keyExtractor: item => item.key,
                      selectedValue: education,
                      data: getOptionList({
                        optionMap: optionsMap.education,
                        language
                      })
                    }}
                    isError={isEducationError.hasError}
                    hintText={isEducationError.message[language]}
                  />
                </View>
                <View style={styles.field}>
                  <SelectorField
                    testID="selectEmploymentStatus"
                    style={styles.field}
                    value={employStatus}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.employmentField"
                    })}
                    isError={isEmployStatusError.hasError}
                    hintText={isEmployStatusError.message[language]}
                    onChange={employStatusOnChange}
                    selectorType={FLAT_LIST}
                    flatListOptions={{
                      renderItemOnPress: (itemKey, item) => {
                        if (item) {
                          employStatusOnChange(item.value);
                        }
                      },
                      extraData: this.state,
                      keyExtractor: item => item.key,
                      selectedValue: employStatus,
                      data: getOptionList({
                        optionMap: optionsMap.employStatus,
                        language
                      })
                    }}
                  />
                  {employStatus === "ot" ? (
                    <EABTextField
                      testID="txtEmploymentStatusOther"
                      style={styles.field}
                      value={employStatusOther}
                      placeholder=" "
                      isError={isEmployStatusOtherError.hasError}
                      hintText={isEmployStatusOtherError.message[language]}
                      onChange={employStatusOtherOnChange}
                    />
                  ) : (
                    <View style={styles.field}>{/* use for alignment */}</View>
                  )}
                </View>
              </View>
              <View style={styles.formRow}>
                <View style={styles.fieldFirst}>
                  <View style={styles.formRow}>
                    <SelectorField
                      testID="selectOccupation"
                      style={styles.field}
                      selectorType={SEARCH_BOX}
                      value={occupation}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path: "clientForm.occupationField"
                      })}
                      options={getOccupationList({
                        optionMap: optionsMap.occupation,
                        language,
                        industry
                      })}
                      onChange={option => {
                        occupationOnChange({
                          newOccupation: option.value,
                          occupationCurrently: occupation
                        });
                      }}
                      isError={isOccupationError.hasError}
                      hintText={isOccupationError.message[language]}
                    />
                    {occupation === "O921" ? (
                      <EABTextField
                        testID="txtOccupationOther"
                        style={styles.occupationOtherField}
                        value={occupationOther}
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path: "clientForm.pleaseSpecifyField"
                        })}
                        onChange={occupationOtherOnChange}
                        maxLength={50}
                      />
                    ) : null}
                  </View>
                </View>
                <View style={styles.field}>
                  <SelectorField
                    testID="selectIndustryField"
                    style={styles.field}
                    selectorType={SEARCH_BOX}
                    value={industry}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.industryField"
                    })}
                    options={getOptionList({
                      optionMap: optionsMap.industry,
                      language
                    })}
                    onChange={option => {
                      industryOnChange(option.value);
                    }}
                    isError={isIndustryError.hasError}
                    hintText={isIndustryError.message[language]}
                  />
                </View>
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtNameOfEmployer"
                  style={styles.fieldFirst}
                  value={
                    nameOfEmployBusinessSchool.length <= 50
                      ? nameOfEmployBusinessSchool
                      : nameOfEmployBusinessSchool.substring(0, 50)
                  }
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.nameOfEmployBusinessSchoolField"
                  })}
                  onChange={newNameOfEmployBusinessSchool => {
                    nameOfEmployBusinessSchoolOnChange({
                      newNameOfEmployBusinessSchool,
                      employStatusData: employStatus,
                      birthdayData: birthday
                    });
                  }}
                  editable={
                    !(
                      employStatus === "hh" ||
                      employStatus === "hw" ||
                      employStatus === "rt" ||
                      employStatus === "ue"
                    )
                  }
                  maxLength={50}
                />
                <View style={styles.field}>
                  <SelectorField
                    testID="selectCountryOfEmployer"
                    style={styles.field}
                    selectorType={SEARCH_BOX}
                    value={countryOfEmployBusinessSchool}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.countryOfEmployBusinessSchoolField"
                    })}
                    options={getOptionList({
                      optionMap: optionsMap.residency,
                      language
                    })}
                    onChange={option => {
                      countryOfEmployBusinessSchoolOnChange({
                        newCountryOfEmployBusinessSchool: option.value,
                        employStatusData: employStatus,
                        birthdayData: birthday
                      });
                    }}
                    editable={
                      !(
                        employStatus === "hh" ||
                        employStatus === "hw" ||
                        employStatus === "rt" ||
                        employStatus === "ue"
                      )
                    }
                  />
                </View>
              </View>
              {nationality &&
              nationality !== "N1" &&
              nationality !== "N2" &&
              singaporePRStatus &&
              singaporePRStatus !== "Y"
                ? [
                    <View key="pass" style={styles.formRow}>
                      <SelectorField
                        testID="selectPassType"
                        style={styles.fieldFirst}
                        value={typeOfPass}
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path: "clientForm.typeOfPassField"
                        })}
                        selectorType={FLAT_LIST}
                        flatListOptions={{
                          renderItemOnPress: (itemKey, item) => {
                            if (item) {
                              typeOfPassOnChange(item.value);
                            }
                          },
                          data: getOptionList({
                            optionMap: optionsMap.pass,
                            language
                          }),
                          selectedValue: typeOfPass
                        }}
                        isError={isTypeOfPassError.hasError}
                        hintText={isTypeOfPassError.message[language]}
                      />
                      {typeOfPass === "o" ? (
                        <EABTextField
                          testID="txtPassField"
                          style={styles.field}
                          value={typeOfPassOther}
                          placeholder={EABi18n({
                            language,
                            textStore,
                            path: "clientForm.otherField"
                          })}
                          isError={isTypeOfPassOtherError.hasError}
                          hintText={isTypeOfPassOtherError.message[language]}
                          onChange={typeOfPassOtherOnChange}
                        />
                      ) : (
                        <View style={styles.field}>
                          {/* use for alignment */}
                        </View>
                      )}
                    </View>,
                    <View key="passDate" style={styles.formRow}>
                      <SelectorField
                        testID="selectPassDate"
                        style={styles.fieldFirst}
                        value={
                          Number.isNaN(passExpiryDate)
                            ? ""
                            : moment(passExpiryDate).format("DD/MM/YYYY")
                        }
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path: "clientForm.passExpiryDateField"
                        })}
                        selectorType={DATE_PICKER}
                        datePickerIOSOptions={{
                          mode: "date",
                          date: Number.isNaN(passExpiryDate)
                            ? new Date()
                            : new Date(
                                moment(passExpiryDate).format("YYYY-MM-DD")
                              ),
                          onDateChange: date => {
                            const timeStampDate = moment(date).valueOf();
                            passExpiryDateOnChange(timeStampDate);
                          },
                          minimumDate: new Date(),
                          maximumDate: new Date(
                            moment()
                              .add(10, "years")
                              .format("YYYY-MM-DD")
                          )
                        }}
                      />
                      <View style={styles.field}>
                        {/* use for alignment */}
                      </View>
                    </View>
                  ]
                : null}
              <View style={styles.formRowLast}>
                <EABTextField
                  testID="txtMonthlyIncome"
                  style={styles.fieldFirst}
                  value={
                    Number.isNaN(income)
                      ? ""
                      : numberToCurrency({
                          value: income,
                          decimal: 0,
                          max: MAX_CURRENCY_VALUE
                        })
                  }
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.incomeField"
                  })}
                  onChange={value => {
                    incomeOnChange(currencyToNumber(value));
                  }}
                  prefix="$"
                  maxLength={13}
                  keyboardType="decimal-pad"
                  isError={isIncomeError.hasError}
                  hintText={isIncomeError.message[language]}
                />
                <View style={styles.field}>{/* use for alignment */}</View>
              </View>
              {shouldShowDeleteButton ? (
                <View style={styles.deleteButtonWrapper}>
                  <ContextConsumer>
                    {({ hideCreateProfileDialog, hideClientNavigator }) => (
                      <EABButton
                        onPress={() => {
                          Alert.alert(
                            "",
                            EABi18n({
                              language,
                              textStore,
                              path: isFamilyMember
                                ? "clientForm.alertUnlink"
                                : "clientForm.alertDelete"
                            }),
                            [
                              {
                                text: "No",
                                onPress: () => {},
                                style: "cancel"
                              },
                              {
                                text: "Yes",
                                onPress: () => {
                                  deleteClient(
                                    isFamilyMember ? "UNLINK" : "DELETE",
                                    () => {
                                      if (!isFamilyMember) {
                                        hideCreateProfileDialog(() => {
                                          hideClientNavigator();
                                        });
                                      } else {
                                        getProfileData(
                                          mainCid,
                                          hideCreateProfileDialog
                                        );
                                        this.props.reloadContactList(() => {});
                                      }
                                    }
                                  );
                                }
                              }
                            ],
                            { cancelable: true }
                          );
                        }}
                      >
                        <Text style={Theme.textButtonLabelNormalAccent}>
                          {EABi18n({
                            language,
                            textStore,
                            path: "clientForm.deleteCustomerButton"
                          })}
                        </Text>
                      </EABButton>
                    )}
                  </ContextConsumer>
                </View>
              ) : null}
            </ScrollView>
          </KeyboardAvoidingView>
        )}
      </ContextConsumer>
    );
  }
}

ClientFormProfile.propTypes = {
  isCreate: REDUCER_TYPE_CHECK[CLIENT_FORM].isCreate.isRequired,
  isFamilyMember: REDUCER_TYPE_CHECK[CLIENT_FORM].isFamilyMember.isRequired,
  isProposerMissing:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isProposerMissing.isRequired,
  isFromProfile: REDUCER_TYPE_CHECK[CLIENT_FORM].isFromProfile.isRequired,
  isPDA: REDUCER_TYPE_CHECK[CLIENT_FORM].isPDA.isRequired,
  relationship: REDUCER_TYPE_CHECK[CLIENT_FORM].relationship.isRequired,
  isRelationshipError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isRelationshipError.isRequired,
  relationshipOther:
    REDUCER_TYPE_CHECK[CLIENT_FORM].relationshipOther.isRequired,
  isRelationshipOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isRelationshipOtherError.isRequired,
  givenName: REDUCER_TYPE_CHECK[CLIENT_FORM].givenName.isRequired,
  isGivenNameError: REDUCER_TYPE_CHECK[CLIENT_FORM].isGivenNameError.isRequired,
  surname: REDUCER_TYPE_CHECK[CLIENT_FORM].surname.isRequired,
  otherName: REDUCER_TYPE_CHECK[CLIENT_FORM].otherName.isRequired,
  hanYuPinYinName: REDUCER_TYPE_CHECK[CLIENT_FORM].hanYuPinYinName.isRequired,
  haveSignDoc: REDUCER_TYPE_CHECK[CLIENT_FORM].haveSignDoc.isRequired,
  name: REDUCER_TYPE_CHECK[CLIENT_FORM].name.isRequired,
  isNameError: REDUCER_TYPE_CHECK[CLIENT_FORM].isNameError.isRequired,
  nameOrder: REDUCER_TYPE_CHECK[CLIENT_FORM].nameOrder.isRequired,
  title: REDUCER_TYPE_CHECK[CLIENT_FORM].title.isRequired,
  isTitleError: REDUCER_TYPE_CHECK[CLIENT_FORM].isTitleError.isRequired,
  gender: REDUCER_TYPE_CHECK[CLIENT_FORM].gender.isRequired,
  isGenderError: REDUCER_TYPE_CHECK[CLIENT_FORM].isGenderError.isRequired,
  birthday: REDUCER_TYPE_CHECK[CLIENT_FORM].birthday.isRequired,
  isBirthdayError: REDUCER_TYPE_CHECK[CLIENT_FORM].isBirthdayError.isRequired,
  nationality: REDUCER_TYPE_CHECK[CLIENT_FORM].nationality.isRequired,
  isNationalityError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isNationalityError.isRequired,
  singaporePRStatus:
    REDUCER_TYPE_CHECK[CLIENT_FORM].singaporePRStatus.isRequired,
  isSingaporePRStatusError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isSingaporePRStatusError.isRequired,
  countryOfResidence:
    REDUCER_TYPE_CHECK[CLIENT_FORM].countryOfResidence.isRequired,
  cityOfResidence: REDUCER_TYPE_CHECK[CLIENT_FORM].cityOfResidence.isRequired,
  isCityOfResidenceError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isCityOfResidenceError.isRequired,
  otherCityOfResidence:
    REDUCER_TYPE_CHECK[CLIENT_FORM].otherCityOfResidence.isRequired,
  isOtherCityOfResidenceError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isOtherCityOfResidenceError.isRequired,
  IDDocumentType: REDUCER_TYPE_CHECK[CLIENT_FORM].IDDocumentType.isRequired,
  isIDDocumentTypeError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isIDDocumentTypeError.isRequired,
  IDDocumentTypeOther:
    REDUCER_TYPE_CHECK[CLIENT_FORM].IDDocumentTypeOther.isRequired,
  isIDDocumentTypeOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isIDDocumentTypeOtherError.isRequired,
  ID: REDUCER_TYPE_CHECK[CLIENT_FORM].ID.isRequired,
  isIDError: REDUCER_TYPE_CHECK[CLIENT_FORM].isIDError.isRequired,
  smokingStatus: REDUCER_TYPE_CHECK[CLIENT_FORM].smokingStatus.isRequired,
  isSmokingError: REDUCER_TYPE_CHECK[CLIENT_FORM].isSmokingError.isRequired,
  maritalStatus: REDUCER_TYPE_CHECK[CLIENT_FORM].maritalStatus.isRequired,
  isMaritalStatusError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isMaritalStatusError.isRequired,
  language: REDUCER_TYPE_CHECK[CLIENT_FORM].language.isRequired,
  isLanguageError: REDUCER_TYPE_CHECK[CLIENT_FORM].isLanguageError.isRequired,
  languageOther: REDUCER_TYPE_CHECK[CLIENT_FORM].languageOther.isRequired,
  isLanguageOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isLanguageOtherError.isRequired,
  education: REDUCER_TYPE_CHECK[CLIENT_FORM].education.isRequired,
  isEducationError: REDUCER_TYPE_CHECK[CLIENT_FORM].isEducationError.isRequired,
  employStatus: REDUCER_TYPE_CHECK[CLIENT_FORM].employStatus.isRequired,
  isEmployStatusError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isEmployStatusError.isRequired,
  employStatusOther:
    REDUCER_TYPE_CHECK[CLIENT_FORM].employStatusOther.isRequired,
  isEmployStatusOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isEmployStatusOtherError.isRequired,
  industry: REDUCER_TYPE_CHECK[CLIENT_FORM].industry.isRequired,
  isIndustryError: REDUCER_TYPE_CHECK[CLIENT_FORM].isIndustryError.isRequired,
  occupation: REDUCER_TYPE_CHECK[CLIENT_FORM].occupation.isRequired,
  isOccupationError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isOccupationError.isRequired,
  occupationOther: REDUCER_TYPE_CHECK[CLIENT_FORM].occupationOther.isRequired,
  nameOfEmployBusinessSchool:
    REDUCER_TYPE_CHECK[CLIENT_FORM].nameOfEmployBusinessSchool.isRequired,
  countryOfEmployBusinessSchool:
    REDUCER_TYPE_CHECK[CLIENT_FORM].countryOfEmployBusinessSchool.isRequired,
  typeOfPass: REDUCER_TYPE_CHECK[CLIENT_FORM].typeOfPass.isRequired,
  typeOfPassOther: REDUCER_TYPE_CHECK[CLIENT_FORM].typeOfPassOther.isRequired,
  isTypeOfPassError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isTypeOfPassError.isRequired,
  isTypeOfPassOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isTypeOfPassOtherError.isRequired,
  passExpiryDate: REDUCER_TYPE_CHECK[CLIENT_FORM].passExpiryDate.isRequired,
  income: REDUCER_TYPE_CHECK[CLIENT_FORM].income.isRequired,
  isIncomeError: REDUCER_TYPE_CHECK[CLIENT_FORM].isIncomeError.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  relationshipOnChange: PropTypes.func.isRequired,
  relationshipOtherOnChange: PropTypes.func.isRequired,
  givenNameOnChange: PropTypes.func.isRequired,
  surnameOnChange: PropTypes.func.isRequired,
  otherNameOnChange: PropTypes.func.isRequired,
  hanYuPinYinNameOnChange: PropTypes.func.isRequired,
  nameOnChange: PropTypes.func.isRequired,
  nameOrderOnChange: PropTypes.func.isRequired,
  titleOnChange: PropTypes.func.isRequired,
  genderOnChange: PropTypes.func.isRequired,
  birthdayOnChange: PropTypes.func.isRequired,
  nationalityOnChange: PropTypes.func.isRequired,
  singaporePRStatusOnChange: PropTypes.func.isRequired,
  countryOfResidenceOnChange: PropTypes.func.isRequired,
  cityOfResidenceOnChange: PropTypes.func.isRequired,
  otherCityOfResidenceOnChange: PropTypes.func.isRequired,
  IDDocumentTypeOnChange: PropTypes.func.isRequired,
  IDOnChange: PropTypes.func.isRequired,
  smokingStatusOnChange: PropTypes.func.isRequired,
  maritalStatusOnChange: PropTypes.func.isRequired,
  languageOnChange: PropTypes.func.isRequired,
  languageOtherOnChange: PropTypes.func.isRequired,
  educationOnChange: PropTypes.func.isRequired,
  employStatusOnChange: PropTypes.func.isRequired,
  employStatusOtherOnChange: PropTypes.func.isRequired,
  industryOnChange: PropTypes.func.isRequired,
  occupationOnChange: PropTypes.func.isRequired,
  occupationOtherOnChange: PropTypes.func.isRequired,
  nameOfEmployBusinessSchoolOnChange: PropTypes.func.isRequired,
  countryOfEmployBusinessSchoolOnChange: PropTypes.func.isRequired,
  typeOfPassOnChange: PropTypes.func.isRequired,
  typeOfPassOtherOnChange: PropTypes.func.isRequired,
  passExpiryDateOnChange: PropTypes.func.isRequired,
  incomeOnChange: PropTypes.func.isRequired,
  photo: REDUCER_TYPE_CHECK[CLIENT_FORM].photo.isRequired,
  photoOnChange: PropTypes.func.isRequired,
  deleteClient: PropTypes.func.isRequired,
  IDDocumentTypeOtherOnChange: PropTypes.func.isRequired,
  getProfileData: PropTypes.func.isRequired,
  mainCid: PropTypes.string,
  reloadContactList: PropTypes.func.isRequired
};

ClientFormProfile.defaultProps = {
  mainCid: ""
};
