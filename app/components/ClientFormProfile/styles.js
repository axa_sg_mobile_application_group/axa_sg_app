import { StyleSheet } from "react-native";
import Theme from "../../theme";

const field = {
  flex: 1
};
const formRow = {
  flexDirection: "row",
  marginBottom: Theme.alignmentXS,
  width: "100%"
};

export default StyleSheet.create({
  scrollView: {
    backgroundColor: Theme.paleGrey,
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "flex-start",
    paddingVertical: Theme.alignmentXL,
    paddingHorizontal: 72,
    paddingBottom: Theme.alignmentXL + 100
  },
  avatarButtonWrapper: {
    ...field,
    marginRight: Theme.alignmentXXXL,
    alignItems: "flex-start"
  },
  avatarWrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: Theme.alignmentXS,
    backgroundColor: Theme.brandTransparent,
    borderRadius: Theme.radiusL,
    width: 64,
    height: 64
  },
  avatar: {
    width: 64,
    height: 64,
    borderRadius: 33
  },
  personalInformationTitle: {
    ...Theme.captionBrand,
    marginVertical: Theme.alignmentL
  },
  formRow,
  formRowLast: {
    ...formRow,
    marginBottom: 0
  },
  field,
  fieldFirst: {
    ...field,
    marginRight: Theme.alignmentXXXL
  },
  fieldLast: {
    ...field,
    marginLeft: Theme.alignmentXL
  },
  wrapper: {
    ...field,
    flexDirection: "row"
  },
  deleteButtonWrapper: {
    alignItems: "flex-end",
    marginTop: Theme.alignmentL,
    width: "100%"
  },
  fieldTextOrHelperTextNegative: {
    ...Theme.fieldTextOrHelperTextNegative
  },
  occupationOtherField: {
    ...field,
    marginLeft: 20
  }
});
