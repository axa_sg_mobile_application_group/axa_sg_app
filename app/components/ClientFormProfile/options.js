export default {
  title: [
    { label: "Dr", value: "Dr", testID: "Dr" },
    { label: "Mr", value: "Mr", testID: "Mr" },
    { label: "Mrs", value: "Mrs", testID: "Mrs" },
    { label: "Ms", value: "Ms", testID: "Ms" },
    { label: "Mdm", value: "Mdm", testID: "Mdm" }
  ],
  gender: [
    { label: "Male", value: "M", testID: "Male" },
    { label: "Female", value: "F", testID: "Female" }
  ],
  IDType: [
    {
      label: "Birth Certificate No.",
      testID: "BirthCertificateNo",
      value: "bcNo"
    },
    {
      label: "FIN No.",
      testID: "FinNo",
      value: "fin"
    },
    {
      label: "NRIC",
      testID: "NRIC",
      value: "nric"
    },
    {
      label: "Passport",
      testID: "Passport",
      value: "passport"
    },
    {
      label: "Others",
      testID: "Others",
      isOther: true,
      max: 30,
      value: "other"
    }
  ],
  smokingStatus: [
    {
      label: "Non-smoker",
      testID: "Nonsmoker",
      value: "N"
    },
    {
      label: "Smoker",
      testID: "Smoker",
      value: "Y"
    }
  ],
  martialStatus: [
    {
      label: "Non-smoker",
      testID: "Nonsmoker",
      value: "N"
    },
    {
      label: "Smoker",
      testID: "Smoker",
      value: "Y"
    }
  ],
  education: [
    {
      label: "Below GCE ‘N’ or ‘O’ Levels certificate",
      testID: "BelowGCE",
      value: "below"
    },
    {
      label: "GCE ‘N’ or ‘O’ Levels and above certificate",
      testID: "AboveGCE",
      value: "above"
    }
  ],
  employStatus: [
    {
      label: "Full Time",
      testID: "FullTime",
      value: "ft"
    },
    {
      label: "Househusband",
      testID: "Househusband",
      value: "hh"
    },
    {
      label: "Housewife",
      testID: "Housewife",
      value: "hw"
    },
    {
      label: "Part Time",
      testID: "PartTime",
      value: "pt"
    },
    {
      label: "Retired",
      testID: "Retired",
      value: "rt"
    },
    {
      label: "Self Employed",
      testID: "SelfEmployed",
      value: "se"
    },
    {
      label: "Student",
      testID: "Student",
      value: "sd"
    },
    {
      label: "Unemployed",
      testID: "Unemployed",
      value: "ue"
    },
    {
      label: "Others",
      testID: "Others",
      isOther: true,
      value: "ot"
    }
  ],
  nameOrder: [
    {
      label: "Given name first",
      testID: "GivenNameFirst",
      value: "F"
    },
    {
      label: "Surname first",
      testID: "SurnameFirst",
      value: "L"
    }
  ],
  singaporePRStatus: [
    {
      label: "Yes",
      testID: "Yes",
      value: "Y"
    },
    {
      label: "No",
      testID: "No",
      value: "N"
    }
  ],
  language: loadVal => [
    {
      key: "en",
      text: "English",
      testID: "English",
      isSelected: loadVal.match("en") !== null
    },
    {
      key: "zh",
      text: "Mandarin",
      testID: "Mandarin",
      isSelected: loadVal.match("zh") !== null
    },
    {
      key: "ms",
      text: "Malay",
      testID: "Malay",
      isSelected: loadVal.match("ms") !== null
    },
    {
      key: "ta",
      text: "Tamil",
      testID: "Tamil",
      isSelected: loadVal.match("ta") !== null
    },
    {
      key: "other",
      text: "Others",
      testID: "Others",
      isSelected: loadVal.match("other") !== null
    }
  ]
};
