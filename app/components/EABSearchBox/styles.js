import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Theme.paleGrey
  },
  wrapper: {
    width: 500,
    height: 500,
    backgroundColor: "white"
  },
  dialogHeader: {
    ...Theme.dialogHeader,
    flexDirection: "column",
    justifyContent: "flex-end",
    paddingBottom: 0,
    height: 84
  },
  headerRow: {
    flexDirection: "row",
    alignItems: "center"
  },
  appBarButton: {
    flexBasis: "auto",
    marginRight: Theme.alignmentL
  },
  searchBar: {
    flex: 1
  },
  recordsRow: {
    padding: Theme.alignmentXS,
    width: "100%",
    alignItems: "center"
  },
  itemSeparatorStyle: {
    height: 1,
    backgroundColor: Theme.lightGrey,
    marginLeft: 16
  },
  itemWrap: {
    height: 50,
    paddingHorizontal: 16,
    width: "100%",
    justifyContent: "center"
  },
  itemText: {
    ...Theme.bodyPrimary
  },
  flatList: {
    flex: 1,
    width: "100%"
  },
  flatListWrap: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start"
  }
});
