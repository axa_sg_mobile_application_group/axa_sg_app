import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import EABButton from "../../components/EABButton";
import { FORM } from "../../components/EABDialog/constants";
import EABDialog from "../../components/EABDialog/EABDialog.ios";
import EABSearchBar from "../../components/EABSearchBar";
import TranslatedText from "../../containers/TranslatedText";
import Theme from "../../theme";
import styles from "./styles";

export default class EABSearchBox extends Component {
  constructor(props) {
    super(props);

    // initial search text
    const selectedOption = props.options.find(
      option => option.value === props.defaultSearchText
    );

    this.state = {
      searchText: selectedOption
        ? selectedOption.label
        : props.defaultSearchText
    };
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  get filteredOptions() {
    const { options } = this.props;
    const { searchText } = this.state;

    if (Array.isArray(options)) {
      return searchText === ""
        ? options
        : options.filter(
            option =>
              option.label.toUpperCase().indexOf(searchText.toUpperCase()) !==
              -1
          );
    }
    throw new Error("options should be array");
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  static getDerivedStateFromProps(props, state) {
    if (!props.isOpen && props.defaultSearchText !== state.searchText) {
      const selectedOption = props.options.find(
        option => option.value === props.defaultSearchText
      );
      return {
        searchText: selectedOption
          ? selectedOption.label
          : props.defaultSearchText
      };
    }
    return null;
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { filteredOptions } = this;
    const { onChange, onClose, placeholder, isOpen, testID } = this.props;
    const { searchText } = this.state;

    return (
      <EABDialog type={FORM} isOpen={isOpen}>
        <View style={styles.dialogHeader}>
          <View style={styles.headerRow}>
            <EABButton
              testID="SearchBox__btnCancel"
              containerStyle={styles.appBarButton}
              onPress={onClose}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.cancel"
              />
            </EABButton>
            <EABSearchBar
              testID="SearchBox__txtSearchBar"
              style={styles.searchBar}
              placeholder={placeholder}
              value={searchText}
              onChange={value => {
                this.setState({
                  searchText: value
                });
              }}
            />
          </View>
          <View style={styles.recordsRow}>
            <TranslatedText
              replace={[
                {
                  value: filteredOptions.length,
                  isPath: false
                }
              ]}
              path="component.foundRecord"
            />
          </View>
        </View>
        <FlatList
          style={styles.flatList}
          data={filteredOptions}
          ItemSeparatorComponent={() => (
            <View style={styles.itemSeparatorStyle} />
          )}
          renderItem={({ item }) => (
            <TouchableOpacity
              testID={`${testID}__btn${item.label}`}
              onPress={() => {
                onChange(item);
                onClose();
              }}
            >
              <View style={styles.itemWrap}>
                <Text style={Theme.bodyPrimary}>
                  {item.labelOnSelecting || item.label}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </EABDialog>
    );
  }
}

EABSearchBox.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      labelOnSelecting: PropTypes.string,
      value: PropTypes.any.isRequired
    })
  ).isRequired,
  isOpen: PropTypes.bool.isRequired,
  defaultSearchText: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  testID: PropTypes.string.isRequired
};

EABSearchBox.defaultValue = {
  testID: ""
};
