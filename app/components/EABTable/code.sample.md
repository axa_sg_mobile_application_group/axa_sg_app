### Table sample code snippets
copy code to container/playground to see the example
```javascript
import React, { Component } from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";
import EABTable from "../components/EABTable";
import EABTableFooter from "../components/EABTableFooter";
import EABTableHeader from "../components/EABTableHeader";
import EABTableSection from "../components/EABTableSection";
import EABTableSectionHeader from "../components/EABTableSectionHeader";

class Applications extends Component {
  constructor() {
    super();

    this.state = {};
  }

  render() {
    return (
      <EABTable
        style={{ width: 600 }}
        tableConfig={[
          {
            key: "A",
            style: {
              flex: 2,
              alignItems: "flex-start"
            }
          },
          {
            key: "B",
            style: {
              flex: 1,
              alignItems: "flex-end"
            }
          },
          {
            key: "C",
            style: {
              flex: 1,
              alignItems: "flex-end"
            }
          },
          {
            key: "D",
            style: {
              flex: 1,
              alignItems: "flex-end"
            }
          }
        ]}
      >
        <EABTableHeader key="A">
          <View>
            <Text>Header</Text>
          </View>
          <View>
            <Text>Header</Text>
          </View>
          <View>
            <Text>Header</Text>
          </View>
          <View>
            <Text>Header</Text>
          </View>
        </EABTableHeader>
        <EABTableSectionHeader key="B">
          <View>
            <Text>Section Header</Text>
          </View>
        </EABTableSectionHeader>
        <EABTableSection key="C">
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
        </EABTableSection>
        <EABTableSection key="D">
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
        </EABTableSection>
        <EABTableSectionHeader key="E">
          <View>
            <Text>Section Header</Text>
          </View>
        </EABTableSectionHeader>
        <EABTableSection key="F">
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
          <View>
            <Text>Some Text</Text>
          </View>
        </EABTableSection>
        <EABTableFooter
          key="G"
          tableConfig={[
            {
              key: "A",
              style: {
                flex: 1,
                alignItems: "flex-end"
              }
            }
          ]}
        >
          <View>
            <Text>Footer</Text>
          </View>
        </EABTableFooter>
      </EABTable>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Applications);
```
