import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  findNodeHandle,
  Image
} from "react-native";
import icInfoActive from "../../../assets/images/icInfoActive.png";
import { disableOpacity } from "../../EABButton/constants";
import Popover from "../../Popover";
import styles from "./styles";
import Theme from "../../../theme";

/**
 * <PopoverTooltip />
 * @description ios popover tooltip component
 * @param {string} text - text in tooltip
 * @param {boolean} isDisable - indicate disable or not
 * @param {number} activeOpacity
 * */
export default class PopoverTooltip extends Component {
  constructor() {
    super();

    this.popoverTarget = React.createRef();

    this.state = {
      popoverVisible: false,
      targetNode: 0
    };
  }

  get popoverOption() {
    return {
      ...{
        preferredContentSize: [375, 100]
      },
      ...this.props.popoverOptions,
      ...{
        popoverBackgroundColor: "#00008f",
        sourceView: this.state.targetNode,
        onHide: () => {
          this.setState({
            popoverVisible: false,
            targetNode: 0
          });
        }
      }
    };
  }

  render() {
    return [
      <TouchableOpacity
        key="icon"
        onPress={() => {
          if (!this.props.isDisable) {
            this.setState({
              popoverVisible: true,
              targetNode: findNodeHandle(this.popoverTarget.current)
            });
          }
        }}
        activeOpacity={this.props.isDisable ? 1 : this.props.activeOpacity}
      >
        <Image
          ref={this.popoverTarget}
          style={[
            styles.image,
            {
              opacity: this.props.isDisable ? disableOpacity : 1
            }
          ]}
          source={icInfoActive}
        />
      </TouchableOpacity>,
      this.state.popoverVisible ? (
        <Popover key="popover" {...this.popoverOption}>
          {this.props.children !== null ? (
            this.props.children
          ) : (
            <View style={styles.popoverWrapper}>
              <View style={styles.textView}>
                <Text style={Theme.toolTipLabel}>{this.props.text}</Text>
              </View>
            </View>
          )}
        </Popover>
      ) : null
    ];
  }
}

PopoverTooltip.propTypes = {
  popoverOptions: PropTypes.shape({
    sourceView: PropTypes.number,
    preferredContentSize: PropTypes.arrayOf(PropTypes.number),
    visible: PropTypes.bool,
    animated: PropTypes.bool,
    cancelable: PropTypes.bool,
    popoverBackgroundColor: PropTypes.string,
    sourceRect: PropTypes.arrayOf(PropTypes.number),
    permittedArrowDirections: PropTypes.arrayOf(PropTypes.number),
    onShow: PropTypes.func,
    onHide: PropTypes.func
  }),
  text: PropTypes.string,
  children: PropTypes.node,
  activeOpacity: PropTypes.number,
  isDisable: PropTypes.bool
};
PopoverTooltip.defaultProps = {
  popoverOptions: {},
  activeOpacity: 0.5,
  text: "",
  children: null,
  isDisable: false
};
