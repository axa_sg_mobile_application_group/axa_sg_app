import { StyleSheet } from "react-native";
import Theme from "../../theme";

const clientInfo = {
  flex: 1,
  flexDirection: "row",
  justifyContent: "space-between",
  paddingVertical: Theme.alignmentM,
  borderTopWidth: 1,
  borderColor: Theme.lightGrey
};

export default StyleSheet.create({
  clientTable: {
    marginVertical: Theme.alignmentXL,
    paddingVertical: Theme.alignmentXS,
    width: 720
  },
  clientRow: {
    flexDirection: "row",
    width: "100%",
    overflow: "hidden"
  },
  clientPhotoWrapper: {
    flexBasis: "auto",
    marginTop: Theme.alignmentL,
    marginHorizontal: Theme.alignmentXL
  },
  clientInfo,
  clientInfoFirst: {
    ...clientInfo,
    borderTopWidth: 0
  },
  clientMainInfo: {
    alignItems: "flex-start"
  },
  policiesWrapper: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    marginRight: Theme.alignmentXL
  }
});
