import { Platform } from "react-native";
import EABDialogIOS from "./EABDialog.ios";

export default Platform.select({
  ios: EABDialogIOS,
  android: {}
});
