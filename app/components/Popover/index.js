import { Platform } from "react-native";
import PopoverIOS from "./PopoverIOS/Popover.ios";

export default Platform.select({
  ios: PopoverIOS,
  android: {}
});
