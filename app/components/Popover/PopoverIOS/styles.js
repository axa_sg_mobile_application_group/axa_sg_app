import { StyleSheet } from "react-native";

export default StyleSheet.create({
  popover: {
    position: "absolute",
    overflow: "hidden",
    opacity: 0,
    backgroundColor: "transparent",
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  container: {
    position: "absolute",
    top: 0
  }
});
