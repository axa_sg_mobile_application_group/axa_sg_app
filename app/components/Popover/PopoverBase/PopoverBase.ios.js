import PropTypes from "prop-types";
import { PureComponent } from "react";
import Popover from "../../Popover";
/**
 *
 * @description ios popover selector component
 * @param {object} popoverOptions - props of Popover's object
 * */
export default class PopoverBase extends PureComponent {
  constructor(props) {
    super(props);
    this.contentHeight = 0;
    this.contentWidth = 0;
    this.measureContentLayout(props.popoverOptions);
  }

  componentWillReceiveProps(nextProps) {
    this.measureContentLayout(nextProps.popoverOptions);
  }

  /**
   * @description measure expected width and height of content layout
   * @param {PropTypes.object} paramProps - current props object
   */
  measureContentLayout(popoverOptions) {
    this.contentHeight = 10;
    this.contentWidth = 100;
    const options = Object.assign({}, Popover.defaultProps, popoverOptions);
    if (options != null) {
      if (options.preferredContentSize != null) {
        const { preferredContentSize } = options;
        if (preferredContentSize.length === 1) {
          [this.contentWidth] = preferredContentSize;
        } else if (preferredContentSize.length >= 2) {
          [this.contentWidth, this.contentHeight] = preferredContentSize;
        }
      }
    }
  }

  render() {
    return null;
  }
}

PopoverBase.propTypes = {
  popoverOptions: PropTypes.oneOfType([PropTypes.object]).isRequired
};
PopoverBase.defaultProps = {};
