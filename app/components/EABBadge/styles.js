import { StyleSheet } from "react-native";

export default StyleSheet.create({
  badge: {
    position: "absolute",
    width: 24,
    height: 24
  }
});
