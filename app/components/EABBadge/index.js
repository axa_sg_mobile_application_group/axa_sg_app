import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image, View } from "react-native";
import icCompleted from "../../assets/images/icCompleted.png";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABBadge />
 * @description badge component. the container position style has been fixed to absolute
 * @param {boolean} isShow - indicates whether the badge is show or not
 * @param {any} [imageSource='assets/images/icons/icCompleted.png'] - image source
 * @param {number} [left=16] - badge left style
 * @param {number} [top=16] - badge top style
 * @param {object=} containerStyle - container style
 * */
class EABBadge extends PureComponent {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get rootStyle() {
    const baseStyle = {
      position: "relative"
    };
    return [baseStyle].concat(this.styleArray);
  }

  get imageStyle() {
    const { top, left } = this.props;
    const { badge } = styles;
    return [
      badge,
      {
        top,
        left
      }
    ];
  }

  render() {
    const { children, isShow, imageSource } = this.props;
    return (
      <View style={this.rootStyle}>
        {children}
        {isShow && <Image style={this.imageStyle} source={imageSource} />}
      </View>
    );
  }
}

EABBadge.propTypes = {
  imageSource: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  left: PropTypes.number,
  top: PropTypes.number,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  isShow: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
};

EABBadge.defaultProps = {
  style: {},
  imageSource: icCompleted,
  left: Theme.alignmentL,
  top: Theme.alignmentL
};

export default EABBadge;
