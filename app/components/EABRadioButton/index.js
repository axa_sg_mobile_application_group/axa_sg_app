import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABRadioButton />
 * @description radio button component
 * @param {array} options - array of options's object
 * @param {string} options[].key - option key
 * @param {string} options[].title - option title
 * @param {function} onPress - the function that will be fire by the onPress event. it will
 *   return the option's object to be the function parameter.
 * @param {string} [buttonColor=Theme.brand] - button default color
 * @param {boolean} [disabled=false] - whether the component is disabled or not
 * @param {string=} selectedOptionKey - the selected option key
 * @param {object|number=} style - the radio button container custom style
 * */
class EABRadioButton extends Component {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get rootStyles() {
    const { container } = styles;
    return [container].concat(this.styleArray);
  }

  render() {
    /**
     * functions
     * */

    const renderPointSign = key => {
      const { selectedOptionKey, buttonColor } = this.props;
      if (key !== selectedOptionKey) return null;
      const { point } = styles;
      const style = [point, { backgroundColor: buttonColor }];
      return <View style={style} />;
    };

    const renderOption = (option, index) => {
      const { key, title } = option;
      const { onPress, buttonColor, disabled, testID } = this.props;
      const { row, rowFirst, button } = styles;
      const viewStyle = index === 0 ? rowFirst : row;
      const btnStyle = [button, { borderColor: buttonColor }];
      const getHightlightedTitleText = oldTitleText => {
        const newTitleText = [];
        const openBracketString = oldTitleText.indexOf("{{");
        const closeBracketString = oldTitleText.indexOf("}}");
        const middleTitleText = oldTitleText.substring(
          openBracketString + 2,
          closeBracketString
        );
        const beforeTitleText = oldTitleText.substring(0, openBracketString);
        const afterTitleText = oldTitleText.substring(closeBracketString + 2);
        newTitleText.push(
          <Text style={styles.title}>
            {beforeTitleText}
            <Text style={styles.replacedTitle}>{middleTitleText}</Text>
            {afterTitleText}
          </Text>
        );
        return newTitleText;
      };
      return (
        <TouchableOpacity
          key={key}
          testID={`${testID}__rb${option.testID}`}
          activeOpacity={0.5}
          disabled={disabled}
          onPress={() => onPress(option)}
        >
          <View style={viewStyle}>
            <View style={btnStyle}>{renderPointSign(key)}</View>
            {title.indexOf("{{") === -1 ? (
              <Text style={styles.title}>{title}</Text>
            ) : (
              getHightlightedTitleText(title)
            )}
          </View>
        </TouchableOpacity>
      );
    };

    const renderOptions = () => {
      const { options } = this.props;
      return options.map(renderOption);
    };

    return <View style={this.rootStyles}>{renderOptions()}</View>;
  }
}

EABRadioButton.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })
  ).isRequired,
  selectedOptionKey: PropTypes.string,
  disabled: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  buttonColor: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

EABRadioButton.defaultProps = {
  testID: "",
  buttonColor: Theme.brand,
  selectedOptionKey: "",
  style: {},
  disabled: false
};

export default EABRadioButton;
