import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  title: {
    fontSize: Theme.fontSizeXXM,
    color: Theme.grey,
    marginBottom: Theme.alignmentXL
  },
  replacedTitle: {
    fontSize: Theme.fontSizeXXM,
    color: Theme.grey,
    marginBottom: Theme.alignmentXL,
    textDecorationLine: "underline"
  }
});
