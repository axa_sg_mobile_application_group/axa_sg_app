import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View } from "react-native";
import * as _ from "lodash";
import {
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  utilities,
  ROP_TABLE,
  NUMBER_FORMAT
} from "eab-web-api";
import styles from "./styles";
import EABTable from "../../EABTable";
import EABTableHeader from "../../EABTableHeader";
import EABTableSectionHeader from "../../EABTableSectionHeader";
import EABTableSection from "../../EABTableSection";
import EABTextField from "../../EABTextField";
import { ContextConsumer } from "../../../context";
import EABi18n from "../../../utilities/EABi18n";
import TranslatedText from "../../../containers/TranslatedText";
import APP_REDUCER_TYPE_CHECK from "../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const {
  PERIOD,
  COMPANY,
  SUMASSURED,
  REPLACE_POLICY_TYPE_AXA,
  REPLACE_POLICY_TYPE_OTHER,
  PENDING_LIFE_AXA,
  PENDING_TOTALPREM_OTHER,
  SHOW_EXIST_TABLE,
  SHOW_ACC_EXIST_TABLE,
  SHOW_PENDING_TABLE,
  SHOW_REPLACE_TABLE,
  EXITSTING_TABLE_MAPPING,
  PENDING_TABLE_MAPPING,
  REPLACE_TABLE_MAPPING
} = ROP_TABLE;

const { ROP_TABLE_MAX_CURRENCY_VALUE } = NUMBER_FORMAT;

const { currencyToNumber, numberToCurrency } = utilities.numberFormatter;

// TODO: ADD COMMENT
class ROPTable extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderTable = this.renderTable.bind(this);
  }

  typeOfPoliciesOnChange({ id, newValue }) {
    const { path, updateROPTable } = this.props;
    updateROPTable({
      path,
      newValue,
      id
    });
  }

  valueOnChange({ id, newValue }) {
    const { path, updateROPTable } = this.props;
    updateROPTable({
      path,
      newValue: currencyToNumber(newValue) || 0,
      id
    });
  }

  static renderTypeOfInsuranceApplication({ tableName }) {
    let titlePath = "";
    if (tableName === PERIOD.EXISTING) {
      titlePath = "application.form.roptable.tableName.exist";
    } else if (tableName === PERIOD.PENDING) {
      titlePath = "application.form.roptable.tableName.pending";
    } else if (tableName === PERIOD.REPLACE) {
      titlePath = "application.form.roptable.tableName.replace";
    }
    return <TranslatedText style={styles.tableName} path={titlePath} />;
  }

  renderTypeOfPolicies({ tableName }) {
    if (tableName !== PERIOD.REPLACE) {
      return null;
    }

    const { rootValues, path, error, textStore, template } = this.props;
    const values = _.get(rootValues, path, {});
    const maxLength = 30;

    const axaIdArr = REPLACE_TABLE_MAPPING[COMPANY.AXA];
    const otherIdArr = REPLACE_TABLE_MAPPING[COMPANY.OTHER];
    const axaNonZero = _.some(
      axaIdArr,
      itemId => _.isNumber(_.get(values, itemId)) && _.get(values, itemId) !== 0
    );
    const otherNonZero = _.some(
      otherIdArr,
      itemId => _.isNumber(_.get(values, itemId)) && _.get(values, itemId) !== 0
    );
    const axaValue = axaNonZero ? _.get(values, REPLACE_POLICY_TYPE_AXA) : "-";
    const otherValue = otherNonZero
      ? _.get(values, REPLACE_POLICY_TYPE_OTHER)
      : "-";
    let axaEditable = true;
    let otherEditable = true;
    if (_.get(template, "disabled") === true || !axaNonZero) {
      axaEditable = false;
    }
    if (_.get(template, "disabled") === true || !otherNonZero) {
      otherEditable = false;
    }

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.hboxDirection}>
            <View style={styles.hboxItem}>
              <EABTextField
                inputStyle={styles.inputShort}
                value={axaValue}
                editable={axaEditable}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "application.form.roptable.typeOfPolicies.axa"
                })}
                isError={
                  axaEditable &&
                  _.get(
                    error,
                    `${path}.${REPLACE_POLICY_TYPE_AXA}.hasError`,
                    false
                  )
                }
                isRequired={
                  axaEditable &&
                  _.get(
                    error,
                    `${path}.${REPLACE_POLICY_TYPE_AXA}.hasError`,
                    false
                  )
                }
                hintText={_.get(
                  error,
                  `${path}.${REPLACE_POLICY_TYPE_AXA}.message.${language}`,
                  ""
                )}
                onChange={newValue =>
                  this.typeOfPoliciesOnChange({
                    id: REPLACE_POLICY_TYPE_AXA,
                    newValue
                  })
                }
                maxLength={maxLength}
              />
            </View>
            <View style={styles.hboxItemMarginLeft}>
              <EABTextField
                inputStyle={styles.inputShort}
                value={otherValue}
                editable={otherEditable}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "application.form.roptable.typeOfPolicies.other"
                })}
                isError={
                  otherEditable &&
                  _.get(
                    error,
                    `${path}.${REPLACE_POLICY_TYPE_OTHER}.hasError`,
                    false
                  )
                }
                isRequired={
                  otherEditable &&
                  _.get(
                    error,
                    `${path}.${REPLACE_POLICY_TYPE_OTHER}.hasError`,
                    false
                  )
                }
                onChange={newValue =>
                  this.typeOfPoliciesOnChange({
                    id: REPLACE_POLICY_TYPE_OTHER,
                    newValue
                  })
                }
                hintText={_.get(
                  error,
                  `${path}.${REPLACE_POLICY_TYPE_OTHER}.message.${language}`,
                  ""
                )}
                maxLength={maxLength}
              />
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  renderItem({ id, editable }) {
    const { rootValues, path, error } = this.props;
    const value = _.get(rootValues, `${path}.${id}`, 0);
    const maxLength = 14;

    return (
      <View style={styles.tableSection}>
        <EABTextField
          inputStyle={styles.inputShort}
          value={numberToCurrency({
            value,
            decimal: 0,
            maxValue: ROP_TABLE_MAX_CURRENCY_VALUE
          })}
          prefix="$"
          editable={editable}
          isError={_.get(error, `${path}.${id}.hasError`, false)}
          isRequired={_.get(error, `${path}.${id}.hasError`, false)}
          onChange={newValue => this.valueOnChange({ id, newValue })}
          maxLength={maxLength}
        />
      </View>
    );
  }

  static renderRowHeader({ company }) {
    let headerTextPath;
    if (company === COMPANY.AXA) {
      headerTextPath = "application.form.roptable.sectionHeader.axa";
    } else if (company === COMPANY.OTHER) {
      headerTextPath = "application.form.roptable.sectionHeader.other";
    } else if (company === COMPANY.TOTAL) {
      headerTextPath = "application.form.roptable.sectionHeader.total";
    }
    return (
      <EABTableSectionHeader
        key={`${company}-header`}
        style={styles.tableSectionHeader}
      >
        <View>
          <TranslatedText
            style={styles.tableSectionHeaderTitle}
            path={headerTextPath}
          />
        </View>
      </EABTableSectionHeader>
    );
  }

  renderRowContent({ company, rowMapping }) {
    const { template } = this.props;
    let editable = true;
    if (template.disabled || company === COMPANY.TOTAL) {
      editable = false;
    }
    return (
      <EABTableSection key={`${company}`}>
        {this.renderItem({ id: rowMapping.LIFE, editable })}
        {this.renderItem({ id: rowMapping.TPD, editable })}
        {this.renderItem({ id: rowMapping.CI, editable })}
        {this.renderItem({ id: rowMapping.PAADB, editable })}
        {this.renderItem({ id: rowMapping.TOTALPREM, editable })}
      </EABTableSection>
    );
  }

  renderTableContent({ tableName }) {
    const result = [];
    let mapping;
    if (tableName === PERIOD.EXISTING) {
      mapping = EXITSTING_TABLE_MAPPING;
    } else if (tableName === PERIOD.PENDING) {
      mapping = PENDING_TABLE_MAPPING;
    } else if (tableName === PERIOD.REPLACE) {
      mapping = REPLACE_TABLE_MAPPING;
    }

    result.push(ROPTable.renderRowHeader({ company: COMPANY.AXA }));
    result.push(
      this.renderRowContent({
        company: COMPANY.AXA,
        rowMapping: _.get(mapping, COMPANY.AXA)
      })
    );

    result.push(ROPTable.renderRowHeader({ company: COMPANY.OTHER }));
    result.push(
      this.renderRowContent({
        company: COMPANY.OTHER,
        rowMapping: _.get(mapping, COMPANY.OTHER)
      })
    );

    result.push(ROPTable.renderRowHeader({ company: COMPANY.TOTAL }));
    result.push(
      this.renderRowContent({
        company: COMPANY.TOTAL,
        rowMapping: _.get(mapping, COMPANY.TOTAL)
      })
    );

    return result;
  }

  renderTableErrorMsg({ tableName }) {
    if (tableName === PERIOD.PENDING) {
      const { error, path } = this.props;
      const tableError = _.get(error, path, {});
      const tableHasError =
        _.get(tableError, `${PENDING_LIFE_AXA}.hasError`) &&
        _.get(tableError, `${PENDING_TOTALPREM_OTHER}.hasError`);
      if (tableHasError) {
        return (
          <TranslatedText style={styles.tableErrorText} path="error.801" />
        );
      }
      return null;
    }
    return null;
  }

  static renderTableHeader() {
    const lifeTextPath = "application.form.roptable.tableHeader.life";
    const tpdTextPath = "application.form.roptable.tableHeader.tpd";
    const ciTextPath = "application.form.roptable.tableHeader.ci";
    const paAdbTextPath = "application.form.roptable.tableHeader.paAdb";
    const totalPremTextPath = "application.form.roptable.tableHeader.totalPrem";

    const headerView = textPath => (
      <View style={styles.tableHeader}>
        <TranslatedText style={styles.tableHeaderTitle} path={textPath} />
      </View>
    );

    return (
      <EABTableHeader key="header">
        {headerView(lifeTextPath)}
        {headerView(tpdTextPath)}
        {headerView(ciTextPath)}
        {headerView(paAdbTextPath)}
        {headerView(totalPremTextPath)}
      </EABTableHeader>
    );
  }

  renderTable({ tableName }) {
    return (
      <View>
        {ROPTable.renderTypeOfInsuranceApplication({ tableName })}
        <EABTable
          style={styles.table}
          tableConfig={[
            {
              key: `${tableName}-${SUMASSURED.LIFE}`,
              style: styles.tableSectionColumn
            },
            {
              key: `${tableName}-${SUMASSURED.TPD}`,
              style: styles.tableSectionColumn
            },
            {
              key: `${tableName}-${SUMASSURED.CI}`,
              style: styles.tableSectionColumn
            },
            {
              key: `${tableName}-${SUMASSURED.PAADB}`,
              style: styles.tableSectionColumn
            },
            {
              key: `${tableName}-${SUMASSURED.TOTALPREM}`,
              style: styles.tableSectionColumn
            }
          ]}
        >
          {ROPTable.renderTableHeader({ tableName })}
          {this.renderTableContent({ tableName })}
        </EABTable>
        {this.renderTableErrorMsg({ tableName })}
        {this.renderTypeOfPolicies({ tableName })}
      </View>
    );
  }

  renderExistingTable() {
    const { rootValues, path, template } = this.props;
    const { presentation = "" } = template;
    return (_.get(rootValues, `${path}.${SHOW_EXIST_TABLE}`) === "Y" &&
      presentation.includes(SHOW_EXIST_TABLE)) ||
      (_.get(rootValues, `${path}.${SHOW_ACC_EXIST_TABLE}`) === "Y" &&
        presentation.includes(SHOW_ACC_EXIST_TABLE))
      ? this.renderTable({ tableName: PERIOD.EXISTING })
      : null;
  }

  renderPendingTable() {
    const { rootValues, path } = this.props;
    return _.get(rootValues, `${path}.${SHOW_PENDING_TABLE}`) === "Y"
      ? this.renderTable({ tableName: PERIOD.PENDING })
      : null;
  }

  renderReplaceTable() {
    const { rootValues, path } = this.props;
    return _.get(rootValues, `${path}.${SHOW_REPLACE_TABLE}`) === "Y"
      ? this.renderTable({ tableName: PERIOD.REPLACE })
      : null;
  }

  render() {
    return (
      <View>
        {this.renderExistingTable()}
        {this.renderPendingTable()}
        {this.renderReplaceTable()}
      </View>
    );
  }
}

ROPTable.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  updateROPTable: PropTypes.func.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired
};

ROPTable.defaultProps = {};

export default ROPTable;
