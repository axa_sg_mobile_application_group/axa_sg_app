import { StyleSheet } from "react-native";
import Theme from "../../../theme";
import Colors from "../../../theme/colors";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  table: {
    paddingVertical: Theme.alignmentM
  },
  tableName: {
    color: Colors.brand,
    fontSize: Theme.fontSizeXXM,
    marginTop: Theme.alignmentXL,
    fontWeight: "bold"
  },
  tableSectionColumn: {
    flex: 1,
    flexBasis: 70,
    alignItems: "flex-start"
  },
  tableSectionColumn2: {
    flex: 1,
    flexBasis: 70,
    alignItems: "flex-end"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel,
    textAlign: "left"
  },
  tableSectionHeader: {
    backgroundColor: Theme.brandSuperTransparent
  },
  tableSectionHeaderTitle: {
    ...Theme.captionBrand
  },
  tableSection: {
    backgroundColor: Theme.white
  },
  hboxDirection: {
    display: "flex",
    flexDirection: "row"
  },
  hboxItem: {
    flex: 1
  },
  hboxItemMarginLeft: {
    flex: 1,
    marginLeft: Theme.alignmentXL
  },
  tableErrorText: {
    ...Theme.tableColumnLabel,
    textAlign: "left",
    color: Theme.red,
    marginBottom: Theme.alignmentXL
  },
  inputShort: {
    fontSize: Theme.fontSizeS,
    lineHeight: Theme.fontSizeXXXM
  }
});
