import { StyleSheet } from "react-native";
import Theme from "../../theme";
import Colors from "../../theme/colors";

export default StyleSheet.create({
  title: {
    fontSize: Theme.fontSizeXXM,
    color: Theme.grey,
    marginBottom: Theme.alignmentXL
  },
  replacedTitle: {
    fontSize: Theme.fontSizeXXM,
    marginBottom: Theme.alignmentXL,
    textDecorationLine: "underline"
  },

  blockTitle: {
    fontSize: Theme.fontSizeXXXM,
    marginVertical: Theme.alignmentXL,
    fontWeight: "bold",
    color: Theme.brand
  },

  tableBlockTitle: {
    fontSize: Theme.fontSizeXXXM,
    marginBottom: Theme.alignmentS,
    fontWeight: "bold",
    color: Theme.brand
  },

  blockError: {
    fontFamily: this.fontFamilySecondary,
    fontWeight: "400",
    fontSize: Theme.fontSizeS,
    lineHeight: 18,
    letterSpacing: -0.07,
    color: Colors.negative
  },

  requiredHint: {
    ...Theme.bodySecondary,
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },

  personalDetailsRedWanning: {
    fontWeight: "bold",
    color: Theme.negative,
    fontSize: Theme.fontSizeXXM
  },

  hboxDirection: {
    display: "flex",
    flexDirection: "row"
  },

  hboxItem: {
    flex: 1
  },

  hboxText: {
    marginTop: -16
  },

  gestationalHboxItemMargin: {
    width: 0,
    height: 0
  },

  hboxItemMarginLeft: {
    flex: 1,
    marginLeft: Theme.alignmentXL
  },

  Column2: {
    flex: 1,
    marginLeft: Theme.alignmentXL
  },

  inputShort: {
    width: 320
  },

  PEPContainer: {
    flexDirection: "row"
  }
});
