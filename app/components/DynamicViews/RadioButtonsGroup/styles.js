import { StyleSheet } from "react-native";
import Theme from "../../../theme";

const labelText = { ...Theme.bodySecondary, textAlign: "left" };

const title = {
  fontSize: Theme.fontSizeXXM,
  color: Theme.darkGrey
};

const requiredHint = {
  ...labelText,
  paddingLeft: Theme.alignmentXXS,
  color: Theme.negative
};

const replacedTitle = {
  fontSize: Theme.fontSizeXXM,
  marginBottom: Theme.alignmentXL,
  textDecorationLine: "underline"
};

const replacedRadioButtonStyle = {
  paddingRight: 25,
  textDecorationLine: "underline",
  marginBottom: 15
};

export default StyleSheet.create({
  title,
  requiredHint,
  replacedTitle,
  replacedRadioButtonStyle
});
