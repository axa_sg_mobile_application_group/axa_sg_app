import * as _ from "lodash";

import {
  getOptionList,
  getOptionListForDynamicCompnent,
  getOptionListForCountryCode
} from "../../../utilities/getOptionList";

export const getDynamicViewsOptions = ({ options, optionsMap, language }) => {
  let data;
  if (_.isString(options) && _.get(optionsMap, options)) {
    if (_.isEqual("countryTelCode", options)) {
      data = getOptionListForCountryCode({
        optionMap: optionsMap.countryTelCode,
        language
      });
    } else {
      data = getOptionList({
        optionMap: optionsMap[options],
        language
      });
    }
  } else if (_.isArray(options)) {
    data = getOptionListForDynamicCompnent(options);
  } else {
    data = [];
  }

  return data;
};

export const getOptionLabel = ({ options, optionsMap, language, value }) => {
  const data = getDynamicViewsOptions({ options, optionsMap, language });
  const foundObj = _.find(data, item => _.get(item, "value") === value);
  return _.get(foundObj, "label", "");
};

export default getDynamicViewsOptions;
