import * as _ from "lodash";

export const findStructureMapping = ({ mapping, id }) => {
  let result;
  if (_.has(mapping, id)) {
    result = _.get(mapping, id);
  } else {
    // find the default structure
    const questionId = _.replace(id, "_DATA", "");
    const defaultId = `${questionId.slice(0, -2)}_DATA_DEFAULT`;
    const defualtSectionIds = _.get(mapping, defaultId);
    // data transformation
    if (!_.isUndefined(defualtSectionIds)) {
      result = {};
      _.each(defualtSectionIds, (itemDefualtSectionIds, columnKey) => {
        result[columnKey] = _.map(
          itemDefualtSectionIds,
          defualtSectionId => `${questionId}${defualtSectionId}`
        );
      });
    }
  }
  return result;
};

export const findTitleMapping = ({ mapping, id }) => {
  let result = {};
  if (_.has(mapping, id)) {
    result = _.get(mapping, id);
  } else {
    // find the default structure
    const questionId = _.replace(id, "_DATA", "");
    const defaultId = `${questionId.slice(0, -2)}_DATA_DEFAULT`;
    result = _.get(mapping, defaultId);
  }
  return result;
};
