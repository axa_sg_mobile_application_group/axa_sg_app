import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { EAPP } from "eab-web-api";
import EABButton from "../../EABButton";
import { RECTANGLE_BUTTON_1 } from "../../EABButton/constants";
import styles from "./styles";
import { ContextConsumer } from "../../../context";
import TranslatedText from "../../../containers/TranslatedText";

class GoFNA extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.buttonOnPress = this.buttonOnPress.bind(this);
  }

  buttonOnPress() {
    const { needsRedirectTo, navigateToFNA, saveForm } = this.props;
    saveForm({
      genPDF: false,
      actionType: EAPP.action.GOFNA,
      callback: () => {
        needsRedirectTo({
          page: "fe",
          callback: () => {
            navigateToFNA();
          }
        });
      }
    });
  }

  render() {
    return (
      <ContextConsumer>
        {({ language, hideApplicationDialog }) => (
          <EABButton
            style={styles.button}
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              hideApplicationDialog();
              this.buttonOnPress();
            }}
          >
            <TranslatedText
              style={styles.text}
              language={language}
              path="application.form.roptable.button.goFNA"
            />
          </EABButton>
        )}
      </ContextConsumer>
    );
  }
}

GoFNA.propTypes = {
  saveForm: PropTypes.func.isRequired,
  needsRedirectTo: PropTypes.func.isRequired,
  navigateToFNA: PropTypes.func.isRequired
};

GoFNA.defaultProps = {};

export default GoFNA;
