import { StyleSheet } from "react-native";
import Theme from "../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  text: {
    ...Theme.tableColumnLabel,
    textAlign: "left"
  },

  button: {
    width: 350
  }
});
