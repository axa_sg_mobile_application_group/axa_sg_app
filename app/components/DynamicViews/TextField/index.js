import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Alert } from "react-native";
import * as _ from "lodash";
import {
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  utilities,
  NUMBER_FORMAT
} from "eab-web-api";
import styles from "./styles";
import {
  checkPostalCode,
  postalCodeFunc,
  checkUnitNo,
  unitNoFunc,
  isNumberInRange,
  numberToStringWithDecimal,
  numberNotInRangeFunc,
  getMaxValueWithValidation
} from "./utils";
import EABTextField from "../../EABTextField";
import APP_REDUCER_TYPE_CHECK from "../../../constants/REDUCER_TYPE_CHECK";
import EABi18n from "../../../utilities/EABi18n";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../context";
import getEABTextByLangType from "../../../utilities/getEABTextByLangType";

const { numberFormatter } = utilities;
const { numberToCurrency, currencyToNumber } = numberFormatter;
const { MAX_CURRENCY_VALUE } = NUMBER_FORMAT;

class TextField extends PureComponent {
  constructor() {
    super();
    this.onChange = this.onChange.bind(this);
    this.postalCodeCallback = this.postalCodeCallback.bind(this);
    this.getPlaceholder = this.getPlaceholder.bind(this);
    this.getMaxLength = this.getMaxLength.bind(this);
  }

  onChange({ newValue, currentValue, language }) {
    const {
      template,
      rootValues,
      path,
      onUpdateValues,
      updateAddress
    } = this.props;
    const { id, subType, max, min, decimal } = template;
    let result = currentValue;

    // if subType is number, max means maximum value
    // otherwise, max means maximum length
    const upperSubType = subType && _.toUpper(subType);

    if (
      upperSubType === "POSTALCODE" &&
      checkPostalCode({
        template,
        rootValues,
        path,
        newValue,
        currentValue
      })
    ) {
      const callback = ({ success }) => {
        this.postalCodeCallback({ language, success });
      };
      postalCodeFunc({
        newValue,
        id,
        path,
        template,
        updateAddress,
        onUpdateValues,
        callback
      });
    } else if (
      upperSubType === "UNIT_NO" &&
      checkUnitNo({ currentValue, newValue })
    ) {
      unitNoFunc({
        id,
        newValue,
        path,
        template,
        onUpdateValues
      });
    } else if (upperSubType === "NUMBER" || upperSubType === "CURRENCY") {
      let numNewValue = currencyToNumber(newValue, max);
      numNewValue = _.isNull(numNewValue) || numNewValue < 0 ? 0 : numNewValue;

      if (numNewValue === 0) {
        result = "";
      } else if (isNumberInRange({ value: numNewValue, max, min })) {
        if (_.isNumber(decimal)) {
          result = numberToStringWithDecimal({ value: numNewValue, decimal });
        } else if (upperSubType === "CURRENCY") {
          result = numberToCurrency({
            value: numNewValue,
            decimal: 0,
            max: MAX_CURRENCY_VALUE
          });
        } else {
          result = numNewValue.toFixed(0);
        }
      } else {
        result = numberNotInRangeFunc({
          newValue: numNewValue,
          max,
          decimal
        });
      }
      onUpdateValues({ id, value: result, path, iTemplate: template });
    } else {
      result = newValue;
      onUpdateValues({ id, value: result, path, iTemplate: template });
    }
  }

  getPlaceholder({ language }) {
    const { template } = this.props;
    const { title, placeholder } = template;
    return getEABTextByLangType({ data: title || placeholder, language });
  }

  getMaxLength() {
    const { template, rootValues, path } = this.props;
    const { subType, max, decimal, maxLength, validation } = template;
    let newMaxLength;
    if (validation) {
      newMaxLength = getMaxValueWithValidation({
        max,
        validation,
        rootValues,
        path
      });
    } else if (_.toUpper(subType) === "NUMBER" && !_.isUndefined(max)) {
      if (decimal) {
        newMaxLength = _.size(_.toString(Math.floor(max))) + decimal + 1;
      } else {
        newMaxLength = _.size(_.toString(max));
      }
    } else if (_.isUndefined(subType) || _.toUpper(subType) !== "NUMBER") {
      newMaxLength = max;
    }
    return newMaxLength || maxLength;
  }

  postalCodeCallback({ success, language }) {
    const { textStore } = this.props;
    if (!success) {
      Alert.alert(
        EABi18n({
          language,
          textStore,
          path: "clientForm.error.postalCodeNotFound"
        })
      );
    }
  }

  render() {
    const { template, rootValues, path, error } = this.props;
    const { id, subType, disabled, mandatory } = template;

    const value = _.get(rootValues, `${path}.${id}`, "");

    return (
      <ContextConsumer>
        {({ language }) => (
          <EABTextField
            style={styles.container}
            value={value}
            editable={!disabled}
            placeholder={this.getPlaceholder({ language }) || ""}
            isError={_.get(error, `${path}.${id}.hasError`, false)}
            isRequired={mandatory && this.getPlaceholder({ language })}
            hintText={_.get(error, `${path}.${id}.message.${language}`, "")}
            onChange={newValue => {
              this.onChange({ newValue, currentValue: value, language });
            }}
            maxLength={this.getMaxLength()}
            prefix={subType === "currency" ? "S$" : ""}
          />
        )}
      </ContextConsumer>
    );
  }
}

TextField.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  updateAddress: PropTypes.func
};

TextField.defaultProps = {
  updateAddress: () => {}
};

export default TextField;
