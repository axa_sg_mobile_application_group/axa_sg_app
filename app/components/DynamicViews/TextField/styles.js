import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  hboxItemMarginLeft: {
    width: 320,
    marginLeft: Theme.alignmentXL
  },
  container: {
    paddingBottom: 10
  },
  specialContainer: {
    marginTop: 30
  }
});
