import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View, Text, Dimensions } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";
import HTML from "react-native-render-html";
import Theme from "../../theme";
import RadioButtonsGroup from "./RadioButtonsGroup";
import TextSelection from "../../containers/DynamicViews/TextSelection";
import TextField from "../../containers/DynamicViews/TextField";
import Title from "./Title";
import SelectorField from "../../containers/DynamicViews/SelectorField";
import CheckBox from "../../containers/DynamicViews/CheckBox";
import PopoverTooltip from "../PopoverTooltip/PopoverTooltipIOS";
import ROPTable from "../../containers/DynamicViews/ROPTable";
import Table from "../../containers/DynamicViews/Table";
import TextArea from "./TextArea";
import GoFNA from "../../containers/DynamicViews/GoFNA";
import { ContextConsumer } from "../../context";
import TranslatedText from "../../containers/TranslatedText";
import styles from "./styles";
import getEABTextByLangType from "../../utilities/getEABTextByLangType";

const { trigger } = utilities;

class DynamicViews extends PureComponent {
  constructor() {
    super();
    this.renderItem = this.renderItem.bind(this);
  }

  /**
   * render individual item by type
   * */
  renderItem({ template, path, language, isInHBox }) {
    const { onUpdateValues, rootValues, error } = this.props;
    const viewType = template.type.toUpperCase();

    // TODO: FIND ANOTHER WAY TO SOLVE THE PROBLEM OF BLOCK ID
    if (viewType === "BLOCK" && _.has(template, "id")) {
      path = `${path}.${template.id}`;
    }

    trigger.setDisable({ template, rootValues, valuePath: path });

    if (!trigger.show({ template, rootValues, valuePath: path })) {
      return null;
    }

    const { title, items, tips, subType } = template;
    const titleText = getEABTextByLangType({
      data: title,
      language
    });

    switch (viewType) {
      case "TAB": {
        if (items) {
          return _.map(items, item =>
            this.renderItem({ template: item, path, language })
          );
        }
        return null;
      }
      case "QTITLE": {
        return (
          <Title
            template={template}
            titleText={titleText}
            rootValues={rootValues}
            path={path}
          />
        );
      }

      case "TABLEDIALOG": {
        const result = [];
        const newPath = `${path}`;
        if (items) {
          _.each(items, item => {
            result.push(
              this.renderItem({ template: item, path: newPath, language })
            );
          });
        }
        return result;
      }
      case "TABLE-BLOCK-T":
      case "TABLE-BLOCK":
      case "BLOCK": {
        const result = [];
        const blockId = template.id || template.key;
        let hasError;
        let blockTitleStyle;

        if (viewType === "TABLE-BLOCK-T" || viewType === "TABLE-BLOCK") {
          hasError = _.get(error, `values.${blockId}.hasError`);
          blockTitleStyle = styles.tableBlockTitle;
        } else {
          hasError = _.get(error, `${path}.${blockId}.hasError`);
          blockTitleStyle = styles.blockTitle;
        }
        if (titleText && viewType !== "TABLE-BLOCK") {
          result.push(
            <View style={styles.PEPContainer}>
              <Text style={blockTitleStyle}>
                {`${titleText} `}
                {(_.has(template, "asterisk") &&
                  _.isEqual(template.asterisk, true)) ||
                !_.has(template, "asterisk") ? (
                  <Text style={styles.requiredHint}>*</Text>
                ) : null}
              </Text>
              {tips ? (
                <PopoverTooltip
                  popoverOptions={{
                    preferredContentSize: [420, 200]
                  }}
                  text={tips.title}
                />
              ) : null}
            </View>
          );
        }

        if (hasError) {
          if (viewType === "TABLE-BLOCK-T" || viewType === "TABLE-BLOCK") {
            result.push(
              <Text style={styles.blockError}>
                {_.get(error, `values.${blockId}.message.${language}`)}
              </Text>
            );
          } else {
            result.push(
              <Text style={styles.blockError}>
                {_.get(error, `${path}.${blockId}.message.${language}`)}
              </Text>
            );
          }
        }

        if (items) {
          _.each(items, item => {
            result.push(this.renderItem({ template: item, path, language }));
          });
        }
        return result;
      }
      case "QRADIOGROUP": {
        return (
          <RadioButtonsGroup
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "TEXTSELECTION": {
        return (
          <TextSelection
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "PARAGRAPH": {
        const { content } = template;
        const paragraphType = _.get(template, "paragraphType");
        let newContent = _.cloneDeep(content);
        let cssStyles = {};
        switch (paragraphType) {
          case "replaceableText":
            newContent = newContent.replace(
              "{{[applicant/investor]}}",
              rootValues.applicationForm.values.proposer.personalInfo.fullName
            );
            cssStyles = {
              p: {
                fontSize: Theme.fontSizeXM,
                marginVertical: Theme.alignmentS
              }
            };
            break;
          case "replaceableTextFC":
            newContent = newContent.replace(
              "{{[customer service officer]}}",
              rootValues.quotation.agent.name
            );
            cssStyles = {
              p: {
                fontSize: Theme.fontSizeXM,
                marginVertical: Theme.alignmentS
              }
            };
            break;
          default:
            newContent = content;
            cssStyles = {
              p: {
                fontSize: Theme.fontSizeXM,
                color: "red",
                marginVertical: Theme.alignmentS
              }
            };
            break;
        }
        return (
          <View>
            {titleText ? (
              <Text style={styles.personalDetailsRedWanning}>{titleText}</Text>
            ) : null}
            <HTML
              html={newContent}
              tagsStyles={cssStyles}
              imagesMaxWidth={Dimensions.get("window").width}
            />
          </View>
        );
      }
      case "HBOX": {
        if (_.isArray(items)) {
          const { length } = items;
          const result = [];
          if (template.key === "checkboxgroup") {
            if (
              _.get(rootValues, `${path}.LIFESTYLE02a_1`) !== "Y" &&
              _.get(rootValues, `${path}.LIFESTYLE02b_1`) !== "Y" &&
              _.get(rootValues, `${path}.LIFESTYLE02c_1`) !== "Y"
            ) {
              result.push(
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.301"
                />
              );
            }
          }
          for (let index = 0; index < length; index += 1) {
            if (index % 2 === 0) {
              if (index + 1 === length) {
                result.push(
                  <View style={styles.hboxDirection}>
                    <View style={styles.hboxItem}>
                      {this.renderItem({
                        template: items[index],
                        path,
                        language
                      })}
                    </View>

                    {items[index].optionsTable &&
                    items[index].optionsTable === "payors" ? (
                      <View style={styles.hboxItemMarginLeft} />
                    ) : null}
                  </View>
                );
              } else {
                result.push(
                  <View>
                    <Text style={styles.hboxText}>{titleText}</Text>
                    <View style={styles.hboxDirection}>
                      <View style={styles.hboxItem}>
                        {this.renderItem({
                          template: items[index],
                          path,
                          language,
                          isInHBox: true
                        })}
                      </View>
                      <View
                        style={
                          items[0].type === "qtitle"
                            ? styles.gestationalHboxItemMargin
                            : styles.hboxItemMarginLeft
                        }
                      >
                        {this.renderItem({
                          template: items[index + 1],
                          path,
                          language,
                          isInHBox: true
                        })}
                      </View>
                    </View>

                    {items[0].type === "qtitle" ? (
                      <View style={styles.hboxDirection}>
                        <View style={styles.hboxItem}>
                          {this.renderItem({
                            template: items[index + 1],
                            path,
                            language,
                            isInHBox: true
                          })}
                        </View>
                        <View style={styles.hboxItemMarginLeft} />
                      </View>
                    ) : null}
                  </View>
                );
              }
            }
          }
          return result;
        }
        return null;
      }
      case "TEXT":
      case "TEXTFIELD": {
        return (
          <TextField
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "DATEPICKER":
      case "PICKER": {
        return (
          <SelectorField
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
            isInHBox={isInHBox}
          />
        );
      }
      case "CHECKBOX": {
        return (
          <CheckBox
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "SATABLE": {
        return (
          <ROPTable
            template={template}
            rootValues={rootValues}
            path={path}
            error={error}
          />
        );
      }
      case "TABLE": {
        return (
          <Table
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "TEXTAREA": {
        return (
          <TextArea
            template={template}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      case "GOFNA": {
        return <GoFNA />;
      }
      case "READONLY": {
        const newTemplate = _.cloneDeep(template);
        newTemplate.disabled = true;
        if (subType) {
          if (
            _.toUpper(subType) === "PICKER" ||
            _.toUpper(subType) === "DATE"
          ) {
            newTemplate.type =
              _.toUpper(subType) === "PICKER" ? "PICKER" : "DATEPICKER";
            return (
              <SelectorField
                template={newTemplate}
                rootValues={rootValues}
                path={path}
                onUpdateValues={onUpdateValues}
                error={error}
              />
            );
          }
          return null;
        }
        return (
          <TextField
            template={newTemplate}
            rootValues={rootValues}
            path={path}
            onUpdateValues={onUpdateValues}
            error={error}
          />
        );
      }
      default:
        return null;
    }
  }
  render() {
    const { template, path } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>{this.renderItem({ template, path, language })}</View>
        )}
      </ContextConsumer>
    );
  }
}

DynamicViews.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  path: PropTypes.string.isRequired
};

DynamicViews.defaultProps = {};

export default DynamicViews;
