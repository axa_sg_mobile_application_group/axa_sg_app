import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import EABTextSelection from "../../EABTextSelection";
import styles from "./styles";

class TextSelection extends PureComponent {
  render() {
    const { template, rootValues, path, onUpdateValues } = this.props;
    const { options, title, mandatory, id, disabled } = template;
    const values = _.get(rootValues, path, "");
    const result = [];
    _.each(options, option => {
      option.isSelected = values[id] === option.value;
    });
    result.push(
      <Text style={styles.title}>
        {title}
        {mandatory ? <Text style={styles.requiredHint}> *</Text> : null}
      </Text>
    );
    result.push(
      <EABTextSelection
        options={options}
        editable={!disabled}
        onPress={option => {
          onUpdateValues({
            id,
            value: option.value,
            path,
            iTemplate: template
          });
        }}
      />
    );
    return result;
  }
}

TextSelection.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired
};

TextSelection.defaultProps = {};

export default TextSelection;
