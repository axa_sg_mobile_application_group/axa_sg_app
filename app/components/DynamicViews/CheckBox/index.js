import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import EABCheckBox from "../../EABCheckBox";
import { ContextConsumer } from "../../../context";
import getEABTextByLangType from "../../../utilities/getEABTextByLangType";
import {
  transformTrustedIndividualContent,
  htmlTransform
} from "../Utils/StringUtils";
import styles from "./styles";

const { OPTIONS_MAP } = REDUCER_TYPES;

class CheckBox extends PureComponent {
  static resetCheckBoxValue({ getCheckedValueFromOthers, value, id }) {
    if (getCheckedValueFromOthers) {
      const {
        othersValue,
        checkedValue: checkBoxTargetValue
      } = getCheckedValueFromOthers;
      const idArr = getCheckedValueFromOthers.id.split(",");
      _.each(idArr, otherFieldId => {
        if (
          _.has(value, otherFieldId) &&
          _.get(value, otherFieldId) !== othersValue
        ) {
          _.set(value, id, checkBoxTargetValue);
        }
      });
    }
  }

  renderHTMLContent({ content, language }) {
    const { rootValues, path, template, optionsMap } = this.props;
    const { id, contentStyle, mandatory } = template;
    const paragraphStyle = _.assign({}, styles.text, contentStyle);
    const result = [];

    if (id === "TRUSTED_IND04") {
      const transformedContent = transformTrustedIndividualContent({
        content,
        rootValues,
        path,
        id,
        optionsMap,
        language
      });
      const arrSplitByStarting = _.split(transformedContent, "<b>");
      _.each(arrSplitByStarting, itemString => {
        if (_.includes(itemString, "</b>")) {
          const arrSplitByEnding = _.split(itemString, "</b>");
          if (arrSplitByEnding && arrSplitByEnding[0]) {
            result.push(
              <Text style={styles.fontBold}>{arrSplitByEnding[0]}</Text>
            );
          }
          if (arrSplitByEnding && arrSplitByEnding[1]) {
            result.push(<Text>{arrSplitByEnding[1]}</Text>);
          }
        } else {
          result.push(<Text>{itemString}</Text>);
        }
      });
    } else {
      result.push(<Text>{htmlTransform({ content })}</Text>);
    }

    if (mandatory) {
      result.push(<Text style={styles.fontRed}> *</Text>);
    }

    return <Text style={paragraphStyle}>{result}</Text>;
  }

  render() {
    const { template, rootValues, path, onUpdateValues, error } = this.props;
    const { id, getCheckedValueFromOthers, content, mandatory } = template;
    let { title } = template;
    const value = _.get(rootValues, `${path}.${id}`, "");
    let resultValue = null;

    if (getCheckedValueFromOthers) {
      CheckBox.resetCheckBoxValue({ getCheckedValueFromOthers, value, id });
    }

    if (_.isEqual(_.get(template, "proposerText"), true)) {
      title = title.replace(
        "{{[applicant/investor]}}",
        rootValues.applicationForm.values.proposer.personalInfo.fullName
      );
    }
    resultValue = _.has(template, "value") ? _.get(template, "value") : value;

    return (
      <ContextConsumer>
        {({ language }) => (
          <EABCheckBox
            labelStyle={
              _.get(template, "labelStyle") ? template.labelStyle : null
            }
            style={styles.container}
            options={[
              {
                key: id,
                text: getEABTextByLangType({
                  data: title,
                  language
                }),
                isSelected: resultValue === "Y"
              }
            ]}
            disable={_.get(template, "disabled", false)}
            onPress={option => {
              onUpdateValues({
                id,
                value: option.isSelected ? "N" : "Y",
                path,
                iTemplate: template
              });
            }}
            isRequired={mandatory}
            htmlContent={
              content ? this.renderHTMLContent({ content, language }) : null
            }
            isError={_.get(error, `${path}.${id}.hasError`, false)}
          />
        )}
      </ContextConsumer>
    );
  }
}

CheckBox.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired
};

CheckBox.defaultProps = {};

export default CheckBox;
