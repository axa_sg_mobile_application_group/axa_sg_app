import { StyleSheet } from "react-native";
import Theme from "../../../theme";

const container = {
  width: "95%"
};

const text = {
  ...Theme.bodyPrimary,
  marginLeft: Theme.alignmentL,
  marginVertical: 1,
  maxWidth: Theme.textMaxWidth,
  fontSize: Theme.fontSizeXXM
};

const fontBold = {
  fontWeight: "bold"
};

const fontRed = {
  color: "red"
};

export default StyleSheet.create({
  container,
  text,
  fontBold,
  fontRed
});
