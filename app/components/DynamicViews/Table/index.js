import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import moment from "moment";
import { View, Text, Image, TouchableOpacity } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, DYNAMIC_VIEWS } from "eab-web-api";
import EABTable from "../../EABTable";
import EABButton from "../../EABButton";
import EABTableHeader from "../../EABTableHeader";
import { TEXT_BUTTON, RECTANGLE_BUTTON_1 } from "../../EABButton/constants";
import icDelete from "../../../assets/images/icDeleteForEAPP.png";
import EABTableSection from "../../EABTableSection";
import styles from "./styles";
import {
  titleMapping,
  structureMapping,
  shieldTitleMapping,
  shieldStructureMapping
} from "./mapping";
import { getOptionLabel } from "../Utils/OptionsMap";
import { findStructureMapping, findTitleMapping } from "../Utils/TableMapping";
import EABi18n from "../../../utilities/EABi18n";
import { ContextConsumer } from "../../../context";
import APP_REDUCER_TYPE_CHECK from "../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import TableDialog from "../../../containers/DynamicViews/TableDialog";
import TranslatedText from "../../../containers/TranslatedText";
import Theme from "../../../theme";
import EABCheckBox from "../../EABCheckBox";

const { DIALOG_NEW_RECORD } = DYNAMIC_VIEWS;

class Table extends PureComponent {
  constructor() {
    super();

    this.state = {
      isDialogOpen: false,
      dialogValues: {},
      recordIndex: -1
    };
    this.deleteRecord = this.deleteRecord.bind(this);
    this.openButtonPress = this.openButtonPress.bind(this);
    this.editRecordPress = this.editRecordPress.bind(this);
    this.renderAddButton = this.renderAddButton.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  deleteRecord({ recordIndex }) {
    const { template, path, deleteApplicationFormTableRecord } = this.props;

    deleteApplicationFormTableRecord({
      path,
      dataId: template.id,
      recordIndex
    });
  }

  closeDialog() {
    this.setState({ isDialogOpen: false });
  }

  openDialog({ dialogValues, recordIndex }) {
    this.setState({
      isDialogOpen: true,
      dialogValues,
      recordIndex
    });
  }

  openButtonPress() {
    this.openDialog({ recordIndex: DIALOG_NEW_RECORD, dialogValues: {} });
  }

  editRecordPress({ recordValue, recordIndex }) {
    this.openDialog({
      dialogValues: recordValue,
      recordIndex
    });
  }

  renderAddButton() {
    const { template, rootValues, path } = this.props;
    const { id, max } = template;
    const recordValues = _.get(rootValues, `${path}.${id}`, []);
    const maxRecords = max || 8;
    const disabled = recordValues.length >= maxRecords || !!template.disabled;

    return (
      <View>
        <EABButton
          buttonType={RECTANGLE_BUTTON_1}
          containerStyle={styles.addButton}
          isDisable={disabled}
          onPress={this.openButtonPress}
        >
          <TranslatedText
            style={Theme.textButtonLabelNormalEmphasizedAccent}
            path="button.add"
          />
        </EABButton>
      </View>
    );
  }

  renderTable({ language }) {
    const { template, path, rootValues } = this.props;
    const { id, disabled } = template;
    const recordValues = _.get(rootValues, `${path}.${id}`, []);
    const recordViews = [];
    const titles = findTitleMapping({
      mapping:
        rootValues.quotation.quotType !== "SHIELD"
          ? titleMapping
          : shieldTitleMapping,
      id
    });

    const renderColumn = ({ recordValue, recordIndex, sectionIds }) => {
      const content = [];
      _.each(sectionIds, sectionId => {
        const foundTemplateObj = _.find(
          template.items,
          item => (item.id || item.key || item.presentation) === sectionId
        );

        if (!_.isUndefined(foundTemplateObj)) {
          let titleComponent;

          const checkList = [
            sectionId,
            sectionId[sectionId.length - 1],
            sectionId[sectionId.length - 2]
          ];

          const checkString =
            checkList.find(check => _.has(titles, `content.${check}`)) || "";

          if (checkString !== "") {
            if (_.get(titles, `content.${checkString}`) === "") {
              titleComponent = null;
            } else {
              titleComponent = (
                <Text style={styles.tableHeaderTitle}>
                  {_.get(titles, `content.${checkString}`)}
                  {_.get(titles, `content.${checkString}`) !== null
                    ? ":"
                    : null}
                </Text>
              );
            }
          } else {
            titleComponent = (
              <Text style={styles.tableHeaderTitle}>
                {foundTemplateObj.title || ""}
                {foundTemplateObj.title ? ":" : null}
              </Text>
            );
          }
          content.push(titleComponent);

          if (
            foundTemplateObj.type &&
            ["TABLE-BLOCK-T", "TABLE-BLOCK"].indexOf(
              _.toUpper(foundTemplateObj.type)
            ) > -1
          ) {
            _.each(foundTemplateObj.items, itemInTableBlock => {
              if (_.toUpper(itemInTableBlock.type) === "CHECKBOX") {
                if (_.get(recordValue, itemInTableBlock.id) === "Y") {
                  let checkBoxTitle;
                  if (_.has(itemInTableBlock, "valueId")) {
                    checkBoxTitle = _.get(
                      recordValue,
                      _.get(itemInTableBlock, "valueId")
                    );
                  } else if (
                    itemInTableBlock.id === "other" &&
                    _.get(recordValue, "resultOther")
                  ) {
                    checkBoxTitle = _.get(recordValue, "resultOther");
                  } else {
                    checkBoxTitle = _.get(itemInTableBlock, "title");
                  }
                  content.push(
                    <EABCheckBox
                      style={styles.checkBoxContainer}
                      options={[
                        {
                          key: itemInTableBlock.id,
                          text: checkBoxTitle,
                          isSelected: true
                        }
                      ]}
                      onPress={() => {}}
                    />
                  );
                }
              } else if (
                ["PICKER", "QRADIOGROUP"].indexOf(
                  _.toUpper(itemInTableBlock.type)
                ) > -1
              ) {
                let resultText = "";
                if (_.has(itemInTableBlock, "valueId")) {
                  resultText = _.get(
                    recordValue,
                    _.get(itemInTableBlock, "valueId")
                  );
                } else if (
                  _.get(recordValue, itemInTableBlock.id) === "other" &&
                  _.get(recordValue, "resultOther")
                ) {
                  resultText = _.get(recordValue, "resultOther");
                } else if (_.has(itemInTableBlock, "options")) {
                  const options = _.get(itemInTableBlock, "options");
                  const selectedOption = _.find(
                    options,
                    option =>
                      option.value === _.get(recordValue, itemInTableBlock.id)
                  );
                  resultText = selectedOption.title || "";
                }
                content.push(
                  <Text style={styles.tableSectionTitle}>{resultText}</Text>
                );
              }
            });
          } else if (
            _.toUpper(_.get(foundTemplateObj, "type", "")) === "PICKER" &&
            _.has(foundTemplateObj, "options")
          ) {
            const { optionsMap } = this.props;
            const label = getOptionLabel({
              options: foundTemplateObj.options,
              optionsMap,
              language,
              value: _.get(recordValue, sectionId, "")
            });
            content.push(<Text style={styles.tableSectionTitle}>{label}</Text>);
          } else if (
            _.toUpper(_.get(foundTemplateObj, "type", "")) === "DATEPICKER" &&
            _.has(foundTemplateObj, "dateFormat")
          ) {
            const value = _.get(recordValue, sectionId, "");
            const dateFormat = _.toUpper(_.get(foundTemplateObj, "dateFormat"));

            let displayText = moment(value).format(dateFormat);
            if (displayText === "Invalid date") {
              displayText = "";
            }
            content.push(
              <Text style={styles.tableSectionTitle}>{displayText}</Text>
            );
          } else if (_.get(foundTemplateObj, "type", "") === "datepicker") {
            const value = _.get(recordValue, sectionId, "");
            const dateFormat = "DD/MM/YYYY";

            let displayText = moment(value).format(dateFormat);
            if (displayText === "Invalid date") {
              displayText = "";
            }
            content.push(
              <Text style={styles.tableSectionTitle}>{displayText}</Text>
            );
          } else if (
            _.toUpper(_.get(foundTemplateObj, "type", "")) === "QRADIOGROUP"
          ) {
            // Handle if question has trigger
            if (
              (foundTemplateObj.trigger &&
                !_.get(recordValue, foundTemplateObj.trigger.id, "")) ||
              !_.get(foundTemplateObj, "trigger", "")
            ) {
              const options = _.get(foundTemplateObj, "options", []);
              const selectedOption = _.find(
                options,
                option => option.value === _.get(recordValue, sectionId)
              );
              const resultText = _.get(selectedOption, "title", "");

              content.push(
                <Text style={styles.tableSectionTitle}>{resultText}</Text>
              );
            }
          } else if (foundTemplateObj.trigger) {
            const { trigger } = foundTemplateObj;
            const value = _.get(recordValue, sectionId, "");
            if (trigger.length > 1) {
              _.forEach(trigger, triggerItem => {
                if (!_.get(recordValue, triggerItem.id, "")) {
                  content.push(
                    <Text style={styles.tableSectionTitle}>{value}</Text>
                  );
                } else {
                  content.push(<Text style={styles.tableSectionTitle} />);
                }
              });
            } else {
              content.push(
                <Text style={styles.tableSectionTitle}>{value}</Text>
              );
            }
          } else {
            const value = _.get(recordValue, sectionId, "");
            content.push(<Text style={styles.tableSectionTitle}>{value}</Text>);
          }
        }
      });
      return (
        <View style={styles.tableSection}>
          <TouchableOpacity
            onPress={() => {
              this.editRecordPress({ recordValue, recordIndex });
            }}
            disabled={!!disabled}
          >
            {content}
          </TouchableOpacity>
        </View>
      );
    };

    const renderRecord = ({ recordValue, recordIndex, isShowDeleteButton }) => {
      const sectionIds = findStructureMapping({
        mapping:
          rootValues.quotation.quotType !== "SHIELD"
            ? structureMapping
            : shieldStructureMapping,
        id
      });

      if (_.isUndefined(sectionIds)) {
        return null;
      }

      const columnView0 = renderColumn({
        recordValue,
        recordIndex,
        sectionIds: sectionIds.column0
      });
      const columnView1 = renderColumn({
        recordValue,
        recordIndex,
        sectionIds: sectionIds.column1
      });
      const columnView2 = renderColumn({
        recordValue,
        recordIndex,
        sectionIds: sectionIds.column2
      });

      const columnView3 = (
        <View style={styles.tableSectionDelete}>
          {isShowDeleteButton ? (
            <EABButton
              style={styles.deleteButton}
              buttonType={TEXT_BUTTON}
              isDisable={!!disabled}
              onPress={() => {
                this.deleteRecord({ recordIndex });
              }}
            >
              <Image style={styles.deleteIcon} source={icDelete} />
            </EABButton>
          ) : null}
        </View>
      );

      return (
        <EABTableSection
          key={`record-${recordIndex}-table`}
          style={styles.tableSectionContainer}
        >
          {columnView0}
          {columnView1}
          {columnView2}
          {columnView3}
        </EABTableSection>
      );
    };

    const renderHeader = () => {
      const headerTitleArr = _.get(titles, "header", []);
      const headerView = text => (
        <View style={styles.tableHeader}>
          <Text style={styles.tableHeaderTitle}>{text}</Text>
        </View>
      );

      if (_.isArray(headerTitleArr) && headerTitleArr.length > 2) {
        // TODO: EABTableHeader does not accept array of headerView, need to fix this problem
        return (
          <EABTableHeader
            key="table-header"
            style={styles.tableHeaderContainer}
          >
            {headerView(headerTitleArr[0])}
            {headerView(headerTitleArr[1])}
            {headerView(headerTitleArr[2])}
            {headerView("")}
          </EABTableHeader>
        );
      }
      return null;
    };

    if (!_.isEmpty(recordValues)) {
      const isShowDeleteButton =
        _.isArray(recordValues) && recordValues.length > 1;
      _.each(recordValues, (recordValue, recordIndex) => {
        recordViews.push(
          renderRecord({
            recordValue,
            recordIndex,
            isShowDeleteButton
          })
        );
      });
    }
    return (
      <View>
        <EABTable
          style={styles.table}
          tableConfig={[
            {
              key: `first-column`,
              style: styles.tableSectionColumn1
            },
            {
              key: `second-column`,
              style: styles.tableSectionColumn2
            },
            {
              key: `third-column`,
              style: styles.tableSectionColumn3
            },
            {
              key: `fourth-column`,
              style: styles.tableSectionColumnDelete
            }
          ]}
        >
          {renderHeader()}
          {recordViews}
        </EABTable>
      </View>
    );
  }

  renderDialog() {
    const { template, path, renderDynamicViews, error } = this.props;
    const { isDialogOpen, dialogValues, recordIndex } = this.state;

    return (
      <TableDialog
        isOpen={isDialogOpen}
        template={template}
        values={dialogValues}
        path={path}
        recordIndex={recordIndex}
        renderDynamicViews={renderDynamicViews}
        error={error}
        closeDialogFunc={this.closeDialog}
      />
    );
  }

  render() {
    const { template, path, rootValues, textStore } = this.props;
    const { id } = template;
    const recordValues = _.get(rootValues, `${path}.${id}`, []);

    const isRequiredText = _.isEmpty(recordValues) ? (
      <ContextConsumer>
        {({ language }) => (
          <Text style={Theme.fieldTextOrHelperTextNegative}>
            {EABi18n({
              language,
              textStore,
              path: "error.103"
            })}
          </Text>
        )}
      </ContextConsumer>
    ) : null;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            {isRequiredText}
            {this.renderTable({ language })}
            {this.renderAddButton()}
            {this.renderDialog()}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

Table.propTypes = {
  renderDynamicViews: PropTypes.func.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  deleteApplicationFormTableRecord: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  optionsMap: REDUCER_TYPE_CHECK[REDUCER_TYPES.OPTIONS_MAP]
};

Table.defaultProps = {
  optionsMap: []
};

export default Table;
