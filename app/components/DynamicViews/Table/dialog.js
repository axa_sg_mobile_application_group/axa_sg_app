import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import * as _ from "lodash";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";
import EABButton from "../../EABButton";
import EABDialog from "../../EABDialog";
import Theme from "../../../theme";
import styles from "./styles";
import TranslatedText from "../../../containers/TranslatedText";
import DynamicViews from "../index";

const { validation } = utilities;

/**
 * <inputDialog />
 * @description the input dialog of dynamic views
 * */
class Dialog extends PureComponent {
  constructor() {
    super();

    this.state = {
      dialogValues: {}
    };
    this.cancelBtnPress = this.cancelBtnPress.bind(this);
    this.doneBtnPress = this.doneBtnPress.bind(this);
    this.onChange = this.onChange.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  onChange({ id, value, dialogValues }) {
    const newDialogValues = _.cloneDeep(dialogValues);
    newDialogValues[id] = value;
    this.setState({ dialogValues: newDialogValues });
  }

  closeDialog() {
    const { closeDialogFunc } = this.props;
    this.setState({ dialogValues: {} });
    closeDialogFunc();
  }

  cancelBtnPress() {
    this.closeDialog();
  }

  doneBtnPress() {
    const {
      saveApplicationFormTableRecord,
      path,
      template,
      recordIndex,
      values
    } = this.props;
    const { dialogValues } = this.state;
    const newDialogValues = _.assign({}, values, dialogValues);

    saveApplicationFormTableRecord({
      path,
      dataId: template.id,
      values: newDialogValues,
      recordIndex
    });
    this.closeDialog();
  }

  renderTableDialog() {
    const { template, isOpen, values } = this.props;
    const { dialogValues } = this.state;
    const newTemplate = _.assign({}, template, { type: "tableDialog" });
    let newDialogValues = _.cloneDeep(dialogValues);
    if (_.isEmpty(newDialogValues)) {
      newDialogValues = values;
    }

    const dynamicViewsValues = {
      values: newDialogValues
    };
    const dynamicViewsPath = "values";

    if (_.has(newTemplate, "trigger")) {
      delete newTemplate.trigger;
    }

    const dialogError = {};
    validation.validateApplicationFormTableDialog({
      iTemplate: newTemplate,
      iValues: newDialogValues,
      iErrorObj: dialogError,
      rootValues: dynamicViewsValues,
      path: dynamicViewsPath
    });

    const dynamicViewsError = { values: dialogError };
    const isDoneDisable = _.some(
      dialogError,
      iError => _.get(iError, "hasError") === true
    );

    return (
      <EABDialog isOpen={isOpen}>
        <View style={styles.titleBar}>
          <View style={styles.titleBarViewLeft}>
            <EABButton
              style={styles.titleBarButtonLeft}
              onPress={() => {
                this.cancelBtnPress();
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalEmphasizedAccent}
                path="button.cancel"
              />
            </EABButton>
          </View>

          <View style={styles.titleBarViewMiddle}>
            <TranslatedText style={Theme.title} path="" />
          </View>

          <View style={styles.titleBarViewRight}>
            <EABButton
              style={styles.titleBarButtonRight}
              onPress={() => {
                this.doneBtnPress();
              }}
              isDisable={isDoneDisable}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalEmphasizedAccent}
                path="button.done"
              />
            </EABButton>
          </View>
        </View>
        <View style={styles.borderLine} />
        <KeyboardAwareScrollView extraScrollHeight={60}>
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
          >
            <View style={styles.scrollViewContent}>
              <DynamicViews
                template={newTemplate}
                rootValues={dynamicViewsValues}
                path={dynamicViewsPath}
                error={dynamicViewsError}
                onUpdateValues={({ id, value }) => {
                  this.onChange({ id, value, dialogValues: newDialogValues });
                }}
              />
            </View>
          </ScrollView>
        </KeyboardAwareScrollView>
      </EABDialog>
    );
  }

  render() {
    return <View>{this.renderTableDialog()}</View>;
  }
}

Dialog.propTypes = {
  path: PropTypes.string.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  saveApplicationFormTableRecord: PropTypes.func.isRequired,
  values: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].dialogValues,
  closeDialogFunc: PropTypes.func.isRequired,
  recordIndex: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired
};

Dialog.defaultProps = {
  values: {}
};

export default Dialog;
