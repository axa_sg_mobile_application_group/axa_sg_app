import { StyleSheet } from "react-native";
import Theme from "../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  addButton: {
    width: 16
  },
  borderLine: {
    width: "100%",
    borderTopWidth: 1,
    borderColor: Theme.lightGrey,
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL
  },
  container: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  checkBoxContainer: {
    width: "85%"
  },
  deleteButton: {
    // ...Theme.textButtonLabelSmallAccent,
    // flex: 1
  },
  deleteIcon: {
    width: 24,
    height: 24
  },
  table: {
    paddingTop: Theme.alignmentXS,
    paddingBottom: Theme.alignmentXL
  },
  tableSectionColumn: {
    flex: 32,
    flexBasis: 70,
    alignItems: "flex-start"
  },
  tableSectionColumnDelete: {
    flex: 4,
    flexBasis: 35,
    flexDirection: "row",
    alignItems: "flex-end",
    borderStyle: "dotted",
    borderBottomColor: Theme.brand,
    borderBottomWidth: 0
  },
  tableSectionColumn1: {
    flex: 32,
    flexBasis: 120,
    alignItems: "flex-start",
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  tableSectionColumn2: {
    flex: 32,
    flexBasis: 140,
    alignItems: "flex-start",
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  tableSectionColumn3: {
    flex: 31,
    flexBasis: 135,
    alignItems: "flex-start",
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  tableSectionColumn4: {
    flex: 1,
    flexBasis: 130,
    alignItems: "flex-end"
  },
  tableSectionColumn5: {
    flex: 1,
    flexBasis: 130,
    alignItems: "flex-end"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableHeaderContainer: {
    borderBottomWidth: 0
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel,
    textAlign: "left"
  },
  tableSectionHeader: {
    backgroundColor: Theme.brandSuperTransparent
  },
  tableSectionHeaderTitle: {
    ...Theme.captionBrand
  },
  tableSection: {
    backgroundColor: Theme.white
  },
  tableSectionContainer: {
    borderBottomWidth: 0
  },
  tableSectionDelete: {
    paddingLeft: 10,
    borderBottomColor: Theme.brand,
    borderBottomWidth: 0
  },
  tableSectionTitle: {
    ...Theme.subheadPrimary
  },
  tableFooterColumn1: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  tableFooterColumn2: {
    flex: 4,
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  tableFooterDateView: {
    backgroundColor: Theme.white
  },
  tableFooterValueView: {
    backgroundColor: Theme.white,
    flexDirection: "row",
    justifyContent: "flex-end",
    flexWrap: "wrap"
  },
  tableFooterColumnDate: {
    ...Theme.subheadSecondary,
    textAlign: "left"
  },
  tableFooterDataView: {
    flexDirection: "row"
  },
  tableFooterDataTitle: {
    ...Theme.subheadSecondary,
    textAlign: "right",
    marginLeft: Theme.alignmentL,
    marginRight: Theme.alignmentL
  },
  tableFooterDataValue: {
    ...Theme.subheadPrimary,
    textAlign: "right",
    flexBasis: 130
  },
  titleBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: Theme.alignmentM,
    alignItems: "center",
    width: "100%"
  },
  titleBarViewLeft: {
    alignItems: "flex-start",
    flex: 1
  },
  titleBarViewRight: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    flex: 1
  },
  titleBarViewMiddle: {
    alignItems: "center",
    flex: 1
  },
  titleBarTitle: {
    ...Theme.title
  },
  titleBarButtonLeft: {
    marginLeft: Theme.alignmentXL,
    position: "absolute",
    left: Theme.alignmentXL,
    top: 0
  },
  titleBarButtonRight: {
    marginRight: Theme.alignmentXL,
    position: "absolute",
    right: Theme.alignmentXL,
    top: 0
  },
  titleBarButtonTitle: {
    ...Theme.textButtonLabelNormalAccent
  },
  scrollView: {
    marginBottom: Theme.alignmentL
  },
  scrollViewContent: {
    marginLeft: Theme.alignmentL,
    marginRight: Theme.alignmentL,
    marginBottom: Theme.alignmentXXXL
  }
});
