import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";
import styles from "./styles";

const { dynamicApplicationForm } = utilities;

class Title extends PureComponent {
  static getHightlightedTitleText({ id, oldTitleText }) {
    const newTitleText = [];
    const openBracketString = oldTitleText.indexOf("{{");
    const closeBracketString = oldTitleText.indexOf("}}");
    const middleTitleText = oldTitleText.substring(
      openBracketString + 2,
      closeBracketString
    );
    const beforeTitleText = oldTitleText.substring(0, openBracketString);
    const afterTitleText = oldTitleText.substring(closeBracketString + 2);
    newTitleText.push(
      <Text key={id} style={styles.title}>
        {beforeTitleText}
        <Text key={id} style={styles.replacedTitle}>
          {middleTitleText}
        </Text>
        {afterTitleText}
      </Text>
    );
    return newTitleText;
  }

  render() {
    const { template, titleText, rootValues, path } = this.props;
    const { id, replaceTitle } = template;
    let result;
    let resultTitle;

    if (replaceTitle) {
      resultTitle = dynamicApplicationForm.getItemValueFunc({
        rootValues,
        valuePath: path,
        id: _.get(template, "replaceTitle.id", "")
      });
    } else {
      resultTitle = titleText;
    }

    if (resultTitle.indexOf("{{") === -1) {
      result = (
        <Text key={id} style={styles.title}>
          {resultTitle}
        </Text>
      );
    } else {
      result = Title.getHightlightedTitleText({
        id,
        oldTitleText: resultTitle
      });
    }

    return result;
  }
}

Title.propTypes = {
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  titleText: PropTypes.string.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired
};

Title.defaultProps = {};

export default Title;
