import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  title: {
    fontSize: Theme.fontSizeXXM,
    marginBottom: Theme.alignmentXL
  },
  replacedTitle: {
    fontSize: Theme.fontSizeXXM,
    marginBottom: Theme.alignmentXL,
    textDecorationLine: "underline"
  }
});
