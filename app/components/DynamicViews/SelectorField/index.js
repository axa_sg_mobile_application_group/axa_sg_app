import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, DYNAMIC_VIEWS } from "eab-web-api";
import moment from "moment";
import styles from "./styles";
import { getDynamicViewsOptions } from "../Utils/OptionsMap";
import SelectorField from "../../SelectorField";
import EABButton from "../../EABButton";
import { TEXT_BUTTON } from "../../EABButton/constants";
import {
  DATE_PICKER,
  FLAT_LIST,
  SEARCH_BOX
} from "../../SelectorField/constants";
import { ContextConsumer } from "../../../context";
import icDelete from "../../../assets/images/icDeleteForEAPP.png";

const { OPTIONS_MAP } = REDUCER_TYPES;
const { FLAT_LIST_MAX_NUM } = DYNAMIC_VIEWS;

class DynamicSelectorField extends PureComponent {
  constructor() {
    super();
    this.getPickerOptions = this.getPickerOptions.bind(this);
    this.deleteButtonPressed = this.deleteButtonPressed.bind(this);
    this.pickerItemOnPressed = this.pickerItemOnPressed.bind(this);
    this.renderPicker = this.renderPicker.bind(this);
    this.renderDatePicker = this.renderDatePicker.bind(this);
  }

  getPickerOptions({ language }) {
    const { template, optionsMap } = this.props;
    const { trigger, options } = template;

    const data = getDynamicViewsOptions({ options, optionsMap, language });

    if (trigger) {
      if (trigger.type === "excludeSG") {
        if (_.isString(trigger.value)) {
          const excludeArr = trigger.value.split(",");
          _.remove(data, record =>
            _.includes(excludeArr, _.get(record, "value", ""))
          );
        }
      }
    }

    return data;
  }

  deleteButtonPressed() {
    const { template, path, onUpdateValues } = this.props;
    const { id } = template;
    onUpdateValues({
      id,
      value: "",
      path,
      iTemplate: template
    });
  }

  pickerItemOnPressed({ item }) {
    const { template, path, onUpdateValues } = this.props;
    const { id } = template;
    onUpdateValues({
      id,
      value: item.value,
      path,
      iTemplate: template
    });
  }

  renderPicker({ language }) {
    const { template, path, rootValues, error, isInHBox } = this.props;
    const { id, title, disabled, mandatory } = template;
    const value = _.get(rootValues, `${path}.${id}`, "");
    const isRequired =
      id === "FUND_SRC02" || id === "FUND_SRC11" ? false : mandatory;

    const pickerOptions = this.getPickerOptions({ language });

    if (pickerOptions) {
      const fields = {
        editable: !disabled,
        isRequired,
        style: styles.container,
        value: value || _.get(template, "value", ""),
        placeholder: title,
        isError: _.get(error, `${path}.${id}.hasError`, false),
        hintText: _.get(error, `${path}.${id}.message.${language}`, ""),
        placeholderFloat: _.size(title) > 40 && isInHBox
      };

      if (_.size(pickerOptions) > FLAT_LIST_MAX_NUM) {
        return (
          <SelectorField
            selectorType={SEARCH_BOX}
            options={pickerOptions}
            onChange={item => {
              this.pickerItemOnPressed({ item });
            }}
            {...fields}
          />
        );
      }

      return (
        <SelectorField
          selectorType={FLAT_LIST}
          flatListOptions={{
            renderItemOnPress: (itemKey, item) => {
              this.pickerItemOnPressed({ item });
            },
            keyExtractor: item => item.key,
            selectedValue: null,
            data: pickerOptions
          }}
          datePickerIOSOptions={{}}
          {...fields}
        />
      );
    }

    return null;
  }

  renderDatePicker() {
    const { template, rootValues, path, onUpdateValues, error } = this.props;
    const {
      id,
      title,
      disabled,
      min,
      max,
      dateFormat,
      mandatory,
      style
    } = template;
    const value = _.get(rootValues, `${path}.${id}`, "");

    const maximumDate = max ? new Date(moment().add(max, "years")) : new Date();
    const minimumDate = min
      ? new Date(moment().add(-min, "years"))
      : new Date();

    return (
      <View style={styles.datePickerContainer}>
        <ContextConsumer>
          {({ language }) => (
            <SelectorField
              selectorType={DATE_PICKER}
              value={value}
              style={style || styles.datePicker}
              placeholder={title}
              editable={!disabled}
              dateFormat={dateFormat}
              datePickerIOSOptions={{
                mode: "date",
                date: value === "" ? new Date() : new Date(value),
                onDateChange: date => {
                  const newDate = date.toISOString();
                  onUpdateValues({
                    id,
                    value: newDate.slice(0, 10),
                    path,
                    iTemplate: template
                  });
                },
                minimumDate,
                maximumDate
              }}
              isError={_.get(error, `${path}.${id}.hasError`, false)}
              isRequired={mandatory}
              hintText={_.get(error, `${path}.${id}.message.${language}`, "")}
            />
          )}
        </ContextConsumer>
        {mandatory ? null : (
          <EABButton
            style={styles.deleteButton}
            buttonType={TEXT_BUTTON}
            isDisable={!!disabled}
            onPress={this.deleteButtonPressed}
          >
            <Image style={styles.deleteIcon} source={icDelete} />
          </EABButton>
        )}
      </View>
    );
  }

  render() {
    const { template } = this.props;
    const { type } = template;

    if (_.toUpper(type) === "PICKER") {
      return (
        <ContextConsumer>
          {({ language }) => <View>{this.renderPicker({ language })}</View>}
        </ContextConsumer>
      );
    } else if (_.toUpper(type) === "DATEPICKER") {
      return this.renderDatePicker();
    }
    return null;
  }
}

DynamicSelectorField.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  isInHBox: PropTypes.bool
};

DynamicSelectorField.defaultProps = {
  isInHBox: false
};

export default DynamicSelectorField;
