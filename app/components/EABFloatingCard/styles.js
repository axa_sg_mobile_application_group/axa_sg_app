import { StyleSheet } from "react-native";
import Theme from "../../theme";

const content = {
  paddingVertical: Theme.alignmentL,
  paddingHorizontal: Theme.alignmentXL,
  width: "100%"
};

const imageBackground = {
  flex: 1,
  width: "100%",
  zIndex: -1
};

export default StyleSheet.create({
  container: {
    width: 278,
    height: 343,
    borderRadius: 14,
    backgroundColor: Theme.white,
    shadowColor: Theme.coolGrey,
    shadowOffset: {
      width: 0,
      height: 14
    },
    shadowRadius: 23,
    shadowOpacity: 1
  },
  contentContainer: {
    width: "100%",
    height: "100%",
    overflow: "hidden"
  },
  content,
  contentFullBox: {
    ...content,
    height: "100%"
  },
  imageBackground,
  imageBackgroundFullBox: {
    ...imageBackground,
    position: "absolute",
    marginTop: 0,
    height: "100%"
  },
  unselectedColor: {
    borderWidth: 3,
    borderColor: Theme.brandTransparent
  },
  selectedColor: {
    borderWidth: 3,
    borderColor: Theme.brand
  }
});
