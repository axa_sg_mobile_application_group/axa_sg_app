import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "flex-end",
    height: "100%",
    width: "100%"
  },
  box: {
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentL,
    marginBottom: Theme.alignmentXL,
    backgroundColor: Theme.snackbarBg,
    borderRadius: Theme.radiusXS,
    shadowColor: Theme.darkGrey,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 70,
    shadowOpacity: 1,
    maxWidth: 568
  }
});
