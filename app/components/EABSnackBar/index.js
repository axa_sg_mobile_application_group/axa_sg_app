import PropTypes from "prop-types";
import React, { Component } from "react";
import { Modal, TouchableWithoutFeedback, View } from "react-native";
import styles from "./styles";

/**
 * <EABSnackBar />
 * @description snack bar component
 * @param {boolean} isOpen - indicates whether the snack bar is open or not
 * @param {function} closeRequest - the function will be fire by the component close request
 * @param {number} [timing=5000] - snack bar automatic shutdown time. 0 for disable automatic
 *   shutdown.
 * */
export default class EABSnackBar extends Component {
  constructor() {
    super();

    this.timeOutFunction = null;
  }

  render() {
    /**
     * initialize
     * */
    if (this.props.isOpen) {
      this.timeOutFunction = setTimeout(() => {
        this.props.closeRequest();
      }, this.props.timing);
    } else {
      clearTimeout(this.timeOutFunction);
    }

    return (
      <Modal
        style={styles.modal}
        visible={this.props.isOpen}
        transparent
        animationType="slide"
      >
        <TouchableWithoutFeedback
          onPress={() => {
            this.props.closeRequest();
          }}
        >
          <View style={styles.container}>
            <View style={styles.box}>{this.props.children}</View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

EABSnackBar.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeRequest: PropTypes.func.isRequired,
  timing: PropTypes.number,
  children: PropTypes.node.isRequired
};
EABSnackBar.defaultProps = {
  timing: 5000
};
