import PropTypes from "prop-types";
import React from "react";
import { StyleSheet, View, Picker } from "react-native";
import Popover from "../../Popover";
import PopoverBase from "../../Popover/PopoverBase";
import styles from "./styles";

/**
 * <PopoverPicker />
 * @description ios popover selector component
 * @param {array} pickerOptions - props of Picker's object
 * */
export default class PopoverPicker extends PopoverBase {
  constructor(props) {
    super(props);
    let val = "";
    if (props.pickerOptions != null) {
      val = props.pickerOptions.selectedValue;
    }
    this.state = {
      itemValue: val,
      itemIndex: ""
    };
  }

  get pickerStyle() {
    if (this.props.pickerOptions != null) {
      return Object.assign(
        { height: this.contentHeight, width: (this.contentWidth * 2) / 3 },
        StyleSheet.flatten(this.props.pickerOptions.style)
      );
    }
    return null;
  }

  get otherPickerOptions() {
    if (this.props.pickerOptions != null) {
      return Object.keys(this.props.pickerOptions).reduce((result, key) => {
        const reduceResult = result;
        if (
          key !== "style" &&
          key !== "onValueChange" &&
          key !== "selectedValue"
        ) {
          reduceResult[key] = this.props.pickerOptions[key];
        }
        return reduceResult;
      }, {});
    }
    return null;
  }

  render() {
    const onValueChangeFunc = (itemValue, itemIndex) => {
      this.setState({
        itemIndex,
        itemValue
      });
      if (
        this.props.pickerOptions != null &&
        this.props.pickerOptions.onValueChange
      ) {
        this.props.pickerOptions.onValueChange(itemValue, itemIndex);
      }
    };

    const { testID } = this.props;

    return (
      <Popover {...this.props.popoverOptions} testID={testID}>
        <View style={styles.pickerWrap}>
          <Picker
            selectedValue={this.state.itemValue}
            onValueChange={onValueChangeFunc}
            {...this.otherPickerOptions}
            style={this.pickerStyle}
          >
            {this.otherPickerOptions && this.otherPickerOptions.items
              ? this.otherPickerOptions.items.map(item => (
                  <Picker.Item
                    key={Math.random()}
                    label={item.label}
                    value={item.value}
                  />
                ))
              : null}
          </Picker>
        </View>
      </Popover>
    );
  }
}

PopoverPicker.propTypes = {
  ...PopoverBase.propTypes,
  pickerOptions: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    )
  ]),
  testID: PropTypes.string
};
PopoverPicker.defaultProps = {
  ...PopoverBase.defaultProps,
  pickerOptions: {},
  testID: ""
};
