import { StyleSheet } from "react-native";
import Theme from "../../theme";

const field = {
  marginTop: Theme.alignmentXS
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    width: "100%"
  },
  field,
  fieldFirst: {
    ...field,
    marginTop: 0
  },
  photoPickerWrapper: {
    marginVertical: Theme.alignmentXL,
    alignItems: "flex-start",
    width: "100%"
  },
  avatarWrapper: {
    backgroundColor: Theme.brandTransparent,
    borderRadius: Theme.radiusL,
    width: 64,
    height: 64,
    overflow: "hidden"
  },
  avatar: {
    width: 64,
    height: 64
  },
  /**
   * mobile no
   * */
  mobileNumberWrapper: {
    flexDirection: "row"
  },
  prefix: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXL,
    width: 120
  },
  mobileNo: {
    flex: 1
  }
});
