import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import * as _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Image, TouchableOpacity, View } from "react-native";
import avatar from "../../assets/images/avatar64X64Dp.png";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import EABi18n from "../../utilities/EABi18n";
import {
  getOptionList,
  getOptionListForCountryCode
} from "../../utilities/getOptionList";
import photoPicker from "../../utilities/photoPicker";
import EABTextField from "../EABTextField";
import SelectorField from "../SelectorField";
import { FLAT_LIST, SEARCH_BOX } from "../SelectorField/constants";
import styles from "./styles";
import { TEXT, SELECT, MOBILE_NO } from "./constants";
import Theme from "../../theme";

const { CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * @description client form of trusted individual
 * @param {function} relationshipOnChange
 * @param {function} relationshipOtherOnChange
 * @param {function} givenNameOnChange
 * @param {function} surnameOnChange
 * @param {function} nameOnChange
 * @param {function} nameOrderOnChange
 * @param {function} IDDocumentTypeOnChange
 * @param {function} IDDocumentTypeOtherOnChange
 * @param {function} IDOnChange
 * @param {function} prefixAOnChange
 * @param {function} mobileNoAOnChange
 * */
export default class ClientFormTrustedIndividual extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formWidth: 0
    };
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description this function is used to formatting props to UI structure.
   *   HAVE TO BE AWARE, photo is not in this list
   * @return {array} field list array
   * */
  get fieldList() {
    const {
      relationship,
      relationshipOnChange,
      isRelationshipError,
      relationshipOther,
      relationshipOtherOnChange,
      isRelationshipOtherError,
      givenName,
      givenNameOnChange,
      isGivenNameError,
      surname,
      surnameOnChange,
      name,
      nameOnChange,
      isNameError,
      nameOrder,
      nameOrderOnChange,
      IDDocumentType,
      IDDocumentTypeOnChange,
      isIDDocumentTypeError,
      IDDocumentTypeOther,
      IDDocumentTypeOtherOnChange,
      isIDDocumentTypeOtherError,
      ID,
      IDOnChange,
      isIDError,
      prefixA,
      prefixAOnChange,
      isPrefixAError,
      mobileNoA,
      mobileNoAOnChange,
      isMobileNoAError
    } = this.props;

    return [
      {
        key: "given name",
        placeholderPath: "clientForm.givenNameField",
        value: givenName,
        other: { hasOther: false },
        onChange: givenNameOnChange,
        hasError: isGivenNameError.hasError,
        errorMessage: isGivenNameError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: { type: TEXT, maxLength: 30 }
      },
      {
        key: "surname",
        placeholderPath: "clientForm.surnameField",
        value: surname,
        other: { hasOther: false },
        onChange: surnameOnChange,
        hasError: false,
        errorMessage: "",
        isRequired: false,
        shouldShow: true,
        fieldType: { type: TEXT, maxLength: 30 }
      },
      {
        key: "name",
        placeholderPath: "clientForm.nameField",
        value: name,
        other: { hasOther: false },
        onChange: nameOnChange,
        hasError: isNameError.hasError,
        errorMessage: isNameError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: { type: TEXT, maxLength: 30 }
      },
      {
        key: "name order",
        placeholderPath: "clientForm.nameOrder",
        value: nameOrder,
        other: { hasOther: false },
        onChange: nameOrderOnChange,
        hasError: false,
        errorMessage: "",
        isRequired: false,
        shouldShow: true,
        fieldType: { type: SELECT, optionsMapKey: "nameOrder" }
      },
      {
        key: "relationship",
        placeholderPath: "clientForm.relationshipField",
        value: relationship,
        onChange: relationshipOnChange,
        hasError: isRelationshipError.hasError,
        errorMessage: isRelationshipError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: { type: SELECT, optionsMapKey: "relationship" }
      },
      {
        key: "relationship other",
        placeholderPath: "clientForm.pleaseSpecifyField",
        value: relationshipOther,
        onChange: relationshipOtherOnChange,
        hasError: isRelationshipOtherError.hasError,
        errorMessage: isRelationshipOtherError.message,
        isRequired: true,
        shouldShow: relationship === "OTH",
        fieldType: { type: TEXT, maxLength: 30 }
      },
      {
        key: "mobile no",
        placeholderPath: "clientForm.mobileNoField",
        value: mobileNoA,
        other: { hasOther: false },
        onChange: newMobileNoA => {
          mobileNoAOnChange({
            newMobileNoA,
            prefixAData: prefixA
          });
        },
        hasError: isMobileNoAError.hasError,
        errorMessage: isMobileNoAError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: {
          type: MOBILE_NO,
          maxLength: 15,
          placeholderPath: "clientForm.mobileNoField",
          value: prefixA,
          onChange: prefixAOnChange,
          hasError: isPrefixAError.hasError,
          errorMessage: isPrefixAError.message,
          isRequired: false,
          optionsMapKey: "countryTelCode"
        }
      },
      {
        key: "ID document type",
        placeholderPath: "clientForm.IDTypeField",
        value: IDDocumentType,
        onChange: IDDocumentTypeOnChange,
        hasError: isIDDocumentTypeError.hasError,
        errorMessage: isIDDocumentTypeError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: { type: SELECT, optionsMapKey: "IDType" }
      },
      {
        key: "ID document type other",
        placeholderPath: "clientForm.IDTypeOtherField",
        value: IDDocumentTypeOther,
        onChange: IDDocumentTypeOtherOnChange,
        hasError: isIDDocumentTypeOtherError.hasError,
        errorMessage: isIDDocumentTypeOtherError.message,
        isRequired: true,
        shouldShow: IDDocumentType === "other",
        fieldType: { type: TEXT, maxLength: 25 }
      },
      {
        key: "id",
        placeholderPath: "clientForm.IDField",
        value: ID,
        other: { hasOther: false },
        onChange: IDOnChange,
        hasError: isIDError.hasError,
        errorMessage: isIDError.message,
        isRequired: true,
        shouldShow: true,
        fieldType: { type: TEXT, maxLength: 20 }
      }
    ];
  }
  /**
   * @description calc the width of field
   * @return {number} field width
   * */
  get fieldWidth() {
    const calcWidth =
      (this.state.formWidth - Theme.alignmentXXXL - Theme.alignmentXL * 2) / 2;

    return calcWidth >= 0 ? calcWidth : 0;
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { fieldList, fieldWidth } = this;
    const { textStore, optionsMap, photo, photoOnChange } = this.props;
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * @description text field render function
     * =========================================================================
     * -------------------------------------------------------------------------
     * basic params
     * -------------------------------------------------------------------------
     * @param {object} fieldData - field data
     * @param {string} fieldData.key - field component key
     * @param {string} fieldData.placeholderPath - field placeholder text store
     *   path
     * @param {string} fieldData.value - field value
     * @param {function} fieldData.onChange - field onChange function
     * @param {boolean} fieldData.hasError - indicate field has error or not
     * @param {string} fieldData.errorMessage - field error message
     * @param {boolean} fieldData.isRequired - indicate field is mandatory or
     *   not
     * @param {object} fieldData.fieldType - field field type
     * @param {number} index - field index
     * @param {string} language
     * -------------------------------------------------------------------------
     * additional params
     * -------------------------------------------------------------------------
     * @param {number} fieldData.fieldType.maxLength - field max length
     * =========================================================================
     * @return {element} field element
     * */
    const renderTextField = ({ fieldData, index, language }) => {
      const {
        key,
        placeholderPath,
        value,
        onChange,
        hasError,
        errorMessage,
        isRequired,
        fieldType
      } = fieldData;

      return (
        <EABTextField
          key={key}
          style={[
            index <= 1 ? styles.fieldFirst : styles.field,
            { width: fieldWidth }
          ]}
          value={value}
          placeholder={EABi18n({
            language,
            textStore,
            path: placeholderPath
          })}
          isError={hasError}
          hintText={errorMessage[language]}
          onChange={onChange}
          isRequired={isRequired}
          maxLength={fieldType.maxLength}
        />
      );
    };
    /**
     * @description select field render function
     * =========================================================================
     * -------------------------------------------------------------------------
     * basic params
     * -------------------------------------------------------------------------
     * @param {object} fieldData - field data
     * @param {string} fieldData.key - field component key
     * @param {string} fieldData.placeholderPath - field placeholder text store
     *   path
     * @param {string} fieldData.value - field value
     * @param {function} fieldData.onChange - field onChange function
     * @param {boolean} fieldData.hasError - indicate field has error or not
     * @param {string} fieldData.errorMessage - field error message
     * @param {boolean} fieldData.isRequired - indicate field is mandatory or
     *   not
     * @param {object} fieldData.fieldType - field field type
     * @param {number} index - field index
     * @param {string} language
     * -------------------------------------------------------------------------
     * additional params
     * -------------------------------------------------------------------------
     * @param {number} fieldData.fieldType.optionsMapKey - optionsMap object key
     * =========================================================================
     * @return {element} field element
     * */
    const renderSelectField = ({ fieldData, index, language }) => {
      const {
        key,
        value,
        placeholderPath,
        onChange,
        hasError,
        errorMessage,
        isRequired,
        fieldType
      } = fieldData;

      return (
        <SelectorField
          key={key}
          style={[
            index <= 1 ? styles.fieldFirst : styles.field,
            { width: fieldWidth }
          ]}
          value={value}
          placeholder={EABi18n({
            language,
            textStore,
            path: placeholderPath
          })}
          selectorType={FLAT_LIST}
          flatListOptions={{
            data: getOptionList({
              optionMap: optionsMap[fieldType.optionsMapKey],
              language
            }),
            renderItemOnPress: (itemKey, item) => {
              onChange(item.value);
            },
            selectedValue: value
          }}
          isError={hasError}
          hintText={_.isEmpty(errorMessage) ? "" : errorMessage[language]}
          isRequired={isRequired}
        />
      );
    };
    /**
     * @description render mobile no field and prefix field function
     * =========================================================================
     * -------------------------------------------------------------------------
     * basic params
     * -------------------------------------------------------------------------
     * @param {object} fieldData - field data
     * @param {string} fieldData.key - field component key
     * @param {string} fieldData.placeholderPath - field placeholder text store
     *   path
     * @param {string} fieldData.value - field value
     * @param {function} fieldData.onChange - field onChange function
     * @param {boolean} fieldData.hasError - indicate field has error or not
     * @param {string} fieldData.errorMessage - field error message
     * @param {boolean} fieldData.isRequired - indicate field is mandatory or
     *   not
     * @param {object} fieldData.fieldType - field field type
     * @param {number} index - field index
     * @param {string} language
     * -------------------------------------------------------------------------
     * additional params
     * -------------------------------------------------------------------------
     * @param {number} fieldData.fieldType.maxLength - field max length
     * @param {number} fieldData.fieldType.optionsMapKey - prefix optionsMap
     *   object key
     * @param {string} fieldData.fieldType.placeholderPath - prefix placeholder
     * @param {string} fieldData.fieldType.value - prefix value
     * @param {function} fieldData.fieldType.onChange - prefix onChange
     * @param {boolean} fieldData.fieldType.hasError - prefix error status
     * @param {string} fieldData.fieldType.errorMessage - prefix error message
     * @param {boolean} fieldData.fieldType.isRequired - prefix mandatory status
     * @param {string} fieldData.fieldType.optionsMapKey - prefix optionsMap key
     * =========================================================================
     * @return {element} field element
     * */
    const renderMobileNoField = ({ fieldData, index, language }) => {
      const {
        key,
        value,
        placeholderPath,
        onChange,
        hasError,
        errorMessage,
        isRequired,
        fieldType
      } = fieldData;
      return (
        <View
          key={key}
          style={[
            index <= 1 ? styles.fieldFirst : styles.field,
            styles.mobileNumberWrapper,
            { width: fieldWidth }
          ]}
        >
          <SelectorField
            style={styles.prefix}
            selectorType={SEARCH_BOX}
            value={fieldType.value}
            placeholder={EABi18n({
              language,
              textStore,
              path: fieldType.placeholderPath
            })}
            options={getOptionListForCountryCode({
              optionMap: optionsMap[fieldType.optionsMapKey],
              language
            })}
            onChange={option => {
              fieldType.onChange(option.value);
            }}
            isError={fieldType.hasError}
            hintText={fieldType.errorMessage[language]}
            isRequired={fieldType.isRequired}
          />
          <EABTextField
            style={styles.mobileNo}
            value={value}
            placeholder={EABi18n({
              language,
              textStore,
              path: placeholderPath
            })}
            isError={hasError}
            hintText={errorMessage[language]}
            onChange={onChange}
            isRequired={isRequired}
            maxLength={fieldType.maxLength}
          />
        </View>
      );
    };
    /**
     * @description render field by its type
     * @param {object} fieldData - field data
     * @param {boolean} fieldData.shouldShow - indicate field should show or not
     * @param {object} fieldData.fieldType - field field type data
     * @param {string} fieldData.fieldType.type - field field type
     * @param {number} index - field index
     * @param {string} language
     * @return {element} field element
     * */
    const fieldSelector = ({ fieldData, index, language }) => {
      if (fieldData.shouldShow) {
        switch (fieldData.fieldType.type) {
          case TEXT:
            return renderTextField({ fieldData, index, language });
          case SELECT:
            return renderSelectField({ fieldData, index, language });
          case MOBILE_NO:
            return renderMobileNoField({ fieldData, index, language });
          default:
            throw new Error(
              `unexpected firstField field type ${fieldData.fieldType.type}`
            );
        }
      }
      return null;
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <View
            style={styles.container}
            onLayout={event => {
              this.setState({
                formWidth: event.nativeEvent.layout.width
              });
            }}
          >
            {fieldWidth > 0
              ? [
                  <View key="photo" style={styles.photoPickerWrapper}>
                    <TouchableOpacity
                      onPress={() => {
                        photoPicker({ language, textStore, photoOnChange });
                      }}
                    >
                      <View style={styles.avatarWrapper}>
                        <Image
                          style={styles.avatar}
                          source={!_.isEmpty(photo) ? photo : avatar}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>,
                  fieldList.map((fieldData, index) =>
                    fieldSelector({ fieldData, index, language })
                  )
                ]
              : null}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ClientFormTrustedIndividual.propTypes = {
  /**
   * photo
   * */
  photo: REDUCER_TYPE_CHECK[CLIENT_FORM].photo.isRequired,
  photoOnChange: PropTypes.func.isRequired,
  /**
   * relationship
   * */
  relationship: REDUCER_TYPE_CHECK[CLIENT_FORM].relationship.isRequired,
  relationshipOnChange: PropTypes.func.isRequired,
  isRelationshipError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isRelationshipError.isRequired,
  relationshipOther:
    REDUCER_TYPE_CHECK[CLIENT_FORM].relationshipOther.isRequired,
  relationshipOtherOnChange: PropTypes.func.isRequired,
  isRelationshipOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isRelationshipOtherError.isRequired,
  /**
   * givenName
   * */
  givenName: REDUCER_TYPE_CHECK[CLIENT_FORM].givenName.isRequired,
  givenNameOnChange: PropTypes.func.isRequired,
  isGivenNameError: REDUCER_TYPE_CHECK[CLIENT_FORM].isGivenNameError.isRequired,
  /**
   * surname
   * */
  surname: REDUCER_TYPE_CHECK[CLIENT_FORM].surname.isRequired,
  surnameOnChange: PropTypes.func.isRequired,
  /**
   * name
   * */
  name: REDUCER_TYPE_CHECK[CLIENT_FORM].name.isRequired,
  nameOnChange: PropTypes.func.isRequired,
  isNameError: REDUCER_TYPE_CHECK[CLIENT_FORM].isNameError.isRequired,
  /**
   * nameOrder
   * */
  nameOrder: REDUCER_TYPE_CHECK[CLIENT_FORM].nameOrder.isRequired,
  nameOrderOnChange: PropTypes.func.isRequired,
  /**
   * IDDocumentType
   * */
  IDDocumentType: REDUCER_TYPE_CHECK[CLIENT_FORM].IDDocumentType.isRequired,
  IDDocumentTypeOnChange: PropTypes.func.isRequired,
  isIDDocumentTypeError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isIDDocumentTypeError.isRequired,
  IDDocumentTypeOther:
    REDUCER_TYPE_CHECK[CLIENT_FORM].IDDocumentTypeOther.isRequired,
  IDDocumentTypeOtherOnChange: PropTypes.func.isRequired,
  isIDDocumentTypeOtherError:
    REDUCER_TYPE_CHECK[CLIENT_FORM].isIDDocumentTypeOtherError.isRequired,
  /**
   * ID
   * */
  ID: REDUCER_TYPE_CHECK[CLIENT_FORM].ID.isRequired,
  IDOnChange: PropTypes.func.isRequired,
  isIDError: REDUCER_TYPE_CHECK[CLIENT_FORM].isIDError.isRequired,
  /**
   * prefixA
   * */
  prefixA: REDUCER_TYPE_CHECK[CLIENT_FORM].prefixA.isRequired,
  prefixAOnChange: PropTypes.func.isRequired,
  isPrefixAError: REDUCER_TYPE_CHECK[CLIENT_FORM].isPrefixAError.isRequired,
  /**
   * mobileNoA
   * */
  mobileNoA: REDUCER_TYPE_CHECK[CLIENT_FORM].mobileNoA.isRequired,
  mobileNoAOnChange: PropTypes.func.isRequired,
  isMobileNoAError: REDUCER_TYPE_CHECK[CLIENT_FORM].isMobileNoAError.isRequired,
  /**
   * other reducers
   * */
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired
};
