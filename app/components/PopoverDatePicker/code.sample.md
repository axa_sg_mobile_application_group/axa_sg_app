### PopoverDatePicker sample code snippets

```javascript
class Sample extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      popoverVisible: false,
      targetNode: null,
      chosenDate: new Date(),
    }
    this.inputRef = React.createRef();
  }

  _onPresent(event) {
    this.setState({
      popoverVisible: true,
      targetNode: findNodeHandle(this.inputRef.current),
    })
  }

  onShow() {
  }

  onHide() {
    this.setState({
      popoverVisible: false,
    })
  }

  renderWrapperPopover() {
    return (
      <PopoverDatePicker
        popoverOptions={{
          sourceView: this.state.targetNode,
              onShow: this.onShow.bind(this),
              onHide: this.onHide.bind(this),
          preferredContentSize: [400, 200],
          permittedArrowDirections: [0],
        }}
        datePickerIOSOptions={{
          date: this.state.chosenDate,
          onDateChange: chosenDate => this.setState({ chosenDate }),
        }}
      />
    )
  }

  render() {
    return (
      <View>
        <TouchableOpacity ref={this.inputRef} onPress={this._onPresent.bind(this)}>
          <Text>Popover</Text>
        </TouchableOpacity>
        {this.state.popoverVisible && this.renderWrapperPopover()}
      </View>
    )
  }
}
```