import { StyleSheet } from "react-native";
import Theme from "../../theme";

const HUDWrapper = {
  justifyContent: "center",
  alignItems: "center",
  borderRadius: Theme.radiusM,
  backgroundColor: Theme.snackbarBg,
  width: 82,
  height: 82
};

export default StyleSheet.create({
  HUDContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  HUDWrapperBig: {
    ...HUDWrapper,
    width: "auto",
    height: "auto",
    padding: Theme.alignmentXL
  },
  HUDWrapper,
  HUD: {
    position: "relative",
    top: 2,
    left: 2
  },
  message: {
    ...Theme.fieldTextOrHelperTextSelected,
    marginTop: Theme.alignmentL,
    maxWidth: 200
  }
});
