import React, { PureComponent } from "react";
import { Image, ScrollView, NativeModules } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { ContextConsumer } from "../../context";
import EABHUD from "../../containers/EABHUD";
import styles from "./styles";
import Theme from "../../theme";

/**
 * <ImageViewer />
 * @description handler the pdf layout
 * @param {string} title - the tag title
 * @param {string} textColor - title text color
 * @param {string} backgroundColor - tag background color
 * */
export default class ImageViewer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      image: null
    };
    this.signDocDialogManager = NativeModules.SignDocDialogManager;
  }

  getImage(signDocImageSource, imageSource, hideImageLoading, isImageLoading) {
    const { image } = this.state;
    if (signDocImageSource) {
      this.generateSignDoc(signDocImageSource);
    }
    if (image) {
      return (
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContent}
        >
          {image.map(base64 => (
            <Image
              key={base64}
              style={styles.signDocImage}
              source={{
                uri: `data:image/png;base64,${base64}`
              }}
              onLoad={isImageLoading ? hideImageLoading : () => {}}
            />
          ))}
        </ScrollView>
      );
    }
    if (imageSource) {
      return (
        <Image
          style={styles.image}
          source={{ uri: imageSource }}
          onLoad={isImageLoading ? hideImageLoading : () => {}}
        />
      );
    }
    return null;
  }

  generateSignDoc(pdfBase64) {
    const { signDocDialogManager } = this;
    const imageArr = [];
    signDocDialogManager.init([], pdfBase64, (err, result) => {
      if (result.images) {
        result.images.forEach(imageBase64 => {
          imageArr.push(imageBase64);
        });
      }
      this.setState({
        image: imageArr.length ? imageArr : null
      });
    });
  }

  render() {
    return (
      <ContextConsumer>
        {({
          isImageLoading,
          hideImageLoading,
          imageSource,
          signDocImageSource
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isImageLoading} />
            {this.getImage(
              signDocImageSource,
              imageSource,
              hideImageLoading,
              isImageLoading
            )}
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

ImageViewer.propTypes = {};
