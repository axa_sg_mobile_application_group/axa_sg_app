import { requireNativeComponent } from "react-native";

const SignDocDialogView = requireNativeComponent("SignDocDialog");
export default SignDocDialogView;
