import { StyleSheet } from "react-native";
import Theme from "../../theme";

const field = {
  flex: 1
};
const fieldWrapper = {
  ...field,
  flexDirection: "row"
};
const fullWidthField = {
  marginBottom: Theme.alignmentXS,
  width: "100%"
};

export default StyleSheet.create({
  scrollView: {
    backgroundColor: Theme.paleGrey,
    width: "100%",
    height: "100%"
  },
  residentialAddressTitle: {
    ...Theme.captionBrand,
    marginVertical: Theme.alignmentL
  },
  scrollViewContent: {
    alignItems: "flex-start",
    paddingVertical: Theme.alignmentXL,
    paddingHorizontal: 72
  },
  formRow: {
    flexDirection: "row",
    marginBottom: Theme.alignmentXS,
    width: "100%"
  },
  field,
  fieldFirst: {
    ...field,
    marginRight: Theme.alignmentXXXL
  },
  fieldWrapper,
  fieldWrapperFirst: {
    ...fieldWrapper,
    marginRight: Theme.alignmentXXXL
  },
  prefixField: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXL,
    width: 56
  },
  mobileField: {
    flex: 1
  },
  fullWidthField,
  fullWidthFieldLast: {
    ...fullWidthField,
    marginBottom: 0
  },
  countryCodeField: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXL,
    width: 120
  }
});
