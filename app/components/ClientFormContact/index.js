import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import {
  KeyboardAvoidingView,
  ScrollView,
  View,
  Text,
  Alert
} from "react-native";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import EABi18n from "../../utilities/EABi18n";
import EABTextField from "../EABTextField";
import styles from "./styles";
import SelectorField from "../SelectorField";
import { FLAT_LIST, SEARCH_BOX } from "../SelectorField/constants";
import {
  getOptionList,
  isTriggerExist,
  getOptionListForCountryCode
} from "../../utilities/getOptionList";

const { CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * <ClientFormContact />
 * @description client form profile part. all props should from reducer
 * */
export default class ClientFormContact extends PureComponent {
  constructor() {
    super();

    this.state = {};
    this.cityField = this.cityField.bind(this);
  }

  // =========================================================================
  // function
  // =========================================================================
  /**
   * cityField
   * @description special handling for city field
   * @param {string} selectedValue - value from "Country of Residence"
   * @param {object} language - language type, must come from LANGUAGE_TYPES
   * @return {null|element} render object
   * */
  cityField(selectedValue, language) {
    const {
      cityState,
      textStore,
      optionsMap,
      cityStateOnChange,
      countryOfResidence
    } = this.props;
    const field = isTriggerExist(selectedValue, optionsMap.residency);
    if (field.inputType) field.inputType = field.inputType.toLowerCase();
    switch (field.inputType) {
      case "selector": {
        return (
          <SelectorField
            style={styles.fieldFirst}
            value={cityState}
            placeholder={EABi18n({
              language,
              textStore,
              path: "clientForm.cityStateField"
            })}
            onChange={newCity => {
              cityStateOnChange(newCity.value);
            }}
            selectorType={FLAT_LIST}
            editable={field.editable === false ? field.editable : true}
            flatListOptions={{
              renderItemOnPress: (itemKey, item) => {
                cityStateOnChange(item.value);
              },
              extraData: this.state,
              keyExtractor: item => item.key,
              selectedValue: null,
              data: getOptionList(
                {
                  optionMap: optionsMap.city,
                  language
                },
                countryOfResidence
              )
            }}
          />
        );
      }
      case "hidden":
        return null;
      default: {
        return (
          <EABTextField
            testID="txtCityState"
            style={styles.fieldFirst}
            value={cityState}
            editable={field.editable === false ? field.editable : true}
            placeholder={EABi18n({
              language,
              textStore,
              path: "clientForm.cityStateField"
            })}
            onChange={cityStateOnChange}
            maxLength={12}
          />
        );
      }
    }
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      isPrefixAError,
      prefixA,
      prefixAOnChange,
      isMobileNoAError,
      mobileNoA,
      mobileNoAOnChange,
      prefixB,
      prefixBOnChange,
      mobileNoB,
      mobileNoBOnChange,
      email,
      isEmailError,
      emailOnChange,
      countryOfResidence,
      country,
      postal,
      postalOnChange,
      blockHouse,
      blockHouseOnChange,
      streetRoad,
      streetRoadOnChange,
      unit,
      unitOnChange,
      buildingEstate,
      buildingEstateOnChange,
      textStore,
      optionsMap,
      postalOnBlur
    } = this.props;
    const cityField = language => this.cityField(country, language);
    return (
      <ContextConsumer>
        {({ language }) => (
          <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={40}>
            <ScrollView
              testID="scrollViewClient"
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              <View style={styles.formRow}>
                <View style={styles.fieldWrapperFirst}>
                  <SelectorField
                    testID="selectMobileNoArea"
                    style={styles.countryCodeField}
                    selectorType={SEARCH_BOX}
                    value={prefixA}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.mobileNoField"
                    })}
                    options={getOptionListForCountryCode({
                      optionMap: optionsMap.countryTelCode,
                      language
                    })}
                    onChange={option => {
                      prefixAOnChange(option.value);
                    }}
                    isError={isPrefixAError.hasError}
                    hintText={isPrefixAError.message[language]}
                  />
                  <EABTextField
                    testID="txtMobileNo"
                    style={styles.mobileField}
                    value={mobileNoA}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.mobileNoField"
                    })}
                    onChange={newMobileNoA => {
                      mobileNoAOnChange({
                        newMobileNoA,
                        prefixAData: prefixA
                      });
                    }}
                    keyboardType="numeric"
                    isError={isMobileNoAError.hasError}
                    hintText={isMobileNoAError.message[language]}
                    maxLength={15 - prefixA.length}
                  />
                </View>
                <View style={styles.fieldWrapper}>
                  <SelectorField
                    testID="selectOtherMobileNoArea"
                    style={styles.countryCodeField}
                    selectorType={SEARCH_BOX}
                    value={prefixB}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.otherNoField"
                    })}
                    options={getOptionListForCountryCode({
                      optionMap: optionsMap.countryTelCode,
                      language
                    })}
                    onChange={option => {
                      prefixBOnChange(option.value);
                    }}
                  />
                  <EABTextField
                    testID="txtOtherMobileNo"
                    style={styles.mobileField}
                    value={mobileNoB}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path: "clientForm.otherNoField"
                    })}
                    onChange={newMobileNoB => {
                      mobileNoBOnChange({
                        newMobileNoB,
                        prefixBData: prefixB
                      });
                    }}
                    keyboardType="numeric"
                    maxLength={15 - prefixB.length}
                  />
                </View>
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtEmailField"
                  style={styles.fieldFirst}
                  value={email}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.emailField"
                  })}
                  isError={isEmailError.hasError}
                  hintText={isEmailError.message[language]}
                  onChange={emailOnChange}
                  keyboardType="email-address"
                  maxLength={50}
                />
              </View>
              <Text style={styles.residentialAddressTitle}>
                {EABi18n({
                  language,
                  textStore,
                  path: "clientForm.residentialAddressTitle"
                }).toUpperCase()}
              </Text>
              <View style={styles.formRow}>
                <SelectorField
                  style={styles.fieldFirst}
                  value={country}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.countryField"
                  })}
                  // onChange={newCountry => {
                  //   countryOnChange({
                  //     newCountry,
                  //     countryOfResidenceData: countryOfResidence
                  //   });
                  // }}
                  selectorType={FLAT_LIST}
                  editable={!countryOfResidence}
                  flatListOptions={{
                    // renderItemOnPress: (itemKey, item) => {
                    //   countryOnChange({
                    //     newCountry: item.value,
                    //     countryOfResidenceData: countryOfResidence
                    //   });
                    // },
                    extraData: this.state,
                    keyExtractor: item => item.key,
                    selectedValue: null,
                    data: getOptionList({
                      optionMap: optionsMap.residency,
                      language
                    })
                  }}
                />
                {cityField(language)}
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtPostalField"
                  style={styles.fieldFirst}
                  value={postal}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.postalField"
                  })}
                  onBlur={newPostal => {
                    postalOnBlur({
                      newPostal,
                      alertFunction: () =>
                        Alert.alert(
                          null,
                          EABi18n({
                            language,
                            textStore,
                            path: "clientForm.error.postalCodeNotFound"
                          }),
                          [
                            {
                              text: EABi18n({
                                language,
                                textStore,
                                path: "button.ok"
                              }),
                              onPress: () => {}
                            }
                          ]
                        )
                    });
                  }}
                  onChange={postalOnChange}
                  keyboardType="numeric"
                  maxLength={10}
                />
                <View style={styles.field}>{/* for alignment */}</View>
              </View>
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtBlockHouse"
                  style={styles.fieldFirst}
                  value={blockHouse}
                  maxLength={12}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.blockHouseField"
                  })}
                  onChange={blockHouseOnChange}
                />
                <View style={styles.field}>{/* for alignment */}</View>
              </View>
              <EABTextField
                testID="txtStreetRoad"
                style={styles.fullWidthField}
                value={streetRoad}
                maxLength={24}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "clientForm.streetRoadField"
                })}
                onChange={streetRoadOnChange}
              />
              <View style={styles.formRow}>
                <EABTextField
                  testID="txtUnit"
                  style={styles.fieldFirst}
                  value={unit}
                  maxLength={13}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "clientForm.unitField"
                  })}
                  onChange={newUnit => {
                    unitOnChange({
                      newUnit,
                      prefix: EABi18n({
                        language,
                        textStore,
                        path: "clientForm.unit"
                      })
                    });
                  }}
                  keyboardType="numeric"
                />
                <View style={styles.field}>{/* for alignment */}</View>
              </View>
              <EABTextField
                testID="txtBuildingEstate"
                style={styles.fullWidthField}
                value={buildingEstate}
                maxLength={40}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "clientForm.buildingEstateField"
                })}
                onChange={buildingEstateOnChange}
              />
            </ScrollView>
          </KeyboardAvoidingView>
        )}
      </ContextConsumer>
    );
  }
}

const typeCheck = REDUCER_TYPE_CHECK[CLIENT_FORM];

ClientFormContact.propTypes = {
  prefixA: typeCheck.prefixA.isRequired,
  mobileNoA: typeCheck.mobileNoA.isRequired,
  prefixB: typeCheck.prefixB.isRequired,
  mobileNoB: typeCheck.mobileNoB.isRequired,
  email: typeCheck.email.isRequired,
  isEmailError: typeCheck.isEmailError.isRequired,
  countryOfResidence: typeCheck.countryOfResidence.isRequired,
  country: typeCheck.country.isRequired,
  cityState: typeCheck.cityState.isRequired,
  postal: typeCheck.postal.isRequired,
  blockHouse: typeCheck.blockHouse.isRequired,
  streetRoad: typeCheck.streetRoad.isRequired,
  unit: typeCheck.unit.isRequired,
  buildingEstate: typeCheck.buildingEstate.isRequired,
  prefixAOnChange: PropTypes.func.isRequired,
  mobileNoAOnChange: PropTypes.func.isRequired,
  prefixBOnChange: PropTypes.func.isRequired,
  mobileNoBOnChange: PropTypes.func.isRequired,
  emailOnChange: PropTypes.func.isRequired,
  cityStateOnChange: PropTypes.func.isRequired,
  postalOnChange: PropTypes.func.isRequired,
  postalOnBlur: PropTypes.func.isRequired,
  blockHouseOnChange: PropTypes.func.isRequired,
  streetRoadOnChange: PropTypes.func.isRequired,
  unitOnChange: PropTypes.func.isRequired,
  buildingEstateOnChange: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  isPrefixAError: REDUCER_TYPE_CHECK[CLIENT_FORM].isPrefixAError.isRequired,
  isMobileNoAError: REDUCER_TYPE_CHECK[CLIENT_FORM].isMobileNoAError.isRequired
};
