### Selector sample code snippets (To be placed in Playground.js)

```javascript
import React, { Component } from "react";
import { View,   Button} from "react-native";
import { connect } from "react-redux";
import SelectorField from "../components/SelectorField";
import { PICKER } from "../components/SelectorField/constants";

class Applications extends Component {
  constructor() {
    super();

    this.state = {
      selectedKey : 'a2'
    };
  }

  onShow() {
    // TODO
  }

  onHide() {
    // TODO
  }

  handleButton = () => {
    this.setState({selectedKey : 'js'});
  }

  render() {
    return (
      <View
        style={{
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <View style={{ width: 300 }}>

          <SelectorField
            value=""
            placeholder="Placeholder"
            isRequired
            isError={false}
            editable
            hintText="Error message."
            checkBoxValue={this.state.checkBoxValue}
            selectorType={PICKER}
            popoverOptions={{
              onShow: this.onShow.bind(this),
              onHide: this.onHide.bind(this),
              preferredContentSize: [300, 200],
              permittedArrowDirections: [0]
            }}
            pickerOptions={{
              items: [
                { label: "Java", value: "java" },
                { label: "JavaScript", value: "js" },
                { label: "A1", value: "a1" },
                { label: "A2", value: "a2" }
              ],
              onValueChange: itemValue => {
                this.setState({
                  selectedValue: itemValue
                });
              },
              selectedValue: this.state.selectedValue
            }}
            flatListOptions={{
              renderItemOnPress: label => {
                this.setState({
                  selectedValue: label
                });
              },
              selectedValue: this.state.selectedValue,
              extraData: this.state,
              keyExtractor: item => item.key,
              data: [
                { key: 'a', label: 'A' },
                { key: 'b', label: 'B' },
                { key: 'c', label: 'C' },
                { key: 'd', label: 'D' },
                { key: 'e', label: 'E' },
                { key: 'f', label: 'F' },
                { key: 'g', label: 'G' },
                { key: 'h', label: 'H' },
              ],
            }}
            datePickerIOSOptions={{
              onDateChange: itemValue => {
                this.setState({
                  date: itemValue
                });
              },
              date: this.state.date,
              mode: "date"
            }}
          />
        </View>

        <Button
          onPress={this.handleButton}
          title="Press"
          color="#841584"
        />
        
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Applications);


```