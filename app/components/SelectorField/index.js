import PropTypes from "prop-types";
import React, { Component } from "react";
import moment from "moment";
import {
  Animated,
  Text,
  View,
  Image,
  TouchableOpacity,
  findNodeHandle
} from "react-native";
import * as _ from "lodash";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import EABSearchBox from "../EABSearchBox";
import styles from "./styles";
import icDropdown from "../../assets/images/icDropdown.png";
import PopoverPicker from "../PopoverPicker";
import PopoverDatePicker from "../../containers/PopoverDatePicker";
import PopoverFlatList from "../PopoverFlatList";
import { PICKER, DATE_PICKER, FLAT_LIST, SEARCH_BOX } from "./constants";

/**
 * <SelectorField />
 * @description selector field component
 * @requires icDropdown - @/assets/images/icDropdown.png
 * @param {string} placeholder - input field label
 * @param {boolean} [editable=true] - if false, field is not editable
 * @param {boolean} [isRequired=false] - indicates whether the text field is
 *   required or not
 * @param {string=} hintText - error message
 * @param {boolean=} isError - indicates whether the text field is error or not
 * @param {object|number=} style - selector field container style.
 * @param {object} [popoverOptions={}] - props of Popover's object
 * @param {object} [datePickerIOSOptions={date: new Date(), onDateChange: () => {}}] - props of
 * @param {string} selectorType - pick the type of selector PICKER or DATE_PICKER or FLAT_LIST
 * @param {object} [flatListOptions={}] - props of FlatList object
 * */
export default class SelectorField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      labelAnimateStatus: new Animated.Value(0),
      labelHeight: 0,
      labelWidth: 0,
      popoverVisible: false,
      fieldPressed: false,
      fieldSelected: false
    };
    this.onFieldPress = this.onFieldPress.bind(this);
    this.popoverTarget = React.createRef();
  }

  componentDidMount() {
    this.setDisplayText();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setDisplayText();
    }
  }

  onShow() {
    if (
      this.props.popoverOptions != null &&
      this.props.popoverOptions.onShow != null
    ) {
      this.props.popoverOptions.onShow();
    }
  }

  onHide() {
    if (
      this.props.popoverOptions != null &&
      this.props.popoverOptions.onHide != null
    ) {
      this.props.popoverOptions.onHide();
    }
    this.setState({
      isFocus: false,
      popoverVisible: false,
      targetNode: 0,
      fieldSelected: true
    });
  }

  onFieldPress() {
    const { editable } = this.props;

    if (editable) {
      this.setState({
        isFocus: true,
        popoverVisible: true,
        targetNode: findNodeHandle(this.popoverTarget.current),
        fieldPressed: true
      });
    }
  }

  get popoverOptions() {
    return Object.assign({}, this.props.popoverOptions, {
      sourceView: this.state.targetNode,
      onShow: this.onShow.bind(this),
      onHide: this.onHide.bind(this)
    });
  }

  get pickerOptions() {
    const pickerOptions = Object.assign({}, this.props.pickerOptions);

    pickerOptions.onValueChange = itemValue => {
      if (this.props.pickerOptions && this.props.pickerOptions.onValueChange) {
        this.props.pickerOptions.onValueChange(itemValue);
      }
    };
    return pickerOptions;
  }

  get datePickerIOSOptions() {
    const datePickerIOSOptions = Object.assign(
      {},
      this.props.datePickerIOSOptions
    );
    datePickerIOSOptions.onDateChange = chosenDate => {
      if (
        this.props.datePickerIOSOptions &&
        this.props.datePickerIOSOptions.onDateChange
      ) {
        this.props.datePickerIOSOptions.onDateChange(new Date(chosenDate));
      }
    };
    return datePickerIOSOptions;
  }

  setDisplayText() {
    this.setState({
      labelAnimateStatus: new Animated.Value(
        this.props.value !== null && this.props.value !== "" ? 1 : 0
      )
    });
  }

  showPopover() {
    const {
      value,
      onChange,
      onHide,
      placeholder,
      options,
      selectorType
    } = this.props;
    const { popoverVisible } = this.state;

    switch (selectorType) {
      case PICKER:
        return (
          this.state.popoverVisible &&
          this.showPopoverPicker(this.popoverOptions)
        );
      case DATE_PICKER:
        return (
          this.state.popoverVisible &&
          this.showPopoverDatePicker(this.popoverOptions)
        );
      case FLAT_LIST:
        return (
          this.state.popoverVisible &&
          this.showPopoverFlatListPicker(this.popoverOptions)
        );
      case SEARCH_BOX:
        return (
          <EABSearchBox
            testID={this.props.testID}
            options={options}
            isOpen={popoverVisible}
            defaultSearchText={value}
            onChange={onChange}
            onClose={() => {
              this.setState(
                {
                  popoverVisible: false,
                  isFocus: false
                },
                onHide
              );
            }}
            placeholder={placeholder}
          />
        );
      default:
        throw new Error(
          "The selectorType must come from the constant 'POPOVER_TYPES'"
        );
    }
  }

  showPopoverPicker(popoverOptions) {
    return (
      <PopoverPicker
        testID={this.props.testID}
        popoverOptions={popoverOptions}
        pickerOptions={this.pickerOptions}
      />
    );
  }

  showPopoverDatePicker(popoverOptions) {
    return (
      <PopoverDatePicker
        testID={this.props.testID}
        popoverOptions={popoverOptions}
        datePickerIOSOptions={this.datePickerIOSOptions}
      />
    );
  }

  // keepPrevData: keep the previous selected data, and stored into an object item.prevData with index and value
  showPopoverFlatListPicker(popoverOptions) {
    const onRenderItemOnPress = (itemKey, item) => {
      if (this.props.flatListOptions) {
        if (this.props.flatListOptions.renderItemOnPress) {
          this.onHide();

          setTimeout(() => {
            if (this.props.flatListOptions.keepPrevData) {
              item = Object.assign({}, item, {
                prevData: {
                  index: this.props.index,
                  value: this.props.value
                }
              });
            }
            this.props.flatListOptions.renderItemOnPress(itemKey, item);
          }, 500);
        }
      }
    };

    if (this.props.flatListOptions.data !== undefined) {
      if (
        this.props.flatListOptions.data.length >= 2 &&
        this.props.flatListOptions.data.length <= 8
      ) {
        popoverOptions.preferredContentSize = [
          512,
          50 * this.props.flatListOptions.data.length
        ];
      } else if (this.props.flatListOptions.data.length > 8) {
        popoverOptions.preferredContentSize = [
          512,
          50 * 8 + Theme.bodyPrimary.lineHeight
        ];
      }
    }

    return (
      <PopoverFlatList
        testID={this.props.testID}
        popoverOptions={popoverOptions}
        flatListOptions={this.props.flatListOptions}
        renderItemOnPress={onRenderItemOnPress}
      />
    );
  }

  renderDisplayText() {
    const { options, value, dateFormat } = this.props;
    let displayText = "";
    switch (this.props.selectorType) {
      case PICKER:
        if (this.props.pickerOptions) {
          if (this.props.value !== "" && this.props.value !== null) {
            const selectedItem = this.props.pickerOptions.items.find(
              item => this.props.value === item.value
            );
            if (selectedItem) {
              displayText = selectedItem.label;
            } else {
              displayText = this.props.value;
            }
          }
        }
        break;
      case DATE_PICKER:
        if (this.props.value) {
          displayText = moment(this.props.datePickerIOSOptions.date).format(
            dateFormat ? _.toUpper(dateFormat) : "DD/MM/YYYY"
          );
        }
        break;
      case FLAT_LIST:
        if (this.props.flatListOptions) {
          if (this.props.value !== "" && this.props.value !== null) {
            const selectedItem = this.props.flatListOptions.data.find(
              item => this.props.value === item.value
            );
            if (selectedItem) {
              if (selectedItem.labelOnSelecting) {
                displayText = selectedItem.labelOnSelecting;
              } else {
                displayText = selectedItem.label;
              }
            } else {
              displayText = this.props.value;
            }
          }
        }
        break;
      case SEARCH_BOX: {
        const selectedOption = options.find(option => option.value === value);
        displayText = selectedOption ? selectedOption.label : value;
        break;
      }
      default:
        throw new Error(
          "The selectorType must come from the constant 'POPOVER_TYPES'"
        );
    }

    return displayText;
  }

  render() {
    /**
     * render variables
     * */
    const reduceSize = 0.75;
    let textInputStyle = styles.input;
    let placeholderStyle;
    /**
     * render functions
     * */
    const displayHintText = () => {
      switch (true) {
        case this.props.isError:
          return (
            <Text style={Theme.fieldTextOrHelperTextNegative}>
              {this.props.hintText}
            </Text>
          );
        case this.props.isRequired &&
          this.props.value === null &&
          !this.state.fieldPressed:
          return (
            <Text style={Theme.fieldTextOrHelperTextNegative}>Required</Text>
          );
        default:
          return null;
      }
    };

    if (this.state.isFocus) {
      textInputStyle = styles.inputFocus;
    } else if (this.props.editable) {
      if (this.props.isError) {
        textInputStyle = styles.inputError;
      } else {
        textInputStyle = styles.input;
      }
    } else {
      textInputStyle = styles.inputNonEditable;
    }

    if (this.props.editable) {
      if (this.state.isFocus) {
        if (this.props.placeholderFloat) {
          placeholderStyle = styles.labelTextFocusFloat;
        } else {
          placeholderStyle = styles.labelTextFocus;
        }
      } else if (this.props.placeholderFloat) {
        placeholderStyle = styles.labelTextFloat;
      } else {
        placeholderStyle = styles.labelText;
      }

      if (this.state.fieldSelected && !this.state.popoverVisible) {
        if (this.props.placeholderFloat) {
          placeholderStyle = styles.labelTextFloat;
        } else {
          placeholderStyle = styles.labelText;
        }
      }
    } else if (this.props.placeholderFloat) {
      placeholderStyle = styles.labelNonEditableFloat;
    } else {
      placeholderStyle = styles.labelNonEditable;
    }

    return (
      <View style={[styles.wrapper, getStyleArray(this.props.style)]}>
        <Animated.View
          style={[
            styles.label,
            {
              transform: [
                {
                  translateY: this.state.labelAnimateStatus.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                      24,
                      -(this.state.labelHeight * ((1 - reduceSize) / 2))
                    ]
                  })
                },
                {
                  translateX: this.state.labelAnimateStatus.interpolate({
                    inputRange: [0, 1],
                    outputRange: [
                      0,
                      -(this.state.labelWidth * ((1 - reduceSize) / 2))
                    ]
                  })
                },
                {
                  scale: this.state.labelAnimateStatus.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, reduceSize]
                  })
                }
              ]
            }
          ]}
          onLayout={event => {
            this.setState({
              labelWidth: event.nativeEvent.layout.width,
              labelHeight: event.nativeEvent.layout.height
            });
          }}
        >
          <Text style={placeholderStyle}>
            {this.props.placeholder}
            {this.props.isRequired && this.props.placeholder ? (
              <Text style={styles.requiredHint}> *</Text>
            ) : null}
          </Text>
        </Animated.View>

        <TouchableOpacity
          testID={this.props.testID}
          activeOpacity={1}
          onPress={this.onFieldPress}
          style={textInputStyle}
        >
          <View>
            {this.props.value ? (
              <Text
                style={
                  this.props.editable
                    ? Theme.bodyPrimary
                    : Theme.bodyPlaceholder
                }
                numberOfLines={1}
              >
                {this.renderDisplayText()}
              </Text>
            ) : null}
            {this.props.editable ? (
              <Image
                ref={this.popoverTarget}
                style={
                  this.props.value
                    ? styles.dropdownImageAlign
                    : styles.dropdownImage
                }
                source={icDropdown}
              />
            ) : null}
            {this.props.isRequired && !this.props.placeholder ? (
              <Text style={styles.requiredHintNoPlaceholder}>*</Text>
            ) : null}
          </View>
        </TouchableOpacity>
        {this.showPopover()}
        {displayHintText()}
      </View>
    );
  }
}

SelectorField.propTypes = {
  editable: PropTypes.bool,
  placeholderFloat: PropTypes.bool,
  placeholder: PropTypes.string.isRequired,
  hintText: PropTypes.string,
  isRequired: PropTypes.bool,
  isError: PropTypes.bool,
  selectorType: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  popoverOptions: PropTypes.oneOfType([PropTypes.object]),
  pickerOptions: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired
      })
    )
  ]),
  datePickerIOSOptions: PropTypes.oneOfType([PropTypes.object]),
  flatListOptions: PropTypes.oneOfType([PropTypes.object]),
  value: PropTypes.string,
  dateFormat: PropTypes.string,
  index: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired
    })
  ),
  onChange: PropTypes.func,
  onHide: PropTypes.func,
  testID: PropTypes.string
};
SelectorField.defaultProps = {
  editable: true,
  placeholderFloat: false,
  isRequired: false,
  hintText: "",
  isError: false,
  style: {},
  popoverOptions: {},
  selectorType: DATE_PICKER,
  pickerOptions: {},
  datePickerIOSOptions: {
    date: new Date(),
    onDateChange: () => {},
    mode: "date"
  },
  flatListOptions: {},
  value: "",
  dateFormat: "",
  index: "",
  options: [],
  onChange: () => {},
  onHide: () => {},
  testID: ""
};
