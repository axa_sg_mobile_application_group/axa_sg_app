import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: Theme.alignmentXS,
    borderRadius: Theme.radiusXS,
    height: 22,
    minWidth: 64
  },
  title: Theme.tagLabel
});
