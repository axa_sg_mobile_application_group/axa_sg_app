import PropTypes from "prop-types";
import React from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity
} from "react-native";
import Theme from "../../../theme";
import Popover from "../../Popover";
import PopoverBase from "../../Popover/PopoverBase";
import styles from "./styles";

/**
 * <PopoverFlatList />
 * @description ios popover selector component
 * @param {array} flatListOptions - props of FlatList's object
 * @param {function} renderItemOnPress - OnPress function if use default renderItem for
 *   item rendering
 * */
export default class PopoverFlatList extends PopoverBase {
  static ItemSeparatorComponent() {
    return <View style={styles.itemSeparatorStyle} />;
  }

  static renderItem(item, onPress) {
    const onItemPress = onPress || (() => {});
    return (
      <TouchableOpacity
        testID={`${item.testID}__${item.label}`}
        onPress={() => {
          onItemPress(item.key, item);
        }}
      >
        <View style={styles.itemWrap}>
          <Text testID={item.label} style={Theme.bodyPrimary} numberOfLines={2}>
            {item.label}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  get flatListStyle() {
    if (this.props.flatListOptions != null) {
      return Object.assign(
        {
          height: this.contentHeight,
          width: this.contentWidth
        },
        StyleSheet.flatten(this.props.flatListOptions.style)
      );
    }
    return null;
  }

  get otherFlatListOptions() {
    let otherFlatListOptions = null;
    if (this.props.flatListOptions != null) {
      otherFlatListOptions = Object.keys(this.props.flatListOptions).reduce(
        (result, key) => {
          const reduceResult = result;
          if (key !== "style") {
            reduceResult[key] = this.props.flatListOptions[key];
          }
          return reduceResult;
        },
        {}
      );
      otherFlatListOptions = Object.assign(
        {},
        {
          ItemSeparatorComponent: PopoverFlatList.ItemSeparatorComponent,
          renderItem: item => {
            if (item != null && item.item != null) {
              item.item.testID = this.props.testID;
              return PopoverFlatList.renderItem(
                item.item,
                this.props.renderItemOnPress
              );
            }
            return null;
          }
        },
        otherFlatListOptions
      );
    }
    return otherFlatListOptions;
  }

  render() {
    const totalLength = this.props.flatListOptions.data.length;

    return (
      <Popover {...this.props.popoverOptions}>
        {/* Don't remove the line below. 
          Automation script will crash without it */}
        <View style={styles.hiddenView} />
        <View style={this.flatListStyle}>
          <FlatList {...this.otherFlatListOptions} style={styles.flatList} />
          {this.props.flatListOptions.data.length > 8 ? (
            <View style={styles.itemTextWrapper}>
              <Text style={styles.recordText}>
                {/* TODO: use textstore once approved */}
                {`Total of ${totalLength} Records`}
              </Text>
            </View>
          ) : null}
        </View>
      </Popover>
    );
  }
}

PopoverFlatList.propTypes = {
  ...PopoverBase.propTypes,
  flatListOptions: PropTypes.oneOfType([PropTypes.object]),
  renderItemOnPress: PropTypes.func,
  testID: PropTypes.testID
};
PopoverFlatList.defaultProps = {
  ...PopoverBase.defaultProps,
  testID: ""
};
