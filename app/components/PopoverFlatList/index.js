import { Platform } from "react-native";
import PopoverFlatListIOS from "./PopoverFlatListIOS";

export default Platform.select({
  ios: PopoverFlatListIOS,
  android: {}
});
