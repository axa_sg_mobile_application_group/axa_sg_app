### PopoverDatePicker sample code snippets

```javascript
class Sample extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      popoverVisible: false,
      targetNode: null,
    }
    this.inputRef = React.createRef();
  }

  _onPresent(event) {
    this.setState({
      popoverVisible: true,
      targetNode: findNodeHandle(this.inputRef.current),
    })
  }

  onShow() {
  }

  onHide() {
    this.setState({
      popoverVisible: false,
    })
  }

  renderWrapperPopover() {
    return (
      <PopoverFlatList
        popoverOptions={{
          sourceView: this.state.targetNode,
          onShow: this.onShow.bind(this),
          onHide: this.onHide.bind(this),
          preferredContentSize: [300, 200],
          permittedArrowDirections: [0],
        }}
        flatListOptions={{
          extraData: this.state,
          keyExtractor: item => item.key,
          data: [
            { key: 'a', label: 'A' },
            { key: 'b', label: 'B' },
            { key: 'c', label: 'C' },
            { key: 'd', label: 'D' },
            { key: 'e', label: 'E' },
            { key: 'f', label: 'F' },
            { key: 'g', label: 'G' },
            { key: 'h', label: 'H' },
          ],
        }}
      />
    )
  }

  render() {
    return (
      <View>
        <TouchableOpacity ref={this.inputRef} onPress={this._onPresent.bind(this)}>
          <Text>Popover</Text>
        </TouchableOpacity>
        {this.state.popoverVisible && this.renderWrapperPopover()}
      </View>
    )
  }
}
```