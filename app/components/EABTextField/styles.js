import { StyleSheet } from "react-native";
import Theme from "../../theme";

const input = {
  ...Theme.bodyPrimary,
  height: Theme.bodyPrimary.lineHeight,
  width: "100%"
};

const labelText = { ...Theme.bodySecondary, textAlign: "right" };

export default StyleSheet.create({
  wrapper: {
    position: "relative",
    width: "100%"
  },
  label: {
    position: "absolute",
    left: 0,
    flexDirection: "row"
  },
  labelText,
  labelNonEditable: {
    ...labelText,
    color: Theme.mediumGrey
  },
  labelTextFocus: {
    ...labelText,
    color: Theme.brand
  },
  requiredHint: {
    ...labelText,
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  inputWrapper: {
    paddingBottom: Theme.alignmentXS - 1,
    height: Theme.bodyPrimary.lineHeight + Theme.alignmentXS
  },
  input,
  inputPlaceholderPlugin: {
    marginTop: Theme.alignmentXL,
    height: input.height + Theme.alignmentXS,
    paddingBottom: Theme.alignmentXS,
    borderBottomColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  /* minus 1 padding bottom for the border width plus one */
  inputPlaceholderFocus: {
    paddingBottom: Theme.alignmentXS - 1,
    borderBottomWidth: 2,
    borderBottomColor: Theme.brand
  },
  inputPlaceholderError: {
    borderBottomColor: Theme.negative
  },
  inputErrorPlugin: {
    marginBottom: Theme.alignmentXXS
  },
  inputNonEditable: {
    color: Theme.mediumGrey
  },
  hitTextWrapper: {
    minHeight: 21
  }
});
