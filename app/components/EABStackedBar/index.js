import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View } from "react-native";
import getStyleArray from "../../utilities/getStyleArray";
import Theme from "../../theme";
import styles from "./styles";

/**
 * <EABStackedBar />
 * @description stack bar component
 * @param {object} values - values to render view
 * @param {number} values.indicator - value for vertial line indicator
 * @param {number} values.topLayer - value for top layer rectangle
 * @param {number} values.bottomLayer - value for bottom layer rectangle
 * @param {number} values.max - max value for calculating width of top & bottom layer rectangle,
 *  and the location of vertial line indicator
 * @param {bool} [isBottomLarger=false] - to control the color of bottom layer rectangle
 * */

export default class EABStackedBar extends PureComponent {
  render() {
    const { values, isBottomLarger } = this.props;
    const { indicator, topLayer, bottomLayer, max } = values;

    return (
      <View style={styles.container}>
        <View
          style={[
            isBottomLarger
              ? styles.bottomBarBrandTransparent
              : styles.bottomBarBrand,
            ...getStyleArray({ width: (bottomLayer / max) * Theme.boxMaxWidth })
          ]}
        />
        <View
          style={[
            styles.topBar,
            ...getStyleArray({ width: (topLayer / max) * Theme.boxMaxWidth })
          ]}
        />
        <View
          style={[
            styles.indicator,
            ...getStyleArray({ left: (indicator / max) * Theme.boxMaxWidth })
          ]}
        />
      </View>
    );
  }
}

EABStackedBar.propTypes = {
  values: PropTypes.shape({
    indicator: PropTypes.number.isRequired,
    topLayer: PropTypes.number.isRequired,
    bottomLayer: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired.isRequired
  }).isRequired,
  isBottomLarger: PropTypes.bool
};

EABStackedBar.defaultProps = {
  isBottomLarger: false
};
