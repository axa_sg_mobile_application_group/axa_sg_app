import { StyleSheet } from "react-native";
import Theme from "../../theme";

const textSelectionView = {
  width: 192,
  height: 48,
  marginRight: Theme.alignmentL,
  marginBottom: 20
};

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    flexDirection: "row"
  },
  textSelectionView,
  textSelectionViewFirst: {
    ...textSelectionView,
    marginLeft: 0
  },
  textSelection: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "100%"
  },
  textSelectionContent: {
    alignItems: "center"
  }
});
