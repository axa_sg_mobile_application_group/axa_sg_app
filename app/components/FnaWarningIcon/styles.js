import { StyleSheet } from "react-native";

export default StyleSheet.create({
  image: {
    width: 28,
    height: 28
  }
});
