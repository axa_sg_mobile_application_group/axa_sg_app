import React, { Component } from "react";
import { Text, View } from "react-native";
import PropTypes from "prop-types";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import EABButton from "../../components/EABButton";
import EABTextField from "../../components/EABTextField";
import EABStepper from "../../components/EABStepper";
import SelectorField from "../../components/SelectorField";
import { FLAT_LIST } from "../../components/SelectorField/constants";
import { ContextConsumer } from "../../context";
import { RECTANGLE_BUTTON_2 } from "../../components/EABButton/constants";
import EABi18n from "../../utilities/EABi18n";
import styles from "../../routes/Needs/components/Analysis/styles";
import REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import getStyleArray from "../../utilities/getStyleArray";

const { FNA } = REDUCER_TYPES;
const { analysisAspect } = utilities.naAnalysis;
const { numberToCurrency } = utilities.numberFormatter;

export default class EASEAssets extends Component {
  constructor() {
    super();

    this.state = {};
  }

  getAssets() {
    const { selectedProduct, fe } = this.props;
    const { assets } = analysisAspect;
    let { selectedProfile } = this.props;

    if (selectedProduct === "ePlanning") {
      selectedProfile = "owner";
    }

    return assets
      .filter(ass => ass.needsRequired.find(need => need === selectedProduct))
      .map(ass => ({
        title: ass.title,
        value: ass.value,
        currentValue: fe[selectedProfile][ass.value] || 0
      }));
  }

  genAssetsSelector() {
    // =============================================================================
    // variables
    // =============================================================================
    const {
      textStore,
      isProtectionError,
      selectedAssets,
      selectedProduct,
      addAssetsOnPress,
      removeAssetsOnPress,
      assetTextOnChange,
      assetSelectorOnChange,
      returnOnChange,
      rateOfReturn,
      calOtherAsset,
      fe,
      testID
    } = this.props;

    let { selectedProfile } = this.props;

    if (selectedProduct === "ePlanning") {
      selectedProfile = "owner";
    }

    const allAssets = this.getAssets();
    const availableAssets = allAssets.filter(
      ass =>
        fe[selectedProfile][ass.value] !== 0 &&
        ass.value !== "all" &&
        calOtherAsset({ key: ass.value, selectedProduct }) <
          fe[selectedProfile][ass.value]
    );

    if (
      (!availableAssets || availableAssets.length === 0) &&
      (!selectedAssets || selectedAssets.length === 0)
    ) {
      return <Text style={styles.analysisProductPointContent}>$0</Text>;
    }

    if (!selectedAssets) {
      return (
        <ContextConsumer>
          {({ language }) => (
            <View>
              <EABButton
                testID={`${testID}__btnAddAdditionalValueAssets`}
                buttonType={RECTANGLE_BUTTON_2}
                containerStyle={styles.analysisAddAssetButton}
                onPress={addAssetsOnPress}
              >
                <Text style={styles.analysisRemoveButtonText}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.button.add"
                  })}
                </Text>
              </EABButton>
            </View>
          )}
        </ContextConsumer>
      );
    }
    const Input = [];

    selectedAssets.forEach((selectedAsset, i) => {
      const targetAssets = allAssets.find(
        ass => ass.value === selectedAssets[i].key
      );

      const usedAssetPercentage =
        (Number(selectedAssets[i].usedAsset) /
          Number(targetAssets.currentValue)) *
        100;

      const otherAssetPercentage =
        (Number(
          calOtherAsset({
            key: selectedAsset.key,
            selectedProduct
          })
        ) /
          Number(targetAssets.currentValue)) *
        100;

      Input.push(
        <ContextConsumer>
          {({ language }) => (
            <View>
              <View style={{ alignItems: "flex-end" }}>
                <Text>Current Value (PV): ${targetAssets.currentValue}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <SelectorField
                  testID={`${testID}__ddlSelectAssets`}
                  index={i}
                  style={styles.analysisSelectorHalfWidth}
                  value={targetAssets ? targetAssets.title : null}
                  placeholder="Asset"
                  selectorType={FLAT_LIST}
                  flatListOptions={{
                    renderItemOnPress: assetSelectorOnChange,
                    keepPrevData: true,
                    data: allAssets
                      .filter(
                        ass =>
                          !selectedAssets.find(
                            select => select.key === ass.value
                          ) &&
                          fe[selectedProfile][ass.value] !== 0 &&
                          ass.value !== "all" &&
                          calOtherAsset({ key: ass.value, selectedProduct }) <
                            fe[selectedProfile][ass.value]
                      )
                      .map(ass => ({
                        label: ass.title,
                        value: ass.value,
                        key: ass.value
                      }))
                  }}
                  isRequired
                />
                <View style={styles.analysisTextFieldHalfWidth}>
                  <EABTextField
                    prefix="$"
                    testID={`${testID}__btnAssetValue`}
                    value={numberToCurrency({
                      value: selectedAssets[i].usedAsset || 0,
                      decimal: 0
                    })}
                    placeholder="Value"
                    isError={isProtectionError.assets[i].usedAsset.hasError}
                    hintText={EABi18n({
                      path: isProtectionError.assets[i].usedAsset.messagePath,
                      language,
                      textStore
                    })}
                    onChange={(value, index = i) =>
                      assetTextOnChange(value, index)
                    }
                    isRequired
                  />

                  {isProtectionError.assets[i].usedAsset.hasError ? (
                    <View
                      style={{
                        flexDirection: "row",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        height: 60,
                        zIndex: -1
                      }}
                    >
                      <View
                        style={{
                          backgroundColor: "rgb(218, 218, 218)",
                          width: "100%",
                          height: 60
                        }}
                      />
                    </View>
                  ) : (
                    <View
                      style={{
                        flexDirection: "row",
                        position: "absolute",
                        top: 0,
                        left: 0,
                        height: 60,
                        zIndex: -1
                      }}
                    >
                      <View
                        style={{
                          backgroundColor: "rgb(28, 197, 78)",
                          width: `${usedAssetPercentage}%`,
                          height: 60
                        }}
                      />
                      <View
                        style={{
                          backgroundColor: "red",
                          width: `${otherAssetPercentage}%`,
                          height: 60
                        }}
                      />
                      <View
                        style={{
                          backgroundColor: "rgb(218, 218, 218)",
                          width: `${100 -
                            otherAssetPercentage -
                            usedAssetPercentage}%`,
                          height: 60
                        }}
                      />
                    </View>
                  )}
                </View>
                <EABButton
                  testID={`${testID}__btnRemoveAssets`}
                  buttonType={RECTANGLE_BUTTON_2}
                  containerStyle={styles.analysisRemoveButton}
                  onPress={() => removeAssetsOnPress(i)}
                >
                  <Text style={styles.analysisRemoveButtonText}>Remove</Text>
                </EABButton>
              </View>
              {rateOfReturn && (
                <View>
                  <Text style={styles.analysisProductPointTitle}>
                    Rate of return (%)
                    <Text style={styles.requiredHint}> *</Text>
                  </Text>
                  <EABStepper
                    testID={`${testID}__ROR`}
                    value={
                      selectedAssets[i].return
                        ? Number(selectedAssets[i].return)
                        : ""
                    }
                    step={0.5}
                    min={-100}
                    max={100}
                    onChange={(value, index = i) =>
                      returnOnChange(value.toString(), index)
                    }
                  />
                </View>
              )}
              <Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 22 }}>
                Asset Value: ${numberToCurrency({
                  value: selectedAssets[i].calAsset || 0,
                  decimal: 2
                })}
              </Text>
            </View>
          )}
        </ContextConsumer>
      );
    });
    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            {Input}
            {availableAssets &&
              availableAssets.length !== 0 &&
              availableAssets.length > selectedAssets.length && (
                <EABButton
                  buttonType={RECTANGLE_BUTTON_2}
                  containerStyle={styles.analysisAddAssetButton}
                  onPress={addAssetsOnPress}
                >
                  <Text style={styles.analysisRemoveButtonText}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.fiProtection.button.add"
                    })}
                  </Text>
                </EABButton>
              )}
          </View>
        )}
      </ContextConsumer>
    );
  }

  render() {
    return (
      <View style={[styles.container, ...getStyleArray(this.props.style)]}>
        <View style={styles.wrapper}>{this.genAssetsSelector()}</View>
      </View>
    );
  }
}

EASEAssets.propTypes = {
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  addAssetsOnPress: PropTypes.func.isRequired,
  removeAssetsOnPress: PropTypes.func.isRequired,
  assetTextOnChange: PropTypes.func.isRequired,
  assetSelectorOnChange: PropTypes.func.isRequired,
  returnOnChange: PropTypes.func.isRequired,
  calOtherAsset: PropTypes.func.isRequired,
  rateOfReturn: PropTypes.bool.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  selectedAssets: PropTypes.shape({}).isRequired,
  selectedProfile: PropTypes.shape({}).isRequired,
  selectedProduct: PropTypes.shape({}).isRequired,
  isProtectionError: PropTypes.string.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  testID: PropTypes.string
};
EASEAssets.defaultProps = {
  style: {},
  testID: ""
};
