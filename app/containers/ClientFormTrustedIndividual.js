import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import ClientFormTrustedIndividual from "../components/ClientFormTrustedIndividual";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const { CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;

const mapStateToProps = state => ({
  photo: state[CLIENT_FORM].photo,
  relationship: state[CLIENT_FORM].relationship,
  isRelationshipError: state[CLIENT_FORM].isRelationshipError,
  relationshipOther: state[CLIENT_FORM].relationshipOther,
  isRelationshipOtherError: state[CLIENT_FORM].isRelationshipOtherError,
  givenName: state[CLIENT_FORM].givenName,
  isGivenNameError: state[CLIENT_FORM].isGivenNameError,
  surname: state[CLIENT_FORM].surname,
  name: state[CLIENT_FORM].name,
  isNameError: state[CLIENT_FORM].isNameError,
  nameOrder: state[CLIENT_FORM].nameOrder,
  IDDocumentType: state[CLIENT_FORM].IDDocumentType,
  isIDDocumentTypeError: state[CLIENT_FORM].isIDDocumentTypeError,
  IDDocumentTypeOther: state[CLIENT_FORM].IDDocumentTypeOther,
  isIDDocumentTypeOtherError: state[CLIENT_FORM].isIDDocumentTypeOtherError,
  ID: state[CLIENT_FORM].ID,
  isIDError: state[CLIENT_FORM].isIDError,
  prefixA: state[CLIENT_FORM].prefixA,
  isPrefixAError: state[CLIENT_FORM].isPrefixAError,
  mobileNoA: state[CLIENT_FORM].mobileNoA,
  isMobileNoAError: state[CLIENT_FORM].isMobileNoAError,
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = dispatch => ({
  photoOnChange: newPhoto => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PHOTO_ON_CHANGE,
      newPhoto
    });
  },
  relationshipOnChange: newRelationship => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_ON_CHANGE,
      newRelationship
    });
  },
  relationshipOtherOnChange: newRelationshipOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE,
      newRelationshipOther
    });
  },
  givenNameOnChange: newGivenName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
      newGivenName
    });
  },
  surnameOnChange: newSurname => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SURNAME_ON_CHANGE,
      newSurname
    });
  },
  nameOnChange: newName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NAME_ON_CHANGE,
      newName
    });
  },
  nameOrderOnChange: newNameOrder => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NAME_ORDER_ON_CHANGE,
      newNameOrder
    });
  },
  IDDocumentTypeOnChange: newIDDocType => {
    // hard code for do not need nationalityData and singaporePRStatusData in
    // trusted individual form
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
      newIDDocType,
      nationalityData: "",
      singaporePRStatusData: ""
    });
  },
  IDDocumentTypeOtherOnChange: newIDDocTypeOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE,
      newIDDocTypeOther
    });
  },
  IDOnChange: newID => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_ON_CHANGE,
      newID
    });
  },
  prefixAOnChange: newPrefixA => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PREFIX_A_ON_CHANGE,
      newPrefixA
    });
  },
  mobileNoAOnChange: ({ newMobileNoA, prefixAData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
      newMobileNoA,
      prefixAData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientFormTrustedIndividual);
