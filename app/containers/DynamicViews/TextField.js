import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import TextField from "../../components/DynamicViews/TextField";

/**
 * TextField
 * @description TextField in Dynamic Views
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  updateAddress: ({ fileName, postalCode, path, fieldType, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_UPDATE_DYNAMIC_APPLICATION_FORM_ADDRESS,
      fileName,
      postalCode,
      fieldType,
      path,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TextField);
