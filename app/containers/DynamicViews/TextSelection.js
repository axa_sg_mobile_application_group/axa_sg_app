import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import TextSelection from "../../components/DynamicViews/TextSelection";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * TextSelection
 * @description Form for normal product in Recommendation page.
 * @requires TextSelection - TextSelection UI
 * */
const mapStateToProps = (state, ownProps) => ({
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  value: state[REDUCER_TYPES.RECOMMENDATION].recommendation[ownProps.quotId],
  error:
    state[REDUCER_TYPES.RECOMMENDATION].error.recommendation[ownProps.quotId],
  isChosen:
    state[REDUCER_TYPES.RECOMMENDATION].recommendation.chosenList.indexOf(
      ownProps.quotId
    ) >= 0,
  disabled: state[REDUCER_TYPES.RECOMMENDATION].component.disabled
});

export default connect(mapStateToProps)(TextSelection);
