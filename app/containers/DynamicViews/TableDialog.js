import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import TableDialog from "../../components/DynamicViews/Table/dialog";

const { APPLICATION } = REDUCER_TYPES;
/**
 * TableDialog
 * @description Input Dialog of Table in DynamicViews
 * */
const mapStateToProps = state => ({
  error: state[REDUCER_TYPES.APPLICATION].error
});

const mapDispatchToProps = dispatch => ({
  saveApplicationFormTableRecord: ({ path, dataId, values, recordIndex }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_SAVE_APPLICATION_FORM_TABLE_RECORD,
      path,
      dataId,
      values,
      recordIndex
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TableDialog);
