import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import CheckBox from "../../components/DynamicViews/CheckBox";

const { OPTIONS_MAP } = REDUCER_TYPES;
/**
 * CheckBox
 * @description CheckBox in DynamicViews
 * */

const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = () => {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckBox);
