import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import ROPTable from "../../components/DynamicViews/ROPTable";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";

const { APPLICATION, OPTIONS_MAP } = REDUCER_TYPES;
/**
 * ROPTable
 * @description ROPTable in DynamicViews
 * */

const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  updateROPTable: ({ path, id, newValue }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_ROP_TABLE,
      path,
      id,
      pathValue: newValue
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ROPTable);
