import { connect } from "react-redux";
import TextInputBugFix from "../components/TextInputBugFix";

const mapStateToProps = () => ({
  reRenderProps: {}
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TextInputBugFix);
