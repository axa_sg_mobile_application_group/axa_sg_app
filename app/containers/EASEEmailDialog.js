import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import EASEEmailDialog from "../components/EASEEmailDialog";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const { FNA, CLIENT_FORM } = REDUCER_TYPES;

/**
 * EASEEmailDialog
 * @requires EASEEmailDialog - EASEEmailDialog UI
 * */
const mapStateToProps = state => ({
  FNAReportEmail: state[FNA].FNAReportEmail,
  email: state[CLIENT_FORM].email,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  saveClientEmail: callback => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT_EMAIL,
      callback
    });
  },
  openEmailDialog: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].OPEN_EMAIL_DIALOG,
      callback
    });
  },
  emailOnChange: newEmail => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].EMAIL_ON_CHANGE,
      newEmail
    });
  },
  updateSkipEmailInReport: value => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_SKIP_EMAIL_IN_REPORT,
      value
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EASEEmailDialog);
