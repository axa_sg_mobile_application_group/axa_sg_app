import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import FnaWarningIcon from "../components/FnaWarningIcon";

const { FNA } = REDUCER_TYPES;

/**
 * FnaWarningIcon
 * @requires FnaWarningIcon - FnaWarningIcon UI
 * */
const mapStateToProps = state => ({
  isFnaInvalid: state[FNA].isFnaInvalid
});

export default connect(mapStateToProps)(FnaWarningIcon);
