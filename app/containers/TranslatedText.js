import { connect } from "react-redux";
import TranslatedText from "../components/TranslatedText";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TranslatedText);
