import { connect } from "react-redux";
import PopoverDatePicker from "../components/PopoverDatePicker";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PopoverDatePicker);
