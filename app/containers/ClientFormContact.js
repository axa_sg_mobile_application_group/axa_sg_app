import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import ClientFormContact from "../components/ClientFormContact";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const { CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * <ClientFormContact />
 * @requires ClientFormContact - ClientFormContact UI
 * alertFunction in the postalOnBlur -- for the alert box message popup
 * */
const mapStateToProps = state => ({
  isCreate: state[CLIENT_FORM].config.isCreate,
  isFamilyMember: state[CLIENT_FORM].config.isFamilyMember,
  isFNA: state[CLIENT_FORM].config.isFNA,
  isApplication: state[CLIENT_FORM].isApplication,
  prefixA: state[CLIENT_FORM].prefixA,
  mobileNoA: state[CLIENT_FORM].mobileNoA,
  prefixB: state[CLIENT_FORM].prefixB,
  mobileNoB: state[CLIENT_FORM].mobileNoB,
  email: state[CLIENT_FORM].email,
  isEmailError: state[CLIENT_FORM].isEmailError,
  countryOfResidence: state[CLIENT_FORM].countryOfResidence,
  country: state[CLIENT_FORM].country,
  cityState: state[CLIENT_FORM].cityState,
  postal: state[CLIENT_FORM].postal,
  blockHouse: state[CLIENT_FORM].blockHouse,
  streetRoad: state[CLIENT_FORM].streetRoad,
  unit: state[CLIENT_FORM].unit,
  buildingEstate: state[CLIENT_FORM].buildingEstate,
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  isPrefixAError: state[CLIENT_FORM].isPrefixAError,
  isMobileNoAError: state[CLIENT_FORM].isMobileNoAError
});

const mapDispatchToProps = dispatch => ({
  prefixAOnChange: newPrefixA => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PREFIX_A_ON_CHANGE,
      newPrefixA
    });
  },
  mobileNoAOnChange: ({ newMobileNoA, prefixAData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
      newMobileNoA,
      prefixAData
    });
  },
  prefixBOnChange: newPrefixB => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PREFIX_B_ON_CHANGE,
      newPrefixB
    });
  },
  mobileNoBOnChange: ({ newMobileNoB, prefixBData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].MOBILE_NO_B_ON_CHANGE,
      newMobileNoB,
      prefixBData
    });
  },
  emailOnChange: newEmail => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].EMAIL_ON_CHANGE,
      newEmail
    });
  },
  countryOnChange: ({ newCountry, countryOfResidenceData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].COUNTRY_ON_CHANGE,
      newCountry,
      countryOfResidenceData
    });
  },
  cityStateOnChange: newCityState => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CITY_STATE_ON_CHANGE,
      newCityState
    });
  },
  postalOnBlur: ({ newPostal, alertFunction }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].POSTALCODE_ON_BLUR,
      newPostal,
      alertFunction
      /* alert function is added to notify user */
    });
  },
  postalOnChange: newPostal => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].POSTAL_ON_CHANGE,
      newPostal
    });
  },
  blockHouseOnChange: newBlockHouse => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
      newBlockHouse
    });
  },
  streetRoadOnChange: newStreetRoad => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].STREET_ROAD_ON_CHANGE,
      newStreetRoad
    });
  },
  unitOnChange: ({ newUnit, prefix }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].UNIT_ON_CHANGE,
      newUnit,
      prefix
    });
  },
  buildingEstateOnChange: newBuildingEstate => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
      newBuildingEstate
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientFormContact);
