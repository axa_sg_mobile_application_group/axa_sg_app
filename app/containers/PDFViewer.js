import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import PDFViewer from "../components/PDFViewer";

const { FNA } = REDUCER_TYPES;

/**
 * PDFViewer
 * @requires PDFViewer - PDFViewer UI
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  FNAReportInitialize: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA_REPORT,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PDFViewer);
