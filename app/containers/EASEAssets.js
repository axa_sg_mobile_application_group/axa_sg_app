import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import EASEAssets from "../components/EASEAssets";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * <EASEAssets />
 * @requires EASEAssets - EASEAssets UI
 * alertFunction in the postalOnBlur -- for the alert box message popup
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  isNaError: state[FNA].isNaError,
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EASEAssets);
