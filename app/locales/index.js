import { LANGUAGE_TYPES, TEXT_STORE } from "eab-web-api";
import ENGLISH from "./textStore/ENGLISH";
import TRADITIONAL_CHINESE from "./textStore/TRADITIONAL_CHINESE";

export default {
  version: TEXT_STORE.version,
  [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TRADITIONAL_CHINESE,
  [LANGUAGE_TYPES.ENGLISH]: ENGLISH
};
