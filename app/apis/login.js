import axios from "axios";
import querystring from "querystring";
import Config from "react-native-config";
import { RSA } from "react-native-rsa-native";
import { BUILD_NUMBER, VERSION_NUMBER } from "../constants/BUILD_CONFIG";
import { aesGen256Key, aesEncrypt, aesDecrypt } from "../utilities/SecureUtils";

/**
 * getPublicKey
 * @description getPublicKey
 * @param {string} environment - environment
 * */
export const getPublicKey = (authToken, environment) => {
  let url = "";
  let baseURL = "";
  let getPublicKeyResult = {};
  // const errorMsg = "Authorization Failed (Error 008)";
  if (Config.ENV_NAME === "PROD") {
    url = Config.GET_PUBLIC_KEY;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    if (environment === "DEV") {
      url = Config.DEV_GET_PUBLIC_KEY;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      url = Config.SIT1_EAB_GET_PUBLIC_KEY;
      baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      url = Config.SIT2_EAB_GET_PUBLIC_KEY;
      baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      url = Config.SIT3_EAB_GET_PUBLIC_KEY;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      url = Config.SIT_AXA_GET_PUBLIC_KEY;
      baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      url = Config.UAT_GET_PUBLIC_KEY;
      baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      url = Config.PRE_PROD_GET_PUBLIC_KEY;
      baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    } else {
      url = Config.SIT3_EAB_GET_PUBLIC_KEY;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    }
  } else {
    url = Config.SIT3_EAB_GET_PUBLIC_KEY;
    baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
  }
  return new Promise(resolve => {
    const AuthStr = `Bearer ${authToken}`;
    const options = {
      method: "get",
      headers: { Authorization: AuthStr },
      baseURL,
      url
    };
    axios
      .request(options)
      .then(response => {
        if (response.status === 401) {
          getPublicKeyResult = {
            hasError: true,
            errorMsg: "Get Public Key Error 401"
          };
          resolve(getPublicKeyResult);
        } else if (response.data && response.data.status === "success") {
          getPublicKeyResult = {
            hasError: false,
            result: response.data.result
          };
          resolve(getPublicKeyResult);
        } else {
          getPublicKeyResult = {
            hasError: true,
            errorMsg: "[getPublicKeyError]"
          };
          resolve(getPublicKeyResult);
        }
      })
      .catch(error => {
        getPublicKeyResult = {
          hasError: true,
          errorMsg: `[getPublicKeyError]${error}`
        };
        resolve(getPublicKeyResult);
      });
  });
};
export const encryption = (dataInput, authToken, environment) =>
  new Promise(resolve => {
    let encryptionResult = {};
    const getPublicKeyResult = getPublicKey(authToken, environment);
    getPublicKeyResult.then(callback => {
      if (!callback.hasError) {
        if (callback.result && callback.result.publicKey) {
          const publicKeyGen = callback.result.publicKey;
          if (publicKeyGen) {
            // gen aesEncryptedKey
            const aesKey = aesGen256Key();
            let d0 = {};
            if (dataInput) {
              const data = JSON.stringify(dataInput);
              d0 = aesEncrypt(data, aesKey);
            } else {
              d0 = {};
            }
            RSA.encrypt(aesKey, publicKeyGen).then(encodedMessage => {
              const dk = encodedMessage;
              encryptionResult = {
                hasError: false,
                result: {
                  aesKey,
                  d0,
                  dk
                }
              };
              resolve(encryptionResult);
            });
          } else {
            // no key is find
            encryptionResult = {
              hasError: true,
              errorMsg: callback.errorMsg
                ? callback.errorMsg
                : "Authorization Failed (Error 008)"
            };
            resolve(encryptionResult);
          }
        } else {
          // error if callback.result.publicyKey is undefined
          encryptionResult = {
            hasError: true,
            errorMsg: callback.errorMsg
              ? callback.errorMsg
              : "Authorization Failed (Error 008)"
          };
          resolve(encryptionResult);
        }
      } else {
        // error if callback.result.publicyKey is undefined
        encryptionResult = {
          hasError: true,
          errorMsg: callback.errorMsg
            ? callback.errorMsg
            : "Authorization Failed (Error 008)"
        };
        resolve(encryptionResult);
      }
    });
  }).catch(error => {
    const encryptionResult = {
      hasError: true,
      errorMsg: `${error}Authorization Failed (Error 008)`
    };
    return encryptionResult;
  });

export const decryption = (encodedMessage, aesKey) =>
  new Promise(resolve => {
    const data = aesDecrypt(encodedMessage, aesKey);
    resolve(data);
  }).catch(() => {
    // need error handling
  });

/**
 * checkVersionAndDevice
 * @description checkVersionAndDevice
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} installationId - installationId
 * @param {string} environment - environment
 * */
export const checkVersionAndDevice = (
  authToken,
  installationId,
  environment
) => {
  let url = "";
  let baseURL = "";
  let isValidVersionAndDevice = true;
  let checkVersionAndDeviceResult = {};
  let errorMsg = "";
  let hasError = false;
  if (Config.ENV_NAME === "PROD") {
    url = Config.CHECK_VERSION_AND_DEVICE;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    if (environment === "DEV") {
      url = Config.DEV_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      url = Config.SIT1_EAB_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      url = Config.SIT2_EAB_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      url = Config.SIT3_EAB_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      url = Config.SIT_AXA_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      url = Config.UAT_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      url = Config.PRE_PROD_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    } else {
      url = Config.SIT3_EAB_CHECK_VERSION_AND_DEVICE;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    }
  } else {
    url = Config.SIT3_EAB_CHECK_VERSION_AND_DEVICE;
    baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
  }
  return new Promise(resolve => {
    if (authToken && installationId) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          versionNumber: VERSION_NUMBER,
          buildNumber: BUILD_NUMBER,
          installationId
        }
      };
      axios
        .request(options)
        .then(response => {
          if (response.data.status === "success") {
            if (!response.data.result.isNeedUpgrade) {
              // no need to upgrade
              isValidVersionAndDevice = true;
              if (!response.data.result.isDeactiveDevice) {
                // is active device
                hasError = false;
                isValidVersionAndDevice = true; // will amend
              } else {
                // no longer an active device
                hasError = false;
                isValidVersionAndDevice = false;
                errorMsg =
                  "This device is no longer registered to use this app. If you wish to use the app on this device, please delete and re-install the app.";
              }
            } else {
              // need to upgrade
              hasError = false;
              isValidVersionAndDevice = false;
              errorMsg =
                "The version of the app you are currently using is no longer supported. Please update the app to proceed further.";
            }
          } else {
            hasError = true;
            isValidVersionAndDevice = false;
            errorMsg = "Authorization Failed (Error 007)";
          }
          // Final result
          checkVersionAndDeviceResult = {
            hasError,
            isValidVersionAndDevice,
            errorMsg
          };

          resolve(checkVersionAndDeviceResult);
        })
        .catch(error => {
          if (error.code === "ECONNABORTED") {
            // Timeout error
            errorMsg = "Authorization Failed (Error 009)";
          } else {
            errorMsg = "Authorization Failed (Error 007)";
          }
          isValidVersionAndDevice = false;
          checkVersionAndDeviceResult = {
            hasError: true,
            isValidVersionAndDevice,
            errorMsg
          };
          resolve(checkVersionAndDeviceResult);
        });
    } else {
      isValidVersionAndDevice = false;
      errorMsg = "Authorization Failed (Error 007)";
      checkVersionAndDeviceResult = {
        hasError: true,
        isValidVersionAndDevice,
        errorMsg
      };
      resolve(checkVersionAndDeviceResult);
    }
  });
};

/**
 * fetchAuthToken
 * @description call AXA api to get authToekn by authCode
 * @param {string} authCode - authCode from AXA api provide
 * */
export const fetchAuthToken = (authCode, environment) => {
  let authCodeCheck = "";
  if (authCode && authCode.length > 0) {
    authCodeCheck = authCode;
  } else {
    authCodeCheck = null;
  }

  return new Promise((resolve, reject) => {
    if (
      authCode &&
      authCode.length > 0 &&
      authCodeCheck &&
      authCodeCheck.length > 0
    ) {
      const options = {
        method: "POST",
        headers: {
          "cache-control": "no-cache",
          "content-type": "application/x-www-form-urlencoded"
        },
        data: querystring.stringify({
          grant_type: "authorization_code",
          code: authCode,
          redirect_uri: "easemobile://"
        })
      };
      if (Config.ENV_NAME === "PROD") {
        options.url = Config.GET_TOKEN_URL;
        options.baseURL = Config.GET_TOKEN_BASIC_URL;
        options.headers.authorization = Config.MAAM_AUTH;
      } else if (environment && environment.length > 0) {
        if (environment === "DEV") {
          options.url = Config.DEV_GET_TOKEN_URL;
          options.baseURL = Config.DEV.GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.DEV_MAAM_AUTH;
        } else if (environment === "SIT1_EAB") {
          options.url = Config.SIT1_EAB_GET_TOKEN_URL;
          options.baseURL = Config.SIT1_EAB_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.SIT1_EAB_MAAM_AUTH;
        } else if (environment === "SIT2_EAB") {
          options.url = Config.SIT2_EAB_GET_TOKEN_URL;
          options.baseURL = Config.SIT2_EAB_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.SIT2_EAB_MAAM_AUTH;
        } else if (environment === "SIT3_EAB") {
          options.url = Config.SIT3_EAB_GET_TOKEN_URL;
          options.baseURL = Config.SIT3_EAB_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.SIT3_EAB_MAAM_AUTH;
        } else if (environment === "SIT_AXA") {
          options.url = Config.SIT_AXA_GET_TOKEN_URL;
          options.baseURL = Config.SIT_AXA_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.SIT_AXA_MAAM_AUTH;
        } else if (environment === "PRE_PROD") {
          options.url = Config.PRE_PROD_GET_TOKEN_URL;
          options.baseURL = Config.PRE_PROD_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.PRE_PROD_MAAM_AUTH;
        } else if (environment === "UAT") {
          options.url = Config.UAT_GET_TOKEN_URL;
          options.baseURL = Config.UAT_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.UAT_MAAM_AUTH;
        } else {
          options.url = Config.SIT3_EAB_GET_TOKEN_URL;
          options.baseURL = Config.SIT3_EAB_GET_TOKEN_BASIC_URL;
          options.headers.authorization = Config.SIT3_EAB_MAAM_AUTH;
        }
      } else {
        options.url = Config.SIT3_EAB_GET_TOKEN_URL;
        options.baseURL = Config.SIT3_EAB_GET_TOKEN_BASIC_URL;
        options.headers.authorization = Config.SIT3_EAB_MAAM_AUTH;
      }

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const result = { ...response.data };
            if (result.access_token && result.access_token.length > 0) {
              resolve(response);
            } else {
              reject(new Error("[fetchAuthToken]-[Error]:  can not get token"));
            }
          } else {
            reject(
              new Error("[fetchAuthToken]-[Error]:Fail to retrieve authToken")
            );
          }
        })
        .catch(error =>
          reject(new Error(`[fetchAuthToken]-[Error]: ${error}`))
        );
    } else {
      reject(new Error("[fetchAuthToken]-[Error]:  No authCode"));
    }
  });
};

/**
 * fetchSessionCookie
 * @description call AXA api to get authToekn by authCode
 * @param {string} idToken - authCode from AXA api provide
 * */
export const fetchSessionCookie = (idToken, authToken, environment) =>
  new Promise(resolve => {
    if (idToken && idToken.length > 0) {
      let clientId = "";
      let clientPw = "";
      let baseURL = "";
      let url = "";
      if (Config.ENV_NAME === "PROD") {
        clientId = Config.CLIENT_ID;
        clientPw = Config.CLIENT_PW;
        url = `${Config.SYNC_GATEWAY_BASIC_URL}:${Config.SYNC_GATEWAY_PORT}/${
          Config.SYNC_GATEWAY_DB
        }/_session`;
        baseURL = Config.WEB_SERVICE_BASIC_URL;
      } else if (environment && environment.length > 0) {
        if (environment === "DEV") {
          clientId = Config.DEV_CLIENT_ID;
          clientPw = Config.DEV_CLIENT_PW;
          url = `${Config.DEV_SYNC_GATEWAY_BASIC_URL}:${
            Config.DEV_SYNC_GATEWAY_PORT
          }/${Config.DEV_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
        } else if (environment === "SIT1_EAB") {
          clientId = Config.SIT1_EAB_CLIENT_ID;
          clientPw = Config.SIT1_EAB_CLIENT_PW;
          url = `${Config.SIT1_EAB_SYNC_GATEWAY_BASIC_URL}:${
            Config.SIT1_EAB_SYNC_GATEWAY_PORT
          }/${Config.SIT1_EAB_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
        } else if (environment === "SIT2_EAB") {
          clientId = Config.SIT2_EAB_CLIENT_ID;
          clientPw = Config.SIT2_EAB_CLIENT_PW;
          url = `${Config.SIT2_EAB_SYNC_GATEWAY_BASIC_URL}:${
            Config.SIT2_EAB_SYNC_GATEWAY_PORT
          }/${Config.SIT2_EAB_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.WEB_SERVICE_BASIC_URL;
        } else if (environment === "SIT3_EAB") {
          clientId = Config.SIT3_EAB_CLIENT_ID;
          clientPw = Config.SIT3_EAB_CLIENT_PW;
          url = `${Config.SIT3_EAB_SYNC_GATEWAY_BASIC_URL}:${
            Config.SIT3_EAB_SYNC_GATEWAY_PORT
          }/${Config.SIT3_EAB_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
        } else if (environment === "SIT_AXA") {
          clientId = Config.SIT_AXA_CLIENT_ID;
          clientPw = Config.SIT_AXA_CLIENT_PW;
          url = `${Config.SIT_AXA_SYNC_GATEWAY_BASIC_URL}:${
            Config.SIT_AXA_SYNC_GATEWAY_PORT
          }/${Config.SIT_AXA_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
        } else if (environment === "UAT") {
          clientId = Config.UAT_CLIENT_ID;
          clientPw = Config.UAT_CLIENT_PW;
          url = `${Config.UAT_SYNC_GATEWAY_BASIC_URL}:${
            Config.UAT_SYNC_GATEWAY_PORT
          }/${Config.UAT_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
        } else if (environment === "PRE_PROD") {
          clientId = Config.PRE_PROD_CLIENT_ID;
          clientPw = Config.PRE_PROD_CLIENT_PW;
          url = `${Config.PRE_PROD_SYNC_GATEWAY_BASIC_URL}:${
            Config.PRE_PROD_SYNC_GATEWAY_PORT
          }/${Config.PRE_PROD_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
        } else {
          clientId = Config.SIT3_EAB_CLIENT_ID;
          clientPw = Config.SIT3_EAB_CLIENT_PW;
          url = `${Config.SIT3_EAB_SYNC_GATEWAY_BASIC_URL}:${
            Config.SIT3_EAB_SYNC_GATEWAY_PORT
          }/${Config.SIT3_EAB_SYNC_GATEWAY_DB}/_session`;
          baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
        }
      } else {
        clientId = Config.CLIENT_ID;
        clientPw = Config.SIT3_EAB_CLIENT_PW;
        url = `${Config.SIT3_EAB_SYNC_GATEWAY_BASIC_URL}:${
          Config.SIT3_EAB_SYNC_GATEWAY_PORT
        }/${Config.SIT3_EAB_SYNC_GATEWAY_DB}/_session`;
        baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      }
      const AuthStr = `Bearer ${idToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          SessionBody: {
            name: clientId,
            password: clientPw
          }
        }
      };
      let aes256Key = "";
      const encryptResult = encryption(options.data, authToken, environment);
      encryptResult
        .then(callback => {
          if (callback.hasError) {
            const responseResult = {
              hasError: true,
              error: callback.errorMsg
                ? callback.errorMsg
                : "Cannot get Public Key"
            };
            resolve(responseResult);
          } else {
            aes256Key = callback.result.aesKey;
            options.data.d0 = callback.result.d0;
            options.data.dk = callback.result.dk;
          }
        })
        .then(() => {
          axios
            .request(options)
            .then(response => {
              const decryptionResult = decryption(response.data, aes256Key);
              decryptionResult.then(decryptedResponseBody => {
                response.data = JSON.parse(decryptedResponseBody);
                if (response.status === 200) {
                  const responseResult = {
                    hasError: false,
                    result: response
                  };
                  resolve(responseResult);
                } else {
                  const responseResult = {
                    hasError: true,
                    error:
                      "[fetchSessionCookie]-[Error]:Fail to fetch Session Cookie"
                  };
                  resolve(responseResult);
                }
              });
            })
            .catch(error => {
              const responseResult = {
                hasError: true,
                error
              };
              resolve(responseResult);
            });
        });
    } else {
      const responseResult = {
        hasError: true,
        error: "[fetchSessionCookie]-[Error]:No idToken / environment"
      };
      resolve(responseResult);
    }
  });

/**
 * verifyUserInfo
 * @description call 121Online API to verify User and Mobile Device
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} uuid - device uuid
 * @param {string} deviceModel - device model
 * @param {string} versionNumber - ease version number
 * @param {string} buildNumber - ease build number
 * */
export const verifyUserInfo = (
  authToken,
  idToken,
  uuid,
  deviceModel,
  versionNumber,
  buildNumber,
  environment
) => {
  let url = "";
  let baseURL = "";
  // TODO: to be removed
  if (Config.ENV_NAME === "PROD") {
    url = Config.ACTIVATE_USER_INFO_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    if (environment === "DEV") {
      url = Config.DEV_ACTIVATE_USER_INFO_URL;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      url = Config.SIT1_EAB_ACTIVATE_USER_INFO_URL;
      baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      url = Config.SIT2_EAB_ACTIVATE_USER_INFO_URL;
      baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      url = Config.SIT3_EAB_ACTIVATE_USER_INFO_URL;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      url = Config.SIT_AXA_ACTIVATE_USER_INFO_URL;
      baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      url = Config.UAT_ACTIVATE_USER_INFO_URL;
      baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      url = Config.PRE_PROD_ACTIVATE_USER_INFO_URL;
      baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    } else {
      url = Config.DEV_ACTIVATE_USER_INFO_URL;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    }
  } else {
    url = Config.DEV_ACTIVATE_USER_INFO_URL;
    baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      uuid &&
      uuid.length > 0 &&
      deviceModel &&
      deviceModel.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          device_model: deviceModel,
          device_id: uuid,
          versionNumber,
          buildNumber,
          id_token: idToken
        }
      };
      let aes256Key = "";
      const encryptResult = encryption(options.data, authToken, environment);
      encryptResult
        .then(callback => {
          if (callback.hasError) {
            const responseResult = {
              hasError: true,
              multipleMsgKey: callback.errorMsg
                ? callback.errorMsg
                : "Cannot get Public Key"
            };
            resolve(responseResult);
          } else {
            aes256Key = callback.result.aesKey;
            options.data.d0 = callback.result.d0;
            options.data.dk = callback.result.dk;
          }
        })
        .then(() => {
          axios
            .request(options)
            .then(response => {
              const decryptionResult = decryption(response.data, aes256Key);
              decryptionResult.then(decryptedResponseBody => {
                response.data = JSON.parse(decryptedResponseBody);
                if (response.status === 200) {
                  const responseData = { ...response.data };
                  if (
                    responseData.result.installation_id &&
                    responseData.result.installation_id.length > 0 &&
                    responseData.result.user_id &&
                    responseData.result.user_id.length > 0
                  ) {
                    const responseResult = {
                      hasError: false,
                      result: response.data
                    };
                    resolve(responseResult);
                  } else if (
                    responseData.result.error &&
                    responseData.result.error.length > 0
                  ) {
                    const responseResult = {
                      hasError: true,
                      multipleMsgKey: responseData.result.error
                    };
                    resolve(responseResult);
                  } else {
                    reject(
                      new Error("[verifyUserInfo]-[Error]:Fail to Verify User")
                    );
                  }
                } else {
                  reject(
                    new Error("[verifyUserInfo]-[Error]:Fail to Verify User")
                  );
                }
              });
            })
            .catch(error => {
              const responseResult = {
                hasError: true,
                error
              };
              reject(responseResult.error);
            });
        });
    } else {
      reject(
        new Error(
          "[verifyUserInfo]-[Error]:No authToken / masterKey / deviceModel"
        )
      );
    }
  });
};

/**
 * updateMasterKeyToServer
 * @description call 121Online API to keep MasterKey as backup in OnlineDB
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} masterKey - non-encrypted masterKey
 * @param {string} installationID - installationID from AXA
 * */
export const updateMasterKeyToServer = (
  authToken,
  masterKey,
  installationID,
  environment
) => {
  let url = "";
  let baseURL = "";
  if (Config.ENV_NAME === "PROD") {
    url = Config.STORE_KEYS_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    if (environment === "DEV") {
      url = Config.DEV_STORE_KEYS_URL;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      url = Config.SIT1_EAB_STORE_KEYS_URL;
      baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      url = Config.SIT2_EAB_STORE_KEYS_URL;
      baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      url = Config.SIT3_EAB_STORE_KEYS_URL;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      url = Config.SIT_AXA_STORE_KEYS_URL;
      baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      url = Config.UAT_STORE_KEYS_URL;
      baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      url = Config.PRE_PROD_STORE_KEYS_URL;
      baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    } else {
      url = Config.SIT3_EAB_STORE_KEYS_URL;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    }
  } else {
    url = Config.SIT3_EAB_STORE_KEYS_URL;
    baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      masterKey &&
      masterKey.length > 0 &&
      installationID &&
      installationID.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "put",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          master_key: masterKey,
          installation_id: installationID
        }
      };
      let aes256Key = "";
      const encryptResult = encryption(options.data, authToken, environment);
      encryptResult
        .then(callback => {
          if (callback.hasError) {
            reject(
              new Error(
                callback.errorMsg ? callback.errorMsg : "Cannot get Public Key"
              )
            );
          } else {
            aes256Key = callback.result.aesKey;
            options.data.d0 = callback.result.d0;
            options.data.dk = callback.result.dk;
          }
        })
        .then(() => {
          axios
            .request(options)
            .then(response => {
              const decryptionResult = decryption(response.data, aes256Key);
              decryptionResult.then(decryptedResponseBody => {
                response.data = JSON.parse(decryptedResponseBody);
                if (response.status === 200) {
                  const responseResult = { ...response.data };
                  if (responseResult.status === "success") {
                    resolve(response);
                  } else {
                    reject(
                      new Error(
                        "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
                      )
                    );
                  }
                } else {
                  reject(
                    new Error(
                      "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
                    )
                  );
                }
              });
            })
            .catch(error => {
              reject(
                new Error(
                  `[updateMasterKeyToServer]-[Response_Error]: ${error}`
                )
              );
            });
        });
    } else {
      reject(
        new Error(
          `[updateMasterKeyToServer]-No authToken / masterKey / installationID`
        )
      );
    }
  });
};

/**
 * verifyUser121Online
 * @description Authenticates the user by access token and user ID in online mode
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} userID - userID
 * @param {string} installationID - installationID
 * */
export const verifyUser121Online = (
  authToken,
  idToken,
  installationID,
  environment
) => {
  let url = "";
  let baseURL = "";
  if (Config.ENV_NAME === "PROD") {
    url = Config.VERIFY_USER_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    if (environment === "DEV") {
      url = Config.DEV_VERIFY_USER_URL;
      baseURL = Config.WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      url = Config.SIT1_EAB_VERIFY_USER_URL;
      baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      url = Config.SIT2_EAB_VERIFY_USER_URL;
      baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      url = Config.SIT3_EAB_VERIFY_USER_URL;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      url = Config.SIT_AXA_VERIFY_USER_URL;
      baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      url = Config.UAT_VERIFY_USER_URL;
      baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      url = Config.PRE_PROD_VERIFY_USER_URL;
      baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    } else {
      url = Config.DEV_VERIFY_USER_URL;
      baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
    }
  } else {
    url = Config.DEV_VERIFY_USER_URL;
    baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      installationID &&
      installationID.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          installation_id: installationID,
          id_token: idToken
        }
      };
      let aes256Key = "";
      const encryptResult = encryption(options.data, authToken, environment);
      encryptResult
        .then(callback => {
          if (callback.hasError) {
            reject(
              new Error(
                callback.errorMsg ? callback.errorMsg : "Cannot get public key"
              )
            );
          } else {
            aes256Key = callback.result.aesKey;
            options.data.d0 = callback.result.d0;
            options.data.dk = callback.result.dk;
          }
        })
        .then(() => {
          axios
            .request(options)
            .then(response => {
              const decryptionResult = decryption(response.data, aes256Key);
              decryptionResult.then(decryptedResponseBody => {
                response.data = JSON.parse(decryptedResponseBody);
                if (response.status === 200) {
                  const responseResult = { ...response.data };
                  if (responseResult.status === "success") {
                    resolve(response);
                  } else if (responseResult.message) {
                    resolve(response);
                  } else {
                    reject(
                      new Error(
                        "Fail to verify user by access token and user ID in online mode"
                      )
                    );
                  }
                } else {
                  reject(
                    new Error(
                      "Fail to verify user by access token and user ID in online mode"
                    )
                  );
                }
              });
            })
            .catch(error => {
              reject(
                new Error(`[verifyUser121Online]-[Response_Error]: ${error}`)
              );
            });
        });
    } else {
      reject(
        new Error(
          `[verifyUser121Online]-[No authToken / userID / installationID`
        )
      );
    }
  });
};
