import axios from "axios";

/**
 * getRemoteTextStore
 * @param {string} textStoreVersion - the local text store version
 * @param {string} token - the request access token
 * */
export default ({ textStoreVersion, token }) =>
  axios.request({
    headers: { "x-access-token": token },
    baseURL: "http://remoteTextStoreDomain.com/api/",
    url: `/textStore/${textStoreVersion}`
  });
