import { StyleSheet } from "react-native";
import Colors from "../../../../theme/colors";
import Theme from "../../../../theme";

const formRow = {
  flexDirection: "row",
  marginBottom: Theme.alignmentXS,
  width: "100%"
};
/**
 * styles
 * */
export default StyleSheet.create({
  dialogHeaderView: {
    ...Theme.dialogHeader,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-end",
    height: 112,
    width: "100%"
  },
  dialogHeader: {
    flexDirection: "column",
    borderBottomColor: Colors.coolGrey,
    borderBottomWidth: 1,
    paddingTop: 16,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 16
  },
  dialogContentWrapper: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonDone: {
    alignItems: "flex-end"
  },
  doneButton: {
    position: "absolute",
    right: 0
  },
  appBarButton: {
    justifyContent: "center"
  },
  documentRow: {
    flexDirection: "row",
    paddingTop: 16,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 16
  },
  textBox: {
    flex: 1,
    paddingLeft: 20
  },
  selector: {
    flex: 1
  },
  disappearTextBox: {
    flex: 1,
    opacity: 0
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentL,
    alignItems: "center",
    width: "100%"
  },
  scrollView: {
    backgroundColor: Theme.paleGrey,
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "flex-start",
    paddingVertical: Theme.alignmentXL,
    paddingHorizontal: 72,
    paddingBottom: Theme.alignmentXL + 100
  },
  formRow,
  formRowLast: {
    ...formRow,
    marginBottom: 0
  },
  sectionView: {
    width: "100%"
  },
  sectionContentView: {
    flexDirection: "row",
    height: 56,
    alignItems: "center"
  },
  sectionHeaderView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  sectionHeaderButton: {
    flex: 1
  },
  sectionHeaderButtonTextOuter: {
    alignItems: "flex-start"
  },
  sectionHeaderButtonText: {
    ...Theme.headerBrand,
    textAlign: "left"
  },
  tooltipsView: {
    flexDirection: "row",
    flex: 1
  },
  sectionHeaderText: {
    ...Theme.headerBrand,
    width: "95%"
  },
  supportingDocumentButton: {
    ...Theme.textButtonLabelSmallAccent,
    flex: 1
  },
  sectionText: {
    marginLeft: Theme.alignmentL,
    fontSize: Theme.fontSizeXXM,
    top: 5
  },
  otherDocumentView: {
    width: "100%"
  },
  analysisSectionBorderLine: {
    width: "100%",
    borderTopWidth: 1,
    borderColor: Theme.lightGrey,
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL
  },
  documentsIcon: {
    width: 36,
    height: 36,
    marginTop: Theme.alignmentXS
  },
  deleteIcon: {
    width: 24,
    height: 24
  },
  addOtherDocumentsButton: {
    color: Theme.brand,
    fontSize: Theme.fontSizeXXM
  },
  redWarning: {
    position: "absolute",
    left: -24 - Theme.alignmentL,
    width: 24,
    height: 24
  },
  sectionTitle: {
    marginBottom: Theme.alignmentL,
    left: -24 - Theme.alignmentL,
    ...Theme.headerBrand
  },
  redStar: {
    flex: 1,
    alignItems: "flex-start"
  },
  otherDocumentsSelectionHeaderView: {
    flex: 1,
    height: 35,
    marginTop: Theme.alignmentXS,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: Theme.brandTransparent
  },
  otherDocumentsSelectionDeleteButton: {
    marginRight: Theme.alignmentL,
    ...Theme.textButtonLabelSmallAccent,
    flex: 1
  },
  buttonGroup: {
    flex: 1,
    height: 35,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  warningText: {
    marginTop: Theme.alignmentXS,
    ...Theme.fieldTextOrHelperTextNegative
  },
  pendingText: {
    ...Theme.fieldTextOrHelperTextNegative,
    fontSize: Theme.fontSizeXM
  },
  flex1: {
    flex: 1
  },
  flex3: {
    flex: 3
  }
});
