import React, { PureComponent } from "react";
import {
  Alert,
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  NativeModules,
  NativeEventEmitter
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import { ContextConsumer } from "../../../../context";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import pdficon from "../../../../assets/images/pdf_blue.png";
import icDelete from "../../../../assets/images/icDelete.png";
import icWarning from "../../../../assets/images/icWarning.png";
import icCompleted from "../../../../assets/images/icCompleted.png";
import icIncomplete from "../../../../assets/images/icIncomplete.png";

import { getAttachmentWithID } from "../../../../utilities/DataManager";
import EABButton from "../../../../components/EABButton";
import EABDialog from "../../../../components/EABDialog";
import EABTextField from "../../../../components/EABTextField";
import { FORM } from "../../../../components/EABDialog/constants";
import { TEXT_BUTTON } from "../../../../components/EABButton/constants";
import TranslatedText from "../../../../containers/TranslatedText";
import { getSupportingDocumentsName } from "../../../../utilities/getOptionList";
import EABi18n from "../../../../utilities/EABi18n";
import SelectorField from "../../../../components/SelectorField";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import Theme from "../../../../theme";
import styles from "./styles";
import supportingDocumentUploadSelector from "../../../../utilities/supportingDocumentUploadSelector";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import getSumOfDocumentSize from "../../utilities/profileUsedSize";
/**
 * SupportingDocumentShieldDetails
 * @description SupportingDocumentShieldDetails page.
 * */

class SupportingDocumentShieldDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedProfile: "policyForm",
      isShowOtherDialog: false,
      otherDocumentName: "",
      customDocumentName: "",
      isEditMode: false,
      editTemplate: null,
      canShowAlert: false
    };
    this.selectedOtherDocumentsList = [];

    const eventEmitter = new NativeEventEmitter(
      NativeModules.DocumentPickerEventEmitter
    );
    this.importDocument = eventEmitter.addListener(
      "importDocument",
      options => {
        const { fileSize, fileName } = options.attachmentValues;
        const { textStore, navigation } = this.props;
        const { language } = options;
        const unit = fileSize.slice(-2);
        let canUpload = false;
        navigation.setParams({
          canDone: false
        });

        const fileType = fileName.slice(-3).toLocaleLowerCase();

        if (fileType !== "pdf" && fileType !== "jpg" && fileType !== "png") {
          Alert.alert(
            EABi18n({
              language,
              textStore,
              path: "clientForm.actionSheet.invalidFileFormat"
            })
          );
          return;
        }

        if (parseFloat(unit, 10)) {
          // Check for format size 'B', return integer or NaN
          canUpload = true;
        }
        if (unit === "KB") {
          canUpload = true;
        }
        if (unit === "MB") {
          if (parseFloat(fileSize, 10) <= 3) {
            canUpload = true;
          }
        }

        if (canUpload) {
          options.callback = () => {
            navigation.setParams({
              canDone: true
            });
          };
          props.onFileUpload(options);
          this.setCanShowAlert(true);
        } else {
          navigation.setParams({
            canDone: true
          });
          Alert.alert(
            EABi18n({
              language,
              textStore,
              path: "clientForm.actionSheet.fileSizeTooBig"
            })
          );
        }
      }
    );
    this.cancelImport = eventEmitter.addListener("cancelImport", () => {
      const { navigation } = this.props;
      navigation.setParams({
        canDone: true
      });
    });
  }

  componentDidUpdate(prevProps) {
    const { pendingSubmitList, navigation, error } = this.props;
    if (prevProps.pendingSubmitList !== pendingSubmitList) {
      navigation.setParams({
        canUpload: true
      });
    }
    if (prevProps.error !== error) {
      this.setSegmentedControl();
    }
  }

  componentWillUnmount() {
    const { navigation } = this.props;
    this.importDocument.remove();
    this.cancelImport.remove();
    navigation.setParams({
      docId: ""
    });
  }

  /**
   * targetProfileFieldName
   * @description return the first letter of the profile, ex. profile = policyForm, return "p"
   * @return {string}
   * */
  get targetProfileFieldName() {
    const { selectedProfile } = this.state;
    switch (selectedProfile) {
      case "policyForm":
        return "p";
      case "proposer":
        return "p";
      default:
        return "i";
    }
  }

  get isSubmitted() {
    const { submission, application } = this.props;
    return (
      submission.isSubmitted ||
      application.isSubmittedStatus ||
      application.submitStatus === "SUCCESS"
    );
  }

  get isReadOnly() {
    const { supportingDocument } = this.props;
    return supportingDocument.isReadOnly || false;
  }

  /**
   * getImagePreview
   * @description this easiest way to generate image preview
   * @return {obj}
   * */
  async getImagePreview(value) {
    const { application } = this.props;
    const base64 = await new Promise(resolve =>
      getAttachmentWithID(application.id, value.id, response => {
        if (response && response.data) {
          resolve(response.data);
        }
      })
    );
    this.setState({
      [value.id]: base64
    });
  }

  /**
   * getTargetProfile
   * @description return the target profile obj data
   * @return {obj}
   * */
  getTargetProfile(targetField) {
    const { supportingDocument } = this.props;
    const { selectedProfile } = this.state;
    return supportingDocument[targetField][selectedProfile];
  }

  /**
   * setSegmentedControl
   * @description set Segmented Control
   * @return {array}
   * */
  setSegmentedControl() {
    const { supportingDocument, navigation, application } = this.props;
    const { selectedProfile } = this.state;

    if (_.isEmpty(supportingDocument)) {
      return null;
    }
    const sortList = ["policyForm", "proposer"];
    const segmentedData = [];
    Object.entries(supportingDocument.template).forEach(([key, value]) => {
      segmentedData.push({
        key,
        title: value.tabName
      });
    });
    segmentedData.sort((prevData, currData) => {
      if (!prevData) {
        return 0;
      }
      const prevIndex = sortList.indexOf(prevData.key);
      const currIndex = sortList.indexOf(currData.key);
      if (prevIndex < 0) {
        return 1;
      }
      if (currIndex < 0) {
        return -1;
      }
      return prevIndex - currIndex;
    });
    const segmentedControlArray = [];
    segmentedData.forEach(value => {
      segmentedControlArray.push({
        key: value.key,
        title: value.title === "Policy Form" ? "Policy Forms" : value.title,
        isShowingBadge: true,
        source: this.checkHasError(value.key) ? icIncomplete : icCompleted,
        onPress: () => {
          this.setState(
            {
              selectedProfile: value.key
            },
            () => {
              navigation.setParams({
                showProfileTab: value.key
              });
            }
          );
        }
      });
    });
    navigation.setParams({
      docId: application.id,
      segmentedControlArray,
      showProfileTab: selectedProfile
    });
    return null;
  }

  setNavigationParams() {
    const {
      supportingDocument,
      navigation,
      saveSupportingDocument,
      saveSubmitedDocument,
      clearPendingList,
      getApplicationsList
    } = this.props;
    const { isSubmitted, isReadOnly } = this;

    if (_.isEmpty(supportingDocument)) {
      return null;
    }
    navigation.setParams({
      isReadOnly,
      isSubmitted,
      canDone: true,
      canUpload: false,
      isShield: true,
      clearPendingList,
      saveSupportingDocument,
      saveSubmitedDocument,
      getApplicationsList
    });
    return null;
  }

  setCanShowAlert(flag) {
    this.setState({ canShowAlert: flag });
  }

  /**
   * getFileName
   * @description return the name of the file filtered from optionsMap
   * @param {string} id the file id
   * @return {string}
   * */
  getFileName(id) {
    const { optionsMap, supportingDocument } = this.props;
    const { selectedProfile } = this.state;
    const docId = id.substr(0, 6);
    if (docId === "appPdf" && id.length > 6) {
      const profileId = id.substr(6, id.length);
      const profileName = supportingDocument.template[profileId].tabName;
      return `e-App (${profileName})`;
    } else if (
      docId === "appPdf" &&
      id.length <= 6 &&
      selectedProfile === "policyForm"
    ) {
      let name = "";
      if (supportingDocument.template.proposer) {
        name = supportingDocument.template.proposer.tabName;
      } else if (supportingDocument.suppDocsAppView) {
        name = supportingDocument.suppDocsAppView.pFullName;
      }
      return `e-App (${name})`;
    }
    const data = getSupportingDocumentsName({
      optionMap: optionsMap.suppDocsNames,
      titleKey: id
    });
    if (data.displayName === "e-PI") {
      return "Product Summary";
    }
    return data.displayName;
  }

  getTargetDocId(pdf) {
    const { application } = this.props;
    switch (pdf) {
      case "fnaReport":
        return application.bundleId;
      case "proposal":
        return application.quotationDocId;
      case "appPdf":
        return application.id;
      case "eCpdPdf":
        return application.id;
      default:
        return application.id;
    }
  }

  /**
   * getPolicyDocuments
   * @description return the policy document section
   * @return {object} component obj
   * */
  getPolicyDocuments() {
    const { navigation, supportingDocument, agent } = this.props;
    if (_.isEmpty(supportingDocument)) {
      return null;
    }
    const targetTemplate = this.getTargetProfile("template").tabContent;
    let templateKey = null;
    Object.keys(targetTemplate).forEach(key => {
      if (key === "sysDocs") {
        templateKey = key;
      }
    });
    // do not show this section if id "sysDocs" dont exists
    if (templateKey === null) {
      return null;
    }
    const policyDocumentsArr = [];
    let push = false;
    targetTemplate[templateKey].items.forEach(value => {
      push = true;
      const fileName = this.getFileName(value.id);
      const isFaChannel = _.get(agent, "channel.type", "") === "FA" || false;
      if (value.id === "fnaReport" && isFaChannel) {
        push = false;
      }
      if (push) {
        policyDocumentsArr.push(
          <ContextConsumer>
            {({
              showImageLoading,
              setSignDocImageSource,
              setImageViewerSource
            }) => (
              <View key={value.id} style={styles.sectionContentView}>
                <EABButton
                  style={styles.supportingDocumentButton}
                  onPress={() => {
                    showImageLoading(() => {
                      const docId = this.getTargetDocId(value.id);
                      if (docId) {
                        setImageViewerSource("");
                        getAttachmentWithID(docId, value.id, response => {
                          setSignDocImageSource(response.data, () => {
                            navigation.navigate({
                              routeName: "ImageViewer",
                              params: {
                                titleDisplay: fileName
                              }
                            });
                            setSignDocImageSource("");
                          });
                        });
                      }
                    });
                  }}
                >
                  <Image style={styles.documentsIcon} source={pdficon} />
                  <Text style={styles.sectionText}>{fileName}</Text>
                </EABButton>
              </View>
            )}
          </ContextConsumer>
        );
      }
    });
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <TranslatedText
              style={styles.sectionTitle}
              path="application.form.supportingDocument.systemDocument"
              language={language}
            />
            {policyDocumentsArr}
          </View>
        )}
      </ContextConsumer>
    );
  }

  getMandatoryDocuments() {
    const {
      navigation,
      textStore,
      supportingDocument,
      application,
      onFileUpload,
      deleteFile,
      pendingSubmitList
    } = this.props;
    const { selectedProfile } = this.state;
    if (_.isEmpty(supportingDocument)) {
      return null;
    }
    const idWarningList = [
      "pNric",
      "iNric",
      "pPass",
      "iPass",
      "pPassport",
      "iPassport",
      "pPassportWStamp",
      "iPassportWStamp",
      "thirdPartyID"
    ];
    const fieldArr = [];
    const targetTemplate = this.getTargetProfile("template").tabContent;

    Object.values(targetTemplate).forEach(value => {
      if (value.id === "mandDocs") {
        value.items.forEach(subValue => {
          fieldArr.push({
            field: subValue.id,
            title: subValue.title,
            toolTips: subValue.toolTips
          });
        });
      }
    });
    const maximumList = [];
    const isUploadedList = [];
    const mandatoryArr = [];
    const targetValue = this.getTargetProfile("values");
    Object.entries(fieldArr).forEach(([index, fieldValue]) => {
      const contentArr = _.get(
        targetValue,
        `mandDocs.${fieldValue.field}`,
        null
      );
      if (contentArr.length) {
        isUploadedList[index] = true;
      } else {
        isUploadedList[index] = false;
      }
      maximumList[index] = contentArr.length >= 8 || false;
      mandatoryArr.push(
        <ContextConsumer>
          {({ language }) => (
            <View style={styles.sectionView}>
              {index === "0" ? (
                <TranslatedText
                  style={styles.sectionTitle}
                  path="application.form.supportingDocument.mandatoryDocument"
                  language={language}
                />
              ) : null}
              <View style={styles.sectionHeaderView}>
                {!isUploadedList[index] ? (
                  <Image style={styles.redWarning} source={icWarning} />
                ) : null}
                <View style={styles.tooltipsView}>
                  <View style={styles.flex2}>
                    <Text style={styles.sectionHeaderText}>
                      {fieldValue.title.toUpperCase()}
                      <Text style={{ color: Theme.negative }}> *</Text>
                    </Text>
                  </View>
                  {fieldValue.toolTips ? (
                    <View style={styles.flex1}>
                      <PopoverTooltip
                        popoverOptions={{ preferredContentSize: [350, 130] }}
                        text={fieldValue.toolTips}
                      />
                    </View>
                  ) : null}
                </View>
                <EABButton
                  style={styles.supportingDocumentButton}
                  buttonType={TEXT_BUTTON}
                  isDisable={maximumList[index] || this.isReadOnly}
                  onPress={() => {
                    const options = {
                      applicationId: application.id,
                      valueLocation: {
                        sectionId: "mandDocs",
                        subSectionId: `${fieldValue.field}`
                      },
                      tabId: selectedProfile,
                      isSupervisorChannel: false
                    };

                    options.totalDocumentSize = getSumOfDocumentSize({
                      supportingDocument
                    });
                    supportingDocumentUploadSelector({
                      language,
                      textStore,
                      onStart: () => {
                        navigation.setParams({
                          canDone: false
                        });
                      },
                      onCancel: () => {
                        navigation.setParams({
                          canDone: true
                        });
                      },
                      onError: err => {
                        Alert.alert(err);
                        navigation.setParams({
                          canDone: true
                        });
                      },
                      options,
                      onChange: newValue => {
                        const docValue = this.setDocumentValue({
                          path: `mandDocs.${fieldValue.field}`,
                          newValue,
                          type: "values"
                        });
                        _.set(
                          options,
                          "attachmentValues",
                          docValue.map(value => ({
                            fileName: value.title,
                            fileSize: value.fileSize,
                            fileType: value.fileType,
                            length: value.length
                          }))[docValue.length - 1]
                        );
                        options.attachment = newValue.base64 || "";
                        options.callback = () => {
                          navigation.setParams({
                            canDone: true
                          });
                        };
                        onFileUpload(options);
                        this.setCanShowAlert(true);
                      }
                    });
                  }}
                >
                  <TranslatedText
                    style={{ color: Theme.brand }}
                    path="button.add"
                    language={language}
                  />
                </EABButton>
              </View>
              {maximumList[index] ? (
                <Text style={styles.warningText}>
                  {EABi18n({
                    path:
                      "application.form.supportingDocument.warningMessage,reachMaximumFiles",
                    language,
                    textStore
                  })}
                </Text>
              ) : null}
              {idWarningList.find(value => value === fieldValue.field) ? (
                <TranslatedText
                  style={styles.warningText}
                  path="application.form.supportingDocument.warningMessage.IDCopy"
                  language={language}
                />
              ) : null}
              {contentArr.map(subValue => (
                <ContextConsumer>
                  {({
                    showImageLoading,
                    setImageViewerSource,
                    setPDFViewerSource,
                    showPDFLoading
                  }) => {
                    if (subValue.fileType !== "application/pdf") {
                      this.getImagePreview(subValue);
                    }
                    return (
                      <View style={styles.sectionContentView}>
                        {subValue.fileType === "application/pdf" ? (
                          <Image
                            style={styles.documentsIcon}
                            source={pdficon}
                          />
                        ) : (
                          <Image
                            style={styles.documentsIcon}
                            source={{
                              uri: `data:${subValue.fileType};base64,${
                                this.state[subValue.id]
                              }`
                            }}
                          />
                        )}
                        <TouchableOpacity
                          style={styles.supportingDocumentButton}
                          onPress={() => {
                            if (subValue.fileType === "application/pdf") {
                              showPDFLoading();
                            } else {
                              showImageLoading();
                            }
                            getAttachmentWithID(
                              application.id,
                              subValue.id,
                              response => {
                                if (response && response.data) {
                                  if (subValue.fileType === "application/pdf") {
                                    setPDFViewerSource(
                                      `data:${subValue.fileType};base64,${
                                        response.data
                                      }`,
                                      () => {
                                        navigation.navigate({
                                          routeName: "PDFViewer",
                                          params: {
                                            titleDisplay: subValue.title
                                          }
                                        });
                                      }
                                    );
                                  } else {
                                    setImageViewerSource(
                                      `data:${subValue.fileType};base64,${
                                        response.data
                                      }`,
                                      () => {
                                        navigation.navigate({
                                          routeName: "ImageViewer",
                                          params: {
                                            titleDisplay: subValue.title
                                          }
                                        });
                                      }
                                    );
                                  }
                                }
                              }
                            );
                          }}
                        >
                          <View style={styles.sectionText}>
                            <Text style={Theme.bodyPrimary}>
                              {subValue.title}
                            </Text>
                            {pendingSubmitList.find(
                              value => value === subValue.id
                            ) ? (
                              <Text style={Theme.subheadSecondary}>
                                {subValue.fileSize}
                                <TranslatedText
                                  style={styles.pendingText}
                                  path="application.form.supportingDocument.pending"
                                  language={language}
                                />
                              </Text>
                            ) : (
                              <Text style={Theme.subheadSecondary}>
                                {subValue.fileSize}
                                <TranslatedText
                                  path={
                                    this.isSubmitted
                                      ? "application.form.supportingDocument.submittedOn"
                                      : "application.form.supportingDocument.uploadedOn"
                                  }
                                  language={language}
                                />
                                {subValue.feUploadDate}
                              </Text>
                            )}
                          </View>
                        </TouchableOpacity>
                        {!this.isSubmitted ? (
                          <EABButton
                            buttonType={TEXT_BUTTON}
                            isDisable={this.isReadOnly}
                            onPress={() => {
                              Alert.alert(
                                EABi18n({
                                  path: "alert.warning",
                                  language,
                                  textStore
                                }).toUpperCase(),
                                EABi18n({
                                  path: "alert.confirmDelete",
                                  language,
                                  textStore
                                }),
                                [
                                  {
                                    text: EABi18n({
                                      path: "button.confirm",
                                      language,
                                      textStore
                                    }),
                                    onPress: () =>
                                      deleteFile({
                                        applicationId: application.id,
                                        attachmentId: subValue.id,
                                        valueLocation: {
                                          sectionId: "mandDocs",
                                          subSectionId: `${fieldValue.field}`
                                        },
                                        tabId: selectedProfile
                                      })
                                  },
                                  {
                                    text: EABi18n({
                                      path: "button.cancel",
                                      language,
                                      textStore
                                    }),
                                    onPress: () => {}
                                  }
                                ],
                                { cancelable: false }
                              );
                            }}
                          >
                            <Image
                              style={styles.deleteIcon}
                              source={icDelete}
                            />
                          </EABButton>
                        ) : null}
                      </View>
                    );
                  }}
                </ContextConsumer>
              ))}
              <View style={styles.analysisSectionBorderLine} />
            </View>
          )}
        </ContextConsumer>
      );
    });

    return mandatoryArr;
  }

  getOptionalDocumentToolTips(title, id, language) {
    const { textStore } = this.props;
    const { targetProfileFieldName } = this;
    let targetTemplate = "";
    let path = "";
    let text = null;
    switch (id) {
      case `${targetProfileFieldName}MedicalTest`:
        path = "application.form.supportingDocument.tooltip.medicalReport";
        break;
      case `${targetProfileFieldName}ChinaVisit`:
        path = "application.form.supportingDocument.tooltip.chinaVisit";
        break;
      case `${targetProfileFieldName}JuvenileQuestions`:
        targetTemplate = this.getTargetProfile("template").tabContent.optDoc
          .items;
        targetTemplate.some(value => {
          if (value.id === id) {
            text = value.toolTips;
          }
          return text;
        });
        break;
      default:
        break;
    }
    return (
      <View style={styles.tooltipsView}>
        <Text style={styles.sectionHeaderText}>{title}</Text>
        {path ? (
          <PopoverTooltip
            popoverOptions={{ preferredContentSize: [350, 130] }}
            text={EABi18n({
              textStore,
              language,
              path
            })}
          />
        ) : null}
        {text ? (
          <PopoverTooltip
            popoverOptions={{ preferredContentSize: [350, 130] }}
            text={text}
          />
        ) : null}
      </View>
    );
  }

  /**
   * getOptionalDocument
   * @description return the other document section
   * @return {object} component obj
   * */
  getOptionalDocument() {
    const {
      supportingDocument,
      textStore,
      navigation,
      application,
      onFileUpload
    } = this.props;
    const { selectedProfile } = this.state;
    if (_.isEmpty(supportingDocument)) {
      return null;
    }
    const targetTemplate = this.getTargetProfile("template").tabContent;
    let sectionHeaderGroup = null;
    Object.values(targetTemplate).some(value => {
      if (value.id === "optDoc") {
        sectionHeaderGroup = value.items;
      }
      return sectionHeaderGroup;
    });
    // do not show this section if id "otherDoc" dont exists
    if (sectionHeaderGroup === null) {
      return null;
    }
    const optionalDocumentsSelectionArr = [];
    if (sectionHeaderGroup !== null) {
      sectionHeaderGroup.forEach(template => {
        optionalDocumentsSelectionArr.push(
          <ContextConsumer>
            {({ language }) => (
              <View>
                <View style={styles.otherDocumentsSelectionHeaderView}>
                  {this.getOptionalDocumentToolTips(
                    template.title,
                    template.id,
                    language
                  )}
                  <EABButton
                    style={styles.supportingDocumentButton}
                    isDisable={this.isReadOnly}
                    buttonType={TEXT_BUTTON}
                    onPress={() => {
                      const options = {
                        applicationId: application.id,
                        valueLocation: {
                          sectionId: "optDoc",
                          subSectionId: `${template.id}`
                        },
                        tabId: selectedProfile,
                        isSupervisorChannel: false
                      };

                      options.totalDocumentSize = getSumOfDocumentSize({
                        supportingDocument
                      });

                      supportingDocumentUploadSelector({
                        language,
                        textStore,
                        onStart: () => {
                          navigation.setParams({
                            canDone: false
                          });
                        },
                        onCancel: () => {
                          navigation.setParams({
                            canDone: true
                          });
                        },
                        onError: err => {
                          Alert.alert(err);
                          navigation.setParams({
                            canDone: true
                          });
                        },
                        options,
                        onChange: newValue => {
                          const docValue = this.setDocumentValue({
                            path: `optDoc.${template.id}`,
                            newValue,
                            type: "values"
                          });
                          _.set(
                            options,
                            "attachmentValues",
                            docValue.map(value => ({
                              fileName: value.title,
                              fileSize: value.fileSize,
                              fileType: value.fileType,
                              length: value.length
                            }))[docValue.length - 1]
                          );
                          options.attachment = newValue.base64 || "";
                          options.callback = () => {
                            navigation.setParams({
                              canDone: true
                            });
                          };
                          onFileUpload(options);
                          this.setCanShowAlert(true);
                        }
                      });
                    }}
                  >
                    <TranslatedText
                      style={{ color: Theme.brand }}
                      path="button.add"
                      language={language}
                    />
                  </EABButton>
                </View>
                {this.optionalDocumentsSelectionContent({
                  templateId: template.id
                })}
              </View>
            )}
          </ContextConsumer>
        );
      });
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <View style={styles.sectionHeaderView}>
              <TranslatedText
                style={styles.sectionTitle}
                path="application.form.supportingDocument.optionalDocument"
                language={language}
              />
            </View>
            {optionalDocumentsSelectionArr}
          </View>
        )}
      </ContextConsumer>
    );
  }

  getOtherDocumentDialog(language) {
    const {
      isShowOtherDialog,
      selectedProfile,
      otherDocumentName,
      customDocumentName,
      isEditMode,
      editTemplate
    } = this.state;
    const {
      addOtherDocument,
      editOtherDocumentSection,
      optionsMap,
      application,
      textStore
    } = this.props;
    let pathName = "clientForm.actionSheet.addOtherDocument";
    this.language = language;
    if (!otherDocumentName || otherDocumentName === "") {
      this.setState({
        otherDocumentName: _.get(optionsMap, "otherDocNames.la[0].title")
      });
    }
    const textStyle = [styles.textBox];
    const isCustomDocument = otherDocumentName === "Other";
    if (!isCustomDocument) {
      textStyle.push(styles.disappearTextBox);
    }

    const optionList = optionsMap.otherDocNames.la.map((ele, index) => ({
      label: ele.title,
      value: ele.title,
      key: index
    }));

    if (isEditMode) {
      pathName = "clientForm.actionSheet.editOtherDocument";
      if (this.initEdit) {
        if (optionList.find(option => option.label === editTemplate.title)) {
          this.setState({
            otherDocumentName: editTemplate.title
          });
        } else if (customDocumentName.length === 0) {
          // Only update when customDocument is empty
          this.setState({
            otherDocumentName: "Other",
            customDocumentName: editTemplate.title
          });
        }
        this.initEdit = false;
      }
    }

    return (
      <EABDialog type={FORM} isOpen={isShowOtherDialog}>
        <View style={styles.dialogHeader}>
          <View style={styles.dialogContentWrapper}>
            <EABButton
              onPress={() => {
                this.setState({
                  isShowOtherDialog: false,
                  otherDocumentName: "",
                  customDocumentName: "",
                  isEditMode: false
                });
              }}
            >
              <TranslatedText
                style={styles.headerButton}
                path="button.cancel"
              />
            </EABButton>
            <TranslatedText style={styles.headerMiddleText} path={pathName} />
            <EABButton
              isDisable={isCustomDocument && customDocumentName.length === 0}
              onPress={() => {
                let errorPath = "";
                let newDocName = "";

                if (!isCustomDocument) {
                  newDocName = otherDocumentName;
                } else {
                  newDocName = customDocumentName;
                }

                if (this.selectedOtherDocumentsList.includes(newDocName)) {
                  errorPath = "clientForm.actionSheet.dulplicateDocumentName";
                } else if (isEditMode) {
                  editOtherDocumentSection({
                    applicationId: application.id,
                    documentId: editTemplate.id,
                    docName: newDocName,
                    tabId: selectedProfile
                  });
                } else {
                  addOtherDocument({
                    appId: application.id,
                    docName: newDocName,
                    docNameOption: newDocName,
                    tabId: selectedProfile
                  });
                }

                if (errorPath.length > 0) {
                  Alert.alert(
                    EABi18n({
                      language,
                      textStore,
                      path: "alert.warning"
                    }),
                    EABi18n({
                      language,
                      textStore,
                      path: errorPath
                    })
                  );
                } else {
                  this.setState({
                    isShowOtherDialog: false,
                    otherDocumentName: "",
                    customDocumentName: "",
                    isEditMode: false
                  });
                }
              }}
            >
              <TranslatedText style={styles.headerButton} path="button.done" />
            </EABButton>
          </View>
        </View>
        <View style={styles.documentRow}>
          <SelectorField
            style={styles.selector}
            value={otherDocumentName}
            selectorType={FLAT_LIST}
            placeholder={EABi18n({
              language,
              textStore,
              path: "clientForm.actionSheet.selected"
            })}
            flatListOptions={{
              renderItemOnPress: (itemKey, item) => {
                this.setState({ otherDocumentName: item.value });
              },
              data: optionList
            }}
          />

          <EABTextField
            style={textStyle}
            editable={isCustomDocument}
            value={customDocumentName}
            isError={customDocumentName.length === 0}
            maxLength={20}
            hintText={EABi18n({
              path: "error.302",
              language,
              textStore
            })}
            onChange={newValue => {
              this.setState({ customDocumentName: newValue });
            }}
          />
        </View>
      </EABDialog>
    );
  }

  /**
   * getOtherDocument
   * @description return the other document section
   * @return {object} component obj
   * */
  getOtherDocument() {
    const {
      supportingDocument,
      textStore,
      navigation,
      application,
      onFileUpload,
      deleteOtherDocumentSection
    } = this.props;
    const { selectedProfile } = this.state;
    const selectedOtherDocumentsList = [];
    if (_.isEmpty(supportingDocument)) {
      return null;
    }

    const targetTemplate = this.getTargetProfile("template").tabContent;

    let sctionExist = false;
    Object.values(targetTemplate).some(value => {
      if (value.id === "otherDoc") {
        sctionExist = true;
      }
      return sctionExist;
    });
    // do not show this section if id "pNric" dont exists
    if (!sctionExist) {
      return null;
    }

    const targetValue = this.getTargetProfile("values");
    const otherDoc = _.get(targetValue, "otherDoc", null);
    const otherDocumentsSelectionArr = [];
    if (otherDoc !== null) {
      otherDoc.template.forEach(template => {
        selectedOtherDocumentsList.push(template.docName);
        const isMaximumFile = (otherDoc.values[template.id] || []).length > 7;
        otherDocumentsSelectionArr.push(
          <ContextConsumer>
            {({ language }) => (
              <View>
                <View style={styles.otherDocumentsSelectionHeaderView}>
                  <EABButton
                    style={styles.sectionHeaderButton}
                    onPress={() => {
                      this.initEdit = true;
                      this.setState({
                        isShowOtherDialog: true,
                        isEditMode: true,
                        editTemplate: template
                      });
                    }}
                  >
                    <View style={styles.sectionHeaderButtonTextOuter}>
                      <Text style={styles.sectionHeaderButtonText}>
                        {template.docName}
                      </Text>
                    </View>
                  </EABButton>
                  <View style={styles.buttonGroup}>
                    {!this.isSubmitted ? (
                      <EABButton
                        style={styles.otherDocumentsSelectionDeleteButton}
                        buttonType={TEXT_BUTTON}
                        isDisable={isMaximumFile || this.isReadOnly}
                        onPress={() => {
                          Alert.alert(
                            EABi18n({
                              path: "alert.warning",
                              language,
                              textStore
                            }).toUpperCase(),
                            EABi18n({
                              path: "alert.confirmDelete",
                              language,
                              textStore
                            }),
                            [
                              {
                                text: EABi18n({
                                  path: "button.confirm",
                                  language,
                                  textStore
                                }),
                                onPress: () =>
                                  deleteOtherDocumentSection({
                                    applicationId: application.id,
                                    documentId: template.id,
                                    tabId: selectedProfile
                                  })
                              },
                              {
                                text: EABi18n({
                                  path: "button.cancel",
                                  language,
                                  textStore
                                }),
                                onPress: () => {}
                              }
                            ],
                            { cancelable: false }
                          );
                        }}
                      >
                        <Image style={styles.deleteIcon} source={icDelete} />
                      </EABButton>
                    ) : (
                      <View
                        style={styles.otherDocumentsSelectionDeleteButton}
                      />
                    )}
                    <EABButton
                      style={styles.supportingDocumentButton}
                      buttonType={TEXT_BUTTON}
                      isDisable={isMaximumFile || this.isReadOnly}
                      onPress={() => {
                        const options = {
                          applicationId: application.id,
                          valueLocation: {
                            sectionId: "otherDoc",
                            subSectionId: `${template.id}`
                          },
                          tabId: selectedProfile,
                          isSupervisorChannel: false
                        };

                        options.totalDocumentSize = getSumOfDocumentSize({
                          supportingDocument
                        });

                        supportingDocumentUploadSelector({
                          language,
                          textStore,
                          onStart: () => {
                            navigation.setParams({
                              canDone: false
                            });
                          },
                          onCancel: () => {
                            navigation.setParams({
                              canDone: true
                            });
                          },
                          onError: err => {
                            Alert.alert(err);
                            navigation.setParams({
                              canDone: true
                            });
                          },
                          options,
                          onChange: newValue => {
                            const docValue = this.setDocumentValue({
                              path: `otherDoc.values.${template.id}`,
                              newValue,
                              type: "values"
                            });
                            _.set(
                              options,
                              "attachmentValues",
                              docValue.map(value => ({
                                fileName: value.title,
                                fileSize: value.fileSize,
                                fileType: value.fileType,
                                length: value.length
                              }))[docValue.length - 1]
                            );
                            options.attachment = newValue.base64 || "";
                            options.callback = () => {
                              navigation.setParams({
                                canDone: true
                              });
                            };
                            onFileUpload(options);
                            this.setCanShowAlert(true);
                          }
                        });
                      }}
                    >
                      <TranslatedText
                        style={{ color: Theme.brand }}
                        path="button.add"
                        language={language}
                      />
                    </EABButton>
                  </View>
                </View>
                {isMaximumFile ? (
                  <Text style={styles.warningText}>
                    {EABi18n({
                      path:
                        "application.form.supportingDocument.warningMessage,reachMaximumFiles",
                      language,
                      textStore
                    })}
                  </Text>
                ) : null}
                {this.otherDocumentsSelectionContent({
                  content: otherDoc.values,
                  templateId: template.id
                })}
              </View>
            )}
          </ContextConsumer>
        );
      });
    }
    this.selectedOtherDocumentsList = selectedOtherDocumentsList;
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <View style={styles.sectionHeaderView}>
              <Text style={styles.sectionTitle}>
                {EABi18n({
                  path: "application.form.supportingDocument.otherDocuments",
                  language,
                  textStore
                })}
              </Text>
              <EABButton
                isDisable={this.isReadOnly}
                style={styles.supportingDocumentButton}
                buttonType={TEXT_BUTTON}
                onPress={() => {
                  this.setState({ isShowOtherDialog: true });
                }}
              >
                <TranslatedText
                  style={{ color: Theme.brand }}
                  path="button.add"
                  language={language}
                />
              </EABButton>
            </View>
            {otherDocumentsSelectionArr.length ? (
              otherDocumentsSelectionArr
            ) : (
              <Text style={styles.sectionText}>
                {EABi18n({
                  path: "application.form.supportingDocument.noRecord",
                  language,
                  textStore
                })}
              </Text>
            )}
          </View>
        )}
      </ContextConsumer>
    );
  }

  /**
   * setDocumentValue
   * @description setting the new value into target array with target index
   * @return {array}
   * */
  setDocumentValue({ path, newValue, type }) {
    const data = _.cloneDeep(_.get(this.getTargetProfile("values"), path, []));
    if (type === "template") {
      data.push({
        docName: newValue,
        docNameOption: newValue,
        id: `${newValue.fileName.replace(/ /g, "")}_${new Date().getTime()}`,
        title: newValue,
        type: "subSection"
      });
    } else if (type === "values") {
      const timestamp = newValue.timestamp || new Date().getTime();
      const fileName = newValue.fileName
        ? `${newValue.fileName.replace(/ /g, "")}_${new Date().getTime()}`
        : timestamp;
      data.push({
        feUploadDate: timestamp || new Date().getTime(),
        fileSize: newValue.fileSize || 0,
        fileType: newValue.type || "image/png",
        id: fileName,
        length: 13236,
        title: fileName,
        uploadDate: timestamp
      });
    }
    return data;
  }

  checkHasError(selectedProfile) {
    const { error } = this.props;
    let hasError = false;
    Object.entries(error).some(([profileName, field]) => {
      if (profileName === selectedProfile) {
        Object.values(field).some(value => {
          if (value.hasError) {
            hasError = true;
          }
          return hasError;
        });
      }
      return hasError;
    });
    return hasError;
  }

  /**
   * deleteDocumentValue
   * @description delete the target data inside an array
   * */
  deleteDocumentValue({ path, id, options }) {
    const values = this.getTargetProfile("values");
    const extraPath = _.get(options, "extraPath", null);
    let targetIndex = null;
    if (extraPath !== null) {
      const motherObj = _.get(values, path, null);
      if (motherObj !== null) {
        Object.keys(motherObj).forEach(key => {
          extraPath.forEach(ePath => {
            if (key === ePath && ePath === "template") {
              motherObj.template.find((value, index) => {
                if (value.id === id) {
                  targetIndex = index;
                }
                return null;
              });
              if (targetIndex !== null) {
                motherObj.template.splice(targetIndex, 1);
              }
            } else if (key === ePath && ePath === "values") {
              delete motherObj.values[id];
            }
          });
        });
        return motherObj;
      }
      return [];
    }
    const template = _.get(values, path, []);
    template.find((value, index) => {
      if (value.id === id) {
        targetIndex = index;
      }
      return null;
    });
    if (targetIndex !== null) {
      template.splice(targetIndex, 1);
    }
    return template;
  }

  /**
   * otherDocumentsSelectionContent
   * @description return the other documents selection section
   * @return {object} component obj
   * */
  otherDocumentsSelectionContent({ content, templateId }) {
    const {
      textStore,
      deleteFile,
      application,
      navigation,
      pendingSubmitList
    } = this.props;
    const { selectedProfile } = this.state;
    const selectionContentArr = [];
    if (_.isEmpty(content) || _.isEmpty(content[templateId])) {
      return null;
    }
    content[templateId].forEach(value => {
      if (value.fileType !== "application/pdf") {
        this.getImagePreview(value);
      }
      selectionContentArr.push(
        <ContextConsumer>
          {({
            language,
            showPDFLoading,
            setPDFViewerSource,
            showImageLoading,
            setImageViewerSource
          }) => (
            <View style={styles.sectionContentView}>
              {value.fileType === "application/pdf" ? (
                <Image style={styles.documentsIcon} source={pdficon} />
              ) : (
                <Image
                  style={styles.documentsIcon}
                  source={{
                    uri: `data:${value.fileType};base64,${this.state[value.id]}`
                  }}
                />
              )}
              <TouchableOpacity
                style={styles.supportingDocumentButton}
                onPress={() => {
                  if (value.fileType === "application/pdf") {
                    showPDFLoading();
                  } else {
                    showImageLoading();
                  }
                  getAttachmentWithID(application.id, value.id, response => {
                    if (response && response.data) {
                      if (value.fileType === "application/pdf") {
                        setPDFViewerSource(
                          `data:${value.fileType};base64,${response.data}`,
                          () => {
                            navigation.navigate({
                              routeName: "PDFViewer",
                              params: {
                                titleDisplay: value.title
                              }
                            });
                          }
                        );
                      } else {
                        setImageViewerSource(
                          `data:${value.fileType};base64,${response.data}`,
                          () => {
                            navigation.navigate({
                              routeName: "ImageViewer",
                              params: {
                                titleDisplay: value.title
                              }
                            });
                          }
                        );
                      }
                    }
                  });
                }}
              >
                <View style={styles.sectionText}>
                  <Text style={Theme.bodyPrimary}>{value.title}</Text>
                  {pendingSubmitList.find(
                    pendingValue => pendingValue === value.id
                  ) ? (
                    <Text style={Theme.subheadSecondary}>
                      {value.fileSize}
                      <TranslatedText
                        style={styles.pendingText}
                        path="application.form.supportingDocument.pending"
                        language={language}
                      />
                    </Text>
                  ) : (
                    <Text style={Theme.subheadSecondary}>
                      {value.fileSize}
                      <TranslatedText
                        path={
                          this.isSubmitted
                            ? "application.form.supportingDocument.submittedOn"
                            : "application.form.supportingDocument.uploadedOn"
                        }
                        language={language}
                      />
                      {value.feUploadDate}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              {!this.isSubmitted ? (
                <EABButton
                  buttonType={TEXT_BUTTON}
                  isDisable={this.isReadOnly}
                  onPress={() => {
                    Alert.alert(
                      EABi18n({
                        path: "alert.warning",
                        language,
                        textStore
                      }).toUpperCase(),
                      EABi18n({
                        path: "alert.confirmDelete",
                        language,
                        textStore
                      }),
                      [
                        {
                          text: EABi18n({
                            path: "button.confirm",
                            language,
                            textStore
                          }),
                          onPress: () =>
                            deleteFile({
                              applicationId: application.id,
                              attachmentId: value.id,
                              valueLocation: {
                                sectionId: "otherDoc",
                                subSectionId: templateId
                              },
                              tabId: selectedProfile
                            })
                        },
                        {
                          text: EABi18n({
                            path: "button.cancel",
                            language,
                            textStore
                          }),
                          onPress: () => {}
                        }
                      ],
                      { cancelable: false }
                    );
                  }}
                >
                  <Image style={styles.deleteIcon} source={icDelete} />
                </EABButton>
              ) : null}
            </View>
          )}
        </ContextConsumer>
      );
    });
    return selectionContentArr;
  }

  /**
   * optionalDocumentsSelectionContent
   * @description return the other documents selection section
   * @return {object} component obj
   * */
  optionalDocumentsSelectionContent({ templateId }) {
    const {
      textStore,
      deleteFile,
      application,
      navigation,
      pendingSubmitList
    } = this.props;
    const { selectedProfile } = this.state;
    const selectionContentArr = [];
    const targetValue = this.getTargetProfile("values");
    const optDoc = _.get(targetValue, "optDoc", null);
    if (_.isEmpty(optDoc) || _.isEmpty(optDoc[templateId])) {
      return null;
    }
    optDoc[templateId].forEach(value => {
      if (value.fileType !== "application/pdf") {
        this.getImagePreview(value);
      }
      selectionContentArr.push(
        <ContextConsumer>
          {({
            language,
            showImageLoading,
            setImageViewerSource,
            showPDFLoading,
            setPDFViewerSource
          }) => (
            <View style={styles.sectionContentView}>
              {value.fileType === "application/pdf" ? (
                <Image style={styles.documentsIcon} source={pdficon} />
              ) : (
                <Image
                  style={styles.documentsIcon}
                  source={{
                    uri: `data:${value.fileType};base64,${this.state[value.id]}`
                  }}
                />
              )}
              <TouchableOpacity
                style={styles.supportingDocumentButton}
                onPress={() => {
                  if (value.fileType === "application/pdf") {
                    showPDFLoading();
                  } else {
                    showImageLoading();
                  }
                  getAttachmentWithID(application.id, value.id, response => {
                    if (response && response.data) {
                      if (value.fileType === "application/pdf") {
                        setPDFViewerSource(
                          `data:${value.fileType};base64,${response.data}`,
                          () => {
                            navigation.navigate({
                              routeName: "PDFViewer",
                              params: {
                                titleDisplay: value.title
                              }
                            });
                          }
                        );
                      } else {
                        setImageViewerSource(
                          `data:${value.fileType};base64,${response.data}`,
                          () => {
                            navigation.navigate({
                              routeName: "ImageViewer",
                              params: {
                                titleDisplay: value.title
                              }
                            });
                          }
                        );
                      }
                    }
                  });
                }}
              >
                <View style={styles.sectionText}>
                  <Text style={Theme.bodyPrimary}>{value.title}</Text>
                  {pendingSubmitList.find(
                    pendingValue => pendingValue === value.id
                  ) ? (
                    <Text style={Theme.subheadSecondary}>
                      {value.fileSize}
                      <TranslatedText
                        style={styles.pendingText}
                        path="application.form.supportingDocument.pending"
                        language={language}
                      />
                    </Text>
                  ) : (
                    <Text style={Theme.subheadSecondary}>
                      {value.fileSize}
                      <TranslatedText
                        path={
                          this.isSubmitted
                            ? "application.form.supportingDocument.submittedOn"
                            : "application.form.supportingDocument.uploadedOn"
                        }
                        language={language}
                      />
                      {value.feUploadDate}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              {!this.isSubmitted ? (
                <EABButton
                  style={styles.supportingDocumentButton}
                  buttonType={TEXT_BUTTON}
                  isDisable={this.isReadOnly}
                  onPress={() => {
                    Alert.alert(
                      EABi18n({
                        path: "alert.warning",
                        language,
                        textStore
                      }).toUpperCase(),
                      EABi18n({
                        path: "alert.confirmDelete",
                        language,
                        textStore
                      }),
                      [
                        {
                          text: EABi18n({
                            path: "button.confirm",
                            language,
                            textStore
                          }),
                          onPress: () =>
                            deleteFile({
                              applicationId: application.id,
                              attachmentId: value.id,
                              valueLocation: {
                                sectionId: "optDoc",
                                subSectionId: templateId
                              },
                              tabId: selectedProfile
                            })
                        },
                        {
                          text: EABi18n({
                            path: "button.cancel",
                            language,
                            textStore
                          }),
                          onPress: () => {}
                        }
                      ],
                      { cancelable: false }
                    );
                  }}
                >
                  <Image style={styles.deleteIcon} source={icDelete} />
                </EABButton>
              ) : null}
            </View>
          )}
        </ContextConsumer>
      );
    });
    return selectionContentArr;
  }

  render() {
    const { navigation, supportingDocument, textStore } = this.props;
    const { canShowAlert } = this.state;
    const docId = navigation.getParam("docId", "");
    if (docId !== this.props.application.id) {
      this.setSegmentedControl();
      this.setNavigationParams();
    }
    if (supportingDocument.exceededMaximumFileSize && canShowAlert) {
      Alert.alert(
        EABi18n({
          language: this.language,
          textStore,
          path: "clientForm.actionSheet.exceededMaximumFileSize"
        })
      );
      this.setCanShowAlert(false);
    }
    const getMandatoryDocuments = this.getMandatoryDocuments();
    const policyDocumentsSection = this.getPolicyDocuments();
    const optionalDocument = this.getOptionalDocument();
    const otherDocumentSection = this.getOtherDocument();

    return (
      <ContextConsumer>
        {({ language, isPDFLoading, isImageLoading }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isPDFLoading || isImageLoading} />
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              {this.getOtherDocumentDialog(language)}

              {policyDocumentsSection}
              {policyDocumentsSection ? (
                <View style={styles.analysisSectionBorderLine} />
              ) : null}
              {getMandatoryDocuments}
              {optionalDocument}
              {optionalDocument ? (
                <View style={styles.analysisSectionBorderLine} />
              ) : null}
              {otherDocumentSection}
            </ScrollView>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

SupportingDocumentShieldDetails.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  supportingDocument:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.SUPPORTING_DOCUMENT].SupportingDocumentData
      .isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.SUPPORTING_DOCUMENT].error.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[REDUCER_TYPES.OPTIONS_MAP].isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  getSupportingDocument: PropTypes.func.isRequired,
  getApplicationsList: PropTypes.func.isRequired,
  onFileUpload: PropTypes.func.isRequired,
  addOtherDocument: PropTypes.func.isRequired,
  pendingSubmitList: PropTypes.func.isRequired,
  deleteFile: PropTypes.func.isRequired,
  saveSupportingDocument: PropTypes.func.isRequired,
  editOtherDocumentSection: PropTypes.func.isRequired,
  deleteOtherDocumentSection: PropTypes.func.isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  saveSubmitedDocument: PropTypes.func.isRequired,
  clearPendingList: PropTypes.func.isRequired,
  submission:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
      .isRequired
};

export default SupportingDocumentShieldDetails;
