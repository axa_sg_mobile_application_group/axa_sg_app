import React, { PureComponent } from "react";
import {
  Alert,
  NetInfo,
  Linking,
  NativeEventEmitter,
  NativeModules
} from "react-native";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import Config from "react-native-config";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import Theme from "../../../../theme";
import styles from "./styles";

export default class UploadButtonAfterSubmitted extends PureComponent {
  constructor(props) {
    super(props);
    const dataSync = new NativeEventEmitter(NativeModules.DataSyncEventManager);
    this.dataSyncListener = dataSync.addListener(
      "DataSyncEventHandler",
      data => {
        if (data === "SUCCESS") {
          const { setParams } = this.props.navigation;
          setParams({
            canUpload: false
          });
          Alert.alert("NOTICE", "Files have been submitted");
        }
      }
    );

    this.state = {
      isNetworkConnected: false,
      getAuthCodeUrl: this.getEnvUrl()
    };
    // listen to network connection => isConnected: ${bool}
    NetInfo.isConnected.addEventListener("connectionChange", isConnected =>
      this.setState({ isNetworkConnected: isConnected })
    );
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({
        isNetworkConnected: isConnected
      });
    });
    this.getAuthCodeCallback = this.getAuthCodeCallback.bind(this);
  }

  componentDidMount() {
    Linking.addEventListener("url", this.getAuthCodeCallback);
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.getAuthCodeCallback);
    this.dataSyncListener.remove();
    this.dataSyncListener = null;
  }

  getEnvUrl() {
    const { environment } = this.props;
    if (Config.ENV_NAME === "PROD") {
      return Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      return Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      return Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      return Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      return Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      return Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      return Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      return Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    }
    return Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
  }

  getAuthCodeCallback(response) {
    const {
      apiPage,
      setOnlineAuthCodeForPayment,
      isOnlineAuthCompleted
    } = this.props;
    if (response.url && apiPage === "uploadButton") {
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (!this.props.isHandlingListener) {
        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            if (callback.hasError) {
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error]  ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(`error msg: Authorization Failed (Error 001)`);
                }, 200);
              }
            } else if (isOnlineAuthCompleted) {
              this.preformUpload();
            }
          }
        });
      }
    }
  }

  pressHandler() {
    const { getAuthCodeUrl } = this.state;
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: getAuthCodeUrl
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  checkTokenBeforeUpload(language) {
    const {
      loginByOnlineAuthForPayment,
      textStore,
      checkTokenExpiryDate
    } = this.props;
    const { isNetworkConnected } = this.state;

    checkTokenExpiryDate(status => {
      // status = false if its need to do online auth
      if (!status) {
        const title = EABi18n({
          language,
          textStore,
          path: "button.upload"
        });
        const warningMessage = EABi18n({
          language,
          textStore,
          path: "application.form.supportingDocument.onlineAuthWarning"
        });
        loginByOnlineAuthForPayment();
        Alert.alert(
          title,
          warningMessage,
          [
            {
              text: "Continue",
              onPress: () => this.pressHandler()
            },
            {
              text: "Cancel",
              onPress: () => {},
              style: "cancel"
            }
          ],
          { cancelable: false }
        );
      } else if (!isNetworkConnected) {
        Alert.alert(
          EABi18n({
            language,
            textStore,
            path: "application.form.supportingDocument.networkDisconnected"
          })
        );
      } else {
        this.preformUpload();
      }
    });
  }

  preformUpload() {
    const { dataSync } = this.props;
    const { state } = this.props.navigation;
    const { params } = state;
    params.saveSubmitedDocument({
      appId: params.docId,
      isSupervisorChannel: false,
      callback: () => {
        dataSync();
      }
    });
  }

  render() {
    const { setApiResponsePage, textStore } = this.props;
    const { isNetworkConnected } = this.state;
    const { state } = this.props.navigation;
    const { params } = state;
    return (
      <ContextConsumer>
        {({ language }) =>
          params.isSubmitted ? (
            <EABButton
              isDisable={!params.canUpload}
              containerStyle={styles.uploadButton}
              onPress={() => {
                if (!isNetworkConnected) {
                  Alert.alert(
                    EABi18n({
                      language,
                      textStore,
                      path:
                        "application.form.supportingDocument.networkDisconnected"
                    })
                  );
                } else {
                  setApiResponsePage({ apiPage: "uploadButton" });
                  this.checkTokenBeforeUpload(language);
                }
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalEmphasizedAccent}
                path="button.upload"
              />
            </EABButton>
          ) : null
        }
      </ContextConsumer>
    );
  }
}

UploadButtonAfterSubmitted.propTypes = {
  dataSync: PropTypes.func.isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  setApiResponsePage: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  apiPage: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  environment: PropTypes.string.isRequired
};
