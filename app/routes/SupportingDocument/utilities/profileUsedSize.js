/**
 * @name clientInfoObject
 * @description return a object of client information
 * @requires languageTypeConvert - use to options map get text
 * @requires EABi18n - get text
 * @param {object} application - object key
 * @param {*} photo - client photo
 * @return {number} the max upload file size
 * */
export default ({ supportingDocument }) => {
  let sum = 0;

  const transferSizeToInteger = fileSize => {
    const unit = fileSize.slice(-2);
    if (parseFloat(unit, 10)) {
      return parseFloat(fileSize, 10);
    }
    if (unit === "KB") {
      return parseFloat(fileSize, 10) * 1000;
    }
    if (unit === "MB") {
      return parseFloat(fileSize, 10) * 1000 * 1000;
    }
    return 0;
  };

  const profileUsedFileSize = profileID => {
    const { mandDocs, otherDoc } = supportingDocument.values[profileID];

    const keyArray = Object.keys(mandDocs);

    keyArray.forEach(key => {
      mandDocs[key].forEach(document => {
        sum += transferSizeToInteger(document.fileSize);
      });
    });

    if (otherDoc) {
      const otherDocKeyArray = (otherDoc.template || []).map(ele => ele.id);
      otherDocKeyArray.forEach(key => {
        if (otherDoc.values[key]) {
          otherDoc.values[key].forEach(file => {
            sum += transferSizeToInteger(file.fileSize);
          });
        }
      });
    }
  };

  Object.keys(supportingDocument.values).forEach(supportingDocumentKey => {
    if (supportingDocumentKey !== "proposer") {
      profileUsedFileSize(supportingDocumentKey);
    }
  });

  return sum;
};
