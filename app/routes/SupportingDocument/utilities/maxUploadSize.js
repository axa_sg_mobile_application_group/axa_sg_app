/**
 * @name clientInfoObject
 * @description return a object of client information
 * @requires languageTypeConvert - use to options map get text
 * @requires EABi18n - get text
 * @param {object} application - object key
 * @param {*} photo - client photo
 * @return {number} the max upload file size
 * */
export default ({ application }) => {
  if (application.iCidMapping) {
    if (Object.keys(application.iCidMapping).length > 0) {
      return Object.keys(application.iCidMapping).length * 10 + 10;
    }
    return 0;
  }
  return 20;
};
