import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import {
  TEXT_STORE,
  PAYMENT as APP_PAYMENT,
  LOGIN
} from "../../../constants/REDUCER_TYPES";
import { dataSync } from "../../../utilities/DataManager";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import UploadButtonAfterSubmitted from "../components/UploadButtonAfterSubmitted";
/**
 * UploadButtonAfterSubmitted
 * @description Application Summary page.
 * @requires UploadButtonAfterSubmitted - UploadButtonAfterSubmitted UI
 * */
const mapStateToProps = state => ({
  environment: state[LOGIN].environment,
  apiPage: state[REDUCER_TYPES.APPLICATION].component.apiPage,
  isOnlineAuthCompleted: state[APP_PAYMENT].isOnlineAuthCompleted,
  isHandlingListener: state[LOGIN].isHandlingListener,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  setApiResponsePage: ({ apiPage }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].UPDATE_API_PAGE,
      newApiPage: apiPage
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  dataSync: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC,
      api: dataSync
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadButtonAfterSubmitted);
