import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES, EAPP } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import SupportingDocumentDetails from "../components/SupportingDocumentDetails";
/**
 * SupportingDocumentDetails
 * @description Application Summary page.
 * @requires SupportingDocumentDetails - SupportingDocumentDetails UI
 * */
const mapStateToProps = state => ({
  agent: state[REDUCER_TYPES.AGENT],
  optionsMap: state[REDUCER_TYPES.OPTIONS_MAP],
  supportingDocument:
    state[REDUCER_TYPES.SUPPORTING_DOCUMENT].supportingDocument,
  textStore: state[TEXT_STORE],
  error: state[REDUCER_TYPES.SUPPORTING_DOCUMENT].error,
  application: state[REDUCER_TYPES.APPLICATION].application,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  pendingSubmitList: state[REDUCER_TYPES.SUPPORTING_DOCUMENT].pendingSubmitList
});

const mapDispatchToProps = dispatch => ({
  // TODO APPLYING
  getSupportingDocument: ({
    appId,
    appStatus = EAPP.appStatus.APPLYING,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_GET_SUPPORTING_DOCUMENT_DATA,
      appId,
      appStatus,
      callback
    });
  },
  clearPendingList: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST,
      newPendingSubmitList: []
    });
  },
  // TODO validate and show tick on the segmented control
  validateSupportingDocument: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_VALIDATE_SUPPORTING_DOCUMENT
    });
  },
  saveSubmitedDocument: ({ appId, isSupervisorChannel, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_SAVE_SUBMITTED_DOCUMENT,
      appId,
      isSupervisorChannel,
      callback
    });
  },
  addOtherDocument: ({ appId, docName, docNameOption, tabId }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_ADD_OTHER_SUPPORTING_DOCUMENT,
      appId,
      docName,
      docNameOption,
      tabId
    });
  },
  onFileUpload: ({
    attachment,
    applicationId,
    attachmentValues,
    valueLocation,
    tabId,
    isSupervisorChannel,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_ON_FILE_UPLOAD_SUPPORTING_DOCUMENT,
      attachment,
      applicationId,
      attachmentValues,
      valueLocation,
      tabId,
      // unknown
      isSupervisorChannel,
      callback
    });
  },
  saveSupportingDocument: ({ applicationId, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_SAVE_SUPPORTING_DOCUMENT_DATA,
      appId: applicationId,
      callback
    });
  },
  reRenderPage: ({ applicationId, currentStep }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_APPLICATION_RERENDER_PAGE,
      dispatch,
      currentStep,
      quotType: "",
      applicationId
    });
  },
  editOtherDocumentSection: ({ applicationId, documentId, tabId, docName }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_EDIT_OTHER_SUPPORTING_DOCUMENT,
      applicationId,
      docName,
      documentId,
      tabId
    });
  },
  deleteFile: ({ applicationId, attachmentId, valueLocation, tabId }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_DELETE_FILE_SUPPORTING_DOCUMENT,
      applicationId,
      attachmentId,
      valueLocation,
      tabId
    });
  },
  deleteOtherDocumentSection: ({ applicationId, documentId, tabId }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
          .SAGA_DELETE_OTHER_DOCUMENT_SECTION,
      applicationId,
      documentId,
      tabId
    });
  },
  getApplicationsList: () => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SupportingDocumentDetails);
