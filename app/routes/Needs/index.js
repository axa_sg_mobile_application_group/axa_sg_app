import React, { PureComponent } from "react";
import { Text } from "react-native";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import EABButton from "../../components/EABButton";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import LearnMore from "./containers/LearnMore";
import NeedsDetail from "./containers/NeedsDetail";
import NeedsDetailAppBarRight from "./containers/NeedsDetailAppBarRight";
import FNAReportAppBarRight from "./containers/FNAReportAppBarRight";
import FNAReport from "./containers/FNAReport";
import FNASummary from "./containers/FNASummary";

/**
 * <Needs />
 * @reducer navigation.needs
 * @requires NeedsDetail - Needs index page
 * @requires LearnMore - Learn more page
 * @requires FNAReport - FNAReport page
 * @requires FNASummary - FNASummary  page
 * */
class Needs extends PureComponent {
  render() {
    return (
      <NeedsNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Needs")
        }}
      />
    );
  }
}

export const NeedsNavigator = createStackNavigator(
  {
    NeedsDetail: {
      screen: NeedsDetail,
      navigationOptions: ({ navigation }) => ({
        title: "Needs",
        headerLeft: (
          <ContextConsumer>
            {({ hideClientNavigator }) => (
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                onPress={() => {
                  hideClientNavigator();
                  cleanClientData(navigation.dispatch);
                }}
              >
                <Text style={Theme.textButtonLabelNormalAccent}>Close</Text>
              </EABButton>
            )}
          </ContextConsumer>
        ),
        headerRight: <NeedsDetailAppBarRight navigation={navigation} />
      })
    },
    LearnMore: {
      screen: LearnMore,
      navigationOptions: {
        title: "Learn More"
      }
    },
    FNAReport: {
      screen: FNAReport,
      navigationOptions: {
        title: "FNA Report",
        headerRight: <FNAReportAppBarRight />
      }
    },
    FNASummary: {
      screen: FNASummary,
      navigationOptions: {
        title: "FNA Summary"
      }
    }
  },
  {
    initialRouteName: "NeedsDetail",
    navigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].needs
});

export default connect(mapStateToProps)(Needs);
