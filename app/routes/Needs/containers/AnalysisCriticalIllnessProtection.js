import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisCriticalIllnessProtection from "../components/AnalysisCriticalIllnessProtection";

const { FNA, CLIENT } = REDUCER_TYPES;
const { CIPROTECTION } = NEEDS;

/**
 * AnalysisCriticalIllnessProtection
 * @requires AnalysisCriticalIllnessProtection - AnalysisCriticalIllnessProtection UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  isCiProtectionError: state[FNA].isNaError.ciProtection,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: CIPROTECTION
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      feData,
      dependantProfilesData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisCriticalIllnessProtection);
