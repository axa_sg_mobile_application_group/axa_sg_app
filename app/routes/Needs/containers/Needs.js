import { connect } from "react-redux";
import { ACTION_TYPES, ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import Needs from "../components/Needs";

const { FNA, CLIENT } = REDUCER_TYPES;
const { updateNAValue } = ACTION_LIST[FNA];

/**
 * Needs
 * @requires Needs - Needs UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  updateNAValue({ target, value }) {
    updateNAValue({
      dispatch,
      target,
      value
    });
  },
  initNAAspects: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ASPECTS,
      value
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    dependantProfilesData,
    feData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData,
      pdaData,
      profileData,
      dependantProfilesData,
      feData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Needs);
