import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import CustomerKnowledgeAssessment from "../components/CustomerKnowledgeAssessment";

const { FNA, FNA_CKA, OPTIONS_MAP, CLIENT } = REDUCER_TYPES;

/**
 * CustomerKnowledgeAssessment
 * @requires CustomerKnowledgeAssessment - CustomerKnowledgeAssessment UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE],
  productType: state[FNA].na.productType,
  profile: state[CLIENT].profile,
  course: state[FNA].na.ckaSection.owner.course,
  isCourseError: state[FNA].na.ckaSection.owner.isCourseError,
  institution: state[FNA].na.ckaSection.owner.institution,
  isInstitutionError: state[FNA].na.ckaSection.owner.isInstitutionError,
  studyPeriodEndYear: state[FNA].na.ckaSection.owner.studyPeriodEndYear,
  isStudyPeriodEndYearError:
    state[FNA].na.ckaSection.owner.isStudyPeriodEndYearError,
  collectiveInvestment: state[FNA].na.ckaSection.owner.collectiveInvestment,
  isCollectiveInvestmentError:
    state[FNA].na.ckaSection.owner.isCollectiveInvestmentError,
  transactionType: state[FNA].na.ckaSection.owner.transactionType,
  isTransactionTypeError: state[FNA].na.ckaSection.owner.isTransactionTypeError,
  insuranceInvestment: state[FNA].na.ckaSection.owner.insuranceInvestment,
  isInsuranceInvestmentError:
    state[FNA].na.ckaSection.owner.isInsuranceInvestmentError,
  insuranceType: state[FNA].na.ckaSection.owner.insuranceType,
  isInsuranceTypeError: state[FNA].na.ckaSection.owner.isInsuranceTypeError,
  profession: state[FNA].na.ckaSection.owner.profession,
  isProfessionError: state[FNA].na.ckaSection.owner.isProfessionError,
  years: state[FNA].na.ckaSection.owner.years,
  isYearsError: state[FNA].na.ckaSection.owner.isYearsError,
  passCka: state[FNA].na.ckaSection.owner.passCka,
  isChanged: state[FNA].naIsChanged,
  isValid: state[FNA].na.ckaSection.isValid
});

const mapDispatchToProps = dispatch => ({
  initCKA: callback => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].INIT_CKA,
      callback
    });
  },
  saveNA: ({ confirm, callback }) => {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_NA,
      confirm,
      callback
    });
  },
  updateCKAinit: callback => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].UPDATE_CKA_INIT,
      callback
    });
  },
  courseOnChange: newCourse => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].COURSE_ON_CHANGE,
      newCourse
    });
  },
  institutionOnChange: newInstitution => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].INSTITUTION_ON_CHANGE,
      newInstitution
    });
  },
  studyPeriodEndYearOnChange: newStudyPeriodEndYear => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].STUDYPERIODENDYEAR_ON_CHANGE,
      newStudyPeriodEndYear
    });
  },
  collectiveInvestmentOnChange: newCollectiveInvestment => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].COLLECTIVEINVESTMENT_ON_CHANGE,
      newCollectiveInvestment: newCollectiveInvestment.key
    });
  },
  transactionTypeOnChange: newTransactionType => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].TRANSACTIONTYPE_ON_CHANGE,
      newTransactionType
    });
  },
  insuranceInvestmentOnChange: newInsuranceInvestment => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].INSURANCEINVESTMENT_ON_CHANGE,
      newInsuranceInvestment: newInsuranceInvestment.key
    });
  },
  insuranceTypeOnChange: newInsuranceType => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].INSURANCETYPE_ON_CHANGE,
      newInsuranceType
    });
  },
  professionOnChange: newProfession => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].PROFESSION_ON_CHANGE,
      newProfession
    });
  },
  yearsOnChange: newYears => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].YEARS_ON_CHANGE,
      newYears
    });
  },
  yearsOnBlur: newYears => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].YEARS_ON_BLUR,
      newYears
    });
  },
  updateCKAisValid: isValid => {
    dispatch({
      type: ACTION_TYPES[FNA_CKA].UPDATE_CKA_IS_VALID,
      isValid
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerKnowledgeAssessment);
