import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import NeedsAnalysis from "../components/NeedsAnalysis";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * NeedsAnalysis
 * @requires NeedsAnalysis - NeedsAnalysis UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[REDUCER_TYPES.CLIENT].profile,
  pda: state[FNA].pda,
  lastStepIndex: state[FNA].na.lastStepIndex,
  completedStep: state[FNA].na.completedStep,
  fe: state[FNA].fe,
  na: state[FNA].na,
  needs: state[FNA].na.needs,
  analysis: state[FNA].na.analysis,
  isChanged: state[FNA].naIsChanged,
  isGloballyChanged: state[FNA].naIsGloballyChanged,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  saveNA: ({ confirm, callback }) => {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_NA,
      confirm,
      callback
    });
  },
  initialValidateNA({ naData, dependantProfilesData, profileData, feData }) {
    dispatch({
      type: ACTION_TYPES[FNA].VALIDATE_NA,
      naData,
      dependantProfilesData,
      profileData,
      feData
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData,
    resetProdType
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData,
      pdaData,
      profileData,
      feData,
      dependantProfilesData,
      resetProdType
    });
  },
  updateNaIsGloballyChanged: newNaIsGloballyChanged => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
      newNaIsGloballyChanged
    });
  },
  updateIsCompleted: isCompleted => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_IS_COMPLETED,
      isCompleted
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NeedsAnalysis);
