import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisEducationPlanning from "../components/AnalysisEducationPlanning";

const { FNA, CLIENT } = REDUCER_TYPES;
const { EPLANNING } = NEEDS;

/**
 * AnalysisEducationPlanning
 * @requires AnalysisEducationPlanning - AnalysisEducationPlanning UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  isEPlanningError: state[FNA].isNaError.ePlanning
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: EPLANNING
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      feData,
      dependantProfilesData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisEducationPlanning);
