import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisPersonalAccidentProtection from "../components/AnalysisPersonalAccidentProtection";

const { FNA, CLIENT } = REDUCER_TYPES;
const { PAPROTECTION } = NEEDS;

/**
 * AnalysisPersonalAccidentProtection
 * @requires AnalysisPersonalAccidentProtection - AnalysisPersonalAccidentProtection UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  isPaProtectionError: state[FNA].isNaError.paProtection
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: PAPROTECTION
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    dependantProfilesData,
    feData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      dependantProfilesData,
      feData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisPersonalAccidentProtection);
