import { connect } from "react-redux";
import {
  ACTION_LIST,
  REDUCER_TYPES,
  ACTION_TYPES,
  utilities
} from "eab-web-api";
import { logout } from "../../../actions/login";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicantsAndDependents from "../components/ApplicantsAndDependents";

const { FNA, CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;
const { updatePDA, updatePDAValue } = ACTION_LIST[FNA];

const { dataMapping } = utilities;
const { profileMapForm } = dataMapping;

/**
 * ApplicantsAndDependents
 * @requires ApplicantsAndDependents - ApplicantsAndDependents UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[REDUCER_TYPES.CLIENT].profile,
  dependantProfiles: state[REDUCER_TYPES.CLIENT].dependantProfiles,
  pda: state[REDUCER_TYPES.FNA].pda,
  fna: state[REDUCER_TYPES.FNA],
  optionsMap: state[OPTIONS_MAP],
  clientForm: state[CLIENT_FORM]
});

const mapDispatchToProps = dispatch => ({
  initPDA(callback) {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_PDA,
      callback
    });
  },
  updatePDA(value) {
    updatePDA({
      dispatch,
      value
    });
  },
  updatePDAValue({ target, value }) {
    updatePDAValue({
      dispatch,
      target,
      value
    });
  },
  savePDA({ confirm, callback }) {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_PDA,
      confirm,
      callback
    });
  },
  logout() {
    logout({ dispatch });
  },
  readyToAddFamilyMember(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isPDA: false,
        isTrustedIndividual: false
      },
      callback
    });
  },
  readyToEditSpouse(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isPDA: true,
        isFNA: true,
        isTrustedIndividual: false
      },
      callback
    });
  },
  readyToAddSpouse(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isPDA: true,
        isFNA: true,
        isTrustedIndividual: false
      },
      callback
    });
  },
  updateCurrentPage(currentPage) {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_CURRENT_PAGE,
      newPage: currentPage
    });
  },
  readyToAddTrustedIndividual(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isFamilyMember: false,
        isApplication: false,
        isProposerMissing: false,
        isPDA: false,
        isFNA: false,
        isTrustedIndividual: true
      },
      callback
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  saveTrustedIndividual({ callback, alert = () => {}, confirm }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_TRUSTED_INDIVIDUAL,
      confirm,
      callback,
      alert
    });
  },
  cancelCreateClient() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  loadClient({ profile, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      ...profileMapForm(profile)
    });
    callback();
  },
  cleanClientForm() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  getFNA: ({ callback }) => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicantsAndDependents);
