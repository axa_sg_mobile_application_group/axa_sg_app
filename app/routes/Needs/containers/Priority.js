import { connect } from "react-redux";
import { ACTION_TYPES, ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import Priority from "../components/Priority";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { FNA, CLIENT } = REDUCER_TYPES;
const { updateNAValue } = ACTION_LIST[FNA];

/**
 * Priority
 * @requires Priority - Priority UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  pda: state[FNA].pda,
  na: state[FNA].na
});

const mapDispatchToProps = dispatch => ({
  updateNAValue({ target, value }) {
    updateNAValue({
      dispatch,
      target,
      value
    });
  },
  initCompletedStep: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_COMPLETED_STEP_SAGA,
      value
    });
  },
  initPriorityNAValue: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_PRIORITY,
      value
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Priority);
