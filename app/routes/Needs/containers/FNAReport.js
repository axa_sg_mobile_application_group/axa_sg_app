import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import FNAReport from "../components/FNAReport";

const { FNA } = REDUCER_TYPES;

/**
 * FNAReport
 * @requires FNAReport - FNAReport UI
 * */
const mapStateToProps = state => ({
  FNAReport: state[FNA].FNAReport
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FNAReport);
