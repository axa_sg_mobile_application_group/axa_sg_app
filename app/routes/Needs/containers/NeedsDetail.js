import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import NeedsDetail from "../components/NeedsDetail";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { FNA, CLIENT_FORM, CLIENT, PRODUCTS } = REDUCER_TYPES;
/**
 * NeedsDetail
 * @description NeedsDetail page.
 * @requires NeedsDetail - NeedsDetail UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  profile: state[CLIENT].profile,
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  pda: state[FNA].pda,
  fe: state[FNA].fe,
  dependantProfiles: state[CLIENT].dependantProfiles,
  fnaPage: state[FNA].fnaPage
});
const mapDispatchToProps = dispatch => ({
  readyToEditProfile(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isApplication: false,
        isProposerMissingData: false,
        isFNA: true
      },
      callback
    });
  },
  getProfileData(cid, callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GET_PROFILE_DATA,
      cid,
      callback
    });
  },
  cancelCreateClient() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  getFNA: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA,
      callback
    });
  },
  initialValidateFE({ pdaData, feData, profileData, dependantProfilesData }) {
    dispatch({
      type: ACTION_TYPES[FNA].VALIDATE_FE,
      pdaData,
      feData,
      profileData,
      dependantProfilesData
    });
  },
  initialValidateNA({ naData, dependantProfilesData, profileData, feData }) {
    dispatch({
      type: ACTION_TYPES[FNA].VALIDATE_NA,
      naData,
      dependantProfilesData,
      profileData,
      feData
    });
  },
  toProductPage: callback => {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].PRODUCT_TAB_BAR_SAGA,
      callback
    });
  },
  clearSelectedPage: () => {
    dispatch({
      type: ACTION_TYPES[FNA].REDIRECT_TO,
      newPage: ""
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NeedsDetail);
