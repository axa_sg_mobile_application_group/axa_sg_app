import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const table = {
  flex: 0,
  backgroundColor: Theme.white,
  shadowColor: Theme.lightGrey,
  shadowOffset: {
    width: 0,
    height: 0
  },
  shadowRadius: 2,
  shadowOpacity: 1
};

const sectionWrapper = {
  alignItems: "flex-start",
  paddingVertical: Theme.alignmentXL,
  borderTopColor: Theme.lightGrey,
  borderTopWidth: 1,
  width: "100%"
};

const CFFormTable = {
  ...table,
  flex: 1,
  height: "100%"
};

/**
 * stylesheet
 * */
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  /**
   * error
   * */
  errorRow: {
    marginTop: Theme.alignmentXS
  },
  errorRowBottom: {
    marginBottom: Theme.alignmentXS
  },
  /**
   * header
   * */
  header: {
    ...Theme.dialogHeader,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-end",
    height: 112
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentL,
    alignItems: "center",
    width: "100%"
  },
  segmentScrollView: {
    minWidth: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  segmentWrapper: {
    flexGrow: 0,
    width: "100%"
  },
  /**
   * scroll view
   * */
  scrollView: {
    flex: 1,
    height: "100%",
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  /**
   * main
   * */
  mainWrapper: {
    marginHorizontal: Theme.alignmentXL,
    maxWidth: Theme.boxMaxWidth,
    width: "100%"
  },
  sectionWrapper,
  sectionWrapperFirst: {
    ...sectionWrapper,
    borderTopWidth: 0
  },
  titleRow: {
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginBottom: Theme.alignmentL
  },
  titleWrapper: {
    flexDirection: "row"
  },
  /**
   * table
   * */
  table,
  tableTitleWrapper: {
    flex: 8,
    alignItems: "flex-start",
    borderRightWidth: 1,
    borderRightColor: Theme.lightGrey
  },
  tableFieldWrapper: {
    flex: 4,
    alignItems: "flex-end"
  },
  tableFieldInputStyle: {
    ...Theme.subheadPrimary,
    textAlign: "right"
  },
  tableDisplayRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    height: "100%"
  },
  emptyWrapper: {
    marginTop: Theme.alignmentXL,
    maxWidth: Theme.textMaxWidth,
    width: "100%"
  },
  emptyTitleWrapper: {
    marginBottom: Theme.alignmentL
  },
  /**
   * net worth
   * */
  netWorthTotalWrapper: {
    marginBottom: Theme.alignmentXL
  },
  netWorthForm: {
    flexDirection: "row"
  },
  assetsWrapper: {
    flex: 1,
    marginRight: Theme.alignmentXXXL,
    height: "100%"
  },
  liabilitiesWrapper: {
    flex: 1,
    marginLeft: Theme.alignmentXXXL,
    height: "100%"
  },
  assetsTitle: {
    ...Theme.title,
    marginBottom: Theme.alignmentXL
  },
  liabilitiesTitle: {
    ...Theme.title,
    marginBottom: Theme.alignmentXL,
    textAlign: "right"
  },
  /**
   * existing insurance portfolio
   * */
  EIPWrapper: {
    flexDirection: "row"
  },
  sumAssuredWrapper: {
    flex: 1,
    marginRight: Theme.alignmentXL,
    alignItems: "flex-start"
  },
  EIPRightWrapper: {
    flex: 1,
    justifyContent: "flex-start"
  },
  annualPremiumsWrapper: {
    marginBottom: Theme.alignmentL,
    alignItems: "flex-start"
  },
  otherPremiums: {
    alignItems: "flex-start"
  },
  EIPSectionTitleWrapper: {
    marginBottom: Theme.alignmentL
  },
  /**
   * cash flow
   * */
  CFWrapper: {
    alignItems: "flex-start"
  },
  disposableWrapper: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: Theme.alignmentL
  },
  disposableTitleWrapper: {
    marginRight: Theme.alignmentXS
  },
  CFForm: {
    flexDirection: "row",
    width: "100%"
  },
  CFFormTable,
  CFFormTableFirst: {
    ...CFFormTable,
    marginRight: Theme.alignmentXL
  },
  emptyTableSection: {
    height: 143
  },
  annualIncome: {
    flex: 1
  },
  additionCommentWrapper: {
    marginTop: Theme.alignmentXL,
    maxWidth: Theme.textMaxWidth,
    width: "100%"
  },
  additionCommentTitle: {
    marginBottom: Theme.alignmentL
  },
  insurancePremiumsTitleWrapper: {
    flexDirection: "row",
    alignItems: "center"
  },
  /**
   * My Budget
   * */
  budgetField: {
    width: 320
  },
  sourceOfFunds: {
    marginTop: Theme.alignmentL,
    width: Theme.boxMaxWidth
  },
  budgetJustify: {
    marginTop: Theme.alignmentL
  },
  /**
   * footer
   * */
  footer: {
    flex: 0,
    flexBasis: "auto",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: Theme.alignmentXL,
    backgroundColor: "rgba(248,248,248,0.82)",
    height: 64,
    borderTopWidth: 1,
    borderTopColor: Theme.coolGrey
  }
});
