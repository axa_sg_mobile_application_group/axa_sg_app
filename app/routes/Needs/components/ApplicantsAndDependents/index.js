import {
  REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  DEPENDANT,
  utilities
} from "eab-web-api";
import PropTypes from "prop-types";
import React, { Component } from "react";
import * as _ from "lodash";
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import icNext from "../../../../assets/images/icNext.png";
import icPrevious from "../../../../assets/images/icPrevious.png";
import EASEProgressTabs from "../../../../components/EASEProgressTabs";
import EABButton from "../../../../components/EABButton";
import EABTextSelection from "../../../../components/EABTextSelection";
import { ContextConsumer } from "../../../../context";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import EABCard from "../../../../components/EABCard";
import EABCheckBox from "../../../../components/EABCheckBox";
import EABDialog from "../../../../components/EABDialog";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import {
  CLIENT_FORM,
  PDA_TRUSTED_INDIVIDUAL_FORM
} from "../../../../constants/FORM_TYPE";
import ClientFormContact from "../../../../containers/ClientFormContact";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import EABFormSectionHeader from "../../../../components/EABFormSectionHeader";
import EABRadioButton from "../../../../components/EABRadioButton";
import LinkClientDialog from "../../../LinkClientDialog";
import TranslatedText from "../../../../containers/TranslatedText";
import { languageTypeConvert } from "../../../../utilities/getEABTextByLangType";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import Theme from "../../../../theme";
import styles from "./styles";
import ClientFormTrustedIndividual from "../../../../containers/ClientFormTrustedIndividual";
import EABHUD from "../../../../containers/EABHUD";

const { isClientFormHasError } = utilities.validation;

/**
 * <ApplicantsAndDependants />
 * @param {function} closeDialog - close dialog function
 * @param {object} profile - profile data
 * @param {object} pda - application and dependant data
 * @param {object} textStore - a group of static text on ui
 * @param {object} dependantProfiles - dependant profiles data
 * @param {func} updatePDA - the function that will be fire by next button and done button in Applicants and Dependents pages
 * @param {func} updatePDAValue - the function that will be fire by buttons, radio buttons in Applicants and Dependents pages
 * @param {function} saveTrustedIndividual - save trusted individual data from
 * */

const { common } = utilities;
const { getOptionTitle } = common;
const { OPTIONS_MAP, CLIENT, FNA } = REDUCER_TYPES;

export default class ApplicantsAndDependants extends Component {
  constructor(props) {
    super(props);
    this.steps = [
      "ad.tabname.first.title",
      "ad.tabname.second.title",
      "ad.tabname.third.title"
    ];

    /**
     * @state {bool} isShowingCreateProfileDialog - an indicator on the create profile dialog
     * @state {bool} showProfileTab - an indicator for the Profile Tab
     * @state {number} currentStep - an indicator for showing the current step number
     * @state {number} noticesDescriptionHeight - height for the notice description
     * @state {bool} layoutFirstTime - an indicator for showing the first time
     */

    this.state = {
      showProfileTab: true,
      currentStep: props.pda.lastStepIndex === -1 ? 0 : props.pda.lastStepIndex,
      noticesDescriptionHeight: 9999,
      layoutFirstTime: true,
      initialFNA: {},
      initialTrustedIndividual: props.profile.trustedIndividuals || {},
      applicantHasChanged:
        props.pda.lastStepIndex === -1
          ? true
          : props.fna.pda.applicantHasChanged,
      consentNoticesHasChanged:
        props.pda.lastStepIndex === -1
          ? true
          : props.fna.pda.consentNoticesHasChanged,
      applicantBtnPressed: false,
      isSavingClient: false
    };
  }

  componentDidMount() {
    const callback = () => {
      const { profile, pda } = this.props;
      const firstRequirement = profile.age >= 62;
      const secondRequirement = profile.education === "below";
      const thirdRequirement = !(profile.language.indexOf("en") !== -1);

      this.needClientAccomplaniment =
        (firstRequirement && secondRequirement) ||
        (secondRequirement && thirdRequirement) ||
        (firstRequirement && thirdRequirement);

      if (this.needClientAccomplaniment && pda.trustedIndividual === "Y") {
        this.handleTrustedIndividualUpdate(pda.trustedIndividual);
      }

      if (!this.needClientAccomplaniment && !_.isEmpty(pda.trustedIndividual)) {
        this.handleTrustedIndividualUpdate(null);
      }

      this.setState({
        initialFNA: _.cloneDeep(this.props.fna)
      });

      this.props.loadClient({
        profile: profile.trustedIndividuals || {},
        callback: () => {}
      });

      if (
        profile.marital === "S" ||
        profile.marital === "D" ||
        profile.marital === "W" ||
        (!this.hasSpouse() && pda.applicant === "joint")
      ) {
        this.props.updatePDAValue({
          target: "applicant",
          value: true
        });
      }
    };

    this.props.initPDA(callback);
  }

  /**
   * onSelectApplicant - a function for the behavior of EAB Card when on press
   * */
  onSelectApplicant(newValue, showCreateProfileDialog) {
    if (newValue === false) {
      if (!this.hasSpouse()) {
        this.props.readyToAddSpouse(() => {
          showCreateProfileDialog(() => {
            this.setState({
              applicantHasChanged: true,
              consentNoticesHasChanged: false,
              applicantBtnPressed: true
            });
          });
        });
      } else {
        this.spouseProfileHasError(hasError => {
          if (hasError) {
            this.props.readyToEditSpouse(() => {
              showCreateProfileDialog(() => {
                this.setState({
                  applicantHasChanged: true,
                  consentNoticesHasChanged: false,
                  applicantBtnPressed: true
                });
              });
            });
          } else {
            this.props.updatePDAValue({
              target: "applicant",
              value: false
            });
            this.props.cleanClientForm();
            this.setState({
              applicantHasChanged: true,
              consentNoticesHasChanged: false,
              applicantBtnPressed: true
            });
          }
        });
      }
    } else {
      this.props.updatePDAValue({
        target: "applicant",
        value: true
      });
      this.setState({
        applicantHasChanged: true,
        consentNoticesHasChanged: false
      });
    }
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate can save client or not
   * @return {boolean} errorStatus
   * */
  get isDisableSaveClient() {
    const { clientForm } = this.props;
    const { hasError } = isClientFormHasError(clientForm);
    return hasError;
  }

  /**
   * @description indicate can press next button or not
   * @return {boolean} errorStatus
   * */
  get isDisableNextBtn() {
    const { pda } = this.props;
    let hasError = false;
    if (this.state.currentStep === 0) {
      hasError = _.isEmpty(pda.applicant);
      return hasError;
    }
    return hasError;
  }

  /**
   * @description indicate if the data is changed
   * @return {boolean} isChanged
   * */
  get isChanged() {
    const { initialFNA } = this.state;
    const { fna } = this.props;

    return (
      initialFNA.pda.applicant !== fna.pda.applicant ||
      initialFNA.pda.trustedIndividual !== fna.pda.trustedIndividual ||
      initialFNA.pda.dependants !== fna.pda.dependants ||
      initialFNA.pda.ownerConsentMethod !== fna.pda.ownerConsentMethod ||
      initialFNA.pda.spouseConsentMethod !== fna.pda.spouseConsentMethod
    );
  }

  /**
   * @description indicate if the trustedIndividual is changed
   * @return {boolean} isChanged
   * */
  get trustedIndividualIsChanged() {
    const { initialTrustedIndividual } = this.state;
    const { profile } = this.props;
    return !_.isEqual(initialTrustedIndividual, profile.trustedIndividuals);
  }

  get allStepsCompleted() {
    const { fna } = this.props;
    return (
      !_.isEmpty(fna.pda.applicant) &&
      !this.state.applicantHasChanged &&
      (!this.state.consentNoticesHasChanged ||
        !this.needClientAccomplaniment) &&
      (fna.pda.trustedIndividual === "N" ||
        (fna.pda.trustedIndividual === "Y" &&
          !this.trustedIndividualHasError()) ||
        !this.needClientAccomplaniment)
    );
  }

  /**
   * getWhichDependantOptions - get the list of dependant options
   * */
  getWhichDependantOptions(language) {
    const { pda, optionsMap } = this.props;
    let keyIndex = "A";
    const whichDependantOptions = [];
    const selectedDependants = pda.dependants ? pda.dependants.split(",") : "";
    Object.entries(this.props.dependantProfiles).forEach(([key, value]) => {
      if (
        this.props.pda.applicant === "joint" &&
        value.relationship === DEPENDANT.SPO
      ) {
        return;
      }

      let label = getOptionTitle({
        value: value.relationship,
        optionsMap: optionsMap.relationship,
        language: languageTypeConvert(language)
      });

      if (label === "Others") {
        label = value.relationshipOther;
      }

      const whichDependantOption = {
        key: keyIndex,
        memberId: key,
        text: value.fullName,
        label,
        isSelected: selectedDependants.includes(key)
      };
      keyIndex = String.fromCharCode(keyIndex.charCodeAt(0) + 1);
      whichDependantOptions.push(whichDependantOption);
    });
    return whichDependantOptions;
  }
  /**
   * getFamilyConsentOption - get the list of family consent options
   * */
  getFamilyConsentOption() {
    const { profile, pda } = this.props;
    const familyConsentOptions = [
      {
        key: "owner",
        name: profile.fullName,
        memberId: profile.cid,
        telephone:
          pda.ownerConsentMethod &&
          pda.ownerConsentMethod.indexOf("phone") !== -1,
        textMessage:
          pda.ownerConsentMethod &&
          pda.ownerConsentMethod.indexOf("text") !== -1,
        fax:
          pda.ownerConsentMethod && pda.ownerConsentMethod.indexOf("fax") !== -1
      }
    ];
    let familyConsentOption;
    Object.entries(this.props.dependantProfiles).forEach(([key, value]) => {
      if (value.relationship === "SPO") {
        let isPhone = false;
        let isText = false;
        let isFax = false;
        if (pda.spouseConsentMethod) {
          isPhone = pda.spouseConsentMethod.indexOf("phone") !== -1;
          isText = pda.spouseConsentMethod.indexOf("text") !== -1;
          isFax = pda.spouseConsentMethod.indexOf("fax") !== -1;
        }
        familyConsentOption = {
          key: "spouse",
          memberId: key,
          name: value.fullName,
          telephone: isPhone,
          textMessage: isText,
          fax: isFax
        };
      }
    });
    if (pda.applicant !== "single") {
      familyConsentOptions.push(familyConsentOption);
    }
    return familyConsentOptions;
  }

  /**
   * @description return spouse id
   * @return string id
   * */
  getSpouseId() {
    const dependants = Object.values(this.props.dependantProfiles);
    for (let i = 0; i < dependants.length; i += 1) {
      if (dependants[i].relationship === DEPENDANT.SPO) {
        return dependants[i]._id;
      }
    }
    return "";
  }

  trustedIndividualHasError() {
    const { clientForm } = this.props;
    let hasError = false;

    Object.keys(clientForm).forEach(key => {
      if (Object.prototype.hasOwnProperty.call(clientForm[key], "hasError")) {
        hasError = hasError || clientForm[key].hasError;
      }
    });

    return hasError;
  }

  /** TODO: REFACTOR
   * @description validate spouse profile
   * @return {boolean} errorStatus
   * */
  spouseProfileHasError(callback) {
    this.props.loadClient({
      profile: this.props.dependantProfiles[this.getSpouseId()],
      callback: () => {
        this.props.readyToEditSpouse(() => {
          const checkList = [
            "idDocType",
            "idCardNo",
            "title",
            "dob",
            "marital",
            "mobileNo",
            "mobileCountryCode",
            "language",
            "education",
            "employStatus",
            "allowance"
          ];
          let hasError = false;

          Object.keys(this.props.dependantProfiles[this.getSpouseId()]).forEach(
            key => {
              const field = checkList.find(list => list === key);

              if (checkList.indexOf(field) > -1) {
                if (
                  _.isEmpty(
                    this.props.dependantProfiles[this.getSpouseId()][
                      field
                    ].toString()
                  )
                ) {
                  hasError = true;
                }
              }
            }
          );
          callback(hasError);
        });
      }
    });
  }

  hasSpouse() {
    const { dependantProfiles } = this.props;
    const dependant = Object.values(dependantProfiles);
    for (let i = 0; i < dependant.length; i += 1) {
      if (dependant[i].relationship === DEPENDANT.SPO) {
        return true;
      }
    }
    return false;
  }

  /**
   * hvNextStep - shows the possible of next step based on current step number
   * */
  hvNextStep() {
    if (this.state.currentStep === 2) return false;
    if (this.state.currentStep === 1 && !this.needClientAccomplaniment)
      return false;
    return true;
  }
  /**
   * handleUpdatefamilyMemberForm - function that handle the update on family member form
   * */
  handleUpdatefamilyMemberForm(selectedDependants) {
    this.props.updatePDAValue({
      target: "dependants",
      value: selectedDependants
    });

    this.setState({
      applicantHasChanged: false,
      consentNoticesHasChanged: false
    });
  }
  /**
   * handleUpdateConsentNotice - function that handle the update on owner and spose consent method
   * */
  handleUpdateConsentNotice(key, selectedOptions) {
    if (key === "owner") {
      this.props.updatePDAValue({
        target: "ownerConsentMethod",
        value: selectedOptions
      });
    } else {
      this.props.updatePDAValue({
        target: "spouseConsentMethod",
        value: selectedOptions
      });
    }
    this.setState({ consentNoticesHasChanged: true });
  }
  /**
   * handleTrustedIndividualUpdate - function that handle the trusted individual
   * */
  handleTrustedIndividualUpdate(selectedOption) {
    this.props.updatePDAValue({
      target: "trustedIndividual",
      value: selectedOption
    });

    this.props.loadClient({
      profile: this.props.profile.trustedIndividuals || {},
      callback: () => {}
    });

    this.props.readyToAddTrustedIndividual(() => {});
  }

  /**
   * @description save PDA after click yes in confirmation alert
   * */
  savePdaWithConfirm(callback) {
    this.props.savePDA({
      confirm: true,
      callback
    });
  }

  /**
   * @description save PDA after clicking Save / Done button
   * */
  savePdaWithoutConfirm({ language, callback }) {
    const { textStore, savePDA } = this.props;
    if (
      this.isChanged ||
      (this.allStepsCompleted && !this.props.fna.fe.isCompleted) ||
      (this.props.fna.pda.trustedIndividual === "Y" &&
        this.state.currentStep > 0)
    ) {
      savePDA({
        confirm: false,
        callback: code => {
          if (code) {
            setTimeout(() => {
              confirmationAlert({
                messagePath: `error.${code}`,
                language,
                textStore,
                yesOnPress: () => {
                  this.savePdaWithConfirm(callback);
                }
              });
            }, 200);
          } else {
            callback();
          }
        }
      });
    } else {
      callback();
    }
  }

  render() {
    /**
     * render functions
     * */

    /**
     * @name renderSearchClientForm
     * @return {element} search client dialog component
     * */
    const renderSearchClientForm = () => (
      <ContextConsumer>
        {({ isShowingLinkClientDialog }) => (
          <EABDialog isOpen={isShowingLinkClientDialog}>
            <LinkClientDialog />
          </EABDialog>
        )}
      </ContextConsumer>
    );

    /**
     * renderClientForm - whole client form
     * */
    const renderClientForm = (
      language,
      showCreateProfileDialog,
      isShowingCreateProfileDialog,
      hideCreateProfileDialog
    ) => {
      const { textStore } = this.props;
      this.segmentedControlArray = [
        {
          key: "A",
          title: EABi18n({
            path: "ad.profile.tabTitle",
            language,
            textStore
          }),
          isShowingBadge: false,
          onPress: () => {
            this.setState({ showProfileTab: true });
          }
        },
        {
          key: "B",
          title: EABi18n({
            path: "ad.contact.tabTitle",
            language,
            textStore
          }),
          isShowingBadge: false,
          onPress: () => {
            this.setState({ showProfileTab: false });
          }
        }
      ];
      return (
        <EABDialog isOpen={isShowingCreateProfileDialog}>
          <View style={Theme.dialogHeader}>
            <EABButton
              onPress={() => {
                hideCreateProfileDialog();
                this.props.cancelCreateClient();
              }}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.cancel"
                })}
              </Text>
            </EABButton>
            <EABSegmentedControl
              testID="segmentedProfileCategory"
              segments={this.segmentedControlArray}
              activatedKey={this.state.showProfileTab ? "A" : "B"}
            />
            <EABButton
              onPress={() => {
                this.setState({ isSavingClient: true });
                this.props.saveClient(() => {
                  if (this.state.applicantBtnPressed) {
                    this.props.updatePDAValue({
                      target: "applicant",
                      value: false
                    });
                  }
                  this.setState({ isSavingClient: false });
                  hideCreateProfileDialog();
                });
              }}
              isDisable={this.isDisableSaveClient}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.done"
                })}
              </Text>
            </EABButton>
            <EABHUD isOpen={this.state.isSavingClient} />
          </View>
          {this.state.showProfileTab ? (
            <ClientFormProfile />
          ) : (
            <ClientFormContact />
          )}
        </EABDialog>
      );
    };

    const familyMemberForm = (language, textStore) =>
      this.getWhichDependantOptions(language).length > 0 ? (
        <EABCheckBox
          options={this.getWhichDependantOptions(language)}
          testID="PersonalDataAcknowledgement__APP"
          isVerticalStyle={false}
          onPress={options => {
            let selectedDependants = "";
            this.getWhichDependantOptions(language).forEach(element => {
              if (element.key === options.key) {
                if (element.isSelected) {
                  element.isSelected = !element.isSelected;
                } else {
                  if (this.props.fna.pda.dependants.split(",").length >= 10) {
                    return;
                  }
                  element.isSelected = true;
                }
              }
              if (element.isSelected) {
                selectedDependants = _.isEmpty(selectedDependants)
                  ? element.memberId
                  : (selectedDependants = `${selectedDependants},${
                      element.memberId
                    }`);
              }
            });
            this.handleUpdatefamilyMemberForm(selectedDependants);
            this.setState({
              applicantHasChanged: true,
              consentNoticesHasChanged: true
            });
          }}
        />
      ) : (
        <Text>
          {EABi18n({
            path: "ad.first.question.dependant.noOption.title",
            language,
            textStore
          })}
        </Text>
      );

    const renderForm = (
      language,
      showCreateProfileDialog,
      showLinkClientDialog,
      setSearchClientDialogFormType
    ) => {
      const { textStore, pda } = this.props;

      this.useTrustedIndividualOptions = [
        {
          key: "Y",
          testID: "YesIAmAccompanied",
          title: EABi18n({
            path: "ad.third.question.yes",
            language,
            textStore
          })
        },
        {
          key: "N",
          testID: "NoIAmNotKeen",
          title: EABi18n({
            path: "ad.third.question.no",
            language,
            textStore
          })
        }
      ];
      switch (this.state.currentStep) {
        case 0:
          return [
            <View key="a" style={styles.questionWrapperFirst}>
              <Text style={styles.question}>
                {EABi18n({
                  path: "ad.first.question.title",
                  language,
                  textStore
                })}
                <Text style={Theme.fieldTextOrHelperTextNegative}> * </Text>
              </Text>

              {_.isEmpty(pda.applicant) &&
              !(
                this.props.profile.marital === "S" ||
                this.props.profile.marital === "D" ||
                this.props.profile.marital === "W"
              ) ? (
                <Text style={Theme.fieldTextOrHelperTextNegative}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "error.302"
                  })}
                </Text>
              ) : null}

              <View style={styles.fieldWrapperRow}>
                <EABTextSelection
                  testID="PersonalDataAcknowledgement__APP"
                  options={[
                    {
                      key: "ad.first.question.me.title",
                      testID: "Myself",
                      title: EABi18n({
                        path: "ad.first.question.me.title",
                        language,
                        textStore
                      }).toUpperCase(),
                      isSelected:
                        pda.applicant === "single" ||
                        this.props.profile.marital === "S" ||
                        this.props.profile.marital === "W" ||
                        this.props.profile.marital === "D"
                    },
                    {
                      key: "ad.first.question.our.title",
                      testID: "MyselfAndSpouse",
                      title: EABi18n({
                        path: "ad.first.question.our.title",
                        language,
                        textStore
                      }).toUpperCase(),
                      isSelected: pda.applicant === "joint" && this.hasSpouse()
                    }
                  ]}
                  editable={this.props.profile.marital === "M"}
                  onPress={option => {
                    const newValue =
                      option.key === "ad.first.question.me.title";
                    this.onSelectApplicant(newValue, showCreateProfileDialog);
                  }}
                />
              </View>
              <Text style={styles.infoText}>
                {EABi18n({
                  path: "ad.first.question.info",
                  language,
                  textStore
                })}
              </Text>
            </View>,
            <View key="b" style={styles.questionWrapper}>
              <Text style={styles.question}>
                {EABi18n({
                  path: "ad.first.question.dependant.title",
                  language,
                  textStore
                })}
              </Text>
              <View style={styles.fieldWrapper}>
                {familyMemberForm(language, textStore)}
              </View>
              <View style={styles.fieldWrapper}>
                <View style={styles.flexRow}>
                  <EABButton
                    style={styles.familyMemberBtn}
                    testID="PersonalDataAcknowledgement__btnCreateFamilyMember"
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={() => {
                      this.props.cleanClientForm();
                      this.props.readyToAddFamilyMember(() => {
                        showCreateProfileDialog(() => {
                          this.setState({
                            applicantHasChanged: true,
                            consentNoticesHasChanged: false,
                            applicantBtnPressed: false,
                            showProfileTab: true
                          });
                        });
                      });
                    }}
                  >
                    <Text style={Theme.rectangleButtonStyle1Label}>
                      {EABi18n({
                        path: "ad.first.question.dependant.add.title",
                        language,
                        textStore
                      })}
                    </Text>
                  </EABButton>
                  <EABButton
                    buttonType={RECTANGLE_BUTTON_1}
                    testID="PersonalDataAcknowledgement__btnLinkFamilyMember"
                    onPress={() => {
                      setSearchClientDialogFormType({
                        type: CLIENT_FORM,
                        callback: () => {
                          showLinkClientDialog();
                        }
                      });
                      this.setState({
                        applicantHasChanged: true,
                        consentNoticesHasChanged: false
                      });
                    }}
                  >
                    <Text style={Theme.rectangleButtonStyle1Label}>
                      {EABi18n({
                        path: "ad.first.question.dependant.add.link",
                        language,
                        textStore
                      })}
                    </Text>
                  </EABButton>
                </View>
              </View>
            </View>
          ];
        case 2:
          return [
            <View key="a" style={styles.questionWrapperFirst}>
              <Text style={styles.descriptionText}>
                {EABi18n({
                  path: "ad.third.descriptionText.title",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.descriptionText}>
                {EABi18n({
                  path: "ad.third.descriptionText.first",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.descriptionText}>
                {EABi18n({
                  path: "ad.third.descriptionText.second",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.descriptionText}>
                {EABi18n({
                  path: "ad.third.descriptionText.third",
                  language,
                  textStore
                })}
              </Text>
            </View>,
            <View key="b" style={styles.questionWrapper}>
              <Text style={styles.question}>
                {EABi18n({
                  path: "ad.third.question.title",
                  language,
                  textStore
                })}
              </Text>
              {_.isEmpty(pda.trustedIndividual) ? (
                <Text style={Theme.fieldTextOrHelperTextNegative}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "error.302"
                  })}
                </Text>
              ) : null}
              <EABRadioButton
                style={styles.fieldWrapper}
                testID="PersonalDataAcknowlegdement__CE"
                options={this.useTrustedIndividualOptions}
                selectedOptionKey={pda.trustedIndividual}
                onPress={options => {
                  this.useTrustedIndividualOptions.forEach(element => {
                    element.isSelected = element.key === options.key;
                  });
                  this.handleTrustedIndividualUpdate(options.key);
                }}
              />
            </View>,
            pda.trustedIndividual === "Y" ? (
              <View key="c" style={styles.questionWrapper}>
                <Text style={styles.descriptionText}>
                  {EABi18n({
                    path: "ad.trustedIndividual.notice",
                    language,
                    textStore
                  })}
                </Text>

                <EABFormSectionHeader
                  style={styles.trustedIndividualHeader}
                  title={EABi18n({
                    path: "ad.trustedIndividual.title",
                    language,
                    textStore
                  })}
                />
                <ClientFormTrustedIndividual />
                <EABButton
                  style={styles.pickFromContactsButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    this.props.updateCurrentPage("PDA");
                    this.props.readyToAddTrustedIndividual(() => {
                      setSearchClientDialogFormType({
                        type: PDA_TRUSTED_INDIVIDUAL_FORM,
                        callback: () => {
                          showLinkClientDialog();
                        }
                      });
                    });
                  }}
                >
                  <Text style={Theme.rectangleButtonStyle1Label}>
                    {EABi18n({
                      path: "ad.trustedIndividual.button.pick.title",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
              </View>
            ) : null
          ];
        case 1: {
          const height = this.state.noticesDescriptionHeight;

          /* give it one more line if odd */
          const cardHeight =
            height % 2 !== 0
              ? (height + Theme.subheadPrimary.lineHeight) / 2
              : height / 2;

          return (
            <View style={styles.questionWrapperFirst}>
              <EABCard
                style={{
                  width: "100%"
                }}
                contentStyle={[
                  styles.noticeCard,
                  {
                    height: cardHeight
                  }
                ]}
              >
                <View
                  style={styles.noticeColumnA}
                  onLayout={event => {
                    if (this.state.layoutFirstTime) {
                      this.setState({
                        noticesDescriptionHeight:
                          event.nativeEvent.layout.height,
                        layoutFirstTime: false
                      });
                    }
                  }}
                >
                  <Text style={styles.noticeParagraphFirst}>
                    {EABi18n({
                      path: "ad.first.noticeText.firstParagraph",
                      language,
                      textStore
                    })}
                  </Text>
                  <Text style={styles.noticeParagraphSecond}>
                    {EABi18n({
                      path: "ad.second.noticeText.secondParagraph",
                      language,
                      textStore
                    })}
                  </Text>
                  <View style={styles.termWrapperFirst}>
                    <Text style={styles.termNumber}>i.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleA",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                  <View style={styles.termWrapper}>
                    <Text style={styles.termNumber}>ii.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleB",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                  <View style={styles.termWrapper}>
                    <Text style={styles.termNumber}>iii.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleC",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                </View>
                <View style={styles.noticeColumnB}>
                  <Text style={styles.noticeParagraphFirst}>
                    {EABi18n({
                      path: "ad.first.noticeText.firstParagraph",
                      language,
                      textStore
                    })}
                  </Text>
                  <Text style={styles.noticeParagraphSecond}>
                    {EABi18n({
                      path: "ad.second.noticeText.secondParagraph",
                      language,
                      textStore
                    })}
                  </Text>
                  <View style={styles.termWrapperFirst}>
                    <Text style={styles.termNumber}>i.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleA",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                  <View style={styles.termWrapper}>
                    <Text style={styles.termNumber}>ii.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleB",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                  <View style={styles.termWrapper}>
                    <Text style={styles.termNumber}>iii.</Text>
                    <Text style={styles.termText}>
                      {EABi18n({
                        path: "ad.second.noticeText.ruleC",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                </View>
              </EABCard>
              <Text style={styles.termText}>
                {EABi18n({
                  path: "ad.second.noticeText.reminder",
                  language,
                  textStore
                })}
              </Text>
              {this.getFamilyConsentOption().map(
                member =>
                  member ? (
                    <View key={member.key} style={styles.familyChoicesWrapper}>
                      <Text style={styles.choicesName}>{member.name}</Text>

                      <EABCheckBox
                        testID="PersonalDataAcknowledgement__CA"
                        options={[
                          {
                            key: "a",
                            testID: "ByTel",
                            text: EABi18n({
                              path: "ad.second.options.telephone",
                              language,
                              textStore
                            }),
                            isSelected: member.telephone
                          },
                          {
                            key: "b",
                            testID: "ByMsg",
                            text: EABi18n({
                              path: "ad.second.options.textMessage",
                              language,
                              textStore
                            }),
                            isSelected: member.textMessage
                          }
                        ]}
                        isVerticalStyle={false}
                        onPress={options => {
                          let selectedOptions = "";
                          if (
                            options.text ===
                            EABi18n({
                              path: "ad.second.options.telephone",
                              language,
                              textStore
                            })
                          ) {
                            member.telephone = !member.telephone;
                          }
                          if (
                            options.text ===
                            EABi18n({
                              path: "ad.second.options.textMessage",
                              language,
                              textStore
                            })
                          ) {
                            member.textMessage = !member.textMessage;
                          }
                          if (member.telephone) {
                            selectedOptions = _.isEmpty(selectedOptions)
                              ? selectedOptions.concat("phone", "")
                              : selectedOptions.concat(",", "phone");
                          }
                          if (member.textMessage) {
                            selectedOptions = _.isEmpty(selectedOptions)
                              ? selectedOptions.concat("text", "")
                              : selectedOptions.concat(",", "text");
                          }
                          this.handleUpdateConsentNotice(
                            member.key,
                            selectedOptions
                          );
                        }}
                      />
                    </View>
                  ) : null
              )}
            </View>
          );
        }
        default:
          this.setState({ currentStep: 0 });
          return 0;
      }
    };

    const consentNoticesCanPress = () => {
      const { fna } = this.props;
      const validApplicants = !_.isEmpty(fna.pda.applicant);
      return validApplicants;
    };

    const consentNoticesIsCompleted = () => {
      const { fna } = this.props;
      const validApplicants = !_.isEmpty(fna.pda.applicant);
      const validConsentNotices = !_.isUndefined(fna.pda.ownerConsentMethod);
      return (
        validApplicants &&
        validConsentNotices &&
        !this.state.applicantHasChanged
      );
    };

    const clientsAccompanimentCanPress = () => {
      const validConsentNotices =
        consentNoticesCanPress() && consentNoticesIsCompleted();
      return this.needClientAccomplaniment && validConsentNotices;
    };

    const clientsAccompanimentIsCompleted = () => {
      const { fna } = this.props;
      const validTrustedIndividual =
        !_.isEmpty(fna.pda.trustedIndividual) &&
        this.needClientAccomplaniment &&
        (fna.pda.trustedIndividual === "N" ||
          (fna.pda.trustedIndividual === "Y" &&
            !this.trustedIndividualHasError()));
      return (
        validTrustedIndividual &&
        ((!this.state.applicantHasChanged &&
          !this.state.consentNoticesHasChanged) ||
          !this.needClientAccomplaniment)
      );
    };

    const handleDoneSaveButton = ({ language, switchToFE }) => {
      const callback = () => {
        if (switchToFE) {
          this.props.getFNA({ callback: switchToFE });
        } else {
          this.props.getFNA({ callback: this.props.closeDialog });
        }
        this.props.updateCurrentPage("");
        this.props.cleanClientForm();
      };

      this.props.updatePDAValue({
        target: "isCompleted",
        value: this.allStepsCompleted
      });

      this.props.updatePDAValue({
        target: "lastStepIndex",
        value: this.state.currentStep
      });

      this.props.updatePDAValue({
        target: "applicantHasChanged",
        value: this.state.applicantHasChanged
      });

      this.props.updatePDAValue({
        target: "consentNoticesHasChanged",
        value: this.state.consentNoticesHasChanged
      });

      if (
        this.isChanged ||
        (this.allStepsCompleted && !this.props.fna.fe.isCompleted)
      ) {
        if (
          this.props.fna.pda.trustedIndividual === "Y" &&
          this.state.currentStep > 0
        ) {
          this.props.saveTrustedIndividual({
            callback: () => this.savePdaWithoutConfirm({ language, callback }),
            alert: () => {},
            confirm: true
          });
        } else {
          this.savePdaWithoutConfirm({ language, callback });
        }
      } else if (
        this.props.fna.pda.trustedIndividual === "Y" &&
        this.state.currentStep > 0
      ) {
        this.props.saveTrustedIndividual({
          callback: () => {
            if (this.trustedIndividualIsChanged) {
              this.savePdaWithoutConfirm({ language, callback });
            }
            this.props.closeDialog();
          },
          alert: () => {},
          confirm: true
        });
      } else {
        this.props.cleanClientForm();
        this.props.closeDialog();
        if (switchToFE) {
          switchToFE();
        }
      }
    };

    const canGoFE = () => {
      const { fna, clientForm } = this.props;
      return (
        (this.state.currentStep === 1 && !this.needClientAccomplaniment) ||
        fna.pda.trustedIndividual === "N" ||
        (fna.pda.trustedIndividual === "Y" &&
          !isClientFormHasError(clientForm).hasError)
      );
    };

    const { textStore } = this.props;

    return (
      <ContextConsumer>
        {({
          language,
          showCreateProfileDialog,
          isShowingCreateProfileDialog,
          hideCreateProfileDialog,
          showLinkClientDialog,
          setSearchClientDialogFormType,
          isShowingLinkClientDialog
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>
              <View style={styles.titleBar}>
                <EABButton
                  containerStyle={[
                    Theme.headerLeft,
                    { left: Theme.alignmentXL }
                  ]}
                  testID="PersonalDataAcknowledgement__APPbtnSave"
                  onPress={() => {
                    handleDoneSaveButton({ language });
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                    {EABi18n({
                      path: "button.save",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
                <Text style={Theme.title}>
                  {EABi18n({
                    path: "ad.title",
                    language,
                    textStore
                  })}
                </Text>
                {((this.state.currentStep === 1 &&
                  !this.needClientAccomplaniment) ||
                  (this.state.currentStep === 2 && canGoFE())) && (
                  <EABButton
                    containerStyle={[
                      Theme.headerRight,
                      { right: Theme.alignmentXL }
                    ]}
                    onPress={() => {
                      handleDoneSaveButton({ language });
                    }}
                  >
                    <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                      {EABi18n({
                        path: "button.done",
                        language,
                        textStore
                      })}
                    </Text>
                  </EABButton>
                )}
              </View>
              <EASEProgressTabs
                testID="PersonalDataAcknowledgement"
                steps={[
                  {
                    key: "a",
                    testID: "Applicants",

                    title: EABi18n({
                      path: this.steps[0],
                      language,
                      textStore
                    }).toUpperCase(),
                    canPress: true,
                    isCompleted: !_.isEmpty(this.props.fna.pda.applicant),
                    onPress: () => {
                      this.savePdaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 0
                          });
                          this.props.updatePDAValue({
                            target: "lastStepIndex",
                            value: 0
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "b",
                    testID: "ConsentNotices",
                    title: EABi18n({
                      path: this.steps[1],
                      language,
                      textStore
                    }).toUpperCase(),
                    canPress: consentNoticesCanPress(),
                    isCompleted: consentNoticesIsCompleted(),
                    onPress: () => {
                      if (
                        _.isUndefined(this.props.fna.pda.ownerConsentMethod)
                      ) {
                        this.handleUpdateConsentNotice("owner", "");
                      }

                      this.savePdaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 1,
                            applicantHasChanged: false
                          });
                          this.props.updatePDAValue({
                            target: "lastStepIndex",
                            value: 1
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "c",
                    testID: "Client'sAccompaniment",
                    title: EABi18n({
                      path: this.steps[2],
                      language,
                      textStore
                    }).toUpperCase(),
                    canPress: clientsAccompanimentCanPress(),
                    isCompleted: clientsAccompanimentIsCompleted(),
                    onPress: () => {
                      this.savePdaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 2,
                            applicantHasChanged: false,
                            consentNoticesHasChanged: false
                          });
                          this.props.updatePDAValue({
                            target: "lastStepIndex",
                            value: 2
                          });
                          this.props.loadClient({
                            profile:
                              this.props.profile.trustedIndividuals || {},
                            callback: () => {}
                          });
                        }
                      });
                    }
                  }
                ]}
                currentStepIndex={this.state.currentStep}
                currentStepOnPress={() => {}}
              />
            </View>
            <View style={styles.KeyboardAvoidingViewWrapper}>
              <KeyboardAvoidingView
                contentContainerStyle={styles.KeyboardAvoidingView}
                behavior="position"
                enabled={
                  !isShowingLinkClientDialog && this.state.currentStep === 2
                }
                keyboardVerticalOffset={20}
              >
                <ScrollView
                  testID="PersonalDataAcknowledgement__scrollView"
                  style={styles.scrollView}
                  contentContainerStyle={styles.scrollViewContent}
                  showsVerticalScrollIndicator={false}
                >
                  {renderForm(
                    language,
                    showCreateProfileDialog,
                    showLinkClientDialog,
                    setSearchClientDialogFormType,
                    isShowingLinkClientDialog
                  )}
                  {renderSearchClientForm()}
                </ScrollView>
              </KeyboardAvoidingView>
            </View>
            <View style={styles.footer}>
              {this.state.currentStep === 0 ? (
                <View>{/* use for alignment */}</View>
              ) : (
                <EABButton
                  testID="PersonalDataAcknowledgement__APPbtnPrevious"
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.savePdaWithoutConfirm({
                      language,
                      callback: () => {
                        this.setState({
                          currentStep: this.state.currentStep - 1
                        });
                        this.props.cleanClientForm();
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <TranslatedText
                    style={styles.footerPreviousButtonText}
                    path="button.previous"
                  />
                </EABButton>
              )}
              {!this.hvNextStep() ? (
                canGoFE() && (
                  <EABButton
                    testID="PersonalDataAcknowledgement__APP__btnContinueToFE"
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={() => {
                      handleDoneSaveButton({
                        language,
                        switchToFE: this.props.switchToFE
                      });
                    }}
                  >
                    <TranslatedText path="ad.footer.continueToFE" />
                  </EABButton>
                )
              ) : (
                <EABButton
                  testID="PersonalDataAcknowledgement__APP__btnNext"
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    this.savePdaWithoutConfirm({
                      language,
                      callback: () => {
                        this.setState(
                          {
                            currentStep: this.state.currentStep + 1,
                            applicantHasChanged: false,
                            consentNoticesHasChanged:
                              this.state.currentStep !== 0
                                ? false
                                : this.state.consentNoticesHasChanged
                          },
                          () => {
                            if (
                              this.state.currentStep === 1 &&
                              _.isUndefined(
                                this.props.fna.pda.ownerConsentMethod
                              )
                            ) {
                              this.handleUpdateConsentNotice("owner", "");
                            }

                            if (this.state.currentStep === 2) {
                              this.props.loadClient({
                                profile:
                                  this.props.profile.trustedIndividuals || {},
                                callback: () => {}
                              });
                            }
                          }
                        );
                      }
                    });
                  }}
                  isDisable={this.isDisableNextBtn}
                >
                  <TranslatedText path="button.next" />
                  <Image style={Theme.iconStyle} source={icNext} />
                </EABButton>
              )}
            </View>
            {renderClientForm(
              language,
              showCreateProfileDialog,
              isShowingCreateProfileDialog,
              hideCreateProfileDialog
            )}
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

ApplicantsAndDependants.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  updatePDA: PropTypes.func.isRequired,
  updatePDAValue: PropTypes.func.isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  pda: REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  readyToAddFamilyMember: PropTypes.func.isRequired,
  cancelCreateClient: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired,
  initPDA: PropTypes.func.isRequired,
  readyToAddSpouse: PropTypes.func.isRequired,
  readyToAddTrustedIndividual: PropTypes.func.isRequired,
  savePDA: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  fna: REDUCER_TYPE_CHECK[FNA].isRequired,
  getFNA: PropTypes.func.isRequired,
  readyToEditSpouse: PropTypes.func.isRequired,
  loadClient: PropTypes.func.isRequired,
  updateCurrentPage: PropTypes.func.isRequired,
  cleanClientForm: PropTypes.func.isRequired,
  saveTrustedIndividual: PropTypes.func.isRequired,
  switchToFE: PropTypes.func.isRequired
};
