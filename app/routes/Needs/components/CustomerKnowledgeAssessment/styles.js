import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const footer = {
  flexBasis: "auto",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingHorizontal: Theme.alignmentXL,
  backgroundColor: Theme.white,
  height: 64,
  shadowColor: "rgba(0, 0, 0, 0.3)",
  shadowOffset: {
    width: 0,
    height: 0.5
  },
  shadowRadius: 0,
  shadowOpacity: 1
};
const questionWrapper = {
  paddingVertical: Theme.alignmentXL,
  alignItems: "flex-start",
  borderTopWidth: 1,
  borderColor: Theme.lightGrey,
  width: Theme.boxMaxWidth
};
const fieldWrapper = {
  marginTop: Theme.alignmentL,
  marginBottom: Theme.alignmentL,
  width: "100%"
};
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  /* header */
  header: {
    flexBasis: "auto",
    paddingTop: Theme.statusBarHeight + Theme.alignmentS,
    paddingBottom: 0,
    alignItems: "center",
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentXS,
    alignItems: "center",
    width: "100%"
  },
  /* form content */
  KeyboardAvoidingViewWrapper: {
    flex: 1,
    width: "100%",
    overflow: "hidden"
  },
  KeyboardAvoidingView: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  emptyBlock: {
    flex: 2
  },
  questionWrapper,
  questionWrapperFirst: {
    ...questionWrapper,
    paddingBottom: Theme.alignmentXXL,
    borderTopWidth: 0
  },
  question: {
    ...Theme.tagLine1Primary
  },
  questionDescription: {
    color: Theme.grey,
    fontSize: Theme.fontSizeXXM,
    paddingTop: 20,
    paddingBottom: 20
  },
  questionThreeDescription: {
    color: Theme.grey,
    fontSize: Theme.fontSizeXXM,
    paddingTop: 20
  },
  questionOneWrapper: {
    flexDirection: "column",
    marginTop: 20
  },
  questionOneSelectorField: {
    height: 60,
    width: 360,
    marginBottom: 20
  },
  questionOneTextBox: {
    width: 360
  },
  questionOneRightSelectorField: {
    flex: 1,
    height: 60
  },
  subTitle: {
    fontSize: Theme.fontSizeXXM,
    paddingTop: 5,
    paddingBottom: 5
  },
  questionOneExtraWrapper: { width: "100%" },
  EABTextBoxSectionWrapper: {
    width: "100%",
    marginBottom: 15
  },
  EABTextBox: {
    backgroundColor: Theme.white
  },
  questionThreeSelectorField: {
    marginRight: 15,
    marginBottom: 20,
    height: 60,
    width: 360
  },
  questionThreeTextField: {
    width: 360
  },
  fieldWrapper,
  fieldWrapperRow: {
    ...fieldWrapper,
    flexDirection: "column"
  },
  fieldWrapperRowNew: {
    ...fieldWrapper,
    marginBottom: 0,
    flexDirection: "column"
  },
  selectionErrorMsg: {
    color: Theme.red,
    marginTop: 10
  },
  textBoxErrorMsg: {
    color: Theme.red
  },
  note: {
    color: Theme.red,
    fontSize: Theme.fontSizeXXM,
    paddingTop: 20,
    paddingBottom: 20
  },
  bottomRow: {
    flexDirection: "row"
  },
  bottomTitleWrap: {
    flex: 5,
    flexDirection: "column"
  },
  bottomTitle: {
    fontSize: Theme.fontSizeXXM,
    fontWeight: "bold"
  },
  bottomEABButton: {
    flex: 1
  },
  bottomEABButtonImage: {
    marginRight: 10
  },
  bottomOutcomeTextWrap: {
    color: Theme.grey
  },
  bottomOutcomeText: {
    marginTop: 20,
    fontSize: Theme.fontSizeXXM
  },
  /* footer */
  footer,
  footerFirst: {
    ...footer,
    justifyContent: "flex-end"
  },
  footerLast: {
    ...footer,
    justifyContent: "flex-start"
  },
  footerPreviousButtonText: {
    ...Theme.rectangleButtonStyle1Label,
    marginRight: Theme.alignmentXXS
  },
  questionStar: {
    color: Theme.red
  },
  underline: {
    textDecorationLine: "underline"
  }
});
