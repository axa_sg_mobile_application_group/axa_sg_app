import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  container: {
    paddingVertical: Theme.alignmentXL,
    alignItems: "flex-start",
    width: Theme.boxMaxWidth
  },
  required: {
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  statement: { ...Theme.bodyPrimary, paddingBottom: 15 },
  warningStatement: {
    color: Theme.negative
  },
  titleContainer: {
    flexDirection: "row",
    marginLeft: 72
  },
  listContainer: {
    alignItems: "center",
    width: Theme.boxMaxWidth
  },
  footerContainer: {
    flex: 1,
    width: Theme.boxMaxWidth,
    alignItems: "center",
    justifyContent: "center"
  },
  headerContainer: {
    flex: 1,
    marginBottom: Theme.alignmentXL,
    width: Theme.boxMaxWidth,
    alignItems: "center",
    justifyContent: "center"
  },
  list: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  listContentContainer: {
    width: 832
  },
  title: {
    ...Theme.tagline1Primary
  },
  header: {
    ...Theme.tagline1Primary
  },
  row: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  rowColor: {
    backgroundColor: Theme.white
  },
  rowHeaderContainer: {
    flexDirection: "column",
    alignItems: "center"
  },
  rowHeaderTitle: {
    ...Theme.tagLine1Primary
  },
  rowIndex: {
    marginTop: Theme.alignmentL,
    marginRight: Theme.alignmentL
  },
  rowIndexTitle: {
    ...Theme.tagLine1Primary,
    width: 30
  },
  rowCard: {
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: Theme.white,
    width: 614,
    minHeight: 82
  },
  rowDataRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: Theme.alignmentXL
  },
  rowDataRowTitle: {
    ...Theme.tagLine1Primary,
    marginRight: Theme.alignmentXXS
  },
  rowDataTitle: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentXXS
  },
  rowDataRowSubTitle: {
    ...Theme.bodySecondary
  },
  rowData: {
    flexDirection: "row",
    alignItems: "center"
  },
  rowValueNegative: {
    ...Theme.bodyNegative
  },
  rowValuePositive: {
    ...Theme.bodyPositive
  },
  rowValueNormal: {
    ...Theme.bodyPrimary
  },
  priorityImage: {
    width: 100,
    height: 82
  }
});
