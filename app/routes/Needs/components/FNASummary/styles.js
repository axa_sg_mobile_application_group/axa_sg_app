import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  list: {
    flex: 1
  },
  listContentContainer: {
    width: 832
  },
  row: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  rowColor: {
    backgroundColor: Theme.white
  },
  rowHeaderContainer: {
    flexDirection: "column",
    alignItems: "center"
  },
  rowHeaderTitle: {
    ...Theme.tagLine1Primary,
    marginTop: Theme.alignmentXL
  },
  rowIndex: {
    marginTop: Theme.alignmentL,
    marginRight: Theme.alignmentL
  },
  rowIndexTitle: {
    ...Theme.tagLine1Primary,
    width: 30
  },
  rowCard: {
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: Theme.white,
    width: 714,
    height: 82
  },
  rowDataRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: Theme.alignmentXL
  },
  rowDataRowTitle: {
    ...Theme.tagLine1Primary,
    marginRight: Theme.alignmentXXS
  },
  rowDataTitle: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentXXS
  },
  rowDataRowSubTitle: {
    ...Theme.bodySecondary
  },
  rowData: {
    flexDirection: "row",
    alignItems: "center"
  },
  rowValueNegative: {
    ...Theme.bodyNegative
  },
  rowValuePositive: {
    ...Theme.bodyPositive
  },
  rowValueNormal: {
    ...Theme.bodyPrimary
  }
});
