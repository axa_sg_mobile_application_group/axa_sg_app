import React, { Component } from "react";
import { Image, View, Text, ScrollView, Slider } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LinearGradient from "react-native-linear-gradient";
import * as _ from "lodash";
import PropTypes from "prop-types";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  PRODUCT_TYPES
} from "eab-web-api";
import icPrevious from "../../../../assets/images/icPrevious.png";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import EABTextSelection from "../../../../components/EABTextSelection";
import SelectorField from "../../../../components/SelectorField";
import EABTextBox from "../../../../components/EABTextBox";
import styles from "./styles";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { RECTANGLE_BUTTON_2 } from "../../../../components/EABButton/constants";
import EABRadioButton from "../../../../components/EABRadioButton";
import EABTable from "../../../../components/EABTable";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableSection from "../../../../components/EABTableSection";
import { FLAT_LIST } from "../../../../components/Selector/constants";
import { getOptionList } from "../../../../utilities/getOptionList";

/**
 * <RiskAssessment />
 * @param {func} closeDialog - Close the current Dialog
 * @param {func} openCKADialog - Open Need Analysis Dialog
 * @param {object} cka - customer knowledge assessment data
 * @param {object} optionsMap - customer knowledge assessment data
 * */

const { OPTIONS_MAP } = REDUCER_TYPES;
const { INVESTLINKEDPLANS } = PRODUCT_TYPES;

export default class RiskAssessment extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.props.initRA();
  }

  get fieldHasError() {
    const {
      riskPotentialReturn,
      avgAGReturn,
      smDroped,
      alofLosses,
      expInvTime,
      invPref
    } = this.props;

    if (
      !riskPotentialReturn ||
      !avgAGReturn ||
      !smDroped ||
      !alofLosses ||
      !expInvTime ||
      !invPref
    ) {
      return true;
    }
    return false;
  }

  /**
   * @description indicate the output textBox has error or not
   * @return {boolean} errorStatus
   * */
  get outputHasError() {
    const { selfSelectedriskLevel, selfRLReasonRP, assessedRL } = this.props;

    if (
      selfSelectedriskLevel !== "" &&
      selfSelectedriskLevel !== assessedRL &&
      !selfRLReasonRP
    ) {
      return true;
    }
    return false;
  }

  /**
   * @description save NA after click yes in confirmation alert
   * */
  saveNaWithConfirm(callback) {
    this.props.saveNA({
      confirm: true,
      callback
    });
  }

  /**
   * @description save NA after clicking Done button
   * */
  saveNaWithoutConfirm({ language, callback }) {
    const { textStore, isChanged, saveNA, updateRAinit } = this.props;

    const onSave = () => {
      saveNA({
        confirm: false,
        callback: code => {
          if (code) {
            setTimeout(() => {
              confirmationAlert({
                messagePath: `error.${code}`,
                language,
                textStore,
                yesOnPress: () => {
                  this.saveNaWithConfirm(callback);
                }
              });
            }, 200);
          } else {
            callback();
          }
        }
      });
    };

    if (isChanged) {
      // if (isChanged || this.state.isValidChange) {
      updateRAinit(() => {
        onSave();
      });
    } else {
      callback();
    }
  }

  render() {
    const {
      textStore,
      productType,
      optionsMap,
      passCka,
      riskPotentialReturn,
      riskPotentialReturnOnChange,
      avgAGReturn,
      avgAGReturnOnChange,
      smDroped,
      smDropedOnChange,
      alofLosses,
      alofLossesOnChange,
      expInvTime,
      expInvTimeOnChange,
      invPref,
      invPrefOnChange,
      assessedRL,
      selfSelectedriskLevel,
      selfSelectedriskLevelOnChange,
      selfRLReasonRP,
      selfRLReasonRPOnChange
    } = this.props;

    const getRiskProfileList = language => {
      const riskProfileList = _.cloneDeep(optionsMap.riskRating);
      if (riskProfileList.options.findIndex(x => x.value === "") === -1) {
        riskProfileList.options.unshift({
          title: {
            en: EABi18n({
              path: "component.select",
              language,
              textStore
            })
          },
          value: ""
        });
      }
      return riskProfileList;
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>
              <View style={styles.titleBar}>
                <EABButton
                  testID="RiskProfileAssessment__btnSave"
                  containerStyle={[
                    Theme.headerLeft,
                    { left: Theme.alignmentXL }
                  ]}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.closeDialog();
                      }
                    });
                  }}
                >
                  <TranslatedText
                    style={Theme.textButtonLabelNormalEmphasizedAccent}
                    path="button.save"
                  />
                </EABButton>

                <Text style={styles.title}>
                  {EABi18n({
                    path: "ra.title",
                    language,
                    textStore
                  })}
                </Text>
                <EABButton
                  testID="RiskProfileAssessment__btnDone"
                  containerStyle={[
                    Theme.headerRight,
                    { right: Theme.alignmentXL }
                  ]}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.closeDialog();
                      }
                    });
                  }}
                  isDisable={
                    this.fieldHasError ||
                    (selfSelectedriskLevel !== "" &&
                      selfSelectedriskLevel !== assessedRL &&
                      !selfRLReasonRP)
                  }
                >
                  <Text style={styles.doneText}>
                    {EABi18n({
                      path: "button.done",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
              </View>
            </View>
            <View style={styles.KeyboardAvoidingViewWrapper}>
              <KeyboardAwareScrollView extraScrollHeight={40}>
                <ScrollView
                  testID="RiskAssessment__PageScroll"
                  style={styles.scrollView}
                  contentContainerStyle={styles.scrollViewContent}
                  ref={scrollView => {
                    this.scrollView = scrollView;
                  }}
                >
                  <View key="a" style={styles.questionWrapperFirst}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionOne.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    {!riskPotentialReturn && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}

                    <View style={styles.questionOneSliderGroupWrapper}>
                      <Text style={styles.questionOneSliderLeftText}>
                        {EABi18n({
                          path: "ra.questionOne.lowest",
                          language,
                          textStore
                        })}
                      </Text>

                      <View style={styles.questionOneSliderWrapper}>
                        <View style={styles.questionOneSliderIndexWrapper}>
                          <Text>1</Text>
                          <Text>2</Text>
                          <Text>3</Text>
                          <Text>4</Text>
                          <Text>5</Text>
                        </View>
                        <Slider
                          testID="RiskProfileAssessment__sldPreferredRisk"
                          minimumValue={1}
                          maximumValue={5}
                          step={1}
                          value={Number(riskPotentialReturn)}
                          style={styles.slider}
                          onValueChange={riskPotentialReturnOnChange}
                          minimumTrackTintColor={Theme.brand}
                        />
                      </View>

                      <Text style={styles.questionOneSliderRightText}>
                        {EABi18n({
                          path: "ra.questionOne.highest",
                          language,
                          textStore
                        })}
                      </Text>
                    </View>
                  </View>
                  <View key="b" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionTwo.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    {!avgAGReturn && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}
                    <View style={styles.fieldWrapperRow}>
                      <EABTextSelection
                        testID="RiskProfileAssessment"
                        style={styles.textSelection}
                        options={[
                          {
                            key: "1",
                            testID: "AverageOption1",
                            title: EABi18n({
                              path: "ra.questionTwo.optionOne",
                              language,
                              textStore
                            }),
                            isSelected: avgAGReturn === "1"
                          },
                          {
                            key: "2",
                            testID: "AverageOption2",
                            title: EABi18n({
                              path: "ra.questionTwo.optionTwo",
                              language,
                              textStore
                            }),
                            isSelected: avgAGReturn === "2"
                          },
                          {
                            key: "3",
                            testID: "AverageOption3",
                            title: EABi18n({
                              path: "ra.questionTwo.optionThree",
                              language,
                              textStore
                            }),
                            isSelected: avgAGReturn === "3"
                          },
                          {
                            key: "4",
                            testID: "AverageOption4",
                            title: EABi18n({
                              path: "ra.questionTwo.optionFour",
                              language,
                              textStore
                            }),
                            isSelected: avgAGReturn === "4"
                          },
                          {
                            key: "5",
                            testID: "AverageOption5",
                            title: EABi18n({
                              path: "ra.questionTwo.optionFive",
                              language,
                              textStore
                            }),
                            isSelected: avgAGReturn === "5"
                          }
                        ]}
                        editable
                        onPress={avgAGReturnOnChange}
                      />
                    </View>
                  </View>
                  <View key="c" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionThree.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    {!smDroped && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}
                    <EABRadioButton
                      testID="RiskProfileAssessment"
                      options={[
                        {
                          key: 1,
                          testID: "StockMarketsOption1",
                          title: EABi18n({
                            path: "ra.questionThree.optionOne",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 2,
                          testID: "StockMarketsOption2",
                          title: EABi18n({
                            path: "ra.questionThree.optionTwo",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 3,
                          testID: "StockMarketsOption3",
                          title: EABi18n({
                            path: "ra.questionThree.optionThree",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 4,
                          testID: "StockMarketsOption4",
                          title: EABi18n({
                            path: "ra.questionThree.optionFour",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 5,
                          testID: "StockMarketsOption5",
                          title: EABi18n({
                            path: "ra.questionThree.optionFive",
                            language,
                            textStore
                          })
                        }
                      ]}
                      selectedOptionKey={smDroped}
                      onPress={smDropedOnChange}
                    />
                  </View>
                  <View key="d" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionFour.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    {!alofLosses && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}
                    <View style={styles.fieldWrapperRow}>
                      <EABTextSelection
                        testID="RiskProfileAssessment"
                        style={styles.textSelection}
                        options={[
                          {
                            key: 1,
                            testID: "EstimatedLossesAcceptedOption1",
                            title: EABi18n({
                              path: "ra.questionFour.optionOne",
                              language,
                              textStore
                            }),
                            isSelected: alofLosses === 1
                          },
                          {
                            key: 2,
                            testID: "EstimatedLossesAcceptedOption2",
                            title: EABi18n({
                              path: "ra.questionFour.optionTwo",
                              language,
                              textStore
                            }),
                            isSelected: alofLosses === 2
                          },
                          {
                            key: 3,
                            testID: "EstimatedLossesAcceptedOption3",
                            title: EABi18n({
                              path: "ra.questionFour.optionThree",
                              language,
                              textStore
                            }),
                            isSelected: alofLosses === 3
                          },
                          {
                            key: 4,
                            testID: "EstimatedLossesAcceptedOption4",
                            title: EABi18n({
                              path: "ra.questionFour.optionFour",
                              language,
                              textStore
                            }),
                            isSelected: alofLosses === 4
                          },
                          {
                            key: 5,
                            testID: "EstimatedLossesAcceptedOption5",
                            title: EABi18n({
                              path: "ra.questionFour.optionFive",
                              language,
                              textStore
                            }),
                            isSelected: alofLosses === 5
                          }
                        ]}
                        editable
                        onPress={alofLossesOnChange}
                      />
                    </View>
                  </View>
                  <View key="e" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionFive.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                      <Text>#</Text>
                    </Text>
                    {!expInvTime && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}
                    <View style={styles.fieldWrapperRow}>
                      <EABTextSelection
                        testID="RiskProfileAssessment"
                        style={styles.textSelection}
                        options={[
                          {
                            key: 1,
                            testID: "ExpectedInvestmentTimeOption1",
                            title: EABi18n({
                              path: "ra.questionFive.optionOne",
                              language,
                              textStore
                            }),
                            isSelected: expInvTime === 1
                          },
                          {
                            key: 2,
                            testID: "ExpectedInvestmentTimeOption2",
                            title: EABi18n({
                              path: "ra.questionFive.optionTwo",
                              language,
                              textStore
                            }),
                            isSelected: expInvTime === 2
                          },
                          {
                            key: 3,
                            testID: "ExpectedInvestmentTimeOption3",
                            title: EABi18n({
                              path: "ra.questionFive.optionThree",
                              language,
                              textStore
                            }),
                            isSelected: expInvTime === 3
                          },
                          {
                            key: 4,
                            testID: "ExpectedInvestmentTimeOption4",
                            title: EABi18n({
                              path: "ra.questionFive.optionFour",
                              language,
                              textStore
                            }),
                            isSelected: expInvTime === 4
                          },
                          {
                            key: 5,
                            testID: "ExpectedInvestmentTimeOption5",
                            title: EABi18n({
                              path: "ra.questionFive.optionFive",
                              language,
                              textStore
                            }),
                            isSelected: expInvTime === 5
                          }
                        ]}
                        editable
                        onPress={expInvTimeOnChange}
                      />
                    </View>
                  </View>
                  <View key="f" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "ra.questionSix.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                      <Text>#</Text>
                    </Text>
                    {!invPref && (
                      <Text style={styles.errorMessage}>
                        {EABi18n({
                          path: "error.302",
                          language,
                          textStore
                        })}
                      </Text>
                    )}
                    <EABRadioButton
                      testID="RiskProfileAssessment"
                      options={[
                        {
                          key: 1,
                          testID: "InvestmentPreferenceOption1",
                          title: EABi18n({
                            path: "ra.questionSix.optionOne",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 2,
                          testID: "InvestmentPreferenceOption2",
                          title: EABi18n({
                            path: "ra.questionSix.optionTwo",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 3,
                          testID: "InvestmentPreferenceOption3",
                          title: EABi18n({
                            path: "ra.questionSix.optionThree",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 4,
                          testID: "InvestmentPreferenceOption4",
                          title: EABi18n({
                            path: "ra.questionSix.optionFour",
                            language,
                            textStore
                          })
                        },
                        {
                          key: 5,
                          testID: "InvestmentPreferenceOption5",
                          title: EABi18n({
                            path: "ra.questionSix.optionFive",
                            language,
                            textStore
                          })
                        }
                      ]}
                      selectedOptionKey={invPref}
                      onPress={invPrefOnChange}
                    />
                    <Text style={styles.bottomOutcomeText}>
                      {EABi18n({
                        path: "ra.end.textOne",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                  {!this.fieldHasError && (
                    <View style={styles.secondOutcomeBackground}>
                      <View style={styles.outcomeWrapper}>
                        <Text style={styles.bottomOutcomeText}>
                          {EABi18n({
                            path: "ra.end.textTwo",
                            language,
                            textStore
                          })}
                          <Text style={styles.highlightedText}>
                            {EABi18n({
                              path: `ra.end.riskProfile.${assessedRL}`,
                              language,
                              textStore
                            })}
                          </Text>
                        </Text>
                        {passCka === "Y" && (
                          <View style={styles.EABTextBoxSectionWrapper}>
                            <Text style={styles.bottomOutcomeText}>
                              {EABi18n({
                                path: "ra.end.textThree",
                                language,
                                textStore
                              })}
                            </Text>
                            <Text style={styles.bottomOutcomeText}>
                              {EABi18n({
                                path: "ra.end.textFour",
                                language,
                                textStore
                              })}
                            </Text>
                            <SelectorField
                              testID="RiskProfileAssessment__ddlPreferredRiskProfile"
                              style={styles.selectorField}
                              value={
                                selfSelectedriskLevel ||
                                EABi18n({
                                  path: "component.select",
                                  language,
                                  textStore
                                })
                              }
                              selectorType={FLAT_LIST}
                              flatListOptions={{
                                data: getOptionList({
                                  optionMap: getRiskProfileList(language),
                                  language
                                }),
                                keyExtractor: item => item.key,
                                selectedValue: null,
                                renderItemOnPress: (itemKey, item) => {
                                  if (item) {
                                    selfSelectedriskLevelOnChange(item.value);
                                  }
                                }
                              }}
                            />
                            {selfSelectedriskLevel !== "" &&
                              selfSelectedriskLevel !== assessedRL && (
                                <View style={styles.EABTextBoxSectionWrapper}>
                                  <EABTextBox
                                    testID="RiskProfileAssessment__txtPreferredRiskProfilePurpose"
                                    style={styles.EABTextBox}
                                    numberOfLines={5}
                                    value={selfRLReasonRP}
                                    onChange={selfRLReasonRPOnChange}
                                  />
                                  {!selfRLReasonRP && (
                                    <Text style={styles.errorMessage}>
                                      {EABi18n({
                                        path: "error.302",
                                        language,
                                        textStore
                                      })}
                                    </Text>
                                  )}
                                </View>
                              )}
                          </View>
                        )}
                        <TranslatedText
                          style={Theme.title}
                          path="ra.output.riskProfileDescriptions"
                        />
                        <EABTable
                          style={styles.table}
                          tableConfig={[
                            {
                              key: "A",
                              style: {
                                flex: 1,
                                alignItems: "flex-start"
                              }
                            },
                            {
                              key: "B",
                              style: {
                                flex: 6,
                                alignItems: "flex-start"
                              }
                            }
                          ]}
                        >
                          <EABTableHeader key="A" style={styles.eabTableHeader}>
                            <View style={styles.profileHeader}>
                              <TranslatedText
                                style={styles.tableHeaderText}
                                path="ra.output.profile"
                              />
                            </View>
                            <View style={styles.descriptionHeader}>
                              <TranslatedText
                                style={styles.tableHeaderText}
                                path="ra.output.description"
                              />
                            </View>
                          </EABTableHeader>
                          <EABTableSection key="B">
                            <View style={styles.profile}>
                              <TranslatedText
                                path="ra.end.riskProfile.1"
                                style={styles.profileText}
                              />
                            </View>
                            <View style={styles.description}>
                              <TranslatedText path="ra.end.riskProfile.1.description" />
                            </View>
                          </EABTableSection>
                          <EABTableSection key="C">
                            <View style={styles.profile}>
                              <TranslatedText
                                path="ra.end.riskProfile.2"
                                style={styles.profileText}
                              />
                            </View>
                            <View style={styles.description}>
                              <TranslatedText path="ra.end.riskProfile.2.description" />
                            </View>
                          </EABTableSection>
                          <EABTableSection key="D">
                            <View style={styles.profile}>
                              <TranslatedText
                                path="ra.end.riskProfile.3"
                                style={styles.profileText}
                              />
                            </View>
                            <View style={styles.description}>
                              <TranslatedText path="ra.end.riskProfile.3.description" />
                            </View>
                          </EABTableSection>
                          <EABTableSection key="E">
                            <View style={styles.profile}>
                              <TranslatedText
                                path="ra.end.riskProfile.4"
                                style={styles.profileText}
                              />
                            </View>
                            <View style={styles.description}>
                              <TranslatedText path="ra.end.riskProfile.4.description" />
                            </View>
                          </EABTableSection>
                          <EABTableSection key="F">
                            <View style={styles.lastProfile}>
                              <TranslatedText
                                path="ra.end.riskProfile.5"
                                style={styles.profileText}
                              />
                            </View>
                            <View style={styles.lastDescription}>
                              <TranslatedText path="ra.end.riskProfile.5.description" />
                            </View>
                          </EABTableSection>
                        </EABTable>
                      </View>
                    </View>
                  )}
                </ScrollView>
              </KeyboardAwareScrollView>
            </View>
            <View style={styles.footer}>
              {productType.prodType.split(",").indexOf(INVESTLINKEDPLANS) >
                -1 && (
                <EABButton
                  testID="RiskProfileAssessment__btnBackToCKA"
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.openCKADialog();
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <Text style={styles.footerPreviousButtonText}>
                    {EABi18n({
                      path: "ra.footer.backToCKA",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
              )}
              {productType.prodType.split(",").indexOf(INVESTLINKEDPLANS) ===
                -1 && (
                <EABButton
                  testID="RiskProfileAssessment__btnBackToNA"
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.openNADialog();
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <Text style={styles.footerPreviousButtonText}>
                    {EABi18n({
                      path: "ra.footer.backToNA",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
              )}
            </View>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

RiskAssessment.propTypes = {
  closeDialog: PropTypes.func.isRequired,
  openCKADialog: PropTypes.func.isRequired,
  openNADialog: PropTypes.func.isRequired,
  saveNA: PropTypes.func.isRequired,
  optionsMap: WEB_API_REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  riskPotentialReturn: PropTypes.number.isRequired,
  riskPotentialReturnOnChange: PropTypes.func.isRequired,
  avgAGReturn: PropTypes.string.isRequired,
  avgAGReturnOnChange: PropTypes.func.isRequired,
  smDroped: PropTypes.number.isRequired,
  smDropedOnChange: PropTypes.func.isRequired,
  alofLosses: PropTypes.number.isRequired,
  alofLossesOnChange: PropTypes.func.isRequired,
  expInvTime: PropTypes.number.isRequired,
  expInvTimeOnChange: PropTypes.func.isRequired,
  invPref: PropTypes.number.isRequired,
  invPrefOnChange: PropTypes.func.isRequired,
  assessedRL: PropTypes.number.isRequired,
  selfSelectedriskLevel: PropTypes.number.isRequired,
  selfSelectedriskLevelOnChange: PropTypes.func.isRequired,
  selfRLReasonRP: PropTypes.string.isRequired,
  selfRLReasonRPOnChange: PropTypes.func.isRequired,
  updateRAisValid: PropTypes.func.isRequired,
  updateRAinit: PropTypes.func.isRequired,
  passCka: PropTypes.string.isRequired,
  isChanged: PropTypes.bool.isRequired,
  initRA: PropTypes.func.isRequired,
  isValid: PropTypes.bool.isRequired,
  // TODO:
  productType: PropTypes.oneOfType([PropTypes.object]).isRequired
};
