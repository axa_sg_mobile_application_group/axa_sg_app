import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const footer = {
  flexBasis: "auto",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingHorizontal: Theme.alignmentXL,
  backgroundColor: Theme.white,
  height: 64,
  shadowColor: "rgba(0, 0, 0, 0.3)",
  shadowOffset: {
    width: 0,
    height: 0.5
  },
  shadowRadius: 0,
  shadowOpacity: 1
};
const questionWrapper = {
  paddingVertical: Theme.alignmentXL,
  alignItems: "flex-start",
  borderTopWidth: 1,
  borderColor: Theme.lightGrey,
  width: Theme.boxMaxWidth
};
const fieldWrapper = {
  marginTop: Theme.alignmentL,
  marginBottom: Theme.alignmentL,
  alignItems: "flex-start",
  width: "100%"
};
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  /* header */
  header: {
    flexBasis: "auto",
    paddingTop: Theme.statusBarHeight + Theme.alignmentS,
    paddingBottom: 0,
    alignItems: "center",
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentXS,
    alignItems: "center",
    width: "100%"
  },
  title: {
    ...Theme.title
  },
  doneText: {
    ...Theme.textButtonLabelNormalEmphasizedAccent
  },
  errorMessage: {
    color: Theme.red,
    marginTop: 10,
    marginBottom: 10
  },
  questionStar: {
    color: Theme.red
  },
  /* form content */
  KeyboardAvoidingViewWrapper: {
    flex: 1,
    width: "100%",
    overflow: "hidden"
  },
  KeyboardAvoidingView: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  questionWrapper,
  questionWrapperFirst: {
    ...questionWrapper,
    paddingBottom: Theme.alignmentXXL,
    borderTopWidth: 0
  },
  question: {
    ...Theme.tagLine1Primary,
    paddingBottom: Theme.alignmentXL,
    flexDirection: "row"
  },
  questionDescription: {
    color: Theme.grey,
    fontSize: 18,
    paddingTop: 20,
    paddingBottom: 20,
    width: 630
  },
  questionOneWrapper: {
    flexDirection: "row"
  },
  questionOneLeftSelectorField: {
    marginRight: 15,
    height: 60,
    flex: 1
  },
  questionOneTextField: {
    flex: 1,
    marginRight: 15,
    marginLeft: 15
  },
  questionOneRightSelectorField: {
    marginLeft: 30,
    height: 60,
    flex: 1
  },
  questionOneSliderGroupWrapper: {
    flexDirection: "row",
    alignItems: "center"
  },
  questionOneSliderLeftText: {
    width: 100,
    marginRight: 40
  },
  questionOneSliderRightText: {
    width: 100,
    marginLeft: 40
  },
  questionOneSliderWrapper: {
    alignItems: "center"
  },
  questionOneSliderIndexWrapper: {
    width: 490,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  slider: {
    width: 512
  },
  subTitle: {
    fontSize: 18,
    paddingTop: 5,
    paddingBottom: 5
  },
  EABTextBoxSectionWrapper: {
    width: "100%",
    marginBottom: 15
  },
  EABTextBox: {
    backgroundColor: Theme.white,
    width: "100%"
  },
  questionTwoSelectorField: {
    marginRight: 15,
    marginBottom: 20,
    height: 60,
    width: 360
  },
  questionThreeTextField: {
    width: 360
  },
  fieldWrapper,
  fieldWrapperRow: {
    ...fieldWrapper,
    flexDirection: "row",
    justifyContent: "flex-start",
    flexWrap: "wrap"
  },
  highlightedText: {
    color: Theme.green,
    fontSize: 24,
    fontWeight: "bold"
  },
  firstOutcomeBackground: {
    paddingTop: Theme.alignmentXL,
    width: "100%",
    backgroundColor: Theme.white,
    alignItems: "center"
  },
  secondOutcomeBackground: {
    paddingBottom: Theme.alignmentXL,
    width: "100%",
    backgroundColor: Theme.white,
    alignItems: "center"
  },
  outcomeWrapper: {
    width: Theme.boxMaxWidth,
    alignItems: "flex-start"
  },
  bottomOutcomeText: {
    marginTop: 20,
    fontSize: 18
  },
  textSelection: {
    flexWrap: "wrap"
  },
  selectorField: {
    height: 60,
    width: 360,
    marginBottom: 20
  },
  table: {
    width: "100%",
    paddingTop: 12
  },
  eabTableHeader: {
    backgroundColor: Theme.brand
  },
  tableHeaderText: {
    ...Theme.headlineWhite,
    fontSize: 18
  },
  profile: {
    backgroundColor: Theme.brandTransparentThree,
    borderColor: Theme.white,
    borderBottomWidth: 1,
    borderRightWidth: 1
  },
  profileHeader: {
    borderColor: Theme.white,
    borderBottomWidth: 1,
    borderRightWidth: 1
  },
  descriptionHeader: {
    borderColor: Theme.white,
    borderBottomWidth: 1
  },
  profileText: {
    color: Theme.white
  },
  description: {
    backgroundColor: Theme.brandTransparent,
    borderColor: Theme.white,
    borderBottomWidth: 1
  },
  lastProfile: {
    backgroundColor: Theme.brandTransparentThree,
    borderColor: Theme.white,
    borderRightWidth: 1
  },
  lastDescription: {
    backgroundColor: Theme.brandTransparent,
    borderColor: Theme.white
  },
  /* footer */
  footer: {
    ...footer,
    justifyContent: "flex-start"
  },
  footerPreviousButtonText: {
    ...Theme.rectangleButtonStyle1Label,
    marginRight: Theme.alignmentXXS
  }
});
