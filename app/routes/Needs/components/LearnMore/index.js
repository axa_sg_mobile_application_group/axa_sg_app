import React, { Component } from "react";
import { ScrollView, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import criticalIllnessImage from "../../../../assets/images/criticalIllnessImage.png";
import educationImage from "../../../../assets/images/educationImage.png";
import financialPlanningImage from "../../../../assets/images/financialPlanningImage.png";
import lifeProtectionImage from "../../../../assets/images/lifeProtectionImage.png";
import retirementImage from "../../../../assets/images/retirementImage.png";
import savingsImage from "../../../../assets/images/savingsImage.png";
import EABFloatingCard from "../../../../components/EABFloatingCard";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * LearnMore
 * @description LearnMore page.
 * */
export default class NeedsDetail extends Component {
  constructor() {
    super();

    this.state = {};
  }

  render() {
    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollViewContainerStyle}
        >
          <View style={styles.cardBox}>
            <EABFloatingCard
              isFullBoxImageBackground={false}
              style={styles.card}
              image={criticalIllnessImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Critical Illness</Text>
              <Text style={Theme.subheadSecondary}>
                Receive the best and latest medical treatment.
              </Text>
            </EABFloatingCard>
            <EABFloatingCard
              style={styles.card}
              image={educationImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Education</Text>
              <Text style={[Theme.subheadSecondary, { width: 110 }]}>
                An investment in knowledge pays the best interest.
              </Text>
            </EABFloatingCard>
            <EABFloatingCard
              isFullBoxImageBackground={false}
              style={styles.card}
              image={financialPlanningImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Financial Planning</Text>
              <Text style={Theme.subheadSecondary}>
                A good financial plan makes a big difference to your future.
              </Text>
            </EABFloatingCard>
            <EABFloatingCard
              style={styles.card}
              image={lifeProtectionImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Life Protection</Text>
              <Text style={Theme.subheadSecondary}>
                In every conceivable manner, the family is a link to our past
                and a bridge to our future.
              </Text>
            </EABFloatingCard>
            <EABFloatingCard
              isFullBoxImageBackground={false}
              style={styles.card}
              image={retirementImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Retirement</Text>
              <Text style={Theme.subheadSecondary}>
                Manage your money to meet your future retirement needs.
              </Text>
            </EABFloatingCard>
            <EABFloatingCard
              style={styles.card}
              image={savingsImage}
              onPress={() => {}}
            >
              <Text style={styles.cardTitle}>Savings</Text>
              <Text style={Theme.subheadSecondary}>
                {
                  "Don't save what is left after spending. Spend what is left after saving."
                }
              </Text>
            </EABFloatingCard>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}
