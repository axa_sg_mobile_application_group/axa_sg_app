import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;

/**
 * AnalysisDisabilityBenefit
 * @description Analysis - Disability Benefit - Template
 * */
export default class AnalysisDisabilityBenefit extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          diProtection: {
            ...na.diProtection,
            [selectedProfile]: {
              ...na.diProtection[selectedProfile],
              [key]: value,
              init: false
            }
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.diProtection.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.diProtection.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          diProtection: {
            ...na.diProtection,
            dependants
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  shortfallUI() {
    const data = this.checkDependant();
    const { totShortfall } = data;

    return {
      totShortfallPath:
        totShortfall >= 0
          ? "na.needs.common.surplus"
          : "na.needs.common.shortfall",
      totShortfallStyle:
        totShortfall >= 0 ? styles.surplusContent : styles.shortfallContent,
      totShortfallInfo:
        totShortfall >= 0
          ? "na.needs.common.surplusContent"
          : "na.needs.common.shortfallContent"
    };
  }

  render() {
    const { onChange } = this;
    const {
      textStore,
      selectedProfile,
      // TODO
      // assetsSelector,
      isDiProtectionError
    } = this.props;
    const data = this.checkDependant();
    const {
      totShortfallPath,
      totShortfallStyle,
      totShortfallInfo
    } = this.shortfallUI();
    const pmtCurrency = numberToCurrency({
      value: data.pmt || 0,
      decimal: 0
    });
    const othRegIncomeCurrency = numberToCurrency({
      value: data.othRegIncome || 0,
      decimal: 0
    });

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView
              extraScrollHeight={100}
              style={{ marginBottom: Theme.alignmentXXXXL }}
            >
              <View key={123} style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {`1. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection.pmt"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__DB__txtPMT"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={pmtCurrency}
                  isError={isDiProtectionError[selectedProfile].pmt.hasError}
                  hintText={EABi18n({
                    textStore,
                    language,
                    path: isDiProtectionError[selectedProfile].pmt.messagePath
                  })}
                  onChange={value => {
                    onChange({
                      value:
                        value !== "0" || value !== ""
                          ? currencyToNumber(value)
                          : "",
                      key: "pmt"
                    });
                  }}
                  maxLength={11 + pmtCurrency.replace(/[^,]/g, "").length}
                />

                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection.annualPmt"
                  })}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.annualPmt || 0,
                    decimal: 0,
                    maxValue: 99999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`2. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection.requireYrIncome"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                <EABStepper
                  testID="NeedsAnalysis__DB"
                  value={
                    data.requireYrIncome === ""
                      ? data.requireYrIncome
                      : Number(data.requireYrIncome)
                  }
                  step={1}
                  min={0}
                  max={99}
                  isError={
                    isDiProtectionError[selectedProfile].requireYrIncome
                      .hasError
                  }
                  hintText={EABi18n({
                    textStore,
                    language,
                    path:
                      isDiProtectionError[selectedProfile].requireYrIncome
                        .messagePath
                  })}
                  onChange={value => {
                    onChange({
                      value: value.toString(),
                      key: "requireYrIncome"
                    });
                  }}
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`3. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection.disabilityBenefit"
                  })} `}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.disabilityBenefit,
                    decimal: 0,
                    maxValue: 99999999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`4. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.diProtection.othRegIncome"
                  })}`}
                </Text>
                <EABTextField
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={othRegIncomeCurrency}
                  onChange={value => {
                    onChange({
                      value:
                        value !== "0" || value !== ""
                          ? currencyToNumber(value)
                          : "",
                      key: "othRegIncome"
                    });
                  }}
                  maxLength={
                    11 + othRegIncomeCurrency.replace(/[^,]/g, "").length
                  }
                />
              </View>
            </KeyboardAwareScrollView>
            <View style={styles.shortfallView}>
              <View style={styles.shortfallWrapper}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path: totShortfallInfo
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: totShortfallPath
                    })}: `}
                    <Text style={totShortfallStyle}>
                      {`$${numberToCurrency({
                        value: Math.abs(data.totShortfall || 0),
                        decimal: 2,
                        maxValue: 999999999999999
                      })}`}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisDisabilityBenefit.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  // TODO
  // assetsSelector: ViewPropTypes.style.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  isDiProtectionError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
