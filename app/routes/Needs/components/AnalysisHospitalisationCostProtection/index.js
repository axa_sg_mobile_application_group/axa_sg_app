import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import EABi18n from "../../../../utilities/EABi18n";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * AnalysisHospitalisationCostProtection
 * @description Analysis - Hospitalisation Cost Protection - Template
 * */
export default class AnalysisHospitalisationCostProtection extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          hcProtection: {
            ...na.hcProtection,
            [selectedProfile]: {
              ...na.hcProtection[selectedProfile],
              [key]: value,
              init: false
            }
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.hcProtection.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.hcProtection.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          hcProtection: {
            ...na.hcProtection,
            dependants
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  render() {
    const { textStore, isHcProtectionError, selectedProfile } = this.props;
    const data = this.checkDependant();
    const { onChange } = this;

    const { provideHeadstart, typeofWard } = data;
    return (
      <ContextConsumer>
        {({ language }) => (
          <KeyboardAwareScrollView extraScrollHeight={100}>
            <View key={123} style={styles.analysisProductContent}>
              <Text style={styles.title}>
                {EABi18n({
                  language,
                  textStore,
                  path: "na.needs.hcProtection"
                })}
              </Text>
              <Text style={styles.analysisProductPointTitle}>
                {`1. ${EABi18n({
                  language,
                  textStore,
                  path: "na.needs.hcProtection.typeOfHospital"
                })} `}
                <Text style={styles.requiredHint}>*</Text>
              </Text>
              {isHcProtectionError[selectedProfile].provideHeadstart
                .hasError && (
                <Text style={styles.errorMessage}>
                  {EABi18n({
                    textStore,
                    language,
                    path:
                      isHcProtectionError[selectedProfile].provideHeadstart
                        .messagePath
                  })}
                </Text>
              )}
              <View style={styles.analysisHospitalisationButtonContainer}>
                <EABButton
                  testID="NeedsAnalysis__HCP__btnPublic"
                  buttonType={
                    provideHeadstart ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.PUBLIC"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() => {
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.PUBLIC"
                      }),
                      key: "provideHeadstart"
                    });
                  }}
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.PUBLIC"
                    })}
                  </Text>
                </EABButton>

                <EABButton
                  testID="NeedsAnalysis__HCP__btnPrivate"
                  buttonType={
                    provideHeadstart ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.PRIVATE"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() =>
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.PRIVATE"
                      }),
                      key: "provideHeadstart"
                    })
                  }
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.PRIVATE"
                    })}
                  </Text>
                </EABButton>
              </View>

              <View style={styles.analysisSectionBorderLine} />

              <Text style={styles.analysisProductPointTitle}>
                {`2. ${EABi18n({
                  language,
                  textStore,
                  path: "na.needs.hcProtection.typeOfWard"
                })} `}
                <Text style={styles.requiredHint}>*</Text>
              </Text>
              {isHcProtectionError[selectedProfile].typeofWard.hasError && (
                <Text style={styles.errorMessage}>
                  {EABi18n({
                    textStore,
                    language,
                    path:
                      isHcProtectionError[selectedProfile].typeofWard
                        .messagePath
                  })}
                </Text>
              )}

              <View style={styles.analysisHospitalisationButtonContainer}>
                <EABButton
                  testID="NeedsAnalysis__HCP__btnA"
                  buttonType={
                    typeofWard ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.A"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() =>
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.A"
                      }),
                      key: "typeofWard"
                    })
                  }
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.A"
                    })}
                  </Text>
                </EABButton>

                <EABButton
                  testID="NeedsAnalysis__HCP__btnB1"
                  buttonType={
                    typeofWard ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.B1"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() =>
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.B1"
                      }),
                      key: "typeofWard"
                    })
                  }
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.B1"
                    })}
                  </Text>
                </EABButton>
              </View>
              <View style={styles.analysisHospitalisationButtonContainer}>
                <EABButton
                  testID="NeedsAnalysis__HCP__btnB2"
                  buttonType={
                    typeofWard ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.B2"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() =>
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.B2"
                      }),
                      key: "typeofWard"
                    })
                  }
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.B2"
                    })}
                  </Text>
                </EABButton>

                <EABButton
                  testID="NeedsAnalysis__HCP__btnC"
                  buttonType={
                    typeofWard ===
                    EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.C"
                    })
                      ? RECTANGLE_BUTTON_1
                      : RECTANGLE_BUTTON_2
                  }
                  style={styles.analysisHospitalisationButton}
                  onPress={() =>
                    onChange({
                      value: EABi18n({
                        language,
                        textStore,
                        path: "na.needs.hcProtection.C"
                      }),
                      key: "typeofWard"
                    })
                  }
                >
                  <Text style={styles.sumbitButton}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "na.needs.hcProtection.C"
                    })}
                  </Text>
                </EABButton>
              </View>
              <View style={styles.analysisSectionBorderLine} />
            </View>
          </KeyboardAwareScrollView>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisHospitalisationCostProtection.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  isHcProtectionError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
