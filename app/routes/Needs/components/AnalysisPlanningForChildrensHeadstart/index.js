import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";

const { FNA, CLIENT } = REDUCER_TYPES;

const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;

/**
 * AnalysisPlanningForChildrensHeadstart
 * @description Analysis - Planning For Children's Headstart - Template
 * */
export default class AnalysisPlanningForChildrensHeadstart extends Component {
  static updateProvidedForValue({ provideFor, key }) {
    const providedForArray = provideFor.split(",");
    const pIndex = providedForArray.indexOf(key);
    if (pIndex !== -1) {
      providedForArray.splice(pIndex, 1);
    } else {
      providedForArray.push(key);
    }
    return providedForArray.join();
  }

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    const index = na.pcHeadstart.dependants.findIndex(
      dependant => dependant.cid === selectedProfile
    );
    const dependants = _.cloneDeep(na.pcHeadstart.dependants);
    dependants[index] = {
      ...dependants[index],
      [key]: value,
      init: false
    };

    updateNA({
      naData: {
        ...na,
        pcHeadstart: {
          ...na.pcHeadstart,
          dependants
        },
        completedStep: 1
      },
      pdaData: pda,
      profileData: profile,
      dependantProfilesData: dependantProfiles,
      feData: fe
    });
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    // pcHeadstart only for children
    return data.dependants.find(val => val.cid === selectedProfile);
  }

  render() {
    const { updateProvidedForValue } = AnalysisPlanningForChildrensHeadstart;
    const { onChange } = this;
    const { textStore, isPcHeadstartError, selectedProfile } = this.props;
    const data = this.checkDependant();

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView extraScrollHeight={100}>
              <View key={123} style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.pcHeadstart"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.pcHeadstart.providedFor"
                  })}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                {isPcHeadstartError[selectedProfile].providedFor.hasError ? (
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    path={
                      isPcHeadstartError[selectedProfile].providedFor
                        .messagePath
                    }
                  />
                ) : null}
                <View style={styles.buttonWrapper}>
                  <EABButton
                    testID="NeedsAnalysis__PCH__btnProtection"
                    containerStyle={styles.firstButton}
                    buttonType={
                      data.providedFor.indexOf("P") !== -1
                        ? RECTANGLE_BUTTON_1
                        : RECTANGLE_BUTTON_2
                    }
                    style={styles.analysisHospitalisationButton}
                    onPress={() => {
                      onChange({
                        key: "providedFor",
                        value: updateProvidedForValue({
                          provideFor: data.providedFor,
                          key: "P"
                        })
                      });
                    }}
                  >
                    <Text style={styles.sumbitButton}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.protection"
                      })}
                    </Text>
                  </EABButton>
                  <EABButton
                    testID="NeedsAnalysis__PCH__btnCriticalIllness"
                    buttonType={
                      data.providedFor.indexOf("CI") !== -1
                        ? RECTANGLE_BUTTON_1
                        : RECTANGLE_BUTTON_2
                    }
                    style={styles.analysisHospitalisationButton}
                    onPress={() => {
                      onChange({
                        key: "providedFor",
                        value: updateProvidedForValue({
                          provideFor: data.providedFor,
                          key: "CI"
                        })
                      });
                    }}
                  >
                    <Text style={styles.sumbitButton}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.criticalIllness"
                      })}
                    </Text>
                  </EABButton>
                </View>
                {data.providedFor !== "" ? (
                  <View style={styles.analysisSectionBorderLine} />
                ) : null}
                {data.providedFor.indexOf("P") !== -1 ? (
                  <View>
                    <Text style={styles.analysisProductPointTitle}>
                      {`1. ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.sumAssured"
                      })} `}
                      <Text style={styles.requiredHint}> *</Text>
                    </Text>
                    <Text style={styles.analysisProductPointTitle}>
                      {`${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.protection"
                      })}`}
                    </Text>
                    <EABTextField
                      testID="NeedsAnalysis__PCH__txtProtectionSum"
                      style={styles.analysisTextInputShort}
                      prefix="$"
                      value={numberToCurrency({
                        value: data.sumAssuredProvided || 0,
                        decimal: 0
                      })}
                      isError={
                        isPcHeadstartError[selectedProfile].sumAssuredProvided
                          .hasError
                      }
                      hintText={EABi18n({
                        textStore,
                        language,
                        path:
                          isPcHeadstartError[selectedProfile].sumAssuredProvided
                            .messagePath
                      })}
                      onChange={value => {
                        onChange({
                          value:
                            value !== "" && value !== "0"
                              ? currencyToNumber(value)
                              : "",
                          key: "sumAssuredProvided"
                        });
                      }}
                    />
                    <Text style={styles.analysisProductPointTitle}>
                      {`2. ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.extInsurance"
                      })} - ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.protection"
                      })} ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.common.less"
                      })}`}
                    </Text>
                    <Text style={styles.analysisProductPointContent}>
                      {`$${numberToCurrency({
                        value: data.extInsurance || 0,
                        decimal: 0
                      })}`}
                    </Text>
                  </View>
                ) : null}
                {data.providedFor.indexOf("P") !== -1 &&
                data.providedFor.indexOf("CI") !== -1 ? (
                  <View style={styles.analysisSectionBorderLine} />
                ) : null}
                {data.providedFor.indexOf("CI") !== -1 ? (
                  <View>
                    <Text style={styles.analysisProductPointTitle}>
                      {`1. ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.sumAssured"
                      })} `}
                      <Text style={styles.requiredHint}> *</Text>
                    </Text>
                    <Text style={styles.analysisProductPointTitle}>
                      {`${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.criticalIllness"
                      })}`}
                    </Text>
                    <EABTextField
                      testID="NeedsAnalysis__PCH__txtCriticalIllnessSum"
                      style={styles.analysisTextInputShort}
                      prefix="$"
                      value={numberToCurrency({
                        value: data.sumAssuredCritical || 0,
                        decimal: 0
                      })}
                      isError={
                        isPcHeadstartError[selectedProfile].sumAssuredCritical
                          .hasError
                      }
                      hintText={EABi18n({
                        textStore,
                        language,
                        path:
                          isPcHeadstartError[selectedProfile].sumAssuredCritical
                            .messagePath
                      })}
                      onChange={value => {
                        onChange({
                          value:
                            value !== "" && value !== "0"
                              ? currencyToNumber(value)
                              : "",
                          key: "sumAssuredCritical"
                        });
                      }}
                    />
                    <Text style={styles.analysisProductPointTitle}>
                      {`2. ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.extInsurance"
                      })} - ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.pcHeadstart.criticalIllness"
                      })} ${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.common.less"
                      })}`}
                    </Text>
                    <Text style={styles.analysisProductPointContent}>
                      {`$${numberToCurrency({
                        value: data.ciExtInsurance || 0,
                        decimal: 0
                      })}`}
                    </Text>
                  </View>
                ) : null}
              </View>
              <View style={{ marginBottom: Theme.alignmentXXXXXL }} />
            </KeyboardAwareScrollView>
            {data.providedFor.indexOf("P") !== -1 && (
              <View style={[styles.shortfallView]}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path:
                      data.pTotShortfall >= 0
                        ? "na.needs.common.surplusContent"
                        : "na.needs.common.shortfallContent"
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.pcHeadstart.protection"
                    })}${EABi18n({
                      language,
                      textStore,
                      path:
                        data.pTotShortfall >= 0
                          ? "na.needs.pcHeadstart.surplus"
                          : "na.needs.pcHeadstart.shortfall"
                    })}: `}
                    <Text
                      style={
                        data.pTotShortfall >= 0
                          ? styles.surplusContent
                          : styles.shortfallContent
                      }
                    >
                      {`$${numberToCurrency({
                        value: Math.abs(data.pTotShortfall || 0),
                        decimal: 2
                      })} `}
                    </Text>
                  </Text>
                </View>
              </View>
            )}

            {data.providedFor.indexOf("CI") !== -1 && (
              <View
                style={
                  data.providedFor.indexOf("P") !== -1
                    ? [
                        styles.shortfallView,
                        { bottom: Theme.alignmentS * Theme.alignmentS }
                      ]
                    : styles.shortfallView
                }
              >
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path:
                      data.ciTotShortfall >= 0
                        ? "na.needs.common.surplusContent"
                        : "na.needs.common.shortfallContent"
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.pcHeadstart.criticalIllness"
                    })}${EABi18n({
                      language,
                      textStore,
                      path:
                        data.ciTotShortfall >= 0
                          ? "na.needs.pcHeadstart.surplus"
                          : "na.needs.pcHeadstart.shortfall"
                    })}: `}
                    <Text
                      style={
                        data.ciTotShortfall >= 0
                          ? styles.surplusContent
                          : styles.shortfallContent
                      }
                    >
                      {`$${numberToCurrency({
                        value: Math.abs(data.ciTotShortfall || 0),
                        decimal: 2
                      })} `}
                    </Text>
                  </Text>
                </View>
              </View>
            )}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisPlanningForChildrensHeadstart.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  initNaAnalysis: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisRule.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  updateNA: PropTypes.func.isRequired,
  isPcHeadstartError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
