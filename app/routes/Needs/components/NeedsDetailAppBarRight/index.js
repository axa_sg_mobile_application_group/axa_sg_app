import React, { Component } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import EABButton from "../../../../components/EABButton";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * NeedsDetailAppBarRight
 * @description needs detail page app bar right.
 * */
export default class NeedsDetailAppBarRight extends Component {
  constructor() {
    super();

    this.state = {
      isLoadingReport: false
    };
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { FNAReportInitialize, navigation } = this.props;
    const { isLoadingReport } = this.state;

    return (
      <View style={styles.container}>
        <EABButton
          style={styles.buttonFirst}
          onPress={async () => {
            /* this.setState(
             {
             isLoadingReport: true
             },
             () => {
             FNAReportInitialize(() => {
             this.setState(
             {
             isLoadingReport: false
             },
             () => {
             navigation.navigate("FNAReport");
             }
             );
             });
             }
             ); */
            await this.setState({ isLoadingReport: true });

            FNAReportInitialize(async () => {
              await this.setState({
                isLoadingReport: false
              });

              await navigation.navigate("FNAReport");
            });
          }}
        >
          <TranslatedText
            style={Theme.textButtonLabelNormalAccent}
            path="needs.view.fna.report"
          />
        </EABButton>
        <EABButton
          onPress={() => {
            navigation.navigate("FNASummary");
          }}
        >
          <TranslatedText
            style={Theme.textButtonLabelNormalAccent}
            path="needs.view.fna.summary"
          />
        </EABButton>
        <EABHUD isOpen={isLoadingReport} />
      </View>
    );
  }
}

NeedsDetailAppBarRight.propTypes = {
  FNAReportInitialize: PropTypes.func.isRequired
};
