import React, { Component } from "react";
import { View, ScrollView, Text, Image } from "react-native";
import PropTypes from "prop-types";
import traverse from "traverse";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  PROGRESS_TABS,
  DEPENDANT,
  NEEDS
} from "eab-web-api";
import * as _ from "lodash";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import diProtectionImg from "../../../../assets/images/diProtectionImg.png";
import ciProtectionImg from "../../../../assets/images/ciProtectionImg.png";
import ePlanningImg from "../../../../assets/images/ePlanningImg.png";
import fiProtectionImg from "../../../../assets/images/fiProtectionImg.png";
import hcProtectionImg from "../../../../assets/images/hcProtectionImg.png";
import otherAspectImg from "../../../../assets/images/otherAspectImg.png";
import paProtectionImg from "../../../../assets/images/paProtectionImg.png";
import pcHeadstartImg from "../../../../assets/images/pcHeadstartImg.png";
import psGoalsImg from "../../../../assets/images/psGoalsImg.png";
import rPlanningImg from "../../../../assets/images/rPlanningImg.png";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABChip from "../../../../components/EABChip";
import EABi18n from "../../../../utilities/EABi18n";
import Theme from "../../../../theme";
import styles from "./styles";

const { FNA, CLIENT } = REDUCER_TYPES;
const {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  PSGOALS,
  EPLANNING,
  RPLANNING,
  OTHER
} = NEEDS;

/**
 * <Needs />
 * @param {object} na - needs analysis data
 * @param {object} pda - application and dependant data
 * @param {object} dependantProfiles - dependant profiles data
 * @param {func} updateNAValue - the function that will be fire by buttons, radio buttons in Applicants and Dependents pages
 * @param {func} initNAAspects - correct the aspects in the redux state
 * */

export default class Needs extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    const { na } = this.props;

    const needs = [
      FIPROTECTION,
      CIPROTECTION,
      DIPROTECTION,
      PAPROTECTION,
      PCHEADSTART,
      HCPROTECTION,
      RPLANNING,
      EPLANNING,
      PSGOALS,
      OTHER
    ];

    const aspectsArr = _.cloneDeep(na.aspects || "").split(",");

    needs.forEach(need => {
      let isActive = false;
      if (na[need]) {
        traverse(na[need]).forEach(x => {
          if (x && x.isActive) {
            isActive = true;
          }
        });
      }

      if (!isActive) {
        const index = aspectsArr.indexOf(need);
        if (index > -1) {
          aspectsArr.splice(index, 1);
        }
      }
    });

    if (aspectsArr.length === 0) {
      this.props.initNAAspects("");
    } else {
      this.props.initNAAspects(aspectsArr.join());
    }
  }

  render() {
    const {
      OWNER,
      SPOUSE,
      SPO,
      SON,
      DAU,
      FAT,
      MOT,
      GFA,
      GMO,
      DEPENDANTS
    } = DEPENDANT;
    const {
      na,
      pda,
      dependantProfiles,
      textStore,
      profile,
      updateNA,
      fe
    } = this.props;
    const needsAspect = na.aspects ? na.aspects.split(",") : [];
    const spouseCid = Object.keys(dependantProfiles).find(
      key => dependantProfiles[key].relationship === SPO
    );

    const renderNeeds = language => {
      const needs = [
        {
          key: FIPROTECTION,
          testID: "IncomeProtection",
          title: EABi18n({
            path: "na.needs.fiProtection",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE],
          image: fiProtectionImg
        },
        {
          key: CIPROTECTION,
          testID: "CriticalIllnessProtection",
          title: EABi18n({
            path: "na.needs.ciProtection",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE],
          image: ciProtectionImg
        },
        {
          key: DIPROTECTION,
          testID: "DisabilityBenefit",
          title: EABi18n({
            path: "na.needs.diProtection",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE, SON, DAU],
          image: diProtectionImg
        },
        {
          key: PAPROTECTION,
          testID: "PersonalAccidentProtection",
          title: EABi18n({
            path: "na.needs.paProtection",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE, SON, DAU],
          image: paProtectionImg
        },
        {
          key: PCHEADSTART,
          testID: "PlanningForChildren",
          title: EABi18n({
            path: "na.needs.pcHeadstart",
            language,
            textStore
          }),
          dependant: [SON, DAU],
          image: pcHeadstartImg
        },
        {
          key: HCPROTECTION,
          testID: "PlanningForChildren",
          title: EABi18n({
            path: "na.needs.hcProtection",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE, SPO, SON, DAU, FAT, MOT, GFA, GMO],
          image: hcProtectionImg
        },
        {
          key: RPLANNING,
          testID: "HospitalisationCostProtection",
          title: EABi18n({
            path: "na.needs.rPlanning",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE],
          image: rPlanningImg
        },
        {
          key: EPLANNING,
          testID: "RetirementPlaning",
          title: EABi18n({
            path: "na.needs.ePlanning",
            language,
            textStore
          }),
          dependant: [SON, DAU],
          image: ePlanningImg
        },
        {
          key: PSGOALS,
          testID: "EducationPlanning",
          title: EABi18n({
            path: "na.needs.psGoals",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE],
          image: psGoalsImg
        },
        {
          key: OTHER,
          testID: "OtherNeeds",
          title: EABi18n({
            path: "na.needs.other",
            language,
            textStore
          }),
          dependant: [OWNER, SPOUSE],
          image: otherAspectImg
        }
      ];
      let needReset = false;
      const naData = _.cloneDeep(na);

      const renderRow = needs.map(needsObj => {
        const showOwner = needsObj.dependant.indexOf(OWNER) > -1;
        const showSpouse =
          needsObj.dependant.indexOf(SPOUSE) > -1 && pda.applicant === "joint";
        if (
          !showSpouse &&
          naData[needsObj.key] &&
          naData[needsObj.key].spouse &&
          naData[needsObj.key].spouse.isActive
        ) {
          naData[needsObj.key].spouse.isActive = false;
          needReset = true;
        }

        let numOfDependantsShown = 0;
        const dependantsArray = [];
        // check available dependants
        const availableDependantsArray = pda.dependants
          ? pda.dependants.split(",")
          : [];
        availableDependantsArray.forEach(cid => {
          if (
            ((pda.applicant === "joint" &&
              dependantProfiles[cid] &&
              dependantProfiles[cid].relationship !== "SPO") ||
              pda.applicant === "single") &&
            (dependantProfiles[cid] &&
              needsObj.dependant.indexOf(dependantProfiles[cid].relationship) >
                -1)
          ) {
            dependantsArray.push(cid);
            numOfDependantsShown += dependantsArray.length;
          }
        });

        // dependant button generation done. start to check and clean
        // protection dependants
        //
        // if protection have dependants
        if (naData[needsObj.key].dependants) {
          // check protection dependant is exist in available dependants list
          naData[needsObj.key].dependants.forEach(
            (protectionDependant, dependantIndex) => {
              // clean dependant isActive data
              if (
                protectionDependant.isActive &&
                dependantsArray.findIndex(
                  dependant => dependant === protectionDependant.cid
                ) === -1
              ) {
                naData[needsObj.key].dependants[
                  dependantIndex
                ].isActive = false;
                needReset = true;
              }
            }
          );
        }

        const numOfChips =
          (showOwner ? 1 : 0) + (showSpouse ? 1 : 0) + numOfDependantsShown;

        let isAnyDependantActive = false;
        if (dependantsArray.length !== 0 && naData[needsObj.key].dependants) {
          naData[needsObj.key].dependants.forEach(dependant => {
            isAnyDependantActive = isAnyDependantActive || dependant.isActive;
          });
        }

        const aspectArray = naData.aspects ? naData.aspects.split(",") : [];
        // if aspect is selected, but owner.isActive === false, spouse.isActive
        // === false, and do not have any available dependant, unselected this
        // aspect
        if (
          aspectArray.indexOf(needsObj.key) !== -1 &&
          !(
            naData[needsObj.key].owner && naData[needsObj.key].owner.isActive
          ) &&
          !(
            naData[needsObj.key].spouse && naData[needsObj.key].spouse.isActive
          ) &&
          !isAnyDependantActive
        ) {
          const protectionIndex = aspectArray.indexOf(needsObj.key);

          // only remove selected aspect
          aspectArray.splice(protectionIndex, 1);
          naData.aspects = aspectArray.join();
          needReset = true;
        }

        return (
          <View style={styles.needsRow}>
            <View style={styles.needsLeft}>
              <Image source={needsObj.image} style={styles.needsImage} />
            </View>
            <View style={styles.needsRight}>
              <Text style={styles.title}>{needsObj.title}</Text>
              <ScrollView
                style={styles.chipScrollView}
                contentContainerStyle={styles.chipWrapper}
                horizontal
                showsHorizontalScrollIndicator={false}
                scrollEnabled={numOfChips > 1}
              >
                {showOwner ? (
                  <EABChip
                    title={profile.fullName}
                    testID={`NeedsAnalysis__NS__chp${needsObj.testID}__${
                      profile.fullName
                    }`}
                    style={styles.chipStyle}
                    isSelected={
                      na[needsObj.key] &&
                      na[needsObj.key].owner &&
                      needsAspect.indexOf(needsObj.key) > -1
                        ? na[needsObj.key].owner.isActive
                        : false
                    }
                    isIconChip
                    onPress={option => {
                      this.props.updateNAValue({
                        target: PROGRESS_TABS[FNA].NEEDS,
                        value: {
                          need: needsObj.key,
                          client: OWNER,
                          cid: profile.cid,
                          isActive: option
                        }
                      });
                    }}
                  />
                ) : null}
                {showSpouse ? (
                  <EABChip
                    title={spouseCid && dependantProfiles[spouseCid].fullName}
                    testID={`NeedsAnalysis__NS__chp${needsObj.testID}__${
                      dependantProfiles[spouseCid].fullName
                    }`}
                    style={styles.chipStyle}
                    isSelected={
                      na[needsObj.key] &&
                      na[needsObj.key].spouse &&
                      needsAspect.indexOf(needsObj.key) > -1
                        ? na[needsObj.key].spouse.isActive
                        : false
                    }
                    isIconChip
                    onPress={option => {
                      this.props.updateNAValue({
                        target: PROGRESS_TABS[FNA].NEEDS,
                        value: {
                          need: needsObj.key,
                          client: SPOUSE,
                          cid: spouseCid,
                          isActive: option
                        }
                      });
                    }}
                  />
                ) : null}
                {dependantsArray.map(cid => {
                  const dependantIndex =
                    na[needsObj.key] && na[needsObj.key].dependants
                      ? na[needsObj.key].dependants.findIndex(
                          item => item.cid === cid
                        )
                      : false;

                  return (
                    <EABChip
                      title={dependantProfiles[cid].fullName}
                      testID={`NeedsAnalysis__NS__chp${needsObj.testID}__${
                        dependantProfiles[cid].fullName
                      }`}
                      style={styles.chipStyle}
                      isSelected={
                        dependantIndex !== false && dependantIndex !== -1
                          ? na[needsObj.key].dependants[dependantIndex].isActive
                          : false
                      }
                      isIconChip
                      onPress={option => {
                        this.props.updateNAValue({
                          target: PROGRESS_TABS[FNA].NEEDS,
                          value: {
                            need: needsObj.key,
                            client: DEPENDANTS,
                            cid,
                            isActive: option
                          }
                        });
                      }}
                    />
                  );
                })}
                {!showOwner && !showSpouse && dependantsArray.length === 0 ? (
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    path="error.712"
                  />
                ) : null}
              </ScrollView>
            </View>
          </View>
        );
      });

      if (!needReset) {
        return renderRow;
      }
      updateNA({
        naData,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
      return null;
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <ScrollView
            testID="NeedsAnalysis__PageScroll"
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
          >
            <View style={styles.container}>
              <Text style={styles.statement}>
                {EABi18n({
                  path: "na.needs.needTitle",
                  language,
                  textStore
                })}
                <Text style={styles.star}> *</Text>
              </Text>

              {renderNeeds(language)}

              <Text style={styles.statement}>
                {EABi18n({
                  path: "na.needs.statementTwo",
                  language,
                  textStore
                })}
              </Text>
            </View>
          </ScrollView>
        )}
      </ContextConsumer>
    );
  }
}

Needs.propTypes = {
  updateNA: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  updateNAValue: PropTypes.func.isRequired,
  initNAAspects: PropTypes.func.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};
