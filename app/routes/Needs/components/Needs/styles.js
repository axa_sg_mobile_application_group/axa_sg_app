import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  container: {
    paddingVertical: Theme.alignmentXL,
    alignItems: "flex-start",
    width: Theme.boxMaxWidth
  },
  statement: {
    fontSize: 18,
    paddingTop: 20,
    paddingBottom: 20
  },
  needsRow: {
    flexDirection: "row",
    marginBottom: 30
  },
  needsLeft: {
    flex: 1
  },
  needsImage: {
    width: 210,
    height: 120
  },
  star: {
    color: Theme.red
  },
  needsRight: { flex: 3 },
  needsTitle: {
    fontWeight: "bold",
    fontSize: 20
  },
  chipScrollView: {
    marginRight: -68,
    paddingRight: 68
  },
  chipWrapper: {
    flexDirection: "row",
    paddingTop: 15,
    paddingRight: Theme.alignmentXXXL
  },
  chipStyle: { marginRight: 10 }
});
