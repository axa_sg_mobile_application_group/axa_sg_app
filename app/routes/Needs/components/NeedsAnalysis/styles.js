import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const footer = {
  flexBasis: "auto",
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingHorizontal: Theme.alignmentXL,
  backgroundColor: Theme.white,
  height: 64,
  shadowColor: "rgba(0, 0, 0, 0.3)",
  shadowOffset: {
    width: 0,
    height: 0.5
  },
  shadowRadius: 0,
  shadowOpacity: 1
};
const questionWrapper = {
  paddingVertical: Theme.alignmentXL,
  alignItems: "flex-start",
  borderTopWidth: 1,
  borderColor: Theme.lightGrey,
  width: Theme.boxMaxWidth
};
const fieldWrapper = {
  marginTop: Theme.alignmentL,
  alignItems: "flex-start",
  width: "100%"
};
const cardRadioButtonContent = {
  justifyContent: "center",
  alignItems: "center",
  width: 192,
  height: 48
};
const textField = {
  maxWidth: 320
};
const termWrapper = {
  flexDirection: "row",
  alignItems: "flex-start",
  marginTop: Theme.subheadPrimary.lineHeight
};

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  /* header */
  header: {
    flexBasis: "auto",
    paddingTop: Theme.statusBarHeight + Theme.alignmentS,
    paddingBottom: 0,
    alignItems: "center",
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentXS,
    alignItems: "center",
    width: "100%"
  },
  /* form content */
  KeyboardAvoidingViewWrapper: {
    flex: 1,
    width: "100%",
    overflow: "hidden"
  },
  KeyboardAvoidingView: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  questionWrapper,
  questionWrapperFirst: {
    ...questionWrapper,
    paddingBottom: Theme.alignmentXXL,
    borderTopWidth: 0
  },
  question: {
    ...Theme.tagLine1Primary,
    maxWidth: Theme.textMaxWidth
  },
  fieldWrapper,
  fieldWrapperRow: {
    ...fieldWrapper,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  /* form application and dependants */
  cardRadioButtonFirst: {
    marginRight: Theme.alignmentL
  },
  cardRadioButtonContent,
  cardRadioButtonContentFirst: {
    ...cardRadioButtonContent,
    marginLeft: 0
  },
  /* client's accompaniment */
  descriptionText: {
    ...Theme.bodySecondary,
    maxWidth: Theme.textMaxWidth
  },
  trustedIndividualHeader: {
    marginLeft: -Theme.alignmentXL,
    marginBottom: Theme.alignmentL
  },
  textField,
  textFieldFirst: {
    ...textField,
    marginRight: Theme.alignmentXXXL
  },
  numberFieldWrapper: {
    flexDirection: "row",
    marginRight: Theme.alignmentXXXL,
    width: 320,
    height: "100%"
  },
  prefixField: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXL,
    width: 56
  },
  mobileField: {
    flex: 1
  },
  pickFromContactsButton: {
    marginTop: Theme.alignmentXL
  },
  /* consent notices */
  noticeCard: {
    flex: 1,
    flexDirection: "row",
    margin: Theme.alignmentXL,
    overflow: "hidden"
  },
  noticeColumnA: {
    flex: 1,
    alignSelf: "flex-start",
    marginRight: Theme.alignmentXL
  },
  noticeColumnB: {
    flex: 1,
    alignSelf: "flex-end"
  },
  noticeParagraphFirst: {
    ...Theme.subheadPrimary,
    marginBottom: Theme.subheadPrimary.lineHeight
  },
  noticeParagraphSecond: {
    ...Theme.subheadPrimary,
    marginBottom: Theme.subheadPrimary.lineHeight
  },
  termWrapper,
  termWrapperFirst: {
    ...termWrapper,
    marginTop: 0
  },
  termNumber: {
    ...Theme.subheadPrimary,
    flexBasis: "auto",
    marginRight: Theme.alignmentXS
  },
  termText: {
    ...Theme.subheadPrimary,
    flex: 1
  },
  familyChoicesWrapper: {
    paddingHorizontal: Theme.alignmentXL,
    paddingTop: Theme.alignmentXL,
    width: "100%"
  },
  choicesName: {
    ...Theme.tagLine1Primary,
    marginBottom: Theme.alignmentXS
  },
  choicesDescription: {
    ...Theme.bodySecondary,
    marginBottom: Theme.alignmentXS,
    maxWidth: Theme.textMaxWidth
  },
  /* dialog style */
  dialogHeader: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: Theme.alignmentXL,
    paddingBottom: Theme.alignmentL,
    height: 64,
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  dialogButton: {
    ...Theme.textButtonLabelNormalAccent,
    paddingHorizontal: Theme.alignmentXL
  },
  /* footer */
  footer,
  footerFirst: {
    ...footer,
    justifyContent: "flex-end"
  },
  footerLast: {
    ...footer,
    justifyContent: "flex-start"
  },
  footerPreviousButtonText: {
    ...Theme.rectangleButtonStyle1Label,
    marginRight: Theme.alignmentXXS
  }
});
