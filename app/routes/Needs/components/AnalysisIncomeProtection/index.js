import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import EABStepper from "../../../../components/EABStepper";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "../Analysis/styles";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import EASEAssets from "../../../../containers/EASEAssets";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;
const { calOtherAsset } = utilities.naAnalysis;

/**
 * AnalysisIncomeProtection
 * @description Analysis - Income Protection - Template
 * */
export default class AnalysisIncomeProtection extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          fiProtection: {
            ...na.fiProtection,
            [selectedProfile]: {
              ...na.fiProtection[selectedProfile],
              [key]: value,
              init: false
            }
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.fiProtection.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.fiProtection.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          fiProtection: {
            ...na.fiProtection,
            dependants
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  shortfallUI() {
    const data = this.checkDependant();
    const { totShortfall } = data;

    return {
      totShortfallPath:
        totShortfall >= 0
          ? "na.needs.common.surplus"
          : "na.needs.common.shortfall",
      totShortfallStyle:
        totShortfall >= 0 ? styles.surplusContent : styles.shortfallContent,
      totShortfallInfo:
        totShortfall >= 0
          ? "na.needs.common.surplusContent"
          : "na.needs.common.shortfallContent"
    };
  }

  render() {
    const {
      textStore,
      selectedProfile,
      profile,
      dependantProfiles,
      // assetsSelector,
      na,
      pda,
      updateNA,
      fe,
      isFiProtectionError
    } = this.props;
    const {
      totShortfallPath,
      totShortfallStyle,
      totShortfallInfo
    } = this.shortfallUI();
    const data = this.checkDependant();
    const spouseKey = Object.keys(dependantProfiles).find(
      key => dependantProfiles[key].relationship === "SPO"
    );
    const allowance =
      selectedProfile === "owner"
        ? profile.allowance
        : dependantProfiles[spouseKey].allowance;
    const pmtCurrency = numberToCurrency({
      value: data.pmt || 0,
      decimal: 0
    });
    const finExpensesCurrency = numberToCurrency({
      value: data.finExpenses || 0,
      decimal: 0
    });
    const selectedAssets = na.fiProtection[selectedProfile].assets;
    const { onChange } = this;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView
              extraScrollHeight={100}
              style={{ marginBottom: Theme.alignmentXXXXL }}
            >
              <View style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {`1. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.pmt"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__IP__txtPMT"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={pmtCurrency}
                  isError={isFiProtectionError[selectedProfile].pmt.hasError}
                  hintText={EABi18n({
                    path: isFiProtectionError[selectedProfile].pmt.messagePath,
                    language,
                    textStore
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        fiProtection: {
                          ...na.fiProtection,
                          [selectedProfile]: {
                            ...na.fiProtection[selectedProfile],
                            pmt:
                              value !== "0" || value !== ""
                                ? currencyToNumber(value)
                                : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  maxLength={11 + pmtCurrency.replace(/[^,]/g, "").length}
                />

                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.annualRepIncome"
                  })}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.annualRepIncome || 0,
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`}
                </Text>

                {!Number.isNaN(parseFloat(allowance)) &&
                allowance < data.pmt ? (
                  <View>
                    <Text style={styles.analysisProductPointText}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "na.needs.fiProtection.unMatchPmtReason"
                      })}
                      <Text style={styles.requiredHint}>*</Text>
                    </Text>
                    <EABTextBox
                      style={styles.analysisProductTextBox}
                      numberOfLines={5}
                      maxLength={200}
                      value={data.unMatchPmtReason || ""}
                      onChange={value =>
                        updateNA({
                          naData: {
                            ...na,
                            fiProtection: {
                              ...na.fiProtection,
                              [selectedProfile]: {
                                ...na.fiProtection[selectedProfile],
                                init: false,
                                unMatchPmtReason: value
                              }
                            },
                            completedStep: 1
                          },
                          pdaData: pda,
                          profileData: profile,
                          dependantProfilesData: dependantProfiles,
                          feData: fe
                        })
                      }
                    />
                    {isFiProtectionError[selectedProfile].unMatchPmtReason
                      .hasError ? (
                      <View>
                        <Text style={Theme.fieldTextOrHelperTextNegative}>
                          {EABi18n({
                            textStore,
                            language,
                            path:
                              isFiProtectionError[selectedProfile]
                                .unMatchPmtReason.messagePath
                          })}
                        </Text>
                      </View>
                    ) : null}
                  </View>
                ) : null}
                <View style={styles.analysisSectionBorderLine} />

                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`2. ${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.fiProtection.requireYrIncome"
                    })}`}
                    <Text style={styles.requiredHint}> *</Text>
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [500, 150] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.needs.fiProtection.requireYrIncomeToolTip"
                    })}
                  />
                </View>
                {isFiProtectionError[selectedProfile].requireYrIncome
                  .hasError ? (
                  <View>
                    <Text style={Theme.fieldTextOrHelperTextNegative}>
                      {EABi18n({
                        textStore,
                        language,
                        path:
                          isFiProtectionError[selectedProfile].requireYrIncome
                            .messagePath
                      })}
                    </Text>
                  </View>
                ) : null}
                <EABStepper
                  testID="NeedsAnalysis__IP"
                  value={
                    data.requireYrIncome === ""
                      ? data.requireYrIncome
                      : Number(data.requireYrIncome)
                  }
                  step={1}
                  min={0}
                  max={99}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        fiProtection: {
                          ...na.fiProtection,
                          [selectedProfile]: {
                            ...na.fiProtection[selectedProfile],
                            init: false,
                            requireYrIncome: value.toString()
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`3. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.iarRate2"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`${data.iarRate2 || na.iarRate || 0}%`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`4. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.lumpSum"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  ${numberToCurrency({
                    value: data.lumpSum || 0,
                    decimal: 2
                  })}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`5. ${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.fiProtection.totLiabilities"
                    })}`}
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [250, 100] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.hints.liabilities"
                    })}
                  />
                </View>
                <Text style={styles.analysisProductPointContent}>
                  ${numberToCurrency({
                    value:
                      data.totLiabilities ||
                      fe[selectedProfile].liabilities ||
                      0,
                    decimal: 0
                  })}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`6. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.finExpenses"
                  })}`}
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__IP__txtFinalExpernses"
                  style={styles.analysisTextInputLong}
                  prefix="$"
                  value={numberToCurrency({
                    value: data.finExpenses || 0,
                    decimal: 0
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        fiProtection: {
                          ...na.fiProtection,
                          [selectedProfile]: {
                            ...na.fiProtection[selectedProfile],
                            init: false,
                            finExpenses:
                              value !== "0" || value !== ""
                                ? currencyToNumber(value)
                                : ""
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  maxLength={
                    11 + finExpensesCurrency.replace(/[^,]/g, "").length
                  }
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`7. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.othFundNeedsName"
                  })}`}
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__IP__txtOtherFundingNeeds"
                  style={styles.analysisTextInputShort}
                  value={data.othFundNeedsName || ""}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        fiProtection: {
                          ...na.fiProtection,
                          [selectedProfile]: {
                            ...na.fiProtection[selectedProfile],
                            init: false,
                            othFundNeedsName: value
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  isError={
                    isFiProtectionError[selectedProfile].othFundNeedsName
                      .hasError
                  }
                  hintText={EABi18n({
                    textStore,
                    language,
                    path:
                      isFiProtectionError[selectedProfile].othFundNeedsName
                        .messagePath
                  })}
                  maxLength={30}
                />
                <EABTextField
                  testID="NeedsAnalysis__IP__txtOtherFundingNeedsValue"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={numberToCurrency({
                    value: data.othFundNeeds || 0,
                    decimal: 0
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        fiProtection: {
                          ...na.fiProtection,
                          [selectedProfile]: {
                            ...na.fiProtection[selectedProfile],
                            othFundNeeds:
                              value !== "0" || value !== ""
                                ? currencyToNumber(value)
                                : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  isError={
                    isFiProtectionError[selectedProfile].othFundNeeds.hasError
                  }
                  hintText={EABi18n({
                    textStore,
                    language,
                    path:
                      isFiProtectionError[selectedProfile].othFundNeeds
                        .messagePath
                  })}
                />

                <Text style={styles.analysisProductPointTitle}>
                  {`8. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.totRequired"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  ${numberToCurrency({
                    value: data.totRequired || 0,
                    decimal: 2,
                    maxValue: 999999999999999
                  })}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`9. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.fiProtection.existLifeIns"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  ${numberToCurrency({
                    value: data.existLifeIns || 0,
                    decimal: 0,
                    maxValue: 999999999999999
                  })}
                </Text>
                <View style={styles.analysisSectionBorderLine} />
                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`10. ${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.fiProtection.assetsSelector"
                    })}`}
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [365, 65] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.needs.assets.toolTip"
                    })}
                  />
                </View>
                <EASEAssets
                  testID="NeedsAnalysis__IP"
                  style={styles.analysisAssetsWrapper}
                  selectedProduct="fiProtection"
                  selectedProfile={selectedProfile}
                  selectedAssets={selectedAssets}
                  addAssetsOnPress={() => {
                    let cloneAssets = _.cloneDeep(selectedAssets);
                    if (!cloneAssets) {
                      cloneAssets = [];
                    }
                    cloneAssets.push({
                      calAsset: 0,
                      key: "all",
                      usedAsset: 0
                    });
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  removeAssetsOnPress={i => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets.forEach((assetsData, index) => {
                      if (i === index) {
                        cloneAssets.splice(index, 1);
                        onChange({ value: cloneAssets, key: "assets" });
                      }
                    });
                  }}
                  assetTextOnChange={(value, index) => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets[index] = {
                      key: selectedAssets[index].key,
                      usedAsset: currencyToNumber(value)
                    };
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  assetSelectorOnChange={(itemKey, item) => {
                    if (
                      !selectedAssets.find(select => select.key === item.value)
                    ) {
                      let cloneAssets = _.cloneDeep(selectedAssets);
                      if (selectedAssets[item.prevData.index]) {
                        cloneAssets[item.prevData.index] = {
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        };
                      } else {
                        cloneAssets = selectedAssets.concat({
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        });
                      }
                      onChange({ value: cloneAssets, key: "assets" });
                    }
                  }}
                  isProtectionError={isFiProtectionError[selectedProfile]}
                  calOtherAsset={({ key, selectedProduct }) =>
                    calOtherAsset({
                      key,
                      selectedProduct,
                      selectedProfile,
                      na
                    })
                  }
                />
              </View>
            </KeyboardAwareScrollView>
            <View style={styles.shortfallView}>
              <View style={styles.shortfallWrapper}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path: totShortfallInfo
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: totShortfallPath
                    })}:`}
                    <Text style={totShortfallStyle}>
                      {`$${numberToCurrency({
                        value: Math.abs(data.totShortfall || 0),
                        decimal: 2,
                        maxValue: 999999999999999
                      })}`}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisIncomeProtection.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  updateNA: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  // assetsSelector: ViewPropTypes.style.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  isFiProtectionError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
