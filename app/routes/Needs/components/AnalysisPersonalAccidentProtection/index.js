import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import EABTextField from "../../../../components/EABTextField";
import PopoverTooltip from "../../../../components/PopoverTooltip";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;

/**
 * AnalysisPersonalAccidentProtection
 * @description Analysis - Personal Accident Protection - Template
 * */
export default class AnalysisPersonalAccidentProtection extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          paProtection: {
            ...na.paProtection,
            [selectedProfile]: {
              ...na.paProtection[selectedProfile],
              [key]: value,
              init: false
            }
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.paProtection.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.paProtection.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          paProtection: {
            ...na.paProtection,
            dependants
          },
          completedStep: 1
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  profileData() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  shortfallUI() {
    const data = this.checkDependant();
    const { totShortfall } = data;

    return {
      totShortfallPath:
        totShortfall >= 0
          ? "na.needs.common.surplus"
          : "na.needs.common.shortfall",
      totShortfallStyle:
        totShortfall >= 0 ? styles.surplusContent : styles.shortfallContent,
      totShortfallInfo:
        totShortfall >= 0
          ? "na.needs.common.surplusContent"
          : "na.needs.common.shortfallContent"
    };
  }

  render() {
    const { textStore, selectedProfile, isPaProtectionError } = this.props;
    const { onChange } = this;
    const data = this.profileData();
    const {
      totShortfallPath,
      totShortfallStyle,
      totShortfallInfo
    } = this.shortfallUI();
    const pmtCurrency = numberToCurrency({
      value: data.pmt || 0,
      decimal: 0
    });

    return (
      <ContextConsumer>
        {({ language }) => (
          <View key={123} style={styles.analysisProductContent}>
            <Text style={styles.title}>
              {EABi18n({
                language,
                textStore,
                path: "na.needs.paProtection"
              })}
            </Text>
            <Text style={styles.analysisProductPointTitle}>
              {`1. ${EABi18n({
                language,
                textStore,
                path: "na.needs.paProtection.pmt"
              })}`}
              <Text style={styles.requiredHint}> *</Text>
            </Text>
            <EABTextField
              testID="NeedsAnalysis__PAP__txtPeronsalAccidentAmount"
              style={styles.analysisTextInputShort}
              prefix="$"
              value={pmtCurrency}
              isError={isPaProtectionError[selectedProfile].pmt.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: isPaProtectionError[selectedProfile].pmt.messagePath
              })}
              onChange={value => {
                onChange({
                  value:
                    value !== "0" || value !== ""
                      ? currencyToNumber(value)
                      : "",
                  key: "pmt"
                });
              }}
              onBlur={() => {
                onChange({
                  value:
                    pmtCurrency !== "0" || pmtCurrency !== ""
                      ? currencyToNumber(
                          currencyToNumber(pmtCurrency) > 0 &&
                          currencyToNumber(pmtCurrency) < 50000
                            ? "50,000"
                            : pmtCurrency
                        )
                      : "",
                  key: "pmt"
                });
              }}
              maxLength={11 + pmtCurrency.replace(/[^,]/g, "").length}
            />
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              {`2. ${EABi18n({
                language,
                textStore,
                path: "na.needs.paProtection.extInsurance"
              })}`}
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {`$${numberToCurrency({
                value: data.extInsurance || 0,
                decimal: 0
              })}`}
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <View style={styles.shortfallView}>
              <View style={styles.shortfallWrapper}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path: totShortfallInfo
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: totShortfallPath
                    })}: `}
                    <Text style={totShortfallStyle}>
                      {`$${numberToCurrency({
                        value: Math.abs(data.totShortfall || 0),
                        decimal: 2,
                        maxValue: 999999999999999
                      })}`}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisPersonalAccidentProtection.propTypes = {
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  selectedProfile: PropTypes.string.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  updateNA: PropTypes.func.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  isPaProtectionError: PropTypes.shape({}).isRequired
};
