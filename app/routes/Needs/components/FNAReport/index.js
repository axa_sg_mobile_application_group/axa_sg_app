import React, { PureComponent } from "react";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import Pdf from "react-native-pdf";
import styles from "./styles";

const { FNA } = REDUCER_TYPES;

/**
 * NeedsDetailAppBarRight
 * @description needs detail page app bar right.
 * */
export default class NeedsDetailAppBarRight extends PureComponent {
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { FNAReport } = this.props;

    return <Pdf style={styles.container} source={{ uri: FNAReport }} />;
  }
}

NeedsDetailAppBarRight.propTypes = {
  FNAReport: REDUCER_TYPE_CHECK[FNA].FNAReport.isRequired
};
