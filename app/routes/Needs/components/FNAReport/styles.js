import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  }
});
