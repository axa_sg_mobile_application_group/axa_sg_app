import {
  NEEDS,
  PROGRESS_TABS,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView, Text, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABStepper from "../../../../components/EABStepper";
import EABTextBox from "../../../../components/EABTextBox";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";

const { FNA, CLIENT } = REDUCER_TYPES;
const { CIPROTECTION, FIPROTECTION, RPLANNING } = NEEDS;

/**
 * <ROI />
 * @param {object} na - needs analysis data
 * */
export default class ROI extends Component {
  constructor() {
    super();

    this.state = {};
  }

  componentDidMount() {
    this.props.initCompletedStep(PROGRESS_TABS[FNA].ROI);
  }

  render() {
    const {
      na,
      textStore,
      updateNA,
      profile,
      dependantProfiles,
      fe,
      pda,
      iarRateNotMatchReasonOnChange
    } = this.props;

    const mandatory =
      na.aspects.indexOf(CIPROTECTION) > -1 ||
      na.aspects.indexOf(FIPROTECTION) > -1 ||
      na.aspects.indexOf(RPLANNING) > -1;
    const iarRate = parseFloat(na.iarRate);

    return (
      <ContextConsumer>
        {({ language }) => (
          <KeyboardAwareScrollView extraScrollHeight={40}>
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              <View style={styles.container}>
                <Text style={styles.statement}>
                  {EABi18n({
                    path: "na.ROI.iarRate",
                    language,
                    textStore
                  })}
                  {mandatory && <Text style={styles.star}> *</Text>}
                </Text>
                {na.iarRate === "" && mandatory ? (
                  <View>
                    <Text style={Theme.fieldTextOrHelperTextNegative}>
                      {EABi18n({
                        textStore,
                        language,
                        path: "error.302"
                      })}
                    </Text>
                  </View>
                ) : null}
                <EABStepper
                  testID="NeedsAnalysis__ROI"
                  step={0.5}
                  value={Number.isNaN(iarRate) ? "" : iarRate}
                  onChange={value => {
                    updateNA({
                      naData: {
                        ...na,
                        iarRate: value.toString(),
                        completedStep: 1
                      },
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe,
                      pdaData: pda
                    });
                  }}
                  min={-100}
                  max={100}
                />
                <Text style={styles.remark}>
                  {EABi18n({
                    path: "na.ROI.remark",
                    language,
                    textStore
                  })}
                </Text>
                {(parseFloat(na.iarRate) < 0 || parseFloat(na.iarRate) > 6) && (
                  <View style={styles.reasonWrapper}>
                    <Text style={styles.statement}>
                      {EABi18n({
                        path: "na.ROI.iarRateNotMatchReason",
                        language,
                        textStore
                      })}
                      <Text style={styles.star}> *</Text>
                    </Text>
                    <EABTextBox
                      style={styles.EABTextBox}
                      numberOfLines={5}
                      value={na.iarRateNotMatchReason}
                      onChange={iarRateNotMatchReasonOnChange}
                      maxLength={300}
                    />
                    {na.iarRateNotMatchReason === "" ? (
                      <View>
                        <Text style={Theme.fieldTextOrHelperTextNegative}>
                          {EABi18n({
                            textStore,
                            language,
                            path: "error.302"
                          })}
                        </Text>
                      </View>
                    ) : null}
                  </View>
                )}
              </View>
            </ScrollView>
          </KeyboardAwareScrollView>
        )}
      </ContextConsumer>
    );
  }
}

ROI.propTypes = {
  updateNA: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  iarRateNotMatchReasonOnChange: PropTypes.func.isRequired,
  initCompletedStep: PropTypes.func.isRequired
};
