import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  stepPage: {
    paddingVertical: Theme.alignmentXL
  },
  header: {
    alignItems: "flex-start",
    marginBottom: Theme.alignmentXL
  },
  headerText: {
    ...Theme.tagLine1Primary,
    marginBottom: Theme.alignmentXXS
  },
  learnMoreButtonIcon: {
    marginLeft: Theme.alignmentXXS,
    width: 24,
    height: 24
  },
  stepRow: {
    flexDirection: "row",
    width: "100%"
  },
  stepContainer: {
    flex: 1
  },
  step: {
    height: "100%",
    width: 308
  },
  stepImage: {
    width: "100%",
    height: 174
  },
  stepContent: {
    flex: 1,
    flexDirection: "row",
    padding: Theme.alignmentL
  },
  stepNumber: {
    ...Theme.headlinePrimary,
    flexBasis: "auto",
    marginRight: Theme.alignmentXXS
  },
  stepMainContent: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "space-between"
  },
  stepTextWrapper: {
    marginBottom: Theme.alignmentL,
    width: "100%"
  },
  stepContentTitle: {
    ...Theme.headlinePrimary,
    marginBottom: Theme.alignmentXXS
  },
  noRecordsContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    top: 223
  },
  noRecordsView: {
    alignItems: "center"
  },
  missingDataTitle: {
    ...Theme.tagLine1Primary,
    marginTop: Theme.alignmentXL
  },
  missingDataHint: {
    ...Theme.bodySecondary,
    marginTop: Theme.alignmentXS,
    marginBottom: Theme.alignmentXL
  },
  /* dialog style */
  headerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  incompleteText: {
    ...Theme.fieldTextOrHelperTextNegative
  }
});
