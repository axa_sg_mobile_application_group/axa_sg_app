import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const barContainer = {
  width: 160,
  height: "100%"
};

const barBase = {
  position: "relative",
  width: "100%"
};

export default StyleSheet.create({
  container: {
    flex: 0,
    flexBasis: "auto",
    flexDirection: "row",
    justifyContent: "space-between",
    height: "100%"
  },
  barContainer,
  barContainerFirst: {
    ...barContainer,
    marginRight: Theme.alignmentXXXL
  },
  /**
   * bar title
   * */
  titleWrapper: {
    flexBasis: "auto",
    alignItems: "center",
    marginBottom: Theme.alignmentL
  },
  /**
   * bar section
   * */
  barWrapper: {
    flex: 1,
    justifyContent: "flex-end",
    backgroundColor: Theme.lightGrey,
    width: "100%",
    height: "100%"
  },
  assetsBar: {
    ...barBase,
    backgroundColor: Theme.green
  },
  liabilitiesBar: {
    ...barBase,
    backgroundColor: Theme.red
  },
  currencyWrapper: {
    padding: Theme.alignmentM,
    position: "absolute",
    alignItems: "center",
    width: "100%"
  }
});
