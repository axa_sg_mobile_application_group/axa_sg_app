import { numberToCurrency } from "eab-web-api/lib/utilities/numberFormatter";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, View } from "react-native";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * @description financial evaluation section net worth chart components
 * @params {number} assets - assets total. MUST BE A NUMBER, we will change to
 *   currency in this component
 * @params {number} liabilities - assets total. MUST BE A NUMBER, we will change to
 *   currency in this component
 * */
export default class NetWorthChart extends Component {
  constructor() {
    super();

    this.state = {
      assetsTextHeight: 0,
      assetsBarHeight: 0,
      liabilitiesTextHeight: 0,
      liabilitiesBarHeight: 0
    };
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description get assets percentage
   * */
  get assetsPercentage() {
    const { assets, liabilities } = this.props;

    return assets > liabilities ? 1 : assets / liabilities;
  }
  /**
   * @description get liabilities percentage
   * */
  get liabilitiesPercentage() {
    const { assets, liabilities } = this.props;

    return liabilities > assets ? 1 : liabilities / assets;
  }
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { assetsPercentage, liabilitiesPercentage } = this;
    const { assets, liabilities } = this.props;
    const {
      assetsTextHeight,
      assetsBarHeight,
      liabilitiesTextHeight,
      liabilitiesBarHeight
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.barContainerFirst}>
          <View style={styles.barWrapper}>
            <View
              style={[
                styles.assetsBar,
                {
                  height: `${assetsPercentage * 100}%`
                }
              ]}
              onLayout={event => {
                this.setState({
                  assetsBarHeight: event.nativeEvent.layout.height
                });
              }}
            >
              <View
                style={[
                  styles.currencyWrapper,
                  assetsBarHeight <= assetsTextHeight
                    ? {
                        bottom: 0
                      }
                    : {
                        top: 0
                      }
                ]}
                onLayout={event => {
                  this.setState({
                    assetsTextHeight: event.nativeEvent.layout.height
                  });
                }}
              >
                <Text style={Theme.bodyPrimary}>{`$${numberToCurrency({
                  value: assets,
                  decimal: 0,
                  isFocus: false,
                  maxValue: 999999999999999
                })}`}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.barContainer}>
          <View style={styles.barWrapper}>
            <View
              style={[
                styles.liabilitiesBar,
                {
                  height: `${liabilitiesPercentage * 100}%`
                }
              ]}
              onLayout={event => {
                this.setState({
                  liabilitiesBarHeight: event.nativeEvent.layout.height
                });
              }}
            >
              <View
                style={[
                  styles.currencyWrapper,
                  liabilitiesBarHeight <= liabilitiesTextHeight
                    ? {
                        bottom: 0
                      }
                    : {
                        top: 0
                      }
                ]}
                onLayout={event => {
                  this.setState({
                    liabilitiesTextHeight: event.nativeEvent.layout.height
                  });
                }}
              >
                <Text style={Theme.bodyPrimary}>{`$${numberToCurrency({
                  value: liabilities,
                  decimal: 0,
                  isFocus: false,
                  maxValue: 999999999999999
                })}`}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

NetWorthChart.propTypes = {
  assets: PropTypes.number.isRequired,
  liabilities: PropTypes.number.isRequired
};
