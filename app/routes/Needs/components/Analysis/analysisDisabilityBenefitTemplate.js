import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import EABTextField from "../../../../components/EABTextField";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";
import { getCurrency, calFv, calTotCoverage } from "./analysisUtil";

// TODO: THIS FILE WILL BE REMOVED AND BE REPLACED BY A NEW FILE IN TEMPLATES FOLDER
/**
 * AnalysisCriticalIllnessProTemplate
 * @description Analysis - Critical Illness Protection - Template
 * */
export default class AnalysisCriticalIllnessProTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calculateData: {
        annualLivingExp: 0,
        lumpSum: 0,
        mtCost: 0
      }
    };
    this.setData = this.setData.bind(this);
  }

  componentWillReceiveProps() {
    this.setData();
  }

  setData() {
    const field = ["annualLivingExp", "lumpSum", "mtCost"];
    const cloneData = this.state.calculateData;
    for (let i = 0; i < field.length; i += 1) {
      cloneData[field[i]] = this.calculateData(field[i]);
    }
    this.setState({
      calculateData: cloneData
    });
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  calculateData(field) {
    const data = this.checkDependant();
    let newData = 0;
    if (!data || _.isEmpty(data)) {
      return newData;
    }
    switch (field) {
      case "annualLivingExp":
        newData = data.annualLivingExp * 12;
        break;
      case "lumpSum":
        newData = getCurrency(
          calFv(
            data.iarRate2 / 100,
            data.requireYrIncome,
            -data.pmt * 12,
            0,
            1
          ),
          "$",
          2
        );
        break;
      case "mtCost":
        newData = getCurrency(calTotCoverage(data), "$", 0);
        break;
      default:
        break;
    }
    return newData;
  }

  initValidate() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    let targetProfile = {};
    if (selectedProfile !== "spouse" && selectedProfile !== "owner") {
      targetProfile = data.dependants.find(val => val.cid === selectedProfile);
    } else {
      targetProfile = data[selectedProfile];
    }
    if (!targetProfile.validate) {
      return {};
    }
    return Object.keys(targetProfile.validate).reduce((preValue, currIndex) => {
      preValue[currIndex] = targetProfile.validate[currIndex];
      return preValue;
    }, {});
  }

  render() {
    const {
      // todo
      // textStore,
      dispatch,
      selectedProfile,
      rule
    } = this.props;
    const {
      pmtOnChange,
      requireYrIncomeOnChange,
      othRegIncomeOnChange
    } = dispatch;
    const validate = this.initValidate();
    const data = this.checkDependant();

    return (
      <ContextConsumer>
        {({ language }) => (
          <View key={123} style={styles.analysisProductContent}>
            <Text style={styles.analysisProductPointTitle}>
              1. Replacement Income (PMT)
            </Text>
            <EABTextField
              style={styles.analysisTextInputShort}
              prefix="$"
              value={data.pmt || 0}
              placeholder="Monthly Replacement Income"
              // TODO
              //   placeholder={EABi18n({
              //     language,
              //     textStore,
              //     path: "clientForm.otherField"
              //   })}
              isError={validate.pmt ? validate.pmt.hasError : false}
              hintText={validate.pmt ? validate.pmt.message[language] : ""}
              onChange={value =>
                pmtOnChange(
                  Object.assign(
                    {},
                    { selectedProfile, value },
                    rule.pmt ? { rule: rule.pmt } : { rule: null }
                  )
                )
              }
            />
            <Text style={styles.analysisProductPointText}>
              Annual Replacement Income: ${this.calculateData(
                "annualLivingExp"
              )}
            </Text>
            <View style={styles.analysisSectionBorderLine} />
            <Text style={styles.analysisProductPointTitle}>
              2. Years Income is Required (n)
            </Text>
            <EABStepper
              value={
                data.requireYrIncome ? parseInt(data.requireYrIncome, 10) : null
              }
              step={1}
              min={0}
              onChange={value =>
                requireYrIncomeOnChange({ selectedProfile, value })
              }
            />
            <View style={styles.analysisSectionBorderLine} />
            <Text style={styles.analysisProductPointTitle}>
              3. Existing yearly disablity benefit (Less)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {data.disabilityBenefit || 0}%
            </Text>
            <View style={styles.analysisSectionBorderLine} />
            <Text style={styles.analysisProductPointTitle}>
              4. Other Regular Income
            </Text>
            <EABTextField
              style={styles.analysisTextInputShort}
              prefix="$"
              value={data.othRegIncome || 0}
              placeholder="Monthly Replacement Income"
              // TODO
              //   placeholder={EABi18n({
              //     language,
              //     textStore,
              //     path: "clientForm.otherField"
              //   })}
              isError={
                validate.othRegIncome ? validate.othRegIncome.hasError : false
              }
              hintText={
                validate.othRegIncome
                  ? validate.othRegIncome.message[language]
                  : ""
              }
              onChange={value =>
                othRegIncomeOnChange(
                  Object.assign(
                    {},
                    { selectedProfile, value },
                    rule.othRegIncome
                      ? { rule: rule.othRegIncome }
                      : { rule: null }
                  )
                )
              }
            />
            <View style={styles.analysisSectionBorderLine} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisCriticalIllnessProTemplate.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  // TODO
  // textStore: PropTypes.oneOfType([PropTypes.object]).isRequired,
  data: PropTypes.oneOfType([PropTypes.object]).isRequired,
  rule: PropTypes.oneOfType([PropTypes.object]).isRequired
};
