import * as _ from "lodash";

export const getCurrency = (value, sign, decimals) => {
  if (value === null || sign === null || decimals === null) {
    return null;
  }
  if (!_.isNumber(value)) {
    return value;
  }
  if (decimals !== 0 && !_.isNumber(decimals)) {
    decimals = 2;
  }
  const text =
    (sign && sign.trim().length > 0 ? sign : "") +
    parseFloat(value).toFixed(decimals);
  const parts = text.split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
};

export const calFv = (fvRate, fvNper, fvPmt, fvPv) => {
  if (fvRate === 0) {
    return -fvPv - fvPmt * fvNper || 0;
  }
  return (
    (((1 - (1 + fvRate) ** fvNper) / fvRate) * fvPmt * (1 + fvRate * 1) - 0) /
      (1 + fvRate ** fvNper) || 0
  );
};

export const calTotRequired = values => {
  _.sumBy(["finExpenses", "totLiabilities", "lumpSum", "othFundNeeds"], id =>
    Number(values[id] || 0)
  );
};

export function calExitLifeIns(
  field,
  values,
  rootValues,
  validRefValues,
  params
) {
  values.existLifeIns = _.sumBy(["lifeInsProt", "invLinkPol"], id =>
    _.get(validRefValues, `fe.${params}.${id}`)
  );
}

export function calTotCoverage(values) {
  const { mtCost = 0, lumpSum = 0 } = values;
  return (
    parseInt(mtCost === "" ? 0 : mtCost, 10) +
    parseInt(lumpSum === "" ? 0 : lumpSum, 10)
  );
  // values.totCoverage = mtCost + lumpSum;
}
