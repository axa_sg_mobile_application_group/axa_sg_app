import PropTypes from "prop-types";
import * as _ from "lodash";
import React, { Component } from "react";
import { Text, View, ScrollView } from "react-native";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  NEEDS,
  utilities,
  PROGRESS_TABS
} from "eab-web-api";
import icCompleted from "../../../../assets/images/icCompleted.png";
import icIncomplete from "../../../../assets/images/icIncomplete.png";
import EABButton from "../../../../components/EABButton";
import EABSectionList from "../../../../components/EABSectionList";
import EABTextField from "../../../../components/EABTextField";
import SelectorField from "../../../../components/SelectorField";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import { RECTANGLE_BUTTON_2 } from "../../../../components/EABButton/constants";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import styles from "./styles";
import AnalysisIncomeProtection from "../../containers/AnalysisIncomeProtection";
import AnalysisCriticalIllnessProtection from "../../containers/AnalysisCriticalIllnessProtection";
import AnalysisPersonalAccidentProtection from "../../containers/AnalysisPersonalAccidentProtection";
import AnalysisHospitalisationCostProtection from "../../containers/AnalysisHospitalisationCostProtection";
import AnalysisPlanningForChildrensHeadstart from "../../containers/AnalysisPlanningForChildrensHeadstart";
import AnalysisDisabilityBenefit from "../../containers/AnalysisDisabilityBenefit";
import AnalysisRetirementPlanning from "../../containers/AnalysisRetirementPlanning";
import AnalysisEducationPlanning from "../../containers/AnalysisEducationPlanning";
import AnalysisOtherNeeds from "../../containers/AnalysisOtherNeeds";
import AnalysisPlanningForSpecificGoals from "../../containers/AnalysisPlanningForSpecificGoals";

const {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  HCPROTECTION,
  PSGOALS,
  RPLANNING,
  PCHEADSTART,
  EPLANNING,
  OTHER
} = NEEDS;
const { FNA, CLIENT } = REDUCER_TYPES;
const { analysisAspect } = utilities.naAnalysis;

export default class Analysis extends Component {
  constructor(props) {
    super(props);

    const selectedProduct = this.props.na.aspects.split(",")[0];

    const ownerIsInit =
      this.props.na[selectedProduct].owner &&
      this.props.na[selectedProduct].owner.init;
    const spouseIsInit =
      this.props.na[selectedProduct].spouse &&
      this.props.na[selectedProduct].spouse.init;
    const dependantIsInit =
      this.props.na[selectedProduct].dependants &&
      this.props.na[selectedProduct].dependants[0].init;

    if (ownerIsInit || spouseIsInit || dependantIsInit) {
      const naData = _.cloneDeep(this.props.na);
      if (ownerIsInit) {
        naData[selectedProduct].owner.init = false;
      } else if (spouseIsInit) {
        naData[selectedProduct].spouse.init = false;
      } else if (dependantIsInit) {
        naData[selectedProduct].dependants[0].init = false;
      }

      this.props.updateNA({
        naData,
        pdaData: this.props.pda,
        profileData: this.props.profile,
        dependantProfilesData: this.props.dependantProfiles,
        feData: this.props.fe,
        resetProdType: false
      });
    }

    this.state = {
      pageRenderReady: false,
      selectedProduct,
      selectedProfile: "",
      selectedAssets: "",
      totalAssetsValue: {
        owner: {
          savAcc: {
            productList: [],
            totalValue: 0
          },
          fixDeposit: {
            productList: [],
            totalValue: 0
          },
          invest: {
            productList: [],
            totalValue: 0
          },
          cpfOa: {
            productList: [],
            totalValue: 0
          },
          cpfSa: {
            productList: [],
            totalValue: 0
          },
          cpfMs: {
            productList: [],
            totalValue: 0
          },
          srs: {
            productList: [],
            totalValue: 0
          }
        },
        spouse: {
          savAcc: {
            productList: [],
            totalValue: 0
          },
          fixDeposit: {
            productList: [],
            totalValue: 0
          },
          invest: {
            productList: [],
            totalValue: 0
          },
          cpfOa: {
            productList: [],
            totalValue: 0
          },
          cpfSa: {
            productList: [],
            totalValue: 0
          },
          cpfMs: {
            productList: [],
            totalValue: 0
          },
          srs: {
            productList: [],
            totalValue: 0
          }
        }
      }
    };
    this.checkDependant = this.checkDependant.bind(this);
    this.getAssets = this.getAssets.bind(this);
    this.genAssetsSelector = this.genAssetsSelector.bind(this);
    this.initAssetsState = this.initAssetsState.bind(this);
    this.totalAssetsCal = this.totalAssetsCal.bind(this);
    this.genSectionListData = this.genSectionListData.bind(this);
    this.isDependantHasError = this.isDependantHasError.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    const { selectedProduct, selectedProfile } = state;

    if (state.selectedProfile !== "") {
      if (
        selectedProfile !== "owner" &&
        selectedProfile !== "spouse" &&
        props.na[selectedProduct].dependants
      ) {
        const index = props.na[selectedProduct].dependants.findIndex(
          dependant => dependant.cid === state.selectedProfile
        );

        if (
          index !== -1 &&
          props.na[selectedProduct].dependants[index].isActive
        ) {
          return null;
        }
      } else if (
        props.na[selectedProduct][state.selectedProfile] &&
        props.na[selectedProduct][state.selectedProfile].isActive
      ) {
        return null;
      }
    }

    let newSelectedProfile = "";
    if (
      props.na[selectedProduct].owner &&
      props.na[selectedProduct].owner.isActive
    ) {
      newSelectedProfile = "owner";
    } else if (
      props.na[selectedProduct].spouse &&
      props.na[selectedProduct].spouse.isActive
    ) {
      newSelectedProfile = "spouse";
    } else {
      newSelectedProfile = props.na[selectedProduct].dependants.find(
        dependant => dependant.isActive
      ).cid;
    }

    return {
      selectedProfile: newSelectedProfile
    };
  }

  componentDidMount() {
    this.props.initCompletedStep(PROGRESS_TABS[FNA].ANALYSIS);
    this.initAssetsState();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selectedProduct !== this.state.selectedProduct ||
      (prevState.selectedProfile !== this.state.selectedProfile &&
        // only "owner" and "spouse" should render assets selector on pages
        (this.state.selectedProfile === "owner" ||
          this.state.selectedProfile === "spouse"))
    ) {
      this.initAssetsState();
    }
  }

  /**
   * getAssets
   * @description Assets is the first part of "Needs - Financial Evaluation",
   * each profile should have it's own dataset,
   * don't need profile other than "owner" and "spouse" under the relationship under the "owner"(current login user).
   * */
  getAssets() {
    const { selectedProduct, selectedProfile } = this.state;
    const { fe } = this.props;
    const { assets } = analysisAspect;
    let targetProfile = {};
    // dependants part, but accroding to web, all the dependants might not need assets
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      let targetIndex = null;
      fe.dependants.some((data, index) => {
        if (data.cid === selectedProfile) {
          targetIndex = index;
          return true;
        }
        return false;
      });
      targetProfile = fe.dependants[targetIndex];
    } else {
      targetProfile = fe[selectedProfile];
    }

    if (!targetProfile) {
      return {};
    }

    return assets
      .filter(ass => ass.needsRequired.find(need => need === selectedProduct))
      .map(ass => ({
        title: ass.title,
        value: ass.value,
        totlaValue: targetProfile[ass.value]
      }));
  }

  isDependantHasError(id) {
    const { isNaError } = this.props;
    const { selectedProduct } = this.state;
    let hasError = false;
    const dependantErrorData = isNaError[selectedProduct][id];

    Object.keys(dependantErrorData).forEach(key => {
      const errorData = dependantErrorData[key];

      if (Array.isArray(errorData)) {
        errorData.forEach((goal, goalIndex) => {
          const goalError = errorData[goalIndex];

          Object.keys(goalError).forEach(goalErrorKey => {
            hasError = goalError[goalErrorKey].hasError || hasError;
          });
        });
      } else {
        hasError = errorData.hasError || hasError;
      }
    });

    return hasError;
  }

  /**
   * initAssetsState
   * @description Assets is the first part of "Needs - Financial Evaluation",
   * each profile should have it's own dataset,
   * don't need profile other than "owner" and "spouse" under the relationship under the "owner"(current login user).
   * */
  initAssetsState() {
    const { na } = this.props;
    const { selectedProduct, selectedProfile } = this.state;
    if (
      selectedProduct &&
      selectedProfile &&
      na[selectedProduct] &&
      na[selectedProduct][selectedProfile]
    ) {
      this.setState({
        selectedAssets: na[selectedProduct][selectedProfile].assets
        // TODO
        // selectedAssets: analysis[selectedProduct][
        //   selectedProfile
        // ].assets.concat({
        //   calAsset: 2,
        //   key: "fixDeposit",
        //   usedAsset: 2
        // })
      });
    }
  }

  // initProfileValidate() {
  //   const { analysis } = this.props;
  //   const { selectedProduct, selectedProfile } = this.state;
  //   if (selectedProduct && selectedProfile) {
  //     this.setState({
  //       profileValidate: Object.assign({}, this.state.profileValidate, Object.assign({}, this.state.profileValidate[selectedProduct][selectedProfile].isAc))
  //     })
  //   }

  // }

  totalAssetsCal(key, newValue) {
    const { selectedProfile, totalAssetsValue, selectedProduct } = this.state;
    if (!_.get(totalAssetsValue, `${selectedProfile}.${key}.productList`)) {
      return 0;
    }
    const cloneProductList = _.clone(
      totalAssetsValue[selectedProfile][key].productList
    );
    let targetObj = {};
    let targetIndex = null;
    cloneProductList.some((data, index) => {
      if (data.product === selectedProduct) {
        targetObj = data;
        targetIndex = index;
        return true;
      }
      return false;
    });
    if (targetObj === {} || targetIndex === null) {
      cloneProductList.push({
        product: selectedProduct,
        value: newValue
      });
    } else {
      cloneProductList[targetIndex].value = newValue;
    }
    this.setState({
      totalAssetsValue: Object.assign({}, totalAssetsValue, {
        [selectedProfile]: Object.assign(
          {},
          totalAssetsValue[selectedProfile],
          {
            [key]: Object.assign({}, totalAssetsValue[selectedProfile][key], {
              productList: cloneProductList
            })
          }
        )
      })
    });
    if (cloneProductList.length > 1) {
      return cloneProductList.reduce(
        (prev, curr) => (prev.value || prev) + curr.value
      );
    }
    return cloneProductList[0].value;
  }

  /**
   * NeedsDetail
   * @description Assets is the first part of "Needs Analysis - Financial Evaluation",
   * each profile should have it's own dataset,
   * don't need profile other than "owner" and "spouse" under the relationship under the "owner"(current login user).
   * */
  // todo
  genAssetsSelector(language) {
    // =============================================================================
    // variables
    // =============================================================================
    const {
      removeAsset,
      assetsValueOnChange,
      isNaError,
      textStore
    } = this.props;

    const { selectedProduct, selectedProfile, selectedAssets } = this.state;
    const { rule } = analysisAspect.needs.find(
      need => need.value === selectedProduct
    );
    const addAssetButton = () => {
      let cloneAssets = _.cloneDeep(selectedAssets);
      if (!cloneAssets) {
        cloneAssets = [];
      }
      cloneAssets.push({
        calAsset: 0,
        key: "all",
        usedAsset: 0
      });
      this.setState({
        selectedAssets: cloneAssets
      });
    };
    if (!selectedAssets) {
      return (
        <View>
          <EABButton
            buttonType={RECTANGLE_BUTTON_2}
            containerStyle={styles.analysisAddAssetButton}
            onPress={addAssetButton}
          >
            <Text style={styles.analysisRemoveButtonText}>
              {EABi18n({
                language,
                textStore,
                path: "na.needs.fiProtection.button.add"
              })}
            </Text>
          </EABButton>
        </View>
      );
    }
    const Input = [];
    const allAssets = this.getAssets();

    const profileError = () => {
      const isError = isNaError[selectedProduct];
      if (!isError || _.isEmpty(isError)) {
        return {};
      }
      if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
        if (isError.dependants && isError.dependants.length) {
          return isError.dependants.find(val => val.cid === selectedProfile);
        }
      } else {
        return isError[selectedProfile];
      }
      return "";
    };

    const assetsOnChange = (value, index) => {
      const clearFirstZero = string => {
        if (
          string.length > 1 &&
          (string.charAt(0) === 0 || string.charAt(0) === "0")
        ) {
          return string.substr(1, string.length - 1);
        }
        return string;
      };
      const { fe, na } = this.props;
      const newAssetsArr = selectedAssets;
      const newAssetsValue =
        value.indexOf("$") > -1 ? value.replace("$", "") : value;
      const totalVal = this.totalAssetsCal(
        selectedAssets[index].key,
        parseInt(newAssetsValue, 10)
      );
      newAssetsArr[index] = {
        calAsset: clearFirstZero(newAssetsValue),
        key: selectedAssets[index].key,
        usedAsset: clearFirstZero(newAssetsValue),
        totalValue: totalVal
      };
      const maxValue = fe[selectedProfile][selectedAssets[index].key];
      assetsValueOnChange(
        Object.assign(
          {},
          {
            selectedProfile,
            selectedProduct,
            key: selectedAssets[index].key,
            index,
            value: newAssetsArr,
            function: `${selectedProduct}AssetsOnChange`,
            analysisData: Object.assign(
              {},
              na[selectedProduct][selectedProfile],
              {
                iarRate: na.iarRate || 0,
                feProfile: fe[selectedProfile],
                assets: newAssetsArr
              }
            )
          },
          rule.assets
            ? {
                rule: rule.assets.max
                  ? Object.assign({}, rule.assets, {
                      max: parseInt(
                        rule.assets.max.replace("{max}", maxValue),
                        10
                      )
                    })
                  : rule.assets
              }
            : { rule: null }
        )
      );
    };
    const removeAssetButton = key => {
      const cloneAssets = _.cloneDeep(selectedAssets);
      cloneAssets.some((data, index) => {
        if (data.key === key) {
          cloneAssets.splice(index, 1);
          removeAsset(
            {
              selectedProfile,
              selectedProduct,
              value: cloneAssets || null
            },
            this.setState({
              selectedAssets: cloneAssets
            })
          );
          return true;
        }
        return false;
      });
    };
    // @ItemOnPress
    // all selector share the same assets list
    // @check-logic IF selectorA selected "A", then no other can select "A" again
    // @trigger-logic ONCE selector selected, the value input should reset to 0
    const ItemOnPress = (itemKey, item) => {
      // @check-logic
      if (!selectedAssets.find(select => select.key === item.value)) {
        let newAssetsArr = selectedAssets;
        if (selectedAssets[item.prevData.index]) {
          newAssetsArr[item.prevData.index] = {
            calAsset: 0,
            key: item.value,
            usedAsset: 0,
            product: [selectedProduct]
          };
          // @trigger-logic
          assetsOnChange("0", item.prevData.index);
        } else {
          newAssetsArr = selectedAssets.concat({
            calAsset: 0,
            key: item.value,
            usedAsset: 0,
            product: [selectedProduct]
          });
          // @trigger-logic
          assetsOnChange("0", newAssetsArr.length - 1);
        }
        this.setState({
          selectedAssets: newAssetsArr
        });
      }
    };

    const error = profileError();

    selectedAssets.forEach((selectedAsset, i) => {
      const targetAssets = allAssets.find(
        ass => ass.value === selectedAssets[i].key
      );
      Input.push(
        <View style={{ flexDirection: "row" }}>
          <SelectorField
            index={i}
            style={styles.analysisSelectorHalfWidth}
            value={targetAssets ? targetAssets.title : null}
            placeholder="Asset"
            selectorType={FLAT_LIST}
            flatListOptions={{
              renderItemOnPress: ItemOnPress,
              keepPrevData: true,
              data: allAssets
                .filter(
                  ass =>
                    !selectedAssets.find(select => select.key === ass.value)
                )
                .map(ass => ({
                  label: ass.title,
                  value: ass.value,
                  key: ass.value
                }))
            }}
            isRequired
          />
          <EABTextField
            style={styles.analysisTextFieldHalfWidth}
            value={selectedAssets[i].usedAsset}
            placeholder="Value"
            isError={_.get(error, `assets.${i}.hasError`)}
            hintText={
              _.get(error, `assets.${i}.hasError`)
                ? _.get(error, `assets.${i}.message.English`)
                : ""
            }
            onChange={(value, index = i) => assetsOnChange(value, index)}
            isRequired
          />
          <EABButton
            buttonType={RECTANGLE_BUTTON_2}
            containerStyle={styles.analysisRemoveButton}
            onPress={() => removeAssetButton(selectedAssets[i].key)}
          >
            <Text style={styles.analysisRemoveButtonText}>Remove</Text>
          </EABButton>
        </View>
      );
    });
    return (
      <View>
        {Input}
        <EABButton
          buttonType={RECTANGLE_BUTTON_2}
          containerStyle={styles.analysisAddAssetButton}
          onPress={addAssetButton}
        >
          <Text style={styles.analysisRemoveButtonText}>
            {EABi18n({
              language,
              textStore,
              path: "na.needs.fiProtection.button.add"
            })}
            {/* Add additional value of assets to be used */}
          </Text>
        </EABButton>
      </View>
    );
  }

  /**
   * genProfileButton
   * @description Generate the profile set button (tab button) on the right section, will switch to different profile set while different product selected
   * @param {object} analysis.dependantProfiles analysis.dependantProfiles stored all the releationship base on the "owner"(current user)
   * */
  genProfileButton() {
    const { isDependantHasError } = this;
    const { na, dependantProfiles, pda, updateNA, profile, fe } = this.props;
    const { selectedProduct, selectedProfile } = this.state;
    const segmentedControlArray = [];

    if (_.get(na, `[${selectedProduct}].owner.isActive`)) {
      segmentedControlArray.push({
        key: "owner",
        title: profile.fullName,
        isShowingBadge: true,
        source: isDependantHasError("owner") ? icIncomplete : icCompleted,
        onPress: () => {
          const naData = _.cloneDeep(na);
          naData[selectedProduct].owner.init = false;
          updateNA({
            naData,
            pdaData: pda,
            profileData: profile,
            dependantProfilesData: dependantProfiles,
            feData: fe,
            resetProdType: false
          });
          this.setState({ selectedProfile: "owner" });
        }
      });
    }

    if (
      pda.applicant === "joint" &&
      _.get(na, `[${selectedProduct}].spouse.isActive`)
    ) {
      const spouseKey = Object.keys(dependantProfiles).find(
        key => dependantProfiles[key].relationship === "SPO"
      );
      segmentedControlArray.push({
        key: "spouse",
        title: dependantProfiles[spouseKey].fullName,
        isShowingBadge: true,
        source: isDependantHasError("spouse") ? icIncomplete : icCompleted,
        onPress: () => {
          const naData = _.cloneDeep(na);
          naData[selectedProduct].spouse.init = false;
          updateNA({
            naData,
            pdaData: pda,
            profileData: profile,
            dependantProfilesData: dependantProfiles,
            feData: fe,
            resetProdType: false
          });
          this.setState({ selectedProfile: "spouse" });
        }
      });
    }

    if (pda.dependants !== "") {
      pda.dependants.split(",").forEach(cid => {
        if (
          na[selectedProduct].dependants &&
          na[selectedProduct].dependants.find(
            dependant => dependant.cid === cid && dependant.isActive
          )
        ) {
          segmentedControlArray.push({
            key: cid,
            title: dependantProfiles[cid].fullName,
            isShowingBadge: true,
            source: isDependantHasError(cid) ? icIncomplete : icCompleted,
            onPress: () => {
              const naData = _.cloneDeep(na);
              const dependantIndex = naData[
                selectedProduct
              ].dependants.findIndex(dependant => dependant.cid === cid);
              naData[selectedProduct].dependants[dependantIndex].init = false;
              updateNA({
                naData,
                pdaData: pda,
                profileData: profile,
                dependantProfilesData: dependantProfiles,
                feData: fe,
                resetProdType: false
              });
              this.setState({
                selectedProfile: cid
              });
            }
          });
        }
      });
    }

    const controlIndex = _.findIndex(
      segmentedControlArray,
      control => control.key === selectedProfile
    );

    return (
      <ScrollView
        horizontal
        scrollEnabled={segmentedControlArray.length > 3}
        showsHorizontalScrollIndicator={false}
      >
        <EABSegmentedControl
          testID="NeedsAnalysis__ANA"
          style={[styles.analysisProfileButton]}
          segments={segmentedControlArray}
          activatedKey={
            controlIndex > 0 ? selectedProfile : segmentedControlArray[0].key
          }
        />
      </ScrollView>
    );
  }

  /**
   * genSectionListData
   * @description Generate the product set button (tab button) on the left section, will switch to different product set while different product selected.
   * @param {string} preNeedsProductsList remapped from backend, same as the choices chosen from "Needs Analysis - Needs Analysis(the third tab) - Needs"
   * @returns {react-object} return button set of object
   * */
  genSectionListData() {
    const { na } = this.props;
    const { selectedProduct } = this.state;
    const data = [];
    if (na.aspects) {
      na.aspects.split(",").forEach(value => {
        const filterNeedsObj = _.find(
          analysisAspect.needs,
          need => need.value === value
        );
        data.push({
          key: value,
          id: value,
          title: _.get(filterNeedsObj, "title"),
          testID: _.get(filterNeedsObj, "testID"),
          selected: value === selectedProduct,
          completed: _.get(na, `[${filterNeedsObj.value}].isValid`)
        });
      });
    }
    return data;
  }

  /**
   * checkDependant
   * @description Generate different dependant(profile) while switch to different product
   * @requires analysisAspect analysisAspect is the pre-set validate object imported from "./option.js"
   * @param {array} analysisAspect.need stored all product validate rule
   * */
  checkDependant(cid, relationship) {
    const { selectedProduct } = this.state;
    const { na } = this.props;
    if (
      !analysisAspect.needs
        .find(need => need.value === selectedProduct)
        .relationshipRequired.find(rel => rel === relationship)
    ) {
      return false;
    } else if (na[selectedProduct].dependants) {
      const dependant = na[selectedProduct].dependants.find(
        dep => dep.cid === cid
      );
      if (!dependant) {
        return false;
      } else if (!dependant.isActive) {
        return false;
      }
    }
    return true;
  }

  /**
   * genProductContent
   * @description Generate different product page content while switch to different product page
   * @requires analysisAspect analysisAspect is the pre-set validate object imported from "./option.js"
   * @param {booleabn} pageRenderReady ensure all params are setted and ready to generate page
   * */
  // TODO blue popup button
  genProductContent(language) {
    const { selectedProduct, pageRenderReady } = this.state;
    const { na, isNaError, profile } = this.props;
    const { selectedProfile } = this.state;
    let assetsSelector = null;
    let aspect = {};
    const isError = isNaError[selectedProduct];

    if (selectedProduct && pageRenderReady) {
      // only "owner" and "spouse" should render assets selector on pages
      if (selectedProfile === "owner" || selectedProfile === "spouse") {
        assetsSelector = this.genAssetsSelector(language);
      }
      // get the product rule from analysis aspect
      aspect = analysisAspect.needs.find(
        need => need.value === selectedProduct
      );
    }

    if (selectedProduct === FIPROTECTION) {
      return (
        <AnalysisIncomeProtection
          selectedProfile={selectedProfile}
          data={na.fiProtection || null}
          rule={aspect.rule || {}}
          profile={profile}
          assetsSelector={assetsSelector}
          isError={isError}
        />
      );
    } else if (selectedProduct === CIPROTECTION) {
      return (
        <AnalysisCriticalIllnessProtection
          selectedProfile={selectedProfile}
          data={na[CIPROTECTION] || null}
          rule={aspect.rule || {}}
          assetsSelector={assetsSelector}
          isError={isError}
        />
      );
    } else if (selectedProduct === DIPROTECTION) {
      return (
        <AnalysisDisabilityBenefit
          selectedProfile={selectedProfile}
          data={na[DIPROTECTION] || null}
          rule={aspect.rule || {}}
          assetsSelector={assetsSelector}
          isError={isError}
        />
      );
    } else if (selectedProduct === PAPROTECTION) {
      return (
        <AnalysisPersonalAccidentProtection
          selectedProfile={selectedProfile}
          data={na.paProtection || null}
          rule={aspect.rule || {}}
          isError={isError}
        />
      );
    } else if (selectedProduct === HCPROTECTION) {
      return (
        <AnalysisHospitalisationCostProtection
          selectedProfile={selectedProfile}
          data={na.hcProtection || null}
          rule={aspect.rule || {}}
          isError={isError}
        />
      );
    } else if (selectedProduct === PSGOALS) {
      return (
        <AnalysisPlanningForSpecificGoals
          selectedProfile={selectedProfile}
          data={na[PSGOALS] || null}
          rule={aspect.rule || {}}
          isError={isError}
          assetsSelector={assetsSelector}
        />
      );
    } else if (selectedProduct === RPLANNING) {
      return (
        <AnalysisRetirementPlanning
          selectedProfile={selectedProfile}
          data={na[RPLANNING] || null}
          rule={aspect.rule || {}}
          assetsSelector={assetsSelector}
          isError={isError}
        />
      );
    } else if (selectedProduct === PCHEADSTART) {
      return (
        <AnalysisPlanningForChildrensHeadstart
          selectedProfile={selectedProfile}
          data={na[PCHEADSTART] || null}
          rule={aspect.rule || {}}
          isError={isError}
        />
      );
    } else if (selectedProduct === EPLANNING) {
      return (
        <AnalysisEducationPlanning
          selectedProfile={selectedProfile}
          data={na[EPLANNING] || null}
          rule={aspect.rule || {}}
          isError={isError}
        />
      );
    } else if (selectedProduct === OTHER) {
      return (
        <AnalysisOtherNeeds
          selectedProfile={selectedProfile}
          data={na[OTHER] || null}
          rule={aspect.rule || {}}
          isError={isError}
        />
      );
    }
    return null;
  }

  /**
   * @description save NA after click yes in confirmation alert
   * */
  saveNaWithConfirm(callback) {
    this.props.saveNA({
      confirm: true,
      callback
    });
  }

  /**
   * @description save NA after clicking Done button
   * */
  saveNaWithoutConfirm({ language, callback }) {
    const { textStore, isChanged, saveNA } = this.props;
    if (isChanged) {
      saveNA({
        confirm: false,
        callback: code => {
          if (code) {
            setTimeout(() => {
              confirmationAlert({
                messagePath: `error.${code}`,
                language,
                textStore,
                yesOnPress: () => {
                  this.saveNaWithConfirm(callback);
                }
              });
            }, 200);
          } else {
            callback();
          }
        }
      });
    } else {
      callback();
    }
  }

  render() {
    const {
      textStore,
      na,
      pda,
      profile,
      dependantProfiles,
      updateNA,
      fe
    } = this.props;
    const DependantProfiles = this.genProfileButton();

    return (
      <ContextConsumer>
        {({ language }) => {
          const ProductContent = this.genProductContent(language);
          const sections = [
            {
              key: "selectedNeeds",
              data: this.genSectionListData(),
              title: _.toUpper(
                EABi18n({
                  path: "na.analysis.section.selectedNeeds",
                  language,
                  textStore
                })
              )
            }
          ];

          return (
            <View key="a" style={styles.analysisTopView}>
              <ScrollView style={styles.listScrollView}>
                <EABSectionList
                  testID="NeedsAnalysis__ANA"
                  sections={sections}
                  showCompleteIcon
                  onPress={id => {
                    let selectedProfile = "";
                    if (na[id].owner && na[id].owner.isActive) {
                      selectedProfile = "owner";

                      if (na[id].owner.init === true) {
                        const naData = _.cloneDeep(na);
                        naData[id].owner.init = false;
                        updateNA({
                          naData,
                          pdaData: pda,
                          profileData: profile,
                          dependantProfilesData: dependantProfiles,
                          feData: fe,
                          resetProdType: false
                        });
                      }
                    } else if (na[id].spouse && na[id].spouse.isActive) {
                      selectedProfile = "spouse";

                      if (na[id].owner.init === true) {
                        const naData = _.cloneDeep(na);
                        naData[id].spouse.init = false;
                        updateNA({
                          naData,
                          pdaData: pda,
                          profileData: profile,
                          dependantProfilesData: dependantProfiles,
                          feData: fe,
                          resetProdType: false
                        });
                      }
                    } else {
                      selectedProfile = na[id].dependants[0].cid;

                      if (
                        na[id].dependants[0] &&
                        na[id].dependants[0].init === true
                      ) {
                        const naData = _.cloneDeep(na);
                        naData[id].dependants[0].init = false;
                        updateNA({
                          naData,
                          pdaData: pda,
                          profileData: profile,
                          dependantProfilesData: dependantProfiles,
                          feData: fe,
                          resetProdType: false
                        });
                      }
                    }

                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.setState({
                          selectedProduct: id,
                          selectedProfile
                        });
                      }
                    });
                  }}
                />
              </ScrollView>

              <View style={styles.mainContent}>
                <View style={styles.analysisDependantButtonView}>
                  {DependantProfiles}
                </View>
                <View style={styles.keyboardView}>{ProductContent}</View>
              </View>
            </View>
          );
        }}
      </ContextConsumer>
    );
  }
}

Analysis.propTypes = {
  updateNA: PropTypes.func.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].isRequired,
  // TODO
  isNaError: PropTypes.shape({}).isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  initCompletedStep: PropTypes.func.isRequired,
  isChanged: PropTypes.bool.isRequired,
  saveNA: PropTypes.func.isRequired,
  removeAsset: PropTypes.func.isRequired,
  assetsValueOnChange: PropTypes.func.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired
};
