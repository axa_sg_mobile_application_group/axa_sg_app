import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  container: {
    width: Theme.boxMaxWidth,
    height: "100%"
  },
  wrapper: { flexDirection: "row" },
  statement: {
    ...Theme.tagLine1Primary,
    paddingTop: 20,
    paddingBottom: 20
  },
  prodTypeWrap: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between"
  },
  floatingCard: {
    marginBottom: 10,
    marginTop: 10,
    height: 155,
    width: 310,
    borderRadius: 1,
    borderColor: "red"
  },
  floatingCardContent: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  floatingCardImage: {
    width: 75,
    height: 75,
    borderRadius: 37.5
  },
  floatingCardText: {
    marginTop: 18,
    fontSize: 18
  }
});
