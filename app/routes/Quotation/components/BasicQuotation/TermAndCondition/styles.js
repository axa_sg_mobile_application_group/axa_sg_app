import { StyleSheet } from "react-native";
import Theme from "../../../../../theme";

export default StyleSheet.create({
  dialogToolBar: {
    flexDirection: "row",
    flexBasis: "auto",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: Theme.alignmentXL,
    paddingBottom: Theme.alignmentL,
    backgroundColor: Theme.white,
    borderBottomWidth: 1,
    borderColor: Theme.lightGrey,
    height: 64,
    width: "100%"
  },
  wrapper: {
    flexDirection: "row",
    width: "100%"
  },
  checkBox: {
    flex: 2
  },
  checkBoxLabel: {
    maxWidth: 900
  },
  viewTermAndConditionButton: {
    flex: 1
  }
});
