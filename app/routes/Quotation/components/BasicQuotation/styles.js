import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const clientInfo = {
  flex: 1,
  alignItems: "flex-start",
  marginRight: Theme.alignmentXL,
  maxWidth: "50%"
};

const toolBarSectionA = {
  flex: 1,
  alignItems: "flex-start"
};

const paymentMethod = {
  justifyContent: "center",
  marginLeft: Theme.alignmentXL,
  alignItems: "flex-end"
};

const paymentModeChip = {
  marginLeft: Theme.alignmentL
};

const fundDialogToolBarRow = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  marginTop: Theme.alignmentL
};

const configWrapper = {
  flexDirection: "row",
  justifyContent: "space-between",
  flexWrap: "wrap",
  width: "100%"
};

const configField = {
  marginTop: Theme.alignmentL,
  width: (Theme.boxMaxWidth - Theme.alignmentXL) / 2
};

export default StyleSheet.create({
  /**
   *  scroll view
   * */
  scrollView: {
    width: "100%",
    height: "100%"
  },
  contentContainerStyle: {
    paddingTop: Theme.alignmentXL + 50,
    paddingBottom: Theme.alignmentXL + 50,
    alignItems: "center"
  },
  container: {
    width: Theme.boxMaxWidth,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  /**
   * client info section
   * */
  clientInfoWrapper: {
    flexDirection: "row",
    width: "100%"
  },
  clientInfo,
  clientInfoFirst: {
    ...clientInfo,
    marginRight: 0
  },
  role: {
    paddingBottom: 1,
    borderBottomWidth: 1,
    borderColor: Theme.brand
  },
  /**
   * config section
   * */
  configWrapper,
  checkBoxField: {
    justifyContent: "center",
    paddingHorizontal: Theme.alignmentL,
    height: 48
  },
  configField,
  configFieldFirst: {
    ...configField,
    marginTop: 0
  },
  configFieldWithHeight: {
    marginTop: Theme.alignmentL,
    width: (Theme.boxMaxWidth - Theme.alignmentXL) / 2,
    height: 48
  },
  configFieldFullWidth: {
    marginTop: Theme.alignmentL,
    width: Theme.boxMaxWidth
  },
  warningFullBoxWidth: {
    marginTop: Theme.alignmentL,
    width: Theme.boxMaxWidth
  },
  warningText: {
    ...Theme.subheadSecondary,
    color: Theme.negative
  },
  fundTitleWrapper: {
    marginBottom: Theme.alignmentXL,
    paddingBottom: 1,
    borderBottomWidth: 1,
    borderColor: Theme.brand
  },
  riskProfileInfo: {
    marginBottom: Theme.alignmentXL
  },
  fundOptionWrapper: {
    ...configWrapper,
    marginBottom: Theme.alignmentXL
  },
  textFieldWrapper: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textFieldTitle: {
    fontSize: 16.5
  },
  textFieldValue: {
    fontSize: Theme.fontSizeXM,
    color: Theme.grey
  },

  inputFieldTitle: {
    fontSize: 16.5
  },
  /**
   * table section
   * */
  tableWrapper: {
    alignItems: "flex-start",
    marginTop: Theme.alignmentL,
    width: "100%"
  },
  paymentMode: {
    flexDirection: "row",
    paddingVertical: Theme.alignmentL,
    alignItems: "center"
  },
  paymentModeTitle: {
    ...Theme.captionBrand,
    marginRight: Theme.alignmentL
  },
  paymentModeChip,
  paymentModeChipFirst: {
    ...paymentModeChip,
    marginLeft: 0
  },
  table: {
    marginBottom: Theme.alignmentL
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  basicPlanHeader: {
    backgroundColor: Theme.brandSuperTransparent
  },
  tableRow: {
    backgroundColor: Theme.white
  },
  tableRow2: {
    backgroundColor: Theme.darkGrey
  },
  tableHint: {
    flex: 1,
    alignItems: "center"
  },
  buttonRow: {
    flexDirection: "row"
  },
  padRightButton: {
    marginRight: 10
  },
  adjustedWarning: {
    marginTop: 10
  },
  riskNote: {
    ...Theme.fieldTextOrHelperTextNormal,
    marginTop: Theme.alignmentXXS
  },
  /**
   * fund section
   * */
  fundWrapper: {
    alignItems: "flex-start",
    marginTop: Theme.alignmentL,
    width: "100%"
  },
  fundDetail: {
    marginBottom: Theme.alignmentXL,
    width: "100%"
  },
  fundTable: {
    flexBasis: "auto",
    flex: 0
  },
  fundTableBackground: {
    backgroundColor: Theme.white
  },
  /**
   * dialog
   * */
  toolBarSectionA,
  toolBarSectionB: {
    ...toolBarSectionA,
    alignItems: "center"
  },
  toolBarSectionC: {
    ...toolBarSectionA,
    alignItems: "flex-end"
  },
  fundPlaceholder: {
    flex: 1,
    marginRight: Theme.alignmentXL
  },
  /**
   * select funds dialog
   * */
  fundHeader: {
    ...Theme.dialogHeader,
    flexDirection: "column",
    justifyContent: "flex-end",
    height: 156
  },
  fundDialogToolBarRow,
  fundSearchOptionsRow: {
    ...fundDialogToolBarRow,
    marginTop: Theme.alignmentXS
  },
  fundDialogToolBarRowFirst: {
    ...fundDialogToolBarRow,
    marginTop: 0
  },
  fundDialogHeader: {
    flexBasis: "auto",
    flex: 0
  },
  fundDialogScrollView: {
    flex: 1,
    width: "100%"
  },
  fundDialogFooter: {
    flexBasis: "auto",
    flex: 0
  },
  footerColumn: {
    flex: 1,
    alignItems: "flex-end"
  },
  fundDialogFieldCheckBox: {
    flexBasis: "auto",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingVertical: Theme.alignmentM - 3,
    paddingHorizontal: 0,
    width: 50
  },
  fundDialogFieldFundName: {
    flex: 2,
    padding: 0,
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: 0,
    paddingRight: Theme.alignmentXXS,
    paddingLeft: 0
  },
  fundDialogCurrencyField: {
    flex: 1.3,
    padding: 0,
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentXXS
  },
  fundDialogField: {
    flex: 2,
    padding: 0,
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentXXS
  },
  fundDialogPercentageField: {
    flex: 1,
    padding: 0,
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentXXS
  },
  checkBoxWrapper: {
    justifyContent: "flex-start"
  },
  checkBox: {
    paddingVertical: 0,
    alignItems: "center"
  },
  disclaimerDialogToolBar: {
    flexDirection: "row",
    flexBasis: "auto",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: Theme.alignmentXL,
    paddingBottom: Theme.alignmentL,
    backgroundColor: Theme.white,
    borderBottomWidth: 1,
    borderColor: Theme.lightGrey,
    height: 64,
    width: "100%"
  },
  disclaimerDialogFooter: {
    ...Theme.bodySecondary,
    padding: Theme.alignmentXL
  },
  remarks: {
    marginBottom: Theme.alignmentL,
    width: Theme.boxMaxWidth
  },
  /**
   * bottom sheet
   * */
  bottomSheetContainer: {
    left: 0,
    right: 0,
    bottom: Theme.alignmentXXL,
    width: "100%"
  },
  bottomSheetWrapper: {
    paddingHorizontal: 72,
    backgroundColor: Theme.white,
    width: "100%",
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  bottomSheetBar: {
    flex: 0,
    flexBasis: "auto",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  quotationDetailWrapper: {
    flexDirection: "row"
  },
  paymentMethod,
  paymentMethodFirst: {
    ...paymentMethod,
    marginLeft: 0
  },
  paymentMethodTitleWrapper: {
    paddingBottom: 1,
    borderBottomWidth: 2,
    borderColor: Theme.brand
  },
  breakdownHeaderWrapper: {
    flexBasis: "auto",
    flex: 0
  },
  breakdownContentWrapper: {
    flex: 1,
    width: "100%"
  },
  noticeMessageRow: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  noticeMessage: {
    marginLeft: Theme.alignmentL
  },
  planHeader: {
    flex: 1,
    paddingHorizontal: Theme.alignmentM
  },
  covNameHeader: {
    flex: 1.4,
    paddingHorizontal: Theme.alignmentXXS,
    paddingLeft: Theme.alignmentM
  },
  compactNameHeader: {
    flex: 2.5,
    paddingHorizontal: Theme.alignmentXXS,
    paddingLeft: Theme.alignmentM
  },
  baaCovNameHeader: {
    flex: 1.2,
    paddingHorizontal: Theme.alignmentXXS,
    paddingLeft: Theme.alignmentM
  },
  polTermHeader: {
    flex: 0.7,
    paddingHorizontal: Theme.alignmentXXS
  },
  lmpPolTermHeader: {
    flex: 1,
    paddingHorizontal: Theme.alignmentXXS
  },
  espPolTermHeader: {
    flex: 1,
    paddingHorizontal: Theme.alignmentXXS
  },
  paymentTermHeader: {
    flex: 0.9,
    paddingHorizontal: Theme.alignmentXXS
  },
  specialPaymentTermHeader: {
    flex: 0.72,
    paddingHorizontal: Theme.alignmentXXS
  },
  sumAssuredHeader: {
    flex: 1.3,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  specialSumAssuredHeader: {
    flex: 1.0,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  rightAlign: {
    textAlign: "right"
  },
  othSaHeader: {
    flex: 1.5,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  specialOthSaHeader: {
    flex: 0.9,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  classHeader: {
    flex: 0.6,
    alignItems: "center",
    paddingHorizontal: Theme.alignmentXXS
  },
  specialClassHeader: {
    flex: 0.7,
    alignItems: "center",
    paddingHorizontal: Theme.alignmentXXS
  },
  premiumHeader: {
    flex: 1.1,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  compactPremiumHeader: {
    flex: 0.7,
    alignItems: "flex-end",
    paddingHorizontal: Theme.alignmentXXS
  },
  cancelButton: {
    paddingHorizontal: Theme.alignmentM,
    paddingLeft: Theme.alignmentXXS,
    width: 24 + Theme.alignmentXXS + Theme.alignmentM
  },
  sumAssuredTableColumn: {
    width: "100%"
  },
  breakDownTableConfigCovName: {
    flex: 1
  },
  breakDownTableConfigSumInsured: {
    flex: 1,
    alignItems: "flex-end"
  },
  breakDownTableConfigMonthPrem: {
    flex: 1,
    alignItems: "flex-end"
  },
  breakDownTableConfigQuarterPrem: {
    flex: 1,
    alignItems: "flex-end"
  },
  breakDownTableConfigHalfYearPrem: {
    flex: 1,
    alignItems: "flex-end"
  },
  breakDownTableConfigYearPrem: {
    flex: 1,
    alignItems: "flex-end"
  },
  warningContainer: {
    flex: 1,
    justifyContent: "center",
    padding: 65,
    backgroundColor: Theme.grey
  },
  warningInnerContainer: {
    backgroundColor: Theme.white,
    padding: 30
  },
  warningTitle: {
    fontSize: Theme.fontSizeXXXM
  },
  warningTitleBold: {
    ...Theme.headlinePrimary,
    fontSize: Theme.fontSizeXXXM
  },
  warningPaddingBottom: {
    marginBottom: 20
  },
  warningBtnRowPadRight: {
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  warningContent: {
    fontSize: Theme.fontSizeXM
  },
  warningContentBold: {
    fontSize: Theme.fontSizeXM,
    fontWeight: "bold"
  }
});
