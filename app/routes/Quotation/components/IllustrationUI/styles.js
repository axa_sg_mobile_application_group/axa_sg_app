import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  ageText: {
    fontSize: Theme.fontSizeXM,
    color: Theme.grey
  },
  ageValue: {
    fontSize: 48,
    fontWeight: "bold"
  },
  ageXaxis: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    paddingBottom: 100
  },
  ageYaxis: {
    fontSize: 14,
    fontWeight: "bold"
  },
  chartXaxis: {
    width: 880,
    marginVertical: Theme.alignmentXL
  },
  chartYaxis: {
    alignItems: "flex-start",
    width: 880
  },
  chartValue: {
    fontSize: 28,
    fontWeight: "bold",
    textAlign: "right"
  },
  chartText: {
    fontSize: 12,
    textAlign: "right"
  },
  PremiumPaidChartText: {
    fontSize: 12,
    textAlign: "right",
    color: Theme.brand
  },
  DeathBenefitChartText: {
    fontSize: 12,
    textAlign: "right",
    color: Theme.positive
  },
  SurrenderValueChartText: {
    fontSize: 12,
    textAlign: "right",
    color: Theme.accent
  },
  rootView: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  topSection: {
    flexDirection: "row",
    width: 880,
    marginVertical: Theme.alignmentXL
  },
  textContainer: {
    flex: 1
  },
  graphContainer: {
    width: 913,
    height: 300
  },
  playhead: {
    width: 16,
    height: 200,
    backgroundColor: Theme.paleGrey,
    bottom: 0,
    position: "absolute"
  },
  animatedViewContainer: {
    position: "absolute",
    height: "100%"
  },
  ageDataArray: {
    backgroundColor: Theme.paleGrey,
    marginTop: -26,
    marginRight: 36,
    width: 50,
    height: 20,
    alignItems: "center"
  },
  ageDataArrayNotice: {
    backgroundColor: Theme.paleGrey,
    justifyContent: "center",
    marginTop: -16,
    right: 16,
    width: 50,
    height: 20
  },
  ageDataArrayText: {
    bottom: -5,
    fontSize: Theme.fontSizeXXXM
  },
  headerTableContainer: {
    marginTop: Theme.alignmentXXXL,
    justifyContent: "center",
    width: 880,
    flex: 0
  },
  firstHeaderItem: {
    flex: 1,
    alignItems: "flex-start"
  },
  headerItem: {
    flex: 1,
    alignItems: "flex-end"
  },
  columnContainer: {
    paddingVertical: 0,
    paddingHorizontal: 0
  },
  rowItemFirst: {
    alignItems: "flex-start",
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentL,
    width: "100%"
  },
  rowItem: {
    alignItems: "flex-end",
    paddingVertical: Theme.alignmentM,
    paddingHorizontal: Theme.alignmentL,
    width: "100%"
  },
  dataTableContainer: {
    width: 880
  },
  tableHeaderItem: {}
});
