import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  /* app bar */
  appBar: {
    flexBasis: "auto",
    paddingHorizontal: Theme.alignmentXL,
    paddingTop: Theme.statusBarHeight,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: Theme.white,
    height: 64,
    width: "100%",
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleWrapper: {
    flex: 1,
    alignItems: "center"
  },
  buttonWrapperLeft: {
    flex: 1,
    alignItems: "flex-start"
  },
  buttonWrapperRight: {
    flex: 1,
    alignItems: "flex-end"
  }
});
