import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES as WEB_WEB_API_REDUCER_TYPES,
  utilities
} from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView, Text, View, Alert, Image } from "react-native";
import icRemove from "../../../../assets/images/icRemove.png";
import EABButton from "../../../../components/EABButton";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import EABCard from "../../../../components/EABCard";
import EABDialog from "../../../../components/EABDialog/EABDialog.ios";
import EABSectionList from "../../../../components/EABSectionList";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import EABTable from "../../../../components/EABTable";
import EABTableFooter from "../../../../components/EABTableFooter";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableSectionHeader from "../../../../components/EABTableSectionHeader";
import EABTextField from "../../../../components/EABTextField";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import Selector from "../../../../components/Selector";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import * as DIALOG_TYPES from "../../../../components/EABDialog/constants";
import REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import * as REDUCER_TYPES from "../../../../constants/REDUCER_TYPES";
import ClientFormContact from "../../../../containers/ClientFormContact";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import RiderDialog from "../../containers/RiderDialog";
import clientInfoObject from "../../utilities/clientInfoObject";
import {
  removeDisabledRiders,
  removeInclusiveRiders
} from "../../utilities/riderHandler";
import ProfileUI from "../ProfileUI";
import styles from "./styles";

const { TEXT_STORE } = REDUCER_TYPES;
const { CLIENT, OPTIONS_MAP, QUOTATION } = WEB_WEB_API_REDUCER_TYPES;
const { numberToCurrency } = utilities.numberFormatter;
const { getCurrencySign } = utilities.common;
const { isClientFormHasError } = utilities.validation;

/**
 * <ShieldQuotation />
 * @description shield quotation component. All processing is in this component.
 * @param {function} selectInsured - select insured quotation
 * */
export default class ShieldQuotation extends Component {
  static getAgeNextBirthday(dob) {
    const dobDate = new Date(dob);
    const targetDate = new Date();
    const tYr = targetDate.getFullYear();
    const tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

    const dobYr = dobDate.getFullYear();
    const dobMonth = dobDate.getMonth();
    const dobDay = dobDate.getDate();

    const tyBirthday = new Date(tYr, dobMonth, dobDay);
    const nextDob =
      tDate - tyBirthday < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

    return nextDob.getFullYear() - dobYr;
  }

  constructor(props) {
    super(props);

    this.getIsProposer = this.getIsProposer.bind(this);
    this.getInsuredData = this.getInsuredData.bind(this);
    this.getQuotationData = this.getQuotationData.bind(this);
    this.sign = this.sign.bind(this);
    this.prepareSectionListOptions = this.prepareSectionListOptions.bind(this);

    this.state = {
      selectingRiderInsuredID: "",
      shouldOpenClientForm: false,
      showProfileTab: true,
      isLoading: false,
      selectedCid:
        props.hasErrorList.length > 0 ? props.hasErrorList[0].cid : ""
    };
  }

  // ===========================================================================
  // getter
  // ===========================================================================
  get insuredsList() {
    const { availableInsureds } = this.props;
    const availableInsuredsList = [];
    const eligibleInsuredsList = [];

    availableInsureds.forEach(insured => {
      const { valid, eligible } = insured;

      if (!valid) {
        availableInsuredsList.push({
          insured,
          isIncompleteProfile: true
        });
      } else if (!eligible) {
        eligibleInsuredsList.push(insured);
      } else {
        availableInsuredsList.push({
          insured,
          isIncompleteProfile: false
        });
      }
    });

    return {
      availableInsuredsList,
      eligibleInsuredsList
    };
  }

  get isFaChannel() {
    const { agentChannel } = this.props;
    return agentChannel === "FA";
  }

  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description convert backend data to usable UI data. this function is use
   *   to the main row section
   * @return {object} usable insured data of main row
   * */
  getIsProposer(cid) {
    const { profile } = this.props;
    return cid === profile.cid;
  }

  getInsuredData({ insured, language }) {
    const { optionsMap, textStore, quotation } = this.props;
    const { profile, availableClasses } = insured;
    const { cid } = profile;
    const photoWidthAndHeight = 40;

    // get planOptions
    const planOptions = [
      {
        key: "empty",
        label: "-",
        value: null
      }
    ];
    if (availableClasses) {
      availableClasses.forEach(classItem => {
        planOptions.push({
          key: classItem.covClass,
          label: getEABTextByLangType({ data: classItem.className, language }),
          value: classItem.covClass
        });
      });
    }

    return {
      insuredInfo: clientInfoObject({
        key: cid,
        language,
        textStore,
        optionsMap,
        gender: profile.gender,
        age: ShieldQuotation.getAgeNextBirthday(profile.dob),
        smoke: profile.isSmoker,
        occupation: profile.occupation,
        residence: profile.residenceCountry,
        fullName: profile.fullName,
        photo: profile.photo
      }),
      photoWidthAndHeight,
      planOptions,
      subQuotation: (quotation.insureds && quotation.insureds[cid]) || false
    };
  }
  /**
   * @description convert backend data to usable UI data. this function is use
   *   to the quotation section
   * @requires sign
   * */
  getQuotationData({
    insured,
    language,
    photoWidthAndHeight,
    subQuotation,
    riderAlert
  }) {
    const { sign } = this;
    const {
      inputConfigs,
      planDetails,
      textStore,
      selectRider,
      quotation,
      quote,
      optionsMap,
      agent
    } = this.props;
    const { cid } = insured.profile;

    // get insuredOptions
    const {
      medishieldLife,
      axaShield,
      awl,
      cashPortion
    } = subQuotation.policyOptions;
    let ccyTitle = "";

    // get ccy title
    let currencyOptions = [];
    optionsMap.ccy.currencies.forEach(currencies => {
      if (currencies.compCode === agent.compCode) {
        currencyOptions = currencies.options;
      }
    });
    currencyOptions.forEach(option => {
      if (option.value === subQuotation.ccy) {
        ccyTitle = getEABTextByLangType({ data: option.title, language });
      }
    });

    const insuredOptions = [
      {
        key: "currency",
        titlePath: "quotation.shield.currency",
        value: ccyTitle
      },
      {
        key: "medishield life",
        titlePath: "quotation.shield.mediShieldLife",
        value: `${sign(subQuotation)}${numberToCurrency({
          value: medishieldLife,
          decimal: 2
        })}`
      },
      {
        key: "axa shield",
        titlePath: "quotation.shield.axaShield",
        value: `${sign(subQuotation)}${numberToCurrency({
          value: axaShield,
          decimal: 2
        })}`
      },
      {
        key: "awl",
        titlePath: "quotation.shield.awl",
        value: `${sign(subQuotation)}${numberToCurrency({
          value: awl,
          decimal: 2
        })}`
      },
      {
        key: "cash portion",
        titlePath: "quotation.shield.cashPortion",
        value: `${sign(subQuotation)}${numberToCurrency({
          value: cashPortion,
          decimal: 2
        })}`
      }
    ];

    // get table config
    const tableConfig = [
      {
        key: "check box",
        style: [
          styles.tableColumnA,
          { width: photoWidthAndHeight + Theme.alignmentL * 2 }
        ]
      },
      {
        key: "plan name",
        style: styles.tableColumnB
      },
      {
        key: "payment method",
        style: styles.tableColumnC
      },
      {
        key: "payment frequency",
        style: styles.tableColumnC
      },
      {
        key: "premium",
        style: styles.tableColumnD
      },
      {
        key: "canncelButton",
        style: styles.cancelButton
      }
    ];
    const tableConfigHeader = [
      {
        key: "check box",
        style: [
          styles.tableColumnA,
          { width: photoWidthAndHeight + Theme.alignmentL * 2 }
        ]
      },
      {
        key: "plan name",
        style: styles.tableHeaderColumn
      }
    ];
    const tableConfigFooter = [
      {
        key: "blank",
        style: [
          styles.tableColumnA,
          { width: photoWidthAndHeight + Theme.alignmentL * 2 }
        ]
      },
      {
        key: "blank1",
        style: styles.tableColumnC
      },
      {
        key: "blank2",
        style: styles.tableColumnD
      },
      {
        key: "totalLabel",
        style: styles.tableFooterColumnA
      },
      {
        key: "premium",
        style: styles.tableFooterColumnB
      },
      {
        key: "blank3",
        style: styles.cancelButton
      }
    ];

    // get basic plan
    const subInputConfig = inputConfigs[cid];
    const basicProductCode = quotation.baseProductCode;
    const basicPlan = subQuotation.plans[0];
    const basicPlanInputConfig = subInputConfig[basicPlan.covCode];
    const basicPlanDetail = planDetails[basicPlan.covCode];
    const basicPlanData = {
      premPostfix: !_.isEmpty(basicPlanDetail.premPostfix)
        ? getEABTextByLangType({
            data: basicPlanDetail.premPostfix,
            language
          })
        : "",
      covName: getEABTextByLangType({
        data: basicPlan.covName,
        language
      }),
      paymentMethod: getEABTextByLangType({
        data: basicPlanInputConfig.paymentMethodList[0].title,
        language
      }),
      payFreq: getEABTextByLangType({
        data: basicPlanInputConfig.payFreqList[0].title,
        language
      }),
      premium: `${sign(subQuotation)}${numberToCurrency({
        value: basicPlan.premium
      })}`
    };

    // get riders
    const riderRows = [];
    subQuotation.plans.forEach((plan, index) => {
      if (plan.covCode !== basicProductCode && subInputConfig) {
        const inputConfig = subInputConfig[plan.covCode];
        const planDetail = planDetails[plan.covCode] || {};
        const { riderList } = subInputConfig[basicProductCode];

        riderRows.push({
          key: plan.covCode,
          covName: getEABTextByLangType({ data: plan.covName, language }),
          paymentMethod: plan.paymentMethod,
          paymentMethodOnChange: (key, option) => {
            const newPlans = _.cloneDeep(subQuotation.plans);
            newPlans[index].paymentMethod = option.value;

            quote({
              ...quotation,
              insureds: {
                ...quotation.insureds,
                [cid]: {
                  ...quotation.insureds[cid],
                  plans: newPlans
                }
              }
            });
          },
          paymentMethodOptions: inputConfig
            ? inputConfig.paymentMethodList.map(item => ({
                key: item.value.toString(),
                label: getEABTextByLangType({ data: item.title, language }),
                value: item.value
              }))
            : [],
          isPaymentMethodDisabled:
            !inputConfig || !inputConfig.canEditPaymentMethod,
          payFreq: plan.payFreq,
          payFreqOnChange: (key, option) => {
            const newPlans = _.cloneDeep(subQuotation.plans);
            newPlans[index].payFreq = option.value;

            quote({
              ...quotation,
              insureds: {
                ...quotation.insureds,
                [cid]: {
                  ...quotation.insureds[cid],
                  plans: newPlans
                }
              }
            });
          },
          payFreqOptions: inputConfig
            ? inputConfig.payFreqList.map(item => ({
                key: item.value.toString(),
                label: getEABTextByLangType({ data: item.title, language }),
                value: item.value
              }))
            : [],
          isPayFreqDisabled: !inputConfig || !inputConfig.canEditPayFreq,
          premium: numberToCurrency({
            value: plan.premium
          }),
          premiumMaxLength: 10,
          isPremiumDisabled: !inputConfig || !inputConfig.canEditPremium,
          removeButtonOnPress: () => {
            Alert.alert(
              EABi18n({
                textStore,
                language,
                path: "alert.confirm"
              }),
              EABi18n({
                textStore,
                language,
                path: "quotation.warning.removeRider"
              }),
              [
                {
                  text: EABi18n({
                    language,
                    textStore,
                    path: "button.no"
                  }),
                  onPress: () => {}
                },
                {
                  text: EABi18n({
                    language,
                    textStore,
                    path: "button.yes"
                  }),
                  onPress: () => {
                    let selectedCovCodes = [];
                    _.each(subQuotation.plans, p => {
                      if (p.covCode !== planDetail.covCode) {
                        selectedCovCodes.push(p.covCode);
                      }
                    });
                    selectedCovCodes = removeInclusiveRiders({
                      covCode: plan.covCode,
                      selectedCovCodes,
                      quotation: subQuotation,
                      bpDetail: basicPlanDetail
                    });
                    selectedCovCodes = removeDisabledRiders({
                      selectedCovCodes,
                      subQuotation,
                      bpDetail: basicPlanDetail,
                      riderList
                    });
                    selectRider({
                      covCodes: selectedCovCodes,
                      cid,
                      alert: errorMessage => {
                        riderAlert({ errorMessage, language });
                      }
                    });
                  }
                }
              ]
            );
          }
        });
      }
    });

    return {
      tableConfig,
      tableConfigHeader,
      tableConfigFooter,
      basicPlanData,
      riderHeaderTitlePath: "quotation.table.riders",
      insuredOptions,
      riderRows
    };
  }
  /**
   * @description get the client profile
   * @param {object} cid - insured clientId
   * @return {string} insured client profile
   * */

  getProfile(cid) {
    const { profile, dependantProfiles } = this.props;
    let loadProfile = {};
    if (profile.cid === cid) {
      loadProfile = profile;
    } else {
      loadProfile = dependantProfiles[cid];
    }
    return loadProfile;
  }
  /**
   * @description get the currency sign
   * @param {object} subQuotation - insured quotation data
   * @return {string} sign
   * */

  sign(subQuotation) {
    const { optionsMap } = this.props;

    return getCurrencySign({
      compCode: subQuotation.compCode,
      ccy: subQuotation.ccy,
      optionsMap
    });
  }

  prepareSectionListOptions() {
    const { hasErrorList } = this.props;
    const { selectedCid } = this.state;

    const profileOptions = _.map(hasErrorList, insuredHasError => ({
      key: insuredHasError.cid,
      id: insuredHasError.cid,
      title: _.get(this.getProfile(insuredHasError.cid), "fullName", ""),
      selected: selectedCid === insuredHasError.cid,
      completed: !insuredHasError.hasError
    }));

    return [
      {
        key: "section",
        data: profileOptions,
        title: ""
      }
    ];
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { getInsuredData, getQuotationData, insuredsList, sign } = this;
    const {
      quotation,
      profile,
      optionsMap,
      textStore,
      planDetails,
      inputConfigs,
      selectInsured,
      selectRider,
      clientForm,
      saveClient,
      updateClient,
      loadInsured,
      updateInsured,
      dependantProfiles,
      checkInsuredList,
      cleanClientForm,
      removeCompleteProfile
    } = this.props;
    const {
      shouldOpenClientForm,
      selectingRiderInsuredID,
      showProfileTab,
      isLoading,
      selectedCid
    } = this.state;
    const { availableInsuredsList, eligibleInsuredsList } = insuredsList;
    const segmentedControlArray = [
      {
        key: "A",
        title: "Profile",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: true });
        }
      },
      {
        key: "B",
        title: "Contact",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: false });
        }
      }
    ];
    const isProposer = this.getIsProposer(selectedCid);
    const { pCid } = quotation;
    const InsuredArray = Object.keys(dependantProfiles);
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * @description use for alert rider error
     * @param {object} errorMessage
     * @param {string} language
     * */
    const riderAlert = ({ errorMessage, language }) => {
      Alert.alert(
        EABi18n({
          textStore,
          language,
          path: "quotation.error.title"
        }),
        errorMessage,
        [
          {
            text: EABi18n({
              language,
              textStore,
              path: "button.ok"
            }),
            onPress: () => {
              this.setState({ selectingRiderInsuredID: "" });
            }
          }
        ]
      );
    };
    /**
     * @description render quotation table
     * @requires getQuotationData
     * @requires riderAlert
     * @param {object} insured - insured data
     * @param {number} photoWidthAndHeight - use to alignment
     * @param {string} language
     * @param {object} subQuotation - quotation data for this insured
     * @return {array} array of insured quotation section
     * */
    const renderQuotationTable = ({
      insured,
      language,
      photoWidthAndHeight,
      subQuotation
    }) => {
      const { cid, fullName } = insured.profile;
      const {
        tableConfig,
        tableConfigHeader,
        tableConfigFooter,
        basicPlanData,
        riderHeaderTitlePath,
        insuredOptions,
        riderRows
      } = getQuotationData({
        insured,
        language,
        photoWidthAndHeight,
        subQuotation,
        riderAlert
      });
      const premiumPrefix = sign(subQuotation);
      const { riderList } = inputConfigs[cid][subQuotation.baseProductCode];

      return [
        <View
          key="options"
          style={[
            styles.optionRow,
            { paddingLeft: photoWidthAndHeight + Theme.alignmentL * 2 }
          ]}
        >
          {insuredOptions.map((option, index) => (
            <View
              key={option.key}
              style={
                index === 0 ? styles.optionWrapperFirst : styles.optionWrapper
              }
            >
              <View style={styles.optionTitle}>
                <Text style={Theme.captionBrand}>
                  {EABi18n({
                    textStore,
                    language,
                    path: option.titlePath
                  }).toUpperCase()}
                </Text>
              </View>
              <Text style={Theme.bodyPrimary}>{option.value}</Text>
            </View>
          ))}
        </View>,
        <EABTable
          key="table"
          style={styles.tableWrapper}
          tableConfig={tableConfig}
        >
          <EABTableHeader key="table header">
            <View>{/* for image padding */}</View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                path="quotation.table.name"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                path="quotation.table.paymentMethod"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                path="quotation.table.paymentFrequency"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                path="quotation.table.premium"
              />
            </View>
            <View>{/* for cancel button space */}</View>
          </EABTableHeader>
          <EABTableSectionHeader
            key="basic plan header"
            tableConfig={tableConfigHeader}
          >
            <View />
            <View>
              <TranslatedText
                style={Theme.captionBrand}
                path="quotation.table.basicPlan"
                funcOnText={text => text.toUpperCase()}
              />
            </View>
          </EABTableSectionHeader>
          <EABTableSection key="basic plan row">
            <View>{/* for image padding */}</View>
            <View>
              <Text style={Theme.subheadPrimary}>{basicPlanData.covName}</Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>
                {basicPlanData.paymentMethod}
              </Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>{basicPlanData.payFreq}</Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>{basicPlanData.premium}</Text>
              {basicPlanData.premPostfix !== "" ? (
                <Text style={Theme.tableColumnLabel}>
                  {basicPlanData.premPostfix}
                </Text>
              ) : null}
            </View>
            <View>
              <EABButton
                testID={`Quotation__Shield__btnRemove__${fullName}`}
                onPress={() => {
                  Alert.alert(
                    EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.confirm"
                    }),
                    EABi18n({
                      textStore,
                      language,
                      path: "quotation.warning.confirmClearLA"
                    }),
                    [
                      {
                        text: EABi18n({
                          language,
                          textStore,
                          path: "button.no"
                        }),
                        onPress: () => {}
                      },
                      {
                        text: EABi18n({
                          language,
                          textStore,
                          path: "button.yes"
                        }),
                        onPress: () => {
                          selectInsured({ cid, covClass: null });
                        }
                      }
                    ]
                  );
                }}
              >
                <Image source={icRemove} />
              </EABButton>
            </View>
          </EABTableSection>
          {riderRows.length !== 0
            ? [
                <EABTableSectionHeader
                  key="rider header"
                  tableConfig={tableConfigHeader}
                >
                  <View />
                  <View>
                    <TranslatedText
                      style={Theme.captionBrand}
                      path={riderHeaderTitlePath}
                      funcOnText={text => text.toUpperCase()}
                    />
                  </View>
                </EABTableSectionHeader>,
                ...riderRows.map((riderRow, riderIndex) => (
                  <EABTableSection key={riderRow.key}>
                    <View>{/* for image padding */}</View>
                    <View>
                      <Text style={Theme.subheadPrimary}>
                        {riderRow.covName}
                      </Text>
                    </View>
                    <View>
                      <Selector
                        selectorType={FLAT_LIST}
                        displayTextStyle={styles.riderSelector}
                        hasCard={false}
                        value={riderRow.paymentMethod}
                        popoverOptions={{
                          preferredContentSize: [150, 105]
                        }}
                        flatListOptions={{
                          data: riderRow.paymentMethodOptions,
                          renderItemOnPress: riderRow.paymentMethodOnChange,
                          selectedValue: riderRow.paymentMethod
                        }}
                        disable={riderRow.isPaymentMethodDisabled}
                      />
                    </View>
                    <View>
                      <Selector
                        testID={`Quotation__Shield__ddlRiderPayment__${fullName}__${riderIndex}`}
                        selectorType={FLAT_LIST}
                        hasCard={false}
                        displayTextStyle={styles.riderSelector}
                        value={riderRow.payFreq}
                        popoverOptions={{
                          preferredContentSize: [150, 105]
                        }}
                        flatListOptions={{
                          data: riderRow.payFreqOptions,
                          renderItemOnPress: riderRow.payFreqOnChange,
                          selectedValue: riderRow.payFreq
                        }}
                        disable={riderRow.isPayFreqDisabled}
                      />
                    </View>
                    <View>
                      <EABTextField
                        numberOfLine={1}
                        inputStyle={styles.riderPremiumText}
                        supportLabelPlaceholder={false}
                        placeholder={EABi18n({
                          textStore,
                          language,
                          path: "quotation.placeholder.enterPremium"
                        })}
                        supportErrorMessage={false}
                        value={riderRow.premium}
                        prefix={premiumPrefix}
                        maxLength={riderRow.premiumMaxLength}
                        editable={!riderRow.isPremiumDisabled}
                        keyboardType="numeric"
                        onChange={() => {
                          // premium not allow edit
                        }}
                      />
                    </View>
                    <View>
                      <EABButton
                        testID={`Quotation__Shield__btnRemoveRider__${fullName}__${riderIndex}`}
                        onPress={riderRow.removeButtonOnPress}
                      >
                        <Image source={icRemove} />
                      </EABButton>
                    </View>
                  </EABTableSection>
                )),
                <EABTableFooter
                  key="rider total"
                  tableConfig={tableConfigFooter}
                >
                  <View>{/* for image padding */}</View>
                  <View>{/* for image padding */}</View>
                  <View>{/* for image padding */}</View>
                  <View>
                    <TranslatedText
                      style={styles.totalRiderPremiumTitle}
                      path="application.planDetails.shield.totalRiderPremium"
                    />
                  </View>
                  <View>
                    <Text style={styles.totalRiderPremiumContent}>
                      {`${premiumPrefix}${numberToCurrency({
                        value: subQuotation.totRiderPrem,
                        decimal: 2
                      })}`}
                    </Text>
                  </View>
                  <View>{/* for image padding */}</View>
                </EABTableFooter>
              ]
            : null}
        </EABTable>,
        Object.keys(inputConfigs).length > 0 &&
        Object.keys(planDetails).length > 1 ? (
          <View
            key="select rider button"
            style={[
              styles.selectRiderButtonWrapper,
              { paddingLeft: photoWidthAndHeight + Theme.alignmentL * 2 }
            ]}
          >
            <EABButton
              testID={`Quotation__Shield__btnSelectRiders__${fullName}`}
              buttonType={RECTANGLE_BUTTON_1}
              onPress={() => {
                this.setState({
                  selectingRiderInsuredID: cid
                });
              }}
              isDisable={!riderList || riderList.length === 0}
            >
              <TranslatedText path="quotation.button.selectRiders" />
            </EABButton>
          </View>
        ) : null
      ];
    };
    /**
     * @description render insured quotation main row right
     * @param {object} insured - insured data
     * @param {array} planOptions - options of plan selector
     * @param {object} subQuotation - quotation data for this insured
     * @param {boolean} isIncompleteProfile - indicate insured is inComplete
     *   profile or not
     * @param {string} language
     * @return {array} elements array
     * */
    const renderInsuredQuotationRight = ({
      insured,
      planOptions,
      subQuotation,
      isIncompleteProfile,
      language
    }) => {
      if (isIncompleteProfile) {
        return [
          <EABButton
            testID="Quotation__Shield__btnIncompleteProfile"
            key="reason"
            containerStyle={styles.disableReasonButton}
            buttonType={RECTANGLE_BUTTON_2}
            onPress={() => {
              checkInsuredList({ pCid, iCids: InsuredArray });
              loadInsured({
                cid: insured.profile.cid,
                profile: this.getProfile(insured.profile.cid),
                isFNA: !this.isFaChannel && isProposer,
                isProposerMissing: true,
                isInsuredMissing: !isProposer,
                callback: () => {
                  this.setState({
                    shouldOpenClientForm: true,
                    selectedCid: insured.profile.cid
                  });
                }
              });
            }}
          >
            <TranslatedText
              style={Theme.rectangleButtonStyle2Label}
              path="quotation.shield.incompleteProfile"
            />
          </EABButton>,
          <PopoverTooltip
            key="tooltip"
            popoverOptions={{ preferredContentSize: [350, 100] }}
            text={EABi18n({
              textStore,
              language,
              path: "quotation.shield.incompleteProfileHint"
            })}
          />
        ];
      }

      const { cid, fullName } = insured.profile;
      return (
        <Selector
          testID={`Quotation__Shield__ddlPlanSelector__${fullName}`}
          selectorType={FLAT_LIST}
          displayTextStyle={styles.planSelector}
          hasCard
          hasLabel
          label={EABi18n({
            language,
            textStore,
            path: "quotation.shield.basicPlan"
          })}
          flatListOptions={{
            data: planOptions,
            renderItemOnPress: (key, option) => {
              if (quotation.insureds[cid]) {
                Alert.alert(
                  EABi18n({
                    textStore,
                    language,
                    path: "quotation.hint.confirm"
                  }),
                  EABi18n({
                    textStore,
                    language,
                    path: "quotation.warning.confirmClearLA"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.no"
                      }),
                      onPress: () => {}
                    },
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.yes"
                      }),
                      onPress: () => {
                        selectInsured({ cid, covClass: option.value });
                      }
                    }
                  ]
                );
              } else if (Object.keys(quotation.insureds).length > 5) {
                Alert.alert(
                  EABi18n({
                    textStore,
                    language,
                    path: "quotation.error.title"
                  }),
                  EABi18n({
                    textStore,
                    language,
                    path: "quotation.error.maxLA"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.ok"
                      }),
                      onPress: () => {}
                    }
                  ]
                );
              } else {
                selectInsured({ cid, covClass: option.value });
              }
            },
            selectedValue: subQuotation ? subQuotation.plans[0].covClass : null
          }}
        />
      );
    };
    /**
     * @description render proposer info
     * @requires clientInfoObject
     * @param {string} language
     * @return {element} client information section
     * */
    const renderClientInfo = language => {
      const {
        pFullName,
        pGender,
        pAge,
        pResidence,
        pSmoke,
        pOccupation
      } = quotation;

      const info = clientInfoObject({
        key: pCid,
        language,
        textStore,
        optionsMap,
        gender: pGender,
        age: pAge,
        smoke: pSmoke,
        occupation: pOccupation,
        residence: pResidence,
        fullName: pFullName
      });

      return (
        <View style={styles.infoWrapper}>
          <View style={styles.role}>
            <TranslatedText
              style={Theme.captionBrand}
              path="quotation.role_P"
              funcOnText={text => text.toUpperCase()}
            />
          </View>
          <ProfileUI
            style={styles.profile}
            photo={profile.photo}
            name={info.fullName}
            firstDescription={`${info.infoA}, ${info.infoB}`}
          />
        </View>
      );
    };
    /**
     * @description render insured quotation. this function just used to render
     *   UI, all data preparing should be done in the getInsuredData
     * @requires getInsuredData
     * @requires renderInsuredQuotationRight
     * @requires renderQuotationTable
     * @param {object} insuredData - insured data
     * @param {object} insuredData.insured - insured data
     * @param {boolean} insuredData.isIncompleteProfile - indicate insured is
     *   inComplete profile or not
     * @param {string} language
     * @return {element} insured quotation section
     * */
    const renderInsuredQuotation = ({ insuredData, language }) => {
      const { insured, isIncompleteProfile } = insuredData;
      const {
        photoWidthAndHeight,
        insuredInfo,
        planOptions,
        subQuotation
      } = getInsuredData({ insured, language });

      return (
        <EABCard key={insuredInfo.key} style={styles.insuredSection}>
          <View style={styles.insuredMainRow}>
            <ProfileUI
              style={styles.insuredInfo}
              name={insuredInfo.fullName}
              photo={insuredInfo.photo}
              photoWidth={photoWidthAndHeight}
              photoHeight={photoWidthAndHeight}
              firstDescription={`${insuredInfo.infoA}, ${insuredInfo.infoB}`}
            />
            <View style={styles.insuredPlanSelectorWrapper}>
              {renderInsuredQuotationRight({
                insured,
                planOptions,
                subQuotation,
                isIncompleteProfile,
                language
              })}
            </View>
          </View>
          {subQuotation
            ? renderQuotationTable({
                insured,
                language,
                photoWidthAndHeight,
                subQuotation
              })
            : null}
        </EABCard>
      );
    };

    const renderIneligibleInsuredRow = ({ insured, language }) => {
      const { profile: insuredProfile } = insured;
      const { cid } = profile;

      const info = clientInfoObject({
        key: cid,
        language,
        textStore,
        optionsMap,
        gender: insuredProfile.gender,
        age: String(Number(insuredProfile.age) + 1),
        smoke: insuredProfile.isSmoker,
        occupation: insuredProfile.occupation,
        residence: insuredProfile.residenceCountry,
        fullName: insuredProfile.fullName
      });

      return (
        <View>
          <Text style={Theme.bodyBrand}>
            {`${info.fullName}  `}
            <Text style={Theme.bodyPrimary}>{`${info.infoA}, ${
              info.infoB
            }`}</Text>
          </Text>
        </View>
      );
    };

    const { hasError } = isClientFormHasError(clientForm);
    return (
      <ContextConsumer>
        {({ language }) => (
          <ScrollView
            testID="Quotation__Shield__pageScroll"
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
          >
            <View style={styles.container}>{renderClientInfo(language)}</View>
            {availableInsuredsList.length !== 0 ? (
              <View style={styles.container}>
                <View style={styles.insuredsTitleWrapper}>
                  <View style={styles.role}>
                    <TranslatedText
                      style={Theme.captionBrand}
                      path="quotation.shield.lifeAssured"
                      funcOnText={text => text.toUpperCase()}
                    />
                  </View>
                </View>
                {availableInsuredsList.map(insuredData =>
                  renderInsuredQuotation({ insuredData, language })
                )}
              </View>
            ) : null}
            {eligibleInsuredsList.length !== 0 ? (
              <View style={styles.eligibleContainer}>
                <View style={styles.eligibleHint}>
                  <Text style={styles.eligibleHintText}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "quotation.shield.ineligibleHint"
                    })}
                  </Text>
                </View>
                {eligibleInsuredsList.map(insured =>
                  renderIneligibleInsuredRow({ insured, language })
                )}
              </View>
            ) : null}
            <EABDialog type={DIALOG_TYPES.SCREEN} isOpen={shouldOpenClientForm}>
              <View style={Theme.dialogHeader}>
                <View style={Theme.headerRow}>
                  <EABButton
                    testID="Quotation__Shield__btnClientFormCancel"
                    containerStyle={Theme.headerLeft}
                    onPress={() => {
                      this.setState({
                        shouldOpenClientForm: false
                      });
                      cleanClientForm();
                    }}
                  >
                    <Text style={Theme.textButtonLabelNormalAccent}>
                      {EABi18n({
                        textStore,
                        language,
                        path: "button.cancel"
                      })}
                    </Text>
                  </EABButton>
                  <EABSegmentedControl
                    testID="Quotation__Shield__btnClientFormSegmentedControl"
                    segments={segmentedControlArray}
                    activatedKey={showProfileTab ? "A" : "B"}
                  />
                  <EABButton
                    testID="Quotation__Shield__btnClientFormCancel"
                    containerStyle={Theme.headerRight}
                    onPress={() => {
                      this.setState({ isLoading: true }, () => {
                        saveClient(() => {
                          // can not set state at the same time. multiple dialogs
                          // can not close at the same time.
                          this.setState({ isLoading: false }, () => {
                            this.setState({
                              shouldOpenClientForm: false
                            });
                            updateClient();
                            removeCompleteProfile();
                          });
                        });
                      });
                    }}
                    isDisable={hasError}
                  >
                    <Text style={Theme.textButtonLabelNormalAccent}>
                      {EABi18n({
                        textStore,
                        language,
                        path: "button.save"
                      })}
                    </Text>
                  </EABButton>
                </View>
              </View>
              <View style={styles.profileContainer}>
                <View style={styles.sectionListView}>
                  <View style={styles.clientWrapper}>
                    <TranslatedText
                      style={Theme.subheadSecondary}
                      path="quotation.shield.client"
                      language={language}
                    />
                  </View>
                  <EABSectionList
                    sections={this.prepareSectionListOptions()}
                    showCompleteIcon
                    onPress={id => {
                      this.setState({ isLoading: true }, () => {
                        saveClient(() => {
                          this.setState(
                            {
                              isLoading: false,
                              selectedCid: id
                            },
                            () => {
                              updateInsured({
                                cid: id,
                                profile: this.getProfile(id),
                                isFNA: !this.isFaChannel && isProposer,
                                isInsuredMissing: true
                              });
                            }
                          );
                        });
                      });
                    }}
                  />
                </View>
                <ScrollView style={styles.scrollViewForClientForm}>
                  {this.state.showProfileTab ? (
                    <ClientFormProfile />
                  ) : (
                    <ClientFormContact />
                  )}
                </ScrollView>
              </View>

              <EABHUD isOpen={isLoading} />
            </EABDialog>
            {Object.keys(inputConfigs).length > 0 &&
            selectingRiderInsuredID !== "" ? (
              <RiderDialog
                isOpen
                subQuotation={quotation.insureds[selectingRiderInsuredID]}
                subInputConfigs={inputConfigs[selectingRiderInsuredID]}
                onClose={() => {
                  this.setState({
                    selectingRiderInsuredID: ""
                  });
                }}
                onSave={covCodes => {
                  selectRider({
                    covCodes,
                    cid: selectingRiderInsuredID,
                    alert: errorMessage => {
                      riderAlert({ errorMessage, language });
                    }
                  });
                }}
              />
            ) : null}
          </ScrollView>
        )}
      </ContextConsumer>
    );
  }
}

ShieldQuotation.propTypes = {
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  quotation: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].quotation.isRequired,
  optionsMap: WEB_API_REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  planDetails: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].planDetails.isRequired,
  inputConfigs: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].inputConfigs.isRequired,
  availableInsureds:
    WEB_API_REDUCER_TYPE_CHECK[QUOTATION].availableInsureds.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles: WEB_API_TYPES_CHECK[CLIENT].dependantProfiles.isRequired,
  textStore: REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  quote: PropTypes.func.isRequired,
  selectInsured: PropTypes.func.isRequired,
  selectRider: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired,
  updateClient: PropTypes.func.isRequired,
  loadInsured: PropTypes.func.isRequired,
  updateInsured: PropTypes.func.isRequired,
  hasErrorList: PropTypes.arrayOf(
    PropTypes.shape({
      cid: PropTypes.string.isRequired,
      hasError: PropTypes.bool.isRequired
    })
  ).isRequired,
  agentChannel: PropTypes.string.isRequired,
  checkInsuredList: PropTypes.func.isRequired,
  cleanClientForm: PropTypes.func.isRequired,
  removeCompleteProfile: PropTypes.func.isRequired
};
