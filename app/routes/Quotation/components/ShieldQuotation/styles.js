import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const optionWrapper = {
  marginLeft: Theme.alignmentXL,
  width: 128
};

const container = {
  width: Theme.boxMaxWidth
};

export default StyleSheet.create({
  /**
   * @name scroll view
   * */
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewForClientForm: {
    marginBottom: Theme.alignmentXXL,
    paddingLeft: Theme.alignmentXXS
  },
  scrollViewContent: {
    paddingVertical: Theme.alignmentXL,
    alignItems: "center"
  },
  /**
   * @name container
   * */
  container,
  containerFirst: {
    ...container,
    marginTop: Theme.alignmentXL
  },
  containerLinear: {
    flex: 1
  },
  eligibleContainer: {
    ...container,
    marginTop: 90,
    paddingVertical: Theme.alignmentXXL,
    borderTopWidth: 1,
    borderTopColor: Theme.coolGrey
  },
  /**
   * @name info section
   * */
  infoWrapper: {
    alignItems: "flex-start"
  },
  role: {
    paddingBottom: 1,
    borderBottomWidth: 2,
    borderBottomColor: Theme.brand
  },
  profile: {
    width: "100%"
  },
  /**
   * @name insured section
   * */
  insuredsTitleWrapper: {
    alignItems: "flex-start",
    marginTop: Theme.alignmentXL
  },
  insuredSection: {
    marginTop: Theme.alignmentXL
  },
  insuredMainRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: Theme.alignmentL,
    paddingRight: Theme.alignmentXL,
    minHeight: 96
  },
  insuredInfo: {
    flex: 1
  },
  insuredPlanSelectorWrapper: {
    flexBasis: "auto",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginLeft: Theme.alignmentXL,
    marginVertical: Theme.alignmentXL,
    width: 374
  },
  disableReasonButton: {
    marginRight: Theme.alignmentL
  },
  disableReasonTitle: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentL
  },
  optionRow: {
    flexDirection: "row",
    paddingTop: 10,
    paddingBottom: 14
  },
  optionWrapper,
  optionWrapperFirst: {
    ...optionWrapper,
    marginLeft: 0
  },
  optionTitle: {
    paddingBottom: 1,
    borderBottomWidth: 2,
    borderBottomColor: Theme.brand
  },
  profileContainer: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  clientWrapper: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    marginRight: 215,
    paddingTop: 16
  },
  sectionListView: {
    flexBasis: 280
  },
  tableWrapper: {
    width: "100%"
  },
  tableColumnA: {
    flexBasis: "auto",
    alignItems: "center"
  },
  tableColumnB: {
    flex: 2,
    /**
     * FIXME paddingLeft can not cover padding
     * @see https://github.com/facebook/react-native/issues/16309
     * */
    padding: 0,
    paddingHorizontal: 0,
    paddingVertical: Theme.alignmentM,
    paddingRight: Theme.alignmentL,
    justifyContent: "flex-start"
  },
  tableColumnC: {
    flex: 1,
    justifyContent: "flex-start"
  },
  tableColumnD: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "flex-start"
  },
  tableFooterColumn: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  tableFooterColumnA: {
    flex: 2,
    padding: 0,
    paddingHorizontal: 0,
    paddingVertical: Theme.alignmentM,
    paddingRight: Theme.alignmentL,
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  tableFooterColumnB: {
    flex: 1,
    paddingRight: 14,
    alignItems: "flex-end",
    justifyContent: "flex-start"
  },
  cancelButton: {
    width: 24 + Theme.alignmentL * 2
  },
  riderPremiumText: {
    ...Theme.subheadPrimary,
    textAlign: "right"
  },
  tableHeaderColumn: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    /**
     * FIXME paddingLeft can not cover padding
     * @see https://github.com/facebook/react-native/issues/16309
     * */
    paddingHorizontal: 0,
    paddingRight: Theme.alignmentL
  },
  selectRiderButtonWrapper: {
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentXL,
    alignItems: "flex-start"
  },
  totalRiderPremiumTitle: {
    ...Theme.bodySecondary,
    color: Theme.black,
    marginRight: Theme.alignmentXXS
  },
  totalRiderPremiumContent: {
    ...Theme.bodySecondary,
    color: Theme.black
  },
  planSelector: {
    color: Theme.black
  },
  riderSelector: {
    color: Theme.black
  },
  /**
   * eligible
   * */
  eligibleHint: {
    marginBottom: Theme.alignmentXL
  },
  eligibleHintText: {
    fontStyle: "italic",
    ...Theme.tagLine2Primary
  }
});
