import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  closeButton: {
    marginLeft: Theme.alignmentXL
  }
});
