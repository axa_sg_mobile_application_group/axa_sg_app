import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * <QuotationAppBarLeft />
 * @description navigation bar left component
 * @param {function} cleanQuotation - clean quotation reducer states
 * */
export default class QuotationAppBarLeft extends PureComponent {
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { cleanQuotation, queryQuickQuote } = this.props;

    return (
      <ContextConsumer>
        {({
          closeQuotationDialog,
          isShowingQuotationNavigator,
          hideQuotationNavigator
        }) => (
          <EABButton
            testID="Quotation__btnCancel"
            style={styles.closeButton}
            onPress={() => {
              if (isShowingQuotationNavigator) {
                hideQuotationNavigator();
                cleanQuotation();
                queryQuickQuote();
              } else {
                closeQuotationDialog();
                cleanQuotation();
              }
            }}
          >
            <TranslatedText
              style={Theme.textButtonLabelNormalAccent}
              path="button.cancel"
            />
          </EABButton>
        )}
      </ContextConsumer>
    );
  }
}

QuotationAppBarLeft.propTypes = {
  cleanQuotation: PropTypes.func.isRequired,
  queryQuickQuote: PropTypes.func.isRequired
};
