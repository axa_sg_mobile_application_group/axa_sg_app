import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  row: {
    flexDirection: "row",
    marginBottom: 50
  },
  arrow: {
    height: 20.5,
    width: 12,
    marginLeft: Theme.alignmentXS
  },
  button: {
    marginLeft: Theme.alignmentXS
  }
});
