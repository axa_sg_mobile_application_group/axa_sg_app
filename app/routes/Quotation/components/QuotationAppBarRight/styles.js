import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const optionDialog = {
  flex: 1
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginRight: Theme.alignmentXL
  },
  optionsButton: {
    marginRight: Theme.alignmentXL
  },
  dialogContentWrapper: {
    flex: 1,
    width: "100%"
  },
  optionWrapper: {
    paddingHorizontal: Theme.alignmentXL,
    height: "100%",
    width: "100%"
  },
  policyOptionWrapper: {
    marginBottom: 25
  },
  optionDialog,
  optionDialogMiddle: {
    ...optionDialog,
    alignItems: "center"
  },
  optionDialogRight: {
    ...optionDialog,
    alignItems: "flex-end"
  },
  groupFields: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
    width: 752 - Theme.alignmentXL * 3 - Theme.alignmentL
  },
  field: {
    marginTop: Theme.alignmentXS,
    width: 320
  },
  fullField: {
    marginTop: Theme.alignmentXS,
    width: "100%"
  },
  configFieldFullWidth: {
    width: Theme.boxMaxWidth
  }
});
