import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "flex-start",
    width: 428
  },
  avatarWrapper: {
    flexBasis: "auto",
    marginTop: Theme.alignmentL,
    marginRight: Theme.alignmentL
  },
  descriptionWrapper: {
    flex: 1,
    marginVertical: 13
  }
});
