import React, { PureComponent } from "react";
import { View } from "react-native";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import * as _ from "lodash";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import EABButton from "../../components/EABButton";
import EABSegmentedControl from "../../components/EABSegmentedControl";
import Selector from "../../components/Selector";
import { FLAT_LIST } from "../../components/SelectorField/constants";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import { CLONE, RE_QUOTE } from "./components/ProposalHeaderLeft/constants";
import ProposalDownloadButton from "./containers/ProposalDownloadButton";
import ProposalHeaderLeft from "./containers/ProposalHeaderLeft";
import QuotationAppBarLeft from "./containers/QuotationAppBarLeft";
import QuotationAppBarRight from "./containers/QuotationAppBarRight";
import QuotationUI from "./containers/QuotationUI";
import ProposalUI from "./containers/ProposalUI";
import IllustrationUI from "./containers/IllustrationUI";
import QuotationEmailSection from "./containers/QuotationEmailSection";

/**
 * <Quotation />
 * @reducer navigation.quotation
 * */
class Quotation extends PureComponent {
  render() {
    return (
      <QuotationNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Quotation")
        }}
      />
    );
  }
}

export const QuotationNavigator = createStackNavigator(
  {
    QuotationUI: {
      screen: QuotationUI,
      navigationOptions: ({ navigation }) => ({
        title: "Quotation",
        headerLeft: <QuotationAppBarLeft />,
        headerRight: <QuotationAppBarRight navigation={navigation} />
      })
    },
    ProposalUI: {
      screen: ProposalUI,
      navigationOptions: ({ navigation }) => {
        const canEmail = navigation.getParam("canEmail");
        const canRequote = navigation.getParam("canRequote");
        const canClone = navigation.getParam("canClone");
        const canIllustrate = navigation.getParam("canIllustrate");
        const quotationId = navigation.getParam("quotationId");
        const activatedKey = navigation.getParam("activatedKey");
        const segments = navigation.getParam("segments");

        return {
          title: "Proposal",
          headerTitle: () => (
            <View
              style={{
                paddingTop: 10,
                height: "100%",
                alignItems: "center"
              }}
            >
              <TranslatedText
                style={{
                  ...Theme.title,
                  marginBottom: Theme.alignmentL
                }}
                path="proposal.title"
              />
              {_.isArray(segments) ? (
                <EABSegmentedControl
                  segments={segments}
                  activatedKey={activatedKey}
                />
              ) : null}
            </View>
          ),
          headerLeft: () => {
            if (canRequote) {
              return (
                <ProposalHeaderLeft
                  type={RE_QUOTE}
                  back={navigation.goBack}
                  quotationId={quotationId}
                />
              );
            }

            if (canClone) {
              return (
                <ProposalHeaderLeft
                  type={CLONE}
                  back={navigation.goBack}
                  quotationId={quotationId}
                />
              );
            }
            return null;
          },
          headerRight: (
            <ContextConsumer>
              {({
                closeQuotationDialog,
                isShowingQuotationNavigator,
                hideQuotationNavigator
              }) => (
                <View style={{ flexDirection: "row", marginBottom: 50 }}>
                  <ProposalDownloadButton
                    quotationId={quotationId}
                    activatedKey={activatedKey}
                  />
                  {canEmail ? (
                    <QuotationEmailSection canEmail={canEmail} />
                  ) : null}

                  {canIllustrate ? (
                    <EABButton
                      style={{
                        marginRight: Theme.alignmentXL
                      }}
                      onPress={() => {
                        navigation.navigate({ routeName: "IllustrationUI" });
                      }}
                    >
                      <TranslatedText
                        style={Theme.textButtonLabelNormalAccent}
                        path="proposal.button.illustrate"
                      />
                    </EABButton>
                  ) : null}
                  <EABButton
                    testID="Quotation__btnDone"
                    style={{
                      marginRight: Theme.alignmentXL
                    }}
                    onPress={() => {
                      if (isShowingQuotationNavigator) {
                        hideQuotationNavigator();
                      } else {
                        closeQuotationDialog();
                      }

                      setTimeout(() => {
                        navigation.dispatch({
                          type:
                            ACTION_TYPES[REDUCER_TYPES.QUOTATION]
                              .GET_QUICK_QUOTES,
                          callback: () => {}
                        });

                        navigation.dispatch({
                          type:
                            ACTION_TYPES[REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL,
                          callback: navigation.goBack
                        });
                      }, 1000);
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalEmphasizedAccent}
                      path="button.done"
                    />
                  </EABButton>
                </View>
              )}
            </ContextConsumer>
          ),
          headerStyle: { height: 92 }
        };
      }
    },
    IllustrationUI: {
      screen: IllustrationUI,
      navigationOptions: ({ navigation }) => {
        const rateOptions = navigation.getParam("rateOptions", []);
        const selectedRateValue = navigation.getParam("selectedRateValue");

        return {
          title: "Illustration",
          headerTitle: (
            <TranslatedText style={Theme.title} path="illustration.title" />
          ),
          headerRight: (
            <Selector
              displayTextStyle={Theme.textButtonLabelNormalAccent}
              hasCard={false}
              value={selectedRateValue}
              // TODO: Remove once confirmed
              // selectorType={PICKER}
              // pickerOptions={{
              //   items: rateOptions,
              //   onValueChange: selectedValue => {
              //     navigation.setParams({ selectedRateValue: selectedValue });
              //   },
              //   selectedValue: selectedRateValue
              // }}

              selectorType={FLAT_LIST}
              flatListOptions={{
                renderItemOnPress: (itemKey, item) => {
                  navigation.setParams({ selectedRateValue: item.value });
                },
                extraData: this.state,
                keyExtractor: item => item.key,
                selectedValue: selectedRateValue,
                data: rateOptions
              }}
            />
          )
        };
      }
    }
  },
  {
    initialRouteName: "QuotationUI",
    navigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].quotation
});

export default connect(mapStateToProps)(Quotation);
