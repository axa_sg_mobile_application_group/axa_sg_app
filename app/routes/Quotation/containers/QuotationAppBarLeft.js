import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import QuotationAppBarLeft from "../components/QuotationAppBarLeft";

const { QUOTATION } = REDUCER_TYPES;

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  cleanQuotation: () => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].CLEAN_QUOTATION
    });
  },
  queryQuickQuote: callback => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_QUICK_QUOTES,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationAppBarLeft);
