import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import QuotationAppBarRight from "../components/QuotationAppBarRight";

const { QUOTATION, OPTIONS_MAP } = REDUCER_TYPES;

const mapStateToProps = state => ({
  quotation: state[QUOTATION].quotation,
  optionsMap: state[OPTIONS_MAP],
  quotationErrors: state[QUOTATION].quotationErrors,
  inputConfigs: state[QUOTATION].inputConfigs,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  quote: quotationData => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].QUOTE,
      quotationData
    });
  },
  resetQuot: () => {},
  createProposal: ({ callback, warningCallback, errorCallback }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].CREATE_PROPOSAL,
      callback,
      warningCallback,
      errorCallback
    });
  },
  updateQuotation(newQuotation) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
      newQuotation
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationAppBarRight);
