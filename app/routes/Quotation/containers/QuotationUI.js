import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import QuotationUI from "../components/QuotationUI";

const { QUOTATION } = REDUCER_TYPES;

const mapStateToProps = state => ({
  quotation: state[QUOTATION].quotation,
  planDetails: state[QUOTATION].planDetails,
  inputConfigs: state[QUOTATION].inputConfigs
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationUI);
