import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ShieldQuotation from "../components/ShieldQuotation";

const {
  QUOTATION,
  CLIENT,
  OPTIONS_MAP,
  CLIENT_FORM,
  AGENT,
  PRE_APPLICATION
} = REDUCER_TYPES;

/**
 * ShieldQuotation
 * @description Shield Quotation page.
 * @requires ShieldQuotation - ShieldQuotation UI
 * */
const mapStateToProps = state => ({
  agent: state[AGENT],
  clientForm: state[CLIENT_FORM],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  quotation: state[QUOTATION].quotation,
  planDetails: state[QUOTATION].planDetails,
  inputConfigs: state[QUOTATION].inputConfigs,
  availableInsureds: state[QUOTATION].availableInsureds,
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE],
  isProposerMissing: state[CLIENT_FORM].config.isProposerMissing,
  hasErrorList: state[CLIENT].availableInsuredProfile.hasErrorList,
  agentChannel: state[PRE_APPLICATION].agentChannel
});

const mapDispatchToProps = dispatch => ({
  quote: quotationData => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].QUOTE,
      quotationData
    });
  },
  selectInsured({ cid, covClass }) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].SELECT_INSURED_QUOTATION,
      cid,
      covClass
    });
  },
  selectRider({ cid, covCodes, alert }) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_SHIELD_RIDERS,
      cid,
      covCodes,
      alert
    });
  },
  updateInsured({ cid, profile, isFNA, isInsuredMissing, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].UPDATE_CLIENT_PROFILE_AND_VALIDATE,
      cid,
      profile,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isProposerMissing: isInsuredMissing,
        isInsuredMissing: true,
        isFNA,
        isAPP: false,
        isShieldQuotation: true
      },
      callback
    });
  },
  loadInsured({ cid, profile, isFNA, isInsuredMissing, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT_PROFILE_AND_INIT_VALIDATE,
      cid,
      profile,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isProposerMissing: isInsuredMissing,
        isInsuredMissing: true,
        isFNA,
        isAPP: false,
        isShieldQuotation: true
      },
      callback
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  updateClient() {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_CLIENTS
    });
  },
  checkInsuredList({ pCid, iCids, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CHECK_INSURED_LIST,
      pCid,
      iCids,
      isInsuredMissing: true,
      callback
    });
  },
  cleanClientForm() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  removeCompleteProfile() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].REMOVE_COMPLETE_PROFILE_FROM_LIST
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShieldQuotation);
