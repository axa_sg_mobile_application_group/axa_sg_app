import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import RiderDialog from "../components/RiderDialog";

const { QUOTATION } = REDUCER_TYPES;

const mapStateToProps = state => ({
  planDetails: state[QUOTATION].planDetails,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiderDialog);
