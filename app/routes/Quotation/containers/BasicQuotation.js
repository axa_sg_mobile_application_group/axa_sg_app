import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import BasicQuotation from "../components/BasicQuotation";

const { QUOTATION, OPTIONS_MAP, AGENT, CLIENT } = REDUCER_TYPES;

const mapStateToProps = state => ({
  agent: state[AGENT],
  optionsMap: state[OPTIONS_MAP],
  quotation: state[QUOTATION].quotation,
  planDetails: state[QUOTATION].planDetails,
  inputConfigs: state[QUOTATION].inputConfigs,
  quotationErrors: state[QUOTATION].quotationErrors,
  quotWarnings: state[QUOTATION].quotWarnings,
  availableFunds: state[QUOTATION].availableFunds,
  textStore: state[TEXT_STORE],
  clientPhoto: state[CLIENT].profile.photo,
  dependant: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  quote: quotationData => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].QUOTE,
      quotationData
    });
  },
  resetQuot: ({
    quotation,
    keepConfigs,
    keepPolicyOptions,
    keepPlans,
    callback
  }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].RESET_QUOTE,
      quotation,
      keepConfigs,
      keepPolicyOptions,
      keepPlans,
      callback
    });
  },
  updateRiders: ({ covCodes }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_RIDERS,
      covCodes
    });
  },
  getFunds: ({ invOpt, callback }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].GET_FUNDS,
      invOpt,
      callback
    });
  },
  allocFunds: ({
    invOpt,
    portfolio,
    fundAllocs,
    topUpAllocs,
    hasTopUpAlloc,
    adjustedModel,
    callback
  }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].ALLOC_FUNDS,
      invOpt,
      portfolio,
      fundAllocs,
      topUpAllocs,
      hasTopUpAlloc,
      adjustedModel,
      callback
    });
  },
  updateQuotation(newQuotation) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
      newQuotation
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BasicQuotation);
