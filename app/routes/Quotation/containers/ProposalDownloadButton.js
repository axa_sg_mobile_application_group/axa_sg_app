import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import ProposalDownloadButton from "../components/ProposalDownloadButton";

const { QUOTATION, PROPOSAL } = REDUCER_TYPES;

const mapStateToProps = state => ({
  pdfData: state[PROPOSAL].pdfData,
  quotation: state[QUOTATION].quotation,
  planDetails: state[QUOTATION].planDetails
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalDownloadButton);
