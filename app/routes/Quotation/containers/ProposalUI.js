import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProposalUI from "../components/ProposalUI";

const { PROPOSAL, QUOTATION } = REDUCER_TYPES;

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  quotation: state[PROPOSAL].quotation,
  pdfData: state[PROPOSAL].pdfData,
  planDetails: state[PROPOSAL].planDetails,
  canEmail: state[PROPOSAL].canEmail,
  canRequote: state[PROPOSAL].canRequote,
  canClone: state[PROPOSAL].canClone,
  isQuickQuote: state[QUOTATION].isQuickQuote
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalUI);
