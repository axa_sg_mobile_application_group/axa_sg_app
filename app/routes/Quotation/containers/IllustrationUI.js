import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import IllustrationUI from "../components/IllustrationUI";

const { PROPOSAL, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * IllustrationUI
 * @description IllustrationUI page.
 * @requires IllustrationUI - Illustration UI
 * */

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  quotation: state[PROPOSAL].quotation,
  illustrateData: state[PROPOSAL].illustrateData,
  planDetails: state[PROPOSAL].planDetails,
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IllustrationUI);
