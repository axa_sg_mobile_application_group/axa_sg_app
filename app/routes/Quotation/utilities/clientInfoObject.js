import * as _ from "lodash";
import EABi18n from "../../../utilities/EABi18n";
import { languageTypeConvert } from "../../../utilities/getEABTextByLangType";

/**
 * @name clientInfoObject
 * @description return a object of client information
 * @requires languageTypeConvert - use to options map get text
 * @requires EABi18n - get text
 * @param {string} key - object key
 * @param {string} role - client role
 * @param {string} language - client information text language
 * @param {object} optionsMap - optionsMap reducer state
 * @param {object} textStore - textStore reducer state
 * @param {string} gender - client gender
 * @param {string} smoke - client smoke status
 * @param {number} age - client age
 * @param {string} occupation - client occupation information
 * @param {string} residence - client residence information
 * @param {string} fullName - client full name
 * @param {string} covCode - basic plan code
 * @param {*} photo - client photo
 * @return {object} object of client information
 * */
export default ({
  key,
  role,
  language,
  textStore,
  optionsMap,
  gender,
  age,
  smoke,
  occupation,
  residence,
  fullName,
  covCode,
  photo,
  policytype
}) => {
  const getText = path =>
    EABi18n({
      textStore,
      language,
      path
    });

  const getRole = () =>
    getText(
      `quotation.${role === "P" ? "role_P" : "role_I"}${
        !_.isEmpty(covCode) &&
        (covCode === "DTJ" ||
          ((covCode === "PVTVUL" || covCode === "PVLVUL") &&
            policytype === "joint"))
          ? ".joint"
          : ""
      }`
    );
  const infoAArray = [];
  const infoBArray = [];

  // infoA
  // gender
  if (gender) {
    infoAArray.push(
      gender === "M" ? getText("quotation.male") : getText("quotation.female")
    );
  }

  // age
  if (typeof age === "number") {
    infoAArray.push(`${age} ${getText("quotation.yearsOld")}`);
  }

  // smoke
  if (smoke) {
    infoAArray.push(
      smoke === "Y"
        ? getText("quotation.smoker")
        : getText("quotation.nonSmoker")
    );
  }

  // infoB
  // occupation
  if (occupation) {
    const option = _.find(
      optionsMap.occupation.options,
      optionData => optionData.value === occupation
    );

    if (option) {
      infoBArray.push(option.title[languageTypeConvert(language)]);
    }
  }

  // residence
  if (residence) {
    const option = _.find(
      optionsMap.residency.options,
      optionData => optionData.value === residence
    );

    if (option) {
      infoBArray.push(option.title[languageTypeConvert(language)]);
    }
  }

  return {
    key,
    role: getRole(),
    fullName,
    photo,
    infoA: infoAArray.join(", "),
    infoB: infoBArray.join(", ")
  };
};
