import { ACTION_TYPES, REDUCER_TYPES, ACTION_LIST } from "eab-web-api";
import React, { PureComponent } from "react";
import { Image, Text, View } from "react-native";
import { createBottomTabNavigator, NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import applicationsActive from "../../assets/images/applicationsActive.png";
import applicationsInactive from "../../assets/images/applicationsInactive.png";
import needsActive from "../../assets/images/needsActive.png";
import needsInactive from "../../assets/images/needsInactive.png";
import productsActive from "../../assets/images/productsActive.png";
import productsInactive from "../../assets/images/productsInactive.png";
import profileActive from "../../assets/images/profileActive.png";
import profileInactive from "../../assets/images/profileInactive.png";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import Theme from "../../theme";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import Applications from "../Applications";
import Needs from "../Needs";
import Products from "../Products";
import Profile from "../Profile";
import FnaWarningIcon from "../../containers/FnaWarningIcon";

const { PRE_APPLICATION, PRODUCTS } = REDUCER_TYPES;

/**
 * <Client />
 * @reducer navigation.client
 * @requires Needs - Needs tab
 * @requires Profile - Profile tab
 * @requires Applications - Applications tab
 * @requires Products - Products tab
 * */
class Client extends PureComponent {
  render() {
    return (
      <ClientNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Client")
        }}
      />
    );
  }
}

export const ClientNavigator = createBottomTabNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginLeft: "30%",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? profileActive : profileInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Profile
              </Text>
            </View>
          );
        }
      }
    },
    Needs: {
      screen: Needs,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginLeft: "15%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? needsActive : needsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Needs
                <FnaWarningIcon />
              </Text>
            </View>
          );
        },
        tabBarOnPress: ({ navigation }) => {
          navigation.dispatch({
            type: ACTION_TYPES[REDUCER_TYPES.FNA].GET_FNA,
            callback: () => {
              navigation.navigate({ routeName: "Needs" });
            }
          });
        }
      }
    },
    Products: {
      screen: Products,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginRight: "15%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? productsActive : productsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Products
              </Text>
            </View>
          );
        },
        tabBarOnPress: ({ navigation }) => {
          navigation.dispatch({
            type: ACTION_TYPES[PRODUCTS].PRODUCT_TAB_BAR_SAGA,
            callback: () => {
              navigation.navigate({ routeName: "ProductsDetail" });
              navigation.navigate({ routeName: "Products" });
            }
          });
        }
      }
    },
    Applications: {
      screen: Applications,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginRight: "30%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? applicationsActive : applicationsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Applications
              </Text>
            </View>
          );
        },
        tabBarOnPress: ({ navigation }) => {
          navigation.dispatch({
            type: ACTION_TYPES[PRE_APPLICATION].SAGA_GET_APPLICATIONS
          });
          navigation.dispatch(
            NavigationActions.navigate({
              routeName: "Summary",
              params: {
                selectModeOn: false
              }
            })
          );
          ACTION_LIST[REDUCER_TYPES.PRE_APPLICATION].updateUiSelectMode({
            dispatch: navigation.dispatch,
            selectModeOn: false
          });
          navigation.dispatch(
            NavigationActions.navigate({
              routeName: "Applications"
            })
          );
        }
      }
    }
  },
  {
    initialRouteName: "Needs"
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].client
});

export default connect(mapStateToProps)(Client);
