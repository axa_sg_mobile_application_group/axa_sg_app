import React, { PureComponent } from "react";
import { View } from "react-native";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import EABButton from "../../components/EABButton";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import ProfileDetail from "./containers/ProfileDetail";

/**
 * <Profile />
 * @reducer navigation.profile
 * @requires ProfileDetail - profile page
 * */
class Profile extends PureComponent {
  render() {
    return (
      <ProfileNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Profile")
        }}
      />
    );
  }
}

export const ProfileNavigator = createStackNavigator(
  {
    ProfileDetail: {
      screen: ProfileDetail,
      navigationOptions: ({ navigation }) => {
        const openEditProfileDialog = navigation.getParam(
          "openEditProfileDialog"
        );
        const reloadContactList = navigation.getParam("reloadContactList");
        const clientInfo = navigation.getParam("clientInfo");

        return {
          title: "Profile",
          headerStyle: {
            height: 92
          },
          headerLeft: (
            <ContextConsumer>
              {({ hideClientNavigator }) => (
                <View
                  style={{
                    marginTop: 21,
                    height: "100%",
                    justifyContent: "flex-start"
                  }}
                >
                  <EABButton
                    testID="btnProfileClose"
                    style={{
                      marginLeft: Theme.alignmentXL
                    }}
                    onPress={() => {
                      reloadContactList();
                      hideClientNavigator();
                      cleanClientData(navigation.dispatch);
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalAccent}
                      path="button.close"
                    />
                  </EABButton>
                </View>
              )}
            </ContextConsumer>
          ),
          headerTitle: clientInfo || null,
          headerRight: (
            <ContextConsumer>
              {({ showCreateProfileDialog }) => (
                <View
                  style={{
                    marginTop: Theme.alignmentS,
                    height: "100%",
                    justifyContent: "flex-start"
                  }}
                >
                  {openEditProfileDialog ? (
                    <EABButton
                      testID="btnProfileEdit"
                      style={{
                        marginRight: Theme.alignmentXL
                      }}
                      onPress={() => {
                        openEditProfileDialog();
                        showCreateProfileDialog();
                      }}
                    >
                      <TranslatedText
                        style={Theme.textButtonLabelNormalAccent}
                        path="button.editProfile"
                      />
                    </EABButton>
                  ) : null}
                </View>
              )}
            </ContextConsumer>
          )
        };
      }
    }
  },
  {
    initialRouteName: "ProfileDetail"
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].profile
});

export default connect(mapStateToProps)(Profile);
