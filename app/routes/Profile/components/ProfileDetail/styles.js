import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const table = {
  width: "100%"
};
const sectionHeader = {
  marginBottom: Theme.alignmentL,
  paddingBottom: 2,
  borderBottomWidth: 2,
  borderColor: Theme.brand
};

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContainerStyle: {
    flexDirection: "row",
    justifyContent: "center"
  },
  info: {
    marginVertical: Theme.alignmentXL,
    marginRight: Theme.alignmentXL,
    width: 604
  },
  /**
   *  info style
   *  */
  infoDetail: {
    padding: Theme.alignmentXL
  },
  icon: {
    margin: Theme.alignmentXS,
    width: 24,
    height: 24
  },
  section: {
    alignItems: "flex-start"
  },
  sectionHeader,
  table,
  tableFirst: {
    ...table,
    marginBottom: Theme.alignmentXL
  },
  tableRow: {
    flexDirection: "row",
    width: "100%"
  },
  tableHeader: {
    ...Theme.bodyPrimary,
    flexBasis: "auto",
    width: 188
  },
  tableCellWrapper: {
    alignItems: "flex-end",
    flex: 1,
    flexDirection: "row"
  },
  tableCell: {
    ...Theme.bodySecondary,
    flex: 1
  },
  /**
   * section header
   * */
  headerWrapper: {
    paddingLeft: Theme.alignmentXL,
    paddingRight: Theme.alignmentL,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  header: {
    ...sectionHeader,
    marginBottom: 0
  },
  buttonWrapper: {
    flexDirection: "row"
  },
  /**
   * family style
   * */
  family: {
    marginTop: Theme.alignmentL,
    paddingVertical: Theme.alignmentL
  },
  memberRow: {
    flexDirection: "row",
    paddingVertical: Theme.alignmentXS,
    paddingHorizontal: Theme.alignmentXL
  },
  memberPhotoWrapper: {
    flexBasis: "auto",
    marginVertical: 2,
    backgroundColor: Theme.brandTransparent,
    borderRadius: Theme.radiusL,
    width: 40,
    height: 40,
    overflow: "hidden"
  },
  memberPhoto: {
    width: 40,
    height: 40
  },
  memberInfo: {
    flex: 1,
    marginLeft: Theme.alignmentL
  },
  errorIconWrapper: {
    flexBasis: "auto",
    justifyContent: "center"
  },
  errorIcon: {
    width: 24,
    height: 24
  },
  relationshipErrorIcon: {
    width: 21,
    height: 21,
    marginBottom: Theme.alignmentXXS
  },
  /**
   * individual
   * */
  individual: {
    marginTop: Theme.alignmentL,
    paddingVertical: Theme.alignmentL
  },
  individualHeader: {
    marginBottom: Theme.alignmentL,
    paddingBottom: 2,
    borderBottomWidth: 2,
    borderColor: Theme.brand
  },
  /* contact style */
  contact: {
    marginVertical: Theme.alignmentXL,
    width: 348
  },
  contactRow: {
    flexDirection: "row",
    paddingHorizontal: Theme.alignmentXL,
    paddingVertical: Theme.alignmentM
  },
  contactIcon: {
    flexBasis: "auto",
    marginTop: Theme.alignmentXL - Theme.alignmentM,
    marginRight: Theme.alignmentXL,
    width: 24,
    height: 24
  },
  contactDetails: {
    flex: 1
  },
  contactTitle: {
    ...Theme.subheadSecondary,
    marginBottom: 4
  },
  /**
   * trusted individual form
   * */
  tiScrollView: {
    width: "100%",
    height: "100%"
  },
  tiScrollViewContainerStyle: {
    paddingHorizontal: 72
  }
});
