import { ACTION_TYPES, REDUCER_TYPES, utilities } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProfileDetail from "../components/ProfileDetail";

const { OPTIONS_MAP, CLIENT_FORM, CLIENT } = REDUCER_TYPES;
const { dataMapping } = utilities;
const { profileMapForm } = dataMapping;

/**
 * ProfileDetail
 * @requires ProfileDetail - ProfileDetail UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  cancelCreateClient() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  readyToAddFamilyMember(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: true,
        isFromProduct: false
      },
      callback
    });
  },
  getFamilyMemberData(data) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      cid: data.cid
    });
  },
  readyToEditFamilyMember(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: true,
        isFromProduct: false
      },
      callback
    });
  },
  saveClient({ confirm, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm,
      callback
    });
  },
  reloadContactList(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
      callback
    });
  },
  readyToEditClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: true,
        isFromProduct: false
      },
      callback
    });
  },
  readyToAddTrustedIndividual(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isTrustedIndividual: true
      },
      callback
    });
  },
  saveTrustedIndividual({ callback, confirm, alert = () => {} }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_TRUSTED_INDIVIDUAL,
      confirm,
      callback,
      alert
    });
  },
  loadClient(profile) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      ...profileMapForm(profile)
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileDetail);
