import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import Main from "./containers/Main";
import { checkLoginStatus } from "../../actions/login";

/**
 * <Main />
 * @reducer navigation.root
 * @requires Main
 * */
class Root extends PureComponent {
  componentDidMount() {
    this.props.checkLoginStatus();
  }

  render() {
    return (
      <RootNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Root")
        }}
      />
    );
  }
}

export const RootNavigator = createStackNavigator(
  {
    Main: { screen: Main }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].root
});

const mapDispatchToProps = dispatch => ({
  checkLoginStatus() {
    checkLoginStatus({ dispatch });
  }
});

Root.propTypes = {
  checkLoginStatus: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);
