import { connect } from "react-redux";
import OnlineAuth from "../components/OnlineAuth";
import {
  loginByOnlineAuth,
  onlineAuthCode,
  checkAppLocked
} from "../../../actions/login";
import ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN } from "../../../constants/REDUCER_TYPES";

/**
 * OnlineAuth
 * @description OnlineAuth container.
 * @reducer login
 * @requires OnlineAuth - OnlineAuth UI
 * */
const mapStateToProps = state => ({
  isAppPasswordExpired: state[LOGIN].isAppPasswordExpired,
  environment: state[LOGIN].environment,
  isAppLocked: state[LOGIN].isAppLocked,
  isHandlingListener: state[LOGIN].isHandlingListener,
  isExceedFailLoginAttemps: state[LOGIN].isExceedFailLoginAttemps
});

const mapDispatchToProps = dispatch => ({
  loginByOnlineAuth() {
    loginByOnlineAuth({ dispatch });
  },
  checkTimeBomb(callback) {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHECK_TIME_BOMB,
      callback
    });
  },
  onlineAuthCode(authCode, callback) {
    onlineAuthCode({ dispatch, authCode, callback });
  },
  checkAppLocked(callback) {
    checkAppLocked({ dispatch, callback });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnlineAuth);
