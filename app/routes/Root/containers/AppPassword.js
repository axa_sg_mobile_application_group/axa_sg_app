import { connect } from "react-redux";
import AppPassword from "../components/AppPassword";
import {
  resetAppPasswordManually,
  saveNewAppPassword,
  checkFailAttempt
} from "../../../actions/login";
import ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN } from "../../../constants/REDUCER_TYPES";
import { hash } from "../../../utilities/SecureUtils";

/**
 * AppPassword
 * @description AppPassword container.
 * @reducer login
 * @requires AppPassword - AppPassword UI
 * */
const mapStateToProps = state => ({
  showResetAppPasswordManuallyScene:
    state[LOGIN].showResetAppPasswordManuallyScene,
  showResetAppPasswordScene: state[LOGIN].showResetAppPasswordScene,
  isAppPasswordExpired: state[LOGIN].isAppPasswordExpired
});

const mapDispatchToProps = dispatch => ({
  saveNewAppPassword(appPassword, callback) {
    const hashedAppPassword = hash(appPassword);
    saveNewAppPassword({ dispatch, hashedAppPassword, callback });
  },
  navigateToLandingPage() {
    dispatch({
      type: ACTION_TYPES[LOGIN].CANCEL_RESET_APP_PASSWORD_MANUALLY
    });
  },
  resetAppPasswordManually(appPassword, callback) {
    const hashedAppPassword = hash(appPassword);
    resetAppPasswordManually({ dispatch, hashedAppPassword, callback });
  },
  exceedFailLoginAttempts() {
    dispatch({
      type: ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS
    });
  },
  checkFailAttempt(callback) {
    checkFailAttempt({ dispatch, callback });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppPassword);
