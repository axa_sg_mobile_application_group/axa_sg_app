import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import OnlineAuthBackPage from "../components/OnlineAuthBackPage";
import {
  loginByOnlineAuth,
  onlineAuthCode,
  verify121OnlineAuth,
  logout,
  setLastDataSyncTime
} from "../../../actions/login";
import ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN } from "../../../constants/REDUCER_TYPES";

const { AGENT } = REDUCER_TYPES;
/**
 * OnlineAuth
 * @description OnlineAuth container.
 * @reducer login
 * @requires OnlineAuth - OnlineAuth UI
 * */
const mapStateToProps = state => ({
  login: state[LOGIN],
  agent: state[AGENT],
  isTimezoneValid: state[LOGIN].isTimezoneValid,
  isAppPasswordExpired: state[LOGIN].isAppPasswordExpired,
  isExceedFailLoginAttemps: state[LOGIN].isExceedFailLoginAttemps
});

const mapDispatchToProps = dispatch => ({
  loginByOnlineAuth() {
    loginByOnlineAuth({ dispatch });
  },
  verify121OnlineAuth(callback) {
    verify121OnlineAuth({ dispatch, callback });
  },
  checkTimeBomb(callback) {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHECK_TIME_BOMB,
      callback
    });
  },
  logout() {
    logout({ dispatch });
  },
  onlineAuthCode(authCode, callback) {
    onlineAuthCode({ dispatch, authCode, callback });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnlineAuthBackPage);
