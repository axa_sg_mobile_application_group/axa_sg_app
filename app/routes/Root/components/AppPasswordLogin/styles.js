import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%"
  },
  loginForm: {
    alignItems: "center",
    paddingHorizontal: Theme.alignmentXXXL,
    paddingTop: Theme.alignmentXXXL,
    paddingBottom: Theme.alignmentXL,
    backgroundColor: Theme.white,
    borderRadius: Theme.radiusXS,
    width: 416,
    shadowColor: Theme.lightGrey,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 2,
    shadowOpacity: 1
  },
  idWrapper: {
    width: "100%",
    alignItems: "flex-start"
  },
  logoWrapper: {
    alignItems: "center",
    marginBottom: Theme.alignmentXL
  },
  logo: {
    marginBottom: Theme.alignmentXL,
    width: 48,
    height: 48
  },
  versionText: Theme.subheadSecondary,
  loginButton: {
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXXXL,
    width: "100%"
  }
});
