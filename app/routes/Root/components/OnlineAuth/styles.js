import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%"
  },
  loginForm: {
    alignItems: "flex-start",
    paddingHorizontal: Theme.alignmentXXXL,
    paddingTop: Theme.alignmentXXXL,
    paddingBottom: Theme.alignmentXXL,
    backgroundColor: Theme.white,
    borderRadius: Theme.radiusXS,
    width: 416,
    shadowColor: Theme.lightGrey,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 2,
    shadowOpacity: 1
  },
  logoWrapper: {
    alignItems: "flex-end",
    marginBottom: Theme.alignmentXL
  },
  logo: {
    width: 48,
    height: 48
  },
  welcome: {
    ...Theme.tagLine1Primary,
    marginBottom: Theme.alignmentXL
  },
  box: { flex: 1, marginTop: 20, width: "100%", height: "100%" },
  button: {
    marginBottom: Theme.alignmentXXXL,
    width: 325
  },
  versionText: Theme.subheadSecondary
});
