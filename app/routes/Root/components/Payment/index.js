import PropTypes from "prop-types";
import Config from "react-native-config";
import React, { PureComponent } from "react";
import LinearGradient from "react-native-linear-gradient";
import {
  Linking,
  View,
  Image,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import Theme from "../../../../theme";
import styles from "./styles";
import TranslatedText from "../../../../containers/TranslatedText";
import SelectorField from "../../../../components/SelectorField";
import { PICKER } from "../../../../components/SelectorField/constants";
/**
 * <Payment />
 * */
export default class Payment extends PureComponent {
  constructor(props) {
    super(props);
    // NB001003-02069
    this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    this.path2 = "http://192.168.223.124:3000/goMobilePayment/NB001003-02073";
    this.payPressed = this.payPressed.bind(this);
    this.onAuthLogin = this.onAuthLogin.bind(this);
    this.rediectViaSafari = this.rediectViaSafari.bind(this);
    this.handAuthCode = this.handAuthCode.bind(this);
    this.goToWebPage = this.goToWebPage.bind(this);
    this.state = {
      paymentMethod: "Credit Card",
      isSelected: false,
      isAllowed: false,
      closeDialog: false
    };
  }
  componentDidMount() {
    // Linking.addEventListener("url", this.handAuthCode);
  }
  componentWillUnmount() {
    // Linking.removeEventListener("url", this.handAuthCode);
  }

  onAuthLogin() {
    // const getPaymentMethod = this.state.paymentMethod;
    if (this.state.isSelected) {
      const warningMessage = "Online Authentication is required.";
      Alert.alert(
        "Payment",
        warningMessage,
        [
          {
            text: "Open Safari",
            onPress: () => this.rediectViaSafari()
          },
          {
            text: "Cancel",
            onPress: () => this.cancel(),
            style: "cancel"
          }
        ],
        { cancelable: false }
      );
    }
  }
  dismiss() {
    this.props.getPaymentStatus("NB001003-02064", getPaymentStatusResponse => {
      if (!getPaymentStatusResponse.hasError) {
        const paymentStatus = getPaymentStatusResponse.status;
        Alert.alert(
          "Your Payment Status is",
          paymentStatus,
          [
            {
              text: "Close",
              onPress: () => this.cancel(),
              style: "close"
            }
          ],
          { cancelable: false }
        );
      } else {
        Alert.alert(`[Error] ${getPaymentStatusResponse.errorMsg}`);
      }
    });
  }
  cancel() {
    this.setState({
      closeDialog: true
    });
  }
  payPressed() {
    this.props.validatePayment("NB001003-02064", responseResult => {
      if (!responseResult.hasError) {
        const validatePaymentResult = responseResult.result;
        const applicationResult = responseResult.result.application;
        const docId = validatePaymentResult.application.id;
        this.props.dataSync(docId, applicationResult, dataSyncResult => {
          if (!dataSyncResult.hasError) {
            const warningMessage = "Online Payment in progress";
            this.setState(
              {
                isAllowed: true
              },
              () => {
                Alert.alert(
                  "Payment",
                  warningMessage,
                  [
                    {
                      text: "Dismiss",
                      onPress: () => this.dismiss(),
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }
            );
          } else {
            Alert.alert(`[Error] ${dataSyncResult.errorMsg}`);
          }
        });
      } else {
        Alert.alert(`[Error] ${responseResult.errorMsg}`);
      }
    });
    this.setState({
      isSelected: true
    });
  }

  selectPaymentMethod() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText
          style={styles.welcome}
          path="payment.PaymentSettingHeader"
        />
        <SelectorField
          value={this.state.paymentMethod}
          placeholder="Select Payment"
          selectorType={PICKER}
          popoverOptions={{
            preferredContentSize: [300, 200],
            permittedArrowDirections: [0]
          }}
          pickerOptions={{
            items: [
              { label: "Credit Card", value: "Credit Card" },
              { label: "IPP", value: "IPP" },
              { label: "eNet", value: "eNet" }
            ],
            onValueChange: itemValue => {
              this.setState({
                paymentMethod: itemValue
              });
            }
          }}
        />
        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.payPressed}
        >
          <TranslatedText path="payment.PaymentButton" />
        </EABButton>
      </View>
    );
  }

  redirectPrePage() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText style={styles.welcome} path="payment.onlinePayment" />

        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.rediectViaSafari}
        >
          <TranslatedText path="payment.goToSafari" />
        </EABButton>
      </View>
    );
  }

  rediectViaSafari() {
    if (this.path) {
      const url = this.path;
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          // TODO: if path is invalid to redirect: what to do
          return null;
        }
        return Linking.openURL(url);
      });
    }
  }
  goToWebPage() {
    if (this.path2) {
      const url = this.path2;
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          // TODO: if path is invalid to redirect: what to do
          return null;
        }
        return Linking.openURL(url);
      });
    }
  }

  render() {
    let renderResult = null;
    if (this.state.closeDialog) {
      renderResult = this.selectPaymentMethod();
    } else if (!this.state.isAllowed) {
      renderResult = this.selectPaymentMethod();
    } else {
      renderResult = this.goToWebPage();
    }
    return (
      <View style={styles.box}>
        <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
          <KeyboardAvoidingView behavior="padding">
            {renderResult}
          </KeyboardAvoidingView>
        </LinearGradient>
      </View>
    );
  }
}

Payment.propTypes = {
  // initPayStore: PropTypes.func.isRequired,
  // startPayment: PropTypes.func.isRequired,
  getApplication: PropTypes.func.isRequired,
  dataSync: PropTypes.func.isRequired,
  validatePayment: PropTypes.func.isRequired,
  getPaymentStatus: PropTypes.func.isRequired
  // isOnlineAuthCompleted: PropTypes.bool.isRequired,
  // loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  // setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  // verify121OnlineAuthForPayment: PropTypes.func.isRequired
};
