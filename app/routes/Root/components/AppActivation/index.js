import PropTypes from "prop-types";
import Config from "react-native-config";
import SafariView from "react-native-safari-view";
import React, { PureComponent } from "react";
import LinearGradient from "react-native-linear-gradient";
import {
  Linking,
  View,
  Image,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import Theme from "../../../../theme";
import styles from "./styles";
import { MUILTIPLE_DEVICE } from "../../../../constants/ERROR_MSG";
import TranslatedText from "../../../../containers/TranslatedText";

/**
 * <AppActivation />
 * @param {string}: PropTypes.string.isRequired,
 * @param {function} setOnlineAuthCode - action after getting the authCode
 * @param {function} LoginFailByOnlineAuth - callback when online auth fail
 * */
export default class AppActivation extends PureComponent {
  constructor(props) {
    super(props);
    const { environment } = this.props;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }

    this.state = {
      changePage: false
    };
    this.pressHandler = this.pressHandler.bind(this);
    this.handAuthCode = this.handAuthCode.bind(this);
  }
  componentDidMount() {
    Linking.addEventListener("url", this.handAuthCode);
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.handAuthCode);
  }

  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  handAuthCode(event) {
    this.setState({ changePage: true });
    const urlGet = event.url;
    SafariView.dismiss();
    const segment1 = urlGet.replace("easemobile://?", "");
    const variables = segment1.split("&");
    const codeString = variables.find(variable => variable.includes("code"));
    const [, authCode] = codeString.split("=");
    if (!this.props.isHandlingListener) {
      this.props.onlineAuthCode(authCode, errorResult => {
        if (errorResult.hasError) {
          if (errorResult.error) {
            if (errorResult.error === MUILTIPLE_DEVICE) {
              setTimeout(() => {
                Alert.alert(
                  "Warning",
                  `[Error] ${errorResult.error}`,
                  [
                    {
                      text: "OK",
                      onPress: () => this.props.onlineAuthCode2(() => {}),
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            } else {
              setTimeout(() => {
                Alert.alert(`${errorResult.error}`);
              }, 200);
            }
          } else {
            setTimeout(() => {
              Alert.alert(`[Error] Authorization Failed (Error 001)`);
            }, 200);
          }
        }
      });
    }
  }
  rediectViaSafari() {
    if (this.path) {
      const url = this.path;
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          return null;
        }
        return Linking.openURL(url);
      });
    }
  }

  pressHandler2() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  renderResult() {
    if (this.state.changePage) {
      return (
        <View style={styles.container}>
          <View style={styles.loginForm}>
            <View style={styles.logoWrapper}>
              <Image style={styles.logo} source={axaLogo} />
            </View>
            <TranslatedText style={styles.welcome} path="login.waiting" />
          </View>
        </View>
      );
    } else if (this.props.isAppLocked) {
      return null;
    }
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText style={styles.welcome} path="login.appActivation" />

        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.pressHandler}
        >
          <TranslatedText path="login.LogIn" />
        </EABButton>
      </View>
    );
  }
  render() {
    return (
      <View style={styles.box}>
        <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
          <KeyboardAvoidingView behavior="padding">
            {this.renderResult()}
          </KeyboardAvoidingView>
        </LinearGradient>
      </View>
    );
  }
}

AppActivation.propTypes = {
  isAppLocked: PropTypes.bool,
  isHandlingListener: PropTypes.bool.isRequired,
  checkAppLocked: PropTypes.func.isRequired,
  disableAppActivationPage: PropTypes.bool.isRequired,
  setEnvironment: PropTypes.func.isRequired,
  environment: PropTypes.string.isRequired,
  onlineAuthCode: PropTypes.func.isRequired,
  onlineAuthCode2: PropTypes.func.isRequired,
  LoginFailByOnlineAuth: PropTypes.func.isRequired
};
AppActivation.defaultProps = {
  isAppLocked: false
};
