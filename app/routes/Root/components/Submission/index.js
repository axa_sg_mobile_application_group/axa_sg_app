import PropTypes from "prop-types";
import Config from "react-native-config";
import React, { PureComponent } from "react";
import LinearGradient from "react-native-linear-gradient";
import {
  Linking,
  View,
  Image,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import styles from "./styles";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import Theme from "../../../../theme";
import TranslatedText from "../../../../containers/TranslatedText";
/**
 * <Submission />
 * */
export default class Submission extends PureComponent {
  constructor(props) {
    super(props);
    this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    this.path2 =
      "http://axa_sg_mobile_application_group.eabgitlab.com/AXA_SG_APP_web_service/mock/payment.html";
    this.submitPressed = this.submitPressed.bind(this);
    this.onAuthLogin = this.onAuthLogin.bind(this);
    this.rediectViaSafari = this.rediectViaSafari.bind(this);
    this.handAuthCode = this.handAuthCode.bind(this);
    this.goToWebPage = this.goToWebPage.bind(this);
    this.state = {
      isAllowed: false,
      isSubmissionCompleted: false
      // to validateSubmission
    };
  }
  componentDidMount() {
    // Linking.addEventListener("url", this.handAuthCode);
  }
  componentWillUnmount() {
    // Linking.removeEventListener("url", this.handAuthCode);
  }

  onAuthLogin() {
    if (this.state.isAllowed) {
      const warningMessage = "Online Authentication is required.";
      // Alert.alert(`${warningMessage}`);
      this.props.loginByOnlineAuthForPayment();
      Alert.alert(
        "Submission",
        warningMessage,
        [
          {
            text: "Continue",
            onPress: () => this.rediectViaSafari()
          },
          {
            text: "Cancel",
            onPress: () => this.cancel(),
            style: "cancel"
          }
        ],
        { cancelable: false }
      );
    }
  }
  cancel() {
    this.setState({
      isAllowed: false
    });
  }
  submitPressed() {
    this.props.validateSubmission("NB001003-02064", responseResult => {
      if (!responseResult.hasError) {
        const validateSubmissionResult = responseResult.result;
        const applicationResult = responseResult.result.application;
        const docId = validateSubmissionResult.application.id;
        this.props.dataSync(docId, applicationResult, dataSyncResult => {
          if (!dataSyncResult.hasError) {
            // this.props.loginByOnlineAuthForPayment();
            this.setState({
              isAllowed: true
            });
          } else {
            Alert.alert(`[Error] ${dataSyncResult.errorMsg}`);
          }
        });
      } else {
        Alert.alert(`[Error] ${responseResult.errorMsg}`);
      }
    });
  }
  // dismiss() {
  //   // do something to close route to different page
  // }
  handAuthCode(event) {
    const urlGet = event.url;
    const segment1 = urlGet.replace("easemobile://?", "");
    const variables = segment1.split("&");
    const codeString = variables.find(variable => variable.includes("code"));
    const [, authCode] = codeString.split("=");
    this.props.setOnlineAuthCodeForPayment(authCode);
  }
  submissionPage() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText
          style={styles.welcome}
          path="submission.SubmissionSettingHeader"
        />
        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.submitPressed}
        >
          <TranslatedText path="submission.SubmitButton" />
        </EABButton>
      </View>
    );
  }
  submissionCompleted() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText
          style={styles.welcome}
          path="submission.SubmissionCompleted"
        />
        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.dismiss}
        >
          <TranslatedText path="submission.backButton" />
        </EABButton>
      </View>
    );
  }
  redirectPrePage() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText style={styles.welcome} path="submission.onlineAuth" />

        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          onPress={this.rediectViaSafari}
        >
          <TranslatedText path="submission.goToSafari" />
        </EABButton>
      </View>
    );
  }
  rediectViaSafari() {
    if (this.path) {
      const url = this.path;
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          // TODO: if path is invalid to redirect: what to do
          return null;
        }
        return Linking.openURL(url);
      });
    }
  }
  goToWebPage() {
    if (this.path2) {
      const url = this.path2;
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          // TODO: if path is invalid to redirect: what to do
          return null;
        }
        return Linking.openURL(url);
      });
    }
  }
  submitApplication() {
    this.props.submitApplication("NB001003-02064", submitResponse => {
      if (!submitResponse.hasError) {
        // Submission is successfully
        this.setState({
          isSubmissionCompleted: true
        });
      } else {
        // Submission is not successfully
        Alert.alert(`[Error] ${submitResponse.errorMsg}`);
      }
    });
  }
  render() {
    let renderResult = null;
    if (this.state.isSubmissionCompleted) {
      renderResult = this.submitApplication();
    } else if (this.props.isOnlineAuthCompleted) {
      renderResult = this.submissionCompleted();
    } else if (!this.state.isAllowed) {
      renderResult = this.submissionPage();
    } else {
      renderResult = this.onAuthLogin();
    }
    return (
      <View style={styles.box}>
        <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
          <KeyboardAvoidingView behavior="padding">
            {renderResult}
          </KeyboardAvoidingView>
        </LinearGradient>
      </View>
    );
  }
}

Submission.propTypes = {
  initPayStore: PropTypes.func.isRequired,
  startPayment: PropTypes.func.isRequired,
  getApplication: PropTypes.func.isRequired,
  validateSubmission: PropTypes.func.isRequired,
  dataSync: PropTypes.func.isRequired,
  submitApplication: PropTypes.func.isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  verify121OnlineAuthForPayment: PropTypes.func.isRequired
};
