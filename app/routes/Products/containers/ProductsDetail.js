import { ACTION_TYPES, REDUCER_TYPES, utilities } from "eab-web-api";
import { connect } from "react-redux";
import ProductsDetail from "../components/ProductsDetail";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const {
  PRODUCTS,
  OPTIONS_MAP,
  CLIENT_FORM,
  CLIENT,
  QUOTATION,
  FNA
} = REDUCER_TYPES;
const { dataMapping } = utilities;
const { profileMapForm } = dataMapping;

/**
 * ProductsDetail
 * @description ProductsDetail page.
 * @requires ProductsDetail - ProductsDetail UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  currency: state[PRODUCTS].currency,
  insuredCid: state[PRODUCTS].insuredCid,
  dependants: state[PRODUCTS].dependants,
  productList: state[PRODUCTS].productList,
  optionsMap: state[OPTIONS_MAP],
  errorMsg: state[PRODUCTS].errorMsg,
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  getProductList(callback) {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].GET_PRODUCT_LIST,
      callback
    });
  },
  getQuotation({
    productID,
    alert,
    confirmRequest,
    openQuotationFunction,
    confirm
  }) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].GET_QUOTATION,
      productID,
      alert,
      confirmRequest,
      openQuotationFunction,
      confirm
    });
  },
  currencyOnChange: currencyData => {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].UPDATE_CURRENCY,
      currencyData
    });
  },
  insuredCidOnChange: insuredCidData => {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].UPDATE_INSURED_CID,
      insuredCidData
    });
  },
  getErrorMsg(callback) {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].GET_ERROR_MSG,
      callback
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  cancelCreateClient() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  readyToEditProfile({ isFamilyMember, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember,
        isApplication: false,
        isProposerMissing: true,
        isFromProfile: false,
        isFromProduct: true
      },
      callback
    });
  },
  getProfileData({ cid, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GET_PROFILE_DATA,
      cid,
      callback
    });
  },
  loadClient(profile) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      ...profileMapForm(profile)
    });
  },
  getFNA: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsDetail);
