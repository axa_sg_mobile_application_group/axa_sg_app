import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image, Switch, View } from "react-native";
import icHistory from "../../../../assets/images/icHistory.png";
import EABButton from "../../../../components/EABButton";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import styles from "./styles";

const { QUOTATION } = REDUCER_TYPES;

/**
 * <QuickQuoteButtonGroup />
 * @description productDetails page app bar quick quote
 * @param textStore - reducer textStore state
 * @param isQuickQuote - reducer quotation.isQuickQuote state
 * @param {function} quickQuoteOnChange - change quickQuote state
 * */
export default class QuickQuoteButtonGroup extends PureComponent {
  constructor() {
    super();

    this.state = {
      isLoadingProducts: false
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.switchContainer}>
          <TranslatedText
            style={styles.quickQuoteText}
            path="products.button.quickQuote"
            textStore={this.props.textStore}
            funcOnText={text => `${text}:`}
          />
          <Switch
            value={this.props.isQuickQuote}
            onValueChange={value => {
              /* should show HUD when loading new product list */
              this.setState(
                {
                  isLoadingProducts: true
                },
                () => {
                  this.props.quickQuoteOnChange({
                    isQuickQuoteData: value,
                    callback: () => {
                      this.setState({
                        isLoadingProducts: false
                      });
                    }
                  });
                }
              );
            }}
          />
        </View>
        <EABButton
          containerStyle={styles.quickQuoteHistory}
          onPress={() => {
            this.props.navigation.navigate({
              routeName: "QuickQuoteHistory"
            });
          }}
          isDisable={!this.props.isQuickQuote}
        >
          <Image style={styles.quickQuoteHistoryIcon} source={icHistory} />
        </EABButton>
        <EABHUD isOpen={this.state.isLoadingProducts} />
      </View>
    );
  }
}

QuickQuoteButtonGroup.propTypes = {
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  isQuickQuote: REDUCER_TYPE_CHECK[QUOTATION].isQuickQuote.isRequired,
  quickQuoteOnChange: PropTypes.func.isRequired
};
