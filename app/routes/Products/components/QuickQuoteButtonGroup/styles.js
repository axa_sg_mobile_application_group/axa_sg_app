import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: Theme.alignmentXL
  },
  switchContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  quickQuoteText: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentL
  },
  quickQuoteHistory: {
    marginLeft: Theme.alignmentL
  },
  quickQuoteHistoryIcon: {
    width: Theme.alignmentXL,
    height: Theme.alignmentXL
  }
});
