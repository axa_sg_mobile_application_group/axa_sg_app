import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const selectorSize = {
  width: 276,
  height: 40
};

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    paddingVertical: Theme.alignmentXL
  },
  optionsWrapper: {
    flexDirection: "row",
    margin: Theme.alignmentXL
  },
  firstSearchField: {
    marginRight: Theme.alignmentXL
  },
  section: {
    marginBottom: Theme.alignmentXL
  },
  sectionHeader: {
    paddingHorizontal: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  seeAllButtonText: {
    ...Theme.textButtonLabelSmallAccent,
    marginRight: Theme.alignmentXXS
  },
  seeAllButtonIcon: {
    width: 24,
    height: 24
  },
  productScrollView: {
    width: "100%"
  },
  productScrollViewContent: {
    paddingHorizontal: Theme.alignmentXL
  },
  productImage: {
    marginBottom: Theme.alignmentXS
  },
  productSize: {
    width: "100%",
    height: "100%"
  },
  productText: {
    ...Theme.subheadPrimary,
    width: "100%"
  },
  selectorPadding: {
    paddingRight: 24
  },
  selectorSize,
  selectorSizeFirst: {
    ...selectorSize,
    paddingRight: 24
  },
  noRecordsContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    flex: 1
  },
  noRecordsView: {
    alignItems: "center"
  },
  missingDataTitle: {
    ...Theme.tagLine1Primary,
    marginTop: Theme.alignmentXL
  },
  missingDataHint: {
    ...Theme.bodySecondary,
    marginTop: Theme.alignmentXS,
    marginBottom: Theme.alignmentXL
  },
  /* dialog style */
  headerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  }
});
