import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import EABButton from "../../components/EABButton";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import ProductsDetail from "./containers/ProductsDetail";
import QuickQuoteButtonGroup from "./containers/QuickQuoteButtonGroup";
import QuickQuoteHistory from "./containers/QuickQuoteHistory";
import arrowIcon from "../../assets/images/back-icon.png";

const { QUOTATION } = REDUCER_TYPES;

/**
 * <Products />
 * @reducer navigation.products
 * @requires ProductsDetail - Products index page
 * */
class Products extends PureComponent {
  render() {
    return (
      <ProductsNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Products")
        }}
      />
    );
  }
}

export const ProductsNavigator = createStackNavigator(
  {
    ProductsDetail: {
      screen: ProductsDetail,
      navigationOptions: ({ navigation }) => ({
        title: "Products",
        headerLeft: (
          <ContextConsumer>
            {({ hideClientNavigator }) => (
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                onPress={() => {
                  hideClientNavigator();
                  cleanClientData(navigation.dispatch);
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="button.close"
                />
              </EABButton>
            )}
          </ContextConsumer>
        ),
        headerRight: <QuickQuoteButtonGroup navigation={navigation} />
      })
    },
    QuickQuoteHistory: {
      screen: QuickQuoteHistory,
      navigationOptions: ({ navigation }) => {
        const selectModeOn = navigation.getParam("selectModeOn", false);
        const selectedIdList = navigation.getParam("selectedIdList", []);

        return {
          headerTitle: selectModeOn ? (
            <TranslatedText
              style={Theme.title}
              path="proposal.title.num_selected"
              replace={[{ value: `${selectedIdList.length}`, isPath: false }]}
            />
          ) : (
            <TranslatedText
              style={Theme.title}
              path="quickQuote.quickQuoteHistory"
            />
          ),
          headerLeft: selectModeOn ? (
            <View style={{ flexDirection: "row" }}>
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                isDisable={selectedIdList.length === 0}
                onPress={() => {
                  const quotIds = selectedIdList.slice();
                  navigation.dispatch({
                    type: ACTION_TYPES[QUOTATION].DELETE_QUICK_QUOTES,
                    quotIds
                  });
                  navigation.setParams({
                    selectedIdList: [],
                    selectModeOn: false
                  });
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="button.delete"
                />
              </EABButton>
            </View>
          ) : (
            <View style={{ flexDirection: "row" }}>
              <Image
                style={{
                  height: 20.5,
                  width: 12,
                  marginLeft: Theme.alignmentXS
                }}
                source={arrowIcon}
              />
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXS
                }}
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="proposal.button.products"
                />
              </EABButton>
            </View>
          ),
          headerRight: selectModeOn ? (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                navigation.setParams({
                  selectModeOn: false,
                  selectedIdList: []
                });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.cancel"
              />
            </EABButton>
          ) : (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                navigation.setParams({ selectModeOn: true });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.select"
              />
            </EABButton>
          )
        };
      }
    }
  },
  {
    initialRouteName: "ProductsDetail",
    navigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].products
});

export default connect(mapStateToProps)(Products);
