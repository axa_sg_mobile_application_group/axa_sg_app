import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationPlanDetails from "../components/ApplicationPlanDetails";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * ApplicationPlanDetails
 * @description Normal Item ApplicationList inside Application Summary page.
 * @requires ApplicationPlanDetails - ApplicationPlanDetails UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  application: state[REDUCER_TYPES.APPLICATION].application,
  selectedAppListFilter:
    state[REDUCER_TYPES.PRE_APPLICATION].component.selectedAppListFilter
});

const mapDispatchToProps = () => {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationPlanDetails);
