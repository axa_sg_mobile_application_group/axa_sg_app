import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import SummaryScreen from "../components/SummaryScreen";

const { CLIENT, PRE_APPLICATION } = REDUCER_TYPES;

/**
 * SummaryScreen
 * @description Application Summary page.
 * @requires SummaryScreen - SummaryScreen UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  applicationsList: state[PRE_APPLICATION].applicationsList,
  pdaMembers: state[PRE_APPLICATION].pdaMembers,
  selectedAppListFilter: state[PRE_APPLICATION].component.selectedAppListFilter,
  recommendCompleted: state[PRE_APPLICATION].recommendation.completed,
  recommendChoiceList: state[PRE_APPLICATION].recommendation.choiceList,
  selectedIdList: state[PRE_APPLICATION].component.selectedIdList,
  selectedBundleId: state[PRE_APPLICATION].component.selectedBundleId
});

const mapDispatchToProps = dispatch => ({
  getApplicationsList: bundleId => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].SAGA_GET_APPLICATIONS,
      bundleId
    });
    dispatch(
      NavigationActions.navigate({
        routeName: "Applications"
      })
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SummaryScreen);
