import { connect } from "react-redux";
import * as _ from "lodash";
import { REDUCER_TYPES, ACTION_LIST } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import RecommendationScreen from "../components/RecommendationScreen";

/**
 * RecommendationScreen
 * @description Recommendation page.
 * @requires RecommendationScreen - RecommendationScreen UI
 * */

const findApplicationItem = state =>
  _.find(state[REDUCER_TYPES.PRE_APPLICATION].applicationsList, item => {
    const { selectedQuotId } = state[REDUCER_TYPES.RECOMMENDATION].component;
    return item.type === "quotation"
      ? item.id === selectedQuotId
      : item.quotationDocId === selectedQuotId;
  });

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  currentStep: state[REDUCER_TYPES.RECOMMENDATION].component.currentStep,
  completedStep: state[REDUCER_TYPES.RECOMMENDATION].component.completedStep,
  showDetails: state[REDUCER_TYPES.RECOMMENDATION].component.showDetails,
  detailsValue: !_.isEmpty(findApplicationItem(state))
    ? findApplicationItem(state)
    : {}
});

const mapDispatchToProps = dispatch => ({
  hideRecommendationDetails: () => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationDetails({
      dispatch,
      newShowDetails: false
    });
  },
  updateRecommendationStep: newCurrentStep => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationStep({
      dispatch,
      newCurrentStep
    });
  },
  closeRecommendation: () => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].closeRecommendation({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecommendationScreen);
