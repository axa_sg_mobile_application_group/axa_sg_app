import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationFormDynamic from "../components/ApplicationFormDynamic";

const { APPLICATION, OPTIONS_MAP } = REDUCER_TYPES;
/**
 * ApplicationFormDynamic
 * @description ApplicationFormDynamic in Application page.
 * @requires ApplicationFormDynamic - ApplicationFormDynamic UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  template: state[REDUCER_TYPES.APPLICATION].template,
  application: state[APPLICATION].application,
  error: state[REDUCER_TYPES.APPLICATION].error,
  optionsMap: state[OPTIONS_MAP],
  selectedSectionKey: state[APPLICATION].component.selectedSectionKey
});

const mapDispatchToProps = dispatch => ({
  updateApplicationFormValues: ({ id, path, pathValue, iTemplate }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_APPLICATION_FORM_VALUES,
      path,
      pathValue,
      id,
      iTemplate
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationFormDynamic);
