import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

import ApplicationShieldPlanDetails from "../components/ApplicationShieldPlanDetails";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * ApplicationShieldPlanDetails
 * @description Normal Item ApplicationList inside Application Summary page.
 * @requires ApplicationShieldPlanDetails - ApplicationShieldPlanDetails UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  applicationPlanDetailsData: state[REDUCER_TYPES.APPLICATION].application,
  selectedAppListFilter:
    state[REDUCER_TYPES.PRE_APPLICATION].component.selectedAppListFilter,
  availableFunds: state[REDUCER_TYPES.QUOTATION].availableFunds
});

const mapDispatchToProps = dispatch => ({
  getFundList: (quotationId, invOpt, paymentMethod) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGE_APPLICATION_FORM_PLAN_DETAILS_GET_FUND_LIST,
      quotationId,
      invOpt,
      paymentMethod
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationShieldPlanDetails);
