import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import ApplicationsListFilter from "../components/ApplicationsListFilter";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

/**
 * ApplicationsListFilter
 * @description ApplicationsListFilter in Application Summary page.
 * @requires ApplicationsListFilter - ApplicationsListFilter UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  applicationsList: state[REDUCER_TYPES.PRE_APPLICATION].applicationsList,
  selectedAppListFilter:
    state[REDUCER_TYPES.PRE_APPLICATION].component.selectedAppListFilter
});

const mapDispatchToProps = dispatch => ({
  updateUiAppListFilter: selectedAppListFilter => {
    ACTION_LIST[REDUCER_TYPES.PRE_APPLICATION].updateUiAppListFilter({
      dispatch,
      selectedAppListFilter
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationsListFilter);
