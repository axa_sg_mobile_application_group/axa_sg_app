import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import Budget from "../components/Budget";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * Budget
 * @description Budget in Recommendation page.
 * @requires Budget - Budget UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  value: state[REDUCER_TYPES.RECOMMENDATION].budget,
  error: state[REDUCER_TYPES.RECOMMENDATION].error.budget,
  disabled: state[REDUCER_TYPES.RECOMMENDATION].component.disabled
});

const mapDispatchToProps = dispatch => ({
  validateBudget: () => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].validateBudget({
      dispatch
    });
  },
  updateBudgetData: newValue => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateBudgetData({
      dispatch,
      newValue
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Budget);
