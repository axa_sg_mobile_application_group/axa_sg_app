import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import ApplicationItemShieldDetails from "../components/ApplicationItemShieldDetails";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * ApplicationItemShieldDetails
 * @description Shield Item ApplicationList inside Application Summary page.
 * @requires ApplicationItemShieldDetails - ApplicationItemShieldDetails UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationItemShieldDetails);
