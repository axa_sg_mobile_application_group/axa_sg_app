import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import ApplicationsList from "../components/ApplicationsList";

const { PRE_APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationsList
 * @description ApplicationsList in Application Summary page.
 * @requires ApplicationsList - ApplicationsList UI
 * */
const mapStateToProps = state => ({
  applicationsList: state[PRE_APPLICATION].applicationsList,
  selectedAppListFilter: state[PRE_APPLICATION].component.selectedAppListFilter,
  selectedIdList: state[PRE_APPLICATION].component.selectedIdList
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationsList);
