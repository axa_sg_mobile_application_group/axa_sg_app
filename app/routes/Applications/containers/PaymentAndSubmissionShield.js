import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import PaymentAndSubmissionShield from "../components/PaymentAndSubmissionShield";

/**
 * PaymentAndSubmissionShield
 * @description PaymentAndSubmissionShield in Application Summary page.
 * @requires PaymentAndSubmissionShield - PaymentAndSubmissionShield UI
 * */
const mapStateToProps = state => ({
  onlinePaymentStatus:
    state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].onlinePaymentStatus
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentAndSubmissionShield);
