import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import ApplicationItemDetails from "../components/ApplicationItemDetails";

const { OPTIONS_MAP } = REDUCER_TYPES;

/**
 * ApplicationItemDetails
 * @description Normal Item ApplicationList inside Application Summary page.
 * @requires ApplicationItemDetails - ApplicationItemDetails UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationItemDetails);
