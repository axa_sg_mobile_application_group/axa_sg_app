import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import {
  TEXT_STORE,
  PAYMENT as APP_PAYMENT,
  LOGIN
} from "../../../constants/REDUCER_TYPES";
import { setLastDataSyncTime } from "../../../actions/login";
import Submission from "../components/PaymentAndSubmission/submission";
import { submitApplication } from "../../../apis/submission";
import { dataSync } from "../../../utilities/DataManager";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
/**
 * Submission
 * @description Submission in Application Summary page.
 * @requires Submission - Submission UI
 * */
const mapStateToProps = state => ({
  environment: state[LOGIN].environment,
  isOnlineAuthCompleted: state[APP_PAYMENT].isOnlineAuthCompleted,
  application: state[REDUCER_TYPES.APPLICATION].application,
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  isHandlingListener: state[LOGIN].isHandlingListener,
  apiPage: state[REDUCER_TYPES.APPLICATION].component.apiPage,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  error: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error,
  textStore: state[TEXT_STORE],
  agent: state[REDUCER_TYPES.AGENT],
  agentCode: state[LOGIN].agentCode,
  authToken: state[LOGIN].authToken,
  lastDataSyncTime: state[LOGIN].lastDataSyncTime
});

const mapDispatchToProps = dispatch => ({
  disableEappButton: newIsDisableEappButton => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].IS_DISABLE_EAPP_BUTTON,
      newIsDisableEappButton
    });
  },
  getApplicationList: callback => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST,
      callback
    });
  },
  getSubmission: docId => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_SUBMISSION,
      docId
    });
  },
  submitPaymentAndSubmission: ({
    application,
    policyDocument,
    appPdf,
    bundle,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_SUBMIT_PAYMENT_AND_SUBMISSION,
      application,
      policyDocument,
      appPdf,
      bundle,
      callback
    });
  },
  callApiSubmitApplication: ({ docId, lang, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_SUBMIT_APPLICATION,
      docId,
      lang,
      api: submitApplication,
      callback
    });
  },
  dataSync: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC,
      api: dataSync
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  },
  setApiResponsePage: ({ apiPage }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].UPDATE_API_PAGE,
      newApiPage: apiPage
    });
  },
  getApplicationsAfterSubmission: ({ callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_GET_APPLICATIONS_AFTER_SUBMISSION,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Submission);
