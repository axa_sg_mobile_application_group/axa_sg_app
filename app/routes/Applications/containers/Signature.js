import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import Signature from "../components/Signature";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { APPLICATION, SIGNATURE, OPTIONS_MAP } = REDUCER_TYPES;
/**
 * Signature
 * @description Application Summary page.
 * @requires Signature - Signature UI
 * */
const mapStateToProps = state => ({
  application: state[APPLICATION].application,
  signature: state[SIGNATURE].signature,
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  isAlertDelayed: state[APPLICATION].component.isAlertDelayed,
  isCrossAgeAlertShow: state[APPLICATION].component.isCrossAgeAlertShow
});

const mapDispatchToProps = dispatch => ({
  getSignature: ({ applicationId, failCallback, successCallback }) => {
    dispatch({
      type: ACTION_TYPES[SIGNATURE].SAGA_GET_SIGNATURE,
      docId: applicationId,
      failCallback,
      successCallback
    });
  },
  saveSignedPdf: ({ docId, attId, pdfData, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[SIGNATURE].SAGA_SAVE_SIGNED_PDF,
      docId,
      attId,
      pdfData,
      isShield,
      callback
    });
  },
  switchPdf: newSelectedPdfIdx => {
    dispatch({
      type: ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF,
      newSelectedPdfIdx
    });
  },
  updateIsAlertDelayed: newIsAlertDelayed => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_ALERT_DELAYED,
      newIsAlertDelayed
    });
  },
  saveForm: ({ currentStep, nextStep, actionType, genPDF, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAVE_APPLICATION_FORM_BEFORE,
      currentStep,
      nextStep,
      actionType,
      genPDF,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signature);
