import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationFormPersonalDetails from "../components/ApplicationFormPersonalDetails";

const { OPTIONS_MAP, APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationFormPersonalDetails
 * @description ApplicationFormPersonalDetails in Application page.
 * @requires ApplicationFormPersonalDetails - ApplicationFormPersonalDetails UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE],
  application: state[APPLICATION].application,
  template: state[APPLICATION].template,
  error: state[APPLICATION].error,
  selectedSectionKey: state[APPLICATION].component.selectedSectionKey
});

const mapDispatchToProps = dispatch => ({
  updateApplicationFormPersonalDetails: ({
    newApplication,
    validateObj,
    triggerValidate,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
      newApplication,
      validateObj,
      triggerValidate,
      callback
    });
  },
  updateAddress: ({
    fileName,
    postalCode,
    targetProfile,
    fieldType,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_UPDATE_PERSONAL_DETAILS_ADDRESS,
      fileName,
      postalCode,
      targetProfile,
      fieldType,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationFormPersonalDetails);
