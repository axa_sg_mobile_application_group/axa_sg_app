import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationDeleteButton from "../components/ApplicationDeleteButton";

/**
 * ApplicationDeleteButton
 * @description Button to delete Applications.
 * @requires ApplicationDeleteButton - ApplicationDeleteButton UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

export default connect(mapStateToProps)(ApplicationDeleteButton);
