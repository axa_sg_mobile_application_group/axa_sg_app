import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import PaymentAndSubmission from "../components/PaymentAndSubmission";
import { setLastDataSyncTime } from "../../../actions/login";
/**
 * PaymentAndSubmission
 * @description PaymentAndSubmission in Application Summary page.
 * @requires PaymentAndSubmission - PaymentAndSubmission UI
 * */
const mapStateToProps = state => ({
  application: state[REDUCER_TYPES.APPLICATION].application,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  error: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error,
  textStore: state[TEXT_STORE],
  onlinePaymentStatus:
    state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].onlinePaymentStatus
});

const mapDispatchToProps = dispatch => ({
  updatePayment: ({ path, newValue, docId, validateObj, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_UPDATE_PAYMENT_AND_SUBMISSION,
      path,
      newValue,
      docId,
      validateObj,
      callback
    });
  },
  submitPaymentAndSubmission: ({ docId }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_SUBMIT_PAYMENT_AND_SUBMISSION,
      docId
    });
  },
  dataSync: (application, callback) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC,
      application,
      callback
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentAndSubmission);
