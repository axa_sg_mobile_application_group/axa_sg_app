import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN, TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import Payment from "../components/PaymentAndSubmission/payment";
import { setLastDataSyncTime } from "../../../actions/login";

/**
 * Payment
 * @description Payment in Application Summary page.
 * @requires Payment - Payment UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[REDUCER_TYPES.OPTIONS_MAP],
  application: state[REDUCER_TYPES.APPLICATION].application,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  error: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error,
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  getPayment: docId => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT,
      docId
    });
  },
  updatePayment: ({ path, newValue, validateObj, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_UPDATE_PAYMENT_AND_SUBMISSION,
      path,
      newValue,
      validateObj,
      callback
    });
  },
  dataSync() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].PRE_CEHCK_ON_DATA_SYNC
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Payment);
