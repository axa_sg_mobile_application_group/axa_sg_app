import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import ApplicationEmailSection from "../components/ApplicationEmailSection";
import { LOGIN, PAYMENT, TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";

const { CLIENT, PRE_APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationEmailSection
 * @description ApplicationEmailSection page.
 * @requires ApplicationEmailSection - ApplicationEmailSection UI
 * */
const mapStateToProps = state => ({
  profile: state[CLIENT].profile,
  preApplication: state[PRE_APPLICATION],
  environment: state[LOGIN].environment,
  isHandlingListener: state[LOGIN].isHandlingListener,
  preApplicationEmail: state[PRE_APPLICATION].preApplicationEmail,
  tab: state[PRE_APPLICATION].preApplicationEmail.tab,
  options: state[PRE_APPLICATION].preApplicationEmail.options,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  openEmailDialog: callback => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].OPEN_EMAIL_DIALOG,
      callback
    });
  },
  checkBoxOnPress: options => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].REPORT_ON_CHANGE,
      options
    });
  },
  sendEmail: callback => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].SEND_EMAIL,
      callback
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  checkValidDevice: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_VALID_DEVICE,
      callback
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  tabOnPress: tab => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].TAB_ON_CHANGE,
      tab
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationEmailSection);
