import { connect } from "react-redux";
import { StackActions } from "react-navigation";
import { ACTION_LIST, ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationItemHeader from "../components/ApplicationItemHeader";

const { PRE_APPLICATION, QUOTATION, APPLICATION, CLIENT, FNA } = REDUCER_TYPES;

/**
 * ApplicationItemHeader
 * @description Header of ApplicationItemDetails / ApplicationItemShieldDetails in ApplicationsList in Application Summary page.
 * @requires ApplicationItemHeader - ApplicationItemHeader UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  selectModeOn: state[PRE_APPLICATION].component.selectModeOn,
  selectedIdList: state[PRE_APPLICATION].component.selectedIdList,
  selectedAppListFilter: state[PRE_APPLICATION].component.selectedAppListFilter,
  recommendCompleted: state[PRE_APPLICATION].recommendation.completed,
  recommendChoiceList: state[PRE_APPLICATION].recommendation.choiceList,
  isFaChannel: state[PRE_APPLICATION].agentChannel === "FA",
  isFnaInvalid: state[FNA].isFnaInvalid
});

const mapDispatchToProps = dispatch => ({
  getSupportingDocument: ({ isShield, appId, appStatus, callback }) => {
    dispatch({
      type: isShield
        ? ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
            .SAGA_GET_SUPPORTING_DOCUMENT_DATA_SHIELD
        : ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
            .SAGA_GET_SUPPORTING_DOCUMENT_DATA,
      appId,
      appStatus,
      callback
    });
  },
  updateApplicationStep: ({ nextStep, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_EAPP_STEP,
      nextStep,
      isShield,
      callback
    });
  },
  updateUiSelectedList: changeData => {
    ACTION_LIST[PRE_APPLICATION].updateUiSelectedList({
      dispatch,
      changeData
    });
  },
  updateRecommendChoiceList: choiceData => {
    ACTION_LIST[PRE_APPLICATION].updateRecommendChoiceList({
      dispatch,
      choiceData
    });
  },
  openProposal: ({ quotationId, callback }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].SAGA_VIEW_PROPOSAL,
      quotationId,
      callback
    });
  },
  requoteInvalid: ({ quotationId, confirm, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE_INVALID,
      quotationId,
      confirm,
      callback
    });
  },
  routeToProposalUI: () => {
    dispatch(StackActions.push({ routeName: "ProposalUI" }));
  },
  checkBeforeApply: ({ pCid, iCids, callback }) => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].CHECK_BEFORE_APPLY,
      pCid,
      iCids,
      callback
    });
  },
  applyApplicationForm: ({ quotationId, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_APPLY_APPLICATION_FORM,
      quotationId,
      isShield,
      callback
    });
  },
  continueApplicationForm: ({ applicationId, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
      applicationId,
      cleanData: true,
      callback
    });
  },
  continueApplicationFormShield: ({ applicationId, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_CONTINUE_APPLICATION_FORM_SHIELD,
      applicationId,
      cleanData: true,
      callback
    });
  },
  comfirmApplicationPaid: ({ applicationId, isShield }) => {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].CONFIRM_APPLICATION_PAID,
      applicationId,
      isShield
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationItemHeader);
