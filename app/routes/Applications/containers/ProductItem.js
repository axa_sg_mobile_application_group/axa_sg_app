import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_LIST } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProductItem from "../components/ProductItem";

const { getQuotation } = ACTION_LIST[REDUCER_TYPES.QUOTATION];

/**
 * ProductItem
 * @description ProductItem in Application Summary page.
 * @requires ProductItem - ProductItem UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  getQuotation({ productID, openQuotationFunction }) {
    getQuotation({
      dispatch,
      productID,
      openQuotationFunction
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductItem);
