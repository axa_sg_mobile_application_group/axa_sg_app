import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import * as _ from "lodash";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import ClientFormContact from "../../../../containers/ClientFormContact";
import EABButton from "../../../../components/EABButton";
import EABSectionList from "../../../../components/EABSectionList";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";
import Theme from "../../../../theme";

const { CLIENT } = REDUCER_TYPES;

/**
 * MultiClientProfile
 * @description MultiClientProfile page.
 * @param {function} closeDialog - close dialog function
 * */

class MultiClientProfile extends PureComponent {
  constructor(props) {
    super(props);

    this.getIsProposer = this.getIsProposer.bind(this);
    this.prepareSegmentedControlArray = this.prepareSegmentedControlArray.bind(
      this
    );
    this.prepareSectionListOptions = this.prepareSectionListOptions.bind(this);
    this.renderTitleBar = this.renderTitleBar.bind(this);
    this.state = {
      selectedCid:
        props.hasErrorList.length > 0 ? props.hasErrorList[0].cid : "",
      activatedKey: "profile",
      isLoading: false
    };
  }

  componentDidMount() {
    const { loadClient /* hasErrorList */ } = this.props;
    const { selectedCid } = this.state;
    const isProposer = this.getIsProposer(selectedCid);

    loadClient({
      cid: selectedCid,
      profile: this.getProfile(selectedCid),
      isFNA: !this.isFaChannel && isProposer,
      isProposerMissing: isProposer,
      isInsuredMissing: !isProposer
      // 36575 point1 -> dont need to do this enhancement
      // callback: () => {
      //   let profileError = null;
      //   hasErrorList.some(list => {
      //     if (list.cid === selectedCid) {
      //       profileError = list;
      //     }
      //     return profileError;
      //   });
      //   if (profileError.errorFields.indexOf("isEmailError") > -1) {
      //     this.setState({
      //       activatedKey: "contact"
      //     });
      //   }
      // }
    });
  }

  /**
   * @description indicate agent channel is FA channel
   * @return {boolean} isFaChannel
   * */
  get isFaChannel() {
    const { agentChannel } = this.props;
    return agentChannel === "FA";
  }

  /**
   * @description indicate cid is proposer's cid
   * @return {boolean} isProposer
   * */
  getIsProposer(cid) {
    const { profile } = this.props;
    return cid === profile.cid;
  }

  getProfile(cid) {
    const { profile, dependantProfiles } = this.props;
    let loadProfile = {};
    if (profile.cid === cid) {
      loadProfile = profile;
    } else {
      loadProfile = dependantProfiles[cid];
    }
    return loadProfile;
  }

  prepareSegmentedControlArray(language) {
    const { textStore } = this.props;
    return [
      {
        key: "profile",
        title: EABi18n({
          path: "ad.profile.tabTitle",
          language,
          textStore
        }),
        isShowingBadge: false,
        onPress: () => {
          this.setState({ activatedKey: "profile" });
        }
      },
      {
        key: "contact",
        title: EABi18n({
          path: "ad.contact.tabTitle",
          language,
          textStore
        }),
        isShowingBadge: false,
        onPress: () => {
          this.setState({ activatedKey: "contact" });
        }
      }
    ];
  }

  prepareSectionListOptions() {
    const { hasErrorList } = this.props;
    const { selectedCid } = this.state;

    const profileOptions = _.map(hasErrorList, clientHasError => ({
      key: clientHasError.cid,
      id: clientHasError.cid,
      title: _.get(this.getProfile(clientHasError.cid), "fullName", ""),
      selected: selectedCid === clientHasError.cid,
      completed: !clientHasError.hasError
    }));

    return [
      {
        key: "section",
        data: profileOptions,
        title: ""
      }
    ];
  }

  isSaveDisable() {
    const { hasErrorList } = this.props;
    let disable = false;
    hasErrorList.some(list => {
      if (list.hasError) {
        disable = true;
      }
      return disable;
    });
    return disable;
  }

  renderTitleBar() {
    const {
      closeDialog,
      saveClient,
      cleanMultiClientProfile,
      applyApplicationForm
    } = this.props;
    return (
      <ContextConsumer>
        {({
          dialogQuotationId,
          dialogIsShield,
          showApplicationDialog,
          setEappLoading
        }) => (
          <View style={styles.titleBar}>
            <View style={styles.titleBarViewLeft}>
              <EABButton
                style={styles.titleBarButtonLeft}
                onPress={() => {
                  cleanMultiClientProfile();
                  closeDialog();
                }}
              >
                <TranslatedText
                  style={styles.titleBarButtonTitle}
                  path="button.cancel"
                />
              </EABButton>
            </View>
            <View style={styles.titleBarViewMiddle}>
              <TranslatedText
                style={styles.titleBarTitle}
                path="ad.profile.tabTitle"
              />
            </View>
            <View style={styles.titleBarViewRight}>
              <EABButton
                style={styles.titleBarButtonRight}
                isDisable={this.isSaveDisable()}
                onPress={() => {
                  this.setState({ isLoading: true }, () => {
                    saveClient(() => {
                      setEappLoading(true);
                      this.setState({ isLoading: false }, () => {
                        cleanMultiClientProfile();
                        closeDialog();
                        applyApplicationForm({
                          quotationId: dialogQuotationId,
                          isShield: dialogIsShield,
                          callback: () => {
                            setEappLoading(false, () => {
                              setTimeout(() => {
                                showApplicationDialog();
                              }, 300);
                            });
                          }
                        });
                      });
                    });
                  });
                }}
              >
                <TranslatedText
                  style={styles.titleBarButtonTitle}
                  path="button.save"
                />
              </EABButton>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  render() {
    const { saveClient, loadClient /* hasErrorList */ } = this.props;
    const { isLoading, activatedKey /* initKey, selectedCid */ } = this.state;
    // 36575 point1 -> dont need to do this enhancement
    // (() => {
    //   let profileError = null;
    //   hasErrorList.some(list => {
    //     if (list.cid === selectedCid) {
    //       profileError = list;
    //     }
    //     return profileError;
    //   });
    //   if (initKey) {
    //     if (
    //       profileError.errorFields.length === 1 &&
    //       profileError.errorFields.indexOf("isEmailError") > -1
    //     ) {
    //       this.setState({
    //         activatedKey: "contact",
    //         initKey: false
    //       });
    //     } else {
    //       this.setState({
    //         activatedKey: "profile",
    //         initKey: false
    //       });
    //     }
    //   }
    // })();
    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>{this.renderTitleBar()}</View>
            <View style={styles.profileContainer}>
              <View style={styles.sectionListView}>
                <EABSectionList
                  sections={this.prepareSectionListOptions()}
                  showCompleteIcon
                  onPress={id => {
                    const isProposer = this.getIsProposer(id);

                    this.setState({ isLoading: true }, () => {
                      saveClient(() => {
                        this.setState(
                          {
                            isLoading: false,
                            // 36575 point1 -> dont need to do this enhancement
                            // initKey: true,
                            activatedKey: "profile",
                            selectedCid: id
                          },
                          () => {
                            loadClient({
                              cid: id,
                              profile: this.getProfile(id),
                              isFNA: !this.isFaChannel && isProposer,
                              isProposerMissing: isProposer,
                              isInsuredMissing: !isProposer
                            });
                          }
                        );
                      });
                    });
                  }}
                />
              </View>
              <View style={styles.KeyboardAvoidingViewWrapper}>
                <View style={styles.profileButtonContentContainer}>
                  <EABSegmentedControl
                    testID="segmentedProfileCategory"
                    style={styles.profileButton}
                    segments={this.prepareSegmentedControlArray(language)}
                    activatedKey={activatedKey}
                  />
                </View>
                <ScrollView style={styles.scrollView}>
                  {activatedKey === "profile" ? (
                    <ClientFormProfile />
                  ) : (
                    <ClientFormContact />
                  )}
                </ScrollView>
              </View>
            </View>
            <EABHUD isOpen={isLoading} />
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

MultiClientProfile.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  agentChannel: PropTypes.string.isRequired,
  closeDialog: PropTypes.func.isRequired,
  loadClient: PropTypes.func.isRequired,
  applyApplicationForm: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired,
  cleanMultiClientProfile: PropTypes.func.isRequired,
  hasErrorList: PropTypes.arrayOf(
    PropTypes.shape({
      cid: PropTypes.string.isRequired,
      hasError: PropTypes.bool.isRequired
    })
  ).isRequired
};
export default MultiClientProfile;
