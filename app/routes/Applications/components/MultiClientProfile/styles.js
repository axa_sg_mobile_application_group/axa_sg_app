import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    marginBottom: Theme.alignmentXXL
  },
  header: {
    flexBasis: "auto",
    paddingTop: Theme.statusBarHeight + Theme.alignmentS,
    alignItems: "center",
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: Theme.alignmentM,
    alignItems: "center",
    width: "100%"
  },
  titleBarViewLeft: {
    alignItems: "flex-start",
    flex: 1
  },
  titleBarViewRight: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end",
    flex: 1
  },
  titleBarViewMiddle: {
    alignItems: "center",
    flex: 1
  },
  titleBarTitle: {
    ...Theme.title
  },
  titleBarButtonLeft: {
    marginLeft: Theme.alignmentXL
  },
  titleBarButtonRight: {
    marginRight: Theme.alignmentXL
  },
  titleBarButtonTitle: {
    ...Theme.textButtonLabelNormalAccent
  },
  sectionListView: {
    flexBasis: 312
  },
  KeyboardAvoidingViewWrapper: {
    flex: 1,
    width: "100%",
    overflow: "hidden"
  },
  KeyboardAvoidingView: {
    width: "100%",
    height: "100%"
  },
  profileContainer: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  profileButtonContentContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  profileButton: {
    marginTop: Theme.alignmentS,
    marginBottom: Theme.alignmentS
  }
});
