import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, ScrollView } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import icCompleted from "../../../../assets/images/icCompleted.png";
import icIncomplete from "../../../../assets/images/icIncomplete.png";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import DynamicViews from "../../../../components/DynamicViews";

import styles from "./styles";

/**
 * ApplicationFormDynamic
 * @description ApplicationFormDynamic page.
 * @param {object} values - data for ApplicationForm page
 * */
class ApplicationFormDynamic extends PureComponent {
  static getTabsTemplate({ template, menuSection, menuItem }) {
    /**
     * template -> items[0] (section "Application")
     * -> items[0] (menu in left side)
     * -> items[menuSection] (menuSection, there are three secions)
     * -> items[menuItem] (menuItem)
     * -> items[0] (tabs) -> items[0] (proposer)
     */
    const menuTemplate =
      // Shield case & non-Shield case
      _.toUpper(_.get(template, "items[0].type", "")) === "MENU"
        ? _.get(template, "items[0]")
        : _.get(template, "items[0].items[0]");

    return _.get(
      menuTemplate,
      `items[${menuSection}].items[${menuItem}].items[0]`,
      {}
    );
  }

  static checkProposerTemplateExist({ template, menuSection, menuItem }) {
    const tabsTemplate = ApplicationFormDynamic.getTabsTemplate({
      template,
      menuSection,
      menuItem
    });
    return _.some(tabsTemplate.items, item => item.subType === "proposer");
  }

  static checkInsuredTemplateExist({ template, menuSection, menuItem }) {
    const tabsTemplate = ApplicationFormDynamic.getTabsTemplate({
      template,
      menuSection,
      menuItem
    });
    return _.some(tabsTemplate.items, item => item.subType === "insured");
  }

  constructor(props) {
    super(props);

    // TODO: put selectedProfile into reducer
    this.state = {
      selectedProfile: ""
    };
    this.scrollView = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    const { template, menuSection, menuItem } = props;
    const { selectedProfile } = state;
    const isPhExist = ApplicationFormDynamic.checkProposerTemplateExist({
      template,
      menuSection,
      menuItem
    });
    const isLaExist = ApplicationFormDynamic.checkInsuredTemplateExist({
      template,
      menuSection,
      menuItem
    });
    if (
      selectedProfile === "" ||
      (selectedProfile === "proposer" && !isPhExist) ||
      (selectedProfile.indexOf("insured") >= 0 && !isLaExist)
    ) {
      if (isPhExist) {
        return {
          selectedProfile: "proposer",
          selectedSectionKey: props.selectedSectionKey
        };
      }
      if (!isPhExist && isLaExist) {
        return {
          selectedProfile: "insured[0]",
          selectedSectionKey: props.selectedSectionKey
        };
      }
    }
    if (props.selectedSectionKey !== state.selectedSectionKey) {
      return {
        selectedProfile: isPhExist ? "proposer" : "insured[0]",
        selectedSectionKey: props.selectedSectionKey
      };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selectedProfile !== this.state.selectedProfile ||
      prevProps.selectedSectionKey !== this.props.selectedSectionKey
    ) {
      this.scrollView.current.scrollTo({ x: 0, y: 0, animated: false });
    }
  }

  get hasProposerTemplate() {
    const { template, menuSection, menuItem } = this.props;
    return ApplicationFormDynamic.checkProposerTemplateExist({
      template,
      menuSection,
      menuItem
    });
  }

  get hasInsuredTemplate() {
    const { template, menuSection, menuItem } = this.props;
    return ApplicationFormDynamic.checkInsuredTemplateExist({
      template,
      menuSection,
      menuItem
    });
  }

  genProfileButton({ blockIdArr }) {
    const { application, error } = this.props;
    const segmentedControlArray = [];
    const values = _.get(application, "applicationForm.values");
    const insuredHasErrorArray = [];

    const proposerPath = "applicationForm.values.proposer";
    const insuredPath = "applicationForm.values.insured";

    const proposerHasError = _.some(blockIdArr, blockId => {
      const errorFields = _.get(error, `${proposerPath}.${blockId}`);
      if (!_.isUndefined(errorFields)) {
        return _.some(errorFields, errorValue => _.get(errorValue, "hasError"));
      }
      return false;
    });

    const insuredError = _.get(error, insuredPath, []);
    _.each(insuredError, (itemInsuredError, errorIndex) => {
      insuredHasErrorArray.push(
        _.some(blockIdArr, blockId => {
          const errorFields = _.get(insuredError, `${errorIndex}.${blockId}`);
          if (!_.isUndefined(errorFields)) {
            return _.some(errorFields, errorValue =>
              _.get(errorValue, "hasError")
            );
          }
          return false;
        })
      );
    });

    if (this.hasProposerTemplate) {
      segmentedControlArray.push({
        key: "proposer",
        title: _.get(values, "proposer.personalInfo.firstName", ""),
        isShowingBadge: true,
        source: proposerHasError ? icIncomplete : icCompleted,
        onPress: () => {
          this.setState({ selectedProfile: "proposer" });
        }
      });
    }

    if (this.hasInsuredTemplate) {
      values.insured.forEach((value, index) => {
        segmentedControlArray.push({
          key: `insured[${index}]`,
          title: _.get(value, "personalInfo.firstName", ""),
          isShowingBadge: true,
          source: _.get(insuredHasErrorArray, index)
            ? icIncomplete
            : icCompleted,
          onPress: () => {
            this.setState({ selectedProfile: `insured[${index}]` });
          }
        });
      });
    }

    return (
      <EABSegmentedControl
        style={[styles.profileButton]}
        segments={segmentedControlArray}
        activatedKey={this.state.selectedProfile}
      />
    );
  }

  render() {
    const {
      updateApplicationFormValues,
      template,
      application,
      menuSection,
      menuItem,
      error
    } = this.props;

    if (!application || _.isEmpty(application)) {
      return null;
    }

    const { selectedProfile } = this.state;
    const tabsTemplate = ApplicationFormDynamic.getTabsTemplate({
      template,
      menuSection,
      menuItem
    });
    let selectedProfileTemplate = null;

    if (selectedProfile === "proposer") {
      selectedProfileTemplate = _.find(
        tabsTemplate.items,
        item => item.subType === "proposer"
      );
    } else if (selectedProfile.indexOf("insured") >= 0) {
      const insuredTemplate = _.filter(
        tabsTemplate.items,
        item => item.subType === "insured"
      );
      selectedProfileTemplate = _.get(
        insuredTemplate,
        `${_.replace(selectedProfile, "insured", "")}`
      );
    }

    const profilePath = `applicationForm.values.${selectedProfile}`;

    return (
      <View style={styles.container}>
        <View style={styles.profileButtonContainer}>
          <ScrollView
            horizontal
            scrollEnabled={
              _.get(application, "applicationForm.values.insured", []).length >
              2
            }
            showsHorizontalScrollIndicator={false}
            style={styles.profileButtonScrollView}
            contentContainerStyle={styles.profileButtonContentContainer}
          >
            <View style={styles.profileButtonView}>
              {this.genProfileButton({
                blockIdArr: selectedProfileTemplate
                  ? _.map(selectedProfileTemplate.items, item => item.id || "")
                  : []
              })}
            </View>
          </ScrollView>
        </View>
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContent}
          showsVerticalScrollIndicator={false}
          ref={this.scrollView}
        >
          {selectedProfileTemplate ? (
            <DynamicViews
              template={selectedProfileTemplate}
              rootValues={application}
              path={profilePath}
              error={error}
              onUpdateValues={({ id, value, path, iTemplate }) =>
                updateApplicationFormValues({
                  id,
                  path,
                  pathValue: value,
                  iTemplate
                })
              }
            />
          ) : null}

          <View style={styles.footerHeight} />
        </ScrollView>
      </View>
    );
  }
}

ApplicationFormDynamic.propTypes = {
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  updateApplicationFormValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  selectedSectionKey: PropTypes.string.isRequired,
  menuSection: PropTypes.number.isRequired,
  menuItem: PropTypes.number.isRequired
};

ApplicationFormDynamic.defaultProps = {};

export default ApplicationFormDynamic;
