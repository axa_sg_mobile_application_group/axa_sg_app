import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flexBasis: 312
  },
  detailView: {
    flex: 1,
    alignItems: "center"
  }
});
