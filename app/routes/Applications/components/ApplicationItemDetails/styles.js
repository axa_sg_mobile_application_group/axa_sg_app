import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  tableSectionColumn1: {
    flex: 2,
    flexBasis: 260,
    alignItems: "flex-start"
  },
  tableSectionColumn2: {
    flex: 1,
    flexBasis: 70,
    alignItems: "flex-end"
  },
  tableSectionColumn3: {
    flex: 1,
    flexBasis: 85,
    alignItems: "flex-end"
  },
  tableSectionColumn4: {
    flex: 1,
    flexBasis: 130,
    alignItems: "flex-end"
  },
  tableSectionColumn5: {
    flex: 1,
    flexBasis: 130,
    alignItems: "flex-end"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel,
    textAlign: "right"
  },
  tableSectionHeader: {
    backgroundColor: Theme.brandSuperTransparent
  },
  tableSectionHeaderTitle: {
    ...Theme.captionBrand
  },
  tableSection: {
    backgroundColor: Theme.white
  },
  tableSectionTitle: {
    ...Theme.subheadPrimary
  },
  tableFooterColumn1: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  tableFooterColumn2: {
    flex: 4,
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  tableFooterDateView: {
    backgroundColor: Theme.white
  },
  tableFooterValueView: {
    backgroundColor: Theme.white,
    flexDirection: "row",
    justifyContent: "flex-end",
    flexWrap: "wrap"
  },
  tableFooterColumnDate: {
    ...Theme.subheadSecondary,
    textAlign: "left"
  },
  tableFooterDataView: {
    flexDirection: "row"
  },
  tableFooterDataTitle: {
    ...Theme.subheadSecondary,
    textAlign: "right",
    marginLeft: Theme.alignmentM,
    marginRight: Theme.alignmentL
  },
  tableFooterDataValue: {
    ...Theme.subheadPrimary,
    textAlign: "right",
    flexBasis: 130
  }
});
