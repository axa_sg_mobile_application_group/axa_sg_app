import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  item: {
    marginRight: Theme.alignmentXL
  },
  productImageView: {
    width: 220,
    height: 108,
    backgroundColor: Theme.white
  },
  productAddIconView: {
    position: "absolute"
  },
  productAddIcon: {
    marginTop: Theme.alignmentXS,
    marginLeft: 188,
    marginRight: Theme.alignmentXS
  },
  productImage: {
    width: 220,
    height: 108
  },
  productText: {
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentXL
  }
});
