import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flex: 0,
    flexBasis: 312
  },
  mainWrapper: {
    flex: 1,
    height: "100%"
  },
  scrollView: {
    height: "100%",
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  }
});
