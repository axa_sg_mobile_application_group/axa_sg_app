import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    marginLeft: Theme.alignmentXL
  },
  text: {
    ...Theme.textButtonLabelNormalAccent
  }
});
