import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  scrollViewContentSubview: {
    width: 880
  },
  otherPlanDetailsView: {
    marginTop: Theme.alignmentXL,
    alignItems: "flex-start"
  },
  planHeader: {
    ...Theme.captionBrand,
    paddingBottom: 30
  },
  sectionHeader: {
    paddingBottom: 2,
    borderBottomWidth: 2,
    borderColor: Theme.brand
  },
  baseProductNameView: {
    marginBottom: Theme.alignmentXL,
    paddingBottom: 2,
    borderBottomWidth: 2,
    borderColor: Theme.brand
  },
  sectionTitle: {
    ...Theme.captionBrand
  },
  sectionDataView: {
    marginTop: Theme.alignmentXL,
    flexDirection: "column"
  },
  otherPlanDetailsDataView: {
    flexDirection: "row"
  },
  otherPlanDetailsDataTitle: {
    ...Theme.bodyPrimary,
    width: 250
  },
  otherPlanDetailsDataLongTitle: {
    ...Theme.bodyPrimary,
    width: 400
  },
  otherPlanDetailsExtraText: {
    ...Theme.bodyPrimary,
    color: Theme.red,
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL
  },
  tableSection: {},
  otherPlanDetailsDataValue: {
    fontSize: Theme.fontSizeXXM,
    color: Theme.grey
  },
  fundSelectionView: {
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL,
    alignItems: "flex-start"
  },
  tableSectionColumn1: {
    flex: 2
  },
  tableSectionColumn2: {
    flex: 1
  },
  tableSectionColumn3: {
    flex: 1
  },
  tableSectionColumn4: {
    flex: 1
  },
  tableSectionColumn5: {
    flex: 1
  },
  tableSectionColumn6: {
    flex: 1
  },
  tableSectionColumn7: {
    flex: 1
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  left: {
    alignItems: "flex-start"
  },
  right: {
    alignItems: "flex-end"
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel,
    textAlign: "left"
  }
});
