import React, { PureComponent } from "react";
import {
  View,
  Text,
  Linking,
  NativeEventEmitter,
  NativeModules
} from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import moment from "moment";
import {
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  EAPP,
  utilities,
  DATE_FORMAT_INPUT,
  PAYMENT_BANK
} from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import TranslatedText from "../../../../containers/TranslatedText";
import EABFormSectionHeader from "../../../../components/EABFormSectionHeader";
import EABRadioButton from "../../../../components/EABRadioButton";
import EABCard from "../../../../components/EABCard";
import EABCheckBox from "../../../../components/EABCheckBox";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import SelectorField from "../../../../components/SelectorField";
import {
  DATE_PICKER,
  FLAT_LIST
} from "../../../../components/SelectorField/constants";
import { ContextConsumer } from "../../../../context";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import PaymentButton from "../../containers/PaymentButton";
import PaymentStatus from "../../containers/PaymentStatus";
import styles from "./styles";

const { getCurrency, getCurrencySign } = utilities.common;
const { OPTIONS_MAP } = REDUCER_TYPES;

const {
  PROPOSALNO,
  POLICYCCY,
  INITBASICPREM,
  INITTOTALPREM,
  BACKDATINGPREM,
  TOPUPPREM,
  INITRSPPREM,
  INITPAYMETHOD,
  TELETRANSFTER,
  TTREMITTINGBANK,
  TTDOR,
  TTREMARKS,
  SRSBLOCK
} = EAPP.fieldId[REDUCER_TYPES.PAYMENT];

const { CASH, AXASAM, DBSCRCARDIPP, SRS } = EAPP.fieldValue[
  REDUCER_TYPES.PAYMENT
];
/**
 * Payment
 * @description Payment in Application Summary page.
 * @param {array} Payment - array of product item object
 * */

class Payment extends PureComponent {
  constructor(props) {
    super(props);

    this.dataSyncEvent = new NativeEventEmitter(
      NativeModules.DataSyncEventManager
    );
    this.axsPath = "http://www.axs.com.sg/";
    this.samPath = "https://www.mysam.sg/index.jsp";
    this.dataSyncCallbackHandler = this.dataSyncCallbackHandler.bind(this);

    this.state = {
      onDataSync: false,
      dataSyncCallback: () => {}
    };
  }

  componentDidMount() {
    const { dataSyncCallbackHandler } = this;
    const { getPayment, application } = this.props;
    if (!_.isEmpty(application)) {
      getPayment(application.id);
    }

    this.dataSyncEventListener = this.dataSyncEvent.addListener(
      "DataSyncEventHandler",
      dataSyncCallbackHandler
    );
  }
  componentWillUnmount() {
    this.dataSyncEventListener.remove();
    this.dataSyncEventListener = null;
  }

  getPaymentSelector(language) {
    const {
      payment,
      updatePayment,
      submission,
      textStore,
      onlinePaymentStatus,
      error,
      isDisableEappButton
    } = this.props;
    let targetOptions = "";
    let editable = true;
    const { trxStatus, trxStatusRemark, trxMethod } = onlinePaymentStatus;
    const initPayMethod = payment.values[INITPAYMETHOD];
    if (
      !trxStatus ||
      trxStatus === "" ||
      trxStatus === "Y" ||
      trxStatus === "N" ||
      trxStatus === "C"
    ) {
      targetOptions = payment.template.items[1].items[0].items[0].options;
      targetOptions = targetOptions.filter(value => value && value.value);
    } else if (trxStatus === "I" || trxStatus === "O") {
      targetOptions = payment.template.items[1].items[0].items[1].options;
      targetOptions = targetOptions.filter(
        value =>
          value &&
          value.value &&
          ((trxMethod === "crCard" &&
            (value.value === "crCard" || value.value === "axasam")) ||
            (trxMethod === "eNets" &&
              (value.value === "eNets" || value.value === "axasam")) ||
            (trxMethod === "dbsCrCardIpp" &&
              (value.value === "dbsCrCardIpp" || value.value === "axasam")))
      );
      if (trxStatusRemark === "I") {
        editable = false;
      }
    }
    if (
      trxStatus === "Y" ||
      submission.values.submitStatus === "SUCCESS" ||
      isDisableEappButton
    ) {
      editable = false;
    }
    return (
      <SelectorField
        style={styles.paymentSelector}
        placeholder={EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.section.payment.initialPaymentPlaceHolder"
        })}
        isRequired
        value={initPayMethod}
        editable={editable}
        selectorType={FLAT_LIST}
        isError={!initPayMethod}
        hintText={_.get(error, `initPayMethod.message[${language}]`, "")}
        flatListOptions={{
          renderItemOnPress: (itemKey, item) => {
            if (item) {
              updatePayment({
                path: `values.${[INITPAYMETHOD]}`,
                newValue: item.value,
                validateObj: {
                  mandatory: true,
                  field: "initPayMethod",
                  value: item.value
                }
              });
            }
          },
          data: targetOptions.map((value, index) => {
            if (value.title && value.value) {
              return {
                label: value.title,
                value: value.value,
                key: index
              };
            }
            return null;
          })
        }}
      />
    );
  }

  getPremiumDetailsTitle(key) {
    const { payment } = this.props;
    let breakLoop = false;
    let targetTitle = "";
    payment.template.items[0].items.some(value => {
      if (breakLoop) {
        return true;
      }
      value.items.some(subValue => {
        if (breakLoop) {
          return true;
        }
        if (subValue.id === key) {
          targetTitle = subValue.title;
          breakLoop = true;
        }
        return false;
      });
      return false;
    });
    return targetTitle;
  }

  getPremiumDetails(language) {
    const { payment, textStore, application, optionsMap } = this.props;
    if (_.isEmpty(payment)) {
      return null;
    }

    const sortList = [
      {
        key: PROPOSALNO,
        sortIndex: 0,
        value: "",
        type: "string"
      },
      {
        key: POLICYCCY,
        sortIndex: 1,
        value: "",
        type: "string"
      },
      {
        key: INITBASICPREM,
        sortIndex: 2,
        value: "",
        type: "number",
        extra: {
          showText: {
            text: EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.section.payment.value_singlePremium"
            }),
            condition: {
              paymentMode: "L"
            }
          }
        }
      },
      {
        key: BACKDATINGPREM,
        sortIndex: 3,
        value: "",
        type: "number",
        condition: {
          greaterThen0: true
        }
      },
      {
        key: TOPUPPREM,
        sortIndex: 4,
        value: "",
        type: "number",
        condition: {
          greaterThen0: true
        },
        extra: {
          showText: {
            text: EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.section.payment.value_singlePremium"
            })
          }
        }
      },
      {
        key: INITRSPPREM,
        sortIndex: 5,
        value: "",
        type: "number",
        condition: {
          greaterThen0: true
        }
      },
      {
        key: INITTOTALPREM,
        sortIndex: 6,
        value: "",
        type: "number"
      }
    ];
    const premiumDetailsArr = [];

    Object.entries(payment.values).forEach(([key, paymentValue]) => {
      sortList.forEach((listValue, listIndex) => {
        const value = paymentValue;
        if (listValue.key === key && value !== null) {
          sortList[listIndex].value = value;
        }
      });
    });
    sortList.sort((prev, curr) => prev.sortIndex - curr.sortIndex);
    sortList.forEach(value => {
      let push = true;
      if (value.condition && value.condition.greaterThen0) {
        if (value.value <= 0 || !value.value) {
          push = false;
        }
      }
      if (push) {
        if (value.type === "number") {
          value.value = getCurrency({
            value: value.value,
            sign: getCurrencySign({
              compCode: application.compCode,
              ccy: application.quotation.ccy,
              optionsMap
            }),
            decimals: 2
          });
        }
        if (value.extra) {
          if (value.extra.showText) {
            let { text } = value.extra.showText;
            if (value.extra.showText.condition) {
              Object.entries(value.extra.showText.condition).some(
                ([field, fieldValue]) => {
                  if (payment.values[field] !== fieldValue) {
                    text = "";
                    return true;
                  }
                  return false;
                }
              );
            }
            value.value = `${value.value} ${text}`;
          }
        }
        premiumDetailsArr.push(
          <View style={styles.sectionContentRowView}>
            <Text style={styles.sectionContentTitle}>
              {this.getPremiumDetailsTitle(value.key)}:
            </Text>
            <Text style={Theme.bodySecondary}>{value.value}</Text>
          </View>
        );
      }
    });

    return (
      <ContextConsumer>
        {() => (
          <View style={styles.sectionView}>
            <View style={styles.sectionHeaderView}>
              <TranslatedText
                style={Theme.captionBrand}
                language={language}
                path="paymentAndSubmission.section.payment.premiumDetails"
              />
            </View>
            <View style={styles.sectionContentView}>{premiumDetailsArr}</View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  getCPFBlock() {
    const {
      payment,
      error,
      textStore,
      updatePayment,
      submission,
      onlinePaymentStatus
    } = this.props;
    const { trxStatus } = onlinePaymentStatus;

    let editable = true;
    if (trxStatus === "Y" || submission.values.submitStatus === "SUCCESS") {
      editable = false;
    }
    if (_.isEmpty(payment) || _.isEmpty(submission)) {
      return null;
    }
    let blockValue = null;
    let blockName = null;
    switch (payment.values.initPayMethod) {
      case "cpfissa":
        blockValue = payment.values.cpfissaBlock;
        blockName = "cpfissa";
        break;
      case "cpfisoa":
        blockValue = payment.values.cpfisoaBlock;
        blockName = "cpfisoa";
        break;
      default:
        return null;
    }

    if (!blockValue || !blockName) {
      return null;
    }

    return (
      <ContextConsumer>
        {({ language }) => (
          <React.Fragment>
            <View style={styles.srsContentRowView}>
              <Text style={styles.srsContentTitle}>
                {EABi18n({
                  path: "paymentAndSubmission.text.cpfAccountNumber",
                  language,
                  textStore
                })}
              </Text>
              <Text style={Theme.bodySecondary}>{blockValue.idCardNo}</Text>
            </View>
            {blockName === "cpfisoa" ? (
              <View style={{ width: "100%" }}>
                <View style={styles.blockView}>
                  <EABFormSectionHeader
                    isRequired
                    style={styles.title}
                    title={EABi18n({
                      path:
                        "paymentAndSubmission.section.payment.placeholder.existingCPFAcc",
                      language,
                      textStore
                    })}
                  />
                  {error[`${blockName}IsHaveCpfAcct`] &&
                  error[`${blockName}IsHaveCpfAcct`].hasError ? (
                    <TranslatedText
                      style={Theme.fieldTextOrHelperTextNegative}
                      language={language}
                      path="error.302"
                    />
                  ) : null}
                  <View style={styles.textSelection}>
                    <EABRadioButton
                      options={[
                        {
                          key: "Y",
                          title: EABi18n({
                            path: "button.yes",
                            language,
                            textStore
                          })
                        },
                        {
                          key: "N",
                          title: EABi18n({
                            path: "button.no",
                            language,
                            textStore
                          })
                        }
                      ]}
                      selectedOptionKey={
                        blockValue[`${blockName}IsHaveCpfAcct`]
                      }
                      onPress={item => {
                        const newValue = item.key;

                        updatePayment({
                          path: `values.${blockName}Block.${blockName}IsHaveCpfAcct`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}IsHaveCpfAcct`,
                            value: newValue
                          }
                        });

                        const keyList = [
                          `${blockName}BankNamePicker`,
                          `${blockName}FundProcessDate`,
                          `${blockName}FundReqDate`,
                          `${blockName}InvmAcctNo`,
                          `${blockName}AcctDeclare`
                        ];

                        keyList.forEach(key => {
                          updatePayment({
                            path: `values.${blockName}Block.${key}`,
                            newValue: "",
                            validateObj: {
                              mandatory: true,
                              field: key,
                              value: ""
                            }
                          });
                        });
                        updatePayment({
                          path: `values.${blockName}Block.${blockName}InvmAcctNo`,
                          newValue: "",
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}InvmAcctNo`,
                            value: "",
                            bankName: ""
                          }
                        });
                      }}
                      disabled={!editable}
                    />
                  </View>
                </View>

                {blockValue[`${blockName}IsHaveCpfAcct`] === "Y" ? (
                  <React.Fragment>
                    <SelectorField
                      style={styles.field}
                      value={blockValue[`${blockName}BankNamePicker`]}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path:
                          "paymentAndSubmission.section.payment.placeholder.bankName"
                      })}
                      isError={
                        blockValue[`${blockName}BankNamePicker`].length === 0
                      }
                      hintText={EABi18n({
                        language,
                        textStore,
                        path: "error.302"
                      })}
                      selectorType={FLAT_LIST}
                      flatListOptions={{
                        renderItemOnPress: (itemKey, item) => {
                          if (item) {
                            const newValue = item.value;
                            if (
                              item.value !==
                              blockValue[`${blockName}BankNamePicker`]
                            ) {
                              updatePayment({
                                path: `values.${blockName}Block.${blockName}InvmAcctNo`,
                                newValue: "",
                                validateObj: {
                                  mandatory: true,
                                  field: `${blockName}InvmAcctNo`,
                                  value: "",
                                  bankName: newValue
                                }
                              });
                            }

                            updatePayment({
                              path: `values.${blockName}Block.${blockName}BankNamePicker`,
                              newValue,
                              validateObj: {
                                mandatory: true,
                                field: `${blockName}BankNamePicker`,
                                value: newValue
                              }
                            });
                          }
                        },
                        extraData: this.state,
                        keyExtractor: item => item.key,
                        data: Object.values(PAYMENT_BANK)
                      }}
                      isRequired
                      editable={editable}
                    />

                    <EABTextField
                      style={styles.fieldFirst}
                      value={blockValue[`${blockName}InvmAcctNo`]}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path:
                          "paymentAndSubmission.section.payment.placeholder.cpfAccountNumber"
                      })}
                      isError={_.get(
                        error,
                        `${blockName}InvmAcctNo.hasError`,
                        false
                      )}
                      hintText={_.get(
                        error,
                        `${blockName}InvmAcctNo.message[${language}]`,
                        ""
                      )}
                      onChange={value => {
                        const sliceValue =
                          value.slice(
                            0,
                            PAYMENT_BANK[
                              blockValue[`${blockName}BankNamePicker`]
                            ].cpfMaxLength
                          ) || "";
                        const newValue = sliceValue.replace(/[^0-9]+/g, "");
                        updatePayment({
                          path: `values.${blockName}Block.${blockName}InvmAcctNo`,
                          newValue: newValue.toString(),
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}InvmAcctNo`,
                            value: newValue,
                            bankName: blockValue[`${blockName}BankNamePicker`]
                          }
                        });
                      }}
                      isRequired
                      maxLength={
                        PAYMENT_BANK[blockValue[`${blockName}BankNamePicker`]]
                          ? PAYMENT_BANK[
                              blockValue[`${blockName}BankNamePicker`]
                            ].cpfMaxLength
                          : 0
                      }
                      editable={editable}
                    />

                    <EABCheckBox
                      options={[
                        {
                          key: "srsDateCheckBox",
                          text: EABi18n({
                            language,
                            textStore,
                            path:
                              "paymentAndSubmission.section.payment.placeholder.processCPFFundRequest"
                          }),
                          isSelected:
                            blockValue[`${blockName}FundReqDate`] === "Y" ||
                            false
                        }
                      ]}
                      onPress={() => {
                        const newValue =
                          blockValue[`${blockName}FundReqDate`] === "Y"
                            ? "N"
                            : "Y";
                        updatePayment({
                          path: `values.${blockName}Block.${blockName}FundReqDate`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}FundReqDate`,
                            value: newValue
                          }
                        });

                        updatePayment({
                          path: `values.${blockName}Block.${blockName}FundProcessDate`,
                          newValue: "",
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}FundProcessDate`,
                            value: ""
                          }
                        });
                      }}
                      disable={!editable}
                    />

                    {blockValue[`${blockName}FundReqDate`] === "Y" ? (
                      <SelectorField
                        style={styles.srsDate}
                        value={blockValue[`${blockName}FundProcessDate`]}
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path:
                            "paymentAndSubmission.section.payment.placeholder.processCPFFundDate"
                        })}
                        selectorType={DATE_PICKER}
                        datePickerIOSOptions={{
                          mode: "date",
                          date:
                            blockValue[`${blockName}FundProcessDate`] === ""
                              ? new Date(moment().format(DATE_FORMAT_INPUT))
                              : new Date(
                                  blockValue[`${blockName}FundProcessDate`]
                                ),
                          onDateChange: value => {
                            const newValue = moment(value).valueOf();

                            updatePayment({
                              path: `values.${blockName}Block.${blockName}FundProcessDate`,
                              newValue,
                              validateObj: {
                                mandatory: true,
                                field: `${blockName}FundProcessDate`,
                                value: newValue
                              }
                            });
                          },
                          maximumDate: new Date(moment().add(+1, "months")),
                          minimumDate: new Date(moment())
                        }}
                        isRequired
                        isError={_.get(
                          error,
                          `${blockName}FundProcessDate.hasError`,
                          false
                        )}
                        hintText={_.get(
                          error,
                          `${blockName}FundProcessDate.message[${language}]`,
                          ""
                        )}
                        editable={editable}
                      />
                    ) : null}
                  </React.Fragment>
                ) : null}
                {blockValue[`${blockName}IsHaveCpfAcct`] === "N" ? (
                  <View style={styles.blockView}>
                    <View>
                      {error[`${blockName}AcctDeclare`] &&
                      error[`${blockName}AcctDeclare`].hasError ? (
                        <TranslatedText
                          style={Theme.fieldTextOrHelperTextNegative}
                          language={language}
                          path="error.302"
                        />
                      ) : null}
                      <EABRadioButton
                        options={[
                          {
                            key: "Y",
                            title: EABi18n({
                              path:
                                "paymentAndSubmission.section.payment.placeholder.cpfAcctDeclareAXA",
                              language,
                              textStore
                            })
                          },
                          {
                            key: "N",
                            title: EABi18n({
                              path:
                                "paymentAndSubmission.section.payment.placeholder.cpfAcctDeclareBank",
                              language,
                              textStore
                            })
                          }
                        ]}
                        selectedOptionKey={
                          blockValue[`${blockName}AcctDeclare`]
                        }
                        onPress={item => {
                          const newValue = item.key;

                          updatePayment({
                            path: `values.${blockName}Block.${blockName}AcctDeclare`,
                            newValue,
                            validateObj: {
                              mandatory: true,
                              field: `${blockName}AcctDeclare`,
                              value: newValue
                            }
                          });
                        }}
                        disabled={!editable}
                      />
                    </View>
                    {blockValue[`${blockName}AcctDeclare`] === "N" ? (
                      <View style={styles.srsAcctDeclareWarningContainer}>
                        <TranslatedText
                          style={Theme.fieldTextOrHelperTextNegative}
                          language={language}
                          path="paymentAndSubmission.section.payment.placeholder.cpfAcctDeclareWarning"
                        />
                      </View>
                    ) : null}
                  </View>
                ) : null}
              </View>
            ) : (
              <View style={{ width: "100%" }}>
                <EABCheckBox
                  options={[
                    {
                      key: "srsDateCheckBox",
                      text: EABi18n({
                        language,
                        textStore,
                        path:
                          "paymentAndSubmission.section.payment.placeholder.processCPFFundRequest"
                      }),
                      isSelected:
                        blockValue[`${blockName}FundReqDate`] === "Y" || false
                    }
                  ]}
                  onPress={() => {
                    const newValue =
                      blockValue[`${blockName}FundReqDate`] === "Y" ? "N" : "Y";
                    updatePayment({
                      path: `values.${blockName}Block.${blockName}FundReqDate`,
                      newValue,
                      validateObj: {
                        mandatory: true,
                        field: `${blockName}FundReqDate`,
                        value: newValue
                      }
                    });

                    updatePayment({
                      path: `values.${blockName}Block.${blockName}FundProcessDate`,
                      newValue: "",
                      validateObj: {
                        mandatory: true,
                        field: `${blockName}FundProcessDate`,
                        value: ""
                      }
                    });
                  }}
                  disable={!editable}
                />

                {blockValue[`${blockName}FundReqDate`] === "Y" ? (
                  <SelectorField
                    style={styles.srsDate}
                    value={blockValue[`${blockName}FundProcessDate`]}
                    placeholder={EABi18n({
                      language,
                      textStore,
                      path:
                        "paymentAndSubmission.section.payment.placeholder.processCPFFundDate"
                    })}
                    selectorType={DATE_PICKER}
                    datePickerIOSOptions={{
                      mode: "date",
                      date:
                        blockValue[`${blockName}FundProcessDate`] === ""
                          ? new Date(moment().format(DATE_FORMAT_INPUT))
                          : new Date(blockValue[`${blockName}FundProcessDate`]),
                      onDateChange: value => {
                        const newValue = moment(value).valueOf();

                        updatePayment({
                          path: `values.${blockName}Block.${blockName}FundProcessDate`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: `${blockName}FundProcessDate`,
                            value: newValue
                          }
                        });
                      },
                      maximumDate: new Date(moment().add(+1, "months")),
                      minimumDate: new Date(moment())
                    }}
                    isRequired
                    isError={_.get(
                      error,
                      `${blockName}FundProcessDate.hasError`,
                      false
                    )}
                    hintText={_.get(
                      error,
                      `${blockName}FundProcessDate.message[${language}]`,
                      ""
                    )}
                    editable={editable}
                  />
                ) : null}
              </View>
            )}
          </React.Fragment>
        )}
      </ContextConsumer>
    );
  }

  getPaymentSection() {
    const {
      payment,
      error,
      textStore,
      updatePayment,
      submission,
      onlinePaymentStatus,
      isDisableEappButton
    } = this.props;
    if (_.isEmpty(payment) || _.isEmpty(submission)) {
      return null;
    }
    const isDisable =
      submission.values.submitStatus === "SUCCESS" ||
      isDisableEappButton ||
      false;
    const initPayMethod = payment.values[INITPAYMETHOD];
    const ttRemittingBank = payment.values[TTREMITTINGBANK];
    const ttDOR = payment.values[TTDOR];
    const ttRemarks = payment.values[TTREMARKS];
    const srsBlock = payment.values[SRSBLOCK];
    const { trxStatus } = onlinePaymentStatus;

    let editable = true;
    if (
      trxStatus === "Y" ||
      submission.values.submitStatus === "SUCCESS" ||
      isDisableEappButton
    ) {
      editable = false;
    }

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <View style={styles.sectionHeaderView}>
              <TranslatedText
                style={Theme.captionBrand}
                language={language}
                path="paymentAndSubmission.section.payment.initialPaymentMethod"
              />
            </View>

            <View style={styles.initalPaymentMethod}>
              {this.getPaymentSelector(language)}
              <PaymentButton />
            </View>
            <PaymentStatus />
            {initPayMethod === DBSCRCARDIPP ? (
              <View style={{ marginTop: Theme.alignmentXS }}>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="paymentAndSubmission.section.payment.dbsWarning"
                />
              </View>
            ) : null}

            {this.getCPFBlock()}

            {initPayMethod === SRS ? (
              <React.Fragment>
                <View style={styles.srsContentRowView}>
                  <Text style={styles.srsContentTitle}>
                    {EABi18n({
                      path: "paymentAndSubmission.section.payment.clientID",
                      language,
                      textStore
                    })}
                  </Text>
                  <Text style={Theme.bodySecondary}>{srsBlock.idCardNo}</Text>
                </View>

                <View style={styles.blockView}>
                  <EABFormSectionHeader
                    isRequired
                    style={styles.title}
                    title={EABi18n({
                      path:
                        "paymentAndSubmission.section.payment.placeholder.existingSRSAcc",
                      language,
                      textStore
                    })}
                  />
                  {error.srsIsHaveSrsAcct && error.srsIsHaveSrsAcct.hasError ? (
                    <TranslatedText
                      style={Theme.fieldTextOrHelperTextNegative}
                      language={language}
                      path="error.302"
                    />
                  ) : null}
                  <View style={styles.textSelection}>
                    <EABRadioButton
                      disable={isDisable}
                      options={[
                        {
                          key: "Y",
                          title: EABi18n({
                            path: "button.yes",
                            language,
                            textStore
                          })
                        },
                        {
                          key: "N",
                          title: EABi18n({
                            path: "button.no",
                            language,
                            textStore
                          })
                        }
                      ]}
                      selectedOptionKey={srsBlock.srsIsHaveSrsAcct}
                      onPress={item => {
                        const newValue = item.key;

                        updatePayment({
                          path: `values.${[SRSBLOCK]}.srsIsHaveSrsAcct`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: "srsIsHaveSrsAcct",
                            value: newValue
                          }
                        });

                        const keyList = [
                          "srsBankNamePicker",
                          "srsFundProcessDate",
                          "srsFundReqDate",
                          "srsInvmAcctNo",
                          "srsAcctDeclare"
                        ];

                        keyList.forEach(key => {
                          updatePayment({
                            path: `values.${[SRSBLOCK]}.${key}`,
                            newValue: "",
                            validateObj: {
                              mandatory: true,
                              field: key,
                              value: ""
                            }
                          });
                        });
                        updatePayment({
                          path: `values.${[SRSBLOCK]}.srsInvmAcctNo`,
                          newValue: "",
                          validateObj: {
                            mandatory: true,
                            field: "srsInvmAcctNo",
                            value: "",
                            bankName: ""
                          }
                        });
                      }}
                      disabled={!editable}
                    />
                  </View>
                </View>

                {srsBlock.srsIsHaveSrsAcct === "Y" ? (
                  <React.Fragment>
                    <SelectorField
                      style={styles.field}
                      value={srsBlock.srsBankNamePicker}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path:
                          "paymentAndSubmission.section.payment.placeholder.bankName"
                      })}
                      isError={srsBlock.srsBankNamePicker.length === 0}
                      hintText={EABi18n({
                        language,
                        textStore,
                        path: "error.302"
                      })}
                      selectorType={FLAT_LIST}
                      flatListOptions={{
                        renderItemOnPress: (itemKey, item) => {
                          if (item) {
                            const newValue = item.value;
                            if (item.value !== srsBlock.srsBankNamePicker) {
                              updatePayment({
                                path: `values.${[SRSBLOCK]}.srsInvmAcctNo`,
                                newValue: "",
                                validateObj: {
                                  mandatory: true,
                                  field: "srsInvmAcctNo",
                                  value: "",
                                  bankName: newValue
                                }
                              });
                            }

                            updatePayment({
                              path: `values.${[SRSBLOCK]}.srsBankNamePicker`,
                              newValue,
                              validateObj: {
                                mandatory: true,
                                field: "srsBankNamePicker",
                                value: newValue
                              }
                            });
                          }
                        },
                        extraData: this.state,
                        keyExtractor: item => item.key,
                        data: Object.values(PAYMENT_BANK)
                      }}
                      isRequired
                      editable={editable}
                    />

                    <EABTextField
                      style={styles.fieldFirst}
                      value={srsBlock.srsInvmAcctNo}
                      placeholder={EABi18n({
                        language,
                        textStore,
                        path:
                          "paymentAndSubmission.section.payment.placeholder.srsAccountNumber"
                      })}
                      isError={_.get(error, "srsInvmAcctNo.hasError", false)}
                      hintText={_.get(
                        error,
                        `srsInvmAcctNo.message[${language}]`,
                        ""
                      )}
                      onChange={value => {
                        const sliceValue =
                          value.slice(
                            0,
                            PAYMENT_BANK[srsBlock.srsBankNamePicker].maxLength
                          ) || "";
                        const newValue = sliceValue.replace(/[^0-9]+/g, "");
                        updatePayment({
                          path: `values.${[SRSBLOCK]}.srsInvmAcctNo`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: "srsInvmAcctNo",
                            value: newValue,
                            bankName: srsBlock.srsBankNamePicker
                          }
                        });
                      }}
                      isRequired
                      maxLength={
                        PAYMENT_BANK[srsBlock.srsBankNamePicker]
                          ? PAYMENT_BANK[srsBlock.srsBankNamePicker].maxLength
                          : 0
                      }
                      editable={editable}
                    />

                    <EABCheckBox
                      options={[
                        {
                          key: "srsDateCheckBox",
                          text: EABi18n({
                            language,
                            textStore,
                            path:
                              "paymentAndSubmission.section.payment.placeholder.processSRSFundRequest"
                          }),
                          isSelected: srsBlock.srsFundReqDate === "Y" || false
                        }
                      ]}
                      onPress={() => {
                        const newValue =
                          srsBlock.srsFundReqDate === "Y" ? "N" : "Y";
                        updatePayment({
                          path: `values.${[SRSBLOCK]}.srsFundReqDate`,
                          newValue,
                          validateObj: {
                            mandatory: true,
                            field: "srsFundReqDate",
                            value: newValue
                          }
                        });

                        updatePayment({
                          path: `values.${[SRSBLOCK]}.srsFundProcessDate`,
                          newValue: "",
                          validateObj: {
                            mandatory: true,
                            field: "srsFundProcessDate",
                            value: ""
                          }
                        });
                      }}
                      disable={!editable}
                    />

                    {srsBlock.srsFundReqDate === "Y" ? (
                      <SelectorField
                        style={styles.srsDate}
                        value={srsBlock.srsFundProcessDate}
                        placeholder={EABi18n({
                          language,
                          textStore,
                          path:
                            "paymentAndSubmission.section.payment.placeholder.processSRSFundDate"
                        })}
                        selectorType={DATE_PICKER}
                        datePickerIOSOptions={{
                          mode: "date",
                          date:
                            srsBlock.srsFundProcessDate === ""
                              ? new Date(moment().format(DATE_FORMAT_INPUT))
                              : new Date(srsBlock.srsFundProcessDate),
                          onDateChange: value => {
                            const newValue = moment(value).valueOf();

                            updatePayment({
                              path: `values.${[SRSBLOCK]}.srsFundProcessDate`,
                              newValue,
                              validateObj: {
                                mandatory: true,
                                field: "srsFundProcessDate",
                                value: newValue
                              }
                            });
                          },
                          maximumDate: new Date(moment().add(+1, "months")),
                          minimumDate: new Date(moment())
                        }}
                        isRequired
                        isError={_.get(
                          error,
                          "srsFundProcessDate.hasError",
                          false
                        )}
                        hintText={_.get(
                          error,
                          `srsFundProcessDate.message[${language}]`,
                          ""
                        )}
                        editable={editable}
                      />
                    ) : null}
                  </React.Fragment>
                ) : null}
                {srsBlock.srsIsHaveSrsAcct === "N" ? (
                  <View style={styles.blockView}>
                    <View>
                      {error.srsAcctDeclare && error.srsAcctDeclare.hasError ? (
                        <TranslatedText
                          style={Theme.fieldTextOrHelperTextNegative}
                          language={language}
                          path="error.302"
                        />
                      ) : null}
                      <EABRadioButton
                        options={[
                          {
                            key: "Y",
                            title: EABi18n({
                              path:
                                "paymentAndSubmission.section.payment.placeholder.srsAcctDeclareAXA",
                              language,
                              textStore
                            })
                          },
                          {
                            key: "N",
                            title: EABi18n({
                              path:
                                "paymentAndSubmission.section.payment.placeholder.srsAcctDeclareBank",
                              language,
                              textStore
                            })
                          }
                        ]}
                        selectedOptionKey={srsBlock.srsAcctDeclare}
                        onPress={item => {
                          const newValue = item.key;

                          updatePayment({
                            path: `values.${[SRSBLOCK]}.srsAcctDeclare`,
                            newValue,
                            validateObj: {
                              mandatory: true,
                              field: "srsAcctDeclare",
                              value: newValue
                            }
                          });
                        }}
                        disabled={!editable}
                      />
                    </View>
                    {srsBlock.srsAcctDeclare === "N" ? (
                      <View style={styles.srsAcctDeclareWarningContainer}>
                        <TranslatedText
                          style={Theme.fieldTextOrHelperTextNegative}
                          language={language}
                          path="paymentAndSubmission.section.payment.placeholder.srsAcctDeclareWarning"
                        />
                      </View>
                    ) : null}
                  </View>
                ) : null}
              </React.Fragment>
            ) : null}

            {!isDisable && initPayMethod === AXASAM ? (
              <View style={styles.axsSamButtonView}>
                <EABButton
                  isDisable={isDisableEappButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    Linking.canOpenURL(this.axsPath).then(supported => {
                      if (!supported) {
                        return null;
                      }
                      return Linking.openURL(this.axsPath);
                    });
                  }}
                >
                  <TranslatedText language={language} path="button.axsOnline" />
                </EABButton>
                <EABButton
                  isDisable={isDisableEappButton}
                  style={styles.samOnlineButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    Linking.canOpenURL(this.samPath).then(supported => {
                      if (!supported) {
                        return null;
                      }
                      return Linking.openURL(this.samPath);
                    });
                  }}
                >
                  <TranslatedText language={language} path="button.samOnline" />
                </EABButton>
              </View>
            ) : null}

            {initPayMethod === TELETRANSFTER ? (
              <View style={styles.teleTransfter}>
                <EABTextField
                  editable={!isDisable}
                  style={styles.fieldFirst}
                  value={ttRemittingBank || ""}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "placeholder.remittingBank"
                  })}
                  isError={_.get(error, "ttRemittingBank.hasError", false)}
                  hintText={_.get(
                    error,
                    `ttRemittingBank.message[${language}]`,
                    ""
                  )}
                  onChange={newValue => {
                    updatePayment({
                      path: `values.${[TTREMITTINGBANK]}`,
                      newValue,
                      validateObj: {
                        mandatory: true,
                        field: "ttRemittingBank",
                        value: newValue
                      }
                    });
                  }}
                  isRequired
                  maxLength={30}
                />
                <SelectorField
                  editable={!isDisable}
                  style={styles.fieldFirst}
                  value={ttDOR}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path:
                      "paymentAndSubmission.section.payment.dateOfRemittance"
                  })}
                  selectorType={DATE_PICKER}
                  datePickerIOSOptions={{
                    mode: "date",
                    date: ttDOR === "" ? new Date() : new Date(ttDOR),
                    onDateChange: value => {
                      const newValue = moment(value).valueOf();
                      updatePayment({
                        path: `values.${[TTDOR]}`,
                        newValue,
                        validateObj: {
                          mandatory: true,
                          field: "ttDOR",
                          value: newValue
                        }
                      });
                    },
                    maximumDate: new Date(moment().add(+3, "years")),
                    minimumDate: new Date(moment().add(-3, "years"))
                  }}
                  isRequired
                  isError={_.get(error, "ttDOR.hasError", false)}
                  hintText={_.get(error, `ttDOR.message[${language}]`, "")}
                />
                <EABTextBox
                  style={
                    _.get(error, "ttRemarks.hasError", false)
                      ? { marginTop: 20, borderColor: Theme.red }
                      : { marginTop: 20 }
                  }
                  editable={!isDisable}
                  placeholder={EABi18n({
                    textStore,
                    language,
                    path: "placeholder.remarks"
                  })}
                  value={ttRemarks || ""}
                  numberOfLines={2}
                  onChange={newValue => {
                    updatePayment({
                      path: `values.${[TTREMARKS]}`,
                      newValue,
                      validateObj: {
                        mandatory: true,
                        field: "ttRemarks",
                        value: newValue
                      }
                    });
                  }}
                  maxLength={100}
                />
                {_.get(error, "ttRemarks.hasError", false) ? (
                  <View style={styles.errorRow}>
                    <TranslatedText
                      style={Theme.fieldTextOrHelperTextNegative}
                      path="error.302"
                    />
                  </View>
                ) : null}
              </View>
            ) : null}
          </View>
        )}
      </ContextConsumer>
    );
  }

  getsubsequentPaymentMethod() {
    const {
      payment,
      updatePayment,
      submission,
      isDisableEappButton
    } = this.props;
    if (_.isEmpty(payment) || _.isEmpty(submission)) {
      return null;
    }
    const { policyCcy, paymentMode, rspSelect, paymentMethod } = payment.values;

    if (
      policyCcy === "SGD" &&
      (paymentMode !== "L" || rspSelect === "Y") &&
      ((paymentMethod === "cash" && rspSelect === "Y") ||
        (paymentMethod !== "srs" &&
          paymentMethod !== "cpfissa" &&
          paymentMethod !== "cpfisoa" &&
          rspSelect === "Y") ||
        paymentMethod === "" ||
        !paymentMethod ||
        rspSelect === "N")
    ) {
      const isDisable = submission.values.submitStatus === "SUCCESS" || false;
      if (isDisable) {
        return null;
      }
      const { items } = payment.template.items[1].items[9];
      return (
        <ContextConsumer>
          {({ language }) => (
            <View style={styles.sectionView}>
              <View style={styles.sectionHeaderView}>
                <TranslatedText
                  style={Theme.captionBrand}
                  language={language}
                  path="paymentAndSubmission.section.payment.subsequentPaymentMethod"
                />
              </View>
              {paymentMode !== "L" ? (
                <View>
                  <EABCheckBox
                    options={[
                      {
                        key: items[0].title,
                        text: items[0].title,
                        isSelected:
                          payment.values.subseqPayMethod === "Y" || false
                      }
                    ]}
                    onPress={newValue => {
                      updatePayment({
                        path: "values.subseqPayMethod",
                        newValue: newValue.isSelected ? "N" : "Y"
                      });
                    }}
                    disable={
                      (payment.values.subseqPayMethod === "Y" &&
                        payment.values.paymentMode === "M") ||
                      isDisableEappButton
                    }
                  />
                </View>
              ) : null}
              {payment.values.paymentMethod === CASH &&
              payment.values.rspSelect === "Y" ? (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="paymentAndSubmission.section.payment.cashWarning"
                />
              ) : null}
              {payment.values.subseqPayMethod === "Y" ||
              (paymentMethod !== "srs" &&
                paymentMethod !== "cpfissa" &&
                paymentMethod !== "cpfisoa" &&
                rspSelect === "Y") ? (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="paymentAndSubmission.section.payment.subseqPayMethodWarning"
                />
              ) : null}
            </View>
          )}
        </ContextConsumer>
      );
    }
    return null;
  }
  /**
   * @description data sync callback function
   * @param {string} status - data sync result
   * */
  dataSyncCallbackHandler(status) {
    const { onDataSync, dataSyncCallback } = this.state;

    if (status === "SUCCESS" && onDataSync) {
      this.props.setLastDataSyncTime();
      this.setState(
        {
          onDataSync: false
        },
        dataSyncCallback
      );
    }
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <EABCard style={styles.paymentCard}>
              <View style={styles.paymentHeaderView}>
                <TranslatedText
                  style={Theme.headlinePrimary}
                  language={language}
                  path="paymentAndSubmission.section.payment.title"
                />
              </View>
              <View style={styles.analysisSectionBorderLine} />
              <View style={styles.contentView}>
                {this.getPremiumDetails(language)}
                {this.getPaymentSection()}
                {this.getsubsequentPaymentMethod()}
              </View>
              <View style={styles.sectionView} />
            </EABCard>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

Payment.propTypes = {
  updatePayment: PropTypes.func.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  getPayment: PropTypes.func.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  submission:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
      .isRequired,
  payment:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment.isRequired,
  error:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired,
  isDisableEappButton: PropTypes.bool.isRequired
};

export default Payment;
