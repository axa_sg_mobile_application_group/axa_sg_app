import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { ContextConsumer } from "../../../../context";
import Payment from "../../containers/Payment";
import Submission from "../../containers/Submission";
import styles from "./styles";

/**
 * PaymentAndSubmission
 * @description PaymentAndSubmission in Application Summary page.
 * @param {array} PaymentAndSubmission - array of product item object
 * */

class PaymentAndSubmission extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ContextConsumer>
        {() => (
          <KeyboardAwareScrollView extraScrollHeight={100}>
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              <View style={styles.motherView}>
                <Payment onlinePaymentStatus={this.props.onlinePaymentStatus} />
                <Submission
                  onlinePaymentStatus={this.props.onlinePaymentStatus}
                />
              </View>
            </ScrollView>
          </KeyboardAwareScrollView>
        )}
      </ContextConsumer>
    );
  }
}

PaymentAndSubmission.propTypes = {
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired
};

export default PaymentAndSubmission;
