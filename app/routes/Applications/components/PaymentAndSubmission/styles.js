import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  productsWrapper: {
    flexDirection: "row"
  },
  alertDialogTextView: {
    alignItems: "center"
  },
  headerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  noProductView: {
    justifyContent: "center",
    height: 164
  },
  motherView: {
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 100
  },
  blueText: {
    ...Theme.textButtonLabelNormalAccent,
    fontWeight: "bold"
  },
  paymentCard: {
    marginTop: Theme.alignmentXL,
    width: 622
  },
  initalPaymentMethod: {
    flexDirection: "row",
    width: "100%",
    alignItems: "center"
  },
  paymentSelector: {
    flex: 1
  },
  paymentHeaderView: {
    marginTop: Theme.alignmentL,
    marginLeft: Theme.alignmentXL,
    marginBottom: Theme.alignmentL
  },
  analysisSectionBorderLine: {
    borderTopWidth: 1,
    borderColor: Theme.lightGrey
  },
  contentView: {
    marginHorizontal: Theme.alignmentXL
  },
  sectionView: {
    alignItems: "flex-start",
    marginTop: Theme.alignmentXL
  },
  sectionHeaderView: {
    paddingBottom: 2,
    borderBottomWidth: 2,
    borderColor: Theme.brand,
    marginBottom: Theme.alignmentL
  },
  sectionContentView: {
    flexDirection: "column"
  },
  submitMessageView: {
    marginTop: Theme.alignmentL,
    flexDirection: "column"
  },
  bottomMarginFix: {
    marginBottom: Theme.alignmentL
  },
  sectionContentRowView: {
    flexDirection: "row",
    width: "100%"
  },
  sectionContentTitle: {
    width: 186,
    ...Theme.bodyPrimary
  },
  submissionSectionContentTitle: {
    width: 260,
    ...Theme.bodyPrimary
  },
  paymentButton: {
    width: 82,
    height: 36
  },
  axsSamButtonView: {
    marginTop: Theme.alignmentXS,
    flexDirection: "row"
  },
  samOnlineButton: {
    marginLeft: Theme.alignmentXL
  },
  teleTransfter: {
    flexDirection: "column",
    marginBottom: Theme.alignmentXS,
    width: "100%"
  },
  fieldFirst: {
    width: "52%"
  },
  srsDate: {
    width: "65%"
  },
  scrollView: {
    backgroundColor: Theme.paleGrey,
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "center",
    paddingVertical: Theme.alignmentXL,
    paddingHorizontal: 72
  },
  errorRow: {
    alignItems: "flex-end"
  },
  textSelection: {
    marginTop: 14,
    marginBottom: Theme.alignmentXL,
    height: 50
  },
  blockView: {
    height: 150
  },
  srsAcctDeclareWarningContainer: {
    marginTop: Theme.alignmentM
  },
  srsContentRowView: {
    width: "100%",
    marginBottom: Theme.alignmentXL
  },
  srsContentTitle: {
    ...Theme.bodyPrimary
  }
});
