import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { ScrollView } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from "./styles";
import PaymentShield from "../../containers/PaymentShield";
import SubmissionShield from "../../containers/SubmissionShield";

/**
 * PaymentAndSubmissionShield
 * @description PaymentAndSubmissionShield in Application Summary page.
 * @param {array} PaymentAndSubmissionShield - array of product item object
 * */

class PaymentAndSubmissionShield extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <KeyboardAwareScrollView extraScrollHeight={100}>
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContent}
        >
          <PaymentShield onlinePaymentStatus={this.props.onlinePaymentStatus} />
          <SubmissionShield
            onlinePaymentStatus={this.props.onlinePaymentStatus}
          />
        </ScrollView>
      </KeyboardAwareScrollView>
    );
  }
}

PaymentAndSubmissionShield.propTypes = {
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired
};

export default PaymentAndSubmissionShield;
