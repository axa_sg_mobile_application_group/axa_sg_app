import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1
  }
});
