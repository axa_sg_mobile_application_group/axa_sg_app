import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import moment from "moment";
import * as _ from "lodash";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";

import { ContextConsumer } from "../../../../context";
import getEABTextByLangType, {
  languageTypeConvert
} from "../../../../utilities/getEABTextByLangType";

import EABCard from "../../../../components/EABCard";
import EABTable from "../../../../components/EABTable";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableFooter from "../../../../components/EABTableFooter";

import TranslatedText from "../../../../containers/TranslatedText";

import styles from "./styles";

const { PRE_APPLICATION, OPTIONS_MAP } = REDUCER_TYPES;
const { getCurrencySign } = utilities.common;
const { numberToCurrency } = utilities.numberFormatter;

/**
 * ApplicationItemShieldDetails
 * @description item in ApplicationsList inside Application Summary page.
 * @param {object} value - value of application / quotation item object
 * */

class ApplicationItemShieldDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderContentTableHeader = this.renderContentTableHeader.bind(this);
    this.renderContentTableSection = this.renderContentTableSection.bind(this);
    this.renderContentTableFooter = this.renderContentTableFooter.bind(this);
  }

  get sign() {
    const { optionsMap } = this.props;
    const { value } = this.props;
    const { ccy } = value;

    return getCurrencySign({
      compCode: "01",
      ccy,
      optionsMap
    });
  }

  renderContentTableHeader() {
    const { value } = this.props;
    const { id } = value;

    return (
      <EABTableHeader key={`${id}-header`}>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.summary.item_header_planName"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.summary.item_header_policyType"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.life_assured"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.summary.item_header_benefitAmount"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.summary.item_header_premiumInclMediLife"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            path="application.summary.item_header_premiumMode"
          />
        </View>
      </EABTableHeader>
    );
  }

  renderContentTableSection(consumerValue) {
    const { sign } = this;
    const { value, optionsMap } = this.props;
    const { id, type } = value;

    const showPlanDetails = [];
    let insureds = [];
    if (type === "masterApplication") {
      insureds = _.get(value, "quotation.insureds", []);
    } else {
      insureds = _.get(value, "insureds", []);
    }

    _.forEach(insureds, insured => {
      _.forEach(insured.plans, (plan, pIndex) => {
        const { covCode, covName, covClass } = plan;

        const premium = _.get(insured, `plans.${pIndex}.premium`);
        const policyType = _.get(
          optionsMap,
          `productLine.options[${_.get(
            plan,
            "productLine"
          )}].title.${languageTypeConvert(consumerValue.language)}`,
          ""
        );

        let benefitAmountPath = "";
        switch (covClass) {
          case "A":
            benefitAmountPath = "product.shield.planA";
            break;
          case "B":
            benefitAmountPath = "product.shield.planB";
            break;
          case "C":
            benefitAmountPath = "product.shield.standardPlan";
            break;
          default:
            benefitAmountPath = "";
            break;
        }

        showPlanDetails.push(
          <EABTableSection key={`${id}-${covCode}`}>
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>
                {getEABTextByLangType({
                  data: covName,
                  language: consumerValue.language
                })}
              </Text>
            </View>
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>{policyType}</Text>
            </View>
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>{insured.iFullName}</Text>
            </View>
            <View style={styles.tableSection}>
              <TranslatedText
                style={styles.tableSectionTitle}
                path={benefitAmountPath}
              />
            </View>
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle} numberOfLines={1}>
                {`${sign}${numberToCurrency({ value: premium })}`}
              </Text>
            </View>
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>RP</Text>
            </View>
          </EABTableSection>
        );
      });
    });

    return showPlanDetails;
  }

  renderContentTableFooter() {
    const { sign } = this;
    const { value } = this.props;
    const {
      id,
      lastUpdateDate,
      totMedisave,
      totCashPortion,
      totPremium,
      type
    } = value;

    let medisave = totMedisave;
    let cashPortion = totCashPortion;
    let premium = totPremium;

    if (type === "application" || type === "masterApplication") {
      medisave = _.get(value, "quotation.totMedisave");
      cashPortion = _.get(value, "quotation.totCashPortion");
      premium = _.get(value, "quotation.totPremium");
    }

    const tableConfig = [
      {
        key: `${id}-lastUpdateDate`,
        style: styles.tableColumnStart3
      },
      {
        key: `${id}-totalMedisave`,
        style: styles.tableColumnEnd2
      },
      {
        key: `${id}-totalCashPortion`,
        style: styles.tableColumnEnd2
      },
      {
        key: `${id}-totalPremiumInclMediLife`,
        style: styles.tableColumnEnd4
      }
    ];

    return (
      <EABTableFooter key={`${id}-footer`} tableConfig={tableConfig}>
        <View style={styles.tableFooter}>
          <Text style={styles.tableFooterTitle}>
            {moment(lastUpdateDate).format("DD MMM YYYY HH:mm:ss")}
          </Text>
        </View>
        <View style={styles.tableFooter}>
          <TranslatedText
            style={styles.tableFooterTitle}
            path="application.summary.item_footer_totalMedisave"
          />
          <Text style={styles.tableFooterValue} numberOfLines={1}>
            {`${sign}${numberToCurrency({ value: medisave })}`}
          </Text>
        </View>
        <View style={styles.tableFooter}>
          <TranslatedText
            style={styles.tableFooterTitle}
            path="application.summary.item_footer_totalCashPortion"
          />
          <Text style={styles.tableFooterValue} numberOfLines={1}>
            {`${sign}${numberToCurrency({ value: cashPortion })}`}
          </Text>
        </View>
        <View style={styles.tableFooter}>
          <TranslatedText
            style={styles.tableFooterTitle}
            path="application.summary.item_footer_totalPremiumInclMediLife"
          />
          <Text style={styles.tableFooterValue} numberOfLines={1}>
            {`${sign}${numberToCurrency({ value: premium })}`}
          </Text>
        </View>
      </EABTableFooter>
    );
  }

  render() {
    const { value } = this.props;
    const { id } = value;

    return (
      <ContextConsumer>
        {consumerValue => (
          <EABCard style={styles.container}>
            <EABTable
              tableConfig={[
                {
                  key: `${id}-plan_name`,
                  style: styles.tableColumnStart2
                },
                {
                  key: `${id}-policy_type`,
                  style: styles.tableColumnEnd
                },
                {
                  key: `${id}-life_assumed`,
                  style: styles.tableColumnEnd
                },
                {
                  key: `${id}-benefit_amount`,
                  style: styles.tableColumnEnd
                },
                {
                  key: `${id}-premium_incl_medi_life`,
                  style: styles.tableColumnEnd
                },
                {
                  key: `${id}-premium_mode`,
                  style: styles.tableColumnEnd
                }
              ]}
            >
              {this.renderContentTableHeader()}
              {this.renderContentTableSection(consumerValue)}
              {this.renderContentTableFooter()}
            </EABTable>
          </EABCard>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationItemShieldDetails.propTypes = {
  value: REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired
};

export default ApplicationItemShieldDetails;
