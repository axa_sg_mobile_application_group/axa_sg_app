import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  tableColumnStart2: {
    flex: 2,
    alignItems: "flex-start"
  },
  tableColumnStart3: {
    flex: 3,
    alignItems: "flex-start"
  },
  tableColumnEnd: {
    flex: 1,
    alignItems: "flex-end"
  },
  tableColumnEnd2: {
    flex: 2,
    alignItems: "flex-end"
  },
  tableColumnEnd4: {
    flex: 4,
    alignItems: "flex-end"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel,
    textAlign: "right"
  },
  tableSection: {
    backgroundColor: Theme.white
  },
  tableSectionTitle: {
    ...Theme.subheadPrimary,
    textAlign: "right"
  },
  tableFooter: {
    backgroundColor: Theme.white
  },
  tableFooterTitle: {
    ...Theme.subheadSecondary,
    textAlign: "right"
  },
  tableFooterValue: {
    ...Theme.subheadPrimary
  }
});
