import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  filterWrapper: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  filterView: {
    marginRight: Theme.alignmentL,
    borderRadius: Theme.radiusL,
    backgroundColor: Theme.white
  }
});
