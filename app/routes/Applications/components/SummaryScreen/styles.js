import { StyleSheet, Dimensions } from "react-native";
import Theme from "../../../../theme";
import { BOTTOM_SHEET_EXPAND_HEIGHT } from "./constants";

const SELECTOR_HEIGHT = 40 + 2 * Theme.alignmentXL;
const APP_LIST_HEIGHT_WITHOUT_BOTTOM_SHEET =
  Dimensions.get("window").height - 63.5 - SELECTOR_HEIGHT - 49;

const appListView = {
  alignItems: "center",
  justifyContent: "space-between"
};

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1
  },
  contentView: {
    height: "100%",
    alignItems: "center",
    justifyContent: "space-between"
  },
  appListViewWithBottomSheet: {
    ...appListView,
    flexBasis: APP_LIST_HEIGHT_WITHOUT_BOTTOM_SHEET - BOTTOM_SHEET_EXPAND_HEIGHT
  },
  appListViewWithoutBottomSheet: {
    ...appListView,
    flexBasis: APP_LIST_HEIGHT_WITHOUT_BOTTOM_SHEET
  },
  topAppListFilterView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL
  },
  contentContainerScrollView: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  scrollView: {
    height: "100%"
  },
  scrollViewContentSubview: {
    width: 880
  },
  selectorView: {
    flexBasis: SELECTOR_HEIGHT,
    width: 880
  },
  selectorsWrapper: {
    flexDirection: "row",
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL
  },
  selector: {
    marginRight: Theme.alignmentXL,
    width: 276
  },
  productListScrollView: {
    width: "100%"
  },
  midAppListFilterView: {
    marginBottom: Theme.alignmentL
  },
  bottomSheetContainer: {
    width: "100%"
  },
  bottomSheetView: {
    flexBasis: BOTTOM_SHEET_EXPAND_HEIGHT,
    flexDirection: "row",
    justifyContent: "center"
  },
  bottomSheetContentView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: 880
  },
  recommendContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  recommendIcon: {
    ...Theme.iconStyle,
    marginRight: Theme.alignmentXXS
  },
  noRecordsContainer: {
    flexDirection: "column",
    marginTop: 180
  },
  noRecordsView: {
    justifyContent: "center",
    alignItems: "center"
  },
  noRecordsTitle: {
    ...Theme.tagLine1Primary,
    marginTop: Theme.alignmentXL
  }
});
