import React, { PureComponent } from "react";
import {
  View,
  Text,
  NativeModules,
  NativeEventEmitter,
  Image,
  ScrollView,
  TouchableHighlight,
  Dimensions
} from "react-native";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK, EAPP } from "eab-web-api";
import PropTypes from "prop-types";
import * as _ from "lodash";
import icIncomplete from "../../../../assets/images/icIncomplete.png";
import icCompleted from "../../../../assets/images/icCompleted.png";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import SignDocDialogView from "../../../../components/SignDocDialog";
import { FORM } from "../../../../components/EABDialog/constants";
import EABDialog from "../../../../components/EABDialog/EABDialog.ios";
import EABSectionList from "../../../../components/EABSectionList";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import { ContextConsumer } from "../../../../context";
import TranslatedText from "../../../../containers/TranslatedText";
import EABi18n from "../../../../utilities/EABi18n";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import Theme from "../../../../theme";

import styles from "./styles";
import EABButton from "../../../../components/EABButton";

const { FNA, PROPOSAL, APPPDF } = EAPP.reportName;
const { NEXTPAGE } = EAPP.action;
/**
 * Signature
 * @description Signature in Application Summary page.
 * @param {array} Signature - array of product item object
 * */

class Signature extends PureComponent {
  static startSign(searchTag, page, options) {
    const signDocDialogManager = NativeModules.SignDocDialogManager;
    signDocDialogManager.showCaptureDialog(searchTag, page, options, () => {});
  }
  constructor(props) {
    super(props);
    const myModuleEvt = new NativeEventEmitter(
      NativeModules.SignDocEventEmitter
    );
    this.resolve = Promise.resolve();
    this.signedPdf = this.signedPdf.bind(this);

    myModuleEvt.addListener("SignDoc", this.signedPdf);
    this.state = {
      isLoading: !this.props.isShield,
      pdfInit: [],
      pdfImageArr: [],
      signatureListArr: [],
      pdfWidth: 0,
      updateAlertDelayed: false,
      showSignatureDialog: false,
      showPdf: true
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let { numOfTabs } = nextProps.signature;
    if (nextProps.signature.numOfTabs > 0 && prevState.pdfInit.length === 0) {
      return {
        pdfInit: (() => {
          const pdfInitArr = [];
          while (numOfTabs) {
            pdfInitArr.push(false);
            numOfTabs -= 1;
          }
          return pdfInitArr;
        })(),
        updateAlertDelayed: nextProps.isAlertDelayed
      };
    }

    return null;
  }

  componentDidMount() {
    const {
      getSignature,
      applicationId,
      isShield,
      saveForm,
      setEappLoading
    } = this.props;
    const successCallback = () => {
      setEappLoading(false, () => {
        setTimeout(() => {
          this.setState({
            isLoading: false
          });
        }, 300);
      });
    };
    const failCallback = () => {
      saveForm({
        genPDF: true,
        currentStep: 0,
        nextStep: 1,
        actionType: NEXTPAGE,
        callback: () => {
          setEappLoading(false, () => {
            setTimeout(() => {
              getSignature({ applicationId, successCallback });
            }, 300);
          });
        }
      });
    };
    if (applicationId && !isShield) {
      getSignature({
        applicationId,
        failCallback,
        successCallback
      });
    }
  }

  static getPdfImage(arr) {
    const pdfImageArr = [];
    // The h and w is the view size, base on the screen size,
    // determine the image height. 1.3 is just a guess number that works well for ipad air, ipad pro (10.5/12.9)
    const { height } = Dimensions.get("window");
    const h = height < 1000 ? height * 1.3 : height * 1.4; //
    const w = "100%";

    arr.forEach(base64 => {
      pdfImageArr.push(
        <Image
          key={base64}
          style={[
            styles.pdfImage,
            {
              width: w,
              height: h,
              resizeMode: "contain"
            }
          ]}
          source={{
            uri: `data:image/png;base64,${base64}`
          }}
        />
      );
    });
    return (
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={styles.scrollViewContent}
      >
        {pdfImageArr}
      </ScrollView>
    );
  }

  getPdf(language) {
    const { pdfInit, updateAlertDelayed } = this.state;
    const { signature, updateIsAlertDelayed, isShield } = this.props;
    const {
      selectedPdfIdx,
      signingTabIdx,
      attachments,
      agentSignFields,
      clientSignFields
    } = signature;

    if (
      _.isEmpty(attachments) ||
      selectedPdfIdx < 0 ||
      pdfInit.length === 0 ||
      this.state.isLoading ||
      !this.state.showPdf
    ) {
      return null;
    }

    // in shield case, if "Application Form - Insurability Info" did not fill in with correct input, then you can not continue to sign
    if (!attachments[selectedPdfIdx].pdfStr) {
      return (
        <View style={styles.completeFormWarning}>
          <TranslatedText
            style={styles.titleBarButtonTitle}
            language={language}
            path="application.signature.shield.completeFormWarning"
          />
        </View>
      );
    }

    [...pdfInit.keys()].forEach(key => {
      if (key !== selectedPdfIdx) {
        pdfInit[key] = false;
      }
    });

    if (!pdfInit[selectedPdfIdx]) {
      this.setState(
        {
          isLoading: true
        },
        () => {
          const { pdfStr, isSigned } = attachments[selectedPdfIdx];

          const searchKey = ({ signArr, keyArr, docType }) => {
            if (_.isEmpty(signArr)) {
              return "";
            }
            if (!_.isEmpty(docType) && isShield && docType === PROPOSAL) {
              return _.find(signArr, s =>
                keyArr.includes(
                  s.searchTag
                    .split(" ")
                    [s.searchTag.split(" ").length - 1].toUpperCase()
                )
              );
            }
            return _.find(signArr, s =>
              keyArr.includes(
                s.searchTag.split("_").length <= 2
                  ? s.searchTag.split("_")[0].toUpperCase()
                  : s.searchTag.split("_")[1].toUpperCase()
              )
            );
          };

          const getSignCase = ({ signArr }) => {
            const hasP = searchKey({
              signArr,
              keyArr: ["PROP", "PROPOSER", "CLIENT"]
            });
            const hasAgent = searchKey({
              signArr,
              keyArr: ["AGENT", "ADVISOR", "CONSULTANT"]
            });
            const hasTI = searchKey({ signArr, keyArr: ["TI"] });
            const hasLA = searchKey({ signArr, keyArr: ["LA"] });
            const hasSP = searchKey({ signArr, keyArr: ["SPOUSE"] });

            return {
              hasP,
              hasAgent,
              hasTI,
              hasLA,
              hasSP
            };
          };
          const getSigner = ({ tabKey, docType }) => {
            const signArr = [
              {
                searchTag: tabKey
              }
            ];
            const isP = searchKey({
              signArr,
              keyArr: ["PROP", "PROPOSER", "CLIENT"],
              docType
            });
            const isAgent = searchKey({
              signArr,
              keyArr: ["AGENT", "ADVISOR", "CONSULTANT"],
              docType
            });
            const isTI = searchKey({ signArr, keyArr: ["TI"], docType });
            const isLA = searchKey({ signArr, keyArr: ["LA"], docType });
            const isSP = searchKey({ signArr, keyArr: ["SPOUSE"], docType });

            if (isP) {
              return "PROPOSER";
            }
            if (isAgent) {
              return "ADVISOR";
            }
            if (isTI) {
              return "TI";
            }
            if (isLA) {
              return "LA";
            }
            if (isSP) {
              return "SPOUSE";
            }
            return "UNDEFINED";
          };
          const getSignPadX = ({ docID, signer, signCase }) => {
            if (docID.includes("appPdf")) {
              docID = `any_${APPPDF}`;
            }
            switch (docID.split("_")[1]) {
              case FNA:
                switch (signer) {
                  case "SPOUSE":
                    return -85;
                  default:
                    return -75;
                }
              case PROPOSAL:
                if (isShield) {
                  switch (signer) {
                    case "PROPOSER":
                      return -20;
                    case "ADVISOR":
                      return -60;
                    default:
                      return -40;
                  }
                }
                return -40;
              case APPPDF:
                switch (signer) {
                  case "PROPOSER":
                    if (signCase.hasTI && signCase.hasLA) {
                      return -50;
                    } else if (signCase.hasLA || signCase.hasTI) {
                      return -25;
                    }
                    return 25;
                  case "ADVISOR":
                    if (signCase.hasTI && signCase.hasLA) {
                      return -65;
                    } else if (
                      (signCase.hasLA || signCase.hasTI) &&
                      !isShield
                    ) {
                      return -40;
                    }
                    return 15;
                  case "TI":
                    if (signCase.hasLA) {
                      return -35;
                    }
                    return -10;
                  case "LA":
                    if (!isShield) {
                      if (signCase.hasTI) {
                        return -37.5;
                      }
                      return -10;
                    }
                    return 35;
                  default:
                    return 0;
                }
              default:
                return -30;
            }
          };
          const getSignPadY = ({ docID, signer, signCase }) => {
            if (docID.includes("appPdf")) {
              docID = `any_${APPPDF}`;
            }
            switch (docID.split("_")[1]) {
              case PROPOSAL:
                return isShield ? 40 : 25;
              case FNA:
              case APPPDF:
                return signer && signCase ? 25 : 25;
              default:
                return 25;
            }
          };
          const searchTagArr = !isSigned
            ? agentSignFields[selectedPdfIdx].concat(
                clientSignFields[selectedPdfIdx]
              )
            : [];

          const signDocDialogManager = NativeModules.SignDocDialogManager;

          signDocDialogManager.init(searchTagArr, pdfStr, (err, result) => {
            // set the selected pdf to true after the reponse
            const prevUpdateAlertDelayed = updateAlertDelayed;
            const newPdfInit = _.cloneDeep(pdfInit);
            newPdfInit[selectedPdfIdx] = true;

            this.setState(
              {
                updateAlertDelayed: false,
                pdfInit: newPdfInit,
                pdfImageArr: result.images,
                signatureListArr: result.tags.map(tab =>
                  Object.assign({}, tab, {
                    signed: false,
                    searchTag: tab.key,
                    signX: getSignPadX({
                      docID: attachments[selectedPdfIdx].docid,
                      signer: getSigner({
                        tabKey: tab.key,
                        docType: attachments[selectedPdfIdx].docid.split("_")[1]
                      }),
                      signCase: getSignCase({ signArr: searchTagArr })
                    }),
                    signY: getSignPadY({
                      docID: attachments[selectedPdfIdx].docid,
                      signer: getSigner({
                        tabKey: tab.key,
                        docType: attachments[selectedPdfIdx].docid.split("_")[1]
                      }),
                      signCase: getSignCase({ signArr: searchTagArr })
                    })
                  })
                )
              },
              () => {
                setTimeout(() => {
                  if (prevUpdateAlertDelayed) {
                    updateIsAlertDelayed(false);
                  }
                  this.setState({
                    isLoading: false
                  });
                }, 500);
              }
            );
          });
        }
      );
    }

    if (this.state.pdfImageArr) {
      return (
        <View
          style={styles.pdfImageView}
          onLayout={event => {
            this.setState({
              pdfWidth: event.nativeEvent.layout.width
            });
          }}
        >
          {this.state.pdfWidth !== 0
            ? Signature.getPdfImage(this.state.pdfImageArr)
            : null}
          {this.state.pdfWidth !== 0 && selectedPdfIdx === signingTabIdx ? (
            <View style={styles.signatureListButtonView}>
              <EABButton
                buttonType={RECTANGLE_BUTTON_1}
                onPress={() => {
                  this.setState({ showSignatureDialog: true });
                }}
              >
                <TranslatedText
                  style={Theme.title}
                  path="signature.dialog.title"
                />
              </EABButton>
            </View>
          ) : null}
        </View>
      );
    }
    return null;
  }

  signedPdf(result) {
    const { signatureListArr } = this.state;
    const { signature, saveSignedPdf, isShield } = this.props;
    const { selectedPdfIdx, attachments } = signature;
    const { images, tag, page, pdfBase64 } = result;

    const newSignatureListArr = _.cloneDeep(signatureListArr);
    const signedItemIndex = signatureListArr.findIndex(
      signatureItem =>
        signatureItem.page === page && signatureItem.searchTag === tag
    );
    if (signedItemIndex !== -1) {
      newSignatureListArr[signedItemIndex].signed = true;

      const nonSignedIndex = newSignatureListArr.findIndex(
        signatureItem => !signatureItem.signed
      );

      this.setState(
        {
          showPdf: false,
          pdfImageArr: images,
          signatureListArr: newSignatureListArr,
          isLoading: true
        },
        () => {
          if (nonSignedIndex < 0) {
            // save PDF
            const signdocIds = attachments[selectedPdfIdx].docid.split("_");

            const docId = signdocIds[0];
            const attId = signdocIds[1];

            saveSignedPdf({
              docId,
              attId,
              pdfData: pdfBase64,
              isShield,
              callback: () => {
                this.setState(
                  {
                    isLoading: false
                  },
                  () => {
                    setTimeout(() => {
                      this.setState({
                        showPdf: true
                      });
                    }, 300);
                  }
                );
              }
            });
          } else {
            this.setState(
              {
                isLoading: false
              },
              () => {
                setTimeout(() => {
                  this.setState({
                    showPdf: true
                  });
                }, 300);
              }
            );
          }
        }
      );
    }
  }

  prepareSectionListOptions(language) {
    const { signature, textStore, isShield, application } = this.props;
    const { selectedPdfIdx, attachments } = signature;
    const getPdfTitle = id => {
      switch (id) {
        case FNA:
          return EABi18n({
            language,
            textStore,
            path: "pdf.title.fnaReport"
          });
        case PROPOSAL:
          return EABi18n({
            language,
            textStore,
            path: "pdf.title.proposal"
          });
        case APPPDF:
          if (isShield) {
            return `${EABi18n({
              language,
              textStore,
              path: "pdf.title.appPdf"
            })} (${application.quotation.pFullName})`;
          }
          return EABi18n({
            language,
            textStore,
            path: "pdf.title.appPdf"
          });
        default:
          break;
      }
      if (isShield) {
        const profileId = `CP${id.split("CP")[1]}`;
        return `${EABi18n({
          language,
          textStore,
          path: "pdf.title.appPdf"
        })} (${application.quotation.insureds[profileId].iFullName})`;
      }
      return "";
    };
    const reports = [];
    attachments.forEach((attachment, index) => {
      const reportId = attachment.docid.split("_")[1];
      reports.push({
        key: reportId,
        id: index,
        title: getPdfTitle(reportId),
        selected: selectedPdfIdx === index,
        completed: attachment.isSigned
      });
    });
    return [
      {
        key: "fnaReport",
        data: reports,
        title: ""
      }
    ];
  }

  render() {
    const { switchPdf, signature, textStore } = this.props;
    if (_.isEmpty(signature)) {
      return null;
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.sectionListView}>
              <EABSectionList
                showCompleteIcon
                sections={this.prepareSectionListOptions(language)}
                onPress={id => {
                  switchPdf(id);
                }}
              />
            </View>
            <EABHUD isOpen={this.state.isLoading}>
              {this.getPdf(language)}
            </EABHUD>
            <View style={styles.SignDocDialogView}>
              <SignDocDialogView />
            </View>
            <EABDialog type={FORM} isOpen={this.state.showSignatureDialog}>
              <View style={Theme.dialogHeader}>
                <View style={Theme.headerRow}>
                  <EABButton
                    containerStyle={Theme.headerLeft}
                    onPress={() => {
                      this.setState({
                        showSignatureDialog: false
                      });
                    }}
                  >
                    <Text style={Theme.textButtonLabelNormalAccent}>
                      {EABi18n({
                        textStore,
                        language,
                        path: "button.cancel"
                      })}
                    </Text>
                  </EABButton>
                  <View>
                    <TranslatedText
                      style={Theme.title}
                      path="signature.dialog.title"
                    />
                  </View>
                </View>
              </View>
              <ScrollView style={styles.scrollView}>
                {this.state.signatureListArr.map(signatureItem => (
                  <TouchableHighlight
                    key={signatureItem.key}
                    disabled={signatureItem.signed}
                    underlayColor={Theme.brandSuperTransparent}
                    onPress={() => {
                      this.setState(
                        {
                          showSignatureDialog: false
                        },
                        () => {
                          Signature.startSign(
                            signatureItem.searchTag,
                            signatureItem.page,
                            {
                              signX: signatureItem.signX,
                              signY: signatureItem.signY
                            }
                          );
                        }
                      );
                    }}
                  >
                    <View style={styles.signatureRow}>
                      <Text style={Theme.subheadPrimary}>
                        {signatureItem.name}
                      </Text>
                      <Image
                        style={styles.image}
                        source={
                          signatureItem.signed ? icCompleted : icIncomplete
                        }
                      />
                    </View>
                  </TouchableHighlight>
                ))}
              </ScrollView>
            </EABDialog>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

Signature.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  signature: REDUCER_TYPE_CHECK[REDUCER_TYPES.SIGNATURE].signature.isRequired,
  getSignature: PropTypes.func.isRequired,
  switchPdf: PropTypes.func.isRequired,
  saveSignedPdf: PropTypes.func.isRequired,
  updateIsAlertDelayed: PropTypes.func.isRequired,
  applicationId: PropTypes.string.isRequired,
  isShield: PropTypes.bool.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  saveForm: PropTypes.func.isRequired,
  setEappLoading: PropTypes.func.isRequired
};

export default Signature;
