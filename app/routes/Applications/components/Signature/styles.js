import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    position: "relative",
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flexBasis: 312
  },
  container1: {
    flex: 1,
    width: "100%",
    height: "100%"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    paddingVertical: Theme.alignmentXL
  },
  signatureRow: {
    flexDirection: "row",
    paddingHorizontal: Theme.alignmentXL,
    alignItems: "center",
    justifyContent: "space-between",
    height: 60
  },
  image: {
    width: 24,
    height: 24
  },
  signDocDialogView: {
    position: "absolute",
    marginLeft: 210
  },
  pdfViewer: {
    flex: 1,
    height: "100%"
  },

  pdfImage: {
    marginBottom: Theme.alignmentL
  },
  signaturePanel: {
    flexBasis: "auto",
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: Theme.alignmentXS
  },
  completeFormWarning: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  pdfImageView: {
    flex: 1,
    height: "100%"
  },
  signatureListButtonView: {
    flexBasis: "auto",
    flexDirection: "row",
    justifyContent: "center",
    paddingVertical: Theme.alignmentXS
  },
  SignDocDialogView: {
    position: "absolute",
    marginLeft: 210
  }
});
