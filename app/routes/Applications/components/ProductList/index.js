import React, { PureComponent } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";

import TranslatedText from "../../../../containers/TranslatedText";
import ProductItem from "../../containers/ProductItem";
import styles from "./styles";

/**
 * ProductList
 * @description ProductList in Application Summary page.
 * @param {array} productList - array of product item object
 * */

class ProductList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { productList, cid } = this.props;
    const filterProductList = _.filter(
      productList,
      product => cid === product.iCid
    );

    return (
      <View style={styles.productsWrapper}>
        {filterProductList.length > 0 ? (
          filterProductList.map(product => (
            <ProductItem
              key={`${product.iCid}-${product.covCode}`}
              productName={product.covName}
              productId={product.id}
              attachmentId={product.attachmentId}
            />
          ))
        ) : (
          <View style={styles.noProductView}>
            <TranslatedText
              path="application.summary.no_proposed_product"
              funcOnText={_.toUpper}
            />
          </View>
        )}
      </View>
    );
  }
}

ProductList.propTypes = {
  productList: PropTypes.arrayOf(
    PropTypes.shape({
      covCode: PropTypes.string.isRequired,
      covName: PropTypes.object,
      iCid: PropTypes.string.isRequired,
      pCid: PropTypes.string.isRequired,
      ccy: PropTypes.string,
      attachmentId: PropTypes.string.isRequired
    })
  ).isRequired,
  cid: PropTypes.string.isRequired
};

export default ProductList;
