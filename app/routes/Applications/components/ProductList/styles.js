import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  productsWrapper: {
    flexDirection: "row"
  },
  noProductView: {
    justifyContent: "center",
    height: 164
  }
});
