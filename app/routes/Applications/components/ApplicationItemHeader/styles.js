import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  headerLeft: {
    flexDirection: "row",
    alignItems: "center",
    minHeight: 50,
    marginLeft: Theme.alignmentL,
    flex: 1,
    flexWrap: "wrap"
  },
  headerRight: {
    flexDirection: "row",
    alignItems: "center",
    minHeight: 50,
    flexBasis: "auto"
  },
  idTitle: {
    ...Theme.headerPrimary
  },
  buttonTitle: {
    ...Theme.textButtonLabelSmallAccent,
    marginLeft: 2,
    marginRight: Theme.alignmentXL
  },
  checkBox: {
    backgroundColor: "rgba(0,0,0,0)",
    marginRight: Theme.alignmentXL
  },
  titleBarIcon: {
    ...Theme.iconStyle,
    marginRight: Theme.alignmentXXS
  },
  invalidateReasonIcon: {
    marginRight: Theme.alignmentS,
    width: 24,
    height: 24
  }
});
