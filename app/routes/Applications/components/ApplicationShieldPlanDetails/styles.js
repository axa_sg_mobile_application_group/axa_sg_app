import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: Theme.alignmentXL
  },
  tableSectionHeader: {
    justifyContent: "center"
  },
  tableSection: {
    flexDirection: "row",
    backgroundColor: Theme.white
  },
  tableSectionColumn1: {
    flex: 3,
    justifyContent: "flex-start"
  },
  tableSectionColumn2: {
    flex: 1,
    justifyContent: "flex-start"
  },
  tableSectionColumn3: {
    flex: 1,
    justifyContent: "flex-start"
  },
  tableSectionColumn4: {
    flex: 1,
    justifyContent: "flex-start"
  },
  tableSectionColumn5: {
    flex: 2,
    justifyContent: "flex-start"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableFooterColumn1: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  tableFooterColumn2: {
    flex: 4,
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  tableFooterDateView: {
    backgroundColor: Theme.white
  },
  tableFooterValueView: {
    backgroundColor: Theme.white,
    flexDirection: "row",
    justifyContent: "flex-end",
    flexWrap: "wrap"
  },
  tableFooterColumnDate: {
    ...Theme.subheadSecondary,
    textAlign: "left"
  },
  tableFooterDataView: {
    flexDirection: "row"
  },
  tableFooterDataTitle: {
    ...Theme.subheadSecondary,
    textAlign: "right",
    marginLeft: Theme.alignmentL,
    marginRight: Theme.alignmentL
  },
  tableFooterDataValue: {
    ...Theme.subheadPrimary,
    textAlign: "right",
    flexBasis: 130
  },
  totalRiderPremiumText: {
    ...Theme.tableColumnLabel,
    fontSize: Theme.fontSizeXM,
    marginRight: Theme.alignmentXS,
    textAlign: "right"
  },
  premiumView: {
    flexDirection: "column"
  },
  left: {
    alignItems: "flex-start"
  },
  right: {
    alignItems: "flex-end"
  }
});
