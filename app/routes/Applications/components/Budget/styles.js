import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const oval = {
  marginTop: Theme.alignmentM,
  marginRight: Theme.alignmentM,
  width: 12,
  height: 12,
  borderRadius: 50
};
/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flexBasis: 312
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    flexGrow: 1,
    alignItems: "center"
  },
  budgetContentView: {
    flex: 1,
    width: Theme.boxMaxWidth
  },
  textSelection: {
    marginTop: 14
  },
  compareView: {
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL
  },
  compareChartView: {
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL
  },
  compareDetailsView: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  compareDetailsBlockView: {
    borderTopWidth: 1,
    marginRight: 66,
    width: 224
  },
  compareDetailsTitleView: {
    flexDirection: "row"
  },
  compareDetailsTitle: {
    ...Theme.indicatorPrimary,
    marginTop: Theme.alignmentM - 3
  },
  compareDetailsTitleNegative: {
    ...Theme.indicatorNegative,
    marginTop: Theme.alignmentM - 3
  },
  compareDetailsValue: {
    ...Theme.bodySecondary,
    marginLeft: Theme.alignmentXL
  },
  compareDetailsValueNegative: {
    ...Theme.bodyNegative,
    marginLeft: Theme.alignmentXL
  },
  OvalAccentView: {
    ...oval,
    backgroundColor: Theme.accent
  },
  OvalBrandView: {
    ...oval,
    backgroundColor: Theme.brand
  },
  OvalBrandTransparentView: {
    ...oval,
    backgroundColor: Theme.brandTransparent
  },
  blockView: {
    justifyContent: "space-between",
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL
  },
  choiceReasonTitle: {
    width: 664
  },
  textBox: {
    width: 664,
    backgroundColor: Theme.white
  },
  requiredHint: {
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  additionCommentWrapper: {
    maxWidth: 664,
    width: "100%"
  },
  budgetNote: {
    fontWeight: "700",
    color: Theme.negative
  },
  budgetNoteText: {
    color: Theme.negative
  },
  multiCcyNoteContainer: {
    paddingVertical: Theme.alignmentXL,
    paddingLeft: Theme.alignmentXL
  },
  multiCcyNote: {
    color: Theme.negative
  },
  compareDetailsTitleAligned: {
    ...Theme.indicatorSecondary,
    marginTop: Theme.alignmentM - 3
  }
});
