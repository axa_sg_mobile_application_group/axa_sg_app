import React, { PureComponent } from "react";
import { View, ScrollView, Text } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  TOLERANCE_LEVEL,
  utilities
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import EABTextBox from "../../../../components/EABTextBox";
import EABTextSelection from "../../../../components/EABTextSelection";
import EABFormSectionHeader from "../../../../components/EABFormSectionHeader";
import EABStackedBar from "../../../../components/EABStackedBar";
import EABi18n from "../../../../utilities/EABi18n";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import styles from "./styles";

const { RECOMMENDATION, OPTIONS_MAP } = REDUCER_TYPES;
const { getCurrencySign } = utilities.common;
const { numberToCurrency } = utilities.numberFormatter;

/**
 * Budget
 * @description Budget page.
 * @param {object} textStore - reducer textStore state
 * @param {object} value - data for Budget page
 * @param {boolean} disabled - change all UI to non-editable
 * @param {func} validateBudget - validation Budget when component is ready
 * @param {func} updateBudgetData - update Recommendation state with inputted data
 * */

class Budget extends PureComponent {
  constructor(props) {
    super(props);

    this.triggerShowBudgetMore = this.triggerShowBudgetMore.bind(this);
    this.triggerShowBudgetLess = this.triggerShowBudgetLess.bind(this);
    this.updateStateValue = this.updateStateValue.bind(this);
    this.renderCompareResult = this.renderCompareResult.bind(this);
    this.renderCompareResultDetails = this.renderCompareResultDetails.bind(
      this
    );
    this.renderChoiceReason = this.renderChoiceReason.bind(this);

    this.scrollView = React.createRef();

    this.state = {};
  }

  componentDidMount() {
    this.props.validateBudget();
  }

  get sign() {
    const { optionsMap } = this.props;

    return getCurrencySign({
      compCode: "01",
      ccy: "SGD",
      optionsMap
    });
  }

  triggerShowBudgetMore() {
    const { value } = this.props;
    return (
      _.get(value, "spCompare") < TOLERANCE_LEVEL.NEGATIVE ||
      _.get(value, "rpCompare") < TOLERANCE_LEVEL.NEGATIVE ||
      _.get(value, "cpfOaCompare") < TOLERANCE_LEVEL.NEGATIVE ||
      _.get(value, "cpfSaCompare") < TOLERANCE_LEVEL.NEGATIVE ||
      _.get(value, "srsCompare") < TOLERANCE_LEVEL.NEGATIVE ||
      _.get(value, "cpfMsCompare") < TOLERANCE_LEVEL.NEGATIVE
    );
  }

  triggerShowBudgetLess() {
    const { value } = this.props;
    return (
      _.get(value, "spCompare") > TOLERANCE_LEVEL.POSITIVE ||
      _.get(value, "rpCompare") > TOLERANCE_LEVEL.POSITIVE ||
      _.get(value, "cpfOaCompare") > TOLERANCE_LEVEL.POSITIVE ||
      _.get(value, "cpfSaCompare") > TOLERANCE_LEVEL.POSITIVE ||
      _.get(value, "srsCompare") > TOLERANCE_LEVEL.POSITIVE ||
      _.get(value, "cpfMsCompare") > TOLERANCE_LEVEL.POSITIVE
    );
  }

  updateStateValue(path, newPathValue) {
    const { updateBudgetData, value } = this.props;
    const newValue = _.cloneDeep(value);
    _.set(newValue, path, newPathValue);

    // reset hidden field value
    if (path === "budgetMoreChoice" && newPathValue === "N") {
      _.set(newValue, "budgetMoreReason", "");
    }
    if (path === "budgetLessChoice" && newPathValue === "N") {
      _.set(newValue, "budgetLessReason", "");
    }

    updateBudgetData(newValue);
  }

  renderCompareResultDetails({ budgetId, premiumId, compareId, maxValue }) {
    const { sign } = this;
    const { value } = this.props;
    const compareValue = _.get(value, `${compareId}`);

    let compareOval = null;
    let compareResult = null;
    if (compareValue < TOLERANCE_LEVEL.NEGATIVE) {
      compareOval = <View style={styles.OvalBrandView} />;
      compareResult = (
        <TranslatedText
          style={styles.compareDetailsTitleNegative}
          path="recommendation.budget.title.compare.exceeded"
          funcOnText={_.toUpper}
        />
      );
    } else if (compareValue > TOLERANCE_LEVEL.POSITIVE) {
      compareOval = <View style={styles.OvalBrandTransparentView} />;
      compareResult = (
        <TranslatedText
          style={styles.compareDetailsTitleNegative}
          path="recommendation.budget.title.compare.underutilized"
          funcOnText={_.toUpper}
        />
      );
    } else {
      compareOval = <View style={styles.OvalBrandTransparentView} />;
      compareResult = (
        <TranslatedText
          style={styles.compareDetailsTitleAligned}
          path="recommendation.budget.title.compare.aligned"
          funcOnText={_.toUpper}
        />
      );
    }

    return (
      <View>
        <View style={styles.compareChartView}>
          <EABStackedBar
            values={{
              indicator: _.get(value, `${budgetId}`),
              topLayer: _.get(value, `${premiumId}`),
              bottomLayer: _.get(value, `${budgetId}`),
              max: maxValue
            }}
            isBottomLarger={compareValue > TOLERANCE_LEVEL.POSITIVE}
          />
        </View>
        <View style={styles.compareDetailsView}>
          <View style={styles.compareDetailsBlockView}>
            <View style={styles.compareDetailsTitleView}>
              <View style={styles.OvalAccentView} />
              <TranslatedText
                style={styles.compareDetailsTitle}
                path={`recommendation.budget.title.${budgetId}`}
                funcOnText={_.toUpper}
              />
            </View>
            <Text style={styles.compareDetailsValue}>
              {`${sign}${numberToCurrency({
                value: _.get(value, `${budgetId}`)
              })}`}
            </Text>
          </View>
          <View style={styles.compareDetailsBlockView}>
            <View style={styles.compareDetailsTitleView}>
              <View style={styles.OvalBrandView} />
              <TranslatedText
                style={styles.compareDetailsTitle}
                path={`recommendation.budget.title.${premiumId}`}
                funcOnText={_.toUpper}
              />
            </View>
            <Text style={styles.compareDetailsValue}>
              {`${sign}${numberToCurrency({
                value: _.get(value, `${premiumId}`)
              })}`}
            </Text>
          </View>
          {compareValue < TOLERANCE_LEVEL.NEGATIVE ||
          compareValue > TOLERANCE_LEVEL.POSITIVE ? (
            <View style={styles.compareDetailsBlockView}>
              <View style={styles.compareDetailsTitleView}>
                {compareOval}
                {compareResult}
              </View>
              <Text style={styles.compareDetailsValueNegative}>
                {`${sign}${numberToCurrency({
                  value: Math.abs(compareValue)
                })}`}
              </Text>
            </View>
          ) : (
            <View style={styles.compareDetailsBlockView}>
              <View style={styles.compareDetailsTitleView}>
                {compareOval}
                {compareResult}
              </View>
            </View>
          )}
        </View>
      </View>
    );
  }

  renderCompareResult() {
    const { value } = this.props;
    const maxValue = _.max([
      _.get(value, "spBudget"),
      _.get(value, "spTotalPremium"),
      _.get(value, "rpBudget"),
      _.get(value, "rpTotalPremium"),
      _.get(value, "cpfOaBudget"),
      _.get(value, "cpfOaTotalPremium"),
      _.get(value, "cpfSaBudget"),
      _.get(value, "cpfSaTotalPremium"),
      _.get(value, "srsBudget"),
      _.get(value, "srsTotalPremium"),
      _.get(value, "cpfMsBudget"),
      _.get(value, "cpfMsTotalPremium")
    ]);

    return (
      <View style={styles.compareView}>
        {this.renderCompareResultDetails({
          budgetId: "spBudget",
          premiumId: "spTotalPremium",
          compareId: "spCompare",
          maxValue
        })}
        {this.renderCompareResultDetails({
          budgetId: "rpBudget",
          premiumId: "rpTotalPremium",
          compareId: "rpCompare",
          maxValue
        })}
        {this.renderCompareResultDetails({
          budgetId: "cpfOaBudget",
          premiumId: "cpfOaTotalPremium",
          compareId: "cpfOaCompare",
          maxValue
        })}
        {this.renderCompareResultDetails({
          budgetId: "cpfSaBudget",
          premiumId: "cpfSaTotalPremium",
          compareId: "cpfSaCompare",
          maxValue
        })}
        {this.renderCompareResultDetails({
          budgetId: "srsBudget",
          premiumId: "srsTotalPremium",
          compareId: "srsCompare",
          maxValue
        })}
        {this.renderCompareResultDetails({
          budgetId: "cpfMsBudget",
          premiumId: "cpfMsTotalPremium",
          compareId: "cpfMsCompare",
          maxValue
        })}
      </View>
    );
  }

  renderChoiceReason(language) {
    const { textStore, value, disabled } = this.props;
    const valueBudgetLessChoice = _.get(value, "budgetLessChoice");
    const valueBudgetMoreChoice = _.get(value, "budgetMoreChoice");

    return (
      <View>
        {this.triggerShowBudgetLess() ? (
          <View style={styles.blockView}>
            <EABFormSectionHeader
              testID="PreAppilcation__Recommendation__LessThanBudget__lblSectionHeader"
              isRequired
              style={styles.choiceReasonTitle}
              title={EABi18n({
                path: "recommendation.budget.title.budgetLessChoice",
                language,
                textStore
              })}
            />
            <View style={styles.textSelection}>
              <EABTextSelection
                testID="PreAppilcation__Recommendation__LessThanBudget"
                options={[
                  {
                    key: "budgetLessChoice-yes",
                    testID: "Yes",
                    title: EABi18n({
                      path: "textSelection.yes",
                      language,
                      textStore
                    }),
                    isSelected: valueBudgetLessChoice === "Y"
                  },
                  {
                    key: "budgetLessChoice-no",
                    testID: "No",
                    title: EABi18n({
                      path: "textSelection.no",
                      language,
                      textStore
                    }),
                    isSelected: valueBudgetLessChoice === "N"
                  }
                ]}
                editable={!disabled}
                onPress={option => {
                  const newValue =
                    option.key === "budgetLessChoice-yes" ? "Y" : "N";
                  this.updateStateValue("budgetLessChoice", newValue);
                }}
              />
            </View>
            {valueBudgetLessChoice === "Y" ? (
              <View style={styles.additionCommentWrapper}>
                <EABTextBox
                  testID="PreAppilcation__Recommendation__LessThanBudget__txtReason"
                  style={styles.textBox}
                  numberOfLines={5}
                  editable={!disabled}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "recommendation.placeholder.PleaseStateReason(s)"
                  })}
                  maxLength={300}
                  value={_.get(value, "budgetLessReason")}
                  onChange={newValue => {
                    this.updateStateValue("budgetLessReason", newValue);
                  }}
                />
                {this.props.error.budgetLessReason.hasError ? (
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    language={language}
                    path="error.302"
                  />
                ) : null}
              </View>
            ) : null}
          </View>
        ) : null}
        {this.triggerShowBudgetMore() ? (
          <View style={styles.blockView}>
            <EABFormSectionHeader
              testID="PreAppilcation__Recommendation__MoreThanBudget__lblSectionHeader"
              isRequired
              style={styles.choiceReasonTitle}
              title={EABi18n({
                path: "recommendation.budget.title.budgetMoreChoice",
                language,
                textStore
              })}
            />
            <View style={styles.textSelection}>
              <EABTextSelection
                testID="PreAppilcation__Recommendation__MoreThanBudget"
                options={[
                  {
                    key: "budgetMoreChoice-yes",
                    testID: "Yes",
                    title: EABi18n({
                      path: "textSelection.yes",
                      language,
                      textStore
                    }),
                    isSelected: valueBudgetMoreChoice === "Y"
                  },
                  {
                    key: "budgetMoreChoice-no",
                    testID: "No",
                    title: EABi18n({
                      path: "textSelection.no",
                      language,
                      textStore
                    }),
                    isSelected: valueBudgetMoreChoice === "N"
                  }
                ]}
                editable={!disabled}
                onPress={option => {
                  const newValue =
                    option.key === "budgetMoreChoice-yes" ? "Y" : "N";
                  this.updateStateValue("budgetMoreChoice", newValue);
                }}
              />
            </View>
            {valueBudgetMoreChoice === "Y" ? (
              <View style={styles.additionCommentWrapper}>
                <EABTextBox
                  testID="PreAppilcation__Recommendation__MoreThanBudget__txtReason"
                  style={styles.textBox}
                  numberOfLines={5}
                  editable={!disabled}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "recommendation.placeholder.PleaseStateReason(s)"
                  })}
                  maxLength={300}
                  value={_.get(value, "budgetMoreReason")}
                  onChange={newValue => {
                    this.updateStateValue("budgetMoreReason", newValue);
                  }}
                />
                {this.props.error.budgetMoreReason.hasError ? (
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    language={language}
                    path="error.302"
                  />
                ) : null}
              </View>
            ) : null}
          </View>
        ) : null}
      </View>
    );
  }

  renderMultiCcyNote(language) {
    const { value, textStore } = this.props;
    if (value.multiCcyNote) {
      const currencyOrder = ["AUD", "GBP", "USD", "EUR"];
      const multiCcyNoteJSX = value.multiCcyNote.map((currency, index) => (
        <Text style={styles.multiCcyNote}>{`\u2022 ${EABi18n({
          path: "recommendation.budget.title.budgetCurrency",
          language,
          textStore
        })} ${currency} \t${currencyOrder[index]}`}</Text>
      ));

      return (
        <React.Fragment>
          <Text style={styles.budgetNote}>
            {EABi18n({
              path: "recommendation.budget.title.budgetNote",
              language,
              textStore
            })}
          </Text>
          <Text style={styles.budgetNoteText}>
            {EABi18n({
              path: "recommendation.budget.title.budgetNoteText",
              language,
              textStore
            })}
          </Text>
          <View style={styles.multiCcyNoteContainer}>{multiCcyNoteJSX}</View>
        </React.Fragment>
      );
    }
    return null;
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => (
          <KeyboardAwareScrollView extraScrollHeight={40}>
            <View style={styles.container}>
              <ScrollView
                testID="PreAppilcation__Recommendation__Budget__PageScroll"
                ref={this.scrollView}
                style={styles.scrollView}
                contentContainerStyle={styles.scrollViewContent}
                showsVerticalScrollIndicator={false}
              >
                <View style={styles.budgetContentView}>
                  {this.renderCompareResult()}
                  {this.renderChoiceReason(language)}
                  {this.renderMultiCcyNote(language)}
                </View>
              </ScrollView>
            </View>
          </KeyboardAwareScrollView>
        )}
      </ContextConsumer>
    );
  }
}

Budget.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  optionsMap: WEB_API_REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  value: REDUCER_TYPE_CHECK[RECOMMENDATION].budget.value.isRequired,
  error: REDUCER_TYPE_CHECK[RECOMMENDATION].budget.error.isRequired,
  disabled: PropTypes.bool.isRequired,
  validateBudget: PropTypes.func.isRequired,
  updateBudgetData: PropTypes.func.isRequired
};

export default Budget;
