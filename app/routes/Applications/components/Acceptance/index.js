import React, { PureComponent } from "react";
import { View, ScrollView, Text } from "react-native";
import * as _ from "lodash";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import { ContextConsumer } from "../../../../context";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import EABCard from "../../../../components/EABCard";
import TranslatedText from "../../../../containers/TranslatedText";

import styles from "./styles";

const { RECOMMENDATION } = REDUCER_TYPES;

/**
 * Acceptance
 * @description Acceptance page.
 * @param {object} value - data for Acceptance page
 * */

class Acceptance extends PureComponent {
  static renderNormalPlanItem({ item, language }) {
    return (
      <View key={item.quotationDocId} style={styles.selectedPlansTableView}>
        <View style={styles.selectedPlansTableRowView}>
          <View style={styles.selectedPlansTableRowValueViewTop}>
            <Text style={styles.selectedPlansTableRowValue}>
              {item.proposerAndLifeAssuredName}
            </Text>
          </View>
        </View>
        <View style={styles.selectedPlansTableRowView}>
          <View style={styles.selectedPlansTableRowValueViewTop}>
            <Text style={styles.selectedPlansTableRowValue}>
              {item.basicPlanName}
            </Text>
          </View>
        </View>
        <View style={styles.selectedPlansTableRowView}>
          <View style={styles.selectedPlansTableRowValueViewBottom}>
            <Text style={styles.selectedPlansTableRowValue}>
              {_.map(
                item.ridersName,
                (riderName, i) =>
                  `${i > 0 ? ", " : ""}${getEABTextByLangType({
                    data: riderName,
                    language
                  })}`
              )}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  static renderShieldPlanItem({ item }) {
    const { groupedShieldInsuredPlans } = item;
    const groupedPlans = [];

    _.forEach(groupedShieldInsuredPlans, (groupPlan, index) => {
      groupedPlans.push(
        <View
          style={
            index === groupedShieldInsuredPlans.length - 1
              ? styles.selectedPlansTableRowValueViewTop
              : styles.selectedPlansTableRowValueViewBottom
          }
        >
          <View>
            <Text style={styles.selectedPlansTableRowValue}>
              {_.map(
                groupPlan.insuredNameList,
                (insuredName, i) => `${i > 0 ? ", " : ""}${insuredName}`
              )}
            </Text>
          </View>
          <View>
            <Text style={styles.selectedPlansTableRowValue}>
              {_.map(
                groupPlan.planNameList,
                (planName, i) => `${i > 0 ? ", " : ""}${planName}`
              )}
            </Text>
          </View>
        </View>
      );
    });

    return (
      <View
        key={item.quotationDocId}
        style={styles.selectedPlansTableViewShield}
      >
        {groupedPlans}
      </View>
    );
  }

  constructor(props) {
    super(props);

    this.renderSelectedPlans = this.renderSelectedPlans.bind(this);

    this.scrollView = React.createRef();

    this.state = {};
  }

  renderSelectedPlans(language) {
    const { value } = this.props;
    return (
      <View style={styles.selectedPlansView}>
        <TranslatedText
          style={styles.selectedPlansStatement}
          path="recommendation.acceptance.selectedPlan.statement.title"
        />
        {_.map(
          value,
          item =>
            item.isShield
              ? Acceptance.renderShieldPlanItem({ item })
              : Acceptance.renderNormalPlanItem({ item, language })
        )}
      </View>
    );
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <ScrollView
              ref={this.scrollView}
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
              showsVerticalScrollIndicator={false}
            >
              <View style={styles.acceptanceContentView}>
                <EABCard style={styles.card} contentStyle={styles.cardContent}>
                  <TranslatedText
                    style={styles.statementTitle}
                    path="recommendation.acceptance.statement.title"
                  />
                  <View style={styles.statementView}>
                    <View style={styles.statementContentColumnLeft}>
                      <View style={styles.statementParagraphView}>
                        <Text style={styles.statementParagraphPoint}>1.</Text>
                        <TranslatedText
                          style={styles.statementParagraphContent}
                          path="recommendation.acceptance.statement.paragraph1"
                        />
                      </View>
                      <View style={styles.statementParagraphView}>
                        <Text style={styles.statementParagraphPoint}>2.</Text>
                        <TranslatedText
                          style={styles.statementParagraphContent}
                          path="recommendation.acceptance.statement.paragraph2"
                        />
                      </View>
                      <View style={styles.statementParagraphView}>
                        <Text style={styles.statementParagraphPoint}>3.</Text>
                        <TranslatedText
                          style={styles.statementParagraphContent}
                          path="recommendation.acceptance.statement.paragraph3"
                        />
                      </View>
                      <View style={styles.statementParagraphView}>
                        <Text style={styles.statementParagraphPoint}>4.</Text>
                        <TranslatedText
                          style={styles.statementParagraphContent}
                          path="recommendation.acceptance.statement.paragraph4"
                        />
                      </View>
                    </View>
                  </View>
                </EABCard>
                {this.renderSelectedPlans(language)}
              </View>
            </ScrollView>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

Acceptance.propTypes = {
  value: REDUCER_TYPE_CHECK[RECOMMENDATION].acceptance.value.isRequired
};

export default Acceptance;
