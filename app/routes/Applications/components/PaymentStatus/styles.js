import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    marginTop: Theme.alignmentXS,
    width: "100%"
  },
  statusWrapper: {
    marginBottom: Theme.alignmentL
  },
  statusTitle: {
    ...Theme.fieldTextOrHelperTextNegative,
    marginBottom: Theme.alignmentXXS
  },
  statusText: {
    ...Theme.fieldTextOrHelperTextNegative,
    fontWeight: "bold"
  },
  dataWrapper: {
    display: "flex",
    flexDirection: "row",
    width: "100%"
  },
  timeWrapper: {
    marginRight: Theme.alignmentL
  },
  noWrapper: {
    flex: 1
  }
});
