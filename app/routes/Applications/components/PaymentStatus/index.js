import { EAPP, REDUCER_TYPES } from "eab-web-api";
import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { View, Text } from "react-native";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";

/**
 * @description Payment status in Payment section.
 * @param {object} paymentValues - payment status values
 * */
export default class PaymentStatus extends Component {
  constructor(props) {
    super(props);

    this.getStatusDescription = this.getStatusDescription.bind(this);
  }
  /**
   * @description get payment variables
   * @return {object}
   * */
  /**
   * @description indicate show component or not
   * @requires CREDIT_CARD
   * @requires E_NETS
   * @requires DBSCRCARDIPP
   * @return {boolean}
   * */
  get shouldShowComponent() {
    const { onlinePaymentStatus } = this.props;
    const { trxStatusRemark, initPayMethod } = onlinePaymentStatus;

    return (
      [
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].CREDIT_CARD,
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].E_NETS,
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].DBSCRCARDIPP
      ].indexOf(initPayMethod) !== -1 &&
      trxStatusRemark &&
      trxStatusRemark !== ""
    );
  }
  /**
   * @description get transaction time, handle by moment.js
   * @requires moment
   * @return {string}
   * */
  get transactionTime() {
    const { onlinePaymentStatus } = this.props;
    const { trxTime } = onlinePaymentStatus;

    return trxTime !== 0 ? moment(trxTime).format("MMM DD, YYYY hh:mm A") : "";
  }
  /**
   * @description get the status description text
   * @return {string}
   * */
  getStatusDescription(language) {
    const { textStore, onlinePaymentStatus } = this.props;
    const { trxStatusRemark, trxMethod } = onlinePaymentStatus;

    switch (trxStatusRemark) {
      case "I":
        return EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.paymentStatus.i"
        });
      case "Y":
        return EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.paymentStatus.y"
        });
      case "N":
        return EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.paymentStatus.n"
        });
      case "C":
        return EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.paymentStatus.c"
        });
      case "O": {
        switch (trxMethod) {
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].CREDIT_CARD:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.o.crCard"
            });
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].E_NETS:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.o.eNets"
            });
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].DBSCRCARDIPP:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.o.dbsCrCardIpp"
            });
          default:
            throw new Error(
              `unexpected data: trxStatusRemark: O, trxMethod: ${trxMethod}`
            );
        }
      }
      case "E": {
        switch (trxMethod) {
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].CREDIT_CARD:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.e.crCard"
            });
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].E_NETS:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.e.eNets"
            });
          case EAPP.fieldValue[REDUCER_TYPES.PAYMENT].DBSCRCARDIPP:
            return EABi18n({
              language,
              textStore,
              path: "paymentAndSubmission.paymentStatus.e.dbsCrCardIpp"
            });
          default:
            throw new Error(
              `unexpected data: trxStatusRemark: E, trxMethod: ${trxMethod}`
            );
        }
      }
      default:
        throw new Error(`unexpected data: trxStatusRemark: ${trxStatusRemark}`);
    }
  }

  render() {
    const { shouldShowComponent, getStatusDescription, transactionTime } = this;
    const { onlinePaymentStatus } = this.props;
    const { trxNo } = onlinePaymentStatus;

    return shouldShowComponent ? (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.statusWrapper}>
              <TranslatedText
                style={styles.statusTitle}
                path="paymentAndSubmission.paymentStatus.title"
              />
              <Text style={styles.statusText}>
                {getStatusDescription(language)}
              </Text>
            </View>
            <View style={styles.dataWrapper}>
              <View style={styles.timeWrapper}>
                <TranslatedText
                  style={
                    transactionTime
                      ? Theme.fieldTextOrHelperTextNegative
                      : Theme.fieldTextOrHelperTextNormal
                  }
                  path="paymentAndSubmission.paymentStatus.transactionTime"
                />
                <Text style={Theme.fieldTextOrHelperTextNegative}>
                  {transactionTime}
                </Text>
              </View>
              <View style={styles.noWrapper}>
                <TranslatedText
                  style={
                    trxNo
                      ? Theme.fieldTextOrHelperTextNegative
                      : Theme.fieldTextOrHelperTextNormal
                  }
                  path="paymentAndSubmission.paymentStatus.transactionReferenceNo"
                />
                <Text style={Theme.fieldTextOrHelperTextNegative}>{trxNo}</Text>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    ) : null;
  }
}

PaymentStatus.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired
};
