import React, { PureComponent } from "react";
import { View } from "react-native";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import EABButton from "../../components/EABButton";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";

import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import SummaryScreen from "./containers/SummaryScreen";
import ApplicationEmailSection from "./containers/ApplicationEmailSection";
import ApplicationDeleteButton from "./containers/ApplicationDeleteButton";

/**
 * <Applications />
 * @reducer navigation.applications
 * @requires Applications - Applications index page
 * */

export const ApplicationNavigator = createStackNavigator(
  {
    Summary: {
      screen: SummaryScreen,
      navigationOptions: ({ navigation }) => {
        const renderHeaderLeft = () => {
          const selectModeOn = navigation.getParam("selectModeOn");
          const selectedIdCount = navigation.getParam("selectedIdCount");

          return (
            <ContextConsumer>
              {({ hideClientNavigator, setEappLoading }) =>
                selectModeOn ? (
                  <View style={{ flexDirection: "row" }}>
                    <ApplicationEmailSection canEmail={selectedIdCount === 0} />
                    <ApplicationDeleteButton
                      navigation={navigation}
                      setEappLoading={setEappLoading}
                    />
                  </View>
                ) : (
                  <EABButton
                    style={{
                      marginLeft: Theme.alignmentXL
                    }}
                    onPress={() => {
                      hideClientNavigator();
                      cleanClientData(navigation.dispatch);
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalAccent}
                      path="button.close"
                    />
                  </EABButton>
                )
              }
            </ContextConsumer>
          );
        };

        const renderHeaderRight = () => {
          const showHeaderRight = navigation.getParam("showHeaderRight");
          const selectModeOn = navigation.getParam("selectModeOn");

          if (!showHeaderRight) {
            return null;
          }

          return selectModeOn ? (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                const nextSelectModeOn = false;
                navigation.setParams({ selectModeOn: nextSelectModeOn });
                ACTION_LIST[REDUCER_TYPES.PRE_APPLICATION].updateUiSelectMode({
                  dispatch: navigation.dispatch,
                  selectModeOn: nextSelectModeOn
                });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.cancel"
              />
            </EABButton>
          ) : (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                const nextSelectModeOn = true;
                navigation.setParams({ selectModeOn: nextSelectModeOn });
                ACTION_LIST[REDUCER_TYPES.PRE_APPLICATION].updateUiSelectMode({
                  dispatch: navigation.dispatch,
                  selectModeOn: nextSelectModeOn
                });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.select"
              />
            </EABButton>
          );
        };

        return {
          headerTitle: () => {
            const selectModeOn = navigation.getParam("selectModeOn");
            const selectedIdCount = navigation.getParam("selectedIdCount");
            return selectModeOn ? (
              <TranslatedText
                style={Theme.title}
                path="application.summary.num_selected"
                replace={[{ value: selectedIdCount, isPath: false }]}
              />
            ) : (
              <TranslatedText
                style={Theme.title}
                path="tab.applications.title"
              />
            );
          },
          headerLeft: renderHeaderLeft(),
          headerRight: renderHeaderRight()
        };
      }
    }
  },
  {
    initialRouteName: "Summary",
    navigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

class Applications extends PureComponent {
  render() {
    return (
      <ApplicationNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("Applications")
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].applications
});

export default connect(mapStateToProps)(Applications);
