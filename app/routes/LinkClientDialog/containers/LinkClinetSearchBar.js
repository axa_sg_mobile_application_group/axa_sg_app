import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import LinkClientSearchBar from "../components/LinkClientSearchBar";

/**
 * ClientSearchBar
 * @requires LinkClientSearchBar - ClientSearchBar UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LinkClientSearchBar);
