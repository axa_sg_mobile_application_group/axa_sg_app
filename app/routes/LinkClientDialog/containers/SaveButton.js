import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import SaveButton from "../components/SaveButton";

const { CLIENT_FORM } = REDUCER_TYPES;

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  clientForm: state[CLIENT_FORM]
});

const mapDispatchToProps = dispatch => ({
  saveTrustedIndividual({ callback, confirm, alert = () => {} }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_TRUSTED_INDIVIDUAL,
      confirm,
      callback,
      alert
    });
  },
  saveClient({ confirm, callback }) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm,
      callback
    });
  },
  readyToAddTrustedIndividual(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isTrustedIndividual: true
      },
      callback
    });
  },
  readyToEditClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: false,
        isFromProduct: false
      },
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaveButton);
