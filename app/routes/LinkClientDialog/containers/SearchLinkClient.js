import { ACTION_TYPES, REDUCER_TYPES, utilities } from "eab-web-api";
import { connect } from "react-redux";
import SearchLinkClient from "../components/SearchLinkClient";

const { CLIENT_FORM, OPTIONS_MAP, CLIENT } = REDUCER_TYPES;
const { dataMapping } = utilities;
const { profileMapForm } = dataMapping;

/**
 * SearchLinkClient
 * @description link client profile dialog search page.
 * @requires SearchLinkClient - search page UI
 * @reducer client.contactList
 * @reducer client.profile
 * */
const mapStateToProps = state => ({
  contactList: state[CLIENT].contactList,
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  optionsMap: state[OPTIONS_MAP]
});

const mapDispatchToProps = dispatch => ({
  cleanClientForm() {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CLEAN_CLIENT_FORM
    });
  },
  readyToAddTrustedIndividual(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isTrustedIndividual: true
      },
      callback
    });
  },
  readyToEditClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: true,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: false,
        isFromProduct: false
      },
      callback
    });
  },
  getProfileData(clientKey, callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GET_PROFILE_DATA,
      cid: clientKey,
      callback
    });
  },
  loadClient(profile) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      ...profileMapForm(profile)
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchLinkClient);
