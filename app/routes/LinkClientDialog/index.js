import React, { PureComponent } from "react";
import { createStackNavigator } from "react-navigation";
import { connect } from "react-redux";
import EABSegmentedControl from "../../components/EABSegmentedControl";
import { NAVIGATION } from "../../constants/REDUCER_TYPES";
import TranslatedText from "../../containers/TranslatedText";
import Theme from "../../theme";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import TrustedIndividualForm from "./components/TrustedIndividualForm";
import EditLinkClientForm from "./components/EditLinkClientForm";
import SaveButton from "./containers/SaveButton";
import SearchLinkClient from "./containers/SearchLinkClient";
import LinkClinetSearchBar from "./containers/LinkClinetSearchBar";

/**
 * <LinkClientDialog />
 * @reducer navigation.needs
 * @requires NeedsDetail - Needs index page
 * @requires LearnMore - Learn more page
 * */
class LinkClientDialog extends PureComponent {
  render() {
    return (
      <LinkClientDialogNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("LinkClientDialog")
        }}
      />
    );
  }
}
export const LinkClientDialogNavigator = createStackNavigator(
  {
    SearchLinkClient: {
      screen: SearchLinkClient,
      navigationOptions: ({ navigation }) => ({
        title: "Search",
        headerTitle: <LinkClinetSearchBar navigation={navigation} />
      })
    },
    EditLinkClientForm: {
      screen: EditLinkClientForm,
      navigationOptions: ({ navigation }) => {
        if (navigation.state.params) {
          const {
            segmentedControlArray,
            showProfileTab
          } = navigation.state.params;

          return {
            headerTitle: (
              <EABSegmentedControl
                style={{ marginBottom: Theme.alignmentXS }}
                segments={segmentedControlArray}
                activatedKey={showProfileTab}
              />
            ),
            headerRight: (
              <SaveButton navigation={navigation} labelPath="button.link" />
            )
          };
        }
        return {};
      }
    },
    TrustedIndividualForm: {
      screen: TrustedIndividualForm,
      navigationOptions: ({ navigation }) => ({
        headerTitle: (
          <TranslatedText
            style={Theme.title}
            path="clientProfile.trustedIndividual.title"
          />
        ),
        headerRight: (
          <SaveButton navigation={navigation} labelPath="button.save" />
        )
      })
    }
  },
  {
    initialRouteName: "SearchLinkClient",
    navigationOptions: {
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

const mapStateToProps = state => ({
  navigation: state[NAVIGATION].linkClientDialog
});

export default connect(mapStateToProps)(LinkClientDialog);
