import { REDUCER_TYPE_CHECK, REDUCER_TYPES, utilities } from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView } from "react-native";
import ClientList from "../../../../components/ClientList";
import EABHUD from "../../../../containers/EABHUD";
import styles from "./styles";
import { languageTypeConvert } from "../../../../utilities/getEABTextByLangType";
import { ContextConsumer } from "../../../../context";
import {
  CLIENT_FORM,
  PDA_TRUSTED_INDIVIDUAL_FORM,
  TRUSTED_INDIVIDUAL_FORM
} from "../../../../constants/FORM_TYPE";

const { searchClient, common } = utilities;
const { getOptionTitle } = common;
const { CLIENT, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * <SearchLinkClient />
 * @param {function} cleanClientForm - clean all clientForm reducer state
 * @param {function} getProfileData - map profile data to clientForm
 * @param {function} loadClient - load dependant profile to clientForm
 * @param {function} readyToEditClient - set clientForm config and initial
 *   validation
 * @param {function} readyToAddTrustedIndividual - set clientForm config and
 *   initial validation
 * */
export default class SearchLinkClient extends Component {
  constructor(props) {
    super(props);

    this.translateClientArray = this.translateClientArray.bind(this);
    this.clientOnPress = this.clientOnPress.bind(this);
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description translate reducer state to UI usable data
   * @param {string} searchClientDialogFormType - form type. indicate search
   *   behavior
   * @param {string} language - UI language type
   * @param {string} searchText - text used to search the client
   * @param {boolean} isViewAll - indicate show all clients or not
   * */
  translateClientArray({
    searchClientDialogFormType,
    language,
    searchText,
    isViewAll
  }) {
    const { contactList, optionsMap, profile } = this.props;
    const returnArray = [];

    searchClient({ contactList, isViewAll, searchText }).forEach(client => {
      if (
        client.id !== profile.cid &&
        (searchClientDialogFormType !== CLIENT_FORM ||
          _.findIndex(
            profile.dependants,
            dependant => dependant.cid === client.id
          ) === -1)
      ) {
        let idStr;

        if (client.idDocType === "") {
          idStr = "";
        } else if (client.idDocType === "other") {
          idStr = client.idDocTypeOther.concat(": ", client.idCardNo);
        } else {
          idStr = getOptionTitle({
            value: client.idDocType,
            optionsMap: optionsMap.idDocType,
            language: languageTypeConvert(language)
          }).concat(": ", client.idCardNo);
        }

        returnArray.push({
          key: client.id,
          cid: client.id,
          photo: client.photo,
          name: `${client.fullName}`.trim(),
          phone: client.mobileNo,
          email: client.email,
          policies: client.applicationCount,
          idInfo: idStr,
          lastName: client.lastName,
          firstName: client.firstName
        });
      }
    });

    return returnArray;
  }
  clientOnPress({
    client,
    readyFunction,
    params,
    closeDialog = false,
    closeDialogFunction = () => {},
    routeName
  }) {
    const {
      dependantProfiles,
      loadClient,
      getProfileData,
      cleanClientForm,
      navigation
    } = this.props;
    // react navigation do not have a onCallBack event. so i need
    // to clean the data in here
    cleanClientForm();
    if (dependantProfiles[client.cid]) {
      loadClient(dependantProfiles[client.cid]);
      readyFunction(() => {
        if (closeDialog) {
          closeDialogFunction();
        } else {
          navigation.navigate({
            routeName,
            params
          });
        }
      });
    } else {
      getProfileData(client.cid, () => {
        readyFunction(() => {
          if (closeDialog) {
            closeDialogFunction();
          } else {
            navigation.navigate({
              routeName,
              params
            });
          }
        });
      });
    }
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { clientOnPress, translateClientArray } = this;
    const { readyToEditClient, readyToAddTrustedIndividual } = this.props;

    return (
      <ContextConsumer>
        {({
          language,
          searchClientDialogFormType,
          searchText,
          isViewAll,
          hideLinkClientDialog
        }) => (
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
          >
            <ClientList
              clientList={translateClientArray({
                searchClientDialogFormType,
                searchText,
                language,
                isViewAll
              })}
              clientOnPress={client => {
                switch (searchClientDialogFormType) {
                  case CLIENT_FORM:
                    clientOnPress({
                      client,
                      readyFunction: readyToEditClient,
                      routeName: "EditLinkClientForm",
                      params: {
                        cid: client.key,
                        segmentedControlArray: [
                          {
                            key: "A",
                            title: "Profile",
                            isShowingBadge: false,
                            onPress: () => {}
                          },
                          {
                            key: "B",
                            title: "Contact",
                            isShowingBadge: false,
                            onPress: () => {}
                          }
                        ],
                        showProfileTab: "A"
                      }
                    });
                    break;
                  case PDA_TRUSTED_INDIVIDUAL_FORM: {
                    clientOnPress({
                      client,
                      readyFunction: readyToAddTrustedIndividual,
                      routeName: "TrustedIndividualForm",
                      closeDialog: true,
                      closeDialogFunction: hideLinkClientDialog,
                      params: {}
                    });
                    break;
                  }
                  case TRUSTED_INDIVIDUAL_FORM: {
                    clientOnPress({
                      client,
                      readyFunction: readyToAddTrustedIndividual,
                      routeName: "TrustedIndividualForm",
                      params: {}
                    });
                    break;
                  }
                  default:
                    throw new Error(
                      `unexpected case "${searchClientDialogFormType}"`
                    );
                }
              }}
            />
            <EABHUD />
          </ScrollView>
        )}
      </ContextConsumer>
    );
  }
}

SearchLinkClient.propTypes = {
  contactList: REDUCER_TYPE_CHECK[CLIENT].contactList.isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  cleanClientForm: PropTypes.func.isRequired,
  getProfileData: PropTypes.func.isRequired,
  readyToEditClient: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  readyToAddTrustedIndividual: PropTypes.func.isRequired,
  loadClient: PropTypes.func.isRequired
};
