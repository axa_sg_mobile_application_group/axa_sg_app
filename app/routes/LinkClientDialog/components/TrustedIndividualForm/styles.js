import { StyleSheet } from "react-native";

export default StyleSheet.create({
  tiScrollView: {
    width: "100%",
    height: "100%"
  },
  tiScrollViewContainerStyle: {
    flexDirection: "row",
    justifyContent: "center"
  }
});
