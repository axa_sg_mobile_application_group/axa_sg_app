import React from "react";
import { ScrollView, KeyboardAvoidingView } from "react-native";
import ClientFormTrustedIndividual from "../../../../containers/ClientFormTrustedIndividual";
import styles from "../../../Profile/components/ProfileDetail/styles";

export default () => (
  <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={85}>
    <ScrollView
      style={styles.tiScrollView}
      contentContainerStyle={styles.tiScrollViewContainerStyle}
    >
      <ClientFormTrustedIndividual />
    </ScrollView>
  </KeyboardAvoidingView>
);
