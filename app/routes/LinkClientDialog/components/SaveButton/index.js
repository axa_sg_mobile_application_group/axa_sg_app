import React, { Component } from "react";
import PropTypes from "prop-types";
import { Alert } from "react-native";
import { NavigationActions } from "react-navigation";
import { utilities } from "eab-web-api";
import EABButton from "../../../../components/EABButton";
import {
  CLIENT_FORM,
  TRUSTED_INDIVIDUAL_FORM
} from "../../../../constants/FORM_TYPE";
import EABHUD from "../../../../containers/EABHUD";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import styles from "./styles";
import Theme from "../../../../theme";

const { isClientFormHasError } = utilities.validation;

/**
 * @param {function} saveClient - save client
 * @param {function} saveTrustedIndividual - save trusted individual
 * @param {string} labelPath - label textStore path
 * */
export default class SaveButton extends Component {
  constructor() {
    super();

    this.onSave = this.onSave.bind(this);
    this.saveClientWithoutConfirm = this.saveClientWithoutConfirm.bind(this);
    this.saveClientWithConfirm = this.saveClientWithConfirm.bind(this);

    this.state = {
      isSavingClient: false
    };
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @default onSave function
   * */
  onSave({ searchClientDialogFormType, hideLinkClientDialog, language }) {
    const { navigation, textStore, saveTrustedIndividual } = this.props;

    const callback = () => {
      this.setState(
        {
          isSavingClient: false
        },
        () => {
          setTimeout(() => {
            hideLinkClientDialog(() => {
              navigation.dispatch(
                NavigationActions.navigate({
                  routeName: "SearchLinkClient"
                })
              );
            });
          }, 200);
        }
      );
    };
    const alert = code => {
      Alert.alert(
        null,
        EABi18n({
          path: `error.${code}`,
          language,
          textStore
        }),
        [
          {
            text: EABi18n({
              path: "button.ok",
              textStore,
              language
            }),
            onPress: () => {
              saveTrustedIndividual({
                confirm: true,
                alert,
                callback
              });
            }
          }
        ]
      );
    };

    switch (searchClientDialogFormType) {
      case CLIENT_FORM:
        this.saveClientWithoutConfirm({
          language,
          callback
        });
        break;
      case TRUSTED_INDIVIDUAL_FORM: {
        this.setState(
          {
            isSavingClient: true
          },
          () => {
            saveTrustedIndividual({
              callback,
              alert
            });
          }
        );
        break;
      }
      default:
        throw new Error(`unexpected case "${searchClientDialogFormType}"`);
    }
  }

  /**
   * @description save client profile after clicking Yes in confirmation dialog
   * */
  saveClientWithConfirm(callback) {
    this.setState({ isSavingClient: true }, () => {
      this.props.saveClient({
        confirm: true,
        callback: () => {
          this.setState({ isSavingClient: false }, callback);
        }
      });
    });
  }

  /**
   * @description save client profile after clicking Save button
   * */
  saveClientWithoutConfirm({ language, callback }) {
    const { textStore, saveClient } = this.props;

    this.setState({ isSavingClient: true }, () => {
      saveClient({
        confirm: false,
        callback: code => {
          this.setState({ isSavingClient: false }, () => {
            if (code) {
              setTimeout(() => {
                confirmationAlert({
                  messagePath: `error.${code}`,
                  language,
                  textStore,
                  yesOnPress: () => {
                    this.saveClientWithConfirm(callback);
                  }
                });
              }, 200);
            } else {
              callback();
            }
          });
        }
      });
    });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { onSave } = this;
    const { labelPath, clientForm } = this.props;
    const { isSavingClient } = this.state;
    const { hasError } = isClientFormHasError(clientForm);
    return [
      <ContextConsumer key="button">
        {({ searchClientDialogFormType, hideLinkClientDialog, language }) => (
          <EABButton
            containerStyle={styles.buttonWrapper}
            onPress={() => {
              onSave({
                searchClientDialogFormType,
                hideLinkClientDialog,
                language
              });
            }}
            isDisable={hasError}
          >
            <TranslatedText
              style={Theme.textButtonLabelNormalEmphasizedAccent}
              path={labelPath}
            />
          </EABButton>
        )}
      </ContextConsumer>,
      <EABHUD key="HUD" isOpen={isSavingClient} />
    ];
  }
}

SaveButton.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  labelPath: PropTypes.string.isRequired,
  saveTrustedIndividual: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired
};
