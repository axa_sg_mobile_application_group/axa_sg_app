import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  buttonWrapper: {
    marginRight: Theme.alignmentXL
  }
});
