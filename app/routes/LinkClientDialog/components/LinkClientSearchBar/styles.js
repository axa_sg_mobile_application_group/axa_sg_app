import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: Theme.alignmentXL,
    marginBottom: 10
  },
  searchBar: {
    flex: 1,
    marginHorizontal: Theme.alignmentL
  },
  button: {
    flexBasis: "auto"
  }
});
