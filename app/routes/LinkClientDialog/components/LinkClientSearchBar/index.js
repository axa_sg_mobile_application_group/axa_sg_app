import React, { PureComponent } from "react";
import { View } from "react-native";
import EABButton from "../../../../components/EABButton";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSearchBar from "../../../../components/EABSearchBar";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";

/**
 * <ClientSearchBar />
 * @description search page search bar
 * */
export default class QuickQuoteButtonGroup extends PureComponent {
  render() {
    const { textStore } = this.props;
    return (
      <ContextConsumer>
        {({
          language,
          hideLinkClientDialog,
          searchText,
          enableShowAll,
          searchTextOnChange
        }) => (
          <View style={styles.container}>
            <EABButton
              containerStyle={styles.button}
              onPress={hideLinkClientDialog}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.cancel"
              />
            </EABButton>
            <EABSearchBar
              style={styles.searchBar}
              placeholder={EABi18n({
                textStore,
                language,
                path: "linkClientSearch.placeholder"
              })}
              value={searchText}
              onChange={searchTextOnChange}
            />
            <EABButton containerStyle={styles.button} onPress={enableShowAll}>
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.viewAll"
              />
            </EABButton>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

QuickQuoteButtonGroup.propTypes = {
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired
};
