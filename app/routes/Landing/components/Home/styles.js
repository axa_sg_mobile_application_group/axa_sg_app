import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  /* sidebar style */
  header: {
    flexBasis: "auto",
    padding: Theme.alignmentXL,
    paddingTop: Theme.alignmentXL + Theme.statusBarHeight,
    backgroundColor: Theme.brand
  },
  avatarWrapper: {
    marginBottom: Theme.alignmentL,
    backgroundColor: Theme.white,
    borderRadius: Theme.radiusL,
    width: 64,
    height: 64,
    overflow: "hidden"
  },
  avatar: {
    width: 64,
    height: 64
  },
  userName: Theme.headlineWhite,
  optionGroup: {
    flex: 1,
    alignItems: "flex-start",
    marginTop: Theme.alignmentXS,
    paddingHorizontal: 0
  },
  optionIcon: {
    marginRight: Theme.alignmentXL,
    width: 24,
    height: 24
  },
  optionButton: {
    height: 48,
    width: 320,
    justifyContent: "flex-start",
    paddingHorizontal: Theme.alignmentXL
  },
  selectedOptionText: Theme.headlineBrand,
  unselectedOptionText: Theme.headlineSecondary,
  informationWrapper: {
    width: 300,
    paddingLeft: Theme.alignmentXL,
    paddingBottom: Theme.alignmentXL
  },
  version: {
    ...Theme.subheadSecondary,
    flexBasis: "auto"
  },
  lastSyncTime: {
    ...Theme.subheadSecondary
  }
});
