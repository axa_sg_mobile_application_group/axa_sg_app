import React, { Component } from "react";
import { ScrollView, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABCheckBox from "../../../../components/EABCheckBox";
import EABSearchField from "../../../../components/EABSearchField";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * <WorkBench />
 * @description WorkBench type layout of home page
 * */
export default class extends Component {
  constructor() {
    super();

    this.filterCheckBoxOptions = [
      {
        key: "A",
        text: "In progress BI",
        isSelected: false
      },
      {
        key: "B",
        text: "In progress e-Application",
        isSelected: false
      },
      {
        key: "C",
        text: "Case approved",
        isSelected: false
      },
      {
        key: "D",
        text: "Case rejected",
        isSelected: false
      },
      {
        key: "E",
        text: "Case expired",
        isSelected: false
      },
      {
        key: "F",
        text: "Pending supervisor approval",
        isSelected: false
      },
      {
        key: "G",
        text: "Pending documents",
        isSelected: false
      },
      {
        key: "H",
        text: "Pending discussion",
        isSelected: false
      },
      {
        key: "I",
        text: "Pending CDA approval",
        isSelected: false
      }
    ];
  }

  render() {
    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        <ScrollView
          style={styles.scrollViewFilter}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.BoardContainer}>
            <View style={styles.findCaseBoard}>
              <EABSearchField
                style={styles.searchField}
                value=""
                placeholder="Case No."
                onChange={() => {}}
              />
              <EABSearchField
                style={styles.searchField}
                value=""
                placeholder="Client Name"
                onChange={() => {}}
              />
              <EABSearchField
                style={styles.searchField}
                value=""
                placeholder="ProductName"
                onChange={() => {}}
              />
              <EABSearchField
                style={styles.searchField}
                value=""
                placeholder="Agent Name"
                onChange={() => {}}
              />
              <EABSearchField
                style={styles.searchField}
                value=""
                placeholder="Supervisor Name"
                onChange={() => {}}
              />
              <EABButton buttonType={RECTANGLE_BUTTON_1} onPress={() => {}}>
                <Text style={styles.sumbitButton}>Find Case</Text>
              </EABButton>
            </View>
            <View style={styles.filterBoard}>
              <View style={styles.filterBoardHeader}>
                <Text style={styles.filterBoardHeaderTitle}>CASE STATUS</Text>
                <EABButton onPress={() => {}}>
                  <Text style={styles.selectAllText}>Select All</Text>
                </EABButton>
              </View>
              <EABCheckBox
                style={styles.checkBoxGroup}
                options={this.filterCheckBoxOptions}
                onPress={() => {}}
              />
              <View style={styles.sectionHeader}>
                <Text style={styles.sectionHeaderTitle}>LAST EDITED</Text>
              </View>
              <View style={styles.selectorField} />
              <View style={styles.sectionHeader}>
                <Text style={styles.sectionHeaderTitle}>DATE SUBMITTED</Text>
              </View>
              <View style={styles.selectorField} />
              <View style={styles.sectionHeader}>
                <Text style={styles.sectionHeaderTitle}>
                  DATE APPROVED/REJECTED
                </Text>
              </View>
              <View style={styles.selectorFieldLast} />
              <EABButton buttonType={RECTANGLE_BUTTON_1} onPress={() => {}}>
                <Text style={styles.sumbitButton}>Filter Cases</Text>
              </EABButton>
            </View>
          </View>
        </ScrollView>
        <ScrollView
          style={styles.scrollViewCase}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.BoardContainer} />
        </ScrollView>
      </LinearGradient>
    );
  }
}
