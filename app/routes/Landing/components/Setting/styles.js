import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    position: "relative",
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flexBasis: 250
  },
  contentView: {
    marginLeft: Theme.alignmentXXXXL,
    flex: 1
  },
  myProfileTitle: {
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentXL,
    fontSize: Theme.fontSizeL,
    color: Theme.brand
  }
});
