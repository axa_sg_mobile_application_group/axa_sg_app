import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  },
  companyPageWrapper: {
    width: 832,
    marginVertical: Theme.alignmentXL
  },
  infoPage: {
    height: "100%",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  companyPage: {
    justifyContent: "center",
    alignItems: "center",
    height: "auto",
    padding: Theme.alignmentXL
  },
  description: {
    ...Theme.tagLine2Secondary,
    marginTop: Theme.alignmentXL,
    textAlign: "center"
  },
  companyImage: {
    width: 532,
    height: 400
  },
  children: {
    marginBottom: Theme.alignmentXXL,
    width: 544,
    height: 248
  },
  iconBar: {
    flexDirection: "row"
  },
  iconContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: Theme.alignmentXL,
    width: 176,
    height: 143
  },
  icon: {
    marginBottom: Theme.alignmentXS,
    width: 94,
    height: 94
  },
  iconText: Theme.tagLine2Brand,
  /* second page */
  agentPage: {
    position: "relative",
    marginVertical: Theme.alignmentXL,
    width: 832
  },
  mapHeader: {
    position: "absolute",
    top: 0,
    height: 128,
    width: 832,
    zIndex: -1
  },
  agentPageWrapper: {
    paddingHorizontal: 72
  },
  userInfo: {
    marginBottom: Theme.alignmentXL,
    flexDirection: "row",
    alignItems: "flex-end",
    height: 192
  },
  agentImage: {
    marginRight: Theme.alignmentXL,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(163,169,170)",
    width: 128,
    height: 128
  },
  userPhoto: {
    minWidth: 128,
    minHeight: 128
  },
  userName: {
    ...Theme.display3,
    marginBottom: Theme.alignmentM
  },
  section: {
    marginBottom: Theme.alignmentXL
  },
  sectionTitleWrapper: {
    alignSelf: "flex-start",
    paddingBottom: 2,
    marginBottom: Theme.alignmentL,
    borderBottomWidth: 2,
    borderColor: Theme.brand,
    width: "auto"
  },
  sectionTitle: Theme.captionBrand,
  sectionRow: {
    flexDirection: "row"
  },
  titleCell: {
    ...Theme.bodyPrimary,
    flexBasis: 240
  },
  dataCell: Theme.bodySecondary,
  sectionDescription: Theme.bodyPrimary
});
