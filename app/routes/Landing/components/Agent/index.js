import React, { Component } from "react";
import PropTypes from "prop-types";
import { Dimensions, Image, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import * as _ from "lodash";
import welcome from "../../../../assets/images/welcome.jpg";
import company from "../../../../assets/images/company_intro.jpg";
import personalIcon from "../../../../assets/images/avatar64X64Dp.png";
import topBarBackground from "../../../../assets/images/topBarBackground.png";
import EABCard from "../../../../components/EABCard";
import EABPageControl from "../../../../containers/EABPageControl";
import Theme from "../../../../theme";
import styles from "./styles";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { getAttachmentWithID } from "../../../../utilities/DataManager";

/**
 * <Client />
 * @description Client type layout of home page
 * */

export default class Agent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      agentProfileAvatar: ""
    };
  }

  componentWillMount() {
    this.getAgentProfileAvatar();
  }

  getAgentProfileAvatar() {
    const { agent } = this.props;
    if (!_.isEmpty(agent)) {
      getAttachmentWithID(
        `UX_${agent.profileId}`,
        "agentProfilePic",
        response => {
          if (response && response.data) {
            this.setState({
              agentProfileAvatar: {
                uri: `data:image/png;base64,${response.data}`
              }
            });
          }
        }
      );
    }
  }

  render() {
    const { agentProfile, agentUXProfile, textStore } = this.props;
    const { agentProfileAvatar } = this.state;

    let achievementGet = null; // true
    let isShowAchievement = "";
    if (!_.isEmpty(agentUXProfile.achievements)) {
      achievementGet = agentUXProfile.achievements;
    } else if (!_.isEmpty(agentProfile.achievements)) {
      achievementGet = agentProfile.achievements;
    } else {
      achievementGet = null;
    }
    if (_.isEmpty(achievementGet)) {
      isShowAchievement = false;
    } else {
      isShowAchievement = true;
    }

    const profileArray = language => [
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.agentCode"
        }),
        data: agentProfile.agentCodeDisp
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.position"
        }),
        data: agentProfile.position
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.manager"
        }),
        data: agentProfile.manager
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.managerCode"
        }),
        data: agentProfile.managerDisp
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.origanization"
        }),
        data: agentProfile.company
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.email"
        }),
        data: agentProfile.email
      },
      {
        title: EABi18n({
          textStore,
          language,
          path: "agent.profile.mobile"
        }),
        data: agentProfile.mobile
      }
    ];

    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABPageControl
              pages={[
                {
                  key: "info",
                  component: (
                    <View style={styles.infoPage}>
                      <Image
                        style={{
                          width: Dimensions.get("window").width,
                          height: Dimensions.get("window").width / 1.3
                        }}
                        source={welcome}
                      />
                    </View>
                  ),
                  canScroll: false
                },
                {
                  key: "description",
                  component: (
                    <EABCard
                      style={styles.companyPageWrapper}
                      contentStyle={styles.companyPage}
                    >
                      <Image style={styles.companyImage} source={company} />
                      <Text style={styles.description}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "landing.companyIntro"
                        })}
                      </Text>
                    </EABCard>
                  ),
                  canScroll: true
                },
                {
                  key: "agentProfile",
                  component: (
                    <EABCard style={styles.agentPage}>
                      <Image
                        style={styles.mapHeader}
                        source={topBarBackground}
                      />
                      <View style={styles.agentPageWrapper}>
                        <View style={styles.userInfo}>
                          <View style={styles.agentImage}>
                            <Image
                              style={styles.userPhoto}
                              source={agentProfileAvatar || personalIcon}
                            />
                          </View>

                          <Text style={styles.userName}>
                            {agentProfile.name}
                          </Text>
                        </View>
                        <View style={styles.section}>
                          <View style={styles.sectionTitleWrapper}>
                            <Text style={styles.sectionTitle}>
                              {EABi18n({
                                textStore,
                                language,
                                path: "agent.profile.title"
                              })}
                            </Text>
                          </View>
                          {profileArray(language).map(row => (
                            <View key={row.title} style={styles.sectionRow}>
                              <Text style={styles.titleCell}>{`${
                                row.title
                              }:`}</Text>
                              <Text style={styles.dataCell}>
                                {row.data ? row.data : "-"}
                              </Text>
                            </View>
                          ))}
                        </View>
                        {isShowAchievement ? (
                          <View style={styles.section}>
                            <View style={styles.sectionTitleWrapper}>
                              <Text style={styles.sectionTitle}>
                                {EABi18n({
                                  textStore,
                                  language,
                                  path: "agent.achievements.title"
                                })}
                              </Text>
                            </View>
                            <Text style={styles.sectionDescription}>
                              {achievementGet}
                            </Text>
                          </View>
                        ) : null}
                        <View style={styles.section}>
                          <View style={styles.sectionTitleWrapper}>
                            <Text style={styles.sectionTitle}>
                              {EABi18n({
                                textStore,
                                language,
                                path: "agent.authorized.title"
                              })}
                            </Text>
                          </View>
                          <Text style={styles.sectionDescription}>
                            {agentProfile.authorisedDes}
                          </Text>
                        </View>
                      </View>
                    </EABCard>
                  ),
                  canScroll: true
                }
              ]}
            />
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

Agent.propTypes = {
  agentProfile: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agentUXProfile: PropTypes.oneOfType([PropTypes.object]).isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired
};
