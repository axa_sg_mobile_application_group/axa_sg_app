import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center"
  },
  wrapper: {
    width: "100%",
    maxWidth: Theme.textMaxWidth,
    alignItems: "flex-start"
  },
  logoWrapper: {
    paddingTop: 24
  },
  descriptionTitleContainer: {
    width: "100%",
    paddingTop: 16
  },
  descriptionDisclaimerContainer: {
    width: "100%",
    paddingTop: 26
  },
  descriptionContainer: {
    width: "100%",
    paddingTop: 36
  }
});
