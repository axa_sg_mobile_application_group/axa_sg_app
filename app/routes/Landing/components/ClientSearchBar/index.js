import React, { PureComponent } from "react";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSearchBar from "../../../../components/EABSearchBar";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";

/**
 * <ClientSearchBar />
 * @description search page search bar
 * */
export default class QuickQuoteButtonGroup extends PureComponent {
  render() {
    const { textStore } = this.props;

    return (
      <ContextConsumer>
        {({ language, searchText, searchTextOnChange }) => (
          <EABSearchBar
            testID="searchBarClients"
            style={styles.searchBar}
            placeholder={EABi18n({
              textStore,
              language,
              path: "search.placeholder"
            })}
            value={searchText}
            onChange={searchTextOnChange}
          />
        )}
      </ContextConsumer>
    );
  }
}

QuickQuoteButtonGroup.propTypes = {
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired
};
