import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import Setting from "../components/Setting";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { AGENT, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * Setting
 * @requires Setting - Setting UI
 * */
const mapStateToProps = state => ({
  agent: state[AGENT],
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  updateAgentProfileAvatar({ imageType, base64, callback }) {
    dispatch({
      type: ACTION_TYPES[AGENT].UPDATE_AGENT_PROFILE_AVATAR,
      imageType,
      base64,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Setting);
