import { ACTION_LIST, ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import Search from "../components/Search";

const { CLIENT, OPTIONS_MAP, AGENT } = REDUCER_TYPES;
const { getProfile } = ACTION_LIST[CLIENT];

/**
 * Search
 * @requires Search - Search UI
 * */
const mapStateToProps = state => ({
  contactList: state[CLIENT].contactList,
  optionsMap: state[OPTIONS_MAP],
  agent: state[AGENT]
});

const mapDispatchToProps = dispatch => ({
  getProfile({ cid, callback }) {
    getProfile({
      dispatch,
      cid,
      callback
    });
  },
  getContactList(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
      callback
    });
  },
  assignEligProductsToAgent(callback) {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.AGENT].ASSIGN_ELIG_PRODUCTS_TO_AGENT,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
