//
//  RNTopViewController.h
//  EASE
//
//  Created by Carlton Chen on 2/8/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RNTopViewController : UIViewController

+ (UIViewController *)getTopViewController;

@end
