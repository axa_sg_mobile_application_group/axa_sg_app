//
//  RNPopoverHostViewManager.h
//  EASE
//
//  Created by Osric Wong on 17/4/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTInvalidating.h>
#import <React/RCTViewManager.h>

typedef void (^RCTModalViewInteractionBlock)(UIViewController *reactViewController, UIViewController *viewController, BOOL animated, dispatch_block_t completionBlock);

@interface RNPopoverHostViewManager : RCTViewManager <RCTInvalidating>

@end
