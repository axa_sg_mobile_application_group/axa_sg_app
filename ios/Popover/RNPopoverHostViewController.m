//
//  RNPopoverHostViewController.m
//  EASE
//
//  Created by Osric Wong on 17/4/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RNPopoverHostViewController.h"

@interface RNPopoverHostViewController ()

@end

@implementation RNPopoverHostViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationPopover;
    }
    return self;
}

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
