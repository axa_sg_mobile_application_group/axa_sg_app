//
//  UIPrintPageRenderer+PDF.h
//  HtmlToPdfTest
//
//  Created by Tim Shiu on 11/6/14.
//  Copyright (c) 2014 EAB Systems (HK) Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPrintPageRenderer (PDF)

- (NSData*) printToPDF;

@end
