//
//  DocumentPickerEventEmitter.m
//  EASE
//
//  Created by Alex Tang on 27/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DocumentPickerEventEmitter.h"

@implementation DocumentPickerEventEmitter

RCT_EXPORT_MODULE();

static id _instace;
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instace = [super allocWithZone:zone];
  });
  return _instace;
}

- (NSArray<NSString *> *)supportedEvents {
  return @[@"importDocument", @"cancelImport"];
}

- (void)completedImport:(NSDictionary *) options{
  [self sendEventWithName:@"importDocument" body:options];
}

- (void)cancelImport{
  [self sendEventWithName:@"cancelImport" body:nil];
}

@end
