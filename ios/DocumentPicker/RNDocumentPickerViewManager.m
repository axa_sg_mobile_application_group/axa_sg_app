//
//  RNDocumentPickerViewManager.m
//  EASE
//
//  Created by Alex Tang on 25/7/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTConvert.h>
#import "RNDocumentPickerViewManager.h"
#import "DocumentPickerEventEmitter.h"
#import "AppDelegate.h"
#import "RNTopViewController.h"

@interface RNDocumentPickerViewManager()

@property (nonatomic, strong) NSMutableDictionary *options;

@end

@implementation RNDocumentPickerViewManager {
  NSHashTable *_hostViews;
}

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(export:(NSDictionary *)options
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  NSString *_base64Data;
  NSString *_fileExtension;
  NSString *_filePath;
  NSString *_fileName;
  
  if (options[@"data"] && options[@"fileExtension"] && options[@"fileName"]){
    _base64Data = [RCTConvert NSString:options[@"data"]];
    _fileExtension = [RCTConvert NSString:options[@"fileExtension"]];
    _filePath = NSTemporaryDirectory();
    _fileName = [NSString stringWithFormat:@"%@.%@", options[@"fileName"], _fileExtension];
  } else {
    NSError *error;
    reject(RCTErrorUnspecified, nil, RCTErrorWithMessage(error.description));
    return;
  }
  
  NSString *filePath = [NSString stringWithFormat:@"%@%@", _filePath, _fileName];
  NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", _filePath, _fileName]];
  NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:_base64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
  [pdfData writeToFile:filePath atomically:YES];
  
  dispatch_async(dispatch_get_main_queue(), ^{
    UIDocumentPickerViewController* documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:url inMode:UIDocumentPickerModeExportToService];
    documentPicker.delegate = self;
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [[RNTopViewController getTopViewController] presentViewController:documentPicker animated:YES completion:nil];
    resolve(@(YES));
  });
}

RCT_EXPORT_METHOD(import:(NSDictionary *)options
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  
  dispatch_async(dispatch_get_main_queue(), ^{
    _options = [NSMutableDictionary dictionaryWithDictionary:options];
    
    UIDocumentPickerViewController *importDocPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:[NSArray arrayWithObjects:@"com.adobe.pdf", @"public.png", @"public.jpeg", nil]
                                                                                                             inMode:UIDocumentPickerModeImport];
    importDocPicker.delegate = self;
    [[RNTopViewController getTopViewController] presentViewController:importDocPicker animated:YES completion:nil];
    resolve(@(YES));
  });
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller
didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls {
  
  if ([controller documentPickerMode] == UIDocumentPickerModeImport) {
    DocumentPickerEventEmitter *docPickerEvent = [[DocumentPickerEventEmitter alloc] init];
    
    NSURL *url = [urls objectAtIndex:0];
    // get the file path
    NSString *filePath = [[urls objectAtIndex:0] path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:NULL];
    
    // escape the special characters
    NSString *newFileName = [[url lastPathComponent] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSArray *newFilePathArray = [[NSArray alloc] initWithObjects:[filePath stringByDeletingLastPathComponent], newFileName, nil];
    // concat the file path and new file name
    NSString *newFilePath = [newFilePathArray componentsJoinedByString:@"/"];
    
    // no error handling now
    NSError *error = nil;
    // rename the file
    [[NSFileManager defaultManager] moveItemAtPath:filePath toPath:newFilePath error:&error];
    
    NSData *data = [NSData dataWithContentsOfFile:newFilePath];
    NSString *pdfBase64 = [data base64EncodedStringWithOptions:0];
    
    NSInteger fileSize = [fileAttributes fileSize];
    
    // calulate file size
    NSString *fileSizeLabel = @"";
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:1];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    if (fileSize > 1048578) {
      NSString *fileSizeFormat = [formatter stringFromNumber: [NSNumber numberWithDouble:fileSize / 1048578.0]];
      fileSizeLabel = [NSString stringWithFormat: @"%@MB", fileSizeFormat];
    } else {
      NSString *fileSizeFormat = [formatter stringFromNumber: [NSNumber numberWithDouble:fileSize / 1024.0]];
      fileSizeLabel = [NSString stringWithFormat: @"%@KB", fileSizeFormat];
    }
    
    // get mime type
    NSString *extension = [url pathExtension];
    NSString *mimeType = @"application/octet-stream";
    if ([extension isEqualToString:@"pdf"]) {
      mimeType = @"application/pdf";
    } else if ([extension isEqualToString:@"jpeg"] || [extension isEqualToString:@"jpg"]) {
      mimeType = @"image/jpeg";
    } else if ([extension isEqualToString:@"png"]) {
      mimeType = @"image/png";
    }
    
    NSMutableDictionary *attachmentValues = [NSMutableDictionary dictionary];
    attachmentValues[@"fileName"] = newFileName;
    attachmentValues[@"fileSize"] = fileSizeLabel;
    attachmentValues[@"fileType"] = mimeType;
    attachmentValues[@"length"] = [NSNumber numberWithInteger: fileSize];
    
    _options[@"attachmentValues"] = attachmentValues;
    _options[@"attachment"] = [NSString stringWithFormat:@"data:%@;base64,%@", mimeType, pdfBase64];
    
    [docPickerEvent completedImport: _options];
    
    NSLog(@"documentPicker didPickDocumentsAtURLs - import completed");
  }
}

- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
  DocumentPickerEventEmitter *docPickerEvent = [[DocumentPickerEventEmitter alloc] init];
  [docPickerEvent cancelImport];
  NSLog(@"documentPicker documentPickerWasCancelled - import cancel");
}

@end
