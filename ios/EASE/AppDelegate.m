/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"
#import <React/RCTLinkingManager.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#if __has_include(<React/RNSentry.h>)
#import <React/RNSentry.h> // This is used for versions of react >= 0.40
#else
#import "RNSentry.h" // This is used for versions of react < 0.40
#endif
#import <React/RCTLog.h>
#import "ViewController.h"

@implementation AppDelegate
@synthesize pull = _pull;
@synthesize push = _push;
@synthesize syncGatewayPwd = _syncGatewayPwd;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  RCTSetLogThreshold(RCTLogLevelError);
  NSURL *jsCodeLocation;
  
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"EASE"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  
  [RNSentry installWithRootView:rootView];
  
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  ViewController *rootViewController = [ViewController new];//add new viewController
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
  return [RCTLinkingManager application:application openURL:url options:options];
}

- (void)applicationWillResignActive:(UIApplication *)application {
  NSLog(@"The application is about to enter an inactive state, about to enter the background.");
  if(self.blockingView == nil){
    self.blockingView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.blockingView.backgroundColor = [UIColor whiteColor];
    //square logo.png
    self.imageView=[[UIImageView alloc] initWithFrame:CGRectMake(
                               (self.blockingView.frame.size.width - 100) /2,
                               (self.blockingView.frame.size.height - 100) /2,
                              100, 100)];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"square logo" ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    [self.imageView setImage:image];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.blockingView addSubview:self.imageView];
  }else{
    self.blockingView.frame = [[UIScreen mainScreen] bounds];
    self.imageView.frame = CGRectMake(
                                      (self.blockingView.frame.size.width - 100) /2,
                                      (self.blockingView.frame.size.height - 100) /2,
                                      100, 100);
  }
  [[[application windows] lastObject] addSubview:self.blockingView];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  NSLog(@"The application is in the foreground and is active");
  self.blockingView.frame = CGRectZero;
  self.imageView.frame = CGRectZero;
}

@end
