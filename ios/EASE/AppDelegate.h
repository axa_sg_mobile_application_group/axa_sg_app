/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property CBLReplication* pull;
@property CBLReplication* push;
@property NSString* syncGatewayPwd;

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *blockingView;
@property (nonatomic, strong) UIImageView *imageView;
@end
