//
//  NSObject+Logger.h
//  EASE
//
//  Created by Alex Tang on 30/7/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import "RCTBridgeModule.h"
#endif

@interface Logger : NSObject <RCTBridgeModule>
@end
