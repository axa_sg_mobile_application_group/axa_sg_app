//
//  EABPrintPageRenderer.h
//  EASE
//
//  Created by Alex Tang on 20/8/2018.
//  Copyright © 2018 EAB Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EABPrintPageRenderer : UIPrintPageRenderer

@property (nonatomic, strong) UIImage *barcode;
@property (nonatomic, strong) UIImage *companyLogo;
@property (nonatomic, strong) UIImage *overlay;
@property (nonatomic, strong) NSDictionary *options;

- (NSData*) printToPDF;

- (void)drawHeaderForPageAtIndex:(NSInteger)pageIndex
                          inRect:(CGRect)headerRect;

- (void)drawPrintFormatter:(UIPrintFormatter *)printFormatter
            forPageAtIndex:(NSInteger)pageIndex;

- (void)drawFooterForPageAtIndex:(NSInteger)pageIndex
                          inRect:(CGRect)footerRect;

@end
