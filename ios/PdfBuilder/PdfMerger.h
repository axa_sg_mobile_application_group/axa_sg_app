//
//  PdfMerger.h
//  PdfGenerator
//
//  Created by Alex Tang on 20/8/2018.
//  Copyright © 2018 EAB Systems. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <UIKit/UIKit.h>

@interface PdfMerger : NSObject <RCTBridgeModule>

@end
