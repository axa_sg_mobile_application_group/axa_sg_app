//
//  PdfMerger.m
//  PdfGenerator
//
//  Created by Alex Tang on 20/8/2018.
//  Copyright © 2018 EAB Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PdfMerger.h"

@implementation PdfMerger

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(mergePdfs:(NSArray *)pdfDataList
                  callback:(RCTResponseSenderBlock)callback) {
  if ([pdfDataList count] <= 1) {
    callback(@[[pdfDataList objectAtIndex:0]]);
    return;
  }
  
  // File path for result pdf
  NSString *filePath = NSTemporaryDirectory();
  NSString *fileName = @"merge.pdf";
  NSString *pdfPathOutput = [NSString stringWithFormat:@"%@%@", filePath, fileName];
  NSURL *url = [NSURL fileURLWithPath:pdfPathOutput];
  
  CFURLRef pdfURLOutput = (CFURLRef)CFBridgingRetain(url);

  NSInteger numberOfPages = 0;
  // Create the output context
  CGContextRef writeContext = CGPDFContextCreateWithURL(pdfURLOutput, NULL, NULL);

  NSMutableArray *tempPDfPaths = [NSMutableArray arrayWithCapacity:[pdfDataList count]];
  for (NSString *pdfBase64 in pdfDataList) {
    int pdfCount = (int)[tempPDfPaths count];
    NSString *tempPdfFileName = [NSString stringWithFormat:@"pdf%d.pdf", pdfCount];
    
    NSString *tempPdfPath = [NSString stringWithFormat:@"%@%@", filePath, tempPdfFileName];
    NSURL *tempPdfUrl = [NSURL URLWithString:tempPdfPath];
    NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:pdfBase64 options:NSDataBase64DecodingIgnoreUnknownCharacters];
    [pdfData writeToFile:[tempPdfUrl absoluteString] atomically:YES];
    [tempPDfPaths addObject:tempPdfPath];
    
    CFURLRef pdfURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:tempPdfPath]);
  
    //file ref
    CGPDFDocumentRef pdfRef = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL);
    numberOfPages = CGPDFDocumentGetNumberOfPages(pdfRef);
    
    // Loop variables
    CGPDFPageRef page;
    CGRect mediaBox;
  
    // Read the first PDF and generate the output pages
    NSLog(@"mergePDF GENERATING PAGES from PDF (%@)...", tempPdfFileName);
    for (int i=1; i<=numberOfPages; i++) {
      page = CGPDFDocumentGetPage(pdfRef, i);
      mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
      CGContextBeginPage(writeContext, &mediaBox);
      CGContextDrawPDFPage(writeContext, page);
      CGContextEndPage(writeContext);
    }
  
    CGPDFDocumentRelease(pdfRef);
    CFRelease(pdfURL);
  }
  CFRelease(pdfURLOutput);
    
  // Finalize the output file
  CGPDFContextClose(writeContext);
  CGContextRelease(writeContext);
  NSLog(@"mergePDF COMPLETED for PDF count=%ld", [tempPDfPaths count]);
  
  // Delete Temp PDF
  NSFileManager *manager = [NSFileManager defaultManager];
  NSError *error = nil;
  for (NSString *pdfPath in tempPDfPaths) {
    [manager removeItemAtPath:pdfPath error:&error];
  }
  
  // Get Base64 string for merged PDF and Delete Temp PDF
  NSData *mergePdfData = [NSData dataWithContentsOfFile:pdfPathOutput];
  NSString *mergePdfString = [mergePdfData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
  [manager removeItemAtPath:pdfPathOutput error:&error];
  
  callback(@[mergePdfString]);
}

@end
