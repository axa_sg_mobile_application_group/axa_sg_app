//
//  EABPrintPageRenderer.m
//  EASE
//
//  Created by Alex Tang on 20/8/2018.
//  Copyright © 2018 EAB Systems. All rights reserved.
//

#import "EABPrintPageRenderer.h"

@implementation EABPrintPageRenderer

static NSString *const PDF_TYPE_BI = @"BI";
static NSString *const PDF_TYPE_FNA = @"FNA";
static NSString *const PDF_TYPE_EAPP = @"EAPP";

static NSString *const BI_ADVISOR_SIGNATURE = @"ADVISOR_SIGNATURE";
static NSString *const BI_PROPOSER_SIGNATURE = @"PROPOSER_SIGNATURE";

static const double BI_MARGIN_HORIZONTAL = 45.0;
static const double BI_MARGIN_HORIZONTAL_WITH_OVERLAY = 22.5;
static const double BI_MARGIN_VERTICAL = 22.5;
static const double BI_COMPANY_LOGO_WIDTH = 37.5;
static const double BI_COMPANY_LOGO_HEIGHT = 37.5;
static const double BI_BARCODE_WIDTH = 144.0;
static const double BI_BARCODE_HEIGHT = 35.25;
static const double BI_OVERLAY_WIDTH = 60.0;
static const double BI_TEXT_MARGIN_VERTICAL = 1.375;
static const double BI_COVER_ADDRESS_MARGIN_LEFT = 13.25;
static const double BI_SIGNATURE_MARGIN_HORIZONTAL = 37.5;
static const double BI_SIGNATURE_WIDTH = 177.75;

static const double BI_MARGIN_HORIZONTAL_THREE_SIGNS = 39.0;
static const double BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS = 21.5;
static const double BI_SIGNATURE_WIDTH_THREE_SIGNS = 127.5;


- (NSData*) printToPDF {
  NSMutableData *pdfData = [NSMutableData data];
  UIGraphicsBeginPDFContextToData(pdfData, self.paperRect, nil);
  [self prepareForDrawingPages: NSMakeRange(0, self.numberOfPages)];
  CGRect bounds = UIGraphicsGetPDFContextBounds();
  for (int i = 0 ; i < self.numberOfPages ; i++) {
    UIGraphicsBeginPDFPage();
    [self drawPageAtIndex:i inRect:bounds];
  }
  UIGraphicsEndPDFContext();
  return pdfData;
}

- (CGFloat)marginHorizontal {
  return ([_options[@"overlayNumOfPage"] intValue] > 0 ? BI_OVERLAY_WIDTH + BI_MARGIN_HORIZONTAL_WITH_OVERLAY : BI_MARGIN_HORIZONTAL);
}

- (void)drawHeaderForPageAtIndex:(NSInteger)pageIndex
                          inRect:(CGRect)headerRect {
  
  if ([_options[@"type"] isEqualToString:PDF_TYPE_BI]) {
    [_companyLogo drawInRect:CGRectMake(BI_MARGIN_HORIZONTAL,
                                        BI_MARGIN_VERTICAL,
                                        BI_COMPANY_LOGO_WIDTH,
                                        BI_COMPANY_LOGO_HEIGHT)];
    
    if (pageIndex == 0 && [_options[@"subType"] isEqualToString:@"cover"]) {
      [_barcode drawInRect:CGRectMake(headerRect.size.width - BI_BARCODE_WIDTH - [self marginHorizontal],
                                      BI_MARGIN_VERTICAL,
                                      BI_BARCODE_WIDTH,
                                      BI_BARCODE_HEIGHT)];
    }
  }
}

- (void)drawPrintFormatter:(UIPrintFormatter *)printFormatter
            forPageAtIndex:(NSInteger)pageIndex {
  [super drawPrintFormatter:printFormatter forPageAtIndex:pageIndex];
  
  if ([_options[@"type"] isEqualToString:PDF_TYPE_BI] &&
      [_options[@"overlayNumOfPage"] intValue] > 0 &&
      pageIndex < [_options[@"overlayNumOfPage"] intValue]) {
    CGRect overlayRect = CGRectMake(self.paperRect.size.width - BI_OVERLAY_WIDTH,
                                    0,
                                    BI_OVERLAY_WIDTH,
                                    self.paperRect.size.height);
    [_overlay drawInRect:overlayRect];
  }
}

- (void)drawFnaFooterForPageAtIndex:(NSInteger)pageIndex
                             inRect:(CGRect)footerRect {
  UIFont *regularFont = [UIFont fontWithName:@"SourceSansPro-Regular" size:10];
  NSDictionary *regularAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     regularFont, NSFontAttributeName,
                                     [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName, nil];
  
  // draw FNA footer
  if (pageIndex > 0) {
    NSString *pageNumText = [NSString stringWithFormat:@"%ld", pageIndex + 1];
    [pageNumText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL, footerRect.origin.y)
              withAttributes:regularAttributes];
  }
}

- (void)drawBiFooterForPageAtIndex:(NSInteger)pageIndex
                             inRect:(CGRect)footerRect {
  NSDictionary *footerDetail = _options[@"footer"];
  NSString *dateText = @"Date:";
  UIFont *regularFont = [UIFont fontWithName:@"SourceSansPro-Regular" size:10];
  NSDictionary *regularAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     regularFont, NSFontAttributeName,
                                     [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName, nil];
  
  if (footerDetail != nil) {
    // draw text starting from bottom to top
    CGFloat originY = footerRect.origin.y + footerRect.size.height;
    
    int overlayNumOfPage = [_options[@"overlayNumOfPage"] intValue];
    int bundledPlanNumOfPage = [_options[@"bundledPlanNumOfPage"] intValue];
    BOOL inCoverPage = overlayNumOfPage > 0 && pageIndex < overlayNumOfPage;
    BOOL inBundledPlan = bundledPlanNumOfPage > 0 && pageIndex >= self.numberOfPages - bundledPlanNumOfPage;

    NSInteger currentPage = pageIndex + 1;
    NSInteger totalPage = self.numberOfPages;
    // in cover page section
    if (inCoverPage) {
      totalPage = overlayNumOfPage;
    }
    
    // in normal page section
    if (!inCoverPage && !inBundledPlan) {
      if (pageIndex >= overlayNumOfPage) {
        currentPage -= overlayNumOfPage;
      }
      totalPage = self.numberOfPages - overlayNumOfPage - bundledPlanNumOfPage;
    }
    
    // in bundled plan section
    if (inBundledPlan) {
      currentPage = (pageIndex + 1) - (self.numberOfPages - bundledPlanNumOfPage);
      totalPage = bundledPlanNumOfPage;
    }

    CGFloat regularFontHeight = regularFont.pointSize + 2 * BI_TEXT_MARGIN_VERTICAL;
    
    UIFont *boldFont = [UIFont fontWithName:@"SourceSansPro-Bold" size:10];
    CGFloat boldFontHeight = boldFont.pointSize + 2 * BI_TEXT_MARGIN_VERTICAL;
    NSDictionary *boldAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    boldFont, NSFontAttributeName,
                                    [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName, nil];
    
    UIFont *whiteFont = [UIFont fontWithName:@"SourceSansPro-Regular" size:10];
    NSDictionary *whiteAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     whiteFont, NSFontAttributeName,
                                     [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName,
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     nil];
    
    int line = 2;
    
    NSString *pageNumText = [NSString stringWithFormat:@"Page %ld of %ld", currentPage, totalPage];
    NSString *releaseVersionText = [NSString stringWithFormat: @"Release Version %@", footerDetail[@"releaseVersion"]];
    NSString *dateWithTitleText = [NSString stringWithFormat: @"%@%@", dateText, footerDetail[@"date"]];
    
    if (overlayNumOfPage > 0 && pageIndex < overlayNumOfPage) {
      NSString *addressText = @"CONFIDENTIAL";
      
      line ++;
      [pageNumText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL, originY - line * boldFontHeight)
                withAttributes:boldAttributes];
      
      CGSize releaseVersionSize = [releaseVersionText sizeWithAttributes:boldAttributes];
      [releaseVersionText drawAtPoint:CGPointMake(footerRect.size.width - releaseVersionSize.width - [self marginHorizontal], originY - line * boldFontHeight)
                       withAttributes:boldAttributes];
      line ++;
      
      CGSize addressSize = [addressText sizeWithAttributes:boldAttributes];
      CGFloat addressMidX = (footerRect.size.width - BI_OVERLAY_WIDTH) / 2;
      CGFloat addressPointX = addressMidX - addressSize.width / 2 + BI_COVER_ADDRESS_MARGIN_LEFT;
      [addressText drawAtPoint:CGPointMake(addressPointX, originY - line * boldFontHeight)
                withAttributes:boldAttributes];
      
      CGSize dateSize = [dateWithTitleText sizeWithAttributes:boldAttributes];
      [dateWithTitleText drawAtPoint:CGPointMake(footerRect.size.width - dateSize.width - [self marginHorizontal], originY - line * boldFontHeight)
                      withAttributes:boldAttributes];
      line += 2;
    } else {
      NSString *planCodeText = footerDetail[@"planCodes"];
      NSArray *address = footerDetail[@"address"];
      
      if ([address count] > 0) {
        for (NSInteger i = [address count] - 1; i >= 0; i --) {
          NSString *addressText = [address objectAtIndex:i];
          CGSize addressSize = [addressText sizeWithAttributes:regularAttributes];
          [addressText drawAtPoint:CGPointMake(footerRect.size.width / 2 - addressSize.width / 2, originY - line * regularFontHeight) withAttributes:regularAttributes];
          
          line ++;
        }
      }
      
      if (pageIndex == overlayNumOfPage) {
        [planCodeText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL, originY - line * regularFontHeight) withAttributes:regularAttributes];
      }
      line ++;
      
      [pageNumText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                withAttributes:boldAttributes];
      CGSize releaseVersionSize = [releaseVersionText sizeWithAttributes:regularAttributes];
      [releaseVersionText drawAtPoint:CGPointMake(footerRect.size.width - releaseVersionSize.width - BI_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                       withAttributes:regularAttributes];
      
      line ++;
    }
    
    if (!inBundledPlan) {
      // draw signature
      NSString *signAdviserTitleText = @"Adviser's Signature";
      NSString *signProposerTitleText = @"Proposer's Signature";
      NSString *signJointApplicantTitleText = @"Joint Applicant's Signature";
      NSString *signJointLifeAssuredTitleText = @"Life Assured 2's Signature";

      if ([_options[@"isQuickQuote"] boolValue] ||
          pageIndex + 1 == self.numberOfPages - bundledPlanNumOfPage ||
          (overlayNumOfPage > 0 && pageIndex + 1 == overlayNumOfPage)) {
        
        CGFloat lineYAdjust = regularFont.pointSize;
        
        if ([_options[@"isJoint"] boolValue]) {
          // Three Signatures Boxes
          [dateText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                 withAttributes:regularAttributes];
          [dateText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_WIDTH_THREE_SIGNS + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                 withAttributes:regularAttributes];
          [dateText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + 2 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 5 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                 withAttributes:regularAttributes];
          line ++;
          
          [signAdviserTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                             withAttributes:regularAttributes];
          [signProposerTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_WIDTH_THREE_SIGNS + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                              withAttributes:regularAttributes];                   
          if ([_options[@"isLifeAssured"] boolValue]){
            [signJointLifeAssuredTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + 2 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 5 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                                    withAttributes:regularAttributes];
          } else {
            [signJointApplicantTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL_THREE_SIGNS + 2 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 5 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight)
                                    withAttributes:regularAttributes];
          }        
          line ++;
          
          // Draw horizontal line.
          CGContextRef context = UIGraphicsGetCurrentContext();
          CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
          CGContextMoveToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          CGContextAddLineToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_WIDTH_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          
          CGContextMoveToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + BI_SIGNATURE_WIDTH_THREE_SIGNS + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          CGContextAddLineToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + 2 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          
          CGContextMoveToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + 2 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 5 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          CGContextAddLineToPoint(context, BI_MARGIN_HORIZONTAL_THREE_SIGNS + 3 * BI_SIGNATURE_WIDTH_THREE_SIGNS + 5 * BI_SIGNATURE_MARGIN_HORIZONTAL_THREE_SIGNS, originY - line * regularFontHeight + lineYAdjust);
          CGContextStrokePath(context);
          line ++;
        } else {
           // Two Signatures Boxes
           [dateText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                  withAttributes:regularAttributes];
           [dateText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_WIDTH + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                  withAttributes:regularAttributes];
           line ++;
           
           [signAdviserTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                  withAttributes:regularAttributes];
           [signProposerTitleText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_WIDTH + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                               withAttributes:regularAttributes];
           line ++;
           
           // Draw horizontal line.
           CGContextRef context = UIGraphicsGetCurrentContext();
           CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
           CGContextMoveToPoint(context, BI_MARGIN_HORIZONTAL + BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight + lineYAdjust);
           CGContextAddLineToPoint(context, BI_MARGIN_HORIZONTAL + BI_SIGNATURE_MARGIN_HORIZONTAL + BI_SIGNATURE_WIDTH, originY - line * regularFontHeight + lineYAdjust);
           
           CGContextMoveToPoint(context, BI_MARGIN_HORIZONTAL + BI_SIGNATURE_WIDTH + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight + lineYAdjust);
           CGContextAddLineToPoint(context, BI_MARGIN_HORIZONTAL + 2 * BI_SIGNATURE_WIDTH + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight + lineYAdjust);
           CGContextStrokePath(context);
           
           line ++;
        }
        
        // Draw white text for SignDoc
        NSString *advisorSignatureText = [_options[@"isQuickQuote"] boolValue] ? @"1" : BI_ADVISOR_SIGNATURE;
        NSString *proposerSignatureText = [_options[@"isQuickQuote"] boolValue] ? @"2" : BI_PROPOSER_SIGNATURE;
        [advisorSignatureText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight + lineYAdjust)
                           withAttributes:whiteAttributes];
        [proposerSignatureText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL + BI_SIGNATURE_WIDTH + 3 * BI_SIGNATURE_MARGIN_HORIZONTAL, originY - line * regularFontHeight + lineYAdjust)
                            withAttributes:whiteAttributes];
      }
    } else {
      CGSize dateSize = [dateWithTitleText sizeWithAttributes:regularAttributes];
      [dateWithTitleText drawAtPoint:CGPointMake(footerRect.size.width - dateSize.width - BI_MARGIN_HORIZONTAL, originY - line * regularFontHeight)
                      withAttributes:regularAttributes];
    }
  }
}

- (void)drawEAppFooterForPageAtIndex:(NSInteger)pageIndex
                              inRect:(CGRect)footerRect {
  CGFloat originY = footerRect.origin.y;
  UIColor *addressColor = [UIColor colorWithRed:0.18 green:0.45 blue:0.71 alpha:1.0];
  
  UIFont *regularFont = [UIFont fontWithName:@"SourceSansPro-Regular" size:8];
  CGFloat regularFontHeight = regularFont.pointSize + 2 * BI_TEXT_MARGIN_VERTICAL;
  NSDictionary *regularAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                  regularFont, NSFontAttributeName,
                                  [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName,
                                  addressColor, NSForegroundColorAttributeName,
                                  nil];
  
  UIFont *largeFont = [UIFont fontWithName:@"SourceSansPro-Bold" size:14];
  CGFloat largeFontHeight = largeFont.pointSize + 2 * BI_TEXT_MARGIN_VERTICAL;
  NSDictionary *largeAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                       largeFont, NSFontAttributeName,
                                       [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName,
                                       addressColor, NSForegroundColorAttributeName,
                                       nil];
  
  NSDictionary *footerDetail = _options[@"footer"];
  NSArray *address = footerDetail[@"address"];
  int line = 0;
  
  if (pageIndex == 0 && [address count] > 0) {
    for (NSInteger i = 0; i < [address count]; i ++) {
      NSString *addressText = [address objectAtIndex:i];
      [addressText drawAtPoint:CGPointMake(BI_MARGIN_HORIZONTAL, originY + line * regularFontHeight)
                withAttributes:regularAttributes];
      
      line ++;
    }
  }
  
  NSString *currentPageText = [NSString stringWithFormat:@"%ld", pageIndex + 1];
  NSString *pageMidText = @" of ";
  NSString *totalPageText = [NSString stringWithFormat:@"%ld", self.numberOfPages];
  
  CGSize currentPageSize = [currentPageText sizeWithAttributes:largeAttributes];
  CGSize pageMidSize = [pageMidText sizeWithAttributes:regularAttributes];
  CGSize totalPageSize = [totalPageText sizeWithAttributes:largeAttributes];
  
  [currentPageText drawAtPoint:CGPointMake(footerRect.size.width - BI_MARGIN_HORIZONTAL - currentPageSize.width - pageMidSize.width - totalPageSize.width, originY)
            withAttributes:largeAttributes];
  [pageMidText drawAtPoint:CGPointMake(footerRect.size.width - BI_MARGIN_HORIZONTAL - pageMidSize.width - totalPageSize.width, originY + largeFontHeight - regularFontHeight)
            withAttributes:regularAttributes];
  [totalPageText drawAtPoint:CGPointMake(footerRect.size.width - BI_MARGIN_HORIZONTAL - totalPageSize.width, originY)
            withAttributes:largeAttributes];
  
}

- (void)drawFooterForPageAtIndex:(NSInteger)pageIndex
                          inRect:(CGRect)footerRect {
  // draw FNA footer
  if ([_options[@"type"] isEqualToString:PDF_TYPE_FNA]) {
    [self drawFnaFooterForPageAtIndex: pageIndex
                               inRect: footerRect];
  }
  
  // draw BI footer
  if ([_options[@"type"] isEqualToString:PDF_TYPE_BI]) {
    [self drawBiFooterForPageAtIndex: pageIndex
                              inRect: footerRect];
  }
  
  // draw EAPP footer
  if ([_options[@"type"] isEqualToString:PDF_TYPE_EAPP]) {
    [self drawEAppFooterForPageAtIndex: pageIndex
                                inRect: footerRect];
  }
}

@end
