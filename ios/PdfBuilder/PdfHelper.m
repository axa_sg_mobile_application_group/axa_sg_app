//
//  PdfHelper.m
//  EASE
//
//  Created by Osric Wong on 10/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "PdfHelper.h"
#import <libxml/xmlmemory.h>
#import <libxml/debugXML.h>
#import <libxml/HTMLtree.h>
#import <libxml/xmlIO.h>
#import <libxml/xinclude.h>
#import <libxml/catalog.h>
#import "xslt.h"
#import "xsltInternals.h"
#import "transform.h"
#import "xsltutils.h"
#import <React/RCTLog.h>


@implementation PdfHelper

#pragma mark - Public functions
+ (void)generatePdf:(UIView *) view
            webView:(UIWebView *)webView
 commonTemplateJson:(NSString *)commonTemplateJson
     reportTemplateJson:(NSString *)reportTemplateJson
                    xml:(NSString *)xml
           templateLang:(NSString *)templateLang
{
  int pageCount = 0;
  NSString *reportHtml = [PdfHelper prepareReportHtmlByTemplateJson:commonTemplateJson
                                                 reportTemplateJson:reportTemplateJson
                                                                xml:xml
                                                          pageCount: &pageCount
                                                       templateLang: templateLang];
  [PdfHelper generatePdf:view
                 webView:webView
                    html:reportHtml];
}

#pragma mark - Private functions
+ (NSString *)prepareReportHtmlByTemplateJson:(NSString *)commonTemplateJson
                           reportTemplateJson:(NSString *)reportTemplateJson
                                          xml:(NSString *)xml
                                    pageCount:(int *)pageCount
                                 templateLang:(NSString *)templateLang
{
    NSError *error;
    NSDictionary *commonTemplate = [NSJSONSerialization JSONObjectWithData:[commonTemplateJson dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    NSDictionary *reportTemplate = [NSJSONSerialization JSONObjectWithData:[reportTemplateJson dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&error];
    if ([commonTemplate objectForKey:@"headerFooter"] == nil) {
        NSLog(@"Invalid commonTemplate: Missing header/footer section in common template");
    }
    if (reportTemplate == nil) {
        NSLog(@"Invalid reportTemplate: Missing header/footer section in common template");
    }

    NSMutableString *styles = [NSMutableString new];
    [styles appendString:[commonTemplate objectForKey:@"style"]];
    [styles appendString:[reportTemplate objectForKey:@"style"]];
  
    NSString *documentHeader = @"";
    NSString *documentFooter = @"";
    NSString *pageHeader = @"";
    NSString *pageFooter = @"";
    NSString *contentHeader = @"";
    NSString *contentFooter = @"";
    NSString *xsltHeader = @"";
    NSString *xsltFooter = @"";
    if ([commonTemplate objectForKey:@"headerFooter"] != nil) {
      documentHeader = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"documentHeader"];
      documentHeader = [NSString stringWithFormat:[documentHeader stringByReplacingOccurrencesOfString:@"%s" withString:@"%@"], styles];
      documentFooter = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"documentFooter"];
      pageHeader = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"pageHeader"];
      pageFooter = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"pageFooter"];
      contentHeader = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"contentHeader"];
      contentFooter = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"contentFooter"];
      xsltHeader = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"xsltHeader"];
      xsltFooter = [[commonTemplate objectForKey:@"headerFooter"] objectForKey:@"xsltFooter"];
    }
  
    NSString *htmlHeader = @"";
    NSString *htmlFooter = @"";
    if ([reportTemplate objectForKey:@"header"] != nil) {
      htmlHeader = [[reportTemplate objectForKey:@"header"] objectForKey:templateLang];
    }
    if ([reportTemplate objectForKey:@"footer"] != nil) {
      htmlFooter = [[reportTemplate objectForKey:@"footer"] objectForKey:templateLang];
    }
  
    documentHeader = documentHeader == nil ? @"" : documentHeader;
    documentFooter = documentFooter == nil ? @"" : documentFooter;
    pageHeader = pageHeader == nil ? @"" : pageHeader;
    pageFooter = pageFooter == nil ? @"" : pageFooter;
    contentHeader = contentHeader == nil ? @"" : contentHeader;
    contentFooter = contentFooter == nil ? @"" : contentFooter;
    xsltHeader = xsltHeader == nil ? @"" : xsltHeader;
    xsltFooter = xsltFooter == nil ? @"" : xsltFooter;
  
    NSMutableString *report = [NSMutableString new];
    [report appendString:documentHeader];
  
    NSArray *templates = [NSArray array];
    if ([reportTemplate objectForKey:@"template"] != nil) {
      templates = [[reportTemplate objectForKey:@"template"] objectForKey:templateLang];
    }
    NSMutableString *pages = [NSMutableString new];
    
    BOOL isFirstPage = YES;
    for (NSString *template in templates)
    {
        NSString * pageStr = [PdfHelper preparePage:template willAppendPageBreak:!isFirstPage];
        [pages appendString:pageStr];
        isFirstPage = NO;
    }
    
    NSString *transformedPages = [PdfHelper transformXml:pages xml:xml xsltHeader:xsltHeader xsltFooter:xsltFooter];
    htmlHeader = [PdfHelper transformXml:htmlHeader xml:xml xsltHeader:xsltHeader xsltFooter:xsltFooter];
    htmlFooter = [PdfHelper transformXml:htmlFooter xml:xml xsltHeader:xsltHeader xsltFooter:xsltFooter];
    pageHeader = [NSString stringWithFormat:[pageHeader stringByReplacingOccurrencesOfString:@"%s" withString:@"%@"], htmlHeader];
    pageFooter = [NSString stringWithFormat:[pageFooter stringByReplacingOccurrencesOfString:@"%s" withString:@"%@"], htmlFooter];
  
    [report appendString:[PdfHelper preparePages:transformedPages
                                      pageHeader:pageHeader
                                      pageFooter:pageFooter
                                   contentHeader:contentHeader
                                   contentFooter:contentFooter
                                       pageCount:pageCount]];
  
    [report appendString:documentFooter];
    NSInteger totalPage = 0;
    NSInteger length = report.length;
    NSRange range = NSMakeRange(0, length);
    while (range.location != NSNotFound) {
        range = [report rangeOfString:@"<div class=\"pageContainer\">" options:0 range:range];
        if (range.location != NSNotFound) {
            range = NSMakeRange(range.location + range.length, length - range.location - range.length);
            totalPage++;
        }
    }
    
//    NSLog(@"### totalPage: %d, pageCount: %d", totalPage, *pageCount);
    
    NSString *retVal = [report stringByReplacingOccurrencesOfString:@"[[@TOTAL_PAGE]]" withString:[NSString stringWithFormat:@"%li", (long)totalPage]];
    for (NSInteger pageNo = 1; pageNo <= totalPage; pageNo++) {
        NSRange range = [retVal rangeOfString:@"[[@PAGE_NO]]"];
        if (range.location != NSNotFound) {
            retVal = [retVal stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"%li", (long)pageNo]];
        }
    }
    return retVal;
}

+ (NSString *)preparePage:(NSString *)template
      willAppendPageBreak:(BOOL)willAppendPageBreak
{
    return [NSString stringWithFormat:@"%@%@", willAppendPageBreak ? @"[[@PAGE_BREAK]]" : @"", template];
}

+ (NSString *)preparePages:(NSString *)allPages
                pageHeader:(NSString *)pageHeader
                pageFooter:(NSString *)pageFooter
             contentHeader:(NSString *)contentHeader
             contentFooter:(NSString *)contentFooter
                 pageCount:(int*)pageCount
{
    NSMutableString *retVal = [NSMutableString new];
    NSArray *pages = [allPages componentsSeparatedByString:@"[[@PAGE_BREAK]]"];
    
    int count = 0;
    for (NSString *page in pages) {
        NSString *pagetrim = [page stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (pagetrim != nil && pagetrim.length > 0) {
            [retVal appendString:pageHeader];
            [retVal appendString:contentHeader];
            [retVal appendString:pagetrim];
            [retVal appendString:contentFooter];
            [retVal appendString:pageFooter];
            count++;
        }
    }
    
    * pageCount = count;
    return retVal;
}

+ (NSString *)transformXml:(NSString *)template xml:(NSString *)xml xsltHeader:(NSString *)xsltHeader xsltFooter:(NSString *)xsltFooter {
    NSString *xslt = [NSString stringWithFormat:@"%@%@%@", xsltHeader, template == nil || template.length == 0 ? @"<div>&#160;</div>" : template, xsltFooter];
    return [PdfHelper transformXml:xml template:xslt];
}

+ (NSString *)transformXml:(NSString *)xml template:(NSString *)xslt {
    NSData *xmlData = [xml dataUsingEncoding:NSUTF8StringEncoding];
    NSData *xsltData = [xslt dataUsingEncoding:NSUTF8StringEncoding];
    xmlSubstituteEntitiesDefault(1);
    xmlLoadExtDtdDefaultValue = 1;
    xmlDocPtr styDoc = xmlParseMemory([xsltData bytes], (int)[xsltData length]);
    xsltStylesheetPtr sty = xsltParseStylesheetDoc(styDoc);
    xmlDocPtr doc = xmlParseMemory([xmlData bytes], (int)[xmlData length]);
    xmlDocPtr res = xsltApplyStylesheet(sty, doc, NULL);
    xmlChar* xmlResultBuffer = nil;
    int length = 0;
    xsltSaveResultToString(&xmlResultBuffer, &length, res, sty);
    NSString* result = xmlResultBuffer == nil ? @"" : [NSString stringWithCString: (char *)xmlResultBuffer encoding: NSUTF8StringEncoding];
    
    free(xmlResultBuffer);
    xsltFreeStylesheet(sty);
    xmlFreeDoc(res);
    xmlFreeDoc(doc);
    xsltCleanupGlobals();
    xmlCleanupParser();
    
    result = [result stringByReplacingOccurrencesOfString:@"&lt;br/&gt;" withString:@"<br/>"];
    return result;
}

+ (void)generatePdf:(UIView *)view
            webView:(UIWebView *)webView
               html:(NSString *)html
{
  [PdfBuilder createPdfWithHtml:view webView:webView html:html];
}

@end
