//
//  PdfBuilder.m
//  EASE
//
//  Created by Osric Wong on 10/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTView.h>
#import <React/UIView+React.h>
#import <React/RCTUtils.h>
#import "PdfBuilder.h"
#import "UIPrintPageRenderer+PDF.h"

@interface PdfBuilder()

@property (strong, nonatomic) UIWebView *webview;
@property (strong, nonatomic) NSString *html;
@property (strong, nonatomic) NSURL *url;
@property (assign, nonatomic) CGSize pageSize;
@property (assign, nonatomic) UIEdgeInsets margins;
@property (strong, nonatomic) NSData *pdfData;

@end

@implementation PdfBuilder

#pragma mark - Public functions
+ (id)createPdfWithHtml:(UIView<UIWebViewDelegate> *)view webView:(UIWebView *)webView html:(NSString *)html{
    PdfBuilder *builder = [[PdfBuilder alloc] init];
    builder.html = html;
  [builder manualLoadView:view webView:webView];
    return builder;
}

+ (id)createPdfWithUrl:(UIView<UIWebViewDelegate>  *)view webView:(UIWebView *)webView url:(NSURL *)url  {
    PdfBuilder *builder = [[PdfBuilder alloc] init];
    builder.url = url;
  [builder manualLoadView:view webView:webView];
    return builder;
}


#pragma mark - Private functions
- (void)manualLoadView:(UIView<UIWebViewDelegate> *__strong)view webView:(UIWebView *)webView {
  webView = [[UIWebView alloc] initWithFrame:view.bounds];
  webView.delegate = view;
  [view addSubview:webView];
  
    if (self.html) {
      NSString *path = [[NSBundle mainBundle] bundlePath];
      NSURL *baseURL = [NSURL fileURLWithPath:path];
      dispatch_async(dispatch_get_main_queue(), ^{
        [webView loadHTMLString:_html baseURL:baseURL];
      });
    } else if (self.url) {
        [webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    }
}


@end
