//
//  PdfHelper.h
//  EASE
//
//  Created by Osric Wong on 10/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PdfBuilder.h"


@interface PdfHelper : NSObject

+ (void)generatePdf:(UIView *)view
            webView:(UIWebView *)webView
 commonTemplateJson:(NSString *)commonTemplateJson
     reportTemplateJson:(NSString *)reportTemplateJson
                    xml:(NSString *)xml
       templateLang:(NSString *)templateLang;

+ (NSString *)prepareReportHtmlByTemplateJson:(NSString *)commonTemplateJson
                           reportTemplateJson:(NSString *)reportTemplateJson
                                          xml:(NSString *)xml
                                    pageCount:(int *)pageCount
                                 templateLang:(NSString *)templateLang;

+ (NSString *)transformXml:(NSString *)xml template:(NSString *)xslt;
+ (void)generatePdf:(UIView *)view webView:(UIWebView *)webView html:(NSString *)html;

@end
