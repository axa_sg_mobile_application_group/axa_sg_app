//
//  UtilsHandler.m
//  EASE
//
//  Created by MacAdmin on 7/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "UtilsHandler.h"
#import "AppDelegate.h"


#define format @"yyyy-MM-dd HH:mm:ss:SSS"
#define kRSA_KEY_SIZE 1024
@interface UtilsHandler(){
  NSDateFormatter * formatter;
  SecKeyRef publicKeyRef; //publicKey
  SecKeyRef privateKeyRef;//privateKey
}
@end

@implementation UtilsHandler


static UtilsHandler* _instance = nil;

+(instancetype) shareInstance
{
  static dispatch_once_t onceToken ;
  dispatch_once(&onceToken, ^{
    _instance = [[super allocWithZone:NULL] init] ;
  }) ;
  
  return _instance ;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
  return [UtilsHandler shareInstance] ;
}

-(id) copyWithZone:(struct _NSZone *)zone
{
  return [UtilsHandler shareInstance] ;
}

-(instancetype) init{
  if (self = [super init]) {
    formatter = [[NSDateFormatter alloc ] init];
    [formatter setDateFormat:format];
  }
  return self;
}

- (NSString *_Nullable)date2String:(NSDate *_Nullable)date{
  return [formatter stringFromDate:date];
}

- (nullable NSDate *)string2Date:(NSString *_Nonnull)string{
  return [formatter dateFromString:string];
}

- (long long) getTimeMillis:(NSDate *_Nullable)date{
  return [[NSNumber numberWithDouble:[date timeIntervalSince1970] * 1000] longLongValue];
}

- (BOOL)generateRSAKeyPair
{
  publicKeyRef = NULL;
  privateKeyRef = NULL;
  OSStatus ret = SecKeyGeneratePair((CFDictionaryRef)@{(id)kSecAttrKeyType:(id)kSecAttrKeyTypeRSA,(id)kSecAttrKeySizeInBits:@(kRSA_KEY_SIZE)}, &publicKeyRef, &privateKeyRef);
  return (ret == errSecSuccess)?YES:NO;
}

-(void) callWSGetSgPwd:(NSString *) webUrl withToken:(NSString *) authToken
{
  NSString *publicKeyEncoded=[[self publicKeyBitsFromSecKey:publicKeyRef] base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
  NSLog(@"=====the rsa public key : %@", publicKeyEncoded);
  NSString *privateKeyEncoded=[[self publicKeyBitsFromSecKey:privateKeyRef] base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
  NSLog(@"=====the rsa private key : %@", privateKeyEncoded);
  // CALL WEB -start
  NSLog(@"=====callWSGetSgPwd to get the sync gateway password from oracle db");
  //NSString *webUrl = @"http://app.ease-sit3.intraeab/dataSync/accessSG";
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
  NSURL *serUrl = [NSURL URLWithString:webUrl];
  
  NSString *post = [NSString stringWithFormat:@"{ \"key\" : \"%@\"}",publicKeyEncoded];
  NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serUrl
                                                         cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                     timeoutInterval:60.0];
  
  [request setHTTPBody:postData];
  [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [request setValue:authToken forHTTPHeaderField:@"Authorization"];
  [request setHTTPMethod:@"POST"];
  
  NSURLSessionDataTask* getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
    if(error){
      NSLog(@"=====call web service return error : %@", error);
    }else{
      NSLog(@"=====call web service success");
      NSString *webServiceStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
      NSDictionary *webServiceJson = [NSJSONSerialization JSONObjectWithData:[webServiceStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&error];
      
      if ([webServiceJson[@"status"] isEqualToString:@"success"]) {
        //save the sync gateway password into AppDelegate
        [self saveSyncGatewayPwd:[webServiceJson valueForKeyPath:@"result"]];
      }
    }
  }];
  [getDataTask resume];
  // CALL WEB -end
}

- (void)saveSyncGatewayPwd:(NSString*)string {
  //decrypt the sync gateway password
  NSData *data = [[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
  NSData *decryptData = [self rsaDecryptData: data];
  NSString *syncGatewayPwd = [[NSString alloc] initWithData:decryptData encoding:NSUTF8StringEncoding];
  NSLog(@"=====the decrypt sync gateway password is : %@", syncGatewayPwd);
  //store the password
  AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  delegate.syncGatewayPwd = syncGatewayPwd;
}

- (NSData*)rsaDecryptData:(NSData*)data {
  SecKeyRef key =privateKeyRef;
  size_t cipherLen = [data length];
  void *cipher = malloc(cipherLen);
  [data getBytes:cipher length:cipherLen];
  size_t plainLen = SecKeyGetBlockSize(key) - 12;
  void *plain = malloc(plainLen);
  OSStatus status = SecKeyDecrypt(key, kSecPaddingOAEP, cipher, cipherLen, plain, &plainLen);
  
  if (status != noErr) {
    return nil;
  }
  NSData *decryptedData = [[NSData alloc] initWithBytes:(const void *)plain length:plainLen];
  return decryptedData;
}

- (void)testRSAEncryptAndDecrypt
{
//  [self generateRSAKeyPair:kRSA_KEY_SIZE];
//
//  NSData *publicKeyData = [self publicKeyBitsFromSecKey:publicKeyRef];
//  //NSData *privateKeyData = [self privateKeyBitsFromSecKey:privateKeyRef];
//  
//  NSString *encodedString=[publicKeyData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
//  [self callWSGetSyPwd:encodedString];
}

#pragma mark ---key to bits
static NSString * const kTransfromIdenIdentifierPublic = @"kTransfromIdenIdentifierPublic";
static NSString * const kTransfromIdenIdentifierPrivate = @"kTransfromIdenIdentifierPrivate";
- (NSData *)publicKeyBitsFromSecKey:(SecKeyRef)givenKey {
  
  NSData *peerTag = [kTransfromIdenIdentifierPublic dataUsingEncoding:NSUTF8StringEncoding];
  
  OSStatus sanityCheck = noErr;
  NSData * keyBits = nil;
  
  NSMutableDictionary * queryKey = [[NSMutableDictionary alloc] init];
  [queryKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
  [queryKey setObject:(id)kSecAttrKeyClassPublic forKey:(id)kSecAttrKeyClass];
  [queryKey setObject:peerTag forKey:(__bridge id)kSecAttrApplicationTag];
  
  [queryKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
  
  [queryKey setObject:(__bridge id)givenKey forKey:(__bridge id)kSecValueRef];
  [queryKey setObject:@YES forKey:(__bridge id)kSecReturnData];

  CFTypeRef result;
  sanityCheck = SecItemAdd((__bridge CFDictionaryRef) queryKey, &result);
  if (sanityCheck == errSecSuccess) {
    keyBits = CFBridgingRelease(result);
    
    (void)SecItemDelete((__bridge CFDictionaryRef) queryKey);
  }
  
  return keyBits;
}

- (NSData *)privateKeyBitsFromSecKey:(SecKeyRef)givenKey {
  
  NSData *peerTag = [kTransfromIdenIdentifierPrivate dataUsingEncoding:NSUTF8StringEncoding];
  
  OSStatus sanityCheck = noErr;
  NSData * keyBits = nil;
  
  NSMutableDictionary * queryKey = [[NSMutableDictionary alloc] init];
  [queryKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
  [queryKey setObject:peerTag forKey:(__bridge id)kSecAttrApplicationTag];
  [queryKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
  [queryKey setObject:(id)kSecAttrKeyClassPrivate forKey:(id)kSecAttrKeyClass];
  
  [queryKey setObject:(__bridge id)givenKey forKey:(__bridge id)kSecValueRef];
  [queryKey setObject:@YES forKey:(__bridge id)kSecReturnData];
  CFTypeRef result;
  sanityCheck = SecItemAdd((__bridge CFDictionaryRef) queryKey, &result);
  if (sanityCheck == errSecSuccess) {
    keyBits = CFBridgingRelease(result);
    
    (void)SecItemDelete((__bridge CFDictionaryRef) queryKey);
  }
  
  return keyBits;
}

#pragma mark - save last sync date into keychain
- (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
  return [NSMutableDictionary dictionaryWithObjectsAndKeys:
          (__bridge_transfer id)kSecClassGenericPassword,(__bridge_transfer id)kSecClass,
          service, (__bridge_transfer id)kSecAttrService,
          service, (__bridge_transfer id)kSecAttrAccount,
          (__bridge_transfer id)kSecAttrAccessibleAfterFirstUnlock,(__bridge_transfer id)kSecAttrAccessible,
          nil];
}

- (void)saveToKeychainWithKey:(NSString *)key value:(NSString *)value {
  NSString *  keychainService = @"app";
  NSData* valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary* query = [NSDictionary dictionaryWithObjectsAndKeys:
                         (__bridge id)(kSecClassGenericPassword), kSecClass,
                         keychainService, kSecAttrService,
                         valueData, kSecValueData,
                         key, kSecAttrAccount, nil];
  OSStatus osStatus = SecItemDelete((__bridge CFDictionaryRef) query);
  osStatus = SecItemAdd((__bridge CFDictionaryRef) query, NULL);
}

- (NSString*)loadFromKeychainWithKey:(NSString *)key {
  
  NSString * keychainService = keychainService = @"app";
  // Create dictionary of search parameters
  NSDictionary* query = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassGenericPassword), kSecClass,
                         keychainService, kSecAttrService,
                         key, kSecAttrAccount,
                         kCFBooleanTrue, kSecReturnAttributes,
                         kCFBooleanTrue, kSecReturnData,
                         nil];
  
  // Look up server in the keychain
  NSDictionary* found = nil;
  CFTypeRef foundTypeRef = NULL;
  OSStatus osStatus = SecItemCopyMatching((__bridge CFDictionaryRef) query, (CFTypeRef*)&foundTypeRef);
  
  if (osStatus != noErr && osStatus != errSecItemNotFound) {
    NSError *error = [NSError errorWithDomain:NSOSStatusErrorDomain code:osStatus userInfo:nil];
    NSLog(@"=====error : %@",error);
  }
  
  found = (__bridge NSDictionary*)(foundTypeRef);
  if (!found) {
    NSLog(@"======There were no events nil");
    return nil;
  } else {
    // Found
    NSString* value = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    NSLog(@"=====here were no events nil%@" ,value);
    return value;
  }
}

- (void)deleteKeychain:(NSString *)service {
  NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
  SecItemDelete((__bridge_retained CFDictionaryRef)keychainQuery);
}


@end
