//
//  UtilsHandler.h
//  EASE
//
//  Created by MacAdmin on 7/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilsHandler : NSObject
+ (instancetype _Nullable ) shareInstance;
- (NSString *_Nullable)date2String:(NSDate *_Nullable)date;
- (nullable NSDate *)string2Date:(NSString *_Nonnull)string;
- (long long) getTimeMillis:(NSDate *_Nullable)date;
- (BOOL) generateRSAKeyPair;//generate the public key and private key
- (void) callWSGetSgPwd:(NSString *_Nullable) webUrl withToken:(NSString *_Nullable) authToken;
- (void)saveToKeychainWithKey:(NSString *_Nullable)key value:(NSString *_Nullable)value;
@end
