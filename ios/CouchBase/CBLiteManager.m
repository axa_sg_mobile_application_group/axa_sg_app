#import "CBLiteManager.h"
#import "EnvConfig.h"
#import <React/RCTLog.h>

#import "CouchbaseLite/CouchbaseLite.h"
#import "CouchbaseLiteListener/CouchbaseLiteListener.h"
#import "CBLRegisterJSViewCompiler.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AppDelegate.h"
#import "DataSyncViewController.h"

@implementation CBLiteManager

RCT_EXPORT_MODULE()

- (void)initDatabaseIfNeed:(NSString *) databaseName
{
  bool isProduction = [@"production" isEqualToString:[[EnvConfig getConfig] objectForKey:@"NODE_ENV"]];
  if(masterManager == nil) {
    masterManager = [CBLManager sharedInstance];
    manager = [masterManager copy];
  } else {
    manager = [masterManager copy];
  }
  
  NSError* error;
  if(isProduction) {
    NSLog(@"is Production ");
    CBLDatabaseOptions* options = [[CBLDatabaseOptions alloc] init];
    options.storageType = kCBLSQLiteStorage;
    options.encryptionKey = encryptionKey;
    options.create = YES;
    database = [manager openDatabaseNamed:databaseName withOptions:options error:&error];
  } else {
    database = [manager existingDatabaseNamed:databaseName error:nil];
    if (database == nil) {
      NSLog(@"Gonna delete the DB ");
      NSString* dbFilePath = [[NSBundle mainBundle] pathForResource: databaseName ofType: @"cblite2"];
      NSString* dbAttPath = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat:@"%@ attachments", databaseName] ofType: @""];
      [manager replaceDatabaseNamed:databaseName withDatabaseFile:dbFilePath withAttachments:dbAttPath error:&error];
      database = [manager existingDatabaseNamed:databaseName error:nil];
    }
  }
  if(error != nil) {
    NSLog(@"initDatabaseIfNeed error: %@", error);
  }
}

- (CBLDatabase *)getDatabaseOnBg:(CBLManager *) bgMgr
                    databaseName:(NSString *) databaseName
                   encryptionKey:(NSString *) encryptionKey
{
  CBLDatabase *db = nil;
  bool isProduction = [@"production" isEqualToString:[[EnvConfig getConfig] objectForKey:@"NODE_ENV"]];
  NSError* error;
  if(isProduction) {
    CBLDatabaseOptions* options = [[CBLDatabaseOptions alloc] init];
    options.storageType = kCBLSQLiteStorage;
    options.encryptionKey = encryptionKey;
    options.create = YES;
    db = [bgMgr openDatabaseNamed:databaseName withOptions:options error:&error];
  } else {
    db = [bgMgr existingDatabaseNamed:databaseName error:nil];
    if (db == nil) {
      NSString* dbFilePath = [[NSBundle mainBundle] pathForResource: databaseName ofType: @"cblite2"];
      NSString* dbAttPath = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat:@"%@ attachments", databaseName] ofType: @""];
      [bgMgr replaceDatabaseNamed:databaseName withDatabaseFile:dbFilePath withAttachments:dbAttPath error:&error];
      db = [bgMgr existingDatabaseNamed:databaseName error:nil];
    }
  }
  if(error != nil) {
    NSLog(@"getDatabaseOnBg error: %@", error);
  }
  
  return db;
}

RCT_EXPORT_METHOD(deleteDatabase:(NSString *)databaseName
                  callback:(RCTResponseSenderBlock)callback) {
  NSError* error;
  [self initDatabaseIfNeed:databaseName];
  if (![database deleteDatabase: &error]) {
    NSLog(@"delete database error: %@", error);
    callback(@[[NSNumber numberWithBool:NO]]);
    return;
  }
  database = nil;
  [manager close];
  callback(@[[NSNumber numberWithBool:YES]]);
}

RCT_EXPORT_METHOD(setEncryptionKey:(NSString *) key
                  isDeleteLocalDB:(BOOL) isDeleteLocalDB
                  databaseName:(NSString *) databaseName
                  callback:(RCTResponseSenderBlock)callback)
{
  encryptionKey = key;
  if(isDeleteLocalDB == TRUE) {
    database = nil;
    if(manager == nil) {
      manager = [CBLManager sharedInstance];
    } else {
      manager = [manager copy];
    }
    NSString *dbFilePath = manager.directory;
    if(dbFilePath != nil) {
      NSFileManager *fileManager = [NSFileManager defaultManager];
      NSError* error;
      [fileManager removeItemAtPath:dbFilePath error:&error];
      NSLog(@"deleted old DB, %@", dbFilePath);
    }
  }
  callback(@[[NSNull null]]);
}

RCT_EXPORT_METHOD(createDocument:(NSString *) databaseName
                  docId: (NSString *) docId
                  docData:(NSDictionary *) docData
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    if(docData != nil) {
      CBLDocument* document = [database documentWithID:docId];
      NSError* error;
      //add lstChgDate for update
      NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
      [docData setValue:curr_date_time forKey:@"lstChgDate"];
      
      if (![document putProperties: docData error: &error]) {
        // fail
        NSMutableDictionary* emptyResult = @{@"error": @"conflict", @"reason": @"Document revision conflict"}.mutableCopy;
        callback(@[emptyResult]);
      } else {
        // success
        NSMutableDictionary* emptyResult = @{
                                             @"id": [document documentID],
                                             @"ok": [NSNumber numberWithBool:YES],
                                             @"rev": [document currentRevisionID]}.mutableCopy;
        callback(@[emptyResult]);
      }
    }
  } else {
    callback(@[[NSNull null]]);
  }
  [manager close];
}

RCT_EXPORT_METHOD(allDocumentIDs:(NSString *) databaseName callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    NSError* error;
    CBLQuery* query = [database createAllDocumentsQuery];
    query.allDocsMode = kCBLAllDocs;
    CBLQueryEnumerator* result = [query run: &error];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    for (CBLQueryRow* row in result) {
      if(row.key != nil && row.value != nil) {
        [dict setObject:row.value forKey:row.key];
      }
    }
    if(error != nil) {
      NSLog(@"allDocumentIDs error: %@", error);
    }
    callback(@[dict]);
  }
  [manager close];
}

- (NSArray *) jsRunner: (NSString *) funcName
             jsMapFunc: (NSString *) jsMapFunc
                   doc: (CBLJSONDict*) doc {
  self.context = [[JSContext alloc] init];
  self.context[@"consoleLog"] = ^(NSString *message) {
    RCTLog(@"Javascript log: %@",message);
  };
  self.context[@"baseFuncErrLog"] = ^(NSString *message, NSDictionary *doc1) {
    RCTLog(@"Javascript log: %@, doc: %@", message, doc1);
  };
  
  NSString *baseFuncFormat = @"function baseFunc(doc, meta) { this.arr = null; try{(%@)(doc, meta);} catch(err) { try{ baseFuncErrLog(err, doc); } catch(err2){ consoleLog(err2); } } return this.arr;  }; ";
  NSString *baseFunc = [NSString stringWithFormat:baseFuncFormat, jsMapFunc];
  NSString *emitFunc = @"function emit(key, val) {  this.arr = [key, val];  }; ";
  NSString *js = [NSString stringWithFormat:@"%@ %@ var %@ = %@", baseFunc, emitFunc, funcName, jsMapFunc];
  [self.context evaluateScript:js];
  JSValue *n = [self.context[@"baseFunc"] callWithArguments:@[doc, [NSNull null]]];
  
  if(n.isNull) {
    return nil;
  } else {
    return [n toArray];
  }
}

RCT_EXPORT_METHOD(
                  createViewsByJs:(NSString *) databaseName
                  viewsMap: (NSDictionary *) viewsMap
                  versionNum: (NSString *) versionNum
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    NSMutableArray *loadedViewArr = [[NSMutableArray alloc] init];
    if(viewsMap != nil) {
      for (NSString *funcName in viewsMap) {
        NSDictionary *valObj = [viewsMap objectForKey:funcName];
        if([valObj objectForKey:@"map"] != nil) {
          NSString *jsMapFunc = [valObj objectForKey:@"map"];
          CBLView* theView = [database viewNamed: funcName];
          [theView setMapBlock: MAPBLOCK({
            @try {
              NSArray *emitArray = [self jsRunner:funcName jsMapFunc:jsMapFunc doc:doc];
              if(emitArray != nil && [emitArray count] == 2) {
                emit([emitArray objectAtIndex:0], [emitArray objectAtIndex:1]);
              }
            }
            @catch (NSException *exception) {
              RCTLog(@"MAPBLOCK error: %@, from doc: %@", exception.reason, doc);
            }
          }) version: versionNum];
          [loadedViewArr addObject:funcName];
        }
      }
    }
    callback(@[loadedViewArr]);
  } else {
    callback(@[[NSNull null]]);
  }
  //[manager close];
}

RCT_EXPORT_METHOD(
                  applicationsCount:(NSString *) databaseName
                  emitViewNamed: (NSString *) emitViewNamed
                  params:(NSDictionary *) params
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    CBLView* theView = [database existingViewNamed:emitViewNamed];
    if (theView != nil) {
      CBLQuery* query = [theView createQuery];
      if(params != nil) {
        for (NSString *paramKey in params) {
          if ([params objectForKey:paramKey] != nil && ![[params objectForKey:paramKey] isNull]) {
            
            if ([paramKey isEqualToString:@"keys"]) {
              NSArray *criteriaArray = [self json2NSArray:[params objectForKey:paramKey]];
              query.keys = criteriaArray;
            } else if ([paramKey isEqualToString:@"startkey"]) {
              NSString *criteriaString = [self json2NSString:[params objectForKey:paramKey]];
              query.startKey = criteriaString;
            } else if ([paramKey isEqualToString:@"endkey"]) {
              NSString *criteriaString = [self json2NSString:[params objectForKey:paramKey]];
              query.endKey = criteriaString;
            }
          }
        }
      }
      NSError* error;
      CBLQueryEnumerator* result = [query run: &error];
      NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
      NSMutableArray *bundleDoc = [[NSMutableArray alloc] init];
      
      for (CBLQueryRow* row in result) {
        NSInteger count = 0;
        if ([row.value isKindOfClass:[NSArray class]]) {
          count = [row.value count];
        } else {
          count = 1;
        }
        for (int i = 0; i < count; i++) {
          if (row.value != nil) {
            NSDictionary *rowValue = nil;
            NSMutableDictionary* docValue = [[NSMutableDictionary alloc] init];
            if ([row.value isKindOfClass:[NSArray class]]) {
              rowValue = [row.value objectAtIndex:i];
            } else {
              rowValue = row.value;
            }
            if (![rowValue isEqual:[NSNull null]]) {
              [docValue setObject:row.documentID forKey:@"id"];
              [docValue setObject:row.key forKey:@"key"];
              [docValue setObject:rowValue forKey:@"value"];
              [bundleDoc addObject:docValue];
            }
          }
        }
      }
      [dict setObject:bundleDoc forKey:@"rows"];
      [dict setObject:@([bundleDoc count]) forKey:@"total_rows"];
      
      if(error != nil) {
        NSLog(@"queryByExistingView error: %@", error);
      }
      callback(@[dict]);
    } else {
      callback(@[[NSNull null]]);
    }
  } else {
    callback(@[[NSNull null]]);
  }
  [manager close];
}

RCT_EXPORT_METHOD(
                  queryByExistingView:(NSString *) databaseName
                  emitViewNamed: (NSString *) emitViewNamed
                  params:(NSDictionary *) params
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    CBLView* theView = [database existingViewNamed:emitViewNamed];
    if (theView != nil) {
      CBLQuery* query = [theView createQuery];
      if(params != nil) {
        for (NSString *paramKey in params) {
          if ([params objectForKey:paramKey] != nil && ![[params objectForKey:paramKey] isNull]) {
            
            if ([paramKey isEqualToString:@"keys"]) {
              NSArray *criteriaArray = [self json2NSArray:[params objectForKey:paramKey]];
              query.keys = criteriaArray;
            } else if ([paramKey isEqualToString:@"startkey"]) {
              NSString *criteriaString = [self json2NSString:[params objectForKey:paramKey]];
              query.startKey = criteriaString;
            } else if ([paramKey isEqualToString:@"endkey"]) {
              NSString *criteriaString = [self json2NSString:[params objectForKey:paramKey]];
              query.endKey = criteriaString;
            }
          }
        }
      }
      NSError* error;
      CBLQueryEnumerator* result = [query run: &error];
      NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
      NSMutableArray *rowsArr = [[NSMutableArray alloc] init];
      for (CBLQueryRow* row in result) {
        if(row != nil) {
          NSMutableDictionary* rowDict = [[NSMutableDictionary alloc] init];
          [rowDict setObject:row.documentID forKey:@"id"];
          [rowDict setObject:row.key forKey:@"key"];
          [rowDict setObject:row.value forKey:@"value"];
          [rowsArr addObject:rowDict];
        }
      }
      [dict setObject:rowsArr forKey:@"rows"];
      [dict setObject:@([rowsArr count]) forKey:@"total_rows"];
      if(error != nil) {
        NSLog(@"queryByExistingView error: %@", error);
      }
      callback(@[dict]);
    } else {
      callback(@[[NSNull null]]);
    }
  } else {
    callback(@[[NSNull null]]);
  }
  [manager close];
}

- (NSString *)json2NSString:(NSString *)str {
  NSString *jsonString = [NSString stringWithFormat:@"{\"criteria\":%@}", str];
  NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSString *criteriaString = nil;
  @try {
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    criteriaString = [json objectForKey:@"criteria"];
  }
  @catch (NSException *exception) {
    criteriaString = str;
  }
  return criteriaString;
}

- (NSArray *)json2NSArray:(NSString *)str {
  NSString *jsonString = [NSString stringWithFormat:@"{\"criteria\":%@}", str];
  NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  NSArray *criteriaArray = nil;
  @try {
    id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    criteriaArray = [json objectForKey:@"criteria"];
  }
  @catch (NSException *exception) {
    criteriaArray = @[str];
  }
  return criteriaArray;
}

RCT_EXPORT_METHOD(
                  queryViewByJs:(NSString *) databaseName
                  emitViewNamed: (NSString *) emitViewNamed
                  jsMapFunc: (NSString *) jsMapFunc
                  emitVersion: (NSString *) emitVersion
                  params:(NSDictionary *) params
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    CBLView* theView = [database viewNamed: emitViewNamed];
    [theView setMapBlock: MAPBLOCK({
      NSArray *emitArray = [self jsRunner:emitViewNamed jsMapFunc:jsMapFunc doc:doc];
      if(emitArray != nil && [emitArray count] > 0) {
        emit([emitArray objectAtIndex:0], [emitArray objectAtIndex:1]);
      }
    }) version: emitVersion];
    CBLQuery* query = [theView createQuery];
    if(params != nil) {
      for (NSString *paramKey in params) {
        if ([paramKey isEqualToString:@"startkey"]) {
          query.startKey = [params objectForKey:paramKey];
        } else if ([paramKey isEqualToString:@"endkey"]) {
          query.endKey = [params objectForKey:paramKey];
        }
      }
    }
    NSError* error;
    CBLQueryEnumerator* result = [query run: &error];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    NSMutableArray *rowsArr = [[NSMutableArray alloc] init];
    for (CBLQueryRow* row in result) {
      if(row != nil) {
        NSMutableDictionary* rowDict = [[NSMutableDictionary alloc] init];
        [rowDict setObject:row.key forKey:@"key"];
        [rowDict setObject:row.value forKey:@"value"];
        [rowsArr addObject:rowDict];
      }
    }
    [dict setObject:rowsArr forKey:@"rows"];
    if(error != nil) {
      NSLog(@"error: %@", error);
    }
    callback(@[dict]);
  } else {
    callback(@[[NSNull null]]);
  }
  [manager close];
}

RCT_EXPORT_METHOD(
                  queryView:(NSString *) databaseName
                  emitViewNamed: (NSString *) emitViewNamed
                  emitDocKey: (NSString *) emitDocKey
                  emitDocValues: (NSArray *) emitDocValues
                  emitVersion: (NSString *) emitVersion
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    CBLView* theView = [database viewNamed: emitViewNamed];
    [theView setMapBlock: MAPBLOCK({
      if(doc[emitDocKey] != nil) {
        NSMutableDictionary* valDict = [[NSMutableDictionary alloc] init];
        for (NSString *valueKey in emitDocValues) {
          if(doc[valueKey] != nil) {
            [valDict setObject:doc[valueKey] forKey:valueKey];
          }
        }
        if([valDict count] > 0) {
          emit(doc[emitDocKey], valDict);
        } else {
          emit(doc[emitDocKey], nil);
        }
      }
    }) version: emitVersion];
    CBLQuery* query = [theView createQuery];
    //    query.startKey = clientName;
    //    query.keys = @[clientName];
    NSError* error;
    CBLQueryEnumerator* result = [query run: &error];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    for (CBLQueryRow* row in result) {
      if(row.value != nil) {
        for (NSDictionary* r in row.value) {
          if(r != nil) {
            [dict setObject:r forKey:row.key];
          }
        }
      }
    }
    if(error != nil) {
      NSLog(@"error: %@", error);
    }
    callback(@[dict]);
    
  }
  [manager close];
  
}

RCT_EXPORT_METHOD(getAttachmentWithID:(NSString *) databaseName
                  docId:(NSString *) docId
                  attName:(NSString *) attName
                  callback:(RCTResponseSenderBlock)callback)
{
  
//  NSLog(@"%@", [NSTimeZone systemTimeZone]);
  BOOL hasData = FALSE;
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    if(docId != nil) {
      CBLDocument* doc = [database documentWithID: docId];
      if(doc != nil) {
        CBLRevision* rev = doc.currentRevision;
        CBLAttachment* att = [rev attachmentNamed: attName];
        if (att != nil) {
          NSData* attData = att.content;
          if(attData != nil) {
            hasData = TRUE;
            NSMutableDictionary* result = @{@"data":[attData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]}.mutableCopy;
            callback(@[result]);
          }
        }
      }
    }
  }
  
  if(!hasData) {
    callback(@[[NSNull null]]);
  }
  [manager close];
}

RCT_EXPORT_METHOD(getDocumentWithID:(NSString *) databaseName docId:(NSString *) docId callback:(RCTResponseSenderBlock)callback)
{
  BOOL hasData = FALSE;
  [self initDatabaseIfNeed:databaseName];
  NSLog(@"DB Access - getDocumentWithID : %@",docId);
  if(database != nil) {
    if(docId != nil) {
      CBLDocument* doc = [database documentWithID: docId];
      if(doc != nil) {
        NSDictionary* properties = doc.properties;
        if(properties != nil) {
          hasData = TRUE;
          callback(@[properties]);
        }
      }
    }
  }
  
  if(!hasData) {
    NSMutableDictionary* emptyResult = @{@"error": @"not_found", @"reason": @"missing"}.mutableCopy;
    callback(@[emptyResult]);
  }
  [manager close];
}

RCT_EXPORT_METHOD(uploadAttachmentWithBase64:(NSString *) databaseName
                  docId:(NSString *) docId
                  attName:(NSString *) attName
                  contentType:(NSString *) contentType
                  base64Data:(NSString *) base64Data
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  NSLog(@" DB Access - uploadAttachmentWithBase64 %@ %@", docId, attName);
  if(database != nil) {
    if(docId != nil && base64Data != nil) {
      //CBLManager* bgMgr = [manager copy];
      //[NSThread detachNewThreadSelector: @selector(uploadAttachmentWithBase64OnBg:)
      //                         toTarget: self
      //                       withObject: [NSArray arrayWithObjects:bgMgr, databaseName, docId, attName, contentType, base64Data, encryptionKey, callback, nil]];
      [self uploadAttachmentWithBase64OnBg:[NSArray arrayWithObjects:@"dummy", databaseName, docId, attName, contentType, base64Data, encryptionKey, callback, nil]];
    } else {
      callback(@[[NSNumber numberWithBool:NO]]);
    }
  }
  [manager close];
}

- (void) uploadAttachmentWithBase64OnBg: (NSArray*) parameters {
  //CBLManager* bgMgr = [parameters objectAtIndex:0];
//  NSString* databaseName = [parameters objectAtIndex:1];
  NSString* docId = [parameters objectAtIndex:2];
  NSString* attName = [parameters objectAtIndex:3];
  NSString* contentType = [parameters objectAtIndex:4];
  NSString* base64Data = [parameters objectAtIndex:5];
//  NSString* encryptionKey = [parameters objectAtIndex:6];
  RCTResponseSenderBlock callback = [parameters objectAtIndex:7];
  
  NSError* error;
  //CBLDatabase* bgDB = [self getDatabaseOnBg:bgMgr databaseName:databaseName encryptionKey:encryptionKey];
  CBLDocument* doc = [database documentWithID: docId];
  if (![doc update: ^BOOL(CBLUnsavedRevision *newRev) {
    NSData * nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:base64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
    [newRev setAttachmentNamed:attName withContentType:contentType content:nsdataFromBase64String];
    //add lstChgDate for update
    NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
    [newRev.properties setObject:curr_date_time forKey:@"lstChgDate"];
    return YES;
  } error: &error]) {
    // fail
    NSMutableDictionary* result = @{@"error": @"conflict", @"reason": @"Document revision conflict"}.mutableCopy;
    callback(@[result]);
  } else {
    // success
    NSMutableDictionary* result = @{
                                    @"id": [doc documentID],
                                    @"ok": [NSNumber numberWithBool:YES],
                                    @"rev": [doc currentRevisionID]}.mutableCopy;
    callback(@[result]);
  }
  //[bgMgr close];
}

RCT_EXPORT_METHOD(updateDocumentWithID:(NSString *) databaseName docId:(NSString *) docId docData:(NSDictionary *) docData callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  NSLog(@" DB Access - updateDocumentWithID - %@", docId);
  if(database != nil) {
    if(docId != nil) {
      //CBLManager* bgMgr = [manager copy];
      //[NSThread detachNewThreadSelector: @selector(updateDocumentOnBg:)
      //                         toTarget: self
      //                       withObject: [NSArray arrayWithObjects:bgMgr, databaseName, docId, docData, encryptionKey, callback, nil]];
      [self updateDocumentOnBg:[NSArray arrayWithObjects: databaseName, docId, docData, encryptionKey, callback, nil]];
    }
  }
  [manager close];
}

- (void) updateDocumentOnBg: (NSArray*) parameters {
  //CBLManager* bgMgr = [parameters objectAtIndex:0];
//  NSString* databaseName = [parameters objectAtIndex:0];
  NSString* docId = [parameters objectAtIndex:1];
  NSDictionary* docData = [parameters objectAtIndex:2];
//  NSString* encryptionKey = [parameters objectAtIndex:3];
  RCTResponseSenderBlock callback = [parameters objectAtIndex:4];
  
  NSError* error;
  //CBLDatabase* bgDB = [self getDatabaseOnBg:bgMgr databaseName:databaseName encryptionKey:encryptionKey];
  CBLDocument* doc = [database documentWithID: docId];
  if (![doc update: ^BOOL(CBLUnsavedRevision *newRev) {
    // delete value in new Doc
    NSMutableArray *removeKeys = [NSMutableArray array];
    
    for (id key in newRev.properties) {
      if (![docData objectForKey:key] && ![key hasPrefix:@"_"]) {
        [removeKeys addObject:key];
      }
    }
    
    for (id key in removeKeys) {
      [newRev.properties removeObjectForKey:key];
    }
    
    // update value
    for (id key in docData) {
      [newRev.properties setObject:[docData objectForKey:key] forKey:key];
    }
    //add lstChgDate for update
    NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
    [newRev.properties setObject:curr_date_time forKey:@"lstChgDate"];
    return YES;
  } error: &error]) {
    // fail
    NSMutableDictionary* emptyResult = @{@"error": @"conflict", @"reason": @"Document revision conflict"}.mutableCopy;
    callback(@[emptyResult]);
  } else {
    // success
    NSMutableDictionary* emptyResult = @{
                                         @"id": [doc documentID],
                                         @"ok": [NSNumber numberWithBool:YES],
                                         @"rev": [doc currentRevisionID]}.mutableCopy;
    callback(@[emptyResult]);
  }
  //[bgMgr close];
  
}

RCT_EXPORT_METHOD(deleteAttachmentWithAttName:(NSString *) databaseName
                  docId:(NSString *) docId
                  attName:(NSString *) attName
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil && docId != nil) {
    CBLManager* bgMgr = [manager copy];
    [NSThread detachNewThreadSelector: @selector(deleteAttachmentWithAttNameOnBg:)
                             toTarget: self
                           withObject: [NSArray arrayWithObjects:bgMgr, databaseName, docId, attName, encryptionKey, callback, nil]];
  }
  [manager close];
}

- (void) deleteAttachmentWithAttNameOnBg: (NSArray*) parameters {
  CBLManager* bgMgr = [parameters objectAtIndex:0];
  NSString* databaseName = [parameters objectAtIndex:1];
  NSString* docId = [parameters objectAtIndex:2];
  NSString* attName = [parameters objectAtIndex:3];
  NSString* encryptionKey = [parameters objectAtIndex:4];
  RCTResponseSenderBlock callback = [parameters objectAtIndex:5];
  
  NSError* error;
  CBLDatabase* bgDB = [self getDatabaseOnBg:bgMgr databaseName:databaseName encryptionKey:encryptionKey];
  CBLDocument* doc = [bgDB documentWithID: docId];
  if (![doc update: ^BOOL(CBLUnsavedRevision *newRev) {
    [newRev removeAttachmentNamed:attName];
    //add lstChgDate for update
    NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
    [newRev.properties setObject:curr_date_time forKey:@"lstChgDate"];
    return YES;
  } error: &error]) {
    callback(@[[NSNumber numberWithBool:NO]]);
  } else {
    callback(@[[NSNumber numberWithBool:YES]]);
  }
  [bgMgr close];
}

RCT_EXPORT_METHOD(deleteDocumentWithID:(NSString *) databaseName loginId:(NSString *) loginId docId:(NSString *) docId callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil && docId != nil) {
    CBLManager* bgMgr = [manager copy];
    [NSThread detachNewThreadSelector: @selector(deleteDocumentOnBg:)
                             toTarget: self
                           withObject: [NSArray arrayWithObjects:bgMgr, databaseName, loginId, docId, encryptionKey, callback, nil]];
  }
  [manager close];
}

- (void) deleteDocumentOnBg: (NSArray*) parameters {
  CBLManager* bgMgr = [parameters objectAtIndex:0];
  NSString* databaseName = [parameters objectAtIndex:1];
  NSString* loginId = [parameters objectAtIndex:2];
  NSString* docId = [parameters objectAtIndex:3];
  NSString* encryptionKey = [parameters objectAtIndex:4];
  RCTResponseSenderBlock callback = [parameters objectAtIndex:5];
  
  NSError* error;
  CBLDatabase* bgDB = [self getDatabaseOnBg:bgMgr databaseName:databaseName encryptionKey:encryptionKey];
  
  if ([bgDB existingDocumentWithID:docId] != nil) {
    CBLDocument* doc = [bgDB documentWithID: docId];
    //add docId into delete list file
    BOOL success = [self saveDeletedDocId:bgDB loginId:loginId deletedDocId:docId];
    if (success && [doc deleteDocument: &error]) {
      callback(@[[NSNumber numberWithBool:YES]]);
    }else{
      callback(@[[NSNumber numberWithBool:NO]]);
    }
  }else{
    callback(@[[NSNumber numberWithBool:NO]]);
  }
  [bgMgr close];
}

#pragma mark - Testing Data
RCT_EXPORT_METHOD(genTestData:(NSString *) databaseName
                  callback:(RCTResponseSenderBlock)callback) {
  NSBundle* bundle = [NSBundle mainBundle];
  NSError *error;
  NSString *initFilePath = [bundle pathForResource:@"testing_data_config" ofType:@"txt"];
  NSString *initFileContents = [NSString stringWithContentsOfFile:initFilePath encoding:NSUTF8StringEncoding error:&error];
  
  NSString *updateFilePath = [bundle pathForResource:@"testing_data_config" ofType:@"txt"];
  NSString *updateFileContents = [NSString stringWithContentsOfFile:updateFilePath encoding:NSUTF8StringEncoding error:&error];
  
  if (error)
    NSLog(@"Error reading file: %@", error.localizedDescription);
  
  BOOL hasData = NO;
  [self initDatabaseIfNeed:databaseName];
  if(database != nil) {
    CBLDocument* doc = [database documentWithID: @"sysParameter"];
    if(doc != nil) {
      NSDictionary* properties = doc.properties;
      NSLog(@"getDocumentWithID is called...4.2..%@", properties);
      if(properties != nil) {
        hasData = YES;
      }
    }
  }
  
  int loadedNum = 0;
  bool isProduction = [@"production" isEqualToString:[[EnvConfig getConfig] objectForKey:@"NODE_ENV"]];
  NSArray *listArray = [NSArray array];
  if(!hasData) {
    listArray = [initFileContents componentsSeparatedByString:@"\n"];
  } else if(isProduction) {
    listArray = [updateFileContents componentsSeparatedByString:@"\n"];
  }
  listArray = [updateFileContents componentsSeparatedByString:@"\n"];
  int rowNum = 0;
  for (NSString *row in listArray) {
    @autoreleasepool {
      rowNum++;
      NSArray *rowArray = nil;
      rowArray = [row componentsSeparatedByString:@"\t"];
      if([rowArray count] == 1) {
        rowArray = [row componentsSeparatedByString:@"  "];
      }
      if([rowArray count] == 1) {
        NSLog(@"-- SKIP row numbers: %d / %lu", rowNum, (unsigned long)[listArray count]);
      } else {
        loadedNum++;
        NSString *type = [rowArray objectAtIndex:0];
        if([type isEqualToString:@"D"]) {
          [self insertTestDoc:databaseName rowArray:rowArray init:!hasData];
        } else if([type isEqualToString:@"A"]) {
          [self insertTestAtt:databaseName rowArray:rowArray];
        } else {
          NSLog(@"import data type is not correct, row num: %d", loadedNum);
        }
      }
    }
  }
  NSLog(@"%@ Completed total: %d / %lu", hasData ? @"Update" : @"Init", loadedNum, (unsigned long)[listArray count]);
  
  callback(@[[NSNumber numberWithInt:loadedNum]]);
  [manager close];
}

- (void)insertTestDoc:(NSString *) databaseName
             rowArray:(NSArray *) rowArray
                 init:(BOOL) init {
  @autoreleasepool {
    NSString *docId = [rowArray objectAtIndex:1];
    NSString *data = [rowArray objectAtIndex:2];
    NSArray *fileNames = [data componentsSeparatedByString:@"."];
    
    NSUInteger index = 0;
    NSString *filename = nil;
    for (NSString *partOfName in fileNames) {
      if(index < [fileNames count] - 1) {
        if(filename == nil) {
          filename = partOfName;
        } else {
          filename = [[filename stringByAppendingString:@"."] stringByAppendingString:partOfName];
        }
      }
      index++;
    }
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSError *error;
    NSString* filepath = [bundle pathForResource:filename ofType:@"json"];
    NSString *jsonString = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
    
    if (error) {
      NSLog(@"TestDoc - Error reading file: %@, %@", error.localizedDescription, data);
    } else {
      NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
      NSError *jsonError;
      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
      
      CBLDocument* document = [database documentWithID:docId];
      NSError* error2;
      CBLSavedRevision* saved = nil;
      if (init) {
        saved = [document putProperties: jsonObject error: &error2];
      } else {
        [document deleteDocument: &error2];
        saved = [document update: ^BOOL(CBLUnsavedRevision *newRev) {
          for (id key in jsonObject) {
            [newRev.properties setObject:[jsonObject objectForKey:key] forKey:key];
          }
          return YES;
        } error: &error2];
      }
      
      if (!saved && error2) {
        NSLog(@"TestDoc - Error save doc: %@ %@", docId, error2.localizedDescription);
      }
    }
  }
}

- (void)insertTestAtt:(NSString *) databaseName
             rowArray:(NSArray *) rowArray {
  @autoreleasepool {
    NSString *parentDocId = [rowArray objectAtIndex:1];
    NSString *docId = [rowArray objectAtIndex:2];
    NSString *data = [rowArray objectAtIndex:3];
    NSArray *fileNames = [data componentsSeparatedByString:@"."];
    
    NSUInteger index = 0;
    NSString *filename = nil;
    NSString *fileType = nil;
    for (NSString *partOfName in fileNames) {
      if(index < [fileNames count] - 1) {
        if(filename == nil) {
          filename = partOfName;
        } else {
          filename = [[filename stringByAppendingString:@"."] stringByAppendingString:partOfName];
        }
      } else if(index == [fileNames count] - 1) {
        fileType = partOfName;
      }
      index++;
    }
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* filepath = [bundle pathForResource:filename ofType:fileType];
    
    NSString *contentType = @"";
    NSString *base64Data = nil;
    if([fileType isEqualToString:@"png"]) {
      contentType = @"image/png";
      UIImage *image       = [UIImage imageWithContentsOfFile:filepath];
      NSData *imageData    = UIImagePNGRepresentation(image);
      base64Data = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    } else if([fileType isEqualToString:@"jpg"]) {
      contentType = @"image/jpeg";
      UIImage *image       = [UIImage imageWithContentsOfFile:filepath];
      NSData *imageData    = UIImagePNGRepresentation(image);
      base64Data = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    } else if([fileType isEqualToString:@"pdf"]) {
      contentType = @"application/pdf";
      if ([[NSFileManager defaultManager] fileExistsAtPath:filepath]){
        NSData *pdfData = [[NSFileManager defaultManager] contentsAtPath:filepath];
        base64Data = [pdfData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
      }
    }
    
    if(parentDocId != nil && base64Data != nil) {
      if(database != nil) {
        //      [NSThread detachNewThreadSelector: @selector(uploadTestAttachmentWithBase64OnBg:)
        //                               toTarget: self
        //                             withObject: [NSArray arrayWithObjects:bgMgr, databaseName, parentDocId, docId, contentType, base64Data, encryptionKey, nil]];
        
        [self uploadTestAttachmentWithBase64OnBg:[NSArray arrayWithObjects:manager, databaseName, parentDocId, docId, contentType, base64Data, encryptionKey, nil]];
      } else {
        NSLog(@"No database is found!");
      }
    } else {
      NSLog(@"No parentDocId: %@, docId: %@, base64Data: %@, ...fileName: %@", parentDocId, docId, base64Data, data);
    }
  }
}


- (void) uploadTestAttachmentWithBase64OnBg: (NSArray*) parameters {
  @autoreleasepool {
    CBLManager* bgMgr = [parameters objectAtIndex:0];
    NSString* databaseName = [parameters objectAtIndex:1];
    NSString* docId = [parameters objectAtIndex:2];
    NSString* attName = [parameters objectAtIndex:3];
    NSString* contentType = [parameters objectAtIndex:4];
    NSString* base64Data = [parameters objectAtIndex:5];
    NSString* encryptionKey = [parameters objectAtIndex:6];
    
    NSError* error;
    CBLDatabase* bgDB = [self getDatabaseOnBg:bgMgr databaseName:databaseName encryptionKey:encryptionKey];
    CBLDocument* doc = [bgDB documentWithID: docId];
    if (![doc update: ^BOOL(CBLUnsavedRevision *newRev) {
      NSData * nsdataFromBase64String = [[NSData alloc] initWithBase64EncodedString:base64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
      [newRev setAttachmentNamed:attName withContentType:contentType content:nsdataFromBase64String];
      return YES;
    } error: &error]) {
      NSLog(@"Fail - parentDocId: %@, docId: %@", docId, attName);
    }
  }
}

RCT_EXPORT_METHOD(dataSync:(NSString *) remoteURL
                  localDbName:(NSString *) localDbName
                  username:(NSString *) username
                  password:(NSString *) password
                  agentCode:(NSString *) agentCode
                  loginId:(NSString *) loginId
                  webServiceUrl:(NSString *) webServiceUrl
                  authToken:(NSString *) authToken
                  isLeftMenu:(BOOL) isLeftMenu
                  isBackground:(BOOL) isBackground
                  sessionCookieSyncGatewaySession:(NSString *) sessionCookieSyncGatewaySession
                  sessionCookieExpiryDate:(NSNumber * __nonnull) sessionCookieExpiryDate
                  sessionCookiePath:(NSString *) sessionCookiePath
                  syncDocumentIds:(NSArray *) syncDocumentIds
                  callback:(RCTResponseSenderBlock)callback)
{
    NSLog(@"goto objective-c replicateWithBasic code");
  
    //Perform the replacement rootViewController operation in the main thread
    dispatch_async(dispatch_get_main_queue(), ^{
      DataSyncViewController *dataSyncView = [[DataSyncViewController alloc] init];
      
      dataSyncView.titleLabelText = @"Data Synchronization";
      dataSyncView.descriptionLabelText = @"Progressing...";
      dataSyncView.leftButtonLabelText = @"";
      dataSyncView.rightButtonLabelText = @"";
      dataSyncView.remoteURL = remoteURL;
      dataSyncView.localDbName = localDbName;
      dataSyncView.syncDbName = [localDbName stringByAppendingString:@"_sync"];
      dataSyncView.username = username;
      dataSyncView.password = password;
      dataSyncView.agentCode = agentCode;
      dataSyncView.loginId = loginId;
      dataSyncView.encryptionKey = encryptionKey;
      dataSyncView.wsurl = webServiceUrl;
      dataSyncView.authToken = authToken;
      dataSyncView.isBackground = isBackground;
      dataSyncView.isLeftMenu = isLeftMenu;
      dataSyncView.sessionCookieSyncGatewaySession =sessionCookieSyncGatewaySession;
      long long time =[sessionCookieExpiryDate longLongValue]/1000.0;
      NSDate *dateTemp = [NSDate dateWithTimeIntervalSince1970:time];
      dataSyncView.sessionCookieExpiryDate = dateTemp ;
      dataSyncView.sessionCookiePath = sessionCookiePath ;
      dataSyncView.syncDocumentIds = syncDocumentIds ;
      NSLog(@"sessionCookieSyncGatewaySession =%@",sessionCookieSyncGatewaySession);
      NSLog(@"sessionCookieExpiryDate =%@[%@]",sessionCookieExpiryDate,dateTemp);
      NSLog(@"sessionCookiePath =%@",sessionCookiePath);
      dataSyncView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
      
      if (isLeftMenu) {
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        app.window.rootViewController.definesPresentationContext = YES;
        if (app.window.rootViewController.presentedViewController) {
          //need first dismiss then present, fix "Warning: Attempt to present <DataSyncViewController: 0x7fdd22262800> on <ViewController: 0x7fdd21c33a60> whose view is not in the window hierarchy!
          [app.window.rootViewController.presentedViewController dismissViewControllerAnimated:NO completion:^{
            [app.window.rootViewController presentViewController:dataSyncView animated:NO completion:nil];
          }];
        }else {
          [app.window.rootViewController presentViewController:dataSyncView animated:NO completion:nil];
        }
      }else{
        UIViewController *topController = [self getCurrentVC];
        [topController presentViewController:dataSyncView animated:NO completion:nil];
      }
      //*******************************************************************************************************************************
      NSLog(@"*********Display data sync view**********");
    });
    
    callback(@[[NSNull null]]);
}

RCT_EXPORT_METHOD(getSyncGatewayPwd:(NSString *) url
                  authToken:(NSString *) authToken
                  callback:(RCTResponseSenderBlock)callback)
{
  UtilsHandler *utilsHandler = [UtilsHandler shareInstance];
  BOOL success = [utilsHandler generateRSAKeyPair];
  if (success) {
    [utilsHandler callWSGetSgPwd:url withToken:authToken];
  }
  NSLog(@"not use");
  callback(@[[NSNull null]]);
}

- (UIViewController *)getCurrentVC
{
  UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
  UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
  return currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
  UIViewController *currentVC;
  if ([rootVC presentedViewController]) {
    rootVC = [rootVC presentedViewController];
  }
  if ([rootVC isKindOfClass:[UITabBarController class]]) {
    currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
  } else if ([rootVC isKindOfClass:[UINavigationController class]]){
    currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
  } else {
    currentVC = rootVC;
  }
  return currentVC;
}

- (BOOL) saveDeletedDocId:(CBLDatabase *) database loginId:(NSString *) loginId deletedDocId:(NSString *) deletedDocId
{
  NSError *error;
  //U_{{LOGIN_ID}}_DELETEDID
  NSString *deletedDocIdFileName = [[@"U_" stringByAppendingString:loginId] stringByAppendingString:@"_DELETEDID"];
  CBLDocument* doc = [database documentWithID:deletedDocIdFileName];
  NSMutableDictionary* p = [doc.properties mutableCopy];
  
  NSMutableArray* deletedIds;
  if (p != nil) {
    deletedIds = p[@"deletedIds"];
  }else{
    p = [NSMutableDictionary dictionary];
    deletedIds = [[NSMutableArray alloc] init];
  }
  
  NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
  NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
  [dictionary setObject:deletedDocId forKey:@"clientId"];
  [dictionary setObject:curr_date_time forKey:@"deletedTime"];
  
  [p setObject:[deletedIds arrayByAddingObject:dictionary] forKey:@"deletedIds"];
  if (![doc putProperties: p error: &error]) {
    return NO;
  }
  return YES;
}

RCT_EXPORT_METHOD(getAllApplicationsWithCID:(NSString *) databaseName
                  pCid:(NSString *) pCid
                  callback:(RCTResponseSenderBlock)callback)
{
  [self initDatabaseIfNeed:databaseName];
  if(database != nil && pCid != nil) {
    NSMutableArray *bundleDoc = [[NSMutableArray alloc] init];
    NSMutableArray *quotationDoc = [[NSMutableArray alloc] init];
    NSError* error;
    NSArray *where = @[@"01", pCid];
    
    CBLView* theView = [database viewNamed: @"clienteApplications"];
    CBLQuery* query = [theView createQuery];
    query.keys = @[where];
    
    CBLQueryEnumerator* result = [query run: &error];
    if (result != nil && result.count > 0) {
      for (CBLQueryRow* row in result) {
        NSInteger count = 0;
        if ([row.value isKindOfClass:[NSArray class]]) {
          count = [row.value count];
        } else {
          count = 1;
        }
        for (int i = 0; i < count; i++) {
          if (row.value != nil) {
            NSDictionary *value = nil;
            if ([row.value isKindOfClass:[NSArray class]]) {
              value = [row.value objectAtIndex:i];
            } else {
              value = row.value;
            }
            if (![value isEqual:[NSNull null]]) {
              NSString *type = [value objectForKey:@"type"];
              if ([type isEqualToString:@"bundleDoc"]) {
                [bundleDoc addObject:value];
              }
              if ([type isEqualToString:@"quotationDoc"]) {
                [quotationDoc addObject:value];
              }
            }
          }
        }
      }
      NSMutableArray *newApplicationData = [NSMutableArray array];
      for (NSDictionary *quotationData in quotationDoc) {
        NSString *bundleId = [quotationData objectForKey:@"bundleId"];
        NSString *quotationDocId = [quotationData objectForKey:@"quotationDocId"];
        for (NSDictionary *bundleData in bundleDoc) {
          NSString *bundleQuotationDocId = [bundleData objectForKey:@"quotationDocId"];
          if ([bundleQuotationDocId isEqualToString:quotationDocId]) {
            [newApplicationData addObject:
             @{
               @"appStatus": [bundleData objectForKey:@"appStatus"] ? [bundleData objectForKey:@"appStatus"] : @"",
               @"quotationDocId": [bundleData objectForKey:@"quotationDocId"] ? [bundleData objectForKey:@"quotationDocId"] : @"",
               @"bundleId": [bundleData objectForKey:@"bundleId"] ? [bundleData objectForKey:@"bundleId"] : @"",
               @"baseProductCode": [quotationData objectForKey:@"baseProductCode"] ? [quotationData objectForKey:@"baseProductCode"] : @""
               }];
          }
        }
        if (bundleId == nil || bundleDoc == nil || [bundleDoc count] == 0) {
          [newApplicationData addObject:
           @{
             @"appStatus": @"",
             @"quotationDocId": [quotationData objectForKey:@"quotationDocId"] ? [quotationData objectForKey:@"quotationDocId"] : @"",
             @"bundleId": @"",
             @"baseProductCode": [quotationData objectForKey:@"baseProductCode"] ? [quotationData objectForKey:@"baseProductCode"] : @""
             }];
        }
      }
      callback(@[newApplicationData]);
    }else{
      callback(@[[NSNull null]]);
    }
    if(error != nil) {
      NSLog(@"getAllApplicationsWithCID error: %@", error);
      callback(@[[NSNull null]]);
    }
  }
  [manager close];
}

@end

