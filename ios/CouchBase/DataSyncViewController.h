//
//  AlterViewController.h
//  EASE
//
//  Created by MacAdmin on 30/8/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "UtilsHandler.h"

@interface DataSyncViewController : UIViewController
  @property NSString* remoteURL;
  @property NSString* localDbName;
  @property NSString* syncDbName;
  @property NSString* username;
  @property NSString* password;
  @property NSString* agentCode;
  @property NSString* loginId;
  @property NSString* encryptionKey;
  @property NSString* wsurl;
  @property NSString* authToken;
  @property BOOL isBackground;
  @property NSDate* lastSyncDate;

  @property NSString* sessionCookieSyncGatewaySession;
  @property NSDate* sessionCookieExpiryDate;
  @property NSString* sessionCookiePath;
  @property NSArray* syncDocumentIds;

  @property NSDate* lastSyncSuccessDate;
  @property NSString *currentAuditLogDocId;
  @property BOOL isLeftMenu;

  @property (weak, nonatomic) IBOutlet UIView *backView;
  @property (weak, nonatomic) IBOutlet UIView *contentView;
  @property (nonatomic, copy) NSString *titleLabelText;
  @property (nonatomic, copy) NSString *descriptionLabelText;
  @property (nonatomic, copy) NSString *leftButtonLabelText;
  @property (nonatomic, copy) NSString *rightButtonLabelText;

@end
