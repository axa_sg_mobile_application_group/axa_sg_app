//
//  EABTableViewController.h
//  helloWorld
//
//  Created by MacAdmin on 21/10/18.
//  Copyright © 2018 MacAdmin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EABTableViewController : UIViewController
@property NSString* authToken;
@property NSString* wsurl;
@end
