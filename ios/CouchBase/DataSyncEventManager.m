//
//  DataSyncEventManager.m
//  EASE
//
//  Created by MacAdmin on 4/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "DataSyncEventManager.h"

@interface DataSyncEventManager(){
  bool hasListeners;
}
@end

@implementation DataSyncEventManager
RCT_EXPORT_MODULE();


-(void)startObserving {
  hasListeners = YES;
  NSLog(@"=====Set up any upstream listeners or background tasks as necessary");
}

// Will be called when this module's last listener is removed, or on dealloc.
-(void)stopObserving {
  hasListeners = NO;
  NSLog(@"=====Remove upstream listeners, stop unnecessary background tasks");
}

-(void)sendDataSyncStatus:(NSString*) status{
  if (hasListeners) { // Only send events if anyone is listening
    [self sendEventWithName:@"DataSyncEventHandler" body:status];
  }else{
    NSLog(@"=====DataSyncEventHandler but , has not listeners");
  }
}

- (NSArray<NSString *> *)supportedEvents {
  return @[@"DataSyncEventHandler",];
}

+(id)allocWithZone:(NSZone *)zone {
  static RCTBridge *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [super allocWithZone:zone];
  });
  return sharedInstance;
}

@end
