#import <React/RCTBridgeModule.h>

static NSDictionary *config;

@interface EnvConfig : NSObject <RCTBridgeModule>

+ (NSDictionary *)getConfig;
@end

