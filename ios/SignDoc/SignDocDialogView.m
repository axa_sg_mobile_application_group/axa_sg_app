//
//  SignDocDialogView.m
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "SignDocDialogView.h"
#import "SignDocDialogViewController.h"

@interface SignDocDialogView ()

@property (nonatomic, strong) SignDocDialogViewController *signDocDialogViewController;

@end

@implementation SignDocDialogView {
  __weak RCTBridge *_bridge;
}


RCT_NOT_IMPLEMENTED(-(instancetype)initWithFrame
                    : (CGRect)frame)
RCT_NOT_IMPLEMENTED(-(instancetype)initWithCoder
                    : coder)

#pragma mark - React

- (instancetype _Nonnull)initWithBridge:(RCTBridge *_Nullable)bridge {
  NSLog(@"SignDocDialogView initWithBridge is called.");
  if ((self = [super initWithFrame:CGRectZero])) {
    _bridge = bridge;
    _signDocDialogViewController = [[SignDocDialogViewController alloc] init];
    [_signDocDialogViewController view];
    _signDocDialogViewController.view = self;
    
  }
  return self;
}

#pragma mark - RCTInvalidating

- (void)invalidate {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self dismissViewController];
  });
}

#pragma mark - Public

- (BOOL)showCaptureDialog:(double)signX signY:(double)signY pdfFilePath:(NSString *)pdfFilePath pdfFileName:(NSString *)pdfFileName word:(NSString *)word order:(NSString *)order{
  _signDocDialogViewController.signatureFieldName = word;
  return [_signDocDialogViewController showCaptureDialog:signX signY:signY pdfFilePath:pdfFilePath pdfFileName:pdfFileName word: word order:order];
}

- (void)dismissViewController {
  //  if (_presented) {
  //    [_delegate dismissPopoverHostView:self withViewController:_popoverHostViewController animated:_animated];
  //    _presented = NO;
  //  }
}

- (NSMutableArray *) convertPdfToPng: (NSString *)path {
  NSMutableArray *arr = [_signDocDialogViewController convertPdfToPng:path];
  return arr;
}

- (NSMutableArray *) findTexts: (NSMutableArray *)texts {
  NSMutableArray *arr = [_signDocDialogViewController findTexts:texts];
  return arr;
}

@end
