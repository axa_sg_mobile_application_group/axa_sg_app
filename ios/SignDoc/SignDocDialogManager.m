//
//  SignDocDialogManager.m
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <PDFKit/PDFKit.h>
#import <React/RCTBridge.h>
#import "SignDocDialogManager.h"
#import "SignDocDialogViewController.h"


static struct SIGNDOC_Exception *ex;
static struct SIGNDOC_DocumentHandler *handler;
static struct SIGNDOC_DocumentLoader *loader;
static struct SIGNDOC_Document *document;
static NSString *docPath;

@implementation SignDocDialogManager

@synthesize bridge = _bridge;

- (dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE()

- (UIView *)view
{
  _signDocDialogView = [[SignDocDialogView alloc] initWithBridge:self.bridge];
  return _signDocDialogView;
  
}

RCT_EXPORT_METHOD(initView) {
  _signDocDialogView = [[SignDocDialogView alloc] initWithBridge:self.bridge];
}

RCT_EXPORT_METHOD(getTimezone:(RCTResponseSenderBlock)callback){
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  callback(@[@{@"value": tzName}]);
}

RCT_EXPORT_METHOD(init:(NSArray *)searchTagArr pdfBase64:(NSString *)pdfBase64 callback:(RCTResponseSenderBlock)callback){
  if (!_signDocDialogView) {
    [self initView];
  }
  // Write pdf to temp file
  NSString *filepath = [NSString stringWithFormat:@"%@%@%@", NSTemporaryDirectory(), @"temp", @".pdf"];
  NSLog(@"%@", filepath);
  NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:pdfBase64 options:NSDataBase64DecodingIgnoreUnknownCharacters];
  [pdfData writeToFile:filepath atomically:YES];
  
  // Convert pdf to image
  NSMutableArray *arr = [_signDocDialogView convertPdfToPng:filepath];
  
  // FindText
   NSMutableArray *texts = [_signDocDialogView findTexts:searchTagArr];
  
  callback(@[@"222a", @{@"tags": texts, @"images": arr}]);
}

RCT_EXPORT_METHOD(showCaptureDialog:(NSString *)word order:(NSString *)order options:(NSDictionary *)options callback:(RCTResponseSenderBlock)callback) {
  NSLog(@"wordd:%@", word);
  NSString *filePath = NSTemporaryDirectory();
//  NSString *tempPdfPath = [NSString stringWithFormat:@"%@%@%@", filePath, @"temp", @".pdf"];
//  NSURL *tempPdfUrl = [NSURL URLWithString:tempPdfPath];
//  NSLog(@"%@", NSTemporaryDirectory());
//  NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:pdfBase64 options:NSDataBase64DecodingIgnoreUnknownCharacters];
//  [pdfData writeToFile:[tempPdfUrl absoluteString] atomically:YES];
// signX: Move X coordinate; signY: Move Y coordinate;
  double signX = 0;
  double signY = 0;
  double widthX = 595.2;
  double widthY = 841.8;
  
  if (options[@"signX"]){
    signX = //widthX * 
    [RCTConvert double:options[@"signX"]];
   // signX = [RCTConvert double:options[@"signX"]];
    NSLog(@"optionX: %g | signX: %g | widthX: %g" , [RCTConvert double:options[@"signX"]], signX, widthX);
  }
  
  if (options[@"signY"]){
    signY = //widthY * 
    [RCTConvert double:options[@"signY"]];
   // signY = [RCTConvert double:options[@"signY"]];
    NSLog(@"optionY: %g | signY: %g | widthY: %g" , [RCTConvert double:options[@"signY"]], signY, widthY);
  }
  
  //  if (options[@"directory"] && [options[@"directory"] isEqualToString:@"docs"]){
  //    // allow document query by iTune, and sync with iCloud
  //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  //    NSString *documentsPath = [paths objectAtIndex:0];
  //    pdfFilePath = documentsPath;
  //  } else {
  //    pdfFilePath = NSTemporaryDirectory();
  //  }
  
  //  NSLog(@"1pdfFileName: %@", pdfFileName);
  //  NSLog(@"pdfFilePath: %@", pdfFilePath);
  
  //  pdfFileName = [NSString stringWithFormat:@"%@.pdf", pdfFileName];
  //  NSLog(@"2pdfFileName: %@", pdfFileName);
  
  //  NSLog(@"pdfFilePath:%@", pdfFilePath);
  BOOL isSucceed = [_signDocDialogView showCaptureDialog:signX signY:signY pdfFilePath: filePath pdfFileName: @"temp.pdf" word: word order: order];
  NSLog(@"isSucceed: %d", isSucceed);
  
  if(callback != nil) {
    callback(@[[NSNumber numberWithBool:isSucceed]]);
  }
}

RCT_EXPORT_METHOD(pdfToImagePdf:(NSString *)pdfFileName callback:(RCTResponseSenderBlock)callback) {
  [self initSignDoc:[pdfFileName UTF8String]];
  if(callback != nil) {
    callback(@[[NSNumber numberWithBool:YES]]);
  }
}


- (void)commonInit {
  NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
  docPath = docPaths[0];
  
  ex = SIGNDOC_Exception_new(SIGNDOC_EXCEPTION_TYPE_PDF, nil);
  if (!ex)
    RCTLog(@"FAIL.....SIGNDOC_Exception_new");
//  SIGNDOC_Exception_setHandler(handleException);
  
  const void* license =
  "h:SPLM2 4.10\n"
  "i:ID:9923321\n"
  "i:Product:SignDocSDK\n"
  "i:Manufacturer:SOFTPRO GmbH\n"
  "i:Customer:Trial License - NOT FOR PRODUCTION\n"
  "i:Version:4.0\n"
  "i:OS:all\n"
  "a:product:unlimited\n"
  "a:signware:unlimited\n"
  "a:sign:2016-03-31\n"
  "a:capture:unlimited\n"
  "a:render:unlimited\n"
  "s:c20fe81f016d01863cc5eef7de71bb77010324ca204d95ba1d41b9dca6cdf08f\n"
  "s:613014af4666487da0c75c409cd8c2e6dd53dcbb7a8f1a30d7ce3b22d6b7922f\n"
  "s:e3c6715fa802ec90b77dab0e691066059f1b8d49e614fa7893b5001db22e4c0e\n"
  "s:07d612771473d3a23d35e4751136d0e1c04bbddab242b6654794d3409c055331\n";
  
  NSLog(@"Length of license: %lu", strlen(license));
  if (!SIGNDOC_DocumentLoader_setLicenseKey(&ex, license, strlen(license), NULL, NULL, NULL, 0)) {
    RCTLog (@"FAIL...Cannot initialize license");
//    doThrow ();
  }
  
  // Create loader
  loader = SIGNDOC_DocumentLoader_new(&ex);
  handler = SIGNDOC_PdfDocumentHandler_new(&ex);
  if (!SIGNDOC_DocumentLoader_registerDocumentHandler(&ex, loader, handler))
    RCTLog (@"FAIL....SIGNDOC_DocumentLoader_registerDocumentHandler");
  
}

- (void)initSignDoc:(const char *)pdfFileName {
  [self commonInit];
  
  document = SIGNDOC_DocumentLoader_loadFromFile(&ex, loader, SIGNDOC_ENCODING_UTF_8, pdfFileName, TRUE);
  if (document == nil)
    RCTLog(@"FAIL...SIGNDOC_DocumentLoader_loadFromFile");
  
  NSLog(@"%d", SIGNDOC_Document_getPageCount(&ex, document));
  
  // convert to images
  UIImage *uiImage = [self pdf2Image];
//  struct SIGNDOC_ByteArray *blob = pdf2Image();
//  const char *image_path = "pdfImages.png";
//  NSString *image_path2 = [NSString stringWithUTF8String:image_path];
//  NSString *image_path3 = [docPath stringByAppendingPathComponent:image_path2];
//  const char *image_path4 = [image_path3 UTF8String];
//  NSLog (@"Image file: %s", image_path4);
//  remove (image_path4);
//  writeFile (image_path4, SIGNDOC_ByteArray_data (blob), SIGNDOC_ByteArray_count (blob));
  
  // convert to pdf
//  PDFDocument *pdf = [[PDFDocument alloc] init];
////  NSImage *image = [[NSImage alloc] initWithContentsOfFile:fileName];
//  PDFPage *page = [[PDFPage alloc] initWithImage:uiImage];
//  [pdf insertPage:page atIndex: [pdf pageCount]];
//
    NSArray *doc_paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *doc_path = [doc_paths objectAtIndex:0];
//  const char *filename = [[doc_path stringByAppendingPathComponent:@"outputDocumentName.pdf"] UTF8String];
//  [pdf writeToFile:filename];
  NSData *pdfData = [self drawPDFData:uiImage];
  [pdfData writeToFile:[doc_path stringByAppendingPathComponent:@"outputDocumentName.pdf"] atomically:YES];
}

- (NSData *)drawPDFData:(UIImage *)uiImage {
  //  default pdf..
  // 8.5 X 11 inch @72dpi
  // = 612 x 792
  CGRect rct = {{0.0 , 0.0 } , {612.0 , 792.0}};
  NSMutableData *pdfData = [[NSMutableData alloc]init];
  UIGraphicsBeginPDFContextToData(pdfData, rct, nil);
  CGContextRef pdfContext = UIGraphicsGetCurrentContext();
  
  
  UIGraphicsBeginPDFPage();
  CGContextSaveGState(pdfContext);
  [uiImage drawInRect:CGRectMake(0.0, 0.0, 100, 100)];
  
  UIGraphicsBeginPDFPage();
  CGContextSaveGState(pdfContext);
  [uiImage drawInRect:CGRectMake(0.0, 0.0, 100, 100)];
  
  UIGraphicsBeginPDFPage();
  CGContextSaveGState(pdfContext);
  [uiImage drawInRect:CGRectMake(0.0, 0.0, 100, 100)];
  
  
//  //textView drawing
//  CGContextSaveGState(pdfContext);
//  CGContextConcatCTM(pdfContext, CGAffineTransformMakeTranslation(50.0,50.0));//this  is  just an offset for the textView drawing. You will  want to play with the values, espeecially if supporting multiple screen sizes you might tranform the scale  as well..
//
//  [textView.layer renderInContext:pdfContext];
//  CGContextRestoreGState(pdfContext);
  
  
  //imageView drawing
//  CGContextConcatCTM(pdfContext, CGAffineTransformMakeTranslation(50.0,50.0)); //this  is  just an offset for the imageView drawing. Thee same stuff applies as above..
//  [imageView.layer renderInContext:pdfContext];
  CGContextRestoreGState(pdfContext);
  
  
  
  //cleanup
  UIGraphicsEndPDFContext();
  return pdfData;
}

//- (void) getPDFDocumentRef:(CGPDFDocumentRef*) document {
//  NSArray *doc_paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
//  NSString *doc_path = [doc_paths objectAtIndex:0];
//  const char *filename = [[doc_path stringByAppendingPathComponent:@"outputDocumentName.pdf"] UTF8String];
//  CFStringRef path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
//  CFURLRef url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
//  CFRelease (path);
//  *document = CGPDFDocumentCreateWithURL(url);
//  CFRelease(url);
//  size_t count = CGPDFDocumentGetNumberOfPages(*document);
//  if (count == 0) {
//    NSLog(@"`%s' needs at least one page!", filename);
//  }
//}

static void writeFile (const char *path, const void *ptr, size_t size) {
  FILE *f = fopen (path, "wb");
  if (f == NULL) {
    NSLog (@"%s: %s", path, strerror (errno));
//    doThrow ();
    return;
  }
  fwrite (ptr, 1, size, f);
  if (ferror (f) || fflush (f) != 0) {
    NSLog (@"%s: %s", path, strerror (errno));
    fclose (f);
//    doThrow ();
    return;
  }
  if (fclose (f) != 0) {
    NSLog (@"%s: %s", path, strerror (errno));
//    doThrow ();
    return;
  }
}

- (UIImage *)pdf2Image {
  int height = 1024;
  struct SIGNDOC_ByteArray *blob;
  struct SIGNDOC_RenderParameters *rp;
  struct SIGNDOC_RenderOutput *ro;
  
  blob = SIGNDOC_ByteArray_new (&ex);
  if (blob == NULL)
    RCTLog(@"SIGNDOC_ByteArray_new");
  rp = SIGNDOC_RenderParameters_new (&ex);
  ro = SIGNDOC_RenderOutput_new (&ex);
  
  SIGNDOC_RenderParameters_setFormat (&ex, rp, "png");
  SIGNDOC_RenderParameters_setPage (&ex, rp, 1);
  SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
//  struct SIGNDOC_Rect *clip = NULL;
  
  int rc = SIGNDOC_Document_renderPageAsImage (&ex, document, blob, ro, rp, NULL);
  if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
    RCTLog(@"Fail.....SIGNDOC_Document_renderPageAsImage");
  }
  else {
    RCTLog (@"renderTest");
    RCTLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
    RCTLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
  }
  
  NSData *data = [NSData dataWithBytes:SIGNDOC_ByteArray_data(blob) length:SIGNDOC_ByteArray_count(blob)];
  UIImage *signatureImage = [UIImage imageWithData:data];
  SIGNDOC_ByteArray_delete(blob);
//  SIGNDOC_Rect_delete(clipForRenderPageAsImage);
  
  if (ro != NULL)
    SIGNDOC_RenderOutput_delete (ro);
  if (rp != NULL)
    SIGNDOC_RenderParameters_delete (rp);
  
//  return blob;
  return signatureImage;
}

//- (void)deviceConnected:(int)deviceType {
//  switch (deviceType) {
//    case IOSFOUNDATIONS_DEVICE_TYPE_WACOM_BLUETOOTH_1:
//      NSLog(@"A wacom pen was connected");
//      break;
//    case IOSFOUNDATIONS_DEVICE_TYPE_APPLE_PENCIL_1:
//      //currently not happening too often; call retrieveConnectivityStatusForDeviceType if you want to know the connectivity status for some reason
//      NSLog(@"An apple pencil was connected");
//      break;
//    default:
//      break;
//  }
//}
//
//- (void)deviceDisconnected:(int)deviceType {
//  if (deviceType == IOSFOUNDATIONS_DEVICE_TYPE_WACOM_BLUETOOTH_1) {
//    NSLog(@"A wacom pen was disonnected");
//  }
//}

@end
