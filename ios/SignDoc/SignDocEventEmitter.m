//
//  SignDocEventEmitter.m
//  EASE
//
//  Created by Kevin Liang on 22/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "SignDocEventEmitter.h"

@implementation SignDocEventEmitter

RCT_EXPORT_MODULE();

static id _instace;
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instace = [super allocWithZone:zone];
  });
  return _instace;
}

- (NSArray<NSString *> *)supportedEvents {
  return @[@"SignDoc"];
}

- (void)tellJS:(NSMutableArray *) pdfBase64{
  [self sendEventWithName:@"SignDoc" body:pdfBase64];
}

@end
