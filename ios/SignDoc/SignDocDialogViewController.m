//
//  SignDocDialogViewController.m
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "SignDocDialogViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

#include "SignProcessor.h"
#import <sys/socket.h>
#import <ifaddrs.h>
#import <arpa/inet.h>

#import "SignPosInfo.h"

@interface SignDocDialogViewController ()

@end

@implementation SignDocDialogViewController

@synthesize handleSignatureDialog;

static struct SIGNDOC_DocumentLoader *loader;

- (void) getPDFDocumentRef:(CGPDFDocumentRef*) document {
  //  NSArray *doc_paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
  //  NSString *doc_path = [doc_paths objectAtIndex:0];
  const char *filename = [[docFilePath stringByAppendingPathComponent:docFileName] UTF8String];
  CFStringRef path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
  CFURLRef url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
  CFRelease (path);
  *document = CGPDFDocumentCreateWithURL(url);
  CFRelease(url);
  size_t count = CGPDFDocumentGetNumberOfPages(*document);
  if (count == 0) {
    NSLog(@"`%s' needs at least one page!", filename);
  }
}

- (UIImage *)getImageWithAppleRenderer {
  CGPDFDocumentRef document = nil;
  [self getPDFDocumentRef:&document];
  CGPDFPageRef PDFPage = CGPDFDocumentGetPage(document, 1);
  // Determine the size of the PDF page.
  CGRect pageRect = CGPDFPageGetBoxRect(PDFPage, kCGPDFMediaBox);
  CGSize pdfPageSize = pageRect.size;
  CGFloat originalMinimumPDFScale = self.scrollView.frame.size.width/pdfPageSize.width;
  CGFloat PDFScale = originalMinimumPDFScale;
  pageRect.size = CGSizeMake(pageRect.size.width*PDFScale, pageRect.size.height*PDFScale);
  /*
   Create a low resolution image representation of the PDF page to display before the TiledPDFView renders its content.
   */
  UIGraphicsBeginImageContext(pageRect.size);
  CGContextRef context = UIGraphicsGetCurrentContext();
  // First fill the background with white.
  CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
  CGContextFillRect(context,pageRect);
  CGContextSaveGState(context);
  // Flip the context so that the PDF page is rendered right side up.
  CGContextTranslateCTM(context, 0.0, pageRect.size.height);
  CGContextScaleCTM(context, 1.0, -1.0);
  // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
  CGContextScaleCTM(context, PDFScale,PDFScale);
  CGContextDrawPDFPage(context, PDFPage);
  CGContextRestoreGState(context);
  UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  CFRelease(document);
  return backgroundImage;
}

- (UIImage *)getImageWithSignDocSDKRenderer {
  return nil;
  //  return getPageImage(self.imageView.frame.size.height, self.imageView.frame.size.width, self.imageView.frame.origin.x, self.imageView.frame.origin.y);
}

- (void) render {
  NSLog(@"controller render is called");
  self.documentWidth = getDocumentWidth();
  self.scale = self.scrollView.frame.size.width/self.documentWidth;
  double documentHeight = getDocumentHeight();
  [self.imageView setFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.width*documentHeight/self.documentWidth)];
  BOOL getImageWithSignDocSDKRenderer = YES;
  double startDate =[[NSDate date] timeIntervalSince1970];
  UIImage *renderedImage = getImageWithSignDocSDKRenderer ? [self getImageWithSignDocSDKRenderer] : [self getImageWithAppleRenderer];
  double endDate =[[NSDate date] timeIntervalSince1970];
  NSLog(@"Render duration: %f", endDate - startDate);
  self.imageView.image = renderedImage;
  if (!getImageWithSignDocSDKRenderer) {
    [self showSignatures];
  }
  self.scrollView.contentSize = CGSizeMake(self.imageView.frame.size.width, self.imageView.frame.size.height);
}

- (void)showSignatures {
  NSMutableArray *signatureSignDocRects = getSignaturesSignDocCoordinates();
  NSMutableArray *signatureRects = getSignatureRects();
  for (int i=0; i < signatureRects.count; i++) {
    UILabel *signatureLabel = [[UILabel alloc]init];
    CGRect currentSignatureFrame = [[signatureRects objectAtIndex:i]  CGRectValue];
    CGRect currentSignatureSignDocFrame = [[signatureSignDocRects objectAtIndex:i]  CGRectValue];
    [signatureLabel setFrame:CGRectMake(currentSignatureFrame.origin.x*self.scale, currentSignatureFrame.origin.y*self.scale, currentSignatureFrame.size.width*self.scale, currentSignatureFrame.size.height*self.scale)];
    //the coordinates do not depend on height. Height only determines the quality and will return an image of different sizes for the desired area
    UIImage *signatureImage = getSignatureImage(self.imageView.frame.size.height, currentSignatureSignDocFrame);
    [signatureLabel setBackgroundColor:[UIColor colorWithPatternImage:signatureImage]];
    [self.imageView addSubview:signatureLabel];
  }
  [self addSignaturesFrame];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  NSLog(@"controller viewDidLoad is called");
  //  initSignProcess(300, 300, NSTemporaryDirectory(), @"test.pdf");
  //  doTest();
  //set this option to YES in order to use a more customizable signing dialog
  handleSignatureDialog = YES;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  NSLog(@"controller viewDidAppear is called");
  //  [self render];
  //  [self addSignaturesFrame];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
  [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
  // Code here will execute before the rotation begins.
  // Equivalent to placing it in the deprecated method -[willRotateToInterfaceOrientation:duration:]
  //remove the previous signature frames
  [[self.imageView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
  [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    // Place code here to perform animations during the rotation.
    // You can pass nil or leave this block empty if not necessary.
  } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    // Code here will execute after the rotation has finished.
    // Equivalent to placing it in the deprecated method -[didRotateFromInterfaceOrientation:]
    //    [self render];
    [self addSignaturesFrame];
  }];
}

- (void)addSignaturesFrame {
  NSMutableArray *signatureRects = getSignatureRects();
  for (NSValue *currentSignatureFrameAsNSValue in signatureRects) {
    CGRect currentSignatureFrame = [currentSignatureFrameAsNSValue CGRectValue];
    UILabel *currentSignatureFrameLabel = [[UILabel alloc]init];
    [currentSignatureFrameLabel setFrame:CGRectMake(currentSignatureFrame.origin.x*self.scale, currentSignatureFrame.origin.y*self.scale, currentSignatureFrame.size.width*self.scale, currentSignatureFrame.size.height*self.scale)];
    [currentSignatureFrameLabel setBackgroundColor:[UIColor clearColor]];
    [currentSignatureFrameLabel.layer setBorderColor:[[UIColor redColor] CGColor]];
    [currentSignatureFrameLabel.layer setBorderWidth:2];
    [self.imageView addSubview:currentSignatureFrameLabel];
  }
}

- (IBAction)handleTapGesture:(UITapGestureRecognizer *)sender {
  CGPoint location =  [sender locationInView:sender.view];
  self.signatureFieldName = getNameOfSignatureWithLocation(CGPointMake(location.x / self.scale, location.y / self.scale));
  if (self.signatureFieldName) {
//    [self showCaptureDialog:100 signY:100 pdfFilePath:docFilePath pdfFileName:docFileName word:@""];
  }
}

- (BOOL)deviceIsIphone {
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    return NO;
  }
  return YES;
}

- (IBAction)signClicked:(id)sender {
  self.signatureFieldName = @"sig1";
//  [self showCaptureDialog:100 signY:100 pdfFilePath:docFilePath pdfFileName:docFileName word:@""];
}

- (NSMutableArray *)findTexts: (NSMutableArray *)texts{
  return startFindTexts(texts);
}

- (NSMutableArray *)convertPdfToPng:(NSString *)path{
  Boolean isSucceed = initSignProcess(300, 300, NSTemporaryDirectory(), @"temp.pdf", @"", @"");
  return startConvertPdfToPng(path);
}

- (BOOL)showCaptureDialog:(double)signX signY:(double)signY pdfFilePath:(NSString *)pdfFilePath pdfFileName: (NSString *)pdfFileName word:(NSString *)word order:(NSString *)order{
  NSLog(@"showCaptureDialog is called");
  BOOL isSucceed = NO;
  @try{
    isSucceed = initSignProcess(signX, signY, pdfFilePath, pdfFileName, word, order);
    
    if (isSucceed) {
      if ([self deviceIsIphone]) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft] forKey:@"orientation"];
      }
      UIImage *anImage = [UIImage imageNamed:@"logo.png"];
      NSArray *xibsArray = [self deviceIsIphone] ? [NSArray arrayWithObjects:@"MyCustomiPhoneDialog", nil] : [NSArray arrayWithObjects:@"MyCustomDialog", nil];
      // setting pen properties
      NSMutableDictionary *penPropertiesDictionary = [[NSMutableDictionary alloc]init];
      [penPropertiesDictionary setObject:[NSNumber numberWithInteger:7] forKey:IOSFOUNDATIONS_PEN_PROPERTIES_PEN_SIZE];
      [penPropertiesDictionary setObject:[UIColor blackColor] forKey:IOSFOUNDATIONS_PEN_PROPERTIES_COLOR];
      //this is the default setting for capture strategies. If you do not want to use some capture strategies from the list below, jsut remove them
      //      [penPropertiesDictionary setObject:[NSArray arrayWithObjects: [NSNumber numberWithInteger:IOSFOUNDATIONS_CAPTURE_STRATEGY_APPLE_PENCIL], [NSNumber numberWithInteger:IOSFOUNDATIONS_CAPTURE_STRATEGY_WACOM_BLUETOOTH_WITH_TOUCH_MANAGER], [NSNumber numberWithInteger:IOSFOUNDATIONS_CAPTURE_STRATEGY_HAND], nil]  forKey:IOSFOUNDATIONS_PEN_PROPERTIES_CAPTURE_STRATEGIES];
      
      //this is the default signing area border strategy. The sigature can go beyond the lmits of the signing area
      //      [penPropertiesDictionary setObject:[NSNumber numberWithInteger:IOSFOUNDATIONS_SIGNING_AREA_BORDER_STRATEGY_BEST_DATA] forKey:IOSFOUNDATIONS_SIGNING_AREA_BORDER_STRATEGY];
      NSLog(@"showCaptureDialog, %@", (handleSignatureDialog ? @"Yes" : @"No"));
      NSLog (@"showCaptureDialog: Padding Info: Assigned: X: %f |Y: %f", signX, signY);

      [SignPosInfo getPos].posX = signX;
      [SignPosInfo getPos].posY = signY;
      
      NSLog (@"showCaptureDialog: Padding Info: Retreived: X: %f |Y: %f", [SignPosInfo getPos].posX, [SignPosInfo getPos].posY);
      if (handleSignatureDialog) {
        self.dialog = [[SDSignatureCaptureController alloc]initWithDelegate:self backgroundImage:anImage dialogXibs:xibsArray languages:nil];
        [self.dialog setPenProperties:penPropertiesDictionary];
        [self addChildViewController:[self.dialog getViewController]];
        UIView *dialogView = [self.dialog getViewController].view;
        UIButton *testButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [testButton addTarget:self action:@selector(testButtonMethod) forControlEvents:UIControlEventTouchUpInside];
        //        [testButton setTitle:@"testCancelButton" forState:UIControlStateNormal];
        testButton.frame = CGRectMake(10, 10, 160, 40);
        [dialogView addSubview:testButton];
        dialogView.alpha = 0;
        
        [[self.dialog getViewController].view.layer setBorderWidth: 1.0f];
        [[self.dialog getViewController].view.layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
        [self.dialog getViewController].view.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:dialogView];
        [UIView animateWithDuration:1 animations:^{dialogView.alpha = 1;} completion:^(BOOL finished) {
          [[self.dialog getViewController] didMoveToParentViewController:self];
        }];
      } else {
        BOOL createCustomizedDialog = YES;
        if (createCustomizedDialog) {
          self.dialog = [[SDSignatureCaptureController alloc] initWithParent: self withDelegate: self backgroundImage:anImage dialogXibs:xibsArray languages:nil];
        } else {
          self.dialog = [[SDSignatureCaptureController alloc] initWithParent: self withDelegate: self];
        }
        [self.dialog setPenProperties:penPropertiesDictionary];
        [self.dialog setDialogPosition:IOSFOUNDATIONS_DIALOG_POSITION_CENTER];
        [self.dialog setTitle:@"Sign here"];
        [self.dialog captureSignature];
      }
    }
  } @catch (NSException *e) {
    NSLog(@"Exception: {name: %@, reason: %@}", [e name], [e reason]);
    isSucceed = NO;
  }
  return isSucceed;
}

- (IBAction)clearSignaturesClicked:(id)sender {
  clearSignatures();
  //  [self render];
}

- (IBAction)click2signClicked:(id)sender {
  NSLog(@"click2signClicked"); 
  UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Name" message:@"Enter your name:" preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    UITextField *textField = alertController.textFields.firstObject;
    NSLog(@"%@", textField.text);
    UIImage *signatureImage = [self getClickToSignImage:[NSString stringWithString:textField.text]];
    NSData *signatureData = UIImageJPEGRepresentation(signatureImage, 1);
    signProcess("sig1", (unsigned char *)[signatureData bytes], [signatureData length], (unsigned char *)[signatureData bytes], [signatureData length], FALSE, 0 , 0);
    //    [self render];
  }];
  okAction.enabled = NO;
  [alertController addAction:okAction];
  [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
  }];
  [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
  [self presentViewController:alertController animated:YES completion:nil];
}

- (void)alertTextFieldDidChange:(UITextField *)sender {
  UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
  if (alertController) {
    UITextField *textField = alertController.textFields.firstObject;
    UIAlertAction *okAction = alertController.actions.firstObject;
    okAction.enabled = textField.text.length > 0;
  }
}

- (UIImage *)getClickToSignImage:(NSString *)name {
  UIFont *defaultFont = [UIFont systemFontOfSize:15.0];
  UIGraphicsBeginImageContext(CGSizeMake(200, 120));
  CGContextRef context = UIGraphicsGetCurrentContext();
  //add a background
  CGContextSetRGBFillColor(context, 1.0, 1.0, 0.0, 1.0);
  CGContextFillRect(context, CGRectMake(0.0, 0.0, 200, 120));
  //add the text
  NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
  NSString *dateInfo = [NSString stringWithFormat:@"Date: %@",[formatter stringFromDate:[NSDate date]]];
  NSString *nameInfo = [NSString stringWithFormat:@"Name: %@",name];
  NSString *ipAddressInfo = [NSString stringWithFormat:@"IP Address: %@",[self getIPAddress]];
  CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0);
  [dateInfo drawAtPoint:CGPointMake(10, 10) withAttributes:@{NSFontAttributeName:defaultFont}];
  CGContextSetRGBFillColor(context, 1.0, 0.0, 1.0, 1.0);
  [nameInfo drawAtPoint:CGPointMake(10, 40) withAttributes:@{NSFontAttributeName:defaultFont}];
  CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0);
  [ipAddressInfo drawAtPoint:CGPointMake(10, 70) withAttributes:@{NSFontAttributeName:defaultFont}];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}

- (void) handleSignature: (CFDataRef) points withFieldId: (NSString *) fieldId { 
  // NSLog(@"handleSignature is called, %@, %@", points, fieldId);
  NSData *signatureData;
  UIImage *signatureImage;
  NSData *imgData;
  
  double posX = [SignPosInfo getPos].posX;
  double posY = [SignPosInfo getPos].posY;

NSLog (@"handleSignature: Padding Info: Retreived: X: %f |Y: %f", posX, posY);
  
  if (!points)
    return;
  
  signatureData = [self.dialog signatureAsISO19794Simple];
  signatureImage = [self.dialog signatureAsUIImage];
  
  BOOL getSignatureImageAsPNG = TRUE;
  if (getSignatureImageAsPNG) {
    signatureImage = [self fixSemiTransparenceInImage:signatureImage];
    [self writeImageToFile:signatureImage];
  }
  imgData = getSignatureImageAsPNG ? [NSData dataWithData:UIImagePNGRepresentation(signatureImage)] : [NSData dataWithData:UIImageJPEGRepresentation(signatureImage, 1)];
  signProcess([self.signatureFieldName UTF8String], (unsigned char *)[signatureData bytes], [signatureData length], (unsigned char *)[imgData bytes], [imgData length], FALSE, posX , posY);
  
  //  [self render];
  if (handleSignatureDialog) {
    [[self.dialog getViewController].view removeFromSuperview];
    [[self.dialog getViewController] removeFromParentViewController];
    self.dialog = nil;
  }
}

- (void) abortSignature: (NSString *) fieldId {
  if (handleSignatureDialog) {
    [[self.dialog getViewController].view removeFromSuperview];
    [[self.dialog getViewController] removeFromParentViewController];
    self.dialog = nil;
  }
}

- (void)testButtonMethod {
  [UIView animateWithDuration:0.5 animations:^{[self.dialog getViewController].view.alpha = 0;} completion:^(BOOL finished) {
    [[self.dialog getViewController].view removeFromSuperview];
    [[self.dialog getViewController] removeFromParentViewController];
    self.dialog = nil;
  }];
}

- (void) pointHistoryX: (CGFloat) x y:(CGFloat) y p: (CGFloat) p t: (unsigned long long) t {
  NSLog(@"x:%.0f y:%.0f p:%.0f t:%llu", x, y, p, t);
}

- (IBAction)saveDocumentClicked:(id)sender {
  NSLog(@"saveDocumentClicked is called");
  saveDocument();
}

- (NSString *)getIPAddress {
  struct ifaddrs *interfaces = NULL;
  struct ifaddrs *temp_addr = NULL;
  NSString *wifiAddress = nil;
  NSString *cellAddress = nil;
  if(!getifaddrs(&interfaces)) {
    temp_addr = interfaces;
    while(temp_addr != NULL) {
      sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
      if(sa_type == AF_INET || sa_type == AF_INET6) {
        NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
        NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
        if([name isEqualToString:@"en0"]) {
          wifiAddress = addr;
        } else if([name isEqualToString:@"pdp_ip0"]) {
          cellAddress = addr;
        }
      }
      temp_addr = temp_addr->ifa_next;
    }
    freeifaddrs(interfaces);
  }
  NSString *addr = wifiAddress ? wifiAddress : cellAddress;
  return addr ? addr : @"0.0.0.0";
}

- (void)eraseSignature {
  NSLog(@"Signature erased");
}

- (IBAction)capturePhotoClicked:(id)sender {
  if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;
    imagePickerController.showsCameraControls = NO;
    [[NSBundle mainBundle] loadNibNamed:@"CameraView" owner:self options:nil];
    self.overlayView.frame = imagePickerController.cameraOverlayView.frame;
    imagePickerController.cameraOverlayView = self.overlayView;
    self.overlayView = nil;
    self.imagePickerController = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
  } else {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"No camera" message:@"There is no camera" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
  }
}

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
      NSLog(@"imagePickerController"); 
  UIImage *signatureImage = [info valueForKey:UIImagePickerControllerOriginalImage];
  UIImage *scaledImage = [signatureImage resizedImage:CGSizeMake(signatureImage.size.width/10, signatureImage.size.height/10) interpolationQuality:kCGInterpolationDefault];
  
  NSData *signatureData = UIImageJPEGRepresentation(scaledImage, 1);
  signProcess("sig1", (unsigned char *)[signatureData bytes], [signatureData length], (unsigned char *)[signatureData bytes], [signatureData length], FALSE, 0, 0);
  
  [self finishAndUpdate];
  //  [self render];
}

- (IBAction)cancel:(id)sender {
  [self finishAndUpdate];
}

- (IBAction)takePhoto:(id)sender {
  [self.imagePickerController takePicture];
}

- (void)finishAndUpdate {
  [self dismissViewControllerAnimated:YES completion:NULL];
  self.imagePickerController = nil;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)writeImageToFile:(UIImage *)image {
  NSData *imgData = UIImagePNGRepresentation(image);
  NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.png"];
  [imgData writeToFile:imagePath atomically:YES];
}

- (UIImage *)fixSemiTransparenceInImage:(UIImage *)image {
  CGImageRef imageRef = [image CGImage];
  NSUInteger width = CGImageGetWidth(imageRef);
  NSUInteger height = CGImageGetHeight(imageRef);
  if (height * width == 0) {
    return nil;
  }
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
  int bytesPerPixel = 4;
  NSUInteger bytesPerRow = bytesPerPixel * width;
  int bitsPerComponent = 8;
  CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
  
  CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
  CGContextRelease(context);
  
  [self removeMarginAlmostBlackBlocks:rawData width:width height:height bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
  [self fixByAddingTransparency:rawData width:width height:height bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
  UInt8 * m_PixelBuf = malloc(sizeof(UInt8) * height * width * 4);
  CGContextRef ctx = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
  CGImageRef newImgRef = CGBitmapContextCreateImage(ctx);
  CGColorSpaceRelease(colorSpace);
  CGContextRelease(ctx);
  free(m_PixelBuf);
  free(rawData);
  UIImage *finalImage = [UIImage imageWithCGImage:newImgRef];
  CGImageRelease(newImgRef);
  return finalImage;
}

- (void)removeMarginAlmostBlackBlocks:(unsigned char *)rawData width:(NSUInteger)width height:(NSUInteger)height bytesPerRow:(NSUInteger)bytesPerRow  bytesPerPixel:(int)bytesPerPixel {
  for(int  y = 0; y < height; y++) {
    bool colorIsDark = TRUE;
    for(int x = 0; x < width; x++) {
      NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
      int red = rawData[byteIndex];
      int green = rawData[byteIndex + 1];
      int blue = rawData[byteIndex + 2];
      if (red + green + blue >= 10) {
        colorIsDark = FALSE;
        break;
      }
    }
    if (colorIsDark) {
      return;
    }
    for(int x = 0; x < width; x++) {
      NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
      int red = rawData[byteIndex];
      int green = rawData[byteIndex + 1];
      int blue = rawData[byteIndex + 2];
      if (red + green + blue < 10) {
        rawData[byteIndex+3] = 0;
      }
    }
  }
}

- (void)fixByAddingTransparency:(unsigned char *)rawData width:(NSUInteger)width height:(NSUInteger)height bytesPerRow:(NSUInteger)bytesPerRow  bytesPerPixel:(NSUInteger)bytesPerPixel {
  for(int  y = 0; y < height; y++) {
    for(int x = 0; x < width; x++) {
      NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
      int alpha = rawData[byteIndex + 3];
      if (alpha != 0 && alpha != 255) {
        //http://en.wikipedia.org/wiki/Alpha_compositing#Alpha_blending for a white background
        rawData[byteIndex]  = (int)((1 - rawData[byteIndex+3]/255.0) * 255 + rawData[byteIndex+3]/255.0 * rawData[byteIndex]);
        rawData[byteIndex+1]  = (int)((1 - rawData[byteIndex+3]/255.0) * 255 + rawData[byteIndex+3]/255.0 * rawData[byteIndex+1]);
        rawData[byteIndex+2]  = (int)((1 - rawData[byteIndex+3]/255.0) * 255 + rawData[byteIndex+3]/255.0 * rawData[byteIndex+2]);
        rawData[byteIndex+3] = 255;
      }
    }
  }
}


//- (void)deviceConnected:(int)deviceType {
//  switch (deviceType) {
//    case IOSFOUNDATIONS_DEVICE_TYPE_WACOM_BLUETOOTH_1:
//      NSLog(@"A wacom pen was connected");
//      break;
//    case IOSFOUNDATIONS_DEVICE_TYPE_APPLE_PENCIL_1:
//      //currently not happening too often; call retrieveConnectivityStatusForDeviceType if you want to know the connectivity status for some reason
//      NSLog(@"An apple pencil was connected");
//      break;
//    default:
//      break;
//  }
//}
//
//- (void)deviceDisconnected:(int)deviceType {
//  if (deviceType == IOSFOUNDATIONS_DEVICE_TYPE_WACOM_BLUETOOTH_1) {
//    NSLog(@"A wacom pen was disonnected");
//  }
//}

@end

