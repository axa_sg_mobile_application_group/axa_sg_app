#import <UIKit/UIKit.h>
static NSString *docFilePath = nil;
static NSString *docFileName = nil;
static double docSignFieldX = 0;
static double docSignFieldY = 0;

void doTest ();
BOOL initSignProcess (double signX, double signY, NSString *pdfFilePath, NSString *pdfFileName, NSString *word, NSString *order);
int signProcess (const char *field_name_sig, unsigned char *signature_data, size_t signature_size, unsigned char *signature_img, size_t image_size, BOOL selfSigned, double signX, double signY);
struct SIGNDOC_ByteArray *renderTest(unsigned int height);
UIImage *getPageImage(unsigned int height, unsigned int width, CGFloat x, CGFloat y);
UIImage * getSignatureImage(unsigned int height, CGRect frame);
NSMutableArray* getSignaturesSignDocCoordinates();
NSString* getNameOfSignatureWithLocation(CGPoint location);

unsigned char *readFile (const char *path, size_t *size);

NSMutableArray* getSignatureRects();
double getDocumentHeight();
double getDocumentWidth();
void clearSignatures();
void saveDocument();
NSMutableArray *startConvertPdfToPng(NSString *path);
NSMutableArray *startFindTexts(NSMutableArray *texts);
