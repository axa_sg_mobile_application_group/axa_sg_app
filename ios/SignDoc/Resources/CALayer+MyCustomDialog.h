//
//  CALayer+MyCustomDialog.h
//  EASE
//
//  Created by Xue Hua on 19/12/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (MyCustomDialog)
@property(nonatomic, assign) UIColor *borderUIColor;
@end
