//
//  CALayer+MyCustomDialog.m
//  EASE
//
//  Created by Xue Hua on 19/12/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "CALayer+MyCustomDialog.h"

@implementation CALayer (MyCustomDialog)

-(void)setBorderUIColor:(UIColor*)color
{
  self.borderColor = color.CGColor;
}
-(UIColor*)borderUIColor
{
  return [UIColor colorWithCGColor:self.borderColor];
}


@end
