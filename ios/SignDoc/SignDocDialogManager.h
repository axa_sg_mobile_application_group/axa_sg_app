//
//  SignDocDialogManager.h
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//
#import <React/RCTViewManager.h>
#import <signdociosfoundations/SPiOSFoundationsAPI.h>
#import <signdociosfoundations/SDSignatureHandler.h>
#import <signdociosfoundations/SDSignatureCaptureController.h>
#import <signdociosfoundations/SDDeviceManager.h>
#import "SignDocDialogView.h"

@interface SignDocDialogManager : RCTViewManager

@property (nonatomic, strong) SignDocDialogView *signDocDialogView;

@end
