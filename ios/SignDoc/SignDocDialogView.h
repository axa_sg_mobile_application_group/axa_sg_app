//
//  SignDocDialogView.h
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <React/RCTInvalidating.h>
#import <React/RCTView.h>
#import <React/RCTBridgeModule.h>

@class RCTBridge;
@class SignDocDialogViewController;

@interface SignDocDialogView : RCTView <RCTInvalidating>

#pragma mark - Method

- (instancetype _Nonnull )initWithBridge:(RCTBridge *_Nullable)bridge NS_DESIGNATED_INITIALIZER;

- (BOOL)showCaptureDialog:(double)signX signY:(double)signY pdfFilePath:(NSString *_Nonnull)pdfFilePath pdfFileName:(NSString *_Nonnull)pdfFileName word:(NSString *)word order:(NSString *)order;
- (void)dismissViewController;
- (NSMutableArray *) convertPdfToPng: (NSString *)path;
- (NSMutableArray *) findTexts: (NSMutableArray *)texts;

@end
