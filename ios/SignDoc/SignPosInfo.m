//
//  SignPosInfo.m
//  EASE
//
//  Created by Felix on 9/1/2019.
//  _
//

#import "SignPosInfo.h"

@implementation SignPosInfo
@synthesize posX;
@synthesize posY;

static SignPosInfo *getPos = nil;

+ (SignPosInfo*)getPos {
  if (getPos == nil ) {
    getPos = [[super allocWithZone:NULL] init];
    getPos.posX = 0;
    getPos.posY = 0;
  }
  return getPos;
}

+ (id)allocWithZone:(NSZone *)zone {
  @synchronized(self)
  {
    if (getPos == nil)
    {
      getPos = [super allocWithZone:zone];
      return getPos;
    }
  }
  return nil;
}

- (id)copyWithZone:(NSZone *)zone {
  return self;
}
/** ARC Conflicts
-(id)retain {
  return self;
}

-(NSUInteger)retainCount {
  return NSUIntegerMax;  //denotes an object that cannot be released
}

-(void)release {
  //do nothing
}

-(id)autorelease {
  return self;
}
*/
@end
